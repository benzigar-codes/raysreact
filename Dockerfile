FROM nginx:alpine
RUN rm -rf /var/www/
RUN rm /etc/nginx/conf.d/default.conf
COPY ./build /var/www/reactProject
COPY nginx.conf /etc/nginx/conf.d