import { atom, useRecoilState } from "recoil";

const alignAtom = atom({
  key: "alignAtom",
  default: "left",
});

export default function useAlign() {
  const [align, setAlign] = useRecoilState(alignAtom);
  const alignLeft = () => {
    setAlign("left");
    document.querySelector("html").setAttribute("dir", "ltr");
  };
  const alignRight = () => {
    setAlign("right");
    document.querySelector("html").setAttribute("dir", "rtl");
  };
  const toggleAlign = () => (align === "left" ? alignRight() : alignLeft());
  return { align, alignLeft, alignRight, toggleAlign };
}
