import React from "react";
import U from "../utils/utils.js";
import A from "../utils/API.js";
import imageCompression from "browser-image-compression";

export default function useImage() {
  // Just to show image size in console
  const bytesToSize = (bytes) => {
    var sizes = ["Bytes", "KB", "MB", "GB", "TB"];
    if (bytes == 0) return "0 Byte";
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + " " + sizes[i];
  };

  const isBase64 = (str) => {
    if (str === "" || str.trim() === "") {
      return false;
    } else if (str.includes("base64")) return true;
    else return false;
    // try {
    //   return btoa(atob(str)) == str;
    // } catch (err) {
    //   return false;
    // }
  };

  const imageUrl = (url) => (url && url.includes("digitaloceanspaces") ? url : A.HOST + url);

  // Input image as base 64
  const compressImage = async (image) => {
    const toBeBlob = await fetch(image);
    const blob = await toBeBlob.blob();
    const options = {
      maxSizeMB: 0.8,
      maxWidthOrHeight: 800,
    };
    const converted = await imageCompression(blob, options);
    return converted;
  };

  const blobToBase64 = (blob) => {
    const reader = new FileReader();
    reader.readAsDataURL(blob);
    return new Promise((resolve) => {
      reader.onloadend = () => {
        resolve(reader.result);
      };
    });
  };

  return { compressImage, bytesToSize, blobToBase64, imageUrl, isBase64 };
}
