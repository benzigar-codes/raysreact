import React from "react";
import { atom, selector, useRecoilState, useRecoilValue } from "recoil";
import Cookies from "js-cookie";
import axios from "axios";
import _ from "lodash";

import A from "../utils/API.js";
import NavLinks from "../utils/navLinks.json";
import Privileges from "../utils/privileges.js";
import { useHistory } from "react-router";
import useUtils from "./useUtils";
import useLanguage from "./useLanguage";
import useAlign from "./useAlign";

export const adminAtom = atom({
  key: "adminAtom",
  default: null,
});

const isAdminSelector = selector({
  key: "isAdminSelector",
  get: ({ get }) => {
    return get(adminAtom) == null ? false : true;
  },
});

export default function useAdmin() {
  const { mergePrivilege } = useUtils();
  const [admin, setAdmin] = useRecoilState(adminAtom);
  const { fetchLanguage } = useLanguage();
  const { alignLeft, alignRight } = useAlign();
  const isAdmin = useRecoilValue(isAdminSelector);
  const history = useHistory();

  const refreshImage = () => {
    const avatar = admin.avatar;
    setAdmin({ ...admin, avatar: null });
    setTimeout(() => setAdmin({ ...admin, avatar }, 100));
  };

  const header = admin
    ? {
        headers: {
          Authorization: `Bearer ${admin.accessToken}`,
          "Content-Type": "application/json",
        },
      }
    : null;

  const adminLogIn = async (object, cookie = true) => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_LOG_IN_CONFIRM_OTP, object);
      if (cookie === true) Cookies.set("AD", { accessToken: data.accessToken }, { expires: 300 });
      if (data.languageCode !== undefined) {
        fetchLanguage(data.languageCode);
      }
      setAdmin(mergePrivileges(data));
      return true;
    } catch (err) {
      return err;
    }
  };

  const authFailure = (err) => {
    if (err.response && err.response.status === 401) {
      setAdmin(null);
      Cookies.remove("AD");
      history.push(NavLinks.ADMIN_HOME);
    }
  };

  const adminProfileFetch = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_PROFILE_GET, {}, header);
      setAdmin({ ...admin, ...data, ...mergePrivileges(data) });
    } catch (err) {
      authFailure(err);
    }
  };

  const adminLogout = () => {
    setAdmin(null);
    Cookies.remove("AD");
  };

  const mergePrivileges = (data) => {
    return Cookies.get("AD")
      ? data.extraPrivileges
        ? {
            ...data,
            privileges: mergePrivilege(
              Privileges,
              mergePrivilege(data.privileges || Privileges, data.extraPrivileges || {})
            ),
          }
        : data
      : null;
  };

  const checkAdmin = async () => {
    const credentials = Cookies.get("AD") ? JSON.parse(Cookies.get("AD")) : null;
    if (credentials == null) setAdmin(credentials);
    else {
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_VALIDATE,
          {},
          {
            headers: {
              Authorization: `Bearer ${credentials.accessToken}`,
              "Content-Type": "application/json",
            },
          }
        );
        if (data.languageCode !== undefined) {
          fetchLanguage(data.languageCode);
        }
        setAdmin(mergePrivileges(data));
        return true;
      } catch (err) {
        return false;
      }
    }
  };

  const token = admin != null ? admin.accessToken : null;

  // React.useEffect(() => console.log(admin), [admin]);

  return {
    admin,
    setAdmin,
    isAdmin,
    checkAdmin,
    header,
    adminLogIn,
    adminLogout,
    adminProfileFetch,
    token,
    refreshImage,
    authFailure,
  };
}
