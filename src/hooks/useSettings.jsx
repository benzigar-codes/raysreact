import axios from "axios";
import { atom, useRecoilState } from "recoil";
import settingsJSON from "../utils/settings.json";
import A from "../utils/API.js";
import useAdmin from "./useAdmin";

const settingsAtom = atom({
  key: "setttingsAtom",
  default: null,
  // default: settingsJSON,
});

export default function useSettings() {
  const [settings, setSettings] = useRecoilState(settingsAtom);
  const { authFailure } = useAdmin();
  const fetchSettings = async () => {
    try {
      const { data } = await axios.get(A.HOST + A.ADMIN_COMMON_CONFIG);
      setSettings(data.data);
      document.getElementById("favicon").setAttribute("href", A.HOST + data.data.favicon);
      document.getElementById("mobile_icon").setAttribute("href", A.HOST + data.data.mobileLogo);
      return data;
    } catch (err) {
      alert("Server Under Construction, Retry later ! ");
      authFailure(err);
      return false;
    }
  };
  const defaultDialCode = settings && settings.isCountryCodeNeeded ? settings.defaultDialCode : "";
  return { settings, setSettings, fetchSettings, defaultDialCode };
}
