import React from "react";
const useDebug = (debugStates) => {
  React.useEffect(() => {
    console.clear();
    Object.keys(debugStates).map((key) => {
      console.log(`%c${key}`, "color: blue");
      console.dir(JSON.stringify(debugStates[key]));
    });
  }, [debugStates]);
};
export default useDebug;
