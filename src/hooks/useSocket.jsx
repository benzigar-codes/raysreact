import React from "react";
import io from "socket.io-client";
import A from "../utils/API.js";
import useAdmin from "../hooks/useAdmin";
import useSettings from "../hooks/useSettings";

export default function useSocket() {
  const adminSocket = React.useRef(null);
  const notificationSocket = React.useRef(null);
  const { settings } = useSettings();

  const [firstRun, setFirstRun] = React.useState(true);
  const { admin, token } = useAdmin();

  const connectSocket = () => {
    if (admin !== null && firstRun) {
      // adminSocket.current = io(A.SOCKET_HOST_ADMIN, {
      //   forceNew: true,
      //   path: "/socket.io",
      //   reconnection: true,
      //   reconnectionDelay: 2000, //starts with 2 secs delay, then 4, 6, 8, until 60 where it stays forever until it reconnects
      //   reconnectionDelayMax: 60000, //1 minute maximum delay between connections
      //   reconnectionAttempts: "Infinity", //to prevent dead clients, having the user to having to manually reconnect after a server restart.
      //   timeout: 10000,
      //   transports: [settings.isSocketPolling === false ? "websocket" : "polling"],
      //   query: {
      //     accessToken: `${token}`,
      //     deviceId: token,
      //   },
      // });
      // adminSocket.current.on("connect", () => console.log("Admin Socket Connected !!! "));
      // adminSocket.current.on("disconnection", () => connectSocket());
      notificationSocket.current = io(A.SOCKET_HOST_ADMIN, {
        forceNew: true,
        path: "/socket.io",
        reconnection: true,
        reconnectionDelay: 2000, //starts with 2 secs delay, then 4, 6, 8, until 60 where it stays forever until it reconnects
        reconnectionDelayMax: 60000, //1 minute maximum delay between connections
        reconnectionAttempts: "Infinity", //to prevent dead clients, having the user to having to manually reconnect after a server restart.
        timeout: 10000,
        transports: [settings.isSocketPolling === false ? "websocket" : "polling"],
        query: {
          accessToken: `${token}`,
          deviceId: token,
        },
      });
      notificationSocket.current.on("connect", () => console.log("Admin Socket Connected !!! "));
      // notificationSocket.current.on("disconnection", () => connectSocket());
      setFirstRun(false);
    }
  };

  React.useEffect(() => {
    connectSocket();
  }, [admin]);

  return {
    adminSocket,
    notificationSocket,
  };
}
