import React from "react";

export default function usePrompt() {
  const [prompt, setPrompt] = React.useState({
    title: null,
    message: null,
    click: () => {},
    show: false,
    close: () => setPrompt({ ...prompt, show: false }),
    type: null,
  });

  const showPrompt = (title, message, type = "add", callback) =>
    setPrompt({ ...prompt, show: true, title, message, type, click: callback });

  return { prompt, showPrompt };
}
