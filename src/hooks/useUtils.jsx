import React from "react";
import useLanguage from "./useLanguage";
import _ from "lodash";
import A from "../utils/API.js";
import U from "../utils/utils.js";
import { adminAtom } from "./useAdmin";
import { useRecoilState } from "recoil";

export default function useUtils() {
  const { language } = useLanguage();
  const [admin, setAdmin] = useRecoilState(adminAtom);

  const paymentSectionFields = [
    {
      id: 1,
      label: language.ALL,
      value: "ALL",
    },
    {
      id: 2,
      label: language.CASH_AND_WALLET,
      value: "CASHANDWALLET",
    },
  ];

  const customizer = (objValue, srcValue) => (_.isEmpty(srcValue) ? srcValue : _.merge(objValue, srcValue));

  const mergePrivilege = (fromDef = {}, target = {}) => {
    var def = JSON.parse(JSON.stringify(fromDef));
    for (var nav in def)
      for (var sub in def[nav])
        for (var innerSub in def[nav][sub]) {
          def[nav][sub][innerSub] =
            target[nav] !== undefined
              ? target[nav][sub] !== undefined
                ? target[nav][sub][innerSub] !== undefined
                  ? target[nav][sub][innerSub]
                  : def[nav][sub][innerSub]
                : def[nav][sub][innerSub]
              : def[nav][sub][innerSub];
        }
    return def;
  };

  const hasAlphabet = (string) => {
    var regExp = /[a-zA-Z]/g;
    if (regExp.test(string)) {
      return true;
    } else {
      return false;
    }
  };

  const morph = (text) => {
    if (admin.userType === "DEVELOPER" || admin.userType === "ADMIN") return text;
    else if (
      admin &&
      admin.privileges &&
      admin.privileges.OTHERS &&
      admin.privileges.OTHERS.MORPH_TEXT &&
      admin.privileges.OTHERS.MORPH_TEXT.VIEW === true
    ) {
      const length = text.toString().length;
      let firstText = text.toString().substring(0, 2);
      for (let i = 0; i < length - 2; i++) firstText = firstText += "X";
      return truncate(firstText, 10);
    } else return text;
  };

  const videoUrl = (url) => (url.includes("digitaloceanspaces") ? url : A.HOST + url);

  const parseError = (err) => {
    return err.response.status === 500
      ? language[err.response.data.message] !== undefined
        ? language[err.response.data.message]
        : err.response.data.message
      : "";
  };

  const truncate = (str, num) => {
    if (str.length > num) {
      return str.slice(0, num) + "...";
    } else {
      return str;
    }
  };

  const diffMinutes = (dt2, dt1) => {
    var diff = (dt2.getTime() - dt1.getTime()) / 100;
    diff /= 60;
    return Math.abs(Math.round(diff));
  };

  return {
    parseError,
    paymentSectionFields,
    customizer,
    mergePrivilege,
    hasAlphabet,
    truncate,
    diffMinutes,
    videoUrl,
    morph,
  };
}
