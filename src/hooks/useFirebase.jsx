import React from "react";

import firebase from "firebase";
import U from "../utils/utils.js";

if (U.mode === "zayRide") {
  // ZayRide Configuration
  if (!firebase.apps.length) {
    firebase.initializeApp({
      apiKey: "AIzaSyDEEctsvjqDG3alV1X0UwRCFQL0ZCH_0I8",
      authDomain: "zay-trak.firebaseapp.com",
      databaseURL: "https://zay-trak.firebaseio.com",
      projectId: "zay-trak",
      storageBucket: "zay-trak.appspot.com",
      messagingSenderId: "134452006434",
      appId: "1:134452006434:web:ab885a725203cc4edb447b",
      measurementId: "G-2Z1DW3FPVD",
    });
  } else {
    firebase.app();
  }
} else if (U.mode === "pamworld") {
  // Pamworld
  if (!firebase.apps.length) {
    firebase.initializeApp({
      apiKey: "AIzaSyBto5Xcir9gVwB6RzUglyWP1kh0XlS5HYE",
      authDomain: "pamdrive-drivers-209406.firebaseapp.com",
      databaseURL: "https://pamdrive-drivers-209406.firebaseio.com",
      projectId: "pamdrive-drivers-209406",
      storageBucket: "pamdrive-drivers-209406.appspot.com",
      messagingSenderId: "196730039656",
      appId: "1:196730039656:web:364be9d94dc3c4e648fadd",
    });
  } else {
    firebase.app();
  }
} else if (U.mode === "starMov") {
  // StarMov
  if (!firebase.apps.length) {
    firebase.initializeApp({
      apiKey: "AIzaSyB8kRNsWTnH6ihRn_DgbVrIJmfciZtWA94",
      authDomain: "starmov-1c215.firebaseapp.com",
      databaseURL: "https://starmov-1c215-default-rtdb.firebaseio.com",
      projectId: "starmov-1c215",
      storageBucket: "starmov-1c215.appspot.com",
      messagingSenderId: "929584710641",
      appId: "1:929584710641:web:132a4cb9ab5973ba9cfa50",
      measurementId: "G-CX6E2JBR28",
    });
  } else {
    firebase.app();
  }
} else {
  // Zervx Configuration
  if (!firebase.apps.length) {
    firebase.initializeApp({
      apiKey: "AIzaSyB0OF6Llm7yBYWQRYFij5EBQmhnDxuMbWc",
      authDomain: "zervx-7b622.firebaseapp.com",
      databaseURL: "https://zervx-7b622-default-rtdb.firebaseio.com",
      projectId: "zervx-7b622",
      storageBucket: "zervx-7b622.appspot.com",
      messagingSenderId: "469093796333",
      appId: "1:469093796333:web:71687a0e0684811d99bfe4",
      measurementId: "G-XTCC9ETJZX",
    });
  } else {
    firebase.app();
  }
}

const db = firebase.firestore();

export default function useFirebase() {
  return {
    firebase,
    db,
  };
}
