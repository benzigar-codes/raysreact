import { atom, useRecoilState } from "recoil";
import axios from "axios";
import LanguageJSON from "../utils/language.json";
import A from "../utils/API.js";

const languageAtom = atom({
  key: "languageAtom",
  // default: null,
  default: LanguageJSON,
});

export default function useLanguage() {
  const [language, setLanguage] = useRecoilState(languageAtom);

  const fetchLanguage = async (lang) => {
    try {
      // const { data } = await axios.post(A.HOST + A.ADMIN_LANGUAGE_KEYS, { languageCode: lang });
      // if (data.languageKeys) setLanguage(data.languageKeys);
      // return data.languageKeys || language;
    } catch (err) {}
  };

  return { language, setLanguage, fetchLanguage };
}
