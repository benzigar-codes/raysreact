import React from "react";

import { useHistory } from "react-router-dom";

import useLanguage from "./useLanguage";
import useSettings from "./useSettings";
import usePrompt from "./usePrompt";

const useDataTable = (dataHeadings = [], dataFields = [], defaultShowField = "ALL") => {
  // HOOKS
  const { language } = useLanguage();
  const { settings } = useSettings();
  const { prompt, showPrompt } = usePrompt();
  const history = useHistory();

  const [loading, setLoading] = React.useState(true);

  const [search, setSearch] = React.useState("");
  const [selected, setSelected] = React.useState([]);

  const [pagination, setPagination] = React.useState({
    skip: 0,
    limit: settings.pageViewLimits[0],
  });

  const [showField, setShowField] = React.useState(
    defaultShowField === "ALL" ? language.ALL : defaultShowField
  );

  const [popup, setPopup] = React.useState(null);

  const [total, setTotal] = React.useState(0);

  const [headings, setHeadings] = React.useState(dataHeadings);
  const [fields, setFields] = React.useState(dataFields);

  const removeFromDom = () => {
    setFields(
      fields
        .map((field) =>
          selected.filter((select) => select === field._id).length > 0 ? null : field
        )
        .filter((field) => field != null)
    );
    setTotal(total - selected.length);
    setPagination({
      ...pagination,
      skip: parseInt(pagination.skip) - parseInt(selected.length),
    });
  };

  const setBulkFresh = async () => {
    // const { status, data } = await fetchAPIToggleStatus(selected, collection, 1);
    // if (status === 200) {
    //   setFields(
    //     fields.map((field) =>
    //       selected.filter((select) => select === field._id).length > 0 ? { ...field, fresh: 1 } : field
    //     )
    //   );
    //   setPopup({ title: language.ACTIVATED, type: "success" });
    // } else {
    //   setPopup({ title: data.data, type: "error" });
    // }
    setSelected([]);
  };

  React.useEffect(() => {
    defaultShowField === "ALL"
      ? setShowField(language.ALL)
      : setShowField(defaultShowField);
  }, [language]);

  return {
    //  Variables
    loading,
    selected,
    headings,
    fields,
    showField,
    search,
    popup,
    total,
    pagination,
    prompt,
    // Set Variables
    setFields,
    setShowField,
    setHeadings,
    setSearch,
    setPopup,
    setTotal,
    setPagination,
    setLoading,
    setSelected,
    showPrompt,
    // Others
    removeFromDom,
  };
};
export default useDataTable;
