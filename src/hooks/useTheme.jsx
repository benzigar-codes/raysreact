import { atom, useRecoilState } from "recoil";

const themeAtom = atom({
  key: "themeAtom",
  default: localStorage.theme ? localStorage.theme : "light",
});
export default function useTheme() {
  const [theme, setTheme] = useRecoilState(themeAtom);
  theme === "light" ? document.documentElement.classList.remove('dark') : document.documentElement.classList.add('dark')
  const toggleTheme = () => {
    if (theme === "light") {
        setTheme("dark")
        document.documentElement.classList.add('dark')
        localStorage.theme = 'dark'
    }
    else {
        setTheme("light")
        document.documentElement.classList.remove('dark')
        localStorage.theme = 'light'
    };
  };
  return { theme, toggleTheme };
}
