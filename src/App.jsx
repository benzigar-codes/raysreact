import { useEffect } from "react";
import React, { useState } from "react";
import "react-datepicker/dist/react-datepicker.css";
import ReactGA from "react-ga";

import useSettings from "./hooks/useSettings";
import useAlign from "./hooks/useAlign";
import useLanguage from "./hooks/useLanguage";
import { MainRoutes } from "./routes/Routes";
import useTheme from "./hooks/useTheme";
import PageLoading from "./components/Loading";
import { useHistory } from "react-router";

export default function App() {
  const history = useHistory();
  const [loading, setLoading] = useState(true);
  const { settings, fetchSettings } = useSettings();
  const { language, fetchLanguage } = useLanguage();
  const { alignLeft, alignRight } = useAlign();

  useEffect(() => {
    return history.listen((location) => {
      // console.log(location.pathname);
      // ReactGA.pageview(window.location.pathname + window.location.search);
      window.scrollTo(0, 0);
    });
  }, [history]);

  useEffect(() => {
    localStorage.theme === "light"
      ? document.documentElement.classList.remove("dark")
      : document.documentElement.classList.add("dark");
    setTimeout(() => fetchingBasicThings(), 1000);
  }, []);

  const fetchingBasicThings = async () => {
    const { data } = await fetchingSettings();
    if (data) {
      data.languageDirection === "RTL" ? alignRight() : alignLeft();
    }
    if (data && data.languageCode) await fetchingLanguage(data.languageCode);
  };

  useEffect(() => {
    if (language != null && settings != null) {
      setLoading(false);
      // ReactGA.initialize("G-5SFS56JRSC");
      // ReactGA.pageview("/");
    }
  }, [language, settings]);

  const fetchingSettings = async () => await fetchSettings();

  const fetchingLanguage = async (lang) => await fetchLanguage(lang);

  return (
    <>
      {loading ? (
        <PageLoading />
      ) : (
        <>
          <MainRoutes />
        </>
      )}
    </>
  );
}
