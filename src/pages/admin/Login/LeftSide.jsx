import React from "react";
import mapImage from "../../../assets/images/map.jpg";
import { BlackScreen } from "./BlackScreen";

export const LeftSide = ({ ...rest }) => {
  return (
    <>
      <div {...rest}>
        <img
          className="absolute inset-0 object-cover h-full w-full z-10"
          src={mapImage}
          alt="" />
        <BlackScreen />
        <div className="hidden lg:absolute h-full w-full z-40 lg:flex justify-center items-center">
          <lottie-player
            src="https://assets10.lottiefiles.com/packages/lf20_eivbhioz.json"
            background="transparent"
            speed="1"
            style={{ widht: 500, height: 500 }}
            loop
            autoplay
          ></lottie-player>
        </div>
      </div>
    </>
  );
};
