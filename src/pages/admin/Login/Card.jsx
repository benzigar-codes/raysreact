import React from "react";
import { useRecoilState } from "recoil";
import useLanguage from "../../../hooks/useLanguage";
import {
  FiLogIn,
  FiSkipBack
} from "react-icons/fi";
import useSettings from "../../../hooks/useSettings";
import { adminLogInAtom } from "./adminLogInAtom";
import { LogIn } from "./LogIn";
import { ForgotPassword } from "./ForgotPassword";
import { Error } from "./Error";

export const Card = () => {
  const { language } = useLanguage();
  const { settings } = useSettings();
  const [state, setState] = useRecoilState(adminLogInAtom);
  return (
    <div className="fade bg-white absolute inset-0 lg:relative text-black shadow-md p-5 sm:p-20 lg:p-10 dark:bg-gray-900 dark:text-white">
      <div className="flex items-center justify-between">

        {state.page === "forgotForm" && (
          <div
            onClick={() => setState({ ...state, page: "logIn" })}
            className="transition cursor-pointer hover:bg-blue-800 bg-green-800 px-4 py-2 text-white flex items-center justify-center"
          >
            <FiSkipBack />
          </div>
        )}
        {state.page === "confirmCode" && (
          <div
            onClick={() => setState({ ...state, page: "forgotForm" })}
            className="transition cursor-pointer hover:bg-blue-800 bg-green-800 px-4 py-2 text-white flex items-center justify-center"
          >
            <FiSkipBack />
          </div>
        )}
        <h1 className="text-4xl font-bold">{settings.site_title}</h1>

        {state.page === "logIn" && (
          <p
            onClick={() => setState({ ...state, page: "forgotForm" })}
            className="mx-4 text-blue-800 cursor-pointer dark:text-gray-300"
          >
            {language.Signin.FRGT_PWD}
          </p>
        )}
      </div>

      <div className="text-xl text-blue-800 mt-5 dark:text-white flex items-center">
        <FiLogIn />

        <p className="mx-3 font-bold">{language.Menu.ADMIN}</p>
      </div>

      <div className="mt-3 text-sm text-gray-700 dark:text-gray-300 border-b-4 pb-4">

        <p>{language.Signin.LINEONE}</p>
        <p>{language.Signin.LINETWO}</p>
      </div>

      {state.error !== "" && <Error title={state.error} />}

      {state.page === "logIn" ? <LogIn /> : <ForgotPassword />}
    </div>
  );
};
