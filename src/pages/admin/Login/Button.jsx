import React from "react";
import { FiLoader } from "react-icons/fi";

export const Button = ({ title, click, btnLoading = false, ...rest }) => {

  return (
    //   Sign in
    <button onClick={click} className="flex focus:outline-none items-center rounded-xl transition justify-center fade bg-green-800 hover:bg-blue-800 w-full dark:bg-blue-800 text-white px-3 py-2" {...rest}>
      <p>{title}</p>
      {btnLoading == true && <FiLoader className="mx-3 animate-spin" />}
    </button>
  );
};
