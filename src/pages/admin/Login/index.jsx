// DEPEDENCIES
import React from "react";
import { Helmet } from "react-helmet";
import gsap from "gsap";
import * as yup from "yup";
import { useHistory } from "react-router-dom";
import { useFormik } from "formik";
import axios from "axios";

// JSONS
import A from "../../../utils/API.js";
import NavLinks from "../../../utils/navLinks.json";
import U from "../../../utils/utils.js";

// HOOKS
import useLanguage from "../../../hooks/useLanguage";
import useAdmin from "../../../hooks/useAdmin";
import useSettings from "../../../hooks/useSettings";
import useImage from "../../../hooks/useImage";

// COMPONENTS
import { FieldWrapper } from "../../../components/common/FieldWrapper";
import { TextField } from "../../../components/common/TextField";
import { CountryCodesPicker } from "../../../components/common/CountryCodesPicker";
import { ToggleButton } from "../../../components/common/ToggleButton";
import { Button } from "./Button";

// IMAGES
import Full from "../../../assets/images/Front/lightBG.svg";
import FullDark from "../../../assets/images/Front/darkBG.svg";
import logoDark from "../../../assets/images/Front/logoDark.svg";
import logoLight from "../../../assets/images/Front/logoLight.svg";

import zayRideLogo from "../../../assets/images/Front/zayride.png";
import pamDriveLogo from "../../../assets/images/Front/pamdrive.png";

import arrow from "../../../assets/images/Front/Arrow Black.svg";
import circleBlue from "../../../assets/images/Front/Circles/Blue Circle.svg";
import circleGreen from "../../../assets/images/Front/Circles/Green Circle.svg";
import circleOrange from "../../../assets/images/Front/Circles/Orange Circle.svg";
import circlePurple from "../../../assets/images/Front/Circles/Purple Circle.svg";
import useUtils from "../../../hooks/useUtils";
import useFirebase from "../../../hooks/useFirebase";

// MAIN COMPONENT
const Index = () => {
  // HOOKS
  const { language } = useLanguage();
  document.title = language.LOG_IN;
  const { admin, adminLogIn } = useAdmin();
  const history = useHistory();
  const { settings } = useSettings();
  const { imageUrl } = useImage();
  const { parseError } = useUtils();
  const { firebase } = useFirebase();

  // STATES
  const [step, setStep] = React.useState(1);
  const [keepLogged, setKeepLogged] = React.useState(true);
  const [error, setError] = React.useState("");
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [showReSendOtp, setShowReSendOtp] = React.useState(false);
  const [resendLoading, setResendLoading] = React.useState(true);
  const [email, setEmail] = React.useState("");
  const [showGoogle, setShowGoogle] = React.useState(false);

  // PHONE NUMBER SUBMIT
  const phoneNumber = useFormik({
    initialValues: { phoneCode: "", phoneNumber: "" },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      phoneCode: yup.string().required(language.REQUIRED),
      phoneNumber: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_LOGIN_IN_OTP, {
          phoneCode: e.phoneCode,
          phoneNumber: e.phoneNumber,
        });
        otp.setFieldValue("phoneCode", e.phoneCode);
        otp.setFieldValue("phoneNumber", e.phoneNumber);
        // if (settings.smsType === "TWILIO") {
        if (data.OTP) otp.setFieldValue("otp", data.OTP);
        setStep(2);
        setEmail(data.email);
        setError("");
        setTimeout(() => setShowReSendOtp(true), 10000);
        // }
        // if (settings.smsType === "FIREBASE") {
        //   setShowGoogle(true);
        //   setTimeout(() => {
        //     const appVerifier = new firebase.auth.RecaptchaVerifier("recaptcha");
        //     firebase
        //       .auth()
        //       .signInWithPhoneNumber(e.phoneCode + e.phoneNumber, appVerifier)
        //       .then((confirmationResult) => {
        //         setStep(2);
        //         setShowGoogle(false);
        //         window.confirmationResult = confirmationResult;
        //       })
        //       .catch((error) => {
        //         setShowGoogle(false);
        //         alert(error);
        //       });
        //   }, 300);
        // }
      } catch (error) {
        setError(parseError(error));
        // if (error.response.data.message === "Invalid credentials") setError(language.NUMBER_NOT_FOUND);
      }
      setBtnLoading(false);
      setResendLoading(false);
    },
  });

  // OTP SUBMIT
  const otp = useFormik({
    initialValues: { phoneCode: "", phoneNumber: "", otp: "" },
    onSubmit: async (e) => {
      setBtnLoading(true);
      // if (settings.smsType === "FIREBASE") {
      //   window.confirmationResult
      //     .confirm(otp.values.otp)
      //     .then(async (result) => {
      //       const ifLog = await adminLogIn({ ...e, otp: "" }, keepLogged);
      //       if (ifLog === true) setError("");
      //       setBtnLoading(false);
      //     })
      //     .catch((error) => {
      //       setError(language.INVALID_OTP);
      //       setBtnLoading(false);
      //     });
      // }
      // if (settings.smsType === "TWILIO") {
      const ifLog = await adminLogIn(e, keepLogged);
      if (ifLog !== true) setError(parseError(ifLog));
      if (ifLog === true) setError("");
      setBtnLoading(false);
      // }
    },
  });

  // ANIMATIONS
  React.useEffect(() => {
    gsap.fromTo(".fade", 0.3, { opacity: 0, x: 5 }, { opacity: 1, x: 0, stagger: 0.2 });
  }, []);

  React.useEffect(() => {
    setError("");
  }, [step]);

  // REDIRECT AFTER LOG IN
  React.useEffect(() => {
    if (admin != null) history.push(NavLinks.ADMIN_HOME);
    console.log(admin);
  }, [admin, history]);

  return (
    <div className="opacity-0 fade min-h-screen flex flex-wrap bg-white text-black dark:bg-gray-800 dark:text-white">
      {/* LEFT SIDE  */}
      <div className="opacity-0 fade hidden w-full lg:w-1/2 lg:flex relative h-screen">
        <img className="absolute" src={circleBlue} style={{ height: 30, left: 10, top: 20 }} alt="" />
        <div className="absolute inset-0 flex justify-center items-center">
          <img
            src={document.getElementsByTagName("html")[0].classList.contains("dark") ? FullDark : Full}
            style={{ maxHeight: 470 }}
            alt=""
          />
        </div>
        <div className="absolute inset-0 flex justify-center items-center">
          <img
            src={
              document.getElementsByTagName("html")[0].classList.contains("dark")
                ? U.mode === "zayRide"
                  ? zayRideLogo
                  : U.mode === "pamworld"
                  ? pamDriveLogo
                  : logoDark
                : U.mode === "zayRide"
                ? zayRideLogo
                : U.mode === "pamworld"
                ? pamDriveLogo
                : logoLight
            }
            style={{ height: 80 }}
            alt=""
          />
        </div>
      </div>
      {/* RIGHT SIDE  */}
      <div className="opacity-0 fade w-full lg:w-1/2 h-screen flex flex-col justify-between">
        <img className="absolute" src={circleBlue} style={{ height: 30, left: 10, top: 20 }} alt="" />
        <img className="absolute" src={circleGreen} style={{ height: 30, right: 10, bottom: 20 }} alt="" />
        {/* LOGO  */}
        <div className="opacity-0 fade p-2 lg:p-5 flex justify-end">
          <img
            className="h-10 object-cover"
            src={
              document.getElementsByTagName("html")[0].classList.contains("dark")
                ? imageUrl(settings.darkLogo)
                : imageUrl(settings.lightLogo)
            }
            alt=""
          />
        </div>
        <div className="opacity-0 fade flex justify-center items-center">
          {/* PHONE NUMBER FORM  */}
          {step === 1 && showGoogle === false && (
            <form onSubmit={phoneNumber.handleSubmit} className="p-3 lg:p-10" style={{ maxWidth: 450 }}>
              <FieldWrapper>
                <h1 className="text-4xl" style={{ fontFamily: "Ubuntu, sans-serif" }}>
                  {language.LOG_IN}
                </h1>
              </FieldWrapper>
              <FieldWrapper title={"Enter the Phone Number"}>
                <CountryCodesPicker
                  placeholder={language.DIAL_CODE}
                  margin={3}
                  width="4/12"
                  defaultValue={phoneNumber.values.phoneCode}
                  change={(e) => phoneNumber.setFieldValue("phoneCode", e)}
                  error={phoneNumber.errors.phoneCode}
                />
                <TextField
                  width="8/12"
                  type="number"
                  placeholder={language.PHONE}
                  defaultValue={phoneNumber.values.phoneNumber}
                  change={(e) => phoneNumber.setFieldValue("phoneNumber", e)}
                  error={phoneNumber.errors.phoneNumber}
                />
              </FieldWrapper>

              <FieldWrapper>
                <Button type={"submit"} btnLoading={btnLoading} title={language.CONTINUE} />
              </FieldWrapper>
              <FieldWrapper>
                {error !== "" && <p className={"bg-white w-full text-center p-2 rounded-xl text-red-500"}>{error}</p>}
              </FieldWrapper>
            </form>
          )}
          {showGoogle && (
            <FieldWrapper>
              <div className="flex justify-center w-full">
                <div id="recaptcha"></div>
              </div>
            </FieldWrapper>
          )}
          {/* OTP FORM  */}
          {step === 2 && (
            <form
              onSubmit={
                otp.values.otp.length > 0 && btnLoading === false ? otp.handleSubmit : (e) => e.preventDefault()
              }
              className="p-3 lg:p-10"
              style={{ maxWidth: 450 }}
            >
              <FieldWrapper padding={false}>
                {resendLoading === true ? (
                  <h1 className="text-3xl">{language.RESENDING_OTP}</h1>
                ) : (
                  <h1 className="text-3xl">{language.ENTER_PIN}</h1>
                )}
              </FieldWrapper>
              <FieldWrapper>
                <p className="text-sm text-gray-500" style={{ fontSize: 12 }}>
                  {/* {new Array(phoneNumber.values.phoneNumber.length - 1).join("*")}
                  {phoneNumber.values.phoneNumber.substring(phoneNumber.values.phoneNumber.length - 2)},{" "} */}
                  {phoneNumber.values.phoneNumber} , {email.split("@")[0].substr(0, 4)}
                  {email !== "" && new Array(email.split("@")[0].length - 3).join("*")}@{email.split("@")[1]}
                </p>
              </FieldWrapper>
              <FieldWrapper title={language.PIN_RULES}>
                <TextField
                  type="password"
                  align={"center"}
                  change={(e) => (e.match(/^[0-9]+$/) != null || e === "") && otp.setFieldValue("otp", e)}
                  value={otp.values.otp}
                  maxLength="6"
                  minLength="6"
                  error={error === "" ? "" : error}
                />
              </FieldWrapper>
              <FieldWrapper>
                <Button btnLoading={btnLoading} title={language.LOG_IN} />
              </FieldWrapper>
              <hr className="my-5 dark:bg-gray-400" />
              <div className="flex flex-col justify-center">
                <p
                  onClick={() => setStep(1)}
                  className="text-sm mt-2 text-gray-500 text-center cursor-pointer hover:underline"
                >
                  {language.WRONG_NUMBER}
                </p>
                {/* <p className="text-sm mt-2 text-center cursor-pointer">{language.DEFAULT_PIN}</p> */}
                {/* {showReSendOtp === true && (
                  <p
                    onClick={() => {
                      setResendLoading(true);
                      phoneNumber.submitForm();
                    }}
                    className="text-sm mt-2 text-gray-500 text-center cursor-pointer hover:underline"
                  >
                    {language.TAKING_LONG}
                  </p>
                )} */}
              </div>
            </form>
          )}
        </div>
        {/* FOOTER  */}
        {U.mode === "zayRide" && (
          <div className="p-10 flex justify-center">
            <p className="text-sm text-gray-500">
              Developed By zayTech
              {/* <a href="https://www.berarkrays.com/" rel="noreferrer" target={"_blank"} className="hover:underline">
              berarkrays.com
            </a> */}
            </p>
          </div>
        )}
        {U.mode !== "zayRide" && (
          <div className="p-10 flex justify-center">
            <p className="text-sm text-gray-500">
              Developed By
              <a href="https://www.berarkrays.com/" rel="noreferrer" target={"_blank"} className="mx-2 hover:underline">
                berarkrays.com
              </a>
            </p>
          </div>
        )}
      </div>
    </div>
  );
};
export default Index;
