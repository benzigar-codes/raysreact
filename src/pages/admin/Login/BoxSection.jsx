import React from "react";
import { FiLogIn } from "react-icons/fi";

export const BoxSection = ({
  icon = <FiLogIn />,
  title = "Username",
  placeholder = "",
  type = "text",
  ...rest
}) => {
  return (
    <div className="flex flex-col">
      <div className="flex items-center font-bold mt-4 text-lg">
        {icon}
        <p className="mx-4">{title} : </p>
      </div>
      <input
        {...rest}
        type={type}
        placeholder={placeholder}
        className="fade mt-3 px-4 py-3 focus:outline-none text-black bg-gray-300 border-b-4 border-green-800 dark:border-blue-800 dark:text-white dark:bg-gray-600"
        name="" />
    </div>
  );
};
