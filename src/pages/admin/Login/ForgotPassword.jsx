import React from "react";
import { useRecoilState } from "recoil";
import { adminLogInAtom } from "./adminLogInAtom";
import { ConfirmCode } from "./ConfirmCode";
import { ForgotPasswordForm } from "./ForgotPasswordForm";

export const ForgotPassword = () => {
  const [state, setState] = useRecoilState(adminLogInAtom);
  return (
    <>
      {state.page === "forgotForm" && <ForgotPasswordForm />}
      {state.page === "confirmCode" && <ConfirmCode />}
    </>
  );
};
