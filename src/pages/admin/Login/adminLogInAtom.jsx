import { atom } from "recoil";

// State in this page

export const adminLogInAtom = atom({
  key: "adminPageAtom",
  default: {
    username: "",
    password: "",
    page: "logIn",
    btnLoading: false,
    error: "",
    remember: true,
    success: false,
    expire: "",
    code: null,
  },
});
