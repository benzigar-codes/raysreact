import React from "react";
import { useRecoilState } from "recoil";
import { BsToggleOff, BsToggleOn } from "react-icons/bs";
import { adminLogInAtom } from "./adminLogInAtom";

export const RememberToggle = () => {
  const [state, setState] = useRecoilState(adminLogInAtom);

  return (
    <div className="flex items-center mt-3">
      {state.remember === true ? (
        <BsToggleOn
          onClick={() => setState({ ...state, remember: !state.remember })}
          className="cursor-pointer text-3xl text-green-800 dark:text-blue-800" />
      ) : (
          <BsToggleOff
            onClick={() => setState({ ...state, remember: !state.remember })}
            className="cursor-pointer text-3xl text-green-800 dark:text-blue-800" />
        )}

      <p className="mx-3 text-sm text-gray-600">Remember me ? </p>
    </div>
  );
};
