import React from "react";
import { useRecoilState } from "recoil";
import useLanguage from "../../../hooks/useLanguage";
import { FiLock, FiUser } from "react-icons/fi";
import useAdmin from "../../../hooks/useAdmin";
import { Button } from "./Button";
import { BoxSection } from "./BoxSection";
import { RememberToggle } from "./RememberToggle";
import { adminLogInAtom } from "./adminLogInAtom";

export const LogIn = () => {
  const { admin, adminLogIn } = useAdmin();
  const { language } = useLanguage();

  const [state, setState] = useRecoilState(adminLogInAtom);

  const formSubmit = async (e) => {
    e.preventDefault();
    setState({ ...state, btnLoading: true });
    if (state.username == "" || state.password == "")
      setState({
        ...state,
        error: language.Signin.INVALID_ACCOUNT,
        btnLoading: false,
      });
    else {
      const data = await adminLogIn(
        state.username,
        state.password,
        state.remember
      );
      if (data != true) {
        setState({
          ...state,
          error: language.Signin.INVALID_ACCOUNT,
          btnLoading: false,
        });
      }

      if (data == true) {
        setState({ ...state, error: "", btnLoading: false, success: true });
      }
    }
  };

  return (
    <form onSubmit={formSubmit} type="get" className="flex flex-col">
      <BoxSection
        onKeyUp={(e) => setState({ ...state, username: e.target.value })}
        icon={<FiUser />}
        type="text"
        title={language.Signin.USERNAME_EMAIL}
        placeholder={language.Signin.USERNAME_EMAIL}
      />

      <BoxSection
        onKeyUp={(e) => setState({ ...state, password: e.target.value })}
        icon={<FiLock />}
        type="password"
        title={language.Signin.PWD}
        placeholder="*******"
      />
      <RememberToggle />
      <Button
        title={language.Signin.SIGNIN}
        onClick={formSubmit}
        title={language.Signin.SIGNIN}
      />
    </form>
  );
};
