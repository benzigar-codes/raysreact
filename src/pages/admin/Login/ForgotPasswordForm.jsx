import React, { useEffect, useState } from "react";
import { useRecoilState } from "recoil";
import useLanguage from "../../../hooks/useLanguage";
import { FiUser } from "react-icons/fi";
import useFetch from "../../../hooks/useFetch";
import { Button } from "./Button";
import { BoxSection } from "./BoxSection";
import { adminLogInAtom } from "./adminLogInAtom";

export const ForgotPasswordForm = () => {
  const [state, setState] = useRecoilState(adminLogInAtom);

  const [error, setError] = useState(state.error);
  const [page, setPage] = useState(state.page);
  const [expire, setExpire] = useState(state.expire);
  const [btnLoading, setBtnLoading] = useState(state.btnLoading);

  useEffect(() => {
    setState({ ...state, error, page, btnLoading, expire });
  }, [error, page, btnLoading, expire]);

  const { language } = useLanguage();
  const { fetchAPIForgotPassword } = useFetch();

  const getCode = async (e) => {
    e.preventDefault();
    setBtnLoading(true);
    const results = await fetchAPIForgotPassword(state.username);
    setBtnLoading(false);
    if (results === false)
      setError(language.Signin.INVALID_EMAIL);
    else {
      setExpire(results.data.expiryAt);
      setError("");
      setPage("confirmCode");
    }
  };

  return (
    <form onSubmit={getCode} className="flex flex-col">

      <BoxSection
        onKeyUp={(e) => setState({ ...state, username: e.target.value })}
        icon={<FiUser />}
        type="email"
        title={language.Signin.EMAIL}
        placeholder={language.Signin.EMAIL} />
      <Button title={language.Common.SUBMIT} />
    </form>
  );
};
