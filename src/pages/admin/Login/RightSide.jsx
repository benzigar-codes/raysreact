import React from "react";
import { Card } from "./Card";

export const RightSide = ({ ...rest }) => {
  return (
    <div {...rest}>
      <Card />
    </div>
  );
};
