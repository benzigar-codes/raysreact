import React from "react";

export const BlackScreen = () => {
  return (
    <div className="absolute top-0 left-0 right-0 bottom-0 bg-black z-20 opacity-50"></div>
  );
};
