import React, { useEffect, useState } from "react";
import { useRecoilState } from "recoil";
import useLanguage from "../../../hooks/useLanguage";
import { FiClock, FiUser } from "react-icons/fi";
import useAdmin from "../../../hooks/useAdmin";
import { useHistory } from "react-router-dom";
import useFetch from "../../../hooks/useFetch";
import { Button } from "./Button";
import { BoxSection } from "./BoxSection";
import { adminLogInAtom } from "./adminLogInAtom";
import NavLinks from "../../../utils/navLinks.json";
import Cookies from "js-cookie";

export const ConfirmCode = () => {
  const history = useHistory();
  const { admin, checkAdminWithoutCookie } = useAdmin();
  const { fetchAPIForgotPasswordConfirm } = useFetch();
  const [state, setState] = useRecoilState(adminLogInAtom);
  const [date, setDate] = useState(new Date());
  const [error, setError] = useState(state.error);
  const [page, setPage] = useState(state.page);
  const [btnLoading, setBtnLoading] = useState(state.btnLoading);
  const { language } = useLanguage();

  useEffect(() => {
    setState({ ...state, btnLoading, error, page });
  }, [btnLoading, error, page]);

  const timeDifference = (date1, date2) => {
    var difference = date1.getTime() - date2.getTime();
    var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
    difference -= hoursDifference * 1000 * 60 * 60;
    var minutesDifference = Math.floor(difference / 1000 / 60);
    difference -= minutesDifference * 1000 * 60;
    var secondsDifference = Math.floor(difference / 1000);
    return { hoursDifference, minutesDifference, secondsDifference };
  };

  const confirmCode = async (e) => {
    e.preventDefault();
    setBtnLoading(true);
    const data = await fetchAPIForgotPasswordConfirm(state.username, state.code);
    if (data === false) setError(language.Signin.INVALID_CODE);
    if (data.data) {
      Cookies.set("AD", data, { expires: 300 });
      const check = await checkAdminWithoutCookie(data.data);
      if (check === true) {
        setBtnLoading(false);
        setPage("logIn");
        history.push(NavLinks.ADMIN_CHANGE_PASSWORD);
      }
    }
    setBtnLoading(false);
  };

  useEffect(() => {
    setInterval(() => {
      setDate(new Date());
    }, 1000);
  }, []);

  const TimeDifference = () => {
    return (
      <div className="flex items-center">
        <p>{timeDifference(new Date(state.expire), date).minutesDifference} :</p>
        <p className="ml-2">{timeDifference(new Date(state.expire), date).secondsDifference}</p>
      </div>
    );
  };
  return (
    <form onSubmit={confirmCode} className="p-2 flex flex-col">
      <h1 className="flex justify-center items-center text-blue-800 dark:text-white font-bold space-x-3">
        <p>{language.Signin.EXPIRE_AT} </p>
        <FiClock />

        <TimeDifference />
      </h1>
      <BoxSection
        onKeyUp={(e) => setState({ ...state, code: e.target.value })}
        icon={<FiUser />}
        type="text"
        title={language.Signin.CODE}
        placeholder={language.Signin.CODE}
      />
      <Button title="Submit" />
    </form>
  );
};
