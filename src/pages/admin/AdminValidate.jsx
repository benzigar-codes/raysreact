import React, { useEffect } from "react";
import Cookies from "js-cookie";
import { Redirect, useHistory } from "react-router-dom";
import gsap from "gsap";
import useAdmin from "../../hooks/useAdmin";
import { Helmet } from "react-helmet";
import { FaRobot } from "react-icons/fa";
import PageLoading from "../../components/Loading";

export default function AdminValidate() {
  const history = useHistory();
  const { admin, checkAdmin } = useAdmin();
  const remember = Cookies.get("AD") ? JSON.parse(Cookies.get("AD")) : null;

  useEffect(() => {
    const fetchEverything = async () => {
      gsap.fromTo(".fade", 0.3, { opacity: 0 }, { opacity: 1, stagger: 0.3 });
      setTimeout(async () => {
        const result = await checkAdmin();
        if (result === false) {
          history.push("/admin");
          Cookies.remove("AD");
        }
      }, 1000);
    };
    fetchEverything();
  }, []);

  useEffect(() => {
    if (admin != null) history.push(Cookies.get("loc"));
  }, [admin]);

  return remember ? (
    <>
      {/* <Helmet>
        <title>Validating ... </title>
      </Helmet>
      <div className="text-xl bg-gray-800 text-white min-h-screen flex justify-center items-center">
        <div className="animate-pulse">
          <div className="flex items-center">
            <h1 className="flex items-center">
              Verifying you are not a robot <FaRobot className={"mx-2"} /> ...{" "}
            </h1>
          </div>
        </div>
      </div> */}
      <PageLoading />
    </>
  ) : (
    <Redirect to="/admin/" />
  );
}
