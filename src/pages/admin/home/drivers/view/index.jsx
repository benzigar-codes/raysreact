import React from "react";
import gsap from "gsap/gsap-core";
import { useParams } from "react-router";
import axios from "axios";
import { format } from "date-fns";
import Lottie from "react-lottie";

import NavLinks from "../../../../../utils/navLinks.json";
import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";

import { FormWrapper } from "../../../../../components/common/FormWrapper";
import JSONViewer from "../../../../../components/common/JSONViewer";
import WalletRecharge from "../../../../../components/WalletRecharge";
import { Section } from "../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { Border } from "../../../../../components/common/Border";
import TrackingMap from "../../../../../components/TrackingMap";
import { Heading } from "../../../../../components/common/Heading";
import { TextArea } from "../../../../../components/common/TextArea";
import { Button } from "../../../../../components/common/Button";
import { DetailsWrapper } from "../../../../../components/common/DetailsWrapper";
import { Detail } from "../../../../../components/common/Detail";
import { Documents } from "../../../../../components/common/Documents";
import ImagePreview from "../../../../../components/common/ImagePreview";
import { PopUp } from "../../../../../components/common/PopUp";
import { SmallLoader } from "../../../../../components/common/SmallLoader";

import useLanguage from "../../../../../hooks/useLanguage";
import useAdmin from "../../../../../hooks/useAdmin";
import useImage from "../../../../../hooks/useImage";
import useUtils from "../../../../../hooks/useUtils";
import { Link } from "react-router-dom";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, authFailure, admin } = useAdmin();
  const { id } = useParams();
  const { morph } = useUtils();
  const [showRechargePage, setShowRechargePage] = React.useState(false);
  const { imageUrl } = useImage();

  const [note, setNote] = React.useState("");
  const [verifyLoading, setVerifyLoading] = React.useState(null);
  const [loading, setLoading] = React.useState(true);
  const [popup, setPop] = React.useState(null);
  const [driver, setDriver] = React.useState({});
  const [notesLoading, setNotesLoading] = React.useState(false);
  const [imageModel, setImagModel] = React.useState(false);

  const noteSave = async () => {
    setNotesLoading(true);
    try {
      await axios.post(
        A.HOST + A.ADMIN_DRIVERS_NOTES,
        {
          professionalId: id,
          notes: note,
        },
        header
      );
    } catch (err) {}
    setNotesLoading(false);
  };

  const fetchDriver = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_DRIVERS_READ, { id }, header);
      setDriver(data);
      setNote(data.notes ? data.notes : "");
      setLoading(false);
    } catch (err) {
      authFailure(err);
      history.goBack();
    }
  };

  const updateByPass = async (value) => {
    setLoading(true);
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_DRIVERS_UPDATE_BYPASS,
        {
          id: id,
          isOtpBypass: value,
        },
        header
      );
      fetchDriver();
    } catch (err) {
      authFailure(err);
      setLoading(false);
    }
  };

  React.useEffect(() => {
    fetchDriver();
  }, []);

  const verify = async (docid, docName, docStatus, docExpiry) => {
    setVerifyLoading(docName);
    try {
      await axios.post(
        A.HOST + A.ADMIN_DRIVERS_PROFILE_VERIFY,
        {
          professionalId: id,
          status: docStatus === U.VERIFIED ? U.UNVERIFIED : U.VERIFIED,
          documentName: docName,
          expiryDate: docExpiry,
        },
        header
      );
      const profileVerification = driver.profileVerification.map((each) => {
        if (each.documentName === docName) {
          return { ...each, status: docStatus === U.VERIFIED ? U.UNVERIFIED : U.VERIFIED };
        } else return each;
      });
      setDriver({ ...driver, profileVerification });
    } catch (err) {
      setPop({ title: language.DOCUMENT_UNVERIFIED_ERROR, type: "error" });
    }
    setVerifyLoading(null);
  };

  React.useEffect(() => {
    if (loading === false) gsap.fromTo(".viewDriver", 0.8, { opacity: 0 }, { opacity: 1, stagger: 0.2 });
  }, [loading]);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      animate="viewDriver"
      width="3/4"
      bread={[{ id: 1, title: language.DRIVERS, path: NavLinks.ADMIN_DRIVERS_LIST }]}
      title={morph(driver.firstName) + " " + morph(driver.lastName)}
    >
      {showRechargePage && (
        <WalletRecharge
          fetchData={fetchDriver}
          type={showRechargePage}
          link={A.HOST + A.ADMIN_DRIVERS_WALLET_RECHARGE}
          wallet={driver?.wallet?.availableAmount}
          userDetails={{
            userType: "PROFESSIONAL",
            id: driver._id,
            phone: driver.phone?.code + " " + driver.phone?.number,
          }}
          setShowRechargePage={setShowRechargePage}
        />
      )}
      <Section width="1/2">
        {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
        <ImagePreview close={() => setImagModel(false)} show={imageModel} />
        <Border animate="viewDriver">
          <div className="flex justify-between items-center">
            <Heading title={language.PERSONAL_DETAILS} />
            <div className="flex">
              <div className="w-full flex justify-end">
                <Link to={NavLinks.ADMIN_RIDES_DRIVERS + "/" + id}>
                  <Button title={language.VIEW_RECENT_RIDES} />
                </Link>
              </div>
            </div>
          </div>
          <FieldWrapper>
            <DetailsWrapper>
              <Detail title={language.FIRSTNAME} value={morph(driver.firstName)} />
              <Detail title={language.LASTNAME} value={morph(driver.lastName)} />
              <Detail title={language.EMAIL} value={morph(driver.email)} />
              <Detail title={language.MOBILE} value={morph(driver.phone.code) + " " + morph(driver.phone.number)} />
              <Detail title={language.GENDER} value={driver.gender} />
              <Detail title={language.DATE_OF_BIRTH} value={driver.dob} />
              <Detail title={language.CURRENCY_CODE} value={driver.currencyCode} />
              {driver.languageCode && driver.languageCode !== null && (
                <Detail title={language.LANG_CODE} value={driver.languageCode} />
              )}
              {driver.review && driver.review !== null && (
                <Detail title={language.AVG_RATINGS} value={parseFloat(driver.review?.avgRating).toFixed(2)} />
              )}
              {admin.userType === "DEVELOPER" && (
                <Detail
                  title={language.OTP_BYPASS}
                  value={
                    <div onClick={() => updateByPass(!driver.isOtpBypass)} className="cursor-pointer">
                      {driver.isOtpBypass ? language.TRUE : language.FALSE}
                    </div>
                  }
                />
              )}
              <Detail
                title={language.PRO_IMG}
                value={<img className="w-full" src={imageUrl(driver.avatar)} alt={"Driver"} />}
              />
            </DetailsWrapper>
          </FieldWrapper>
        </Border>
        {driver.vehicles.map((vehicle) => (
          <Border animate="viewDriver">
            <Heading title={language.VEHICLE_DETAILS} />
            <FieldWrapper>
              <DetailsWrapper>
                <Detail title={language.PLATE_NUMBER} value={vehicle?.plateNumber} />
                <Detail title={language.VIN_NUMBER} value={vehicle?.vinNumber} />
                <Detail title={language.MODEL} value={vehicle?.model} />
                <Detail title={language.YEAR} value={vehicle?.year} />
                <Detail title={language.DOOR_NO} value={vehicle?.noOfDoors} />
                <Detail title={language.SEATS_NO} value={vehicle?.noOfSeats} />
              </DetailsWrapper>
            </FieldWrapper>
          </Border>
        ))}
        <Border animate="viewDriver">
          <Heading title={language.NOTES} />
          <FieldWrapper>
            <TextArea change={(e) => setNote(e)} value={note} />
          </FieldWrapper>
          <FieldWrapper>
            <Button onClick={noteSave} loading={notesLoading} title={language.SAVE} />
          </FieldWrapper>
        </Border>
        <JSONViewer data={driver} />
      </Section>
      <Section width="1/2">
        <Border animate="viewDriver">
          <div className="flex justify-between items-center">
            <Heading title={language.WALLET} />
            <div className="flex">
              <Button click={() => setShowRechargePage("CREDIT")} title={language.RECHARGE_WALLET} />
              <Button click={() => setShowRechargePage("DEBIT")} title={language.DEBIT_WALLET} />
            </div>
          </div>
          <FieldWrapper>
            <DetailsWrapper>
              <Detail title={language.BALANCE} value={parseFloat(driver.wallet?.availableAmount).toFixed(2)} />
              <Detail title={language.DUE_AMOUNT} value={driver.wallet?.dueAmount} />
              <Detail title={language.FREEZED_AMOUNT} value={driver.wallet?.freezedAmount} />
              <Detail
                // title={language.TRANSACTION_HISTORY}
                value={
                  <div className="w-full flex justify-end">
                    <Link to={NavLinks.ADMIN_DRIVERS_TRANSACTION + "/" + id}>
                      <Button title={language.VIEW_TRANSACTION_HISTORY} />
                    </Link>
                  </div>
                }
              />
            </DetailsWrapper>
          </FieldWrapper>
        </Border>
        <Border animate="viewDriver">
          <Heading title={language.ADDRESS_DETAILS} />
          <FieldWrapper>
            <DetailsWrapper>
              <Detail title={language.ADDRESS_LINE_1} value={morph(driver.address.line)} />
              <Detail title={language.CITY} value={morph(driver.address.city)} />
              <Detail title={language.STATE} value={morph(driver.address.state)} />
              <Detail title={language.COUNTRY} value={morph(driver.address.country)} />
              <Detail title={language.ZIPCODE} value={morph(driver.address.zipcode)} />
            </DetailsWrapper>
          </FieldWrapper>
        </Border>

        <Border animate="viewDriver">
          <Heading title={language.PROFESSIONAL_STATUS} />
          <FieldWrapper>
            <DetailsWrapper>
              <Detail title={language.STATUS} value={driver.onlineStatus ? language.ONLINE : language.OFFLINE} />
              <Detail
                title={language.ON_GOING_STATUS}
                value={driver.bookingInfo?.ongoingBooking ? language.YES : language.NO}
              />
              <Detail
                title={language.TAIL_RIDE}
                value={driver.bookingInfo?.ongoingTailBooking ? language.YES : language.NO}
              />
            </DetailsWrapper>
          </FieldWrapper>
          {admin.userType === "DEVELOPER" && (
            <TrackingMap
              user={false}
              professional={{
                _id: id,
                firstName: driver.firstName,
                lastName: driver.lastName,
                phone: {
                  code: driver.phone.code,
                  number: driver.phone.number,
                },
              }}
              responseOfficer={false}
            />
          )}
        </Border>
        <Border animate="viewDriver">
          <Heading title={language.DOCUMENT_DETAILS} />
          {driver.profileVerification.map((doc) => (
            <Documents
              title={doc.documentName}
              imageClick={(docImage) => setImagModel(docImage)}
              showVerifyBtn={false}
              verified={doc.status === U.VERIFIED}
              loading={verifyLoading === doc.documentName}
              verifyClick={() => verify(doc._id, doc.documentName, doc.status, doc.expiryDate)}
              expireDate={
                doc.expiryDate && doc.expiryDate !== "" ? format(new Date(doc.expiryDate), "do MMM yyyy") : null
              }
              image={imageUrl(doc.documents[0])}
            />
          ))}
        </Border>
      </Section>
    </FormWrapper>
  );
}
