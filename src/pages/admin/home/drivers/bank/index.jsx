import React from "react";
import gsap from "gsap/gsap-core";
import { useParams } from "react-router-dom";
import axios from "axios";
import * as yup from "yup";

import NavLinks from "../../../../../utils/navLinks.json";
import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";

import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { Section } from "../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../components/common/TextField";
import useLanguage from "../../../../../hooks/useLanguage";
import { useFormik } from "formik";
import useAdmin from "../../../../../hooks/useAdmin";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import { PopUp } from "../../../../../components/common/PopUp";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { id } = useParams();
  const { header, authFailure } = useAdmin();
  const { parseError, morph } = useUtils();
  const [popup, setPop] = React.useState(null);
  document.title = language.BANK_DETAILS;
  const [loading, setLoading] = React.useState(true);
  const [btnLoading, setBtnLoading] = React.useState(false);

  const formik = useFormik({
    initialValues: {
      acc_name: "",
      bank_name: "",
      acc_no: "",
      routing_number: "",
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      acc_name: yup.string().required(language.REQUIRED),
      bank_name: yup.string().required(language.REQUIRED),
      acc_no: yup.string().required(language.REQUIRED),
      routing_number: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        await axios.post(
          A.HOST + A.ADMIN_DRIVERS_BANK_SAVE,
          {
            id,
            data: {
              accountName: formik.values.acc_name,
              accountNumber: formik.values.acc_no,
              bankName: formik.values.bank_name,
              routingNumber: formik.values.routing_number,
            },
          },
          header
        );
        history.goBack();
      } catch (err) {
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
      setBtnLoading(false);
    },
  });

  React.useEffect(() => {
    const fetchBankDetails = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_DRIVERS_READ, { id }, header);
        formik.setFieldValue("acc_name", data.bankDetails.accountName);
        formik.setFieldValue("bank_name", data.bankDetails.bankName);
        formik.setFieldValue("acc_no", data.bankDetails.accountNumber);
        formik.setFieldValue("routing_number", data.bankDetails.routingNumber);
        formik.setFieldValue("name", data.firstName + " " + data.lastName);
        setLoading(false);
        gsap.fromTo(".bank_details", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
      } catch (err) {
        history.goBack();
      }
    };
    fetchBankDetails();
  }, []);
  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      width="3/4"
      animate={"bank_details"}
      submit={formik.handleSubmit}
      btnLoading={btnLoading}
      bread={[
        { id: 1, title: language.DRIVERS, path: NavLinks.ADMIN_DRIVERS_LIST },
        { id: 2, title: morph(formik.values.name), path: NavLinks.ADMIN_DRIVERS_VIEW + "/" + id },
      ]}
      title={language.BANK_DETAILS}
      submitBtn={true}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}

      <Section>
        <FieldWrapper animate={"bank_details"} title={language.ACC_NAME}>
          <TextField
            error={formik.errors.acc_name}
            change={(e) => formik.setFieldValue("acc_name", e)}
            value={formik.values.acc_name}
          />
        </FieldWrapper>
        <FieldWrapper animate={"bank_details"} title={language.BANK_NAME}>
          <TextField
            error={formik.errors.bank_name}
            change={(e) => formik.setFieldValue("bank_name", e)}
            value={formik.values.bank_name}
          />
        </FieldWrapper>
      </Section>
      <Section>
        <FieldWrapper animate={"bank_details"} title={language.ACC_NO}>
          <TextField
            error={formik.errors.acc_no}
            change={(e) => formik.setFieldValue("acc_no", e)}
            value={formik.values.acc_no}
          />
        </FieldWrapper>
        <FieldWrapper animate={"bank_details"} title={language.ROUTING_NUMBER}>
          <TextField
            error={formik.errors.routing_number}
            change={(e) => formik.setFieldValue("routing_number", e)}
            value={formik.values.routing_number}
          />
        </FieldWrapper>
      </Section>
    </FormWrapper>
  );
}
