import React from "react";
import * as yup from "yup";
import { useFormik } from "formik";
import gsap from "gsap/gsap-core";
import { FiPhone } from "react-icons/fi";
import axios from "axios";
import { parse, format } from "date-fns";

import NavLinks from "../../../../../utils/navLinks.json";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";

import useLanguage from "../../../../../hooks/useLanguage";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";
import useImage from "../../../../../hooks/useImage";

import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { Section } from "../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../components/common/TextField";
import { CountryCodesPicker } from "../../../../../components/common/CountryCodesPicker";
import { DropdownNormal } from "../../../../../components/common/DropDownNormal";
import { FileUpload } from "../../../../../components/common/FileUpload";
import { TextFormat } from "../../../../../components/common/TextFormat";
import { Heading } from "../../../../../components/common/Heading";
import { Border } from "../../../../../components/common/Border";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import { PopUp } from "../../../../../components/common/PopUp";
import { useParams } from "react-router";
import useSettings from "../../../../../hooks/useSettings";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, token, authFailure } = useAdmin();
  const { hasAlphabet, parseError } = useUtils();
  const { settings } = useSettings();
  const { id } = useParams();
  const { imageUrl, isBase64 } = useImage();

  const [page, setPage] = React.useState(1);
  const [loading, setLoading] = React.useState(true);
  const [formik2Values, setFormik2Values] = React.useState({});

  const driverId = id;

  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);

  // LISTS
  const [docs, setDocs] = React.useState(false);

  const formik1 = useFormik({
    initialValues: {
      first_name: "",
      last_name: "",
      email: "",
      phone_code: "",
      phone_number: "",
      dob: "",
      gender: "",
      line1: "",
      city: "",
      state: "",
      country: "",
      zipcode: "",
      avatar: "",
    },
    validationSchema: yup.object().shape({
      first_name: yup.string().required(language.REQUIRED),
      last_name: yup.string().required(language.REQUIRED),
      email: yup.string().required(language.REQUIRED).email(language.INVALID_EMAIL),
      phone_code: yup.string().required(language.REQUIRED),
      phone_number: yup
        .string()
        .min(6, language.MIN + " 6")
        .max(15, language.MAX + " 15")
        .required(language.REQUIRED),
      dob: yup.string().required(language.REQUIRED),
      gender: yup.string().required(language.REQUIRED),
      line1: yup.string().required(language.REQUIRED),
      city: yup.string().required(language.REQUIRED),
      state: yup.string().required(language.REQUIRED),
      country: yup.string().required(language.REQUIRED),
      zipcode: yup.string().required(language.REQUIRED),
      avatar: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async () => {
      setBtnLoading(true);
      try {
        let imageFinal;

        if (isBase64(formik1.values.avatar)) {
          const img = await fetch(formik1.values.avatar);
          imageFinal = await img.blob();
        } else imageFinal = formik1.values.avatar;

        const formData = new FormData();
        formData.append("professionalId", driverId);
        formData.append("phoneCode", formik1.values.phone_code);
        formData.append("phoneNumber", formik1.values.phone_number);
        formData.append("firstName", formik1.values.first_name);
        formData.append("lastName", formik1.values.last_name);
        formData.append("email", formik1.values.email);
        formData.append("gender", formik1.values.gender);
        formData.append("avatar", imageFinal);
        formData.append("dob", formik1.values.dob);
        formData.append("line1", formik1.values.line1);
        formData.append("city", formik1.values.city);
        formData.append("state", formik1.values.state);
        formData.append("country", formik1.values.country);
        formData.append("zipcode", formik1.values.zipcode);

        await axios.post(A.HOST + A.ADMIN_DRIVERS_ADD_STEP_1, formData, {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/form-data",
          },
        });
        setPage(2);
      } catch (err) {
        authFailure(err);
        if (err.response.data.message === "AGE MUST BE 18 AND ABOVE TO PROCEED") {
          setPop({ title: language.DRIVER_MIN_AGE + " " + settings.driverMinAge, type: "error" });
        } else setPop({ title: parseError(err), type: "error" });
      }
      setBtnLoading(false);
    },
  });

  const formik2 = useFormik({
    initialValues: formik2Values,
    enableReinitialize: true,
    onSubmit: async (e) => {
      setBtnLoading(true);
      const ifLoading = Object.keys(formik2.values).filter(
        (key) => formik2.values[key].uploading === language.UPLOADING
      ).length;
      if (ifLoading) {
        setPop({ title: language.DOCS_UPLOADING, type: "error" });
      } else {
        const mandatory_list = Object.keys(formik2.values)
          .filter((key) => formik2.values[key].status === U.ACTIVE)
          .filter((key) => formik2.values[key].mandatory === true).length;
        const uploaded_images = Object.keys(formik2.values)
          .filter((key) => formik2.values[key].status === U.ACTIVE)
          .filter(
            (key) => formik2.values[key].uploading === language.UPLOADED && formik2.values[key].mandatory === true
          ).length;
        if (uploaded_images >= mandatory_list) {
          history.goBack();
        } else setPop({ title: language.UPLOAD_MANDATORY, type: "error" });
      }
      setBtnLoading(false);
    },
  });

  React.useEffect(() => {
    const fetchEverything = async () => {
      const fetchDriver = async () => {
        try {
          const { data } = await axios.post(
            A.HOST + A.ADMIN_DRIVERS_READ,
            {
              id: driverId,
            },
            header
          );
          formik1.setFieldValue("first_name", data.firstName);
          formik1.setFieldValue("last_name", data.lastName);
          formik1.setFieldValue("email", data.email);
          formik1.setFieldValue("phone_code", data.phone.code);
          formik1.setFieldValue("phone_number", data.phone.number);
          formik1.setFieldValue("dob", data.dob);
          formik1.setFieldValue("gender", data.gender);
          formik1.setFieldValue("line1", data.address.line);
          formik1.setFieldValue("city", data.address.city);
          formik1.setFieldValue("state", data.address.state);
          formik1.setFieldValue("country", data.address.country);
          formik1.setFieldValue("zipcode", data.address.zipcode);
          formik1.setFieldValue("avatar", data.avatar);
          data.profileVerification.map((each) => {
            formik2.setFieldValue(
              `${each.documentName}.expiry`,
              each.expiryDate && each.expiryDate !== "" ? format(new Date(each.expiryDate), "MM-dd-yyyy") : ""
            );
            formik2.setFieldValue(`${each.documentName}.doc_id`, each._id);
            formik2.setFieldValue(`${each.documentName}.image`, each.documents[0]);
            formik2.setFieldValue(`${each.documentName}.uploading`, language.UPLOADED);
          });
          setLoading(false);
          gsap.fromTo(".addDriver", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
        } catch (err) {
          authFailure(err);
          history.goBack();
        }
      };
      const fetchDocs = async () => {
        try {
          const { data } = await axios.post(A.HOST + A.ADMIN_DRIVERS_DOC_LIST, {}, header);
          setDocs(data.data);
          let forForm2 = {};
          data.data.PROFILEDOCUMENTS.forEach((doc) => {
            forForm2[doc.docsName] = {
              doc_id: null,
              doc_name: doc.docsName,
              expiry: "",
              expiry_mandatory: doc.docsExpiry,
              mandatory: doc.docsMandatory,
              uploading: false,
              image: null,
              status: doc.status,
            };
          });
          setFormik2Values(forForm2);
        } catch (err) {
          history.goBack();
        }
      };
      await fetchDocs();
      fetchDriver();
    };
    fetchEverything();
  }, []);

  // React.useEffect(() => {
  //   console.log(formik2.values);
  // }, [formik2.values]);

  const driverDocUpload = async (docName, image) => {
    formik2.setFieldValue(`${docName}.image`, image);
    formik2.setFieldValue(`${docName}.uploading`, language.UPLOADING);
    try {
      const formData = new FormData();

      const driverDocument = {
        professionalId: driverId,
        documentName: formik2.values[docName].doc_name,
        expiryDate: "",
      };

      if (formik2.values[docName].expiry_mandatory) {
        driverDocument.expiryDate = new Date(parse(formik2.values[docName].expiry, "MM-dd-yyyy", new Date()));
      }

      const img = await fetch(image);
      const imageFinal = await img.blob();

      driverDocument[`${docName}[0]`] = imageFinal;
      for (let key in driverDocument) {
        formData.append(key, driverDocument[key]);
      }
      await axios.post(A.HOST + A.ADMIN_DRIVERS_PROFILE_DOC_UPLOAD, formData, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/form-data",
        },
      });
      formik2.setFieldValue(`${docName}.uploading`, language.UPLOADED);
      formik2.setFieldValue(`${docName}.doc_id`, "");
    } catch (err) {
      setPop({ title: parseError(err), type: "error" });
      formik2.setFieldValue(`${docName}.uploading`, null);
    }
  };

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      title={language.EDIT}
      bread={[{ id: 1, title: language.DRIVERS, path: NavLinks.ADMIN_DRIVERS_LIST }]}
      width="full"
      showNext={page === 1}
      nextClick={page === 1 ? formik1.handleSubmit : null}
      btnLoading={btnLoading}
      page={page}
      totalPage={2}
      submitBtn={page === 2}
      submit={formik2.handleSubmit}
      animate={"addDriver"}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      {page === 1 && (
        <>
          <Section>
            <FieldWrapper title={language.FULLNAME}>
              {/* First Name  */}
              <TextField
                value={formik1.values.first_name}
                width="2/4"
                type="text"
                margin={3}
                error={formik1.errors.first_name}
                placeholder={language.FIRSTNAME}
                change={(e) => formik1.setFieldValue("first_name", e.trim(), true)}
              />
              {/* Last Name  */}
              <TextField
                value={formik1.values.last_name}
                width="2/4"
                type="text"
                placeholder={language.LASTNAME}
                error={formik1.errors.last_name}
                change={(e) => formik1.setFieldValue("last_name", e.trim(), true)}
              />
            </FieldWrapper>
            <FieldWrapper title={language.EMAIL}>
              <TextField
                placeholder={language.EMAIL}
                change={(e) => formik1.setFieldValue("email", e)}
                error={formik1.errors.email}
                value={formik1.values.email}
              />
            </FieldWrapper>
            <FieldWrapper title={language.MOBILE}>
              <CountryCodesPicker
                placeholder={"code"}
                defaultValue={formik1.values.phone_code}
                error={formik1.errors.phone_code}
                margin={3}
                change={(e) => formik1.setFieldValue("phone_code", e, true)}
                width="4/12"
              />
              <TextField
                width="8/12"
                type="number"
                value={formik1.values.phone_number}
                placeholder="number"
                icon={<FiPhone />}
                error={formik1.errors.phone_number}
                change={(e) => formik1.setFieldValue("phone_number", e, true)}
              />
            </FieldWrapper>
            <FieldWrapper title={language.DATE_OF_BIRTH}>
              <TextFormat
                format={"##-##-####"}
                value={formik1.values.dob}
                error={formik1.errors.dob}
                change={(e) => formik1.setFieldValue("dob", e)}
                placeholder="MM-DD-YYYY"
                mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
              />
            </FieldWrapper>
            <FieldWrapper title={language.GENDER}>
              <DropdownNormal
                defaultValue={language[formik1.values.gender]}
                fields={[
                  {
                    id: 1,
                    label: language.FEMALE,
                    value: "FEMALE",
                  },
                  {
                    id: 2,
                    label: language.MALE,
                    value: "MALE",
                  },
                  {
                    id: 3,
                    label: language.OTHER,
                    value: "OTHER",
                  },
                ]}
                error={formik1.errors.gender}
                change={(e) => formik1.setFieldValue("gender", e, true)}
              />
            </FieldWrapper>
          </Section>
          <Section>
            <FieldWrapper title={language.STREET}>
              <TextField
                change={(e) => formik1.setFieldValue("line1", e)}
                value={formik1.values.line1}
                error={formik1.errors.line1}
              />
            </FieldWrapper>
            <FieldWrapper title={language.CITY}>
              <TextField
                change={(e) => formik1.setFieldValue("city", e)}
                value={formik1.values.city}
                error={formik1.errors.city}
              />
            </FieldWrapper>
            <FieldWrapper title={language.STATE}>
              <TextField
                change={(e) => formik1.setFieldValue("state", e)}
                value={formik1.values.state}
                error={formik1.errors.state}
              />
            </FieldWrapper>
            <FieldWrapper title={language.COUNTRY}>
              <TextField
                change={(e) => formik1.setFieldValue("country", e)}
                value={formik1.values.country}
                error={formik1.errors.country}
              />
            </FieldWrapper>
            <FieldWrapper title={language.ZIPCODE}>
              <TextField
                change={(e) => formik1.setFieldValue("zipcode", e)}
                value={formik1.values.zipcode}
                error={formik1.errors.zipcode}
              />
            </FieldWrapper>{" "}
          </Section>
          <Section>
            <FieldWrapper title={language.AVATAR}>
              <FileUpload
                crop={true}
                ratio={1 / 1}
                defaultValue={imageUrl(formik1.values.avatar)}
                change={(e) => formik1.setFieldValue("avatar", e)}
              />
            </FieldWrapper>
          </Section>
        </>
      )}
      {page === 2 && (
        <>
          <Section>
            {docs.PROFILEDOCUMENTS.filter((doc) => doc.status === U.ACTIVE)
              .filter((doc, index) => index % 3 === 0)
              .map((doc) => (
                <>
                  <Border>
                    <Heading title={doc.docsMandatory === true ? `${doc.docsName} *` : `${doc.docsName}`} />

                    {doc.docsExpiry && (
                      <FieldWrapper title={language.EXPIRY_DATE}>
                        <TextFormat
                          format={"##-##-####"}
                          value={formik2.values[doc.docsName].expiry}
                          error={formik1.errors.dob}
                          change={(e) => formik2.setFieldValue(`${doc.docsName}.expiry`, e)}
                          placeholder="MM-DD-YYYY"
                          mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
                        />
                      </FieldWrapper>
                    )}
                    {formik2.values[doc.docsName].expiry_mandatory === false && (
                      <FieldWrapper title={language.IMAGE}>
                        <FileUpload
                          defaultValue={imageUrl(formik2.values[doc.docsName].image)}
                          change={(e) => driverDocUpload(`${doc.docsName}`, e)}
                          crop={false}
                        />
                      </FieldWrapper>
                    )}
                    {formik2.values[doc.docsName].expiry_mandatory === true &&
                      formik2.values[doc.docsName].expiry !== "" &&
                      hasAlphabet(formik2.values[doc.docsName].expiry) === false && (
                        <FieldWrapper title={language.IMAGE}>
                          <FileUpload
                            defaultValue={imageUrl(formik2.values[doc.docsName].image)}
                            change={(e) => driverDocUpload(`${doc.docsName}`, e)}
                            crop={false}
                          />
                        </FieldWrapper>
                      )}
                    <FieldWrapper>
                      <div className="flex w-full justify-between items-center">
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {doc.docsDetail}
                        </p>
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {formik2.values[doc.docsName].uploading === false
                            ? ""
                            : formik2.values[doc.docsName].uploading}
                        </p>
                      </div>
                    </FieldWrapper>
                  </Border>
                </>
              ))}
          </Section>
          <Section>
            {docs.PROFILEDOCUMENTS.filter((doc) => doc.status === U.ACTIVE)
              .filter((doc, index) => index % 3 === 1)
              .map((doc) => (
                <>
                  <Border>
                    <Heading title={doc.docsMandatory === true ? `${doc.docsName} *` : `${doc.docsName}`} />

                    {doc.docsExpiry && (
                      <FieldWrapper title={language.EXPIRY_DATE}>
                        <TextFormat
                          format={"##-##-####"}
                          value={formik2.values[doc.docsName].expiry}
                          error={formik1.errors.dob}
                          change={(e) => formik2.setFieldValue(`${doc.docsName}.expiry`, e)}
                          placeholder="MM-DD-YYYY"
                          mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
                        />
                      </FieldWrapper>
                    )}
                    {formik2.values[doc.docsName].expiry_mandatory === false && (
                      <FieldWrapper title={language.IMAGE}>
                        <FileUpload
                          defaultValue={imageUrl(formik2.values[doc.docsName].image)}
                          change={(e) => driverDocUpload(`${doc.docsName}`, e)}
                          crop={false}
                        />
                      </FieldWrapper>
                    )}
                    {formik2.values[doc.docsName].expiry_mandatory === true &&
                      formik2.values[doc.docsName].expiry !== "" &&
                      hasAlphabet(formik2.values[doc.docsName].expiry) === false && (
                        <FieldWrapper title={language.IMAGE}>
                          <FileUpload
                            defaultValue={imageUrl(formik2.values[doc.docsName].image)}
                            change={(e) => driverDocUpload(`${doc.docsName}`, e)}
                            crop={false}
                          />
                        </FieldWrapper>
                      )}
                    <FieldWrapper>
                      <div className="flex w-full justify-between items-center">
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {doc.docsDetail}
                        </p>
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {formik2.values[doc.docsName].uploading}
                        </p>
                      </div>
                    </FieldWrapper>
                  </Border>
                </>
              ))}
          </Section>
          <Section>
            {docs.PROFILEDOCUMENTS.filter((doc) => doc.status === U.ACTIVE)
              .filter((doc, index) => index % 3 === 2)
              .map((doc) => (
                <>
                  <Border>
                    <Heading title={doc.docsMandatory === true ? `${doc.docsName} *` : `${doc.docsName}`} />

                    {doc.docsExpiry && (
                      <FieldWrapper title={language.EXPIRY_DATE}>
                        <TextFormat
                          format={"##-##-####"}
                          value={formik2.values[doc.docsName].expiry}
                          error={formik1.errors.dob}
                          change={(e) => formik2.setFieldValue(`${doc.docsName}.expiry`, e)}
                          placeholder="MM-DD-YYYY"
                          mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
                        />
                      </FieldWrapper>
                    )}
                    {formik2.values[doc.docsName].expiry_mandatory === false && (
                      <FieldWrapper title={language.IMAGE}>
                        <FileUpload
                          defaultValue={imageUrl(formik2.values[doc.docsName].image)}
                          change={(e) => driverDocUpload(`${doc.docsName}`, e)}
                          crop={false}
                        />
                      </FieldWrapper>
                    )}
                    {formik2.values[doc.docsName].expiry_mandatory === true &&
                      formik2.values[doc.docsName].expiry !== "" &&
                      hasAlphabet(formik2.values[doc.docsName].expiry) === false && (
                        <FieldWrapper title={language.IMAGE}>
                          <FileUpload
                            defaultValue={imageUrl(formik2.values[doc.docsName].image)}
                            change={(e) => driverDocUpload(`${doc.docsName}`, e)}
                            crop={false}
                          />
                        </FieldWrapper>
                      )}
                    <FieldWrapper>
                      <div className="flex w-full justify-between items-center">
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {doc.docsDetail}
                        </p>
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {formik2.values[doc.docsName].uploading}
                        </p>
                      </div>
                    </FieldWrapper>
                  </Border>
                </>
              ))}
          </Section>
        </>
      )}
    </FormWrapper>
  );
}
