import React from "react";
import * as yup from "yup";
import { useFormik } from "formik";
import gsap from "gsap/gsap-core";
import { FiLoader, FiPhone } from "react-icons/fi";
import axios from "axios";
import { parse } from "date-fns";

import NavLinks from "../../../../../utils/navLinks.json";
import CountryCodes from "../../../../../utils/countryCodes.json";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";

import useLanguage from "../../../../../hooks/useLanguage";
import useDebug from "../../../../../hooks/useDebug";

import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { ToggleButton } from "../../../../../components/common/ToggleButton";
import { Section } from "../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../components/common/TextField";
import Flex from "../../../../../components/common/Flex";
import { CountryCodesPicker } from "../../../../../components/common/CountryCodesPicker";
import { DatePicker } from "../../../../../components/common/DatePicker";
import { DropdownNormal } from "../../../../../components/common/DropDownNormal";
import { DropDownSearch } from "../../../../../components/common/DropDownSearch";
import { FileUpload } from "../../../../../components/common/FileUpload";
import { TextFormat } from "../../../../../components/common/TextFormat";
import { Heading } from "../../../../../components/common/Heading";
import { Border } from "../../../../../components/common/Border";
import { Line } from "../../../../../components/common/Line";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";
import { PopUp } from "../../../../../components/common/PopUp";
import useSettings from "../../../../../hooks/useSettings";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, token, authFailure } = useAdmin();
  const { hasAlphabet, parseError } = useUtils();
  const { settings } = useSettings();

  const [page, setPage] = React.useState(1);
  const [loading, setLoading] = React.useState(true);
  const [loadingHubs, setLoadingHubs] = React.useState(false);
  const [formik2Values, setFormik2Values] = React.useState({});
  const [formik4Values, setFormik4Values] = React.useState({});

  const [driverId, setDriverId] = React.useState(null);
  const [vehicleId, setVehicleId] = React.useState(null);

  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);

  // LISTS
  const [docs, setDocs] = React.useState(false);
  const [cities, setCities] = React.useState([]);
  const [hubs, setHubs] = React.useState([]);
  const [vehicleCategory, setVehicleCategory] = React.useState([]);

  const formik1 = useFormik({
    initialValues: {
      first_name: "",
      last_name: "",
      email: "",
      phone_code: "",
      phone_number: "",
      dob: "",
      gender: "",
      line1: "",
      city: "",
      state: "",
      country: "",
      zipcode: "",
      avatar: "",
    },
    validationSchema: yup.object().shape({
      first_name: yup.string().required(language.REQUIRED),
      last_name: yup.string().required(language.REQUIRED),
      email: yup.string().required(language.REQUIRED).email(language.INVALID_EMAIL),
      phone_code: yup.string().required(language.REQUIRED),
      phone_number: yup
        .string()
        .min(6, language.MIN + " 6")
        .max(15, language.MAX + " 15")
        .required(language.REQUIRED),
      dob: yup.string().required(language.REQUIRED),
      gender: yup.string().required(language.REQUIRED),
      line1: yup.string().required(language.REQUIRED),
      city: yup.string().required(language.REQUIRED),
      state: yup.string().required(language.REQUIRED),
      country: yup.string().required(language.REQUIRED),
      zipcode: yup.string().required(language.REQUIRED),
      avatar: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async () => {
      setBtnLoading(true);
      try {
        const img = await fetch(formik1.values.avatar);
        const imageFinal = await img.blob();
        const formData = new FormData();
        formData.append("phoneCode", formik1.values.phone_code);
        formData.append("phoneNumber", formik1.values.phone_number);
        formData.append("firstName", formik1.values.first_name);
        formData.append("lastName", formik1.values.last_name);
        formData.append("email", formik1.values.email);
        formData.append("gender", formik1.values.gender);
        formData.append("avatar", imageFinal);
        formData.append("dob", formik1.values.dob);
        formData.append("line1", formik1.values.line1);
        formData.append("city", formik1.values.city);
        formData.append("state", formik1.values.state);
        formData.append("country", formik1.values.country);
        formData.append("zipcode", formik1.values.zipcode);
        const { data } = await axios.post(A.HOST + A.ADMIN_DRIVERS_ADD_STEP_1, formData, {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/form-data",
          },
        });
        setDriverId(data.data._id);
        setPage(2);
      } catch (err) {
        authFailure(err);
        if (err.response.data.message === "AGE MUST BE 18 AND ABOVE TO PROCEED") {
          setPop({ title: language.DRIVER_MIN_AGE + " " + settings.driverMinAge, type: "error" });
        } else setPop({ title: parseError(err), type: "error" });
      }
      setBtnLoading(false);
    },
  });

  const formik2 = useFormik({
    initialValues: formik2Values,
    enableReinitialize: true,
    onSubmit: (e) => {
      const ifLoading = Object.keys(formik2.values).filter(
        (key) => formik2.values[key].uploading === language.UPLOADING
      ).length;
      if (ifLoading) {
        setPop({ title: language.DOCS_UPLOADING, type: "error" });
      } else {
        const mandatory_list = Object.keys(formik2.values)
          .filter((key) => formik2.values[key].status === U.ACTIVE)
          .filter((key) => formik2.values[key].mandatory === true).length;
        const uploaded_images = Object.keys(formik2.values)
          .filter((key) => formik2.values[key].status === U.ACTIVE)
          .filter(
            (key) => formik2.values[key].uploading === language.UPLOADED && formik2.values[key].mandatory === true
          ).length;
        if (uploaded_images >= mandatory_list) setPage(3);
        else setPop({ title: language.UPLOAD_MANDATORY, type: "error" });
      }
    },
  });

  const formik3 = useFormik({
    initialValues: {
      vehicleCategory: "",
      defaultVehicle: 1,
      city: "",
      hubs: "",
      vinNumber: "",
      plateNumber: "",
      type: "PETROL",
      maker: "",
      model: "",
      year: "",
      color: "",
      noOfDoors: "",
      noOfSeats: "",
      imageFront: "",
      imageBack: "",
      imageLeft: "",
      imageRight: "",
      handicap: 0,
      childSeat: 0,
      companyVehicle: 0,
    },
    validateOnChange: true,
    validationSchema: yup.object().shape({
      vehicleCategory: yup.string().required(language.REQUIRED),
      city: yup.string().required(language.REQUIRED),
      hubs: yup.string().required(language.REQUIRED),
      vinNumber: yup.string().required(language.REQUIRED),
      plateNumber: yup.string().required(language.REQUIRED),
      type: yup.string().required(language.REQUIRED),
      maker: yup.string().required(language.REQUIRED),
      model: yup.string().required(language.REQUIRED),
      year: yup
        .number()
        .min(999, language.NOT_A_VALID_YEAR)
        .max(9999, language.NOT_A_VALID_YEAR)
        .required(language.REQUIRED),
      color: yup.string().required(language.REQUIRED),
      noOfDoors: yup
        .number()
        .min(1, language.MIN + " 1")
        .max(8, language.MAX + " 8")
        .required(language.REQUIRED),
      noOfSeats: yup
        .number()
        .min(1, language.MIN + " 1")
        .max(8, language.MAX + " 8")
        .required(language.REQUIRED),
      imageFront: yup.string().required(language.REQUIRED),
      imageBack: yup.string().required(language.REQUIRED),
      imageLeft: yup.string().required(language.REQUIRED),
      imageRight: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        const formData = new FormData();
        formData.append("professionalId", driverId);
        formData.append("vehicleCategoryId", formik3.values.vehicleCategory);
        formData.append("serviceCategory", cities.filter((each) => each.locationName)[0]._id);
        formData.append("vinNumber", formik3.values.vinNumber);
        formData.append("plateNumber", formik3.values.plateNumber);
        formData.append("type", formik3.values.type);
        formData.append("maker", formik3.values.maker);
        formData.append("model", formik3.values.model);
        formData.append("year", formik3.values.year);
        formData.append("color", formik3.values.color);
        formData.append("defaultVehicle", true);
        formData.append("noOfDoors", formik3.values.noOfDoors);
        formData.append("noOfSeats", formik3.values.noOfSeats);
        formData.append("hub", formik3.values.hubs);
        formData.append("scheduleDate", new Date());
        formData.append("status", U.UNVERIFIED);
        formData.append("isCompanyVehicle", formik3.values.companyVehicle === 0 ? false : true);
        formData.append("childseatAvailable", formik3.values.childSeat === 0 ? false : true);
        formData.append("handicapAvailable", formik3.values.handicap === 0 ? false : true);

        let front = await fetch(formik3.values.imageFront);
        front = await front.blob();

        let back = await fetch(formik3.values.imageBack);
        back = await back.blob();

        let left = await fetch(formik3.values.imageLeft);
        left = await left.blob();

        let right = await fetch(formik3.values.imageRight);
        right = await right.blob();

        formData.append("frontImage", front);
        formData.append("backImage", back);
        formData.append("leftImage", left);
        formData.append("rightImage", right);

        const { data } = await axios.post(A.HOST + A.ADMIN_DRIVERS_ADD_STEP_3, formData, {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/form-data",
          },
        });
        setVehicleId(data.data._id);
        setPage(4);
      } catch (err) {
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
      setBtnLoading(false);
    },
  });

  const formik4 = useFormik({
    initialValues: formik4Values,
    enableReinitialize: true,
    onSubmit: async (e) => {
      setBtnLoading(true);
      const ifLoading = Object.keys(formik4.values).filter(
        (key) => formik4.values[key].uploading === language.UPLOADING
      ).length;
      if (ifLoading) {
        setPop({ title: language.DOCS_UPLOADING, type: "error" });
      } else {
        const mandatory_list = Object.keys(formik4.values)
          .filter((key) => formik4.values[key].status === U.ACTIVE)
          .filter((key) => formik4.values[key].mandatory === true).length;
        const uploaded_images = Object.keys(formik4.values)
          .filter((key) => formik4.values[key].status === U.ACTIVE)
          .filter(
            (key) => formik4.values[key].uploading === language.UPLOADED && formik4.values[key].mandatory === true
          ).length;
        if (uploaded_images >= mandatory_list) {
          try {
            await axios.post(
              A.HOST + A.ADMIN_DRIVERS_VERIFICATION,
              {
                professionalId: driverId,
              },
              header
            );
            history.goBack();
          } catch (err) {}
        } else setPop({ title: language.UPLOAD_MANDATORY, type: "error" });
      }
      setBtnLoading(false);
    },
  });

  React.useEffect(() => {
    const fetchDocs = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_DRIVERS_DOC_LIST, {}, header);
        setDocs(data.data);
        let forForm2 = {};
        data.data.PROFILEDOCUMENTS.forEach((doc) => {
          forForm2[doc.docsName] = {
            doc_id: null,
            doc_name: doc.docsName,
            expiry: "",
            expiry_mandatory: doc.docsExpiry,
            mandatory: doc.docsMandatory,
            uploading: false,
            image: null,
            status: doc.status,
          };
        });
        setFormik2Values(forForm2);
        let forForm4 = {};
        data.data.DRIVERDOCUMENTS.forEach((doc) => {
          forForm4[doc.docsName] = {
            doc_id: null,
            doc_name: doc.docsName,
            expiry: "",
            expiry_mandatory: doc.docsExpiry,
            mandatory: doc.docsMandatory,
            uploading: false,
            image: null,
            status: doc.status,
          };
        });
        setFormik4Values(forForm4);
        setLoading(false);
        gsap.fromTo(".addDriver", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
      } catch (err) {
        alert(err);
        history.goBack();
      }
    };

    const fetchCities = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_CITY_POLYGON_LIST, { categoryName: "ride" }, header);
        setCities(data);
      } catch (err) {}
    };

    fetchDocs();
    fetchCities();
  }, []);

  React.useEffect(() => {
    if (formik3.values.city !== "") {
      setLoadingHubs(true);
      const fetchHubs = async () => {
        try {
          const { data } = await axios.post(
            A.HOST + A.ADMIN_HUB_READ_FROM_CITY,
            {
              serviceArea: cities.filter((each) => each.locationName === formik3.values.city)[0]._id,
            },
            header
          );
          setHubs(data.data);
          if (data.data.length > 0) formik3.setFieldValue("hubs", data.data[0]._id);
          else formik3.setFieldValue("hubs", "");
        } catch (err) {
          alert(err);
        }
        setLoadingHubs(false);
      };
      fetchHubs();
    }
  }, [formik3.values.city]);

  const fetchVehicleCategory = async () => {
    try {
      setLoadingHubs(true);
      const { data } = await axios.post(
        A.HOST + A.ADMIN_VEHCILE_CATEGORY_BASED_ON_CITY,
        {
          city:
            cities.filter((each) => each.locationName === formik3.values.city).length > 0
              ? cities.filter((each) => each.locationName === formik3.values.city)[0]._id
              : cities.filter((each) => each._id === formik3.values.city).length > 0
              ? formik3.values.city
              : "",
        },
        header
      );
      setVehicleCategory(data);
      formik3.setFieldValue("vehicleCategory", data[0]._id);
      setLoadingHubs(false);
    } catch (err) {
      alert(err);
    }
  };

  React.useEffect(() => {
    formik3.values.city !== "" && fetchVehicleCategory();
  }, [formik3.values.city]);

  const driverDocUpload = async (docName, image) => {
    formik2.setFieldValue(`${docName}.image`, image);
    formik2.setFieldValue(`${docName}.uploading`, language.UPLOADING);
    try {
      const formData = new FormData();

      const driverDocument = {
        professionalId: driverId,
        documentName: formik2.values[docName].doc_name,
        expiryDate: "",
      };

      if (formik2.values[docName].expiry_mandatory) {
        driverDocument.expiryDate = new Date(parse(formik2.values[docName].expiry, "MM-dd-yyyy", new Date()));
      }

      const img = await fetch(image);
      const imageFinal = await img.blob();

      driverDocument[`${docName}[0]`] = imageFinal;
      for (let key in driverDocument) {
        formData.append(key, driverDocument[key]);
      }
      const { data } = await axios.post(A.HOST + A.ADMIN_DRIVERS_PROFILE_DOC_UPLOAD, formData, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/form-data",
        },
      });
      formik2.setFieldValue(`${docName}.doc_id`, data.data._id);
      formik2.setFieldValue(`${docName}.uploading`, language.UPLOADED);
    } catch (err) {
      setPop({ title: parseError(err), type: "error" });
      formik2.setFieldValue(`${docName}.uploading`, null);
    }
  };

  const vehicleDocUpload = async (docName, image) => {
    formik4.setFieldValue(`${docName}.image`, image);
    formik4.setFieldValue(`${docName}.uploading`, language.UPLOADING);
    try {
      const formData = new FormData();

      const driverDocument = {
        professionalId: driverId,
        documentName: formik4.values[docName].doc_name,
        vehicleId: vehicleId,
        expiryDate: "",
      };

      if (formik4.values[docName].expiry_mandatory) {
        driverDocument.expiryDate = new Date(parse(formik4.values[docName].expiry, "MM-dd-yyyy", new Date()));
      }

      const img = await fetch(image);
      const imageFinal = await img.blob();

      driverDocument[`${docName}[0]`] = imageFinal;
      for (let key in driverDocument) {
        formData.append(key, driverDocument[key]);
      }
      const { data } = await axios.post(A.HOST + A.ADMIN_DRIVERS_ADD_STEP_4, formData, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/form-data",
        },
      });
      formik4.setFieldValue(`${docName}.uploading`, language.UPLOADED);
      formik4.setFieldValue(`${docName}.doc_id`, data.data._id);
    } catch (err) {
      alert(err);
      setPop({ title: parseError(err), type: "error" });
      formik4.setFieldValue(`${docName}.uploading`, null);
    }
  };

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      title={language.ADD}
      bread={[{ id: 1, title: language.DRIVERS, path: NavLinks.ADMIN_DRIVERS_LIST }]}
      width="full"
      info={language.DRIVERS_INFO}
      showNext={page === 1 || page === 2 || page === 3}
      nextClick={
        page === 1 ? formik1.handleSubmit : page === 2 ? formik2.handleSubmit : page === 3 ? formik3.handleSubmit : null
      }
      btnLoading={btnLoading}
      prevClick={() => setPage(page - 1)}
      page={page}
      totalPage={4}
      submitBtn={page === 4}
      submit={formik4.handleSubmit}
      animate={"addDriver"}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      {page === 1 && (
        <>
          <Section>
            <Heading title={language.ADD_DRIVER} />
            <FieldWrapper title={language.FULLNAME}>
              {/* First Name  */}
              <TextField
                value={formik1.values.first_name}
                width="2/4"
                type="text"
                margin={3}
                error={formik1.errors.first_name}
                placeholder={language.FIRSTNAME}
                change={(e) => formik1.setFieldValue("first_name", e.trim(), true)}
              />
              {/* Last Name  */}
              <TextField
                value={formik1.values.last_name}
                width="2/4"
                type="text"
                placeholder={language.LASTNAME}
                error={formik1.errors.last_name}
                change={(e) => formik1.setFieldValue("last_name", e.trim(), true)}
              />
            </FieldWrapper>
            <FieldWrapper title={language.EMAIL}>
              <TextField
                placeholder={language.EMAIL}
                change={(e) => formik1.setFieldValue("email", e)}
                error={formik1.errors.email}
                value={formik1.values.email}
              />
            </FieldWrapper>
            <FieldWrapper title={language.MOBILE}>
              <CountryCodesPicker
                placeholder={"code"}
                defaultValue={formik1.values.phone_code}
                error={formik1.errors.phone_code}
                margin={3}
                change={(e) => formik1.setFieldValue("phone_code", e, true)}
                width="4/12"
              />
              <TextField
                width="8/12"
                type="number"
                value={formik1.values.phone_number}
                placeholder="number"
                icon={<FiPhone />}
                error={formik1.errors.phone_number}
                change={(e) => formik1.setFieldValue("phone_number", e, true)}
              />
            </FieldWrapper>
            <FieldWrapper title={language.DATE_OF_BIRTH}>
              <TextFormat
                format={"##-##-####"}
                value={formik1.values.dob}
                error={formik1.errors.dob}
                change={(e) => formik1.setFieldValue("dob", e)}
                placeholder="MM-DD-YYYY"
                mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
              />
            </FieldWrapper>
            <FieldWrapper title={language.GENDER}>
              <DropdownNormal
                defaultValue={language[formik1.values.gender]}
                fields={[
                  {
                    id: 1,
                    label: language.FEMALE,
                    value: "FEMALE",
                  },
                  {
                    id: 2,
                    label: language.MALE,
                    value: "MALE",
                  },
                  {
                    id: 3,
                    label: language.OTHER,
                    value: "OTHER",
                  },
                ]}
                error={formik1.errors.gender}
                change={(e) => formik1.setFieldValue("gender", e, true)}
              />
            </FieldWrapper>
          </Section>
          <Section>
            {/* <FieldWrapper title={language.CITY}>
                  <DropdownNormal
                    defaultValue={
                      formik1.values.city !== "" &&
                      citites.filter((city) => city._id === formik1.values.city).length > 0
                        ? citites.filter((city) => city._id === formik1.values.city)[0].locationName
                        : ""
                    }
                    change={(e) => formik1.setFieldValue("city", e)}
                    fields={citites.map((city, index) => ({
                      id: index,Thingal Nagar, Tamil Nadu, India
                      label: city.locationName,
                      value: city._id,
                    }))}
                    error={formik1.errors.city}
                  />
                </FieldWrapper> */}
            <FieldWrapper title={language.STREET}>
              <TextField
                change={(e) => formik1.setFieldValue("line1", e)}
                value={formik1.values.line1}
                error={formik1.errors.line1}
              />
            </FieldWrapper>
            <FieldWrapper title={language.CITY}>
              <TextField
                change={(e) => formik1.setFieldValue("city", e)}
                value={formik1.values.city}
                error={formik1.errors.city}
              />
            </FieldWrapper>
            <FieldWrapper title={language.STATE}>
              <TextField
                change={(e) => formik1.setFieldValue("state", e)}
                value={formik1.values.state}
                error={formik1.errors.state}
              />
            </FieldWrapper>
            <FieldWrapper title={language.COUNTRY}>
              <TextField
                change={(e) => formik1.setFieldValue("country", e)}
                value={formik1.values.country}
                error={formik1.errors.country}
              />
            </FieldWrapper>
            <FieldWrapper title={language.ZIPCODE}>
              <TextField
                change={(e) => formik1.setFieldValue("zipcode", e)}
                value={formik1.values.zipcode}
                error={formik1.errors.zipcode}
              />
            </FieldWrapper>{" "}
          </Section>
          <Section>
            <FieldWrapper title={language.AVATAR}>
              <FileUpload crop={true} ratio={1 / 1} change={(e) => formik1.setFieldValue("avatar", e)} />
            </FieldWrapper>
          </Section>
        </>
      )}
      {page === 2 && (
        <>
          <Section>
            {docs.PROFILEDOCUMENTS.filter((doc) => doc.status === U.ACTIVE)
              .filter((doc) => doc.status === U.ACTIVE)
              .filter((doc, index) => index % 3 === 0)
              .map((doc) => (
                <>
                  <Border>
                    <Heading title={doc.docsMandatory === true ? `${doc.docsName} *` : `${doc.docsName}`} />

                    {doc.docsExpiry && (
                      <FieldWrapper title={language.EXPIRY_DATE}>
                        <TextFormat
                          format={"##-##-####"}
                          value={formik2.values[doc.docsName].expiry}
                          error={formik1.errors.dob}
                          change={(e) => formik2.setFieldValue(`${doc.docsName}.expiry`, e)}
                          placeholder="MM-DD-YYYY"
                          mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
                        />
                      </FieldWrapper>
                    )}
                    {formik2.values[doc.docsName].expiry_mandatory === false && (
                      <FieldWrapper title={language.IMAGE}>
                        <FileUpload change={(e) => driverDocUpload(`${doc.docsName}`, e)} crop={false} />
                      </FieldWrapper>
                    )}
                    {formik2.values[doc.docsName].expiry_mandatory === true &&
                      formik2.values[doc.docsName].expiry !== "" &&
                      hasAlphabet(formik2.values[doc.docsName].expiry) === false && (
                        <FieldWrapper title={language.IMAGE}>
                          <FileUpload change={(e) => driverDocUpload(`${doc.docsName}`, e)} crop={false} />
                        </FieldWrapper>
                      )}
                    <FieldWrapper>
                      <div className="flex w-full justify-between items-center">
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {doc.docsDetail}
                        </p>
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {formik2.values[doc.docsName].uploading === false
                            ? ""
                            : formik2.values[doc.docsName].uploading}
                        </p>
                      </div>
                    </FieldWrapper>
                  </Border>
                </>
              ))}
          </Section>
          <Section>
            {docs.PROFILEDOCUMENTS.filter((doc) => doc.status === U.ACTIVE)
              .filter((doc, index) => index % 3 === 1)
              .map((doc) => (
                <>
                  <Border>
                    <Heading title={doc.docsMandatory === true ? `${doc.docsName} *` : `${doc.docsName}`} />

                    {doc.docsExpiry && (
                      <FieldWrapper title={language.EXPIRY_DATE}>
                        <TextFormat
                          format={"##-##-####"}
                          value={formik2.values[doc.docsName].expiry}
                          error={formik1.errors.dob}
                          change={(e) => formik2.setFieldValue(`${doc.docsName}.expiry`, e)}
                          placeholder="MM-DD-YYYY"
                          mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
                        />
                      </FieldWrapper>
                    )}
                    {formik2.values[doc.docsName].expiry_mandatory === false && (
                      <FieldWrapper title={language.IMAGE}>
                        <FileUpload change={(e) => driverDocUpload(`${doc.docsName}`, e)} crop={false} />
                      </FieldWrapper>
                    )}
                    {formik2.values[doc.docsName].expiry_mandatory === true &&
                      formik2.values[doc.docsName].expiry !== "" &&
                      hasAlphabet(formik2.values[doc.docsName].expiry) === false && (
                        <FieldWrapper title={language.IMAGE}>
                          <FileUpload change={(e) => driverDocUpload(`${doc.docsName}`, e)} crop={false} />
                        </FieldWrapper>
                      )}
                    <FieldWrapper>
                      <div className="flex w-full justify-between items-center">
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {doc.docsDetail}
                        </p>
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {formik2.values[doc.docsName].uploading}
                        </p>
                      </div>
                    </FieldWrapper>
                  </Border>
                </>
              ))}
          </Section>
          <Section>
            {docs.PROFILEDOCUMENTS.filter((doc) => doc.status === U.ACTIVE)
              .filter((doc, index) => index % 3 === 2)
              .map((doc) => (
                <>
                  <Border>
                    <Heading title={doc.docsMandatory === true ? `${doc.docsName} *` : `${doc.docsName}`} />

                    {doc.docsExpiry && (
                      <FieldWrapper title={language.EXPIRY_DATE}>
                        <TextFormat
                          format={"##-##-####"}
                          value={formik2.values[doc.docsName].expiry}
                          error={formik1.errors.dob}
                          change={(e) => formik2.setFieldValue(`${doc.docsName}.expiry`, e)}
                          placeholder="MM-DD-YYYY"
                          mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
                        />
                      </FieldWrapper>
                    )}
                    {formik2.values[doc.docsName].expiry_mandatory === false && (
                      <FieldWrapper title={language.IMAGE}>
                        <FileUpload change={(e) => driverDocUpload(`${doc.docsName}`, e)} crop={false} />
                      </FieldWrapper>
                    )}
                    {formik2.values[doc.docsName].expiry_mandatory === true &&
                      formik2.values[doc.docsName].expiry !== "" &&
                      hasAlphabet(formik2.values[doc.docsName].expiry) === false && (
                        <FieldWrapper title={language.IMAGE}>
                          <FileUpload change={(e) => driverDocUpload(`${doc.docsName}`, e)} crop={false} />
                        </FieldWrapper>
                      )}
                    <FieldWrapper>
                      <div className="flex w-full justify-between items-center">
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {doc.docsDetail}
                        </p>
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {formik2.values[doc.docsName].uploading}
                        </p>
                      </div>
                    </FieldWrapper>
                  </Border>
                </>
              ))}
          </Section>
        </>
      )}
      {page === 3 && (
        <>
          <Section>
            <Heading title={language.ADD_VEHICLE} />
            <FieldWrapper title={language.CITY}>
              <DropdownNormal
                change={(e) => formik3.setFieldValue("city", e)}
                fields={cities.map((each) => ({ id: each._id, label: each.locationName }))}
              />
            </FieldWrapper>
            {loadingHubs === true && (
              <FieldWrapper>
                <p className={"text-sm flex items-center mx-2"}>
                  {language.HUBS_LOADING} <FiLoader className={"mx-3 animate-spin"} />
                </p>
              </FieldWrapper>
            )}
            {loadingHubs === false && formik3.values.city !== "" && hubs.length === 0 && (
              <FieldWrapper>
                <p className={"text-sm flex items-center mx-2 text-red-500"}>{language.NO_HUBS_FOR_CITY}</p>
              </FieldWrapper>
            )}
            {formik3.values.city !== "" && hubs.length > 0 && loadingHubs === false && (
              <>
                <FieldWrapper title={language.HUBS}>
                  <DropdownNormal
                    change={(e) => formik3.setFieldValue("hubs", e)}
                    defaultValue={
                      hubs.filter((each) => each._id === formik3.values.hubs).length > 0
                        ? hubs.filter((each) => each._id === formik3.values.hubs)[0].hubsName
                        : ""
                    }
                    fields={hubs.map((each) => ({ id: each._id, label: each.hubsName, value: each._id }))}
                  />
                </FieldWrapper>
                <FieldWrapper title={language.VEHICLE_CATEGORY}>
                  <DropdownNormal
                    error={formik3.errors.vehicleCategory}
                    defaultValue={
                      vehicleCategory.filter((each) => each._id === formik3.values.vehicleCategory).length > 0
                        ? vehicleCategory.filter((each) => each._id === formik3.values.vehicleCategory)[0].name
                        : ""
                    }
                    change={(e) => formik3.setFieldValue("vehicleCategory", e)}
                    fields={vehicleCategory.map((each) => ({
                      id: each._id,
                      label: each.name,
                      value: each._id,
                    }))}
                  />
                </FieldWrapper>
                {formik3.values.vehicleCategory !== "" && (
                  <>
                    <FieldWrapper title={language.MAKER}>
                      <TextField
                        change={(e) => formik3.setFieldValue("maker", e)}
                        error={formik3.errors.maker}
                        value={formik3.values.maker}
                        placeholder={language.MAKER}
                      />
                    </FieldWrapper>
                    <FieldWrapper title={language.MODEL}>
                      <TextField
                        change={(e) => formik3.setFieldValue("model", e)}
                        error={formik3.errors.model}
                        value={formik3.values.model}
                        placeholder={language.MODEL}
                      />
                    </FieldWrapper>
                    <FieldWrapper title={language.YEAR}>
                      <TextField
                        type={"number"}
                        change={(e) => formik3.setFieldValue("year", e)}
                        error={formik3.errors.year}
                        value={formik3.values.year}
                        placeholder={language.YEAR}
                      />
                    </FieldWrapper>
                  </>
                )}
              </>
            )}
          </Section>
          <Section>
            {formik3.values.city !== "" &&
              formik3.values.vehicleCategory !== "" &&
              hubs.length > 0 &&
              loadingHubs === false && (
                <>
                  <FieldWrapper title={language.VIN_NUMBER}>
                    <TextField
                      change={(e) => formik3.setFieldValue("vinNumber", e)}
                      error={formik3.errors.vinNumber}
                      value={formik3.values.vinNumber}
                      placeholder={language.VIN_NUMBER}
                    />
                  </FieldWrapper>
                  <FieldWrapper title={language.PLATE_NUMBER}>
                    <TextField
                      change={(e) => formik3.setFieldValue("plateNumber", e)}
                      error={formik3.errors.plateNumber}
                      value={formik3.values.plateNumber}
                      placeholder={language.PLATE_NUMBER}
                    />
                  </FieldWrapper>
                  <FieldWrapper title={language.TYPE}>
                    <DropdownNormal
                      defaultValue={language[formik3.values.type]}
                      error={formik3.errors.type}
                      change={(e) => formik3.setFieldValue("type", e)}
                      fields={[
                        { id: 1, label: language.PETROL, value: "PETROL" },
                        { id: 2, label: language.DIESEL, value: "DIESEL" },
                        { id: 3, label: language.ELECTRIC, value: "ELECTRIC" },
                        { id: 4, label: language.CNG, value: "CNG" },
                        { id: 5, label: language.OTHERS, value: "OTHER" },
                      ]}
                    />
                  </FieldWrapper>
                  <FieldWrapper title={language.COLOR}>
                    <TextField
                      change={(e) => formik3.setFieldValue("color", e)}
                      error={formik3.errors.color}
                      value={formik3.values.color}
                      placeholder={language.COLOR}
                    />
                  </FieldWrapper>
                  <Flex>
                    <Section padding={false}>
                      <FieldWrapper title={language.DOOR_NO}>
                        <TextField
                          type={"number"}
                          change={(e) => formik3.setFieldValue("noOfDoors", e)}
                          error={formik3.errors.noOfDoors}
                          value={formik3.values.noOfDoors}
                          placeholder={language.DOOR_NO}
                        />
                      </FieldWrapper>
                    </Section>
                    <Section padding={false}>
                      <FieldWrapper title={language.SEATS_NO}>
                        <TextField
                          type={"number"}
                          change={(e) => formik3.setFieldValue("noOfSeats", e)}
                          error={formik3.errors.noOfSeats}
                          value={formik3.values.noOfSeats}
                          placeholder={language.SEATS_NO}
                        />
                      </FieldWrapper>
                    </Section>
                  </Flex>
                  <div className="flex">
                    <Section>
                      <FieldWrapper title={language.HANDICAP}>
                        <ToggleButton
                          change={(e) => formik3.setFieldValue("handicap", e)}
                          defaultValue={formik3.values.handicap}
                        />
                      </FieldWrapper>
                    </Section>
                    <Section>
                      <FieldWrapper title={language.CHILD_SEAT}>
                        <ToggleButton
                          change={(e) => formik3.setFieldValue("childSeat", e)}
                          defaultValue={formik3.values.childSeat}
                        />
                      </FieldWrapper>
                    </Section>
                  </div>
                  <Flex>
                    <Section>
                      <FieldWrapper title={language.COMPANY_VEHICLE}>
                        <ToggleButton
                          change={(e) => formik3.setFieldValue("companyVehicle", e)}
                          defaultValue={formik3.values.companyVehicle}
                        />
                      </FieldWrapper>
                    </Section>
                  </Flex>
                  {/* <FieldWrapper title={language.DEFAULT_VEHICLE}>
                    <ToggleButton
                      change={(e) => formik3.setFieldValue("defaultVehicle", e)}
                      defaultValue={formik3.values.defaultVehicle}
                    />
                  </FieldWrapper> */}
                </>
              )}
          </Section>
          <Section>
            {formik3.values.city !== "" &&
              formik3.values.vehicleCategory !== "" &&
              hubs.length > 0 &&
              loadingHubs === false && (
                <>
                  <FieldWrapper title={language.VEHICLE_IMAGE_FRONT}>
                    <FileUpload change={(e) => formik3.setFieldValue("imageFront", e)} crop={false} />
                  </FieldWrapper>
                  <FieldWrapper title={language.VEHICLE_IMAGE_BACK}>
                    <FileUpload change={(e) => formik3.setFieldValue("imageBack", e)} crop={false} />
                  </FieldWrapper>
                  <FieldWrapper title={language.VEHICLE_IMAGE_LEFT}>
                    <FileUpload change={(e) => formik3.setFieldValue("imageLeft", e)} crop={false} />
                  </FieldWrapper>
                  <FieldWrapper title={language.VEHICLE_IMAGE_RIGHT}>
                    <FileUpload change={(e) => formik3.setFieldValue("imageRight", e)} crop={false} />
                  </FieldWrapper>
                </>
              )}
          </Section>
        </>
      )}
      {page === 4 && (
        <>
          <Section>
            {docs.DRIVERDOCUMENTS.filter((doc) => doc.status === U.ACTIVE)
              .filter((doc, index) => index % 3 === 0)
              .map((doc) => (
                <>
                  <Border>
                    <Heading title={doc.docsMandatory === true ? `${doc.docsName} *` : `${doc.docsName}`} />

                    {doc.docsExpiry && (
                      <FieldWrapper title={language.EXPIRY_DATE}>
                        <TextFormat
                          format={"##-##-####"}
                          value={formik4.values[doc.docsName].expiry}
                          change={(e) => formik4.setFieldValue(`${doc.docsName}.expiry`, e)}
                          placeholder="MM-DD-YYYY"
                          mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
                        />
                      </FieldWrapper>
                    )}
                    {formik4.values[doc.docsName].expiry_mandatory === false && (
                      <FieldWrapper title={language.IMAGE}>
                        <FileUpload change={(e) => vehicleDocUpload(`${doc.docsName}`, e)} crop={false} />
                      </FieldWrapper>
                    )}
                    {formik4.values[doc.docsName].expiry_mandatory === true &&
                      formik4.values[doc.docsName].expiry !== "" &&
                      hasAlphabet(formik4.values[doc.docsName].expiry) === false && (
                        <FieldWrapper title={language.IMAGE}>
                          <FileUpload change={(e) => vehicleDocUpload(`${doc.docsName}`, e)} crop={false} />
                        </FieldWrapper>
                      )}
                    <FieldWrapper>
                      <div className="flex w-full justify-between items-center">
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {doc.docsDetail}
                        </p>
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {formik4.values[doc.docsName].uploading === false
                            ? ""
                            : formik4.values[doc.docsName].uploading}
                        </p>
                      </div>
                    </FieldWrapper>
                  </Border>
                </>
              ))}
          </Section>
          <Section>
            {docs.DRIVERDOCUMENTS.filter((doc) => doc.status === U.ACTIVE)
              .filter((doc, index) => index % 3 === 1)
              .map((doc) => (
                <>
                  <Border>
                    <Heading title={doc.docsMandatory === true ? `${doc.docsName} *` : `${doc.docsName}`} />

                    {doc.docsExpiry && (
                      <FieldWrapper title={language.EXPIRY_DATE}>
                        <TextFormat
                          format={"##-##-####"}
                          value={formik4.values[doc.docsName].expiry}
                          change={(e) => formik4.setFieldValue(`${doc.docsName}.expiry`, e)}
                          placeholder="MM-DD-YYYY"
                          mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
                        />
                      </FieldWrapper>
                    )}
                    {formik4.values[doc.docsName].expiry_mandatory === false && (
                      <FieldWrapper title={language.IMAGE}>
                        <FileUpload change={(e) => vehicleDocUpload(`${doc.docsName}`, e)} crop={false} />
                      </FieldWrapper>
                    )}
                    {formik4.values[doc.docsName].expiry_mandatory === true &&
                      formik4.values[doc.docsName].expiry !== "" &&
                      hasAlphabet(formik4.values[doc.docsName].expiry) === false && (
                        <FieldWrapper title={language.IMAGE}>
                          <FileUpload change={(e) => vehicleDocUpload(`${doc.docsName}`, e)} crop={false} />
                        </FieldWrapper>
                      )}
                    <FieldWrapper>
                      <div className="flex w-full justify-between items-center">
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {doc.docsDetail}
                        </p>
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {formik4.values[doc.docsName].uploading}
                        </p>
                      </div>
                    </FieldWrapper>
                  </Border>
                </>
              ))}
          </Section>
          <Section>
            {docs.DRIVERDOCUMENTS.filter((doc) => doc.status === U.ACTIVE)
              .filter((doc, index) => index % 3 === 2)
              .map((doc) => (
                <>
                  <Border>
                    <Heading title={doc.docsMandatory === true ? `${doc.docsName} *` : `${doc.docsName}`} />

                    {doc.docsExpiry && (
                      <FieldWrapper title={language.EXPIRY_DATE}>
                        <TextFormat
                          format={"##-##-####"}
                          value={formik4.values[doc.docsName].expiry}
                          change={(e) => formik4.setFieldValue(`${doc.docsName}.expiry`, e)}
                          placeholder="MM-DD-YYYY"
                          mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
                        />
                      </FieldWrapper>
                    )}
                    {formik4.values[doc.docsName].expiry_mandatory === false && (
                      <FieldWrapper title={language.IMAGE}>
                        <FileUpload change={(e) => vehicleDocUpload(`${doc.docsName}`, e)} crop={false} />
                      </FieldWrapper>
                    )}
                    {formik4.values[doc.docsName].expiry_mandatory === true &&
                      formik4.values[doc.docsName].expiry !== "" &&
                      hasAlphabet(formik4.values[doc.docsName].expiry) === false && (
                        <FieldWrapper title={language.IMAGE}>
                          <FileUpload change={(e) => vehicleDocUpload(`${doc.docsName}`, e)} crop={false} />
                        </FieldWrapper>
                      )}
                    <FieldWrapper>
                      <div className="flex w-full justify-between items-center">
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {doc.docsDetail}
                        </p>
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {formik4.values[doc.docsName].uploading}
                        </p>
                      </div>
                    </FieldWrapper>
                  </Border>
                </>
              ))}
          </Section>
        </>
      )}
    </FormWrapper>
  );
}
