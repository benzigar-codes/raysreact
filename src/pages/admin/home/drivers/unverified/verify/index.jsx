import React from "react";
import { useParams } from "react-router";
import axios from "axios";
import gsap from "gsap";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import useAdmin from "../../../../../../hooks/useAdmin";
import useImage from "../../../../../../hooks/useImage";
import useLanguage from "../../../../../../hooks/useLanguage";
import useDebug from "../../../../../../hooks/useDebug";

import NavLinks from "../../../../../../utils/navLinks.json";
import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import { Section } from "../../../../../../components/common/Section";
import ImagePreview from "../../../../../../components/common/ImagePreview";
import { Border } from "../../../../../../components/common/Border";
import { Heading } from "../../../../../../components/common/Heading";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { DetailsWrapper } from "../../../../../../components/common/DetailsWrapper";
import { Detail } from "../../../../../../components/common/Detail";
import { TextArea } from "../../../../../../components/common/TextArea";
import { Button } from "../../../../../../components/common/Button";
import { Documents } from "../../../../../../components/common/Documents";
import { format } from "date-fns";
import useUtils from "../../../../../../hooks/useUtils";
import { PopUp } from "../../../../../../components/common/PopUp";
import { DropdownNormal } from "../../../../../../components/common/DropDownNormal";
import { FileUpload } from "../../../../../../components/common/FileUpload";
import { FiEdit, FiPlus } from "react-icons/fi";
import { Link } from "react-router-dom";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, authFailure, token } = useAdmin();
  const { id } = useParams();
  const { imageUrl, compressImage } = useImage();
  const { parseError, morph } = useUtils();

  const [driverNote, setDriverNote] = React.useState("");
  const [vehicleNote, setVehicleNote] = React.useState("");
  const [verifyLoading, setVerifyLoading] = React.useState(null);
  const [loading, setLoading] = React.useState(true);
  const [driver, setDriver] = React.useState({});
  const [popup, setPop] = React.useState(null);

  const [frontImage, setFrontImage] = React.useState(null);
  const [backImage, setBackImage] = React.useState(null);
  const [leftImage, setLeftImage] = React.useState(null);
  const [rightImage, setRightImage] = React.useState(null);

  const [frontModified, setFrontModified] = React.useState(false);
  const [backModified, setBackModified] = React.useState(false);
  const [leftModified, setLeftModified] = React.useState(false);
  const [rightModified, setRightModified] = React.useState(false);

  const [btnLoading, setBtnLoading] = React.useState(false);
  const [vehicle, setVehicle] = React.useState({});
  const [notesLoading, setNotesLoading] = React.useState(false);
  const [imageModel, setImagModel] = React.useState(false);

  const [extraInfo, setExtraInfo] = React.useState({
    handicap: 0,
    childSeat: 0,
    companyVehicle: 0,
  });

  const [vehicleDataMissing, setVehicleDataMissing] = React.useState(false);

  const [professionalDocMissing, setProfessionalDocMissing] = React.useState(false);
  const [vehicleDocMissing, setVehicleDocMissing] = React.useState(false);

  const [categories, setCategories] = React.useState([]);

  const driverVerify = async (docid, docName, docStatus, docExpiry) => {
    setVerifyLoading(docName);
    try {
      await axios.post(
        A.HOST + A.ADMIN_DRIVERS_PROFILE_VERIFY,
        {
          professionalId: id,
          status: docStatus === U.VERIFIED ? U.UNVERIFIED : U.VERIFIED,
          documentName: docName,
          expiryDate: docExpiry,
        },
        header
      );
      const profileVerification = driver.profileVerification.map((each) => {
        if (each.documentName === docName) {
          return {
            ...each,
            uploadedStatus: docStatus === U.VERIFIED ? U.UNVERIFIED : U.VERIFIED,
            status: docStatus === U.VERIFIED ? U.UNVERIFIED : U.VERIFIED,
          };
        } else return each;
      });
      setDriver({ ...driver, profileVerification });
    } catch (err) {
      setPop({ title: language.DOCUMENT_UNVERIFIED_ERROR, type: "error" });
    }
    setVerifyLoading(null);
  };

  const driverNoteSave = async () => {
    setNotesLoading(true);
    try {
      await axios.post(
        A.HOST + A.ADMIN_DRIVERS_NOTES,
        {
          professionalId: id,
          notes: driverNote,
        },
        header
      );
    } catch (err) {}
    setNotesLoading(false);
  };

  const vehicleVerify = async (docid, docName, docStatus, docExpiry) => {
    setVerifyLoading(docName);
    try {
      await axios.post(
        A.HOST + A.ADMIN_VEHICLES_DOC_VERIFY,
        {
          professionalId: id,
          id: vehicle._id || "",
          status: docStatus === U.VERIFIED ? U.UNVERIFIED : U.VERIFIED,
          documentName: docName,
          expiryDate: docExpiry,
        },
        header
      );
      const vehicleDocuments = vehicle.vehicleDocuments.map((each) => {
        if (each.documentName === docName) {
          return {
            ...each,
            uploadedStatus: docStatus === U.VERIFIED ? U.UNVERIFIED : U.VERIFIED,
            status: docStatus === U.VERIFIED ? U.UNVERIFIED : U.VERIFIED,
          };
        } else return each;
      });
      setVehicle({ ...vehicle, vehicleDocuments });
    } catch (err) {
      setPop({ title: language.DOCUMENT_UNVERIFIED_ERROR, type: "error" });
    }
    setVerifyLoading(null);
  };

  const vehicleNoteSave = async () => {
    setNotesLoading(true);
    try {
      await axios.post(
        A.HOST + A.ADMIN_VEHICLES_NOTES,
        {
          professionalId: id,
          id: vehicle._id || "",
          notes: vehicleNote,
        },
        header
      );
    } catch (err) {}
    setNotesLoading(false);
  };

  const verifyProfessional = async () => {
    setBtnLoading(true);
    let varifiable = true;
    driver.profileVerification.forEach((doc) => {
      if (doc.status === U.UNVERIFIED || doc.uploadedStatus === U.UPLOADED) varifiable = false;
    });
    vehicle.vehicleDocuments.forEach((doc) => {
      if (doc.status === U.UNVERIFIED || doc.uploadedStatus === U.UPLOADED) varifiable = false;
    });
    if (varifiable) {
      if (
        frontImage !== null &&
        frontImage !== "" &&
        backImage !== null &&
        backImage !== "" &&
        leftImage !== null &&
        leftImage !== "" &&
        rightImage !== null &&
        rightImage !== ""
      ) {
        if (vehicle.vehicleCategoryId) {
          try {
            const formData = new FormData();
            formData.append("professionalId", id);
            formData.append("id", vehicle._id);
            formData.append("vehicleCategoryId", vehicle.vehicleCategoryId);
            formData.append("vinNumber", vehicle.vinNumber);
            formData.append("serviceCategory", vehicle.serviceCategory);
            formData.append("plateNumber", vehicle.plateNumber);
            formData.append("type", vehicle.type);
            formData.append("maker", vehicle.maker);
            formData.append("model", vehicle.model);
            formData.append("year", vehicle.year);
            formData.append("color", vehicle.color);
            formData.append("noOfDoors", vehicle.noOfDoors);
            formData.append("noOfSeats", vehicle.noOfSeats);
            formData.append("hub", vehicle.hubs);
            formData.append("scheduleDate", vehicle.scheduleDate);
            formData.append("status", U.VERIFIED);
            formData.append("handicapAvailable", extraInfo.handicap === 0 ? false : true);
            formData.append("childseatAvailable", extraInfo.childSeat === 0 ? false : true);
            formData.append("defaultVehicle", vehicle.defaultVehicle);
            formData.append("isCompanyVehicle", extraInfo.companyVehicle === 0 ? false : true);
            let front, back, left, right;

            if (frontModified) {
              front = await compressImage(frontImage);
            } else front = frontImage;

            if (backModified) {
              back = await compressImage(backImage);
            } else back = backImage;

            if (leftModified) {
              left = await compressImage(leftImage);
            } else left = leftImage;

            if (rightModified) {
              right = await compressImage(rightImage);
            } else right = rightImage;

            formData.append("frontImage", front);
            formData.append("backImage", back);
            formData.append("leftImage", left);
            formData.append("rightImage", right);

            await axios.post(A.HOST + A.ADMIN_DRIVERS_ADD_STEP_3, formData, {
              headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/form-data",
              },
            });
            try {
              await axios.post(
                A.HOST + A.ADMIN_DRIVERS_VERIFICATION,
                {
                  professionalId: id,
                },
                header
              );
              history.push(NavLinks.ADMIN_DRIVERS_LIST);
            } catch (err) {
              setPop({ title: parseError(err), type: "error" });
            }
          } catch (er) {
            alert(er);
            setPop({ title: language.PROBLEM_IN_VEHICLE_UPDATE, type: "error" });
          }
        } else {
          setPop({ title: language.VEHICLE_CATEGORY_REQUIRED, type: "error" });
        }
      } else {
        setPop({ title: language.IMAGE_REQUIRED, type: "error" });
      }
    } else setPop({ title: language.ALL_DOC_VERIFIED, type: "error" });
    //
    setBtnLoading(false);
  };

  React.useEffect(() => {
    const fetchDriver = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_DRIVERS_READ, { id }, header);
        const docs = await axios.post(A.HOST + A.ADMIN_DRIVERS_DOC_LIST, {}, header);

        // PROFESSIONAL DOC TESTING
        if (data.profileVerification && data.profileVerification.length > 0) {
          const professionalMandatoryDocs = docs.data.data.PROFILEDOCUMENTS.filter(
            (doc) => doc.status === U.ACTIVE && doc.docsMandatory === true
          ).map((doc) => doc.docsName);
          const professionalMandatoryDocsAvailable = data.profileVerification.map((doc) => doc.documentName);

          let docsMatch = 0;
          professionalMandatoryDocs.forEach((doc) => {
            if (professionalMandatoryDocsAvailable.includes(doc)) docsMatch++;
          });
          if (docsMatch !== professionalMandatoryDocs.length) setProfessionalDocMissing(true);
        } else setProfessionalDocMissing(false);

        setDriver(data);
        if (
          data.vehicles &&
          data.vehicles.length > 0 &&
          data.vehicles.filter((each) => each.defaultVehicle === true).length > 0
        ) {
          setVehicleDataMissing(
            data.vehicles.filter((each) => each.defaultVehicle === true)[0].serviceCategory &&
              data.vehicles.filter((each) => each.defaultVehicle === true)[0].hub
              ? false
              : true
          );

          // VEHICLE DOC TESTING
          if (
            data.vehicles.filter((each) => each.defaultVehicle === true)[0].vehicleDocuments &&
            data.vehicles.filter((each) => each.defaultVehicle === true)[0].vehicleDocuments.length > 0
          ) {
            const vehicleMandatoryDocs = docs.data.data.DRIVERDOCUMENTS.filter(
              (doc) => doc.status === U.ACTIVE && doc.docsMandatory === true
            ).map((doc) => doc.docsName);
            const vehicleMandatoryDocsAvailable = data.vehicles
              .filter((each) => each.defaultVehicle === true)[0]
              .vehicleDocuments.map((doc) => doc.documentName);
            let vehicleDocsMatch = 0;
            vehicleMandatoryDocs.forEach((doc) => {
              if (vehicleMandatoryDocsAvailable.includes(doc)) vehicleDocsMatch++;
            });
            if (vehicleDocsMatch !== vehicleMandatoryDocs.length) setVehicleDocMissing(true);
          } else setVehicleDocMissing(false);

          setVehicle(data.vehicles.filter((each) => each.defaultVehicle === true)[0]);
          setFrontImage(data.vehicles.filter((each) => each.defaultVehicle === true)[0].frontImage || null);
          setBackImage(data.vehicles.filter((each) => each.defaultVehicle === true)[0].backImage || null);
          setLeftImage(data.vehicles.filter((each) => each.defaultVehicle === true)[0].leftImage || null);
          setRightImage(data.vehicles.filter((each) => each.defaultVehicle === true)[0].rightImage || null);

          if (data.vehicles.filter((each) => each.defaultVehicle === true)[0].serviceCategory) {
            const categories = await axios.post(
              A.HOST + A.ADMIN_VEHCILE_CATEGORY_BASED_ON_CITY,
              {
                city: data.vehicles.filter((each) => each.defaultVehicle === true)[0].serviceCategory,
              },
              header
            );
            setCategories(categories.data);
          }
        } else {
          setVehicle(null);
        }
        setDriverNote(data.notes ? data.notes : "");
        setLoading(false);
      } catch (err) {
        alert(err);
        authFailure(err);
      }
    };
    fetchDriver();
  }, []);

  React.useEffect(() => {
    if (loading === false) gsap.fromTo(".viewDriver", 0.8, { opacity: 0 }, { opacity: 1, stagger: 0.2 });
  }, [loading]);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      btnLoading={btnLoading}
      showVerify={
        categories !== null &&
        vehicle !== null &&
        vehicleDataMissing === false &&
        professionalDocMissing === false &&
        vehicleDocMissing === false
      }
      verifyClick={verifyProfessional}
      width={"5/6"}
      bread={[{ id: 1, title: language.DRIVERS, path: NavLinks.ADMIN_DRIVERS_UNVERIFIED_LIST }]}
      title={language.VERIFY_DRIVER}
    >
      <div className={"w-full"}>
        {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
        {vehicle === null && (
          <p className={"w-full text-red-500 flex justify-center items-center w-full text-sm my-2"}>
            Default Vehicle is missing, Add a vehicle to verify Professional{" "}
            <Link to={NavLinks.ADMIN_DRIVERS_LIST + id + NavLinks.ADMIN_VEHICLES_ADD}>
              <FiPlus className={"text-4xl text-white bg-blue-800 mx-3 rounded-full p-2"} />
            </Link>
          </p>
        )}
        {vehicle !== null && vehicleDataMissing === true && (
          <p className={"w-full text-red-500 flex justify-center items-center w-full text-sm my-2"}>
            Vehicle Registeration is not completed correctly
            <Link to={NavLinks.ADMIN_DRIVERS_LIST + id + NavLinks.ADMIN_VEHICLES_EDIT + "/" + vehicle._id}>
              <FiEdit className={"text-4xl text-white bg-blue-800 mx-3 rounded-full p-2"} />
            </Link>
          </p>
        )}
        {professionalDocMissing === true && (
          <p className={"w-full text-red-500 flex justify-center items-center w-full text-sm my-2"}>
            Professional Mandatory Documents is not uploaded
            <Link to={NavLinks.ADMIN_DRIVERS_EDIT + "/" + id}>
              <FiEdit className={"text-4xl text-white bg-blue-800 mx-3 rounded-full p-2"} />
            </Link>
          </p>
        )}
        {vehicleDocMissing === true && vehicleDataMissing === false && (
          <p className={"w-full text-red-500 flex justify-center items-center w-full text-sm my-2"}>
            Vehicle Mandatory Documents is not uploaded
            <Link to={NavLinks.ADMIN_DRIVERS_LIST + id + NavLinks.ADMIN_VEHICLES_EDIT + "/" + vehicle._id}>
              <FiEdit className={"text-4xl text-white bg-blue-800 mx-3 rounded-full p-2"} />
            </Link>
          </p>
        )}
        <div className="flex w-full items-center justify-between px-10 mb-4">
          <div className="w-full bg-blue-800" style={{ height: 2 }}></div>
          <h1 className={"text-blue-800 text-sm text-center"} style={{ width: 400 }}>
            {language.PROFESSIONAL_DETAILS}
          </h1>
          <Link to={NavLinks.ADMIN_DRIVERS_EDIT + "/" + id}>
            <FiEdit className={"text-4xl text-white bg-blue-800 mx-3 rounded-full p-2"} />
          </Link>
          <div className="w-full bg-blue-800" style={{ height: 2 }}></div>
        </div>
        <div className={"flex w-full"}>
          <Section width="1/2">
            <ImagePreview close={() => setImagModel(false)} show={imageModel} />
            <Border animate="viewDriver">
              <Heading title={language.PERSONAL_DETAILS} />
              <FieldWrapper>
                <DetailsWrapper>
                  <Detail title={language.FIRSTNAME} value={morph(driver.firstName)} />
                  <Detail title={language.LASTNAME} value={morph(driver.lastName)} />
                  <Detail title={language.EMAIL} value={morph(driver.email)} />
                  <Detail title={language.MOBILE} value={morph(driver.phone.code) + " " + morph(driver.phone.number)} />
                  <Detail title={language.GENDER} value={driver.gender} />
                  <Detail title={language.DATE_OF_BIRTH} value={driver.dob} />
                  <Detail
                    title={language.PRO_IMG}
                    value={<img className="w-full" src={imageUrl(driver.avatar)} alt={"Driver"} />}
                  />
                </DetailsWrapper>
              </FieldWrapper>
            </Border>
            <Border animate="viewDriver">
              <Heading title={language.NOTES} />
              <FieldWrapper>
                <TextArea change={(e) => setDriverNote(e)} value={driverNote} />
              </FieldWrapper>
              <FieldWrapper>
                <Button onClick={driverNoteSave} loading={notesLoading} title={language.SAVE} />
              </FieldWrapper>
            </Border>
          </Section>
          <Section width="1/2">
            <Border animate="viewDriver">
              <Heading title={language.ADDRESS_DETAILS} />
              <FieldWrapper>
                <DetailsWrapper>
                  <Detail title={language.ADDRESS_LINE_1} value={morph(driver.address.line)} />
                  <Detail title={language.CITY} value={morph(driver.address.city)} />
                  <Detail title={language.STATE} value={morph(driver.address.state)} />
                  <Detail title={language.COUNTRY} value={morph(driver.address.country)} />
                  <Detail title={language.ZIPCODE} value={morph(driver.address.zipcode)} />
                </DetailsWrapper>
              </FieldWrapper>
            </Border>
            {driver.profileVerification.length > 0 && (
              <Border animate="viewDriver">
                <Heading title={language.DOCUMENT_DETAILS} />
                {driver.profileVerification.map((doc) => (
                  <Documents
                    title={doc.documentName}
                    imageClick={(docImage) => setImagModel(docImage)}
                    verified={doc.status === U.VERIFIED && doc.uploadedStatus === U.VERIFIED}
                    verifyText={
                      doc.status === U.VERIFIED && doc.uploadedStatus === U.VERIFIED
                        ? language.VERIFIED
                        : doc.status === U.VERIFIED && doc.uploadedStatus === U.UPLOADED
                        ? language.UPLOADED
                        : language.UNVERIFIED
                    }
                    loading={verifyLoading === doc.documentName}
                    verifyClick={() =>
                      driverVerify(
                        doc._id,
                        doc.documentName,
                        doc.status === U.VERIFIED && doc.uploadedStatus === U.VERIFIED ? U.VERIFIED : U.UNVERIFIED,
                        doc.expiryDate
                      )
                    }
                    expireDate={
                      doc.expiryDate && doc.expiryDate !== "" ? format(new Date(doc.expiryDate), "do MMM yyyy") : null
                    }
                    image={imageUrl(doc.documents[0])}
                  />
                ))}
              </Border>
            )}
          </Section>
        </div>
        {vehicle !== null && vehicleDataMissing === false && (
          <div className={"w-full"}>
            <div className="flex w-full items-center justify-between px-10 mb-4">
              <div className="w-full bg-blue-800" style={{ height: 2 }}></div>
              <h1 className={"text-blue-800 text-sm text-center"} style={{ width: 400 }}>
                {language.DEFAULT_VEHICLE}
              </h1>
              <Link to={NavLinks.ADMIN_DRIVERS_LIST + id + NavLinks.ADMIN_VEHICLES_EDIT + "/" + vehicle._id}>
                <FiEdit className={"text-4xl text-white bg-blue-800 mx-3 rounded-full p-2"} />
              </Link>
              <div className="w-full bg-blue-800" style={{ height: 2 }}></div>
            </div>
            <div className={"flex w-full"}>
              <Section>
                <ImagePreview close={() => setImagModel(false)} show={imageModel} />
                <Border>
                  <FieldWrapper title={language.VEHICLE_CATEGORY}>
                    <DropdownNormal
                      defaultValue={
                        categories !== null &&
                        vehicle !== null &&
                        categories.filter((category) => category._id === vehicle.vehicleCategoryId).length > 0
                          ? categories.filter((category) => category._id === vehicle.vehicleCategoryId)[0].name
                          : ""
                      }
                      change={(e) => setVehicle({ ...vehicle, vehicleCategoryId: e })}
                      fields={categories.map((each) => ({
                        id: each._id,
                        label: each.name,
                        value: each._id,
                      }))}
                    />
                  </FieldWrapper>
                  <div className="flex">
                    <FieldWrapper
                      title={language.VEHICLE_IMAGE_FRONT}
                      style={{
                        width: "50%",
                      }}
                    >
                      {frontImage === null ? (
                        <FileUpload
                          change={(e) => {
                            setFrontImage(e);
                            setFrontModified(true);
                          }}
                          crop={false}
                        />
                      ) : (
                        <FileUpload
                          defaultValue={imageUrl(frontImage)}
                          change={(e) => {
                            setFrontImage(e);
                            setFrontModified(true);
                          }}
                          crop={false}
                        />
                      )}
                    </FieldWrapper>
                    <FieldWrapper
                      title={language.VEHICLE_IMAGE_BACK}
                      style={{
                        width: "50%",
                      }}
                    >
                      {backImage === null ? (
                        <FileUpload
                          change={(e) => {
                            setBackImage(e);
                            setBackModified(true);
                          }}
                          crop={false}
                        />
                      ) : (
                        <FileUpload
                          defaultValue={imageUrl(backImage)}
                          change={(e) => {
                            setBackImage(e);
                            setBackModified(true);
                          }}
                          crop={false}
                        />
                      )}
                    </FieldWrapper>
                  </div>
                  <div className="flex">
                    <FieldWrapper
                      title={language.VEHICLE_IMAGE_LEFT}
                      style={{
                        width: "50%",
                      }}
                    >
                      {leftImage === null ? (
                        <FileUpload
                          change={(e) => {
                            setLeftImage(e);
                            setLeftModified(true);
                          }}
                          crop={false}
                        />
                      ) : (
                        <FileUpload
                          defaultValue={imageUrl(leftImage)}
                          change={(e) => {
                            setLeftImage(e);
                            setLeftModified(true);
                          }}
                          crop={false}
                        />
                      )}
                    </FieldWrapper>
                    <FieldWrapper
                      title={language.VEHICLE_IMAGE_RIGHT}
                      style={{
                        width: "50%",
                      }}
                    >
                      {rightImage === null ? (
                        <FileUpload
                          change={(e) => {
                            setRightImage(e);
                            setRightModified(true);
                          }}
                          crop={false}
                        />
                      ) : (
                        <FileUpload
                          defaultValue={imageUrl(rightImage)}
                          change={(e) => {
                            setRightImage(e);
                            setRightModified(true);
                          }}
                          crop={false}
                        />
                      )}
                    </FieldWrapper>
                  </div>
                  <div className="flex">
                    <Section>
                      <FieldWrapper title={language.HANDICAP}>
                        <ToggleButton
                          change={(e) => setExtraInfo({ ...extraInfo, handicap: e })}
                          defaultValue={extraInfo.handicap}
                        />
                      </FieldWrapper>
                    </Section>
                    <Section>
                      <FieldWrapper title={language.CHILD_SEAT}>
                        <ToggleButton
                          change={(e) => setExtraInfo({ ...extraInfo, childSeat: e })}
                          defaultValue={extraInfo.childSeat}
                        />
                      </FieldWrapper>
                    </Section>
                    <Section>
                      <FieldWrapper title={language.COMPANY_VEHICLE}>
                        <ToggleButton
                          change={(e) => setExtraInfo({ ...extraInfo, companyVehicle: e })}
                          defaultValue={extraInfo.companyVehicle}
                        />
                      </FieldWrapper>
                    </Section>
                  </div>
                </Border>
                <Border animate="viewDriver">
                  <Heading title={language.NOTES} />
                  <FieldWrapper>
                    <TextArea change={(e) => setVehicleNote(e)} value={vehicleNote} />
                  </FieldWrapper>
                  <FieldWrapper>
                    <Button onClick={vehicleNoteSave} loading={notesLoading} title={language.SAVE} />
                  </FieldWrapper>
                </Border>
              </Section>
              <Section>
                {vehicle.vehicleDocuments.length > 0 && (
                  <Border animate="viewDriver">
                    <Heading title={language.DOCUMENT_DETAILS} />
                    {vehicle.vehicleDocuments.map((doc) => (
                      <Documents
                        title={doc.documentName}
                        imageClick={(docImage) => setImagModel(docImage)}
                        verified={doc.status === U.VERIFIED && doc.uploadedStatus === U.VERIFIED}
                        verifyText={
                          doc.status === U.VERIFIED && doc.uploadedStatus === U.VERIFIED
                            ? language.VERIFIED
                            : doc.status === U.VERIFIED && doc.uploadedStatus === U.UPLOADED
                            ? language.UPLOADED
                            : language.UNVERIFIED
                        }
                        loading={verifyLoading === doc.documentName}
                        verifyClick={() =>
                          vehicleVerify(
                            doc._id,
                            doc.documentName,
                            doc.status === U.VERIFIED && doc.uploadedStatus === U.VERIFIED ? U.VERIFIED : U.UNVERIFIED,
                            doc.expiryDate
                          )
                        }
                        expireDate={doc.expiryDate !== "" ? format(new Date(doc.expiryDate), "do MMM yyyy") : null}
                        image={imageUrl(doc.documents[0])}
                      />
                    ))}
                  </Border>
                )}
                <Border animate="viewDriver">
                  <Heading title={language.VEHICLE_DETAILS} />
                  <FieldWrapper>
                    <DetailsWrapper>
                      <Detail title={language.MAKER} value={vehicle.maker} />
                      <Detail title={language.MODEL} value={vehicle.model} />
                      <Detail title={language.YEAR} value={vehicle.year} />
                      <Detail title={language.COLOR} value={vehicle.color} />
                      <Detail title={language.TYPE} value={vehicle.type} />
                      <Detail title={language.DOOR_NO} value={vehicle.noOfDoors} />
                      <Detail title={language.SEATS_NO} value={vehicle.noOfSeats} />
                      <Detail title={language.VIN_NUMBER} value={vehicle.vinNumber} />
                      <Detail title={language.PLATE_NUMBER} value={vehicle.plateNumber} />
                    </DetailsWrapper>
                  </FieldWrapper>
                </Border>
              </Section>
            </div>
          </div>
        )}
      </div>
    </FormWrapper>
  );
}
