import React from "react";
import { useParams } from "react-router-dom";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";
import { format } from "date-fns";

export default function Index({ history }) {
  const { id } = useParams();
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <Table
      title={language.TRANSACTION_HISTORY}
      startingHeadings={[
        {
          id: 2,
          title: language.TNX_ID,
          key: "tnx_id",
          show: true,
        },
        {
          id: 3,
          title: language.FROM,
          key: "oneByOne",
          show: true,
        },
        {
          id: 4,
          title: language.TO,
          key: "oneByOne2",
          show: true,
        },
        {
          id: 5,
          title: language.TNX_DATE,
          key: "oneByOne3",
          show: true,
        },
        {
          id: 6,
          title: language.TNX_AMOUNT,
          key: "tnx_amount",
          show: true,
        },
        {
          id: 7,
          title: language.TXN_TYPE,
          key: "txn_type",
          show: true,
        },
        {
          id: 8,
          title: language.STATUS,
          key: "tnx_status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_DRIVERS_TRANACTIONS + "/" + id}
      assignData={(data) =>
        data.map((wallet) => ({
          _id: wallet._id,
          tnx_id: wallet.transactionId,
          oneByOne: wallet.from.name ? [morph(wallet.from.name), morph(wallet.from.phoneNumber)] : [""],
          oneByOne2: wallet.to.name ? [morph(wallet.to.name), morph(wallet.to.phoneNumber)] : [""],
          oneByOne3: [
            format(new Date(wallet.transactionDate), "do MMM yyyy"),
            format(new Date(wallet.transactionDate), "hh : mm aa"),
          ],
          txn_type: wallet.transactionType,
          tnx_amount: wallet.transactionAmount,
          tnx_status: language.SUCCESS,
        }))
      }
      bread={[{ id: 1, title: language.PROFESSIONALS, path: NavLinks.ADMIN_DRIVERS_VIEW }]}
      showAdd={admin.privileges.ADMIN.ROLES ? (admin.privileges.ADMIN.ROLES.ADD ? true : false) : false}
      showArchieve={false}
      showStatus={false}
      showEdit={admin.privileges.ADMIN.ROLES ? (admin.privileges.ADMIN.ROLES.EDIT ? true : false) : false}
      addClick={() => history.push(NavLinks.ADMIN_ADMIN_ROLE_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_ADMIN_ROLE_EDIT + "/" + e)}
      showBulk={false}
      showSearch={false}
      showAction={false}
      statusList={A.HOST + A.ADMIN_ROLE_STATUS}
    />
  );
}
