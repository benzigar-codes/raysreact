import React, { useState, useEffect, useRef } from "react";
import { Link, NavLink, useHistory } from "react-router-dom";
import { nanoid } from "nanoid";
import gsap from "gsap/gsap-core";
import {
  FiAirplay,
  FiArrowRightCircle,
  FiBell,
  FiCommand,
  FiCreditCard,
  FiSearch,
  FiSettings,
  FiUser,
  FiUsers,
  FiVolume2,
} from "react-icons/fi";
import { GiCommercialAirplane, GiSteeringWheel } from "react-icons/gi";
import {
  AiOutlineAntDesign,
  AiOutlineClose,
  AiOutlineDeploymentUnit,
  AiOutlineEnvironment,
  AiOutlineExpand,
  AiOutlineExpandAlt,
  AiOutlineFolderView,
  AiOutlineMail,
  AiOutlineMoneyCollect,
  AiOutlineNotification,
  AiOutlinePayCircle,
  AiOutlinePercentage,
} from "react-icons/ai";
import { HiOutlineUser } from "react-icons/hi";
import useLanguage from "../../../hooks/useLanguage";

import NavLinks from "../../../utils/navLinks.json";
import U from "../../../utils/utils.js";
import useAdmin from "../../../hooks/useAdmin";
import useSettings from "../../../hooks/useSettings";

const Fields = ({ field, toggleSelect, fadeOutNav = () => {} }) => {
  const history = useHistory();
  const borders =
    field.selected === true
      ? "text-xl text-white cursor-pointer flex justify-start p-3 py-1"
      : "text-xl text-white cursor-pointer flex justify-start p-3 py-1";
  return (
    <>
      <div
        onClick={() => {
          if (field.path) {
            history.push(field.path);
            fadeOutNav();
          }
          toggleSelect(field.id);
        }}
        className={borders}
      >
        <div className="flex items-center fade fadeOut">
          <p className={field.selected === true ? "opacity-100" : "opacity-50"}>
            {field.icon}
          </p>

          <p className={field.selected === true ? "mx-3 opacity-100" : "mx-3 opacity-50"}>
            {field.name}
          </p>
          {field.selected === true && <FiArrowRightCircle className="text-sm" />}
        </div>
      </div>
    </>
  );
};

const SideFields = ({ children, fadeOutNav }) => {
  useEffect(() => {
    gsap.fromTo(".fade3", 0.3, { opacity: 0, y: 5 }, { opacity: 1, y: 0, stagger: 0.05 });
  }, [children]);
  return (
    <>
      {children.length > 0 && (
        <div className="hoverflow-y-scroll text-white text-md pt-5 mt-20 opacity-0 fade3 fadeOut mx-4 pl-10 flex flex-col z-30">
          {children
            .filter((field) => field.show === true)
            .map((ch) => (
              <Link
                to={ch.path}
                onClick={fadeOutNav}
                key={ch.key}
                className="opacity-0 fade3 fadeOut cursor-pointer flex items-center my-1 px-2 py-1"
              >
                {/* {ch.icon} */}
                <h1 className="text-md mx-3 opacity-50 hover:opacity-100">{ch.name}</h1>
              </Link>
            ))}
        </div>
      )}
    </>
  );
};

const NavBar = ({ closeNavBar, selected = 1, setSelected = () => {} }) => {
  const { admin } = useAdmin();
  const searchRef = useRef("");
  const { language } = useLanguage();
  const history = useHistory();
  const [selector, setSelector] = React.useState(0);
  const { settings } = useSettings();

  const ifHasView = (menu) => {
    return admin.privileges[menu]
      ? Object.keys(admin.privileges[menu])
          .map((eachKey) => admin.privileges[menu][eachKey])
          .filter(
            (eachField) =>
              eachField.VIEW === true || eachField.ADD === true || eachField.EDIT === true
          ).length > 0
        ? true
        : false
      : false;
  };

  const ifHasField = (main, field, VIEW = "VIEW") =>
    admin.privileges[main] !== undefined
      ? admin.privileges[main][field] !== undefined
        ? admin.privileges[main][field][VIEW] !== undefined
          ? admin.privileges[main][field][VIEW] === true
            ? true
            : false
          : false
        : false
      : false;

  const [fields, setFields] = useState([
    {
      id: 1,
      name: language.ADMIN,
      icon: <FiUser />,
      selected: false,
      show: ifHasView("ADMIN"),
      children: [
        {
          id: 1,
          icon: <FiCommand />,
          name: language.ROLES,
          path: NavLinks.ADMIN_ADMIN_ROLE_LIST,
          show: ifHasField("ADMIN", "ROLES"),
        },
        {
          id: 2,
          icon: <FiCommand />,
          name: language.OPERATORS,
          path: NavLinks.ADMIN_ADMIN_OPERATORS_LIST,
          show: ifHasField("ADMIN", "OPERATORS"),
        },
      ],
    },
    {
      id: 2,
      name: language.SERVICE_SETUP,
      icon: <FiCommand />,
      selected: false,
      show: ifHasView("SETUP"),
      children: [
        {
          id: 1,
          icon: <FiCommand />,
          name: language.VEHICLE_CATEGORY_SETUP,
          path: NavLinks.ADMIN_SETUP_VECHICLE_CATEGORY,
          show: ifHasField("SETUP", "VEHICLE_CATEGORYS"),
        },

        {
          id: 2,
          icon: <FiCommand />,
          name: language.CITY_FARE_SETUP,
          path: NavLinks.ADMIN_SETUP_CITY_COMMISION,
          show: ifHasField("SETUP", "CITY_COMMISIONS"),
        },
        {
          id: 3,
          icon: <FiCommand />,
          name: language.HUBS,
          path: NavLinks.ADMIN_ADMIN_HUBS_LIST,
          show: ifHasField("ADMIN", "HUBS"),
        },
        {
          id: 4,
          icon: <FiCommand />,
          name: language.ONBOARDING_DOCUMENTS,
          path: NavLinks.ADMIN_SETUP_DOCUMENTS,
          show: ifHasField("SETUP", "DOCUMENTS"),
        },

        {
          id: 5,
          icon: <FiCommand />,
          name: language.ONBOARDING_COMMON_DOC_LIST,
          path: NavLinks.ADMIN_SETUP_VERIFICATION_DOCUMENTS,
          show: ifHasField("SETUP", "VERIFICATION_DOCUMENTS"),
        },
        {
          id: 6,
          icon: <FiCommand />,
          name: language.CANCELLATION_SETUP,
          path: NavLinks.ADMIN_SETUP_CANCELLATION_REASONS,
          show: ifHasField("SETUP", "CANCELLATION_REASONS"),
        },
        // {
        //   id: 7,
        //   icon: <FiCommand />,
        //   name: language.EMAIL_TEMPLATES,
        //   path: NavLinks.ADMIN_SETUP_EMAIL_TEMPLATES,
        //   show: ifHasField("SETUP", "EMAIL_TEMPLATES"),
        // },
        // {
        //   id: 7,
        //   icon: <FiCommand />,
        //   name: language.TOLLS,
        //   path: NavLinks.ADMIN_SETUP_TOLLS,
        //   show: ifHasField("SETUP", "TOLLS"),
        // },
        // {
        //   id: 7,
        //   icon: <FiCommand />,
        //   name: language.POPULAR_PLACES,
        //   path: NavLinks.ADMIN_SETUP_POPULAR_PLACES,
        //   show: ifHasField("SETUP", "POPULAR_PLACES"),
        // },
        // {
        //   id: 8,
        //   icon: <FiCommand />,
        //   name: language.COUPONS,
        //   path: NavLinks.ADMIN_SETUP_COUPONS,
        //   show: ifHasField("SETUP", "COUPONS"),
        // },
        // {
        //   id: 9,
        //   icon: <FiCommand />,
        //   name: language.AIRPORTS,
        //   path: NavLinks.ADMIN_SETUP_AIRPORTS,
        //   show: ifHasField("SETUP", "AIRPORTS"),
        // },
      ],
    },
    {
      id: 3,
      name: language.SETTINGS,
      icon: <FiSettings />,
      selected: false,
      show: ifHasView("SETTINGS"),
      children: [
        {
          id: 1,
          icon: <FiCommand />,
          name: language.GENERAL_SETTINGS,
          path: NavLinks.ADMIN_GENERAL,
          // show: ifHasField("SETTINGS", "GENERAL", "EDIT"),
          show: admin.userType === "ADMIN" || admin.userType === "DEVELOPER",
        },
        {
          id: 2,
          icon: <FiCommand />,
          name: language.SMTP,
          path: NavLinks.ADMIN_SMTP,
          // show: ifHasField("SETTINGS", "SMTP", "EDIT"),
          show: admin.userType === "DEVELOPER",
        },
        {
          id: 3,
          icon: <FiCommand />,
          name: language.SMS,
          path: NavLinks.ADMIN_SMS,
          // show: ifHasField("SETTINGS", "SMS", "EDIT"),
          show: admin.userType === "DEVELOPER",
        },
        {
          id: 4,
          icon: <FiCommand />,
          name: language.LANGUAGE,
          path: NavLinks.ADMIN_LANGUAGE,
          show: ifHasField("SETTINGS", "LANGUAGE", "EDIT"),
        },
      ],
    },
    {
      id: 4,
      name: language.USER,
      icon: <FiUsers />,
      selected: false,
      show: ifHasView("USERS"),
      children: [
        {
          id: 1,
          icon: <FiCommand />,
          name: language.USER,
          path: NavLinks.ADMIN_USERS_VIEW,
          show: ifHasField("USERS", "USERS_LIST"),
        },
        {
          id: 2,
          icon: <FiCommand />,
          name: language.USERS_FAILED_TO_REGISTER,
          path: NavLinks.ADMIN_UNREGISTERED_USERS,
          show: ifHasField("USERS", "UNREGISTERED_USER"),
        },
      ],
    },
    {
      id: 5,
      name: language.DRIVER,
      icon: <GiSteeringWheel />,
      selected: false,
      show: ifHasView("DRIVERS"),
      children: [
        {
          id: 1,
          icon: <FiCommand />,
          name: language.DRIVERS,
          path: NavLinks.ADMIN_DRIVERS_LIST,
          show: ifHasField("DRIVERS", "DRIVERS_LIST"),
        },
        {
          id: 2,
          icon: <FiCommand />,
          name: language.PROFESSIONALS_NOT_YET_VERIFIED,
          path: NavLinks.ADMIN_DRIVERS_UNVERIFIED_LIST,
          show: ifHasField("DRIVERS", "UNVERIFIED_DRIVERS"),
        },
        {
          id: 3,
          icon: <FiCommand />,
          name: language.PROFESSIONALS_EXPIRED_UNVERIFIED_DOCUMENTS,
          path: NavLinks.ADMIN_DRIVERS_UPLOADED_LIST,
          show: ifHasField("DRIVERS", "DRIVERS_LIST"),
        },
        {
          id: 4,
          icon: <FiCommand />,
          name: language.PROFESSIONALS_FAILED_TO_REGISTER,
          path: NavLinks.ADMIN_DRIVERS_UNREGISTERTED_LIST,
          show: ifHasField("DRIVERS", "UNREGISTERED_DRIVERS"),
        },
      ],
    },
    {
      id: 6,
      name: language.RIDES,
      icon: <FiAirplay />,
      selected: false,
      show: ifHasView("RIDES"),
      children: [
        {
          id: 1,
          icon: <FiCommand />,
          name: language.ONGOING_RIDES,
          path: NavLinks.ADMIN_RIDES_ONGOING,
          show: ifHasField("RIDES", "RIDES_LIST"),
        },
        {
          id: 2,
          icon: <FiCommand />,
          name: language.COMPLETED_RIDES,
          path: NavLinks.ADMIN_RIDES_COMPLETED,
          show: ifHasField("RIDES", "RIDES_LIST"),
        },
        {
          id: 3,
          icon: <FiCommand />,
          name: language.USERCANCELLED,
          path: NavLinks.ADMIN_RIDES_USER_CANCEL,
          show: ifHasField("RIDES", "RIDES_LIST"),
        },
        {
          id: 4,
          icon: <FiCommand />,
          name: language.DRIVERCANCELLED,
          path: NavLinks.ADMIN_RIDESD_DRIVER_CANCEL,
          show: ifHasField("RIDES", "RIDES_LIST"),
        },
        {
          id: 5,
          icon: <FiCommand />,
          name: language.EXPIRED_RIDES,
          path: NavLinks.ADMIN_RIDES_EXPIRED,
          show: ifHasField("RIDES", "RIDES_LIST"),
        },
        {
          id: 6,
          icon: <FiCommand />,
          name: language.SCHEDULED_RIDES,
          path: NavLinks.ADMIN_RIDES_SCHEDULED,
          show: ifHasField("RIDES", "RIDES_LIST"),
        },
        {
          id: 7,
          icon: <FiCommand />,
          name: language.NEW_INSTANT_RIDES_INITIATED,
          path: NavLinks.ADMIN_RIDES_NEW,
          show: ifHasField("RIDES", "RIDES_LIST"),
        },

        // {
        //   id: 9,
        //   icon: <FiCommand />,
        //   name: language.GUESTS_RIDE,
        //   path: NavLinks.ADMIN_RIDES_GUESTS,
        //   show: ifHasField("RIDES", "RIDES_LIST"),
        // },
        // {
        //   id: 10,
        //   icon: <FiCommand />,
        //   name: settings.siteTitle + " " + language.METERS,
        //   path: NavLinks.ADMIN_RIDES_METERS,
        //   show: ifHasField("RIDES", "RIDES_LIST"),
        // },
        {
          id: 8,
          icon: <FiCommand />,
          name: language.RIDES_WITH_PAYMENT_ISSUE,
          path: NavLinks.ADMIN_RIDES_ISSUE,
          show: ifHasField("RIDES", "RIDES_LIST"),
        },
        {
          id: 9,
          icon: <FiCommand />,
          name: language.SEARCH_RIDE,
          path: NavLinks.ADMIN_RIDES_SEARCH,
          show: ifHasField("RIDES", "RIDES_LIST"),
        },
        {
          id: 10,
          icon: <FiCommand />,
          name: language.BOOK_A_RIDE,
          path: NavLinks.ADMIN_DISPATCH,
          show: ifHasField("RIDES", "RIDES_LIST"),
        },
      ],
    },
    {
      id: 7,
      name: language.CORPORATES_SETUP,
      icon: <FiUser />,
      selected: false,
      show: ifHasView("CORPORATES") || ifHasView("RIDES"),
      children: [
        {
          id: 1,
          icon: <FiCommand />,
          name: language.CORPORATE,
          path: NavLinks.ADMIN_CORPORATE_CORPORATE_LIST,
          show: ifHasField("CORPORATE", "CORPORATE"),
        },

        {
          id: 2,
          icon: <FiCommand />,
          name: language.CORPORATE_RIDE,
          path: NavLinks.ADMIN_RIDES_CORPORATE,
          show: ifHasField("RIDES", "RIDES_LIST"),
        },
      ],
    },
    {
      id: 8,
      name: language.WALLET,
      icon: <FiCreditCard />,
      selected: false,
      show: ifHasView("ASSET"),
      children: [
        {
          id: 1,
          icon: <FiCommand />,
          name: language.AMOUNT_IN_WALLET,
          path: NavLinks.ADMIN_ASSET_WALLET_AMOUNT_USER,
          show: ifHasField("ASSET", "WALLETS"),
        },
        {
          id: 2,
          icon: <FiCommand />,
          name: language.WALLET_TRANSACTIONS,
          path: NavLinks.ADMIN_ASSET_WALLET,
          show: ifHasField("ASSET", "WALLETS"),
        },

        {
          id: 3,
          icon: <FiCommand />,
          name: language.WALLET_DUE,
          path: NavLinks.ADMIN_ASSET_DUE_FREEZE_AMOUNT,
          show: ifHasField("ASSET", "DUE_FREEZED"),
        },
        {
          id: 4,
          icon: <FiCommand />,
          name: language.REFUNDS,
          path: NavLinks.ADMIN_ASSET_REFUND,
          show: ifHasField("ASSET", "WALLETS"),
        },
        {
          id: 5,
          icon: <FiCommand />,
          name: language.WITHDRAWS,
          path: NavLinks.ADMIN_ASSET_WITHDRAW,
          show: ifHasField("ASSET", "WALLETS"),
        },
      ],
    },
    {
      id: 9,
      name: language.EARNINGS,
      icon: <AiOutlineMoneyCollect />,
      selected: false,
      show: ifHasView("SETUP"),
      path: NavLinks.ADMIN_ASSET_EARNINGS,
      children: [],
    },
    {
      id: 10,
      name: language.PAYMENT_SETUP,
      icon: <AiOutlinePayCircle />,
      selected: false,
      show:
        (admin.userType === "DEVELOPER" || admin.userType === "ADMIN") &&
        ifHasView("SETUP"),
      path: NavLinks.ADMIN_SETUP_PAYMENTS,
      children: [],
    },
    {
      id: 11,
      name: language.TOLL_SETUP,
      icon: <AiOutlineAntDesign />,
      selected: false,
      show: ifHasView("SETUP"),
      path: NavLinks.ADMIN_SETUP_TOLLS,
      children: [],
    },
    {
      id: 12,
      name: language.POPULAR_PLACES_SETUP,
      icon: <AiOutlineEnvironment />,
      selected: false,
      show: ifHasView("SETUP"),
      path: NavLinks.ADMIN_SETUP_POPULAR_PLACES,
      children: [],
    },
    {
      id: 13,
      name: language.OUTSTATION_PLACES_SETUP,
      icon: <AiOutlineExpandAlt />,
      selected: false,
      show: ifHasView("SETUP"),
      path: NavLinks.ADMIN_SETUP_POPULAR_PLACES,
      children: [],
    },
    {
      id: 14,
      name: language.AIRPORT_SETUP,
      icon: <GiCommercialAirplane />,
      selected: false,
      show: ifHasView("SETUP"),
      path: NavLinks.ADMIN_SETUP_AIRPORTS,
      children: [],
    },
    {
      id: 15,
      name: language.SERVICE_VIEW,
      icon: <AiOutlineFolderView />,
      selected: false,
      show: ifHasView("SETUP"),
      children: [
        {
          id: 1,
          icon: <FiCommand />,
          name: language.SATELLITE_VIEW,
          show: true,
          path: NavLinks.ADMIN_SATILLITE_VIEW,
        },
        {
          id: 2,
          icon: <FiCommand />,
          name: language.EAGLE_VIEW,
          show: true,
          path: NavLinks.ADMIN_EAGLE_VIEW,
        },
      ],
    },
    {
      id: 16,
      name: language.PROMOTIONS_AND_COUPONS,
      icon: <AiOutlinePercentage />,
      selected: false,
      show: ifHasView("REPORTS"),
      path: NavLinks.ADMIN_SETUP_COUPONS,
      children: [],
    },
    {
      id: 17,
      name: language.REFERRALS,
      icon: <FiVolume2 />,
      selected: false,
      show: ifHasView("SETUP"),
      children: [
        {
          id: 5,
          icon: <FiCommand />,
          name: language.REFERAL_SETUP,
          path: NavLinks.ADMIN_REFER_CONFIG,
          show:
            settings.isReferralNeeded &&
            ifHasField("SETTINGS", "REFER_EARN_CONFIG", "EDIT"),
        },
        {
          id: 7,
          icon: <FiCommand />,
          name: language.REFERRAL_LISTS,
          path: NavLinks.ADMIN_SETUP_REFER_USER_LIST,
          show: settings.isReferralNeeded && ifHasField("SETUP", "REFER_EARN"),
        },
      ],
    },
    {
      id: 18,
      name: language.SERVICE_EMAIL_TEMPLATES,
      icon: <AiOutlineMail />,
      selected: false,
      show: admin.userType === "DEVELOPER" && ifHasView("SETUP"),
      path: NavLinks.ADMIN_SETUP_EMAIL_TEMPLATES,
      children: [],
    },
    {
      id: 19,
      name: language.NOTIFICATIONS,
      icon: <AiOutlineNotification />,
      selected: false,
      show: ifHasView("SETUP"),
      children: [
        {
          id: 1,
          icon: <FiCommand />,
          name: language.SEND_NOTIFICATIONS,
          show: ifHasField("SETUP", "NOTIFICATION_TEMPLATES", "EDIT"),
          path: NavLinks.ADMIN_SETUP_NOTIFICATIONS_SEND_PROMOTIONS,
        },
        {
          id: 1,
          icon: <FiCommand />,
          name: language.NOTIFICATION_TEMPLATES,
          show: ifHasField("SETUP", "NOTIFICATION_TEMPLATES", "VIEW"),
          path: NavLinks.ADMIN_SETUP_NOTIFICATIONS,
        },
      ],
    },
    {
      id: 20,
      name: language.REPORTS_FEEDBACKS,
      icon: <FiVolume2 />,
      selected: false,
      show: ifHasView("SETUP"),
      path: NavLinks.ADMIN_REPORTS_FEEDBACK,
      children: [],
    },
    {
      id: 21,
      name: language.EMERGENCY_SOS,
      icon: <FiBell />,
      selected: false,
      show: ifHasView("SECURITY"),
      children: [
        {
          id: 3,
          icon: <FiCommand />,
          name: language.NEW_ATTENDED_REQUESTS,
          path: NavLinks.ADMIN_SECURITY_REQUESTS,
          show: ifHasField("SECURITY", "NEW_REQUESTS"),
        },
        {
          id: 4,
          icon: <FiCommand />,
          name: language.CLOSED_REQUESTS,
          path: NavLinks.ADMIN_SECURITY_CLOSED_REQUESTS,
          show: ifHasField("SECURITY", "CLOSED_REQUESTS"),
        },
        {
          id: 5,
          icon: <FiCommand />,
          name: language.MY_ACCEPTED_REQUESTS,
          path: NavLinks.ADMIN_SECURITY_MY_REQUESTS,
          show: ifHasField("SECURITY", "CLOSED_REQUESTS"),
        },
      ],
    },
    {
      id: 30,
      name: language.RESPONSE_OFFICE_SETUP,
      icon: <AiOutlineDeploymentUnit />,
      selected: false,
      show: ifHasView("RESPONSE_OFFICERS"),
      children: [
        {
          id: 4,
          icon: <FiCommand />,
          name: language.EMERGENCY_RESPONSE_OFFICE,
          path: NavLinks.ADMIN_ADMIN_RESPONSE_OFFICE_LIST,
          show: ifHasField("ADMIN", "EMERGENCY_RESPONSE_OFFICES"),
        },
        {
          id: 1,
          icon: <FiCommand />,
          name: language.RESPONSE_OFFICERS,
          path: NavLinks.ADMIN_RESPONSE_OFFICERS_LIST,
          show: ifHasField("RESPONSE_OFFICERS", "RESPONSE_OFFICERS_LIST"),
        },
      ],
    },
    {
      id: 32,
      name: language.SUBSCRIPTIONS,
      icon: <AiOutlineExpand />,
      selected: false,
      show: ifHasView("SECURITY"),
      children: [
        {
          id: 1,
          icon: <FiCommand />,
          name: language.SUBSCRIPTION_SERVICES,
          path: NavLinks.ADMIN_SECURITY_SERVICES,
          show: ifHasField("SECURITY", "SERVICES", "EDIT"),
        },
        {
          id: 2,
          icon: <FiCommand />,
          name: language.SUBSCRIPTIONS_SETUP,
          path: NavLinks.ADMIN_SECURITY_SUBSCRIPTIONS,
          show: ifHasField("SECURITY", "SUBSCRIPTIONS"),
        },
      ],
    },
  ]);
  const [searchFields, setSearchFields] = useState([]);
  const [searchText, setSearchText] = useState("");

  const searchNavBar = (e) => {
    setSearchText(e.target.value);
    setSelector(0);
    let allFields = [];
    fields
      .filter((main) => main.show === true)
      .map((field) =>
        field.children
          .filter((ch) => ch.show === true)
          .map((ch) => {
            allFields.push({
              ...ch,
              show: `${field.name.toUpperCase()} / ${ch.name}`,
            });
          })
      );

    const filtered = allFields.filter((field) =>
      field.show.toUpperCase().includes(e.target.value.toUpperCase())
    );
    setSearchFields(filtered);
  };

  const goToFirstResult = () => {
    let allFields = [];
    fields
      .filter((main) => main.show === true)
      .map((field) =>
        field.children
          .filter((ch) => ch.show === true)
          .map((ch) => {
            allFields.push({
              ...ch,
              show: `${field.name.toUpperCase()} / ${ch.name}`,
            });
          })
      );

    const filtered = allFields.filter((field) =>
      field.show.toUpperCase().includes(searchText.toUpperCase())
    );
    if (filtered.length > 0) {
      history.push(filtered[selector].path);
      fadeOutNav();
    }
  };

  const fadeOutNav = () => {
    gsap.to(".fadeOutTextBox", 0.25, { opacity: 0 });
    gsap.fromTo(".fadeOut", 0.25, { opacity: 1 }, { opacity: 0, delay: 0 });
    setTimeout(() => closeNavBar(), 0);
  };

  const eventListen = (e) => {
    if (e.key === "ArrowDown") {
      setSelector((selector) =>
        searchFields.length - 1 === selector ? selector : selector + 1
      );
    }
    if (e.key === "ArrowUp") {
      setSelector((selector) => (selector === 0 ? 0 : selector - 1));
    }
  };

  useEffect(() => {
    searchRef.current.focus();
    document.addEventListener("keydown", eventListen);
    gsap.fromTo(".fade", 0.3, { opacity: 0, x: 5 }, { opacity: 1, x: 0, stagger: 0.05 });
    gsap.fromTo(".fade1", 0.3, { opacity: 0 }, { opacity: 0.5 });
    return () => document.removeEventListener("keydown", eventListen);
  }, []);

  const toggleSelect = (id) => {
    setFields(
      fields.map((f) =>
        id === f.id
          ? {
              id: f.id,
              name: f.name,
              icon: f.icon,
              selected: true,
              path: f?.path ?? false,
              show: f.show,
              children: f.children,
            }
          : {
              id: f.id,
              name: f.name,
              icon: f.icon,
              selected: false,
              path: f?.path ?? false,
              show: f.show,
              children: f.children,
            }
      )
    );
  };

  React.useEffect(() => {
    toggleSelect(selected);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selected]);

  return (
    <>
      {/* // Large Screens */}
      <h1 className="fixed flex inset-0 z-50">
        {/* background color  */}
        <div
          onClick={() => fadeOutNav()}
          className="fadeOut opacity-0 fade bg-gray-900 fixed inset-0"
        ></div>
        {/* Main area  */}
        <div className="fade opacity-0 ml-8 mt-10 fade z-30 flex flex-col">
          <div className="fadeOutTextBox fade rounded-full text-white bg-blue-800 dark:bg-gray-800 flex items-center">
            <FiSearch className="text-xl text-white mx-3" />
            <input
              autoComplete="off"
              ref={searchRef}
              onKeyDown={(e) =>
                (e.key === "Enter" && searchText.length > 0 && goToFirstResult()) ||
                (e.key === "Escape" && fadeOutNav())
              }
              type="text"
              onChange={searchNavBar}
              className="fadeOutTextBox placeholder-white text-white rounded-full bg-blue-800 dark:bg-gray-800 py-3 focus:outline-none"
              name="search"
              id=""
              placeholder={language.SEARCH}
            />
          </div>
          {/* Search Results  */}
          {searchRef != null && searchRef.current.value != 0 && (
            <>
              <div className="overflow-y-scroll text-white text-md flex flex-col z-30 fadeOutTextBox">
                {searchFields.map((field, idx) => (
                  <Link
                    onClick={fadeOutNav}
                    to={field.path}
                    key={field.key}
                    className="cursor-pointer flex items-center my-1 px-2 py-2"
                  >
                    {/* {field.icon} */}
                    <h1
                      className={`mx-3 opacity-50 ${idx === selector && "opacity-100"}`}
                    >
                      {field.show}
                    </h1>
                  </Link>
                ))}
              </div>
            </>
          )}
          {/* Left MEnu Item  */}
          <div
            className="mt-2  overflow-y-scroll customScrollBar"
            style={{ paddingBottom: 25, minWidth: 310 }}
          >
            {searchText.length === 0 &&
              fields
                .filter((f) => f.show === true)
                .map((f) => (
                  <Fields
                    fadeOutNav={fadeOutNav}
                    field={f}
                    toggleSelect={(id) => setSelected(id)}
                  />
                ))}
          </div>
        </div>
        {/* Right menu items  */}
        {searchText.length === 0 && fields.find((fa) => fa.selected === true) && (
          <SideFields
            fadeOutNav={fadeOutNav}
            children={fields.find((fa) => fa.selected === true).children}
          />
        )}
        <div className="flex-1">
          <div className="flex justify-end">
            {/* Close icon */}
            <div
              onClick={fadeOutNav}
              className="fadeOut opacity-0 fade bg-blue-800 dark:bg-gray-800 p-3 right-0 mt-14 m-10 rounded-full text-white"
            >
              <AiOutlineClose className="cursor-pointer text-2xl text-gray-200" />
            </div>
          </div>
        </div>
      </h1>
    </>
  );
};

export default NavBar;
