import { useFormik } from "formik";
import React from "react";
import { FiMessageCircle } from "react-icons/fi";
import * as yup from "yup";
import gsap from "gsap";
import axios from "axios";

import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { Section } from "../../../../../components/common/Section";
import { TextField } from "../../../../../components/common/TextField";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import { ToggleButton } from "../../../../../components/common/ToggleButton";
import { PopUp } from "../../../../../components/common/PopUp";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";

import useLanguage from "../../../../../hooks/useLanguage";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";

export default function Index() {
  // STATES
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);
  const [loading, setLoading] = React.useState(true);

  // HOOKS
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { parseError } = useUtils();

  const validationSchema = yup.object().shape({
    account_sid: yup.string().required(language.REQUIRED),
    auth_token: yup.string().required(language.REQUIRED),
    twilio_number: yup.number().required(language.REQUIRED),
    mode: yup.number().required(language.REQUIRED),
  });

  const submit = async (e) => {
    setBtnLoading(true);
    try {
      await axios.post(
        A.HOST + A.ADMIN_SETTINSG_SMS_UPDATE,
        {
          id: formik.values.id,
          data: {
            accountSid: e.account_sid,
            authToken: e.auth_token,
            twilioNumber: e.twilio_number,
            mode: e.mode === 1 ? U.PRODUCTION : U.DEVELOPMENT,
          },
        },
        header
      );
      setPop({ title: language.UPDATE_SUCCESS, type: "success" });
    } catch (err) {
      authFailure(err);
      setPop({ title: parseError(err), type: "error" });
    }
    setBtnLoading(false);
  };

  const formik = useFormik({
    initialValues: {
      account_sid: "",
      auth_token: "",
      twilio_number: "",
      mode: 1,
    },
    enableReinitialize: true,
    validateOnChange: false,
    validationSchema,
    onSubmit: submit,
  });

  React.useEffect(() => {
    const fetchSMS = async () => {
      window.scrollTo(0, 0);
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_SETTINGS_SMS_READ, {}, header);
        formik.setFieldValue("id", data._id);
        formik.setFieldValue("account_sid", data.data.accountSid);
        formik.setFieldValue("auth_token", data.data.authToken);
        formik.setFieldValue("twilio_number", data.data.twilioNumber);
        formik.setFieldValue("mode", data.data.mode === U.PRODUCTION ? 1 : 0);
        setLoading(false);
        gsap.fromTo(".smsSettings", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
      } catch (err) {
        authFailure(err);
      }
    };
    fetchSMS();
  }, []);

  return loading === false ? (
    <FormWrapper
      animate="smsSettings"
      submitBtn={true}
      btnLoading={btnLoading}
      icon={<FiMessageCircle />}
      submit={formik.handleSubmit}
      width="3/4"
      bread={[{ id: 1, title: language.SETTINGS }]}
      title={language.CONFIG_SMS_SETTINGS}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <Section animate="smsSettings">
        <FieldWrapper animate="smsSettings" title={language.ACCOUNT_SID}>
          <TextField
            change={(e) => formik.setFieldValue("account_sid", e)}
            error={formik.errors.account_sid}
            value={formik.values.account_sid}
          />
        </FieldWrapper>
        <FieldWrapper animate="smsSettings" title={language.TWILIO_NUMBER}>
          <TextField
            type={"number"}
            change={(e) => formik.setFieldValue("twilio_number", e)}
            error={formik.errors.twilio_number}
            value={formik.values.twilio_number}
          />
        </FieldWrapper>
      </Section>
      <Section animate="smsSettings">
        <FieldWrapper animate="smsSettings" title={language.AUTH_TOKEN}>
          <TextField
            type="password"
            change={(e) => formik.setFieldValue("auth_token", e)}
            error={formik.errors.auth_token}
            value={formik.values.auth_token}
          />
        </FieldWrapper>
        <FieldWrapper animate="smsSettings" title={language.PRODUCTION}>
          <ToggleButton change={(e) => formik.setFieldValue("mode", e, true)} defaultValue={formik.values.mode} />
        </FieldWrapper>
      </Section>
    </FormWrapper>
  ) : (
    <SmallLoader />
  );
}
