import gsap from "gsap/gsap-core";
import React from "react";
import { FiPhone } from "react-icons/fi";
import * as yup from "yup";
import axios from "axios";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";

import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { Section } from "../../../../../components/common/Section";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import { TextField } from "../../../../../components/common/TextField";
import useLanguage from "../../../../../hooks/useLanguage";
import useImage from "../../../../../hooks/useImage";
import { ToggleButton } from "../../../../../components/common/ToggleButton";
import { useFormik } from "formik";
import useAdmin from "../../../../../hooks/useAdmin";
import { PopUp } from "../../../../../components/common/PopUp";
import useUtils from "../../../../../hooks/useUtils";
import Flex from "../../../../../components/common/Flex";
import { Border } from "../../../../../components/common/Border";
import { Heading } from "../../../../../components/common/Heading";
import { TextArea } from "../../../../../components/common/TextArea";
import { FileUpload } from "../../../../../components/common/FileUpload";

export default function Index() {
  const [btnLoading, setBtnLoading] = React.useState(false);
  const { language } = useLanguage();
  const [loading, setLoading] = React.useState(true);
  const [done, setDone] = React.useState(false);
  const { parseError } = useUtils();
  const { imageUrl, isBase64, compressImage } = useImage();
  const { header, authFailure } = useAdmin();
  const [popup, setPop] = React.useState(null);
  const [id, setId] = React.useState("");

  const validationSchema = yup.object().shape({
    forUser: yup.object({
      showText: yup
        .string()
        .min(2, language.MIN + " 2")
        .required(language.REQUIRED),
      messageText: yup
        .string()
        .min(2, language.MIN + " 2")
        .required(language.REQUIRED),
      amountToInviter: yup
        .number()
        .min(0, language.MIN + " 0")
        .required(language.REQUIRED),
      amountToJoiner: yup
        .number()
        .min(0, language.MIN + " 0")
        .required(language.REQUIRED),
      image: yup
        .string()
        .min(2, language.MIN + " 2")
        .required(language.REQUIRED),
      description: yup
        .string()
        .min(2, language.MIN + " 2")
        .required(language.REQUIRED),
      rideCount: yup
        .number()
        .min(0, language.MIN + " 0")
        .max(100, language.MAX + " 100")
        .required(language.REQUIRED),
    }),
    forProfessional: yup.object({
      showText: yup
        .string()
        .min(2, language.MIN + " 2")
        .required(language.REQUIRED),
      messageText: yup
        .string()
        .min(2, language.MIN + " 2")
        .required(language.REQUIRED),
      amountToInviter: yup
        .number()
        .min(0, language.MIN + " 0")
        .required(language.REQUIRED),
      amountToJoiner: yup
        .number()
        .min(0, language.MIN + " 0")
        .required(language.REQUIRED),
      image: yup
        .string()
        .min(2, language.MIN + " 2")
        .required(language.REQUIRED),
      description: yup
        .string()
        .min(2, language.MIN + " 2")
        .required(language.REQUIRED),
      rideCount: yup
        .number()
        .min(0, language.MIN + " 0")
        .max(100, language.MAX + " 100")
        .required(language.REQUIRED),
    }),
  });

  const [refer, setRefer] = React.useState({
    forUser: {
      showText: "",
      messageText: "",
      amountToInviter: 0,
      amountToJoiner: 0,
      image: "",
      description: "",
      rideCount: 0,
    },
    forProfessional: {
      showText: "",
      messageText: "",
      amountToInviter: 0,
      amountToJoiner: 0,
      image: "",
      description: "",
      rideCount: 0,
    },
  });

  const submit = async (e) => {
    setBtnLoading(true);
    // For User
    e.forUser.amountToInviter = parseInt(e.forUser.amountToInviter);
    e.forUser.amountToJoiner = parseInt(e.forUser.amountToJoiner);
    e.forUser.rideCount = parseInt(e.forUser.rideCount);
    // For Professional
    e.forProfessional.amountToInviter = parseInt(e.forProfessional.amountToInviter);
    e.forProfessional.amountToJoiner = parseInt(e.forProfessional.amountToJoiner);
    e.forProfessional.rideCount = parseInt(e.forProfessional.rideCount);

    if (e.forUser && e.forUser.image && isBase64(e.forUser.image)) {
      const compressedImage = await compressImage(e.forUser.image);
      const formData = new FormData();
      formData.append("for", "user");
      formData.append("image", compressedImage);
      const { data } = await axios.post(
        A.HOST + A.ADMIN_REFER_CONFIG_IMAGE_UPLOAD,
        formData,
        header
      );
      e.forUser.image = data.image;
    }
    if (
      e.forProfessional &&
      e.forProfessional.image &&
      isBase64(e.forProfessional.image)
    ) {
      const compressedImage = await compressImage(e.forProfessional.image);
      const formData = new FormData();
      formData.append("for", "professional");
      formData.append("image", compressedImage);
      const { data } = await axios.post(
        A.HOST + A.ADMIN_REFER_CONFIG_IMAGE_UPLOAD,
        formData,
        header
      );
      e.forProfessional.image = data.image;
    }
    try {
      await axios.post(
        A.HOST + A.ADMIN_REFER_CONFIG_UPDATE,
        {
          id,
          ...e,
        },
        header
      );
      setBtnLoading(false);
      setPop({ title: language.UPDATE_SUCCESS, type: "success" });
    } catch (err) {
      authFailure(err);
      setPop({ title: parseError(err), type: "error" });
    }
    setBtnLoading(false);
  };

  const formik = useFormik({
    initialValues: refer,
    enableReinitialize: true,
    validateOnChange: false,
    validationSchema,
    onSubmit: submit,
  });

  React.useEffect(() => {
    const fetchConfig = async () => {
      window.scrollTo(0, 0);
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_REFER_CONFIG, {}, header);
        setId(data._id);
        setRefer(data.data);
        setLoading(false);
        gsap.fromTo(".referSettings", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
      } catch (err) {
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
      setLoading(false);
    };
    fetchConfig();
  }, []);

  return loading === false ? (
    <FormWrapper
      submit={formik.handleSubmit}
      done={done}
      width="5/5"
      animate="referSettings"
      submitBtn={true}
      bread={[]}
      submitBtnText={language}
      btnLoading={btnLoading}
      icon={<FiPhone />}
      title={language.REFERAL_SETUP}
    >
      {popup != null && (
        <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />
      )}
      <Section>
        <Border>
          <Heading title={language.USER} />
          <Flex align={false}>
            <Section padding={false}>
              <FieldWrapper animate="referSettings" title={language.SHOW_TEXT}>
                <TextField
                  error={formik.errors.forUser && formik.errors.forUser.showText}
                  change={(e) => formik.setFieldValue("forUser.showText", e, true)}
                  value={formik.values.forUser && formik.values?.forUser?.showText}
                />
              </FieldWrapper>
              <FieldWrapper animate="referSettings" title={language.MESSAGE_TEXT}>
                <TextField
                  error={formik.errors.forUser && formik.errors.forUser.messageText}
                  change={(e) => formik.setFieldValue("forUser.messageText", e, true)}
                  value={formik.values.forUser && formik.values?.forUser?.messageText}
                />
              </FieldWrapper>
              <FieldWrapper animate="referSettings" title={language.DESCRIPTION}>
                <TextArea
                  rows={12}
                  error={formik.errors.forUser && formik.errors.forUser.description}
                  change={(e) => formik.setFieldValue("forUser.description", e, true)}
                  value={formik.values.forUser && formik.values?.forUser?.description}
                />
              </FieldWrapper>
            </Section>
            <Section padding={false}>
              <FieldWrapper animate="referSettings" title={language.INVITE_AMOUNT}>
                <TextField
                  type="number"
                  error={formik.errors.forUser && formik.errors.forUser.amountToInviter}
                  change={(e) => formik.setFieldValue("forUser.amountToInviter", e, true)}
                  value={formik.values.forUser && formik.values?.forUser?.amountToInviter}
                />
              </FieldWrapper>
              <FieldWrapper animate="referSettings" title={language.JOIN_AMOUNT}>
                <TextField
                  type="number"
                  error={formik.errors.forUser && formik.errors.forUser.amountToJoiner}
                  change={(e) => formik.setFieldValue("forUser.amountToJoiner", e, true)}
                  value={formik.values.forUser && formik.values?.forUser?.amountToJoiner}
                />
              </FieldWrapper>
              <FieldWrapper animate="referSettings" title={language.RIDE_COUNT}>
                <TextField
                  type="number"
                  error={formik.errors.forUser && formik.errors.forUser.rideCount}
                  change={(e) => formik.setFieldValue("forUser.rideCount", e)}
                  value={formik.values.forUser && formik.values?.forUser?.rideCount}
                />
              </FieldWrapper>
              <FieldWrapper animate="referSettings" title={language.IMAGE}>
                <FileUpload
                  crop={false}
                  defaultValue={imageUrl(
                    formik.values.forUser && formik.values?.forUser?.image
                  )}
                  change={(e) => formik.setFieldValue("forUser.image", e)}
                />
              </FieldWrapper>
            </Section>
          </Flex>
        </Border>
      </Section>
      <Section>
        <Border>
          <Heading title={language.PROFESSIONAL} />
          <Flex align={false}>
            <Section padding={false}>
              <FieldWrapper animate="referSettings" title={language.SHOW_TEXT}>
                <TextField
                  error={
                    formik.errors.forProfessional &&
                    formik.errors.forProfessional.showText
                  }
                  change={(e) =>
                    formik.setFieldValue("forProfessional.showText", e, true)
                  }
                  value={
                    formik.values?.forProfessional &&
                    formik.values?.forProfessional.showText
                  }
                />
              </FieldWrapper>
              <FieldWrapper animate="referSettings" title={language.MESSAGE_TEXT}>
                <TextField
                  error={
                    formik.errors.forProfessional &&
                    formik.errors.forProfessional.messageText
                  }
                  change={(e) =>
                    formik.setFieldValue("forProfessional.messageText", e, true)
                  }
                  value={
                    formik.values?.forProfessional &&
                    formik.values?.forProfessional.messageText
                  }
                />
              </FieldWrapper>
              <FieldWrapper animate="referSettings" title={language.DESCRIPTION}>
                <TextArea
                  rows={12}
                  error={
                    formik.errors.forProfessional &&
                    formik.errors.forProfessional.description
                  }
                  change={(e) =>
                    formik.setFieldValue("forProfessional.description", e, true)
                  }
                  value={
                    formik.values?.forProfessional &&
                    formik.values?.forProfessional.description
                  }
                />
              </FieldWrapper>
            </Section>
            <Section padding={false}>
              <FieldWrapper animate="referSettings" title={language.INVITE_AMOUNT}>
                <TextField
                  type="number"
                  error={
                    formik.errors.forProfessional &&
                    formik.errors.forProfessional.amountToInviter
                  }
                  change={(e) =>
                    formik.setFieldValue("forProfessional.amountToInviter", e, true)
                  }
                  value={
                    formik.values?.forProfessional &&
                    formik.values?.forProfessional.amountToInviter
                  }
                />
              </FieldWrapper>
              <FieldWrapper animate="referSettings" title={language.JOIN_AMOUNT}>
                <TextField
                  type="number"
                  error={
                    formik.errors.forProfessional &&
                    formik.errors.forProfessional.amountToJoiner
                  }
                  change={(e) =>
                    formik.setFieldValue("forProfessional.amountToJoiner", e, true)
                  }
                  value={
                    formik.values?.forProfessional &&
                    formik.values?.forProfessional.amountToJoiner
                  }
                />
              </FieldWrapper>
              <FieldWrapper animate="referSettings" title={language.RIDE_COUNT}>
                <TextField
                  type="number"
                  error={
                    formik.errors.forProfessional &&
                    formik.errors.forProfessional.rideCount
                  }
                  change={(e) => formik.setFieldValue("forProfessional.rideCount", e)}
                  value={
                    formik.values.forProfessional &&
                    formik.values?.forProfessional?.rideCount
                  }
                />
              </FieldWrapper>
              <FieldWrapper animate="referSettings" title={language.IMAGE}>
                <FileUpload
                  crop={false}
                  defaultValue={imageUrl(
                    formik.values?.forProfessional && formik.values?.forProfessional.image
                  )}
                  change={(e) => formik.setFieldValue("forProfessional.image", e)}
                />
              </FieldWrapper>
            </Section>
          </Flex>
        </Border>
      </Section>
    </FormWrapper>
  ) : (
    <SmallLoader></SmallLoader>
  );
}
