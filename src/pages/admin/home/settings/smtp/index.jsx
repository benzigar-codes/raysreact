import gsap from "gsap/gsap-core";
import React from "react";
import { FiPhone } from "react-icons/fi";
import * as yup from "yup";
import axios from "axios";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";

import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { Section } from "../../../../../components/common/Section";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import { TextField } from "../../../../../components/common/TextField";
import useLanguage from "../../../../../hooks/useLanguage";
import { ToggleButton } from "../../../../../components/common/ToggleButton";
import { useFormik } from "formik";
import useAdmin from "../../../../../hooks/useAdmin";
import { PopUp } from "../../../../../components/common/PopUp";
import useUtils from "../../../../../hooks/useUtils";

export default function Index() {
  const [btnLoading, setBtnLoading] = React.useState(false);
  const { language } = useLanguage();
  const [loading, setLoading] = React.useState(true);
  const [done, setDone] = React.useState(false);
  const { parseError } = useUtils();
  const { header, authFailure } = useAdmin();
  const [popup, setPop] = React.useState(null);

  const validationSchema = yup.object().shape({
    smtp_host: yup.string().min(2, "The minimum is 2").required(language.REQUIRED),
    smtp_port: yup.string().required(language.REQUIRED),
    smtp_client: yup.string().required(language.REQUIRED),
    smtp_secret: yup.string().required(language.REQUIRED),
    mode: yup.number().required(),
  });

  const [smtp, setSmtp] = React.useState({
    smtp_host: "",
    smtp_port: "",
    smtp_client: "",
    smtp_secret: "",
    mode: 0,
  });

  const submit = async (e) => {
    setBtnLoading(true);
    try {
      await axios.post(
        A.HOST + A.ADMIN_SETTINGS_SMTP_UPDATE,
        {
          id: formik.values.id,
          data: {
            host: e.smtp_host,
            port: e.smtp_port,
            client: e.smtp_client,
            secret: e.smtp_secret,
            mode: e.mode === 1 ? U.PRODUCTION : U.DEVELOPMENT,
          },
        },
        header
      );
      setBtnLoading(false);
      setPop({ title: language.UPDATE_SUCCESS, type: "success" });
    } catch (err) {
      authFailure(err);
      setPop({ title: parseError(err), type: "error" });
    }
    setBtnLoading(false);
  };

  const formik = useFormik({
    initialValues: smtp,
    enableReinitialize: true,
    validateOnChange: false,
    validationSchema,
    onSubmit: submit,
  });

  React.useEffect(() => {
    const fetchSMTP = async () => {
      window.scrollTo(0, 0);
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_SETTINGS_SMTP_READ, {}, header);
        formik.setFieldValue("id", data._id);
        formik.setFieldValue("smtp_host", data.data.host);
        formik.setFieldValue("smtp_port", data.data.port);
        formik.setFieldValue("smtp_client", data.data.client);
        formik.setFieldValue("smtp_secret", data.data.secret);
        formik.setFieldValue("mode", data.data.mode === U.PRODUCTION ? 1 : 0);
        setLoading(false);
        gsap.fromTo(".smtpSettings", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
      } catch (err) {
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
      setLoading(false);
    };
    fetchSMTP();
  }, []);

  return loading === false ? (
    <FormWrapper
      submit={formik.handleSubmit}
      done={done}
      width="2/4"
      animate="smtpSettings"
      submitBtn={true}
      bread={[{ id: 1, title: language.SETTINGS }]}
      submitBtnText={language}
      btnLoading={btnLoading}
      icon={<FiPhone />}
      title={language.SMTP}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <Section>
        {/* SMTP HOST  */}
        <FieldWrapper animate="smtpSettings" title={language.SMTP_HOST}>
          <TextField
            error={formik.errors.smtp_host}
            change={(e) => formik.setFieldValue("smtp_host", e, true)}
            value={formik.values.smtp_host}
          />
        </FieldWrapper>
        {/* CLIENT ID  */}
        <FieldWrapper animate="smtpSettings" title={language.CLIENTID}>
          <TextField
            name="smtp_client"
            error={formik.errors.smtp_client}
            change={(e) => formik.setFieldValue("smtp_client", e, true)}
            value={formik.values.smtp_client}
          />
        </FieldWrapper>
        <FieldWrapper animate="smtpSettings" title={language.PRODUCTION}>
          <ToggleButton change={(e) => formik.setFieldValue("mode", e, true)} defaultValue={formik.values.mode} />
        </FieldWrapper>
      </Section>
      <Section>
        {/* SMTP PORT  */}
        <FieldWrapper animate="smtpSettings" title={language.SMTP_PORT}>
          <TextField
            name="smtp_port"
            error={formik.errors.smtp_port}
            change={(e) => formik.setFieldValue("smtp_port", e, true)}
            value={formik.values.smtp_port}
          />
        </FieldWrapper>
        {/* Secret  */}
        <FieldWrapper animate="smtpSettings" title={language.SECRET}>
          <TextField
            type="password"
            name="smtp_secret"
            error={formik.errors.smtp_secret}
            change={(e) => formik.setFieldValue("smtp_secret", e, true)}
            value={formik.values.smtp_secret}
          />
        </FieldWrapper>
      </Section>
    </FormWrapper>
  ) : (
    <SmallLoader></SmallLoader>
  );
}
