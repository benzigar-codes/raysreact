// DEPENDENCIES
import React from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import gsap from "gsap";
import { FiCheck, FiEdit, FiMinus, FiSave, FiX } from "react-icons/fi";
import axios from "axios";
import FormData from "form-data";
import timezones from "../../../../../utils/timezones.json";

// API JSON
import A from "../../../../../utils/API.js";
import Currency from "../../../../../utils/currency.json";
import CountryCodes from "../../../../../utils/countryCodes.json";

// HOOKS
import useLanguage from "../../../../../hooks/useLanguage";
import useImage from "../../../../../hooks/useImage";
import useSettings from "../../../../../hooks/useSettings";

// COMPONENTS
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { Section } from "../../../../../components/common/Section";
import { TextField } from "../../../../../components/common/TextField";
import Flex from "../../../../../components/common/Flex";
import { Heading } from "../../../../../components/common/Heading";
import { FileUpload } from "../../../../../components/common/FileUpload";
import { Button } from "../../../../../components/common/Button";
import { MultiSelect } from "../../../../../components/common/MultiSelect";
import { ToggleButton } from "../../../../../components/common/ToggleButton";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import { PopUp } from "../../../../../components/common/PopUp";
import { Border } from "../../../../../components/common/Border";
import useAdmin from "../../../../../hooks/useAdmin";
import { DropdownNormal } from "../../../../../components/common/DropDownNormal";
import { CountryCodesPicker } from "../../../../../components/common/CountryCodesPicker";
import { DropDownSearch } from "../../../../../components/common/DropDownSearch.jsx";
import useUtils from "../../../../../hooks/useUtils.jsx";

const ProtectedField = ({ Component = TextField, hidden = false, ...rest }) => {
  const { admin } = useAdmin();
  return admin.userType === "DEVELOPER" ? (
    <Component {...rest} />
  ) : Component === ToggleButton ? (
    <ToggleButton
      {...rest}
      value={hidden ? "***************************" : rest.defaultValue ?? rest.value}
      change={() => {}}
    />
  ) : (
    <TextField
      editable={false}
      {...rest}
      value={hidden ? "***************************" : rest.defaultValue ?? rest.value}
      change={() => {}}
    />
  );
};

export default function Index() {
  // HOOKS
  const { language } = useLanguage();
  const { compressImage, imageUrl, isBase64 } = useImage();
  const { token, header, authFailure, admin } = useAdmin();
  const { fetchSettings } = useSettings();
  const { paymentSectionFields } = useUtils();
  // STATES
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);
  const [loading, setLoading] = React.useState(true);
  const [keys, setKeys] = React.useState({
    fcm: false,
    map: {
      web: false,
      android: false,
      ios: false,
    },
    space: false,
    version: false,
  });
  // DOCUMENT TITLE
  document.title = language.GENERAL + " " + language.SETTINGS;
  // VALIDATION SCHEMA
  const validationSchema = yup.object().shape({
    billingDays: yup.number().required(language.REQUIRED),
    bookingPrefix: yup.string().required(language.REQUIRED),
    darkLogo: yup.string().required(language.REQUIRED),
    driverMinAge: yup.number().required(language.REQUIRED),
    paymentSection: yup.string().required(language.REQUIRED),
    distanceCalculateType: yup.string().required(language.REQUIRED),
    minimumWalletAmountToOnline: yup
      .number()
      .min(1, language.MIN + " " + 1)
      .required(language.REQUIRED),
    driverRequestTimeout: yup.number().required(language.REQUIRED),
    emailAddress: yup.string().required(language.REQUIRED).email(language.INVALID_EMAIL),
    favicon: yup.string().required(language.REQUIRED),
    firebaseAdmin: yup.object().shape({
      androidUser: yup.string().required(language.REQUIRED),
      iosUser: yup.string().required(language.REQUIRED),
      androidDriver: yup.string().required(language.REQUIRED),
      iosDriver: yup.string().required(language.REQUIRED),
    }),
    lightLogo: yup.string().required(language.REQUIRED),
    mapApi: yup.object().shape({
      web: yup.string().required(language.REQUIRED),
      android: yup.string().required(language.REQUIRED),
      ios: yup.string().required(language.REQUIRED),
    }),
    spaces: yup.object().shape({
      spacesKey: yup.string().required(language.REQUIRED),
      spacesSecret: yup.string().required(language.REQUIRED),
      spacesEndpoint: yup.string().required(language.REQUIRED),
      spacesBaseUrl: yup.string().required(language.REQUIRED),
      spacesObjectName: yup.string().required(language.REQUIRED),
      spacesBucketName: yup.string().required(language.REQUIRED),
    }),
    mobileLogo: yup.string().required(language.REQUIRED),
    pageViewLimits: yup.array().of(yup.number()),
    requestDistance: yup.number().required(language.REQUIRED),
    responceOfficerMinAge: yup.number().required(language.REQUIRED),
    retryRequestDistance: yup.number().required(language.REQUIRED),
    // isRerouteEnable: yup.string().required(language.REQUIRED),
    defaultCountryCode: yup.string().required(language.REQUIRED),
    isTollToDriver: yup.string().required(language.REQUIRED),
    professionalPenaltyStatus: yup.string().required(language.REQUIRED),
    isWaitingEnable: yup.string().required(language.REQUIRED),
    isProduction: yup.string().required(language.REQUIRED),
    isOtpNeeded: yup.string().required(language.REQUIRED),
    cronTimeZone: yup.string().required(language.REQUIRED),
    approxCalculationDistance: yup.number().required(language.REQUIRED),
    bookingAlgorithm: yup.string().required(language.REQUIRED),
    approxCalculationTime: yup
      .number()
      .min(1, language.MIN + " 1")
      .required(language.REQUIRED),
    timeDurationThershold: yup
      .number()
      .min(1, language.MIN + " 1")
      .required(language.REQUIRED),
    turfDistanceThershold: yup
      .number()
      .min(1, language.MIN + " 1")
      .required(language.REQUIRED),
    androidVersionKey: yup
      .number()
      .min(1, language.MIN + " 1")
      .required(language.REQUIRED),
    nextProfessionalThreshold: yup
      .number()
      .min(1, language.MIN + " 1")
      .required(language.REQUIRED),
    adminAssignThreshold: yup
      .number()
      .min(1, language.MIN + " 1")
      .required(language.REQUIRED),
    iosVersionKey: yup
      .number()
      .min(1, language.MIN + " 1")
      .required(language.REQUIRED),
    iosDriverVersionKey: yup
      .number()
      .min(1, language.MIN + " 1")
      .required(language.REQUIRED),
    iosUserVersionKey: yup
      .number()
      .min(1, language.MIN + " 1")
      .required(language.REQUIRED),
    documentExpireThreshold: yup
      .number()
      .min(0, language.MIN + " 0")
      .required(language.REQUIRED),
    androidUserVersionKey: yup
      .number()
      .min(1, language.MIN + " 1")
      .required(language.REQUIRED),
    androidDriverVersionKey: yup
      .number()
      .min(1, language.MIN + " 1")
      .required(language.REQUIRED),
    stopLimit: yup
      .number()
      .min(0, language.MIN + " 1")
      .required(language.REQUIRED),
    siteTitle: yup.string().required(language.REQUIRED),
    isManualMeterNeeded: yup.string().required(language.REQUIRED),
    siteUrl: yup.string().required(language.REQUIRED),
    tipStatus: yup.string().required(language.REQUIRED),
    isTollNeeded: yup.string().required(language.REQUIRED),
    emailConfigurationEnable: yup.string().required(language.REQUIRED),
    smsConfigurationEnable: yup.string().required(language.REQUIRED),
    isTollManual: yup.string().required(language.REQUIRED),
    isVinNeeded: yup.string().required(language.REQUIRED),
    isMapOptimized: yup.string().required(language.REQUIRED),
    isUserReroute: yup.string().required(language.REQUIRED),
    isDriverReroute: yup.string().required(language.REQUIRED),
    siteStatus: yup.string().required(language.REQUIRED),
    isReferralNeeded: yup.string().required(language.REQUIRED),
    isAddressChangeEnable: yup.string().required(language.REQUIRED),
    isCancelToNextProfessional: yup.string().required(language.REQUIRED),
    isTailRideNeeded: yup.string().required(language.REQUIRED),
    isSocketPolling: yup.string().required(language.REQUIRED),
    isCountryCodeNeeded: yup.string().required(language.REQUIRED),
    defaultDialCode: yup.string().required(language.REQUIRED),
    appUpdateStatus: yup.string().required(language.REQUIRED),
    driverAppUpdateStatus: yup.string().required(language.REQUIRED),
    androidUserAppUpdateStatus: yup.string().required(language.REQUIRED),
    userAppUpdateStatus: yup.string().required(language.REQUIRED),
    androidDriverAppUpdateStatus: yup.string().required(language.REQUIRED),
    iosDriverAppUpdateStatus: yup.string().required(language.REQUIRED),
    iosUserAppUpdateStatus: yup.string().required(language.REQUIRED),
    lastPriorityStatus: yup.string().required(language.REQUIRED),
    lastPriorityCount: yup
      .number()
      .min(0, language.MIN + " " + 0)
      .required(language.REQUIRED),

    walletSendMinAmount: yup
      .number()
      .min(0, language.MIN + " " + 0)
      .required(language.REQUIRED),
    walletSendMaxAmount: yup
      .number()
      .min(0, language.MIN + " " + 0)
      .required(language.REQUIRED),
    walletMinAmountThreshold: yup
      .number()
      .min(0, language.MIN + " " + 0)
      .required(language.REQUIRED),
    walletRechargeMaxAmount: yup
      .number()
      .min(0, language.MIN + " " + 0)
      .required(language.REQUIRED),
    walletRechargeMinAmount: yup
      .number()
      .min(0, language.MIN + " " + 0)
      .required(language.REQUIRED),
    withDrawMinAmount: yup
      .number()
      .min(0, language.MIN + " " + 0)
      .required(language.REQUIRED),
    callCenterPhone: yup.string(),
    sosEmergencyNumber: yup.string(),
    smsType: yup.string().required(language.REQUIRED),
    tipsMaxAmount: yup.string().required(language.REQUIRED),
    siteStatusMessage: yup.string().required(language.REQUIRED),
    currencyCode: yup.string().required(language.REQUIRED),
    currencySymbol: yup.string().required(language.REQUIRED),
    payoutOptions: yup.string().required(language.REQUIRED),
    tipsMinAmount: yup
      .number()
      .min(1, language.MIN + " " + 1)
      .required(language.REQUIRED),
    onlineStatusThreshold: yup
      .number()
      .min(1, language.MIN + " " + 1)
      .required(language.REQUIRED),
    bookingRetryCount: yup.string().required(language.REQUIRED),
    redirectUrls: yup.object().shape({
      userPlayStore: yup.string().required(language.REQUIRED),
      driverPlayStore: yup.string().required(language.REQUIRED),
      userAppstore: yup.string().required(language.REQUIRED),
      driverAppstore: yup.string().required(language.REQUIRED),
      userAppleId: yup.string().required(language.REQUIRED),
      driverAppleId: yup.string().required(language.REQUIRED),
      faqUrl: yup.string().required(language.REQUIRED),
      termsAndConditionUrl: yup.string().required(language.REQUIRED),
      privacyUrl: yup.string().required(language.REQUIRED),
      aboutUrl: yup.string().required(language.REQUIRED),
      paymentRedirectDomain: yup.string().required(language.REQUIRED),
    }),
    driverCancelFeeStatus: yup.string().required(language.REQUIRED),
    mapApiArray: yup.object().shape({
      web: yup.array().of(
        yup.object().shape({
          label: yup.string().required(language.REQUIRED),
          key: yup.string().required(language.REQUIRED),
          time: yup.string().required(language.REQUIRED),
          default: yup.boolean(),
        })
      ),
      android: yup.array().of(
        yup.object().shape({
          label: yup.string().required(language.REQUIRED),
          key: yup.string().required(language.REQUIRED),
          time: yup.string().required(language.REQUIRED),
          default: yup.boolean(),
        })
      ),
      ios: yup.array().of(
        yup.object().shape({
          label: yup.string().required(language.REQUIRED),
          key: yup.string().required(language.REQUIRED),
          time: yup.string().required(language.REQUIRED),
          default: yup.boolean(),
        })
      ),
    }),
  });
  const [general, setGeneral] = React.useState({
    id: "",
    billingDays: 0,
    bookingPrefix: "",
    darkLogo: "",
    distanceCalculateType: "",
    driverMinAge: 0,
    minimumWalletAmountToOnline: 0,
    driverRequestTimeout: 0,
    isTollNeeded: 0,
    emailConfigurationEnable: 0,
    smsConfigurationEnable: 0,
    documentExpireThreshold: 10,
    // isRerouteEnable: 0,
    isTollToDriver: 0,
    defaultCountryCode: "IN",
    isWaitingEnable: 0,
    isTollManual: 0,
    bookingAlgorithm: "COMPETITIVE",
    professionalPenaltyStatus: 0,
    stopLimit: 1,
    isManualMeterNeeded: 1,
    androidVersionKey: 1,
    isAddressChangeEnable: 0,
    cronTimeZone: "Africa/Addis_Ababa",
    isOtpNeeded: 0,
    isProduction: 0,
    iosDriverVersionKey: 1,
    iosVersionKey: 1,
    iosUserVersionKey: 1,
    isMapOptimized: 1,
    isUserReroute: 1,
    isDriverReroute: 1,
    androidUserVersionKey: 1,
    androidDriverVersionKey: 1,
    currencyCode: "",
    currencySymbol: "",
    onlineStatusThreshold: 1,
    timeDurationThershold: 1,
    turfDistanceThershold: 1,
    sosEmergencyNumber: "911",
    walletSendMinAmount: 0,
    walletSendMaxAmount: 0,
    walletMinAmountThreshold: 0,
    walletRechargeMaxAmount: 0,
    walletRechargeMinAmount: 0,
    withDrawMinAmount: 0,
    callCenterPhone: "",
    emailAddress: "",
    paymentSection: "ALL",
    mapApiArray: {
      web: [],
      android: [],
      ios: [],
    },
    favicon: "",
    firebaseAdmin: {
      androidUser: "",
      iosUser: "",
      androidDriver: "",
      iosDriver: "",
    },
    lightLogo: "",
    mapApi: {
      web: "",
      android: "",
      ios: "",
    },
    spaces: {
      spacesKey: "",
      spacesSecret: "",
      spacesEndpoint: "",
    },
    approxCalculationDistance: 0,
    approxCalculationTime: 0,
    mobileLogo: "",
    arrayData: {
      pageViewLimits: [],
    },
    requestDistance: 0,
    responceOfficerMinAge: 0,
    retryRequestDistance: 0,
    siteTitle: "",
    siteUrl: "",
    tipStatus: 0,
    siteStatus: 0,
    siteStatusMessage: "",
    isReferralNeeded: 0,
    isCancelToNextProfessional: 0,
    isTailRideNeeded: 0,
    isSocketPolling: 0,
    isQuickTripNeeded: 0,
    isDailyTripNeeded: 0,
    isOutstationNeeded: 0,
    isRentalNeeded: 0,
    isAirportTripNeeded: 0,
    isCarpoolTripNeeded: 0,
    isAmbulanceNeeded: 0,
    isSmallPackageNeeded: 0,
    isHandicapNeeded: 0,
    isChildseatNeeded: 0,
    isgenderTripNeeded: 0,
    isCountryCodeNeeded: 0,
    defaultDialCode: "+91",
    appUpdateStatus: "NONE",
    driverAppUpdateStatus: "NONE",
    userAppUpdateStatus: "NONE",
    androidUserAppUpdateStatus: "NONE",
    androidDriverAppUpdateStatus: "NONE",
    iosDriverAppUpdateStatus: "NONE",
    iosUserAppUpdateStatus: "NONE",
    smsType: "FIREBASE",
    lastPriorityStatus: 0,
    lastPriorityCount: 0,
    isVinNeeded: 0,
    tipsMaxAmount: 0,
    tipsMinAmount: 0,
    payoutOptions: "",
    bookingRetryCount: 0,
    redirectUrls: {
      userPlayStore: "",
      driverPlayStore: "",
      userAppstore: "",
      driverAppstore: "",
      userAppleId: "",
      driverAppleId: "",
      faqUrl: "",
      termsAndConditionUrl: "",
      privacyUrl: "",
      aboutUrl: "",
      paymentRedirectDomain: "",
    },
    driverCancelFeeStatus: 0,
  });

  React.useEffect(() => {
    // window.scrollTo(0, 0);
  }, []);

  const appData = [
    {
      id: 1,
      label: language.APP_DATA_NONE,
      value: "NONE",
    },
    {
      id: 2,
      label: language.APP_DATA_OPTIONAL,
      value: "OPTIONAL",
    },
    {
      id: 3,
      label: language.FORCE_UPDATE,
      value: "FORCEUPDATE",
    },
  ];

  const algorithmsData = [
    {
      id: 1,
      label: language.COMPETITIVE,
      value: "COMPETITIVE",
    },
    {
      id: 2,
      label: language.NEAREST,
      value: "NEAREST",
    },
  ];

  const distanceCalculateTypeData = [
    {
      id: 1,
      label: language.MANUAL,
      value: "MANUAL",
    },
    {
      id: 2,
      label: language.ROUTE,
      value: "ROUTE",
    },
  ];

  const smsTypeData = [
    {
      id: 1,
      label: language.TWILIO,
      value: "TWILIO",
    },
    {
      id: 2,
      label: language.FIREBASE,
      value: "FIREBASE",
    },
  ];

  const payoutOptions = [
    {
      id: 1,
      label: language.AUTOMATED,
      value: "AUTOMATED",
    },
    {
      id: 2,
      label: language.MANUAL,
      value: "MANUAL",
    },
    { id: 3, label: language.DISPLAY_INFO, value: "DISPLAYINFO" },
  ];

  const submit = async (e) => {
    if (e.mapApiArray.web.filter((each) => each.default === 1).length === 0)
      return alert(language.GOOGLE_MAP_WEB_VALIDATION);
    if (e.mapApiArray.android.filter((each) => each.default === 1).length === 0)
      return alert(language.GOOGLE_MAP_ANDROID_VALIDATION);
    if (e.mapApiArray.ios.filter((each) => each.default === 1).length === 0)
      return alert(language.GOOGLE_MAP_IOS_VALIDATION);
    setBtnLoading(true);
    let formData = new FormData();
    let fileLoaded = false;
    if (isBase64(e.lightLogo)) {
      fileLoaded = true;
      const image = await compressImage(e.lightLogo);
      formData.append("lightLogo", image);
    }
    if (isBase64(e.darkLogo)) {
      fileLoaded = true;
      const image = await compressImage(e.darkLogo);
      formData.append("darkLogo", image);
    }
    if (isBase64(e.mobileLogo)) {
      fileLoaded = true;
      const image = await compressImage(e.mobileLogo);
      formData.append("mobileLogo", image);
    }
    if (isBase64(e.favicon)) {
      fileLoaded = true;
      const image = await compressImage(e.favicon);
      formData.append("favicon", image);
    }
    if (fileLoaded) {
      try {
        const imageList = await axios.post(
          A.HOST + A.ADMIN_SETTINGS_GENERAL_UPLOAD,
          formData,
          {
            headers: {
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/form-data",
            },
          }
        );
        e = { ...e, ...imageList.data };
      } catch (err) {
        authFailure(err);
      }
    }
    try {
      await axios.post(
        A.HOST + A.ADMIN_SETTINGS_GENERAL_UPDATE,
        {
          ...e,
          driverMinAge: parseInt(e.driverMinAge),
          isCountryCodeNeeded: e.isCountryCodeNeeded === 1 ? true : false,
          driverCancelFeeStatus: e.driverCancelFeeStatus === 1 ? true : false,
          isSocketPolling: e.isSocketPolling === 1 ? true : false,
          tipStatus: e.tipStatus === 1 ? true : false,
          siteStatus: e.siteStatus === 1 ? true : false,
          isQuickTripNeeded: e.isQuickTripNeeded === 1 ? true : false,
          isDailyTripNeeded: e.isDailyTripNeeded === 1 ? true : false,
          isOutstationNeeded: e.isOutstationNeeded === 1 ? true : false,
          isRentalNeeded: e.isRentalNeeded === 1 ? true : false,
          isAirportTripNeeded: e.isAirportTripNeeded === 1 ? true : false,
          isCarpoolTripNeeded: e.isCarpoolTripNeeded === 1 ? true : false,
          isAmbulanceNeeded: e.isAmbulanceNeeded === 1 ? true : false,
          isSmallPackageNeeded: e.isSmallPackageNeeded === 1 ? true : false,
          isHandicapNeeded: e.isHandicapNeeded === 1 ? true : false,
          isChildseatNeeded: e.isChildseatNeeded === 1 ? true : false,
          isVinNeeded: e.isVinNeeded === 1 ? true : false,
          emailConfigurationEnable: e.emailConfigurationEnable === 1 ? true : false,
          smsConfigurationEnable: e.smsConfigurationEnable === 1 ? true : false,
          isgenderTripNeeded: e.isgenderTripNeeded === 1 ? true : false,
          isManualMeterNeeded: e.isManualMeterNeeded === 1 ? true : false,
          isTollManual: e.isTollManual === 1 ? true : false,
          professionalPenaltyStatus: e.professionalPenaltyStatus === 1 ? true : false,
          isTollNeeded: e.isTollNeeded === 1 ? true : false,
          isMapOptimized: e.isMapOptimized === 1 ? true : false,
          isUserReroute: e.isUserReroute === 1 ? true : false,
          isDriverReroute: e.isDriverReroute === 1 ? true : false,
          isTollToDriver: e.isTollToDriver === 1 ? true : false,
          isAddressChangeEnable: e.isAddressChangeEnable === 1 ? true : false,
          // isRerouteEnable: e.isRerouteEnable === 1 ? true : false,
          isWaitingEnable: e.isWaitingEnable === 1 ? true : false,
          lastPriorityStatus: e.lastPriorityStatus === 1 ? true : false,
          isReferralNeeded: e.isReferralNeeded === 1 ? true : false,
          isCancelToNextProfessional: e.isCancelToNextProfessional === 1 ? true : false,
          isProduction: e.isProduction === 1 ? true : false,
          isOtpNeeded: e.isOtpNeeded === 1 ? true : false,
          isTailRideNeeded: e.isTailRideNeeded === 1 ? true : false,
          minimumWalletAmountToOnline: parseInt(e.minimumWalletAmountToOnline),
          stopLimit: parseInt(e.stopLimit),
          onlineStatusThreshold: parseInt(e.onlineStatusThreshold),
          lastPriorityCount: parseInt(e.lastPriorityCount),
          timeDurationThershold: parseInt(e.timeDurationThershold),
          turfDistanceThershold: parseInt(e.turfDistanceThershold),
          androidVersionKey: parseInt(e.androidVersionKey),
          iosVersionKey: parseInt(e.iosVersionKey),
          iosDriverVersionKey: parseInt(e.iosDriverVersionKey),
          documentExpireThreshold: parseInt(e.documentExpireThreshold),
          iosUserVersionKey: parseInt(e.iosUserVersionKey),
          androidUserVersionKey: parseInt(e.androidUserVersionKey),
          androidDriverVersionKey: parseInt(e.androidDriverVersionKey),
          requestDistance: parseInt(e.requestDistance),
          driverRequestTimeout: parseInt(e.driverRequestTimeout),
          retryRequestDistance: parseInt(e.retryRequestDistance),
          billingDays: parseInt(e.billingDays),
          tipsMaxAmount: parseInt(e.tipsMaxAmount),
          approxCalculationDistance: parseInt(e.approxCalculationDistance),
          approxCalculationTime: parseInt(e.approxCalculationTime),
          tipsMinAmount: parseInt(e.tipsMinAmount),
          bookingRetryCount: parseInt(e.bookingRetryCount),

          nextProfessionalThreshold: parseInt(e.nextProfessionalThreshold),
          adminAssignThreshold: parseInt(e.adminAssignThreshold),

          walletSendMinAmount: parseInt(e.walletSendMinAmount),
          walletSendMaxAmount: parseInt(e.walletSendMaxAmount),
          walletMinAmountThreshold: parseInt(e.walletMinAmountThreshold),
          walletRechargeMaxAmount: parseInt(e.walletRechargeMaxAmount),
          walletRechargeMinAmount: parseInt(e.walletRechargeMinAmount),
          withDrawMinAmount: parseInt(e.withDrawMinAmount),
        },
        header
      );
      setPop({ title: language.UPDATE_SUCCESS });
    } catch (err) {
      authFailure(err);
      setPop({ title: language.ERROR, type: "error" });
    }
    setBtnLoading(false);
    await fetchSettings();
  };

  const pages = ["10", "20", "30", "40"];
  const formik = useFormik({
    initialValues: general,
    enableReinitialize: true,
    validateOnChange: false,
    validationSchema,
    onSubmit: submit,
  });

  React.useEffect(() => {
    fetchGeneral();
  }, []);

  React.useEffect(() => {
    if (formik.values.defaultDialCode) {
      const countryCode = CountryCodes.filter(
        (each) => each.dial_code === formik.values.defaultDialCode
      );
      if (countryCode.length > 0)
        formik.setFieldValue("defaultCountryCode", countryCode[0].code);
    }
  }, [formik.values.defaultDialCode]);

  const fetchGeneral = async () => {
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_SETTINGS_GENERAL_READ,
        {},
        header
      );
      if (
        !data.data.mapApiArray ||
        !data.data.mapApiArray.web ||
        !data.data.mapApiArray.android ||
        !data.data.mapApiArray.ios
      ) {
        data.data.mapApiArray = {
          web: [
            {
              label: "",
              key: data.data.mapApi.web,
              default: 1,
              time: new Date(),
            },
          ],
          android: [
            {
              label: "",
              key: data.data.mapApi.android,
              default: 1,
              time: new Date(),
            },
          ],
          ios: [
            {
              label: "",
              key: data.data.mapApi.ios,
              default: 1,
              time: new Date(),
            },
          ],
        };
      }
      setGeneral({
        ...general,
        id: data._id,
        ...data.data,
        isCountryCodeNeeded: data.data.isCountryCodeNeeded === true ? 1 : 0,
        driverCancelFeeStatus: data.data.driverCancelFeeStatus === true ? 1 : 0,
        tipStatus: data.data.tipStatus === true ? 1 : 0,
        isTollNeeded: data.data.isTollNeeded === true ? 1 : 0,
        isTollToDriver: data.data.isTollToDriver === true ? 1 : 0,
        isAddressChangeEnable: data.data.isAddressChangeEnable === true ? 1 : 0,
        isQuickTripNeeded: data.data.isQuickTripNeeded === true ? 1 : 0,
        isDailyTripNeeded: data.data.isDailyTripNeeded === true ? 1 : 0,
        isOutstationNeeded: data.data.isOutstationNeeded === true ? 1 : 0,
        isVinNeeded: data.data.isVinNeeded === true ? 1 : 0,
        isRentalNeeded: data.data.isRentalNeeded === true ? 1 : 0,
        isAirportTripNeeded: data.data.isAirportTripNeeded === true ? 1 : 0,
        isCarpoolTripNeeded: data.data.isCarpoolTripNeeded === true ? 1 : 0,
        isAmbulanceNeeded: data.data.isAmbulanceNeeded === true ? 1 : 0,
        isSmallPackageNeeded: data.data.isSmallPackageNeeded === true ? 1 : 0,
        isHandicapNeeded: data.data.isHandicapNeeded === true ? 1 : 0,
        isChildseatNeeded: data.data.isChildseatNeeded === true ? 1 : 0,
        isgenderTripNeeded: data.data.isgenderTripNeeded === true ? 1 : 0,
        emailConfigurationEnable: data.data.emailConfigurationEnable === true ? 1 : 0,
        smsConfigurationEnable: data.data.smsConfigurationEnable === true ? 1 : 0,
        isWaitingEnable: data.data.isWaitingEnable === true ? 1 : 0,
        isMapOptimized: data.data.isMapOptimized === true ? 1 : 0,
        isUserReroute: data.data.isUserReroute === true ? 1 : 0,
        isDriverReroute: data.data.isDriverReroute === true ? 1 : 0,
        // isRerouteEnable: data.data.isRerouteEnable === true ? 1 : 0,
        isOtpNeeded: data.data.isOtpNeeded === true ? 1 : 0,
        isProduction: data.data.isProduction === true ? 1 : 0,
        isManualMeterNeeded: data.data.isManualMeterNeeded === true ? 1 : 0,
        lastPriorityStatus: data.data.lastPriorityStatus === true ? 1 : 0,
        isTollManual: data.data.isTollManual === true ? 1 : 0,
        professionalPenaltyStatus: data.data.professionalPenaltyStatus === true ? 1 : 0,
        siteStatus: data.data.siteStatus === true ? 1 : 0,
        isReferralNeeded: data.data.isReferralNeeded === true ? 1 : 0,
        isCancelToNextProfessional: data.data.isCancelToNextProfessional === true ? 1 : 0,
        isTailRideNeeded: data.data.isTailRideNeeded === true ? 1 : 0,
        isSocketPolling: data.data.isSocketPolling === true ? 1 : 0,
      });
      setLoading(false);
    } catch (err) {
      alert(err);
      authFailure(err);
    }
  };

  React.useEffect(() => {
    formik.setFieldValue("currencySymbol", Currency[formik.values.currencyCode]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formik.values.currencyCode]);

  React.useEffect(() => {
    loading === false &&
      gsap.fromTo(".generalSettings", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.05 });
  }, [loading]);
  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      animate="generalSettings"
      width="3/4"
      submitBtn={true}
      btnLoading={btnLoading}
      submit={formik.handleSubmit}
      title={language.GENERAL + " " + language.SETTINGS}
    >
      <Section>
        {popup != null && (
          <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />
        )}
        <Border>
          <Heading animate="generalSettings" title={language.SITE_CONFIGURE} />
          <Flex border={true} wrap={true} align={false}>
            <Section padding={false}>
              {/* Site title  */}
              <FieldWrapper animate="generalSettings" title={language.SITE_TITLE}>
                <TextField
                  value={formik.values.siteTitle}
                  change={(e) => formik.setFieldValue("siteTitle", e)}
                  error={formik.errors.siteTitle}
                />
              </FieldWrapper>
              {/* Booking id prefix  */}
              <FieldWrapper animate="generalSettings" title={language.BOOKING_ID_PREFIX}>
                <TextField
                  value={formik.values.bookingPrefix}
                  change={(e) => formik.setFieldValue("bookingPrefix", e)}
                  error={formik.errors.bookingPrefix}
                  readOnly={true}
                />
              </FieldWrapper>
              {/* Site url  */}
              <FieldWrapper animate="generalSettings" title={language.SITE_URL}>
                <TextField
                  change={(e) => formik.setFieldValue("siteUrl", e)}
                  error={formik.errors.siteUrl}
                  value={formik.values.siteUrl}
                />
              </FieldWrapper>
              {/* Email */}
              <FieldWrapper animate="generalSettings" title={language.EMAIL}>
                <TextField
                  value={formik.values.emailAddress}
                  change={(e) => formik.setFieldValue("emailAddress", e)}
                  error={formik.errors.emailAddress}
                />
              </FieldWrapper>
              <FieldWrapper title={language.CALL_CENTER_PHONE}>
                <TextField
                  change={(e) => formik.setFieldValue("callCenterPhone", e)}
                  error={formik.errors.callCenterPhone}
                  value={formik.values.callCenterPhone}
                />
              </FieldWrapper>
              <FieldWrapper title={language.SOS_EMERGENCY_NUMBER}>
                <TextField
                  type={"number"}
                  change={(e) => formik.setFieldValue("sosEmergencyNumber", e)}
                  error={formik.errors.sosEmergencyNumber}
                  value={formik.values.sosEmergencyNumber}
                />
              </FieldWrapper>
            </Section>
            <Section padding={false}>
              {/* LOGO  */}
              <Flex align={false}>
                <Section padding={false}>
                  <FieldWrapper animate="generalSettings" title={language.LIGHT_LOGO}>
                    {/* Light logo */}
                    <FileUpload
                      margin={false}
                      crop={false}
                      padding={false}
                      change={(e) => formik.setFieldValue("lightLogo", e)}
                      defaultValue={imageUrl(formik.values.lightLogo)}
                    />
                  </FieldWrapper>
                </Section>
                <Section padding={false}>
                  <FieldWrapper animate="generalSettings" title={language.DARK_LOGO}>
                    {/* Dark Logo  */}
                    <FileUpload
                      margin={false}
                      crop={false}
                      padding={false}
                      change={(e) => formik.setFieldValue("darkLogo", e)}
                      defaultValue={imageUrl(formik.values.darkLogo)}
                    />
                  </FieldWrapper>
                </Section>
              </Flex>
              {/* Mobile and favicon  */}
              <Flex align={false}>
                <Section padding={false}>
                  <FieldWrapper animate="generalSettings" title={language.MOBV_LOGO}>
                    <FileUpload
                      margin={false}
                      crop={false}
                      change={(e) => formik.setFieldValue("mobileLogo", e)}
                      padding={false}
                      defaultValue={imageUrl(formik.values.mobileLogo)}
                    />
                  </FieldWrapper>
                </Section>
                <Section padding={false}>
                  <FieldWrapper animate="generalSettings" title={language.FAVICON}>
                    <FileUpload
                      margin={false}
                      crop={false}
                      change={(e) => formik.setFieldValue("favicon", e)}
                      padding={false}
                      defaultValue={imageUrl(formik.values.favicon)}
                    />
                  </FieldWrapper>
                </Section>
              </Flex>
            </Section>
          </Flex>
        </Border>
        <Border>
          <Heading title={language.ADMIN_PANEL_SETTINGS} />
          <Flex wrap={true}>
            <Section>
              <FieldWrapper
                animate="generalSettings"
                title={language.SET_DEFAULT_PHONE_CODE}
              >
                <ToggleButton
                  change={(e) => formik.setFieldValue("isCountryCodeNeeded", e)}
                  defaultValue={formik.values.isCountryCodeNeeded}
                />
              </FieldWrapper>
            </Section>
            <Section>
              {formik.values.isCountryCodeNeeded === 1 && (
                <>
                  <div className="flex items-center">
                    <FieldWrapper title={language.PHONE_CODE}>
                      <CountryCodesPicker
                        placeholder={language.DIAL_CODE}
                        defaultValue={formik.values.defaultDialCode}
                        margin={3}
                        change={(e) => formik.setFieldValue("defaultDialCode", e, true)}
                      />
                    </FieldWrapper>
                    <FieldWrapper title={language.COUNTRY_CODE}>
                      <TextField
                        value={formik.values.defaultCountryCode}
                        change={(e) => formik.setFieldValue("defaultCountryCode", e)}
                        error={formik.errors.defaultCountryCode}
                        readOnly={true}
                      />
                    </FieldWrapper>
                  </div>
                </>
              )}
            </Section>
          </Flex>
        </Border>
        <Border>
          <Heading title={language.TABLE_CONFIGURE} />
          <Flex wrap={true}>
            <Section>
              <FieldWrapper animate="generalSettings" title={language.PAGE_LIMIT}>
                <MultiSelect
                  change={(e) => formik.setFieldValue("arrayData.pageViewLimits", e)}
                  error={
                    formik.errors.arrayData && formik.errors.arrayData.pageViewLimits
                  }
                  allFields={pages}
                  defaultValue={formik.values.arrayData.pageViewLimits}
                ></MultiSelect>
              </FieldWrapper>
            </Section>
            <Section></Section>
          </Flex>
        </Border>
        <Border>
          <Heading title={language.WALLET_SETTINGS} />
          <Flex wrap={true} style={{ alignItems: "flex-start" }}>
            <Section>
              <FieldWrapper
                animate="generalSettings"
                title={language.MINIMUM_WALLET_AMOUNT_TO_ONLINE}
              >
                <TextField
                  type={"number"}
                  change={(e) => formik.setFieldValue("minimumWalletAmountToOnline", e)}
                  error={formik.errors.minimumWalletAmountToOnline}
                  value={formik.values.minimumWalletAmountToOnline}
                />
              </FieldWrapper>
              <FieldWrapper
                animate="generalSettings"
                title={language.WARNING_MINIMUM_AMOUNT_THRESHOLD}
              >
                <TextField
                  type={"number"}
                  change={(e) => formik.setFieldValue("walletMinAmountThreshold", e)}
                  error={formik.errors.walletMinAmountThreshold}
                  value={formik.values.walletMinAmountThreshold}
                />
              </FieldWrapper>
              <FieldWrapper
                animate="generalSettings"
                title={language.WITHDRAW_MINIMUM_AMOUNT}
              >
                <TextField
                  type={"number"}
                  change={(e) => formik.setFieldValue("withDrawMinAmount", e)}
                  error={formik.errors.withDrawMinAmount}
                  value={formik.values.withDrawMinAmount}
                />
              </FieldWrapper>
            </Section>
            <Section>
              <FieldWrapper
                animate="generalSettings"
                title={language.TRANSFER_MINIMUM_AMOUNT}
              >
                <TextField
                  type={"number"}
                  change={(e) => formik.setFieldValue("walletSendMinAmount", e)}
                  error={formik.errors.walletSendMinAmount}
                  value={formik.values.walletSendMinAmount}
                />
              </FieldWrapper>
              <FieldWrapper
                animate="generalSettings"
                title={language.TOP_UP_MINIMUM_AMOUNT}
              >
                <TextField
                  type={"number"}
                  change={(e) => formik.setFieldValue("walletRechargeMinAmount", e)}
                  error={formik.errors.walletRechargeMinAmount}
                  value={formik.values.walletRechargeMinAmount}
                />
              </FieldWrapper>
            </Section>
            <Section>
              <FieldWrapper
                animate="generalSettings"
                title={language.TRANSFER_MAX_AMOUNT}
              >
                <TextField
                  type={"number"}
                  change={(e) => formik.setFieldValue("walletSendMaxAmount", e)}
                  error={formik.errors.walletSendMaxAmount}
                  value={formik.values.walletSendMaxAmount}
                />
              </FieldWrapper>
              <FieldWrapper
                animate="generalSettings"
                title={language.TOP_UP_MAXIMUM_AMOUNT}
              >
                <TextField
                  type={"number"}
                  change={(e) => formik.setFieldValue("walletRechargeMaxAmount", e)}
                  error={formik.errors.walletRechargeMaxAmount}
                  value={formik.values.walletRechargeMaxAmount}
                />
              </FieldWrapper>
            </Section>
          </Flex>
        </Border>
        <Border>
          <Heading title={language.DRIVER_SETTINGS} />
          <Flex wrap={true} style={{ alignItems: "flex-start" }}>
            <Section>
              <FieldWrapper animate="generalSettings" title={language.MIN_AGE_FOR_DRIVER}>
                <TextField
                  type={"number"}
                  change={(e) => formik.setFieldValue("driverMinAge", e)}
                  error={formik.errors.driverMinAge}
                  value={formik.values.driverMinAge}
                />
              </FieldWrapper>
              <div className="flex">
                <FieldWrapper
                  animate="generalSettings"
                  title={language.DRIVER_CANCEL_FEE_AVAILABLE}
                >
                  <ToggleButton
                    change={(e) => formik.setFieldValue("driverCancelFeeStatus", e)}
                    defaultValue={formik.values.driverCancelFeeStatus}
                  />
                </FieldWrapper>
                <FieldWrapper animate="generalSettings" title={language.TIPS_STATUS}>
                  <ToggleButton
                    change={(e) => formik.setFieldValue("tipStatus", e)}
                    defaultValue={formik.values.tipStatus}
                  />
                </FieldWrapper>
              </div>
              {formik.values.tipStatus === 1 && (
                <>
                  <FieldWrapper title={language.TIPS_MIN_AMOUNT}>
                    <TextField
                      type={"number"}
                      value={formik.values.tipsMinAmount}
                      change={(e) => formik.setFieldValue("tipsMinAmount", e)}
                      error={formik.errors.tipsMinAmount}
                    />
                  </FieldWrapper>{" "}
                  <FieldWrapper title={language.TIPS_MAX_AMOUNT}>
                    <TextField
                      type={"number"}
                      value={formik.values.tipsMaxAmount}
                      change={(e) => formik.setFieldValue("tipsMaxAmount", e)}
                      error={formik.errors.tipsMaxAmount}
                    />
                  </FieldWrapper>{" "}
                </>
              )}
            </Section>
            <Section>
              <FieldWrapper animate="generalSettings" title={language.VIN_NUMBER}>
                <ToggleButton
                  change={(e) => formik.setFieldValue("isVinNeeded", e)}
                  defaultValue={formik.values.isVinNeeded}
                />
              </FieldWrapper>
            </Section>
          </Flex>
        </Border>
        <Border>
          <Heading title={language.PAYMENT_SETTINGS} />
          <Flex align={false}>
            <Section>
              <FieldWrapper animate="generalSettings" title={language.PAYMENT_SECTION}>
                <ProtectedField
                  Component={DropdownNormal}
                  defaultValue={
                    paymentSectionFields.filter(
                      (each) => each.value === formik.values.paymentSection
                    )[0]?.label
                  }
                  fields={paymentSectionFields}
                  error={formik.errors.paymentSection}
                  change={(e) => formik.setFieldValue("paymentSection", e)}
                />
              </FieldWrapper>
              {formik.values.paymentSection !== "CASHANDWALLET" && (
                <FieldWrapper title={language.BILLING_CYCLE_IN_DAYS}>
                  <ProtectedField
                    type={"number"}
                    value={formik.values.billingDays}
                    change={(e) => formik.setFieldValue("billingDays", e)}
                    error={formik.errors.billingDays}
                  />
                </FieldWrapper>
              )}
            </Section>
            <Section>
              <FieldWrapper animate="generalSettings" title={language.PAYMENT_OPTIONS}>
                <ProtectedField
                  Component={DropdownNormal}
                  defaultValue={
                    payoutOptions.filter(
                      (each) => each.value === formik.values?.payoutOptions
                    )[0]?.label
                  }
                  fields={payoutOptions}
                  error={formik.errors.payoutOptions}
                  change={(e) => formik.setFieldValue("payoutOptions", e)}
                />
              </FieldWrapper>
              <FieldWrapper title={language.REDIRECT_DOMAIN}>
                <ProtectedField
                  value={formik.values.redirectUrls?.paymentRedirectDomain}
                  change={(e) =>
                    formik.setFieldValue("redirectUrls.paymentRedirectDomain", e)
                  }
                  error={formik.errors.redirectUrls?.paymentRedirectDomain}
                />
              </FieldWrapper>
            </Section>
          </Flex>
        </Border>

        <Border>
          {/* {JSON.stringify(formik.errors, null, 2)} */}
          <Heading title={language.DEVELOPER_SETTINGS} />
          <Flex>
            <Section>
              <FieldWrapper
                animate="generalSettings"
                title={language.APP_UPDATE_SETTINGS}
              >
                <ProtectedField
                  Component={DropdownNormal}
                  defaultValue={
                    appData.filter((each) => each.value === formik.values.appUpdateStatus)
                      .length > 0
                      ? appData.filter(
                          (each) => each.value === formik.values.appUpdateStatus
                        )[0].label
                      : false
                  }
                  fields={appData}
                  error={formik.errors.appUpdateStatus}
                  change={(e) => formik.setFieldValue("appUpdateStatus", e)}
                />
              </FieldWrapper>
            </Section>
            <Section>
              <FieldWrapper animate="generalSettings" title={language.BOOKING_ALGORITHM}>
                <ProtectedField
                  Component={DropdownNormal}
                  defaultValue={
                    algorithmsData.filter(
                      (each) => each.value === formik.values.bookingAlgorithm
                    ).length > 0
                      ? algorithmsData.filter(
                          (each) => each.value === formik.values.bookingAlgorithm
                        )[0].label
                      : false
                  }
                  fields={algorithmsData}
                  error={formik.errors.bookingAlgorithm}
                  change={(e) => formik.setFieldValue("bookingAlgorithm", e)}
                />
              </FieldWrapper>
            </Section>
          </Flex>
          {/* <Flex>
                <Section>
                  <FieldWrapper animate="generalSettings" title={language.DRIVER_APP_UPDATE_STATUS}>
                    <DropdownNormal
                      defaultValue={
                        appData.filter((each) => each.value === formik.values.driverAppUpdateStatus).length > 0
                          ? appData.filter((each) => each.value === formik.values.driverAppUpdateStatus)[0].label
                          : false
                      }
                      fields={appData}
                      error={formik.errors.driverAppUpdateStatus}
                      change={(e) => formik.setFieldValue("driverAppUpdateStatus", e)}
                    />
                  </FieldWrapper>
                </Section>
                <Section>
                  <FieldWrapper animate="generalSettings" title={language.USER_APP_UPDATE_STATUS}>
                    <DropdownNormal
                      defaultValue={
                        appData.filter((each) => each.value === formik.values.userAppUpdateStatus).length > 0
                          ? appData.filter((each) => each.value === formik.values.userAppUpdateStatus)[0].label
                          : false
                      }
                      fields={appData}
                      error={formik.errors.userAppUpdateStatus}
                      change={(e) => formik.setFieldValue("userAppUpdateStatus", e)}
                    />
                  </FieldWrapper>
                </Section>
              </Flex> */}
          <Flex wrap={true} align={false}>
            <Section>
              {/* <FieldWrapper animate="generalSettings" title={language.ANDROID_VERSION_KEY}>
                    <TextField
                      type={"number"}
                      value={formik.values.androidVersionKey}
                      change={(e) => formik.setFieldValue("androidVersionKey", e)}
                      error={formik.errors.androidVersionKey}
                    />
                  </FieldWrapper> */}
              <FieldWrapper
                animate="generalSettings"
                title={language.ONLINE_STATUS_THRESHOLD}
              >
                <ProtectedField
                  type={"number"}
                  value={formik.values.onlineStatusThreshold}
                  change={(e) => formik.setFieldValue("onlineStatusThreshold", e)}
                  error={formik.errors.onlineStatusThreshold}
                />
              </FieldWrapper>
              <FieldWrapper
                animate="generalSettings"
                title={language.RIDE_REQUEST_DISTANCE}
              >
                <ProtectedField
                  type={"number"}
                  value={formik.values.requestDistance}
                  change={(e) => formik.setFieldValue("requestDistance", e)}
                  error={formik.errors.requestDistance}
                />
              </FieldWrapper>

              {/* <Flex>
                <Section padding={false}> */}
              <FieldWrapper
                animate="generalSettings"
                title={language.DRIVER_REQUEST_TIMEOUT}
              >
                <ProtectedField
                  type={"number"}
                  // icon={language.SECONDS}
                  value={formik.values.driverRequestTimeout}
                  change={(e) => formik.setFieldValue("driverRequestTimeout", e)}
                  error={formik.errors.driverRequestTimeout}
                />
              </FieldWrapper>
              <FieldWrapper
                animate="generalSettings"
                title={language.CALCULATE_APPROXIMATE_TIME}
              >
                <ProtectedField
                  type={"number"}
                  value={formik.values.approxCalculationTime}
                  change={(e) => formik.setFieldValue("approxCalculationTime", e)}
                  error={formik.errors.approxCalculationTime}
                />
              </FieldWrapper>
              <FieldWrapper animate="generalSettings" title={language.SOCKET_POLLING}>
                <ProtectedField
                  Component={ToggleButton}
                  change={(e) => formik.setFieldValue("isSocketPolling", e)}
                  defaultValue={formik.values.isSocketPolling}
                />
              </FieldWrapper>
              <Flex>
                <FieldWrapper animate="generalSettings" title={language.CANCEL_TO_NEXT}>
                  <ProtectedField
                    Component={ToggleButton}
                    change={(e) => formik.setFieldValue("isCancelToNextProfessional", e)}
                    defaultValue={formik.values.isCancelToNextProfessional}
                  />
                </FieldWrapper>
                <FieldWrapper animate="generalSettings" title={language.TAIL_RIDE}>
                  <ProtectedField
                    Component={ToggleButton}
                    change={(e) => formik.setFieldValue("isTailRideNeeded", e)}
                    defaultValue={formik.values.isTailRideNeeded}
                  />
                </FieldWrapper>
              </Flex>
              <FieldWrapper animate="generalSettings" title={language.REFERAL_NEEDED}>
                <ProtectedField
                  Component={ToggleButton}
                  change={(e) => formik.setFieldValue("isReferralNeeded", e)}
                  defaultValue={formik.values.isReferralNeeded}
                />
              </FieldWrapper>
              <FieldWrapper animate="generalSettings" title={language.UNDER_MAINTAINENCE}>
                <ProtectedField
                  Component={ToggleButton}
                  change={(e) => formik.setFieldValue("siteStatus", e)}
                  defaultValue={formik.values.siteStatus}
                />
              </FieldWrapper>
              {formik.values.siteStatus === 1 && (
                <FieldWrapper title={language.MAINTAINENCE_MESSAGE}>
                  <ProtectedField
                    value={formik.values.siteStatusMessage}
                    change={(e) => formik.setFieldValue("siteStatusMessage", e)}
                    error={formik.errors.siteStatusMessage}
                  />
                </FieldWrapper>
              )}
              <Flex>
                <Section width={"1/3"} padding={false}>
                  <FieldWrapper animate="generalSettings" title={language.IS_TOLL_NEEDED}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("isTollNeeded", e)}
                      defaultValue={formik.values.isTollNeeded}
                    />
                  </FieldWrapper>
                </Section>
                <Section width={"3/4"} padding={false}>
                  {formik.values.isTollNeeded === 1 && (
                    <Flex>
                      <Section>
                        <FieldWrapper title={language.IS_TOLL_MANUAL}>
                          <ProtectedField
                            Component={ToggleButton}
                            change={(e) => formik.setFieldValue("isTollManual", e)}
                            defaultValue={formik.values.isTollManual}
                          />
                        </FieldWrapper>
                      </Section>
                      <Section>
                        <FieldWrapper title={language.TOLL_TO_DRIVER}>
                          <ProtectedField
                            Component={ToggleButton}
                            change={(e) => formik.setFieldValue("isTollToDriver", e)}
                            defaultValue={formik.values.isTollToDriver}
                          />
                        </FieldWrapper>
                      </Section>
                    </Flex>
                  )}
                </Section>
              </Flex>
              <FieldWrapper
                animate="generalSettings"
                title={language.DISTANCE_CALCULATE_TYPE}
              >
                <ProtectedField
                  Component={DropdownNormal}
                  defaultValue={
                    distanceCalculateTypeData.filter(
                      (each) => each.value === formik.values.distanceCalculateType
                    ).length > 0
                      ? distanceCalculateTypeData.filter(
                          (each) => each.value === formik.values.distanceCalculateType
                        )[0].label
                      : false
                  }
                  fields={distanceCalculateTypeData}
                  error={formik.errors.distanceCalculateType}
                  change={(e) => formik.setFieldValue("distanceCalculateType", e)}
                />
              </FieldWrapper>
              {/* </Section>
              </Flex> */}
            </Section>
            <Section>
              {/* <FieldWrapper animate="generalSettings" title={language.IOS_VERSION_KEY}>
                    <TextField
                      type={"number"}
                      value={formik.values.iosVersionKey}
                      change={(e) => formik.setFieldValue("iosVersionKey", e)}
                      error={formik.errors.iosVersionKey}
                    />
                  </FieldWrapper> */}

              <FieldWrapper
                animate="generalSettings"
                title={language.RETRY_REQUEST_DISTANCE}
              >
                <ProtectedField
                  type={"number"}
                  value={formik.values.retryRequestDistance}
                  change={(e) => formik.setFieldValue("retryRequestDistance", e)}
                  error={formik.errors.retryRequestDistance}
                />
              </FieldWrapper>

              <FieldWrapper
                animate="generalSettings"
                title={language.BOOKING_RETRY_COUNT}
              >
                <ProtectedField
                  type={"number"}
                  value={formik.values.bookingRetryCount}
                  change={(e) => formik.setFieldValue("bookingRetryCount", e)}
                  error={formik.errors.bookingRetryCount}
                />
              </FieldWrapper>
              <FieldWrapper
                animate="generalSettings"
                title={language.CALCULATE_APPROXIMATE_DISTANCE}
              >
                <ProtectedField
                  type={"number"}
                  value={formik.values.approxCalculationDistance}
                  change={(e) => formik.setFieldValue("approxCalculationDistance", e)}
                  error={formik.errors.approxCalculationDistance}
                />
              </FieldWrapper>
              <FieldWrapper
                animate="generalSettings"
                title={language.ADMIN_BOOKING_EXPIRED_THRESHOLD}
              >
                <ProtectedField
                  type={"number"}
                  value={formik.values.onlineStatusThreshold}
                  change={(e) => formik.setFieldValue("onlineStatusThreshold", e)}
                  error={formik.errors.onlineStatusThreshold}
                />
              </FieldWrapper>
              <FieldWrapper
                animate="generalSettings"
                title={language.TIME_DURATION_THRESHOLD}
              >
                <ProtectedField
                  type={"number"}
                  value={formik.values.timeDurationThershold}
                  change={(e) => formik.setFieldValue("timeDurationThershold", e)}
                  error={formik.errors.timeDurationThershold}
                  icon={language.SECONDS}
                />
              </FieldWrapper>
              <FieldWrapper
                animate="generalSettings"
                title={language.TURF_DISTANCE_THRESHOLD}
              >
                <ProtectedField
                  type={"number"}
                  value={formik.values.turfDistanceThershold}
                  change={(e) => formik.setFieldValue("turfDistanceThershold", e)}
                  error={formik.errors.turfDistanceThershold}
                  icon={language.METERS}
                />
              </FieldWrapper>
              <FieldWrapper animate="generalSettings" title={language.STOP_LIMIT}>
                <ProtectedField
                  type={"number"}
                  value={formik.values.stopLimit}
                  change={(e) => formik.setFieldValue("stopLimit", e)}
                  error={formik.errors.stopLimit}
                />
              </FieldWrapper>
              <FieldWrapper animate="generalSettings" title={language.MANUAL_METER}>
                <ProtectedField
                  Component={ToggleButton}
                  change={(e) => formik.setFieldValue("isManualMeterNeeded", e)}
                  defaultValue={formik.values.isManualMeterNeeded}
                />
              </FieldWrapper>
            </Section>
          </Flex>
          <Flex>
            <Section>
              <Flex>
                {/* <Section>
                      <FieldWrapper title={language.IS_REROUTE_ENABLED}>
                        <ToggleButton
                          change={(e) => formik.setFieldValue("isRerouteEnable", e)}
                          defaultValue={formik.values.isRerouteEnable}
                        />
                      </FieldWrapper>
                    </Section> */}
                <Section>
                  <FieldWrapper title={language.IS_WAITING_ENABLED}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("isWaitingEnable", e)}
                      defaultValue={formik.values.isWaitingEnable}
                    />
                  </FieldWrapper>
                </Section>
              </Flex>
            </Section>
            <Section>
              <Flex>
                <Section>
                  <FieldWrapper animate="generalSettings" title={language.LAST_PRIORITY}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("lastPriorityStatus", e)}
                      defaultValue={formik.values.lastPriorityStatus}
                    />
                  </FieldWrapper>
                </Section>
                <Section>
                  {formik.values.lastPriorityStatus === 1 && (
                    <FieldWrapper title={language.LAST_PRIORITY_COUNT}>
                      <ProtectedField
                        type={"number"}
                        value={formik.values.lastPriorityCount}
                        change={(e) => formik.setFieldValue("lastPriorityCount", e)}
                        error={formik.errors.lastPriorityCount}
                      />
                    </FieldWrapper>
                  )}
                </Section>
              </Flex>
            </Section>
          </Flex>
          <Flex>
            <Section>
              <Flex>
                <Section>
                  <FieldWrapper title={language.IS_ADDRESS_CHANGABLE}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("isAddressChangeEnable", e)}
                      defaultValue={formik.values.isAddressChangeEnable}
                    />
                  </FieldWrapper>
                </Section>
                <Section>
                  <FieldWrapper title={language.PROFESSIONAL_PENALITY}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("professionalPenaltyStatus", e)}
                      defaultValue={formik.values.professionalPenaltyStatus}
                    />
                  </FieldWrapper>
                </Section>
              </Flex>
            </Section>
            <Section>
              <Flex>
                <Section>
                  <FieldWrapper title={language.PRODUCTION}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("isProduction", e)}
                      defaultValue={formik.values.isProduction}
                    />
                  </FieldWrapper>
                </Section>
                <Section>
                  <FieldWrapper title={language.OTP_NEEDED}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("isOtpNeeded", e)}
                      defaultValue={formik.values.isOtpNeeded}
                    />
                  </FieldWrapper>
                </Section>
              </Flex>
            </Section>
          </Flex>
          <Flex align={false}>
            <Section padding={false}>
              <FieldWrapper title={language.CURRENCY_CODE}>
                <ProtectedField
                  Component={DropDownSearch}
                  change={(e) => {
                    formik.setFieldValue("currencyCode", e);
                  }}
                  defaultValue={formik.values.currencyCode}
                  fields={Object.keys(Currency).map((each, i) => ({
                    id: i,
                    label: each,
                    value: each,
                  }))}
                />
              </FieldWrapper>
            </Section>
            <Section padding={false}>
              <FieldWrapper title={language.CURRENCY_SYMBOL}>
                <ProtectedField value={formik.values.currencySymbol} readOnly={true} />
              </FieldWrapper>
            </Section>
          </Flex>
          <Flex>
            <Section>
              <Flex>
                <Section>
                  <FieldWrapper title={language.HANDICAP}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("isHandicapNeeded", e)}
                      defaultValue={formik.values.isHandicapNeeded}
                    />
                  </FieldWrapper>
                </Section>
                <Section>
                  <FieldWrapper title={language.CHILD_SEAT}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("isChildseatNeeded", e)}
                      defaultValue={formik.values.isChildseatNeeded}
                    />
                  </FieldWrapper>
                </Section>
                <Section>
                  <FieldWrapper title={language.GENDER_TRIP}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("isgenderTripNeeded", e)}
                      defaultValue={formik.values.isgenderTripNeeded}
                    />
                  </FieldWrapper>
                </Section>
              </Flex>
            </Section>
            <Section padding={false}>
              <FieldWrapper title={language.TIMEZONE}>
                <ProtectedField
                  Component={DropdownNormal}
                  defaultValue={formik.values.cronTimeZone}
                  change={(e) => formik.setFieldValue("cronTimeZone", e)}
                  fields={timezones.map((each, idx) => ({
                    id: idx,
                    label: each,
                    value: each,
                  }))}
                />
              </FieldWrapper>
            </Section>
          </Flex>
          <Flex>
            <Section>
              <Flex>
                <Section>
                  <FieldWrapper title={language.EMAIL_CONFIGURATION}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("emailConfigurationEnable", e)}
                      defaultValue={formik.values.emailConfigurationEnable}
                    />
                  </FieldWrapper>
                </Section>
                <Section>
                  <FieldWrapper title={language.SMS_CONFIGURATION}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("smsConfigurationEnable", e)}
                      defaultValue={formik.values.smsConfigurationEnable}
                    />
                  </FieldWrapper>
                </Section>
              </Flex>
            </Section>
            <Section>
              <FieldWrapper title={language.DOCUMENT_EXPIRE_THRESHOLD}>
                <ProtectedField
                  type={"number"}
                  value={formik.values.documentExpireThreshold}
                  change={(e) => formik.setFieldValue("documentExpireThreshold", e)}
                  error={formik.errors.documentExpireThreshold}
                />
              </FieldWrapper>
            </Section>
          </Flex>
          <Flex>
            <Section>
              <FieldWrapper title={language.NOTIFY_ADMIN_SCHEDULE}>
                <ProtectedField
                  type={"number"}
                  icon={language.MINUTES}
                  value={formik.values.nextProfessionalThreshold}
                  change={(e) => formik.setFieldValue("nextProfessionalThreshold", e)}
                  error={formik.errors.nextProfessionalThreshold}
                />
              </FieldWrapper>
            </Section>
            <Section>
              <FieldWrapper title={language.AUTOMATICALLY_CANCEL_RIDE_SCHEDULE}>
                <ProtectedField
                  type={"number"}
                  icon={language.MINUTES}
                  value={formik.values.adminAssignThreshold}
                  change={(e) => formik.setFieldValue("adminAssignThreshold", e)}
                  error={formik.errors.adminAssignThreshold}
                />
              </FieldWrapper>
            </Section>
          </Flex>
        </Border>
        <Border>
          <Heading title={language.GOOGLE_OPTIMIZAION} />
          <Flex>
            <Section padding={true}>
              <Flex>
                <Section>
                  <FieldWrapper title={language.MAP_OPTIMIZED}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("isMapOptimized", e)}
                      defaultValue={formik.values.isMapOptimized}
                    />
                  </FieldWrapper>
                </Section>
                <Section>
                  <FieldWrapper title={language.USER_REROUTE}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("isUserReroute", e)}
                      defaultValue={formik.values.isUserReroute}
                    />
                  </FieldWrapper>
                </Section>
                <Section>
                  <FieldWrapper title={language.DRIVER_REROUTE}>
                    <ProtectedField
                      Component={ToggleButton}
                      change={(e) => formik.setFieldValue("isDriverReroute", e)}
                      defaultValue={formik.values.isDriverReroute}
                    />
                  </FieldWrapper>
                </Section>
              </Flex>
            </Section>
          </Flex>
        </Border>
        {admin.userType === "DEVELOPER" && (
          <Border>
            <Heading title={language.TRIP_TYPES} />
            <Flex>
              <Section>
                <FieldWrapper title={language.QUICK_TRIP}>
                  <ProtectedField
                    Component={ToggleButton}
                    change={(e) => formik.setFieldValue("isQuickTripNeeded", e)}
                    defaultValue={formik.values.isQuickTripNeeded}
                  />
                </FieldWrapper>
              </Section>
              <Section>
                <FieldWrapper title={language.DAILY_TRIP}>
                  <ProtectedField
                    Component={ToggleButton}
                    change={(e) => formik.setFieldValue("isDailyTripNeeded", e)}
                    defaultValue={formik.values.isDailyTripNeeded}
                  />
                </FieldWrapper>
              </Section>
              <Section>
                <FieldWrapper title={language.RENTAL}>
                  <ProtectedField
                    Component={ToggleButton}
                    change={(e) => formik.setFieldValue("isRentalNeeded", e)}
                    defaultValue={formik.values.isRentalNeeded}
                  />
                </FieldWrapper>
              </Section>
              <Section>
                <FieldWrapper title={language.AIRPORT_TRIP}>
                  <ProtectedField
                    Component={ToggleButton}
                    change={(e) => formik.setFieldValue("isAirportTripNeeded", e)}
                    defaultValue={formik.values.isAirportTripNeeded}
                  />
                </FieldWrapper>
              </Section>
              <Section>
                <FieldWrapper title={language.CARPOOL_TRIP}>
                  <ProtectedField
                    Component={ToggleButton}
                    change={(e) => formik.setFieldValue("isCarpoolTripNeeded", e)}
                    defaultValue={formik.values.isCarpoolTripNeeded}
                  />
                </FieldWrapper>
              </Section>
              <Section>
                <FieldWrapper title={language.AMBULANCE}>
                  <ProtectedField
                    Component={ToggleButton}
                    change={(e) => formik.setFieldValue("isAmbulanceNeeded", e)}
                    defaultValue={formik.values.isAmbulanceNeeded}
                  />
                </FieldWrapper>
              </Section>
              <Section>
                <FieldWrapper title={language.SMALL_PACKAGE}>
                  <ProtectedField
                    Component={ToggleButton}
                    change={(e) => formik.setFieldValue("isSmallPackageNeeded", e)}
                    defaultValue={formik.values.isSmallPackageNeeded}
                  />
                </FieldWrapper>
              </Section>
            </Flex>
          </Border>
        )}
        {admin.userType === "DEVELOPER" && (
          <Border>
            <Heading title={language.VERSIONING} />
            <p className="m-4 mt-2 my-0">{language.ANDROID}</p>
            <Flex>
              <Section padding={false}>
                <FieldWrapper
                  animate="generalSettings"
                  title={language.ANDROID_USER_VERSION}
                >
                  <ProtectedField
                    type={"number"}
                    value={formik.values.androidUserVersionKey}
                    change={(e) => formik.setFieldValue("androidUserVersionKey", e)}
                    error={formik.errors.androidUserVersionKey}
                  />
                </FieldWrapper>
                <FieldWrapper
                  animate="generalSettings"
                  title={language.ANDROID_DRIVER_VERSION}
                >
                  <ProtectedField
                    type={"number"}
                    value={formik.values.androidDriverVersionKey}
                    change={(e) => formik.setFieldValue("androidDriverVersionKey", e)}
                    error={formik.errors.androidDriverVersionKey}
                  />
                </FieldWrapper>
              </Section>
              <Section padding={false}>
                <FieldWrapper
                  animate="generalSettings"
                  title={language.ANDROID_USER_UPDATE_STATUS}
                >
                  <DropdownNormal
                    defaultValue={
                      appData.filter(
                        (each) => each.value === formik.values.androidUserAppUpdateStatus
                      ).length > 0
                        ? appData.filter(
                            (each) =>
                              each.value === formik.values.androidUserAppUpdateStatus
                          )[0].label
                        : false
                    }
                    fields={appData}
                    error={formik.errors.androidUserAppUpdateStatus}
                    change={(e) => formik.setFieldValue("androidUserAppUpdateStatus", e)}
                  />
                </FieldWrapper>
                <FieldWrapper
                  animate="generalSettings"
                  title={language.ANDROID_DRIVER_UPDATE_STATUS}
                >
                  <DropdownNormal
                    defaultValue={
                      appData.filter(
                        (each) =>
                          each.value === formik.values.androidDriverAppUpdateStatus
                      ).length > 0
                        ? appData.filter(
                            (each) =>
                              each.value === formik.values.androidDriverAppUpdateStatus
                          )[0].label
                        : false
                    }
                    fields={appData}
                    error={formik.errors.androidDriverAppUpdateStatus}
                    change={(e) =>
                      formik.setFieldValue("androidDriverAppUpdateStatus", e)
                    }
                  />
                </FieldWrapper>
              </Section>
            </Flex>
            <p className="m-4 mt-2 my-0">{language.IOS}</p>
            <Flex>
              <Section padding={false}>
                <FieldWrapper animate="generalSettings" title={language.IOS_USER_VERSION}>
                  <ProtectedField
                    type={"number"}
                    value={formik.values.iosUserVersionKey}
                    change={(e) => formik.setFieldValue("iosUserVersionKey", e)}
                    error={formik.errors.iosUserVersionKey}
                  />
                </FieldWrapper>
                <FieldWrapper
                  animate="generalSettings"
                  title={language.IOS_DRIVER_VERSION}
                >
                  <ProtectedField
                    type={"number"}
                    value={formik.values.iosDriverVersionKey}
                    change={(e) => formik.setFieldValue("iosDriverVersionKey", e)}
                    error={formik.errors.iosDriverVersionKey}
                  />
                </FieldWrapper>
              </Section>
              <Section padding={false}>
                <FieldWrapper
                  animate="generalSettings"
                  title={language.IOS_USER_UPDATE_STATUS}
                >
                  <DropdownNormal
                    defaultValue={
                      appData.filter(
                        (each) => each.value === formik.values.iosUserAppUpdateStatus
                      ).length > 0
                        ? appData.filter(
                            (each) => each.value === formik.values.iosUserAppUpdateStatus
                          )[0].label
                        : false
                    }
                    fields={appData}
                    error={formik.errors.iosUserAppUpdateStatus}
                    change={(e) => formik.setFieldValue("iosUserAppUpdateStatus", e)}
                  />
                </FieldWrapper>
                <FieldWrapper
                  animate="generalSettings"
                  title={language.IOS_DRIVER_UPDATE_STATUS}
                >
                  <DropdownNormal
                    defaultValue={
                      appData.filter(
                        (each) => each.value === formik.values.iosDriverAppUpdateStatus
                      ).length > 0
                        ? appData.filter(
                            (each) =>
                              each.value === formik.values.iosDriverAppUpdateStatus
                          )[0].label
                        : false
                    }
                    fields={appData}
                    error={formik.errors.iosDriverAppUpdateStatus}
                    change={(e) => formik.setFieldValue("iosDriverAppUpdateStatus", e)}
                  />
                </FieldWrapper>
              </Section>
            </Flex>
          </Border>
        )}
        <Border>
          <div className="flex items-center justify-between">
            <Heading title={language.FCM_SETTINGS} />
            {admin.userType === "DEVELOPER" && (
              <div>
                {keys.fcm ? (
                  <FiX
                    onClick={() => setKeys({ ...keys, fcm: false })}
                    className="text-xl cursor-pointer mx-3"
                  />
                ) : (
                  <FiEdit
                    onClick={() => setKeys({ ...keys, fcm: true })}
                    className="text-xl cursor-pointer mx-3"
                  />
                )}
              </div>
            )}
          </div>
          <Flex>
            <Section>
              <FieldWrapper animate="generalSettings" title={language.SMS_TYPE}>
                <ProtectedField
                  Component={DropdownNormal}
                  defaultValue={
                    smsTypeData.filter((each) => each.value === formik.values.smsType)
                      .length > 0
                      ? smsTypeData.filter(
                          (each) => each.value === formik.values.smsType
                        )[0].label
                      : false
                  }
                  fields={smsTypeData}
                  error={formik.errors.smsType}
                  change={(e) => keys.fcm && formik.setFieldValue("smsType", e)}
                />
              </FieldWrapper>
            </Section>
            <Section></Section>
          </Flex>
          <Flex wrap={true}>
            <Section>
              <FieldWrapper
                animate="generalSettings"
                title={language.KEY_FOR_ANDROID_USER}
              >
                <ProtectedField
                  hidden={true}
                  value={formik.values.firebaseAdmin.androidUser}
                  change={(e) =>
                    keys.fcm && formik.setFieldValue("firebaseAdmin.androidUser", e)
                  }
                  error={
                    formik.errors.firebaseAdmin && formik.errors.firebaseAdmin.androidUser
                  }
                />
              </FieldWrapper>
              <FieldWrapper animate="generalSettings" title={language.KEY_FOR_IOS_USER}>
                <ProtectedField
                  hidden={true}
                  value={formik.values.firebaseAdmin.iosUser}
                  change={(e) =>
                    keys.fcm && formik.setFieldValue("firebaseAdmin.iosUser", e)
                  }
                  error={
                    formik.errors.firebaseAdmin && formik.errors.firebaseAdmin.iosUser
                  }
                />
              </FieldWrapper>
            </Section>
            <Section>
              <FieldWrapper
                animate="generalSettings"
                title={language.KEY_FOR_ANDROID_DRIVER}
              >
                <ProtectedField
                  hidden={true}
                  value={formik.values.firebaseAdmin.androidDriver}
                  change={(e) =>
                    keys.fcm && formik.setFieldValue("firebaseAdmin.androidDriver", e)
                  }
                  error={
                    formik.errors.firebaseAdmin &&
                    formik.errors.firebaseAdmin.androidDriver
                  }
                />
              </FieldWrapper>
              <FieldWrapper animate="generalSettings" title={language.KEY_FOR_IOS_DRIVER}>
                <ProtectedField
                  hidden={true}
                  value={formik.values.firebaseAdmin.iosDriver}
                  change={(e) =>
                    keys.fcm && formik.setFieldValue("firebaseAdmin.iosDriver", e)
                  }
                  error={
                    formik.errors.firebaseAdmin && formik.errors.firebaseAdmin.iosDriver
                  }
                />
              </FieldWrapper>
            </Section>
          </Flex>
        </Border>
        <Border>
          <div className="flex items-center justify-between">
            <Heading title={language.GOOGLE_MAP_WEB_API_SETTINGS} />
            {admin.userType === "DEVELOPER" && (
              <div>
                {keys.map.web ? (
                  <FiX
                    onClick={() => setKeys({ ...keys, map: { ...keys.map, web: false } })}
                    className="text-xl cursor-pointer mx-3"
                  />
                ) : (
                  <FiEdit
                    onClick={() => setKeys({ ...keys, map: { ...keys.map, web: true } })}
                    className="text-xl cursor-pointer mx-3"
                  />
                )}
              </div>
            )}
          </div>
          {formik.values.mapApiArray &&
            formik.values.mapApiArray.web &&
            formik.values.mapApiArray.web.map((each, idx) => (
              <>
                <Flex wrap={true} align={false}>
                  <Section>
                    <Flex className="flex">
                      <Section padding={false} width={"1/5"}>
                        <FieldWrapper title={language.ACTIVE}>
                          <ProtectedField
                            Component={ToggleButton}
                            hidden={true}
                            change={(e) => {
                              if (keys.map.web) {
                                if (e === 1) {
                                  if (formik.values?.mapApiArray?.web[idx]?.key)
                                    formik.setFieldValue(
                                      "mapApi.web",
                                      formik.values?.mapApiArray?.web[idx]?.key
                                    );
                                  else return alert(language.KEY_IS_EMPTY);
                                }
                                formik.values.mapApiArray.web.map((each, idx) =>
                                  formik.setFieldValue(
                                    "mapApiArray.web[" + idx + "].default",
                                    0
                                  )
                                );
                                setTimeout(
                                  () =>
                                    formik.setFieldValue(
                                      "mapApiArray.web[" + idx + "].default",
                                      e
                                    ),
                                  100
                                );
                              }
                            }}
                            defaultValue={
                              formik.values?.mapApiArray?.web[idx]?.default || 0
                            }
                          />
                        </FieldWrapper>
                      </Section>
                      <Section padding={false}>
                        <FieldWrapper title={language.LABEL}>
                          <ProtectedField
                            hidden={true}
                            value={formik.values?.mapApiArray?.web[idx]?.label || ""}
                            change={(e) =>
                              keys.map.web &&
                              formik.setFieldValue(
                                "mapApiArray.web[" + idx + "].label",
                                e
                              )
                            }
                            placeholder={language.LABEL}
                            error={
                              formik.errors.mapApiArray &&
                              formik.errors.mapApiArray.web &&
                              formik.errors.mapApiArray.web[idx] &&
                              formik.errors.mapApiArray.web[idx].label
                            }
                          />
                        </FieldWrapper>
                      </Section>
                    </Flex>
                  </Section>
                  <Section padding={false}>
                    <FieldWrapper title={language.KEY}>
                      <ProtectedField
                        hidden={true}
                        value={formik.values?.mapApiArray?.web[idx]?.key || ""}
                        change={(e) =>
                          keys.map.web &&
                          formik.setFieldValue("mapApiArray.web[" + idx + "].key", e)
                        }
                        placeholder={language.KEY}
                        error={
                          formik.errors.mapApiArray &&
                          formik.errors.mapApiArray.web &&
                          formik.errors.mapApiArray.web[idx] &&
                          formik.errors.mapApiArray.web[idx].key
                        }
                      />
                    </FieldWrapper>
                  </Section>
                </Flex>
              </>
            ))}
          <FieldWrapper>
            {keys.map.web && (
              <Button
                click={() =>
                  formik.setFieldValue("mapApiArray.web", [
                    ...(formik.values.mapApiArray &&
                    Array.isArray(formik.values.mapApiArray.web)
                      ? formik.values.mapApiArray.web
                      : []),
                    {
                      label: "",
                      key: "",
                      default: 0,
                      time: new Date(),
                    },
                  ])
                }
                title={language.ADD}
                icon={"+"}
              />
            )}
            {keys.map.web &&
              formik.values.mapApiArray &&
              formik.values.mapApiArray.web &&
              formik.values.mapApiArray.web.length > 1 &&
              formik.values.mapApiArray.web[formik.values.mapApiArray.web.length - 1] &&
              formik.values.mapApiArray.web[formik.values.mapApiArray.web.length - 1]
                .default === 0 && (
                <Button
                  icon={"-"}
                  onClick={() => {
                    formik.setFieldValue("mapApiArray.web", [
                      ...formik.values.mapApiArray.web.filter(
                        (each, idx) => formik.values.mapApiArray.web.length !== idx + 1
                      ),
                    ]);
                  }}
                  title={language.REMOVE}
                />
              )}
          </FieldWrapper>
        </Border>
        <Border>
          <div className="flex items-center justify-between">
            <Heading title={language.GOOGLE_MAP_ANDROID_API_SETTINGS} />
            {admin.userType === "DEVELOPER" && (
              <div>
                {keys && keys.map.android ? (
                  <FiX
                    onClick={() =>
                      setKeys({ ...keys, map: { ...keys.map, android: false } })
                    }
                    className="text-xl cursor-pointer mx-3"
                  />
                ) : (
                  <FiEdit
                    onClick={() =>
                      setKeys({ ...keys, map: { ...keys.map, android: true } })
                    }
                    className="text-xl cursor-pointer mx-3"
                  />
                )}
              </div>
            )}
          </div>
          {formik.values.mapApiArray &&
            formik.values.mapApiArray.android &&
            formik.values.mapApiArray.android.map((each, idx) => (
              <>
                <Flex wrap={true} align={false}>
                  <Section>
                    <Flex className="flex">
                      <Section padding={false} width={"1/5"}>
                        <FieldWrapper title={language.ACTIVE}>
                          <ProtectedField
                            Component={ToggleButton}
                            change={(e) => {
                              if (keys && keys.map.android) {
                                if (e === 1) {
                                  if (formik.values?.mapApiArray?.android[idx]?.key)
                                    formik.setFieldValue(
                                      "mapApi.android",
                                      formik.values?.mapApiArray?.android[idx]?.key
                                    );
                                  else return alert(language.KEY_IS_EMPTY);
                                }
                                formik.values.mapApiArray.android.map((each, idx) =>
                                  formik.setFieldValue(
                                    "mapApiArray.android[" + idx + "].default",
                                    0
                                  )
                                );
                                setTimeout(
                                  () =>
                                    formik.setFieldValue(
                                      "mapApiArray.android[" + idx + "].default",
                                      e
                                    ),
                                  100
                                );
                              }
                            }}
                            defaultValue={
                              formik.values?.mapApiArray?.android[idx]?.default || 0
                            }
                          />
                        </FieldWrapper>
                      </Section>
                      <Section padding={false}>
                        <FieldWrapper title={language.LABEL}>
                          <ProtectedField
                            hidden={true}
                            value={formik.values?.mapApiArray?.android[idx]?.label || ""}
                            change={(e) =>
                              keys &&
                              keys.map.android &&
                              formik.setFieldValue(
                                "mapApiArray.android[" + idx + "].label",
                                e
                              )
                            }
                            placeholder={language.LABEL}
                            error={
                              formik.errors.mapApiArray &&
                              formik.errors.mapApiArray.android &&
                              formik.errors.mapApiArray.android[idx] &&
                              formik.errors.mapApiArray.android[idx].label
                            }
                          />
                        </FieldWrapper>
                      </Section>
                    </Flex>
                  </Section>
                  <Section padding={false}>
                    <FieldWrapper title={language.KEY}>
                      <ProtectedField
                        hidden={true}
                        value={formik.values?.mapApiArray?.android[idx]?.key || ""}
                        change={(e) =>
                          keys &&
                          keys.map.android &&
                          formik.setFieldValue("mapApiArray.android[" + idx + "].key", e)
                        }
                        placeholder={language.KEY}
                        error={
                          formik.errors.mapApiArray &&
                          formik.errors.mapApiArray.android &&
                          formik.errors.mapApiArray.android[idx] &&
                          formik.errors.mapApiArray.android[idx].key
                        }
                      />
                    </FieldWrapper>
                  </Section>
                </Flex>
              </>
            ))}
          <FieldWrapper>
            {keys && keys.map.android && (
              <Button
                click={() =>
                  formik.setFieldValue("mapApiArray.android", [
                    ...(formik.values.mapApiArray &&
                    Array.isArray(formik.values.mapApiArray.android)
                      ? formik.values.mapApiArray.android
                      : []),
                    {
                      label: "",
                      key: "",
                      default: 0,
                      time: new Date(),
                    },
                  ])
                }
                title={language.ADD}
                icon={"+"}
              />
            )}
            {keys &&
              keys.map.android &&
              formik.values.mapApiArray &&
              formik.values.mapApiArray.android &&
              formik.values.mapApiArray.android.length > 1 &&
              formik.values.mapApiArray.android[
                formik.values.mapApiArray.android.length - 1
              ] &&
              formik.values.mapApiArray.android[
                formik.values.mapApiArray.android.length - 1
              ].default === 0 && (
                <Button
                  icon={"-"}
                  onClick={() => {
                    formik.setFieldValue("mapApiArray.android", [
                      ...formik.values.mapApiArray.android.filter(
                        (each, idx) =>
                          formik.values.mapApiArray.android.length !== idx + 1
                      ),
                    ]);
                  }}
                  title={language.REMOVE}
                />
              )}
          </FieldWrapper>
        </Border>
        <Border>
          <div className="flex items-center justify-between">
            <Heading title={language.GOOGLE_MAP_IOS_API_SETTINGS} />
            {admin.userType === "DEVELOPER" && (
              <div>
                {keys.map.ios ? (
                  <FiX
                    onClick={() => setKeys({ ...keys, map: { ...keys.map, ios: false } })}
                    className="text-xl cursor-pointer mx-3"
                  />
                ) : (
                  <FiEdit
                    onClick={() => setKeys({ ...keys, map: { ...keys.map, ios: true } })}
                    className="text-xl cursor-pointer mx-3"
                  />
                )}
              </div>
            )}
          </div>
          {formik.values.mapApiArray &&
            formik.values.mapApiArray.ios &&
            formik.values.mapApiArray.ios.map((each, idx) => (
              <>
                <Flex wrap={true} align={false}>
                  <Section>
                    <Flex className="flex">
                      <Section padding={false} width={"1/5"}>
                        <FieldWrapper title={language.ACTIVE}>
                          <ProtectedField
                            Component={ToggleButton}
                            change={(e) => {
                              if (keys.map.ios) {
                                if (e === 1) {
                                  if (formik.values?.mapApiArray?.ios[idx]?.key)
                                    formik.setFieldValue(
                                      "mapApi.ios",
                                      formik.values?.mapApiArray?.ios[idx]?.key
                                    );
                                  else return alert(language.KEY_IS_EMPTY);
                                }
                                formik.values.mapApiArray.ios.map((each, idx) =>
                                  formik.setFieldValue(
                                    "mapApiArray.ios[" + idx + "].default",
                                    0
                                  )
                                );
                                setTimeout(
                                  () =>
                                    formik.setFieldValue(
                                      "mapApiArray.ios[" + idx + "].default",
                                      e
                                    ),
                                  100
                                );
                              }
                            }}
                            defaultValue={
                              formik.values?.mapApiArray?.ios[idx]?.default || 0
                            }
                          />
                        </FieldWrapper>
                      </Section>
                      <Section padding={false}>
                        <FieldWrapper title={language.LABEL}>
                          <ProtectedField
                            hidden={true}
                            value={formik.values?.mapApiArray?.ios[idx]?.label || ""}
                            change={(e) =>
                              keys.map.ios &&
                              formik.setFieldValue(
                                "mapApiArray.ios[" + idx + "].label",
                                e
                              )
                            }
                            placeholder={language.LABEL}
                            error={
                              formik.errors.mapApiArray &&
                              formik.errors.mapApiArray.ios &&
                              formik.errors.mapApiArray.ios[idx] &&
                              formik.errors.mapApiArray.ios[idx].label
                            }
                          />
                        </FieldWrapper>
                      </Section>
                    </Flex>
                  </Section>
                  <Section padding={false}>
                    <FieldWrapper title={language.KEY}>
                      <ProtectedField
                        hidden={true}
                        value={formik.values?.mapApiArray?.ios[idx]?.key || ""}
                        change={(e) =>
                          keys.map.ios &&
                          formik.setFieldValue("mapApiArray.ios[" + idx + "].key", e)
                        }
                        placeholder={language.KEY}
                        error={
                          formik.errors.mapApiArray &&
                          formik.errors.mapApiArray.ios &&
                          formik.errors.mapApiArray.ios[idx] &&
                          formik.errors.mapApiArray.ios[idx].key
                        }
                      />
                    </FieldWrapper>
                  </Section>
                </Flex>
              </>
            ))}
          <FieldWrapper>
            {keys.map.ios && (
              <Button
                click={() =>
                  formik.setFieldValue("mapApiArray.ios", [
                    ...(formik.values.mapApiArray &&
                    Array.isArray(formik.values.mapApiArray.ios)
                      ? formik.values.mapApiArray.ios
                      : []),
                    {
                      label: "",
                      key: "",
                      default: 0,
                      time: new Date(),
                    },
                  ])
                }
                title={language.ADD}
                icon={"+"}
              />
            )}
            {keys.map.ios &&
              formik.values.mapApiArray &&
              formik.values.mapApiArray.ios &&
              formik.values.mapApiArray.ios.length > 1 &&
              formik.values.mapApiArray.ios[formik.values.mapApiArray.ios.length - 1] &&
              formik.values.mapApiArray.ios[formik.values.mapApiArray.ios.length - 1]
                .default === 0 && (
                <Button
                  icon={"-"}
                  onClick={() => {
                    formik.setFieldValue("mapApiArray.ios", [
                      ...formik.values.mapApiArray.ios.filter(
                        (each, idx) => formik.values.mapApiArray.ios.length !== idx + 1
                      ),
                    ]);
                  }}
                  title={language.REMOVE}
                />
              )}
          </FieldWrapper>
        </Border>
        <Border>
          <div className="flex items-center justify-between">
            <Heading title={language.DIGITAL_OCEAN_SPACES} />
            {admin.userType === "DEVELOPER" && (
              <div>
                {keys.space ? (
                  <FiX
                    onClick={() => setKeys({ ...keys, space: false })}
                    className="text-xl cursor-pointer mx-3"
                  />
                ) : (
                  <FiEdit
                    onClick={() => setKeys({ ...keys, space: true })}
                    className="text-xl cursor-pointer mx-3"
                  />
                )}
              </div>
            )}
          </div>
          <Flex wrap={true} align={false}>
            <Section>
              <FieldWrapper animate="generalSettings" title={language.KEY}>
                <ProtectedField
                  hidden={true}
                  value={formik.values.spaces.spacesKey}
                  change={(e) =>
                    keys.space && formik.setFieldValue("spaces.spacesKey", e)
                  }
                  error={formik.errors.spaces && formik.errors.spaces.spacesKey}
                />
              </FieldWrapper>
              <FieldWrapper animate="generalSettings" title={language.SECRET}>
                <ProtectedField
                  hidden={true}
                  value={formik.values.spaces.spacesSecret}
                  change={(e) =>
                    keys.space && formik.setFieldValue("spaces.spacesSecret", e)
                  }
                  error={formik.errors.spaces && formik.errors.spaces.spacesSecret}
                />
              </FieldWrapper>
              <FieldWrapper animate="generalSettings" title={language.BUCKET_NAME}>
                <ProtectedField
                  hidden={true}
                  value={formik.values.spaces.spacesBucketName}
                  change={(e) =>
                    keys.space && formik.setFieldValue("spaces.spacesBucketName", e)
                  }
                  error={formik.errors.spaces && formik.errors.spaces.spacesBucketName}
                />
              </FieldWrapper>
            </Section>
            <Section>
              <FieldWrapper animate="generalSettings" title={language.ENDPOINT}>
                <ProtectedField
                  hidden={true}
                  value={formik.values.spaces.spacesEndpoint}
                  change={(e) =>
                    keys.space && formik.setFieldValue("spaces.spacesEndpoint", e)
                  }
                  error={formik.errors.spaces && formik.errors.spaces.spacesEndpoint}
                />
              </FieldWrapper>
              <FieldWrapper animate="generalSettings" title={language.BASE_URL}>
                <ProtectedField
                  hidden={true}
                  value={formik.values.spaces.spacesBaseUrl}
                  change={(e) =>
                    keys.space && formik.setFieldValue("spaces.spacesBaseUrl", e)
                  }
                  error={formik.errors.spaces && formik.errors.spaces.spacesBaseUrl}
                />
              </FieldWrapper>
              <FieldWrapper animate="generalSettings" title={language.FOLDER_NAME}>
                <ProtectedField
                  hidden={true}
                  value={formik.values.spaces.spacesObjectName}
                  change={(e) =>
                    keys.space && formik.setFieldValue("spaces.spacesObjectName", e)
                  }
                  error={formik.errors.spaces && formik.errors.spaces.spacesObjectName}
                />
              </FieldWrapper>
            </Section>
          </Flex>
        </Border>
        <Border>
          <Heading title={language.STORE_DETAILS} />
          <Flex wrap={true} align={false}>
            <Section>
              <FieldWrapper animate="generalSettings" title={language.USER_PLAY_STORE}>
                <ProtectedField
                  value={formik.values.redirectUrls.userPlayStore}
                  change={(e) => formik.setFieldValue("redirectUrls.userPlayStore", e)}
                  error={
                    formik.errors.redirectUrls && formik.errors.redirectUrls.userPlayStore
                  }
                />
              </FieldWrapper>
              <FieldWrapper animate="generalSettings" title={language.DRIVER_PLAY_STORE}>
                <ProtectedField
                  value={formik.values.redirectUrls.driverPlayStore}
                  change={(e) => formik.setFieldValue("redirectUrls.driverPlayStore", e)}
                  error={
                    formik.errors.redirectUrls &&
                    formik.errors.redirectUrls.driverPlayStore
                  }
                />
              </FieldWrapper>{" "}
              <FieldWrapper animate="generalSettings" title={language.USER_APPLE_ID}>
                <ProtectedField
                  value={formik.values.redirectUrls.userAppleId}
                  change={(e) => formik.setFieldValue("redirectUrls.userAppleId", e)}
                  error={
                    formik.errors.redirectUrls && formik.errors.redirectUrls.userAppleId
                  }
                />
              </FieldWrapper>{" "}
            </Section>
            <Section>
              <FieldWrapper animate="generalSettings" title={language.USER_APP_STORE}>
                <ProtectedField
                  value={formik.values.redirectUrls.userAppstore}
                  change={(e) => formik.setFieldValue("redirectUrls.userAppstore", e)}
                  error={
                    formik.errors.redirectUrls && formik.errors.redirectUrls.userAppstore
                  }
                />
              </FieldWrapper>
              <FieldWrapper animate="generalSettings" title={language.DRIVER_APP_STORE}>
                <ProtectedField
                  value={formik.values.redirectUrls.driverAppstore}
                  change={(e) => formik.setFieldValue("redirectUrls.driverAppstore", e)}
                  error={
                    formik.errors.redirectUrls &&
                    formik.errors.redirectUrls.driverAppstore
                  }
                />
              </FieldWrapper>{" "}
              <FieldWrapper animate="generalSettings" title={language.DRIVER_APPLE_ID}>
                <ProtectedField
                  value={formik.values.redirectUrls.driverAppleId}
                  change={(e) => formik.setFieldValue("redirectUrls.driverAppleId", e)}
                  error={
                    formik.errors.redirectUrls && formik.errors.redirectUrls.driverAppleId
                  }
                />
              </FieldWrapper>{" "}
            </Section>
          </Flex>
        </Border>

        <Border>
          <Heading title={language.APP_CONFIGURE} />
          <Flex>
            <Section></Section>
            <Section></Section>
          </Flex>
          <Flex wrap={true} align={false}>
            <Section>
              <FieldWrapper animate="generalSettings" title={language.FAQ_URL}>
                <TextField
                  value={formik.values.redirectUrls.faqUrl}
                  change={(e) => formik.setFieldValue("redirectUrls.faqUrl", e)}
                  error={formik.errors.redirectUrls && formik.errors.redirectUrls.faqUrl}
                />
              </FieldWrapper>
              <FieldWrapper animate="generalSettings" title={language.ABOUT_URL}>
                <TextField
                  value={formik.values.redirectUrls.aboutUrl}
                  change={(e) => formik.setFieldValue("redirectUrls.aboutUrl", e)}
                  error={
                    formik.errors.redirectUrls && formik.errors.redirectUrls.aboutUrl
                  }
                />
              </FieldWrapper>
            </Section>
            <Section>
              <FieldWrapper
                animate="generalSettings"
                title={language.TERMS_AND_CONDITIONS_URL}
              >
                <TextField
                  value={formik.values.redirectUrls.termsAndConditionUrl}
                  change={(e) =>
                    formik.setFieldValue("redirectUrls.termsAndConditionUrl", e)
                  }
                  error={
                    formik.errors.redirectUrls &&
                    formik.errors.redirectUrls.termsAndConditionUrl
                  }
                />
              </FieldWrapper>{" "}
              <FieldWrapper animate="generalSettings" title={language.PRIVACY_URL}>
                <TextField
                  value={formik.values.redirectUrls.privacyUrl}
                  change={(e) => formik.setFieldValue("redirectUrls.privacyUrl", e)}
                  error={
                    formik.errors.redirectUrls && formik.errors.redirectUrls.privacyUrl
                  }
                />
              </FieldWrapper>{" "}
            </Section>
          </Flex>
        </Border>

        <Button
          loading={btnLoading}
          icon={<FiSave />}
          click={formik.handleSubmit}
          title={language.SUBMIT}
        />
      </Section>
    </FormWrapper>
  );
}
