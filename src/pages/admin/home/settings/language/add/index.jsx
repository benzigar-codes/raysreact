import { useFormik } from "formik";
import React from "react";
import * as yup from "yup";
import { HiTranslate } from "react-icons/hi";
import gsap from "gsap";
import axios from "axios";

import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import Flex from "../../../../../../components/common/Flex";
import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { TextField } from "../../../../../../components/common/TextField";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";
import useLanguage from "../../../../../../hooks/useLanguage";

import NavLinks from "../../../../../../utils/navLinks.json";
import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";
import Lang_keys_JSON from "../../../../../../utils/language.json";

import useAdmin from "../../../../../../hooks/useAdmin";
import { PopUp } from "../../../../../../components/common/PopUp";
import useUtils from "../../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { parseError } = useUtils();
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);

  const validationSchema = yup.object().shape({
    lang_code: yup.string().required(language.REQUIRED),
    lang_name: yup.string().required(language.REQUIRED),
    status: yup.number().required(language.REQUIRED),
    default_status: yup.number().required(language.REQUIRED),
    lang_direction: yup.number().required(language.REQUIRED),
  });

  const submit = async (e) => {
    setBtnLoading(true);
    try {
      await axios.post(
        A.HOST + A.ADMIN_LANGUAGE_UPDATE,
        {
          data: {
            languageName: formik.values.lang_name,
            languageCode: formik.values.lang_code,
            languageDefault: false,
            languageDirection: formik.values.lang_direction === 1 ? "RTL" : "LTR",
            languageKeys: Lang_keys_JSON,
            status: U.ACTIVE,
          },
        },
        header
      );
      history.goBack();
    } catch (err) {
      authFailure(err);
      setPop({ title: parseError(err), type: "error" });
    }
    setBtnLoading(false);
  };
  React.useEffect(() => {
    gsap.fromTo(".languageAdd", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, []);
  const formik = useFormik({
    initialValues: {
      lang_code: "",
      lang_name: "",
      status: 1,
      default_status: 1,
      lang_direction: 0,
    },
    validateOnChange: false,
    validationSchema,
    onSubmit: submit,
  });
  return (
    <FormWrapper
      animate="languageAdd"
      width={"5/6"}
      icon={<HiTranslate />}
      title={language.ADD}
      submit={formik.handleSubmit}
      submitBtn={true}
      btnLoading={btnLoading}
      bread={[
        {
          id: 1,
          title: language.LANGUAGE,
          path: NavLinks.ADMIN_LANGUAGE,
        },
      ]}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}

      <Section>
        <FieldWrapper animate="languageAdd" title={language.NAME}>
          <TextField
            change={(e) => formik.setFieldValue("lang_name", e)}
            value={formik.values.lang_name}
            error={formik.errors.lang_name}
          />
        </FieldWrapper>
        <FieldWrapper animate="languageAdd" title={language.LANGUAGE_CODE}>
          <TextField
            change={(e) => formik.setFieldValue("lang_code", e.toLowerCase())}
            value={formik.values.lang_code}
            error={formik.errors.lang_code}
          />
        </FieldWrapper>
      </Section>
      <Section>
        <Flex>
          <Section>
            <FieldWrapper animate="languageAdd" title={language.RIGHT_TO_LEFT}>
              <ToggleButton
                change={(e) => formik.setFieldValue("lang_direction", e === 1 ? 1 : 2)}
                defaultValue={formik.values.lang_direction}
              />
            </FieldWrapper>
          </Section>
        </Flex>
      </Section>
    </FormWrapper>
  );
}
