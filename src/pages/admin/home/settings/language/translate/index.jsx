import React from "react";
import { HiTranslate } from "react-icons/hi";
import gsap from "gsap";
import serialize from "form-serialize";
import axios from "axios";
import { useParams } from "react-router-dom";
import _ from "lodash";

import NavLinks from "../../../../../../utils/navLinks.json";
import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";

import LangJSON from "../../../../../../utils/language.json";

import useLanguage from "../../../../../../hooks/useLanguage";

import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import Flex from "../../../../../../components/common/Flex";
import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { TextField } from "../../../../../../components/common/TextField";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import { Button } from "../../../../../../components/common/Button";
import useAdmin from "../../../../../../hooks/useAdmin";
import useDebug from "../../../../../../hooks/useDebug";
import { PopUp } from "../../../../../../components/common/PopUp";
import useUtils from "../../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { parseError } = useUtils();
  const [btnLoading, setBtnLoading] = React.useState(false);

  const [loading, setLoading] = React.useState(true);
  const [popup, setPop] = React.useState(null);
  const [languageJSON, setLanguageJSON] = React.useState(LangJSON);

  const { id } = useParams();
  const [lang, setLang] = React.useState({});

  const submit = async (e) => {
    e.preventDefault();
    setBtnLoading(true);
    var form = document.querySelector("#translateForm");
    var obj = serialize(form, { hash: true });
    try {
      await axios.post(
        A.HOST + A.ADMIN_LANGUAGE_TRANSLATE,
        {
          id,
          languageKeys: obj.lang,
        },
        header
      );
      history.push(NavLinks.ADMIN_LANGUAGE);
    } catch (err) {
      setPop({ title: parseError(err), type: "error" });
    }
    setBtnLoading(false);
  };

  React.useEffect(() => {
    const fetchLanguage = async () => {
      try {
        let languageFromJSONCopy = { ...languageJSON };
        const { data } = await axios.post(A.HOST + A.ADMIN_LANGUAGE_READ, { id }, header);
        setLang({ ...data.data, languageKeys: _.merge(languageFromJSONCopy, data.data.languageKeys) });
        setLoading(false);
        gsap.fromTo(".languageAdd", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
      } catch (err) {
        authFailure(err);
        history.goBack();
      }
    };
    fetchLanguage();
  }, []);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <form id="translateForm" onSubmit={submit} method={"Post"} action="#">
      <FormWrapper
        animate="languageAdd"
        bread={[
          {
            id: 1,
            title: language.LANGUAGE,
            path: NavLinks.ADMIN_LANGUAGE,
          },
          {
            id: 2,
            title: lang.languageName,
          },
        ]}
        width={"4/5"}
        icon={<HiTranslate />}
        title={language.TRANSLATE}
        submitBtn={true}
        btnLoading={btnLoading}
      >
        {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}

        <Section>
          {lang.languageKeys && (
            <Flex align={false} wrap={true}>
              <Section>
                {Object.keys(lang.languageKeys)
                  .filter((la, index) => index % 2 === 0)
                  .map((l) => (
                    <FieldWrapper title={languageJSON[l]}>
                      <TextField name={`lang[${l}]`} defaultValue={lang.languageKeys[l]} />
                    </FieldWrapper>
                  ))}
              </Section>
              <Section>
                {Object.keys(lang.languageKeys)
                  .filter((la, index) => index % 2 !== 0)
                  .map((l) => (
                    <FieldWrapper title={languageJSON[l]}>
                      <TextField name={`lang[${l}]`} defaultValue={lang.languageKeys[l]} />
                    </FieldWrapper>
                  ))}
              </Section>
            </Flex>
          )}
          <Button loading={btnLoading} title={language.SUBMIT} className="mb-4" />
        </Section>
      </FormWrapper>
    </form>
  );
}
