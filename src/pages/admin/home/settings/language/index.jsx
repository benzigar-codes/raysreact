import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  return (
    <Table
      title={language.LANGUAGE}
      startingHeadings={[
        {
          id: 1,
          title: language.NAME,
          key: "lang_name",
          show: true,
        },
        {
          id: 2,
          title: language.LANGUAGE_CODE,
          key: "lang_code",
          show: true,
        },
        {
          id: 3,
          title: language.LANGUAGE_DIRECTION,
          key: "lang_prefix",
          show: true,
        },
        {
          id: 4,
          title: language.STATUS,
          key: "status",
          show: true,
        },
        {
          id: 5,
          title: language.DEFAULT,
          key: "default_status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_LANGUAGE_LIST}
      assignData={(data) =>
        data.map((lang) => ({
          _id: lang._id,
          lang_name: lang.data.languageName,
          lang_code: lang.data.languageCode,
          lang_prefix: lang.data.languageDirection,
          default_status: lang.data.languageDefault === true ? 1 : 0,
          status: lang.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      bread={[{ id: 1, title: language.LANGUAGE }]}
      showAdd={admin.privileges.SETTINGS.LANGUAGE ? (admin.privileges.SETTINGS.LANGUAGE.ADD ? true : false) : false}
      showArchieve={false}
      showEdit={admin.privileges.SETTINGS.LANGUAGE ? (admin.privileges.SETTINGS.LANGUAGE.EDIT ? true : false) : false}
      addClick={() => history.push(NavLinks.ADMIN_LANGUAGE_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_LANGUAGE_EDIT + "/" + e)}
      showBulk={false}
      showSearch={true}
      showAction={admin.privileges.SETTINGS.LANGUAGE ? (admin.privileges.SETTINGS.LANGUAGE.EDIT ? true : false) : false}
      add={admin.privileges.SETTINGS.LANGUAGE ? (admin.privileges.SETTINGS.LANGUAGE.ADD ? true : false) : false}
      edit={admin.privileges.SETTINGS.LANGUAGE ? (admin.privileges.SETTINGS.LANGUAGE.EDIT ? true : false) : false}
      statusList={A.HOST + A.ADMIN_LANGUAGE_STATUS}
      defaultLink={A.HOST + A.ADMIN_LANGUAGE_DEFAULT}
      showTranslate={true}
      translateClick={(e) => history.push(NavLinks.ADMIN_LANGUAGE_TRANSLATE + "/" + e)}
    />
  );
}
