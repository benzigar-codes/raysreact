import React from "react";

const Text = ({ title, ...rest }) => {
  const direction = document
    .getElementsByTagName("html")[0]
    .getAttribute("dir");
  return (
    <p
      className={`${direction === "ltr" ? "pl-3" : "pr-3"} text-sm cursor-pointer text-gray-600 dark:text-gray-200`}
      {...rest}
    >
      {title}
    </p>
  );
};

export default Text
