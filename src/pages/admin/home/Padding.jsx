import React from "react";

export default function Padding({children, ...rest}) {
    return <div className="p-5" {...rest}>{children}</div>;
}
