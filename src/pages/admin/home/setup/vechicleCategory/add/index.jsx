import { useFormik } from "formik";
import React from "react";
import gsap from "gsap/gsap-core";
import * as yup from "yup";
import { AiOutlineCar } from "react-icons/ai";

import useLanguage from "../../../../../../hooks/useLanguage";
import useImage from "../../../../../../hooks/useImage";

import NavLinks from "../../../../../../utils/navLinks.json";
import U from "../../../../../../utils/utils.js";
import A from "../../../../../../utils/API.js";

import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import Flex from "../../../../../../components/common/Flex";
import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { TextField } from "../../../../../../components/common/TextField";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";
import { FileUpload } from "../../../../../../components/common/FileUpload";
import { PopUp } from "../../../../../../components/common/PopUp";
import axios from "axios";
import useAdmin from "../../../../../../hooks/useAdmin";
import useUtils from "../../../../../../hooks/useUtils";

export default function Index({ history }) {
  // HOOKS
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { compressImage } = useImage();
  const { parseError } = useUtils();
  // STATES
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);
  const [vehicle, setVehicle] = React.useState({
    category: "",
    seat_count: "",
    share_status: 0,
    mandatory_share: 0,
    share_percent: 0,
    status: 1,
    category_image: "",
    category_map_image: "",
  });
  const validationSchema = yup.object().shape({
    category: yup
      .string()
      .min(2, language.MIN + "2")
      .required(language.REQUIRED),
    seat_count: yup
      .number()
      .min(1, language.MIN + "1")
      .max(100, language.MAX + "100")
      .required(language.REQUIRED),
    share_status: yup.number().required(language.REQUIRED),
    mandatory_share: yup.number().required(language.REQUIRED),
    share_percent: yup
      .number()
      .min(0, language.MIN + "0")
      .max(100, language.MAX + "100")
      .required(language.REQUIRED),
    status: yup.number().required(language.REQUIRED),
    category_image: yup.string().required(language.REQUIRED),
    category_map_image: yup.string().required(language.REQUIRED),
  });
  const submit = async (e) => {
    setBtnLoading(true);
    const category_image = await compressImage(e.category_image);
    const category_map_image = await compressImage(e.category_map_image);
    const data = new FormData();
    data.append("vehicleCategory", e.category);
    data.append("mandatoryShare", e.mandatory_share === 1 ? true : false);
    data.append("seatCount", parseInt(e.seat_count));
    data.append("sharePercent", parseInt(e.share_percent));
    data.append("shareStatus", e.share_status === 1 ? true : false);
    data.append("status", U.ACTIVE);
    data.append("categoryImage", category_image);
    data.append("categoryMapImage", category_map_image);
    try {
      await axios.post(A.HOST + A.ADMIN_VEHICLE_CATEGORY_UPDATE, data, header);
      history.goBack();
    } catch (err) {
      authFailure(err);
      setPop({ title: parseError(err), type: "error" });
    }
    setBtnLoading(false);
  };
  const formik = useFormik({
    initialValues: vehicle,
    validationSchema,
    validateOnChange: false,
    onSubmit: submit,
  });
  React.useEffect(() => {
    gsap.fromTo(".addCategory", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, []);
  return (
    <FormWrapper
      animate={"addCategory"}
      width="3/4"
      bread={[
        {
          id: 1,
          title: language.VEHICLE_CATEGORY,
          path: NavLinks.ADMIN_SETUP_VECHICLE_CATEGORY,
        },
      ]}
      btnLoading={btnLoading}
      submit={formik.handleSubmit}
      submitBtn={true}
      icon={<AiOutlineCar />}
      title={language.ADD + " " + language.CATEGORY}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <Section>
        <FieldWrapper animate={"addCategory"} title={language.CATEGORY}>
          <TextField
            change={(e) => formik.setFieldValue("category", e.toLowerCase())}
            value={formik.values.category}
            error={formik.errors.category}
          />
        </FieldWrapper>
        <FieldWrapper animate={"addCategory"} title={language.VEHICLE_SEATING_CAPACITY}>
          <TextField
            type={"number"}
            change={(e) => formik.setFieldValue("seat_count", e)}
            value={formik.values.seat_count}
            error={formik.errors.seat_count}
          />
        </FieldWrapper>
        <Flex>
          <Section padding={false}>
            <FieldWrapper animate={"addCategory"} title={language.SHAREABLE}>
              <ToggleButton
                change={(e) => formik.setFieldValue("share_status", e)}
                defaultValue={formik.values.share_status}
              />
            </FieldWrapper>
          </Section>
        </Flex>

        {formik.values.share_status === 1 && (
          <>
            <FieldWrapper title={language.MANDATORY_SHARE_FOR_ALL_DRIVERS}>
              <ToggleButton
                change={(e) => formik.setFieldValue("mandatory_share", e)}
                defaultValue={formik.values.mandatory_share}
              />
            </FieldWrapper>
            <FieldWrapper title={language.SHARE_PERCENT}>
              <TextField
                defaultValue={formik.values.share_percent}
                change={(e) => formik.setFieldValue("share_percent", e)}
                errors={formik.errors.share_percent}
                type={"number"}
              />
            </FieldWrapper>
          </>
        )}
      </Section>
      <Section>
        <FieldWrapper hint={"something"} animate={"addCategory"} title={language.CATEGORY_IMAGE}>
          <FileUpload crop={false} change={(e) => formik.setFieldValue("category_image", e)} />
        </FieldWrapper>
      </Section>
      <Section>
        <FieldWrapper animate={"addCategory"} title={language.CATEGORY_MAP_IMAGE}>
          <div>
            <FileUpload crop={false} change={(e) => formik.setFieldValue("category_map_image", e)} />
            <div className="flex flex-col mx-2">
              <p className="my-3 text-sm text-gray-500">{language.VEHICLE_HINT_1}</p>
              <p className="my-3 text-sm text-gray-500">{language.VEHICLE_HINT_2}</p>
              <p className="my-3 text-sm text-gray-500">{language.VEHICLE_HINT_3}</p>
            </div>
          </div>
        </FieldWrapper>
      </Section>
    </FormWrapper>
  );
}
