// import React from "react";
// import { useParams } from "react-router-dom";

// import NavLinks from "../../../../../../utils/navLinks.json";
// import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
// import { FormWrapper } from "../../../../../../components/common/FormWrapper";
// import { Section } from "../../../../../../components/common/Section";
// import { TextField } from "../../../../../../components/common/TextField";
// import useLanguage from "../../../../../../hooks/useLanguage";
// import useFetch from "../../../../../../hooks/useFetch";
// import useFetchVechicleCategories from "../../../../../../hooks/api/useFetchVechicleCategories";
// import { SmallLoader } from "../../../../../../components/common/SmallLoader";
// import { useFormik } from "formik";
// import Flex from "../../../../../../components/common/Flex";
// import gsap from "gsap/gsap-core";

// export default function Index({ history }) {
//   const { id } = useParams();
//   const { language } = useLanguage();
//   const {
//     fetchVechicleCategoriesReadOne,
//     fetchVechicleCategoriesTranslate,
//   } = useFetchVechicleCategories();

//   const [langList, setLangLists] = React.useState([]);
//   const [vehicle, setVehicle] = React.useState({});

//   const [loading, setLoading] = React.useState(true);
//   const [btnLoading, setBtnLoading] = React.useState(false);
//   const { fetchAPILanguageList } = useFetch();

//   const submit = async (e) => {
//     setBtnLoading(true);
//     const { status } = await fetchVechicleCategoriesTranslate({
//       _id: vehicle._id,
//       data: e,
//     });
//     if (status === 200) history.goBack();
//     setBtnLoading(false);
//   };

//   const formik = useFormik({
//     initialValues: langList,
//     enableReinitialize: true,
//     onSubmit: submit,
//   });

//   React.useEffect(() => {
//     let LangLists = [];
//     const fetchEssentials = async () => {
//       const { status, data } = await fetchAPILanguageList();
//       if (status === 200)
//         LangLists = data.map((eachlang) => ({
//           lang_name: eachlang.lang_name,
//           category_name: "",
//         }));

//       const vehicleCategory = await fetchVechicleCategoriesReadOne(id);
//       setVehicle(vehicleCategory.data);
//       if (vehicleCategory.data.lang_trans)
//         LangLists = { ...LangLists, ...vehicleCategory.data.lang_trans };
//       LangLists = Object.keys(LangLists).map((eachLang) => LangLists[eachLang]);
//       setLangLists(LangLists);
//       setLoading(false);
//       gsap.fromTo(".trans", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
//     };
//     fetchEssentials();
//   }, []);

//   return loading === true ? (
//     <SmallLoader />
//   ) : (
//     <FormWrapper
//       animate={"trans"}
//       submit={formik.handleSubmit}
//       submitBtn={true}
//       btnLoading={btnLoading}
//       bread={[
//         {
//           id: 1,
//           title: language.SubMenu.VEHICLE_CATEGORY,
//           path: NavLinks.ADMIN_SETUP_VECHICLE_CATEGORY,
//         },
//       ]}
//       width="3/4"
//       title={language.Common.TRANSLATE + " " + vehicle.category}
//     >
//       <Flex align={false}>
//         <Section>
//           {formik.values.map(
//             (lang, index) =>
//               index % 2 === 0 && (
//                 <FieldWrapper
//                   animate={"trans"}
//                   key={lang.lang_name}
//                   title={lang.lang_name}
//                 >
//                   <TextField
//                     change={(e) =>
//                       formik.setFieldValue(`[${index}].category_name`, e)
//                     }
//                     value={lang.category_name}
//                   />
//                 </FieldWrapper>
//               )
//           )}
//         </Section>
//         <Section>
//           {formik.values.map(
//             (lang, index) =>
//               index % 2 !== 0 && (
//                 <FieldWrapper
//                   animate={"trans"}
//                   key={lang.lang_name}
//                   title={lang.lang_name}
//                 >
//                   <TextField
//                     change={(e) =>
//                       formik.setFieldValue(`[${index}].category_name`, e)
//                     }
//                     value={lang.category_name}
//                   />
//                 </FieldWrapper>
//               )
//           )}
//         </Section>
//       </Flex>
//     </FormWrapper>
//   );
// }
