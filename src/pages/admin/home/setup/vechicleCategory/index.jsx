import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  return (
    <Table
      title={language.VEHICLE_CATEGORY}
      startingHeadings={[
        {
          id: 1,
          title: language.CATEGORY,
          key: "category",
          show: true,
        },
        {
          id: 2,
          title: language.SHAREABLE,
          key: "sharable",
          show: true,
        },
        {
          id: 3,
          title: language.VEHICLE_SEATING_CAPACITY,
          key: "seat",
          show: true,
        },
        {
          id: 4,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_VEHICLE_CATEGORY_LIST}
      assignData={(data) =>
        data.map((vehicleCategory) => ({
          _id: vehicleCategory._id,
          category: vehicleCategory.data.vehicleCategory,
          sharable: vehicleCategory.data.shareStatus === true ? language.YES : language.NO,
          seat: vehicleCategory.data.seatCount,
          status: vehicleCategory.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      bread={[{ id: 1, title: language.VEHICLE_CATEGORY }]}
      showAdd={
        admin.privileges.SETUP.VEHICLE_CATEGORYS ? (admin.privileges.SETUP.VEHICLE_CATEGORYS.ADD ? true : false) : false
      }
      showArchieve={false}
      showEdit={
        admin.privileges.SETUP.VEHICLE_CATEGORYS
          ? admin.privileges.SETUP.VEHICLE_CATEGORYS.EDIT
            ? true
            : false
          : false
      }
      addClick={() => history.push(NavLinks.ADMIN_SETUP_VECHICLE_CATEGORY_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_SETUP_VECHICLE_CATEGORY_EDIT + "/" + e)}
      showBulk={true}
      showSearch={true}
      showAction={
        admin.privileges.SETUP.VEHICLE_CATEGORYS
          ? admin.privileges.SETUP.VEHICLE_CATEGORYS.EDIT
            ? true
            : false
          : false
      }
      add={
        admin.privileges.SETUP.VEHICLE_CATEGORYS ? (admin.privileges.SETUP.VEHICLE_CATEGORYS.ADD ? true : false) : false
      }
      edit={
        admin.privileges.SETUP.VEHICLE_CATEGORYS
          ? admin.privileges.SETUP.VEHICLE_CATEGORYS.EDIT
            ? true
            : false
          : false
      }
      statusList={A.HOST + A.ADMIN_VEHICLE_CATEGORY_STATUS}
    />
  );
}
