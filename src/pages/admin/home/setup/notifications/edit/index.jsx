import React from "react";
import * as yup from "yup";
import axios from "axios";
import { useFormik } from "formik";
import gsap from "gsap/gsap-core";

import "tinymce/tinymce";
import "tinymce/icons/default";
import "tinymce/themes/silver";
import "tinymce/plugins/paste";
import "tinymce/plugins/link";
import "tinymce/plugins/image";
import "tinymce/plugins/table";
import "tinymce/skins/ui/oxide/skin.min.css";
import "tinymce/skins/ui/oxide/content.min.css";
import "tinymce/skins/content/default/content.min.css";
import { Editor } from "@tinymce/tinymce-react";

import NavLinks from "../../../../../../utils/navLinks.json";
import U from "../../../../../../utils/utils.js";
import A from "../../../../../../utils/API.js";

import useAdmin from "../../../../../../hooks/useAdmin";
import useImage from "../../../../../../hooks/useImage";
import useUtils from "../../../../../../hooks/useUtils";
import useLanguage from "../../../../../../hooks/useLanguage";

import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { TextField } from "../../../../../../components/common/TextField";
import { DropdownNormal } from "../../../../../../components/common/DropDownNormal";
import { TextArea } from "../../../../../../components/common/TextArea";
import { FileUpload } from "../../../../../../components/common/FileUpload";
import Flex from "../../../../../../components/common/Flex";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";
import { PopUp } from "../../../../../../components/common/PopUp";
import { useParams } from "react-router";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import { Button } from "../../../../../../components/common/Button";
import { FiCopy } from "react-icons/fi";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { parseError } = useUtils();
  const { compressImage, imageUrl, isBase64 } = useImage();

  const { id, type } = useParams();

  const [showCode, setShowCode] = React.useState(false);

  const [btnLoading, setBtnLoading] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [popup, setPop] = React.useState(null);
  const docsFor = [
    { id: 1, label: language.PROFESSIONALS, value: U.PROFILE_DOCUMENTS },
    { id: 2, label: language.VEHICLES, value: U.DRIVER_DOCUMENTS },
  ];

  const formik = useFormik({
    initialValues: {
      title: "",
      type: "EMAIL",
      subject: "",
      content: "",
      image: "",
      status: U.ACTIVE,
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      title: yup
        .string()
        .required(language.REQUIRED)
        .min(4, language.MIN + " " + 4),
      subject: yup.string().required(language.REQUIRED),
      content: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        let imageUrl = e.image;
        if (e.type === "PUSH" && e.image === "") {
          setBtnLoading(false);
          return alert("Image Required");
        }
        if (e.type === "PUSH" && isBase64(e.image)) {
          const compressedImage = await compressImage(e.image);
          const formData = new FormData();
          formData.append("files", compressedImage);
          formData.append("filesName", `NOTIFICAIONS.${e.title + e.subject + e.type}`);
          const { data } = await axios.post(A.HOST + A.ADMIN_OTHERS_UPLOAD_IMAGE, formData, header);
          imageUrl = data;
        }
        const sendData = {
          id,
          title: e.title,
          subject: e.subject,
          image: imageUrl,
          type: e.type,
          content: e.content,
          status: e.status,
        };
        await axios.post(A.HOST + A.ADMIN_SETUP_NOTIFICAIONS_UPDATE, sendData, header);
        setBtnLoading(false);
        history.goBack();
      } catch (err) {
        alert(err);
        setBtnLoading(false);
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
    },
  });

  const fetchDetails = async () => {
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_SETUP_NOTIFICATIONS_READ,
        {
          id,
          type:
            type === language.EMAIL ? "EMAIL" : type === language.SMS ? "SMS" : type === language.PUSH ? "PUSH" : null,
        },
        header
      );
      const { data: editableKeys } = await axios.post(A.HOST + A.ADMIN_SETUP_NOTIFICATIONS_GET_LISTS, {}, header);
      formik.setFieldValue("emailTemplateEditable", editableKeys?.data?.data?.emailTemplateEditableFields ?? []);
      formik.setFieldValue("title", data.title);
      formik.setFieldValue("type", data.type);
      formik.setFieldValue("subject", data.subject);
      formik.setFieldValue("content", data.content);
      formik.setFieldValue("image", data.image);
      formik.setFieldValue("status", data.status);
      setLoading(false);
    } catch (err) {
      authFailure(err);
    }
  };

  React.useEffect(() => {
    fetchDetails();
  }, []);

  React.useEffect(() => {
    if (loading === false)
      setTimeout(() => gsap.fromTo(".emailEdit", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 }), 200);
  }, [loading]);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      animate={"emailEdit"}
      width="4/5"
      btnLoading={btnLoading}
      submit={formik.handleSubmit}
      bread={[
        {
          id: 1,
          title: language.NOTIFICATION_TEMPLATES,
          path: NavLinks.ADMIN_SETUP_NOTIFICATIONS,
        },
      ]}
      title={language.EDIT}
      submitBtn={true}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <div className="w-3/5 flex-col">
        <div className="flex">
          <Section padding={false}>
            <FieldWrapper animate="emailEdit" title={language.TITLE}>
              <TextField
                error={formik.errors.title}
                change={(e) => formik.setFieldValue("title", e)}
                value={formik.values.title}
                placeholder={language.TITLE}
              />
            </FieldWrapper>
            <FieldWrapper animate="emailEdit" title={language.SUBJECT}>
              <TextField
                error={formik.errors.subject}
                change={(e) => formik.setFieldValue("subject", e)}
                value={formik.values.subject}
                placeholder={language.SUBJECT}
              />
            </FieldWrapper>
          </Section>
          <Section padding={false}>
            {/* <FieldWrapper title={language.TYPE}>
              <DropdownNormal
                change={(e) => formik.setFieldValue("type", e)}
                defaultValue={language[formik.values.type]}
                fields={[
                  { id: 1, label: language.EMAIL, value: "EMAIL" },
                  { id: 2, label: language.SMS, value: "SMS" },
                  { id: 3, label: language.PUSH, value: "PUSH" },
                ]}
              />
            </FieldWrapper> */}
            {formik.values.type === "PUSH" && (
              <FieldWrapper title={language.IMAGE}>
                <FileUpload defaultValue={formik.values.image} change={(e) => formik.setFieldValue("image", e)} />
              </FieldWrapper>
            )}
          </Section>
        </div>
        {formik.values.type !== "EMAIL" && (
          <FieldWrapper title={language.CONTENT}>
            <TextArea
              placeholder={language.CONTENT}
              error={formik.errors.content}
              style={{
                width: 900,
              }}
              rows="20"
              change={(e) => formik.setFieldValue("content", e)}
              value={formik.values.content}
            />
          </FieldWrapper>
        )}
        {formik.values.type === "EMAIL" && (
          // <div className="flex flex justify-center items-center">
          <FieldWrapper title={language.EMAIL_CONTENT}>
            <div className="flex flex-col">
              <div className="mb-2">
                <Button
                  title={showCode === false ? language.SHOW_CODE : language.SHOW_EDITOR}
                  click={() => setShowCode(!showCode)}
                ></Button>
              </div>
              {showCode ? (
                <TextArea
                  style={{
                    width: 700,
                  }}
                  rows="20"
                  change={(e) => formik.setFieldValue("content", e)}
                  value={formik.values.content}
                />
              ) : (
                <Editor
                  init={{
                    selector: "textarea",
                    skin: false,
                    height: 500,
                    width: 700,
                    paste_data_images: true,
                    images_upload_handler: function (blobInfo, success, failure) {
                      success("data:" + blobInfo.blob().type + ";base64," + blobInfo.base64());
                    },
                    plugins: [
                      "advlist autolink lists link image charmap print preview anchor",
                      "searchreplace visualblocks code fullscreen",
                      "insertdatetime media table contextmenu paste",
                    ],
                    toolbar:
                      "undo redo | styleselect | bold italic | link | alignleft aligncenter alignright | link image",
                  }}
                  value={formik.values.content}
                  onEditorChange={(e) => formik.setFieldValue("content", e)}
                />
              )}
            </div>
          </FieldWrapper>
          // </div>
        )}
      </div>
      <div className="w-2/5">
        {formik.values.emailTemplateEditable?.length > 0 ? (
          <FieldWrapper title={language.AVAILABLE_KEYS_FOR_THIS_TEMPLATE}>
            <div>
              <p className="text-sm mx-2 mb-2">{`(Click the Icon to Copy and Paste in Email Content)`}</p>
              {formik.values.emailTemplateEditable.map((each) => (
                <li className="text-sm mx-2 flex items-center">
                  {`{{${each}}}`}
                  <FiCopy
                    onClick={() => {
                      setPop({ title: "Copied" });
                      navigator?.clipboard?.writeText(`{{${each}}}`);
                    }}
                    className="cursor-pointer mx-2 text-lg text-blue-800"
                  />
                </li>
              ))}
            </div>
          </FieldWrapper>
        ) : (
          <FieldWrapper title={language.AVAILABLE_KEYS_NOT_YET_CONFIGURED_FOR_THIS_TEMPLATE}></FieldWrapper>
        )}
      </div>
    </FormWrapper>
  );
}
