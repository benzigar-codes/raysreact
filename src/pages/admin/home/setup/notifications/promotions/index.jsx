import React from "react";
import * as yup from "yup";
import axios from "axios";
import { useFormik } from "formik";
import gsap from "gsap/gsap-core";

import NavLinks from "../../../../../../utils/navLinks.json";
import U from "../../../../../../utils/utils.js";
import A from "../../../../../../utils/API.js";

import useAdmin from "../../../../../../hooks/useAdmin";
import useImage from "../../../../../../hooks/useImage";
import useUtils from "../../../../../../hooks/useUtils";
import useLanguage from "../../../../../../hooks/useLanguage";

import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { TextField } from "../../../../../../components/common/TextField";
import { DropdownNormal } from "../../../../../../components/common/DropDownNormal";
import { TextArea } from "../../../../../../components/common/TextArea";
import { FileUpload } from "../../../../../../components/common/FileUpload";
import { PopUp } from "../../../../../../components/common/PopUp";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import { Button } from "../../../../../../components/common/Button";
import { FiMail, FiPhone, FiStar, FiUser } from "react-icons/fi";
import { AiFillCloseCircle, AiFillLeftCircle, AiFillRightCircle, AiOutlineCar } from "react-icons/ai";
import { HiOutlineLocationMarker } from "react-icons/hi";
import useSettings from "../../../../../../hooks/useSettings";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { settings } = useSettings();
  const { parseError } = useUtils();
  const { compressImage, imageUrl, isBase64 } = useImage();

  const [showCode, setShowCode] = React.useState(false);

  const [btnLoading, setBtnLoading] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [popup, setPop] = React.useState(null);

  const [templates, setTemplates] = React.useState(null);
  const [pagination, setPagination] = React.useState({
    skip: 0,
    limit: 30,
  });
  const [users, setUsers] = React.useState(null);

  const formik = useFormik({
    initialValues: {
      userData: [],
      template: "",
      type: "EMAIL",
      userType: "",
      searchText: "",
    },
    validationSchema: yup.object().shape({
      template: yup.string(language.REQUIRED).required(language.REQUIRED),
      userType: yup.string().required(language.REQUIRED),
    }),
    validateOnChange: false,
    onSubmit: async (e) => {
      if (
        formik.values.userData.length === 0 ||
        templates.filter((each) => each._id === formik.values.template).length === 0
      )
        return alert(language.ATLEAST_ONE_DATA);
      setBtnLoading(true);
      const ids = formik.values.userData.map((each) => each._id);
      const template = templates.filter((each) => each._id === formik.values.template)[0];
      try {
        const sendData = {
          ids: ids,
          type: formik.values.type,
          userType: formik.values.userType,
          template: {
            title: template.title,
            content: template.content,
            image: template.image,
            subject: template.subject,
          },
        };
        await axios.post(A.HOST + A.ADMIN_SETUP_NOTIFICATIONS_SEND_PROMO, sendData, header);
        formik.setFieldValue("userData", []);
        setBtnLoading(false);
        setPop({ title: language.SUCCESS });
      } catch (err) {
        alert(err);
        setBtnLoading(false);
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
    },
  });

  const fetchTemplates = async () => {
    try {
      setTemplates(null);
      const { data } = await axios.post(
        A.HOST + A.ADMIN_SETUP_NOTIFICATIONS_LIST,
        {
          skip: 0,
          limit: 100000,
          search: "",
          filter: "ACTIVE",
          type: formik.values.type,
        },
        header
      );
      setTemplates(data.response);
    } catch (err) {
      alert(err);
      authFailure(err);
    }
  };

  const fetchUsers = async () => {
    if (formik.values.template && (formik.values.userType === "USER" || formik.values.userType === "PROFESSIONAL")) {
      try {
        // setUsers(null);
        const link = formik.values.userType === "USER" ? A.HOST + A.ADMIN_USERS_LIST : A.HOST + A.ADMIN_DRIVERS_LIST;
        const { data } = await axios.post(
          link,
          {
            skip: pagination.skip,
            limit: pagination.limit,
            search: formik.values.searchText,
            filter: "",
          },
          header
        );
        setUsers(data.response.map((each) => ({ _id: each._id, ...each.data })));
      } catch (err) {
        authFailure(err);
      }
    } else setUsers(null);
  };

  React.useEffect(() => {
    setPagination({
      ...pagination,
      skip: 0,
    });
  }, [formik.values.searchText]);

  React.useEffect(() => {
    fetchUsers();
  }, [pagination]);

  React.useEffect(() => {
    formik.setFieldValue("template", "");
    fetchTemplates();
  }, [formik.values.type]);

  React.useEffect(() => {
    formik.setFieldValue("userData", []);
    setUsers(null);
    formik.setFieldValue("searchText", "");
    setPagination({
      ...pagination,
      skip: 0,
    });
  }, [formik.values.userType]);

  React.useEffect(() => {
    setLoading(false);
    setTimeout(() => gsap.fromTo(".emailEdit", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 }), 200);
  }, []);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      animate={"emailEdit"}
      width="4/4"
      btnLoading={btnLoading}
      submit={formik.handleSubmit}
      bread={[]}
      title={language.SEND_PROMOTIONS}
      submitBtn={true}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <div className="w-full flex-col">
        <div className="flex">
          <Section padding={false}>
            <FieldWrapper title={language.TYPE}>
              <DropdownNormal
                change={(e) => formik.setFieldValue("type", e)}
                defaultValue={language.EMAIL}
                fields={[
                  { id: 1, label: language.EMAIL, value: "EMAIL" },
                  { id: 2, label: language.SMS, value: "SMS" },
                  { id: 3, label: language.PUSH, value: "PUSH" },
                ]}
              />
            </FieldWrapper>
          </Section>
          <Section padding={false}>
            {templates && (
              <FieldWrapper title={language.TEMPLATES}>
                <DropdownNormal
                  error={formik.errors.template}
                  change={(e) => formik.setFieldValue("template", e)}
                  defaultValue={language.EMAIL}
                  fields={templates.map((each, idx) => ({
                    id: idx,
                    label: each.title,
                    value: each._id,
                  }))}
                />
              </FieldWrapper>
            )}
          </Section>
          <Section padding={false}>
            {formik.values.template && formik.values.type && (
              <FieldWrapper title={language.USER_TYPE}>
                <DropdownNormal
                  defaultValue={language[formik.values.userType]}
                  error={formik.errors.userType}
                  change={(e) => formik.setFieldValue("userType", e)}
                  fields={[
                    {
                      id: 1,
                      label: language.USER,
                      value: "USER",
                    },
                    {
                      id: 2,
                      label: language.PROFESSIONAL,
                      value: "PROFESSIONAL",
                    },
                  ]}
                />
              </FieldWrapper>
            )}
          </Section>
          <Section></Section>
        </div>

        {formik.values.userData && formik.values.userData.length > 0 && (
          <FieldWrapper title={language.SELECTED}>
            <div className="flex flex-wrap">
              {formik.values.userData.map((each) => (
                <div className="flex items-center p-2 mb-2 px-4 bg-gray-100 dark:bg-gray-900 dark:text-white rounded-full mx-1 text-sm">
                  <p>
                    {each.firstName} {each.lastName}
                  </p>
                  <AiFillCloseCircle
                    onClick={() =>
                      formik.setFieldValue(
                        "userData",
                        formik.values.userData.filter((eachUser) => each._id !== eachUser._id)
                      )
                    }
                    className="cursor-pointer dark:text-white mx-1"
                  />
                </div>
              ))}
            </div>
          </FieldWrapper>
        )}
        {formik.values.template && formik.values.type && formik.values.userType && (
          <div className="flex">
            <Section padding={false}>
              <FieldWrapper
                title={formik.values.userType === "USER" ? language.SEARCH_FOR_USER : language.SEARCH_FOR_PROFESSIONAL}
              >
                <TextField
                  placeholder={language.SEARCH}
                  change={(e) => formik.setFieldValue("searchText", e)}
                  value={formik.values.searchText}
                />
              </FieldWrapper>
            </Section>
            <Section></Section>
            <Section></Section>
            <Section></Section>
          </div>
        )}
        {users && (
          <>
            {users && users.length > 0 && (
              <FieldWrapper title={formik.values.userType === "USER" ? language.USERS : language.PROFESSIONALS}>
                <Button
                  click={() => {
                    let data = [];
                    users.forEach((each) => {
                      if (formik.values.userData.filter((eachUser) => eachUser._id === each._id).length > 0)
                        // data = data.filter((eachUser) => each._id !== eachUser._id);
                        return;
                      else {
                        data.push(each);
                      }
                    });
                    formik.setFieldValue("userData", [...formik.values.userData, ...data]);
                  }}
                  title={language.SELECT_ALL_FROM_BELOW}
                />
                {users.filter((each) =>
                  formik.values.userData.filter((eachUser) => eachUser._id === each._id).length > 0 ? true : false
                ).length > 0 && (
                  <Button
                    click={() => {
                      let data = formik.values.userData;
                      users.forEach((each) => {
                        data = data.filter((eachUser) => eachUser._id !== each._id);
                      });
                      formik.setFieldValue("userData", data);
                    }}
                    title={language.DESELECT_ALL_FROM_BELOW}
                  />
                )}
              </FieldWrapper>
            )}
            <div className="flex flex-wrap">
              {users.length === 0 && <p className="text-sm px-4">{language.DATA_NOT_AVAILABLE}</p>}
              {users.map((each) => (
                <div
                  onClick={() =>
                    formik.values.userData.filter((eachUser) => eachUser._id === each._id).length > 0
                      ? formik.setFieldValue(
                          "userData",
                          formik.values.userData.filter((eachUser) => each._id !== eachUser._id)
                        )
                      : formik.setFieldValue("userData", [...formik.values.userData, each])
                  }
                  className="w-1/5"
                >
                  <div
                    className={`${
                      formik.values.userData.filter((eachUser) => eachUser._id === each._id).length > 0 &&
                      "border-green-800 dark:border-blue-800"
                    } flex flex-wrap items-center cursor-pointer transition m-3 p-3 border-2 rounded-xl dark:border-gray-900 `}
                  >
                    <div className="w-1/5">
                      <FiUser className={"text-blue-800"} />
                    </div>
                    <div className="w-4/5">{each.firstName + " " + each.lastName}</div>
                    <div className="w-1/5 mt-2 truncate" title={each.firstName + " " + each.lastName}>
                      <FiMail className={"text-blue-800"} />
                    </div>
                    <div className="w-4/5 mt-2 truncate" title={each.email}>
                      {each.email}
                    </div>
                    <div className="w-1/5 mt-2">
                      <FiPhone className={"text-blue-800"} />
                    </div>
                    <div className="w-4/5 mt-2 truncate" title={each.phone.code + " " + each.phone.number}>
                      {each.phone.code + " " + each.phone.number}
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <div className="px-4 py-2 flex items-center">
              {pagination.skip !== 0 && (
                <AiFillLeftCircle
                  onClick={() =>
                    setPagination({
                      ...pagination,
                      skip: pagination.skip - parseInt(pagination.limit),
                    })
                  }
                  className="mx-2 text-4xl dark:text-white text-black cursor-pointer"
                />
              )}
              {users.length !== 0 && (
                <AiFillRightCircle
                  onClick={() =>
                    setPagination({
                      ...pagination,
                      skip: pagination.skip + parseInt(pagination.limit),
                    })
                  }
                  className="mx-2 text-4xl dark:text-white text-black cursor-pointer"
                />
              )}
            </div>
          </>
        )}
      </div>
    </FormWrapper>
  );
}
