import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import axios from "axios";
import useAdmin from "../../../../../hooks/useAdmin";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin, header, authFailure } = useAdmin();
  return (
    <Table
      title={language.NOTIFICATION_TEMPLATES}
      startingHeadings={[
        {
          id: 1,
          title: language.TITLE,
          key: "title",
          show: true,
        },
        {
          id: 2,
          title: language.SUBJECT,
          key: "subject",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_SETUP_NOTIFICATIONS_LIST}
      assignData={(data) =>
        data.map((template) => ({
          _id: template._id,
          title: template.title,
          subject: template.subject,
          status: template.status === U.ACTIVE ? 1 : 0,
        }))
      }
      bread={[]}
      showFilter={true}
      showAdd={true}
      showNotificationTypeFilter={true}
      showStatus={true}
      showActiveInactiveFilter={true}
      addClick={() => history.push(NavLinks.ADMIN_SETUP_NOTIFICATIONS_ADD)}
      editClick={(e, type) => history.push(NavLinks.ADMIN_SETUP_NOTIFICATIONS_EDIT + "/" + type + "/" + e)}
      showBulk={false}
      statusList={A.HOST + A.ADMIN_SETUP_NOTIFICATIONS_CHANGE_STATUS}
      showEdit={true}
      showSearch={true}
      showAction={
        admin.privileges.SETUP.NOTIFICATION_TEMPLATES
          ? admin.privileges.SETUP.NOTIFICATION_TEMPLATES.EDIT
            ? true
            : false
          : false
      }
      add={
        admin.privileges.SETUP.NOTIFICATION_TEMPLATES
          ? admin.privileges.SETUP.NOTIFICATION_TEMPLATES.ADD
            ? true
            : false
          : false
      }
      edit={
        admin.privileges.SETUP.NOTIFICATION_TEMPLATES
          ? admin.privileges.SETUP.NOTIFICATION_TEMPLATES.EDIT
            ? true
            : false
          : false
      }
    />
  );
}
