import React from "react";
import * as yup from "yup";
import axios from "axios";
import { useFormik } from "formik";
import gsap from "gsap/gsap-core";

import "tinymce/tinymce";
import "tinymce/icons/default";
import "tinymce/themes/silver";
import "tinymce/plugins/paste";
import "tinymce/plugins/link";
import "tinymce/plugins/image";
import "tinymce/plugins/table";
import "tinymce/skins/ui/oxide/skin.min.css";
import "tinymce/skins/ui/oxide/content.min.css";
import "tinymce/skins/content/default/content.min.css";
import { Editor } from "@tinymce/tinymce-react";

import NavLinks from "../../../../../../utils/navLinks.json";
import U from "../../../../../../utils/utils.js";
import A from "../../../../../../utils/API.js";

import useAdmin from "../../../../../../hooks/useAdmin";
import useImage from "../../../../../../hooks/useImage";
import useUtils from "../../../../../../hooks/useUtils";
import useLanguage from "../../../../../../hooks/useLanguage";

import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { TextField } from "../../../../../../components/common/TextField";
import { DropdownNormal } from "../../../../../../components/common/DropDownNormal";
import { TextArea } from "../../../../../../components/common/TextArea";
import { FileUpload } from "../../../../../../components/common/FileUpload";
import Flex from "../../../../../../components/common/Flex";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";
import { PopUp } from "../../../../../../components/common/PopUp";
import { useParams } from "react-router";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import { Button } from "../../../../../../components/common/Button";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { parseError } = useUtils();
  const { compressImage, imageUrl, isBase64 } = useImage();

  const [showCode, setShowCode] = React.useState(false);

  const [btnLoading, setBtnLoading] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [popup, setPop] = React.useState(null);
  const docsFor = [
    { id: 1, label: language.PROFESSIONALS, value: U.PROFILE_DOCUMENTS },
    { id: 2, label: language.VEHICLES, value: U.DRIVER_DOCUMENTS },
  ];

  const formik = useFormik({
    initialValues: {
      title: "",
      type: "EMAIL",
      subject: "",
      content: "",
      image: "",
      status: U.ACTIVE,
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      title: yup
        .string()
        .required(language.REQUIRED)
        .min(4, language.MIN + " " + 4),
      subject: yup.string().required(language.REQUIRED),
      content: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        let imageUrl = e.image;
        if (e.type === "PUSH" && e.image === "") {
          setBtnLoading(false);
          return alert("Image Required");
        }
        if (e.type === "PUSH") {
          const compressedImage = await compressImage(e.image);
          const formData = new FormData();
          formData.append("files", compressedImage);
          formData.append("filesName", `NOTIFICAIONS.${e.title + e.subject + e.type}`);
          const { data } = await axios.post(A.HOST + A.ADMIN_OTHERS_UPLOAD_IMAGE, formData, header);
          imageUrl = data;
        }
        const sendData = {
          title: e.title,
          subject: e.subject,
          image: imageUrl,
          type: e.type,
          content: e.content,
          status: e.status,
        };
        await axios.post(A.HOST + A.ADMIN_SETUP_NOTIFICAIONS_UPDATE, sendData, header);
        setBtnLoading(false);
        history.goBack();
      } catch (err) {
        alert(err);
        setBtnLoading(false);
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
    },
  });

  React.useEffect(() => {
    setLoading(false);
    setTimeout(() => gsap.fromTo(".emailEdit", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 }), 200);
  }, []);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      animate={"emailEdit"}
      width="3/4"
      btnLoading={btnLoading}
      submit={formik.handleSubmit}
      bread={[
        {
          id: 1,
          title: language.NOTIFICATION_TEMPLATES,
          path: NavLinks.ADMIN_SETUP_NOTIFICATIONS,
        },
      ]}
      title={language.ADD}
      submitBtn={true}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <div className="w-full flex-col">
        <div className="flex">
          <Section padding={false}>
            <FieldWrapper animate="emailEdit" title={language.TITLE}>
              <TextField
                error={formik.errors.title}
                change={(e) => formik.setFieldValue("title", e)}
                value={formik.values.title}
                placeholder={language.TITLE}
              />
            </FieldWrapper>
            <FieldWrapper animate="emailEdit" title={language.SUBJECT}>
              <TextField
                error={formik.errors.subject}
                change={(e) => formik.setFieldValue("subject", e)}
                value={formik.values.subject}
                placeholder={language.SUBJECT}
              />
            </FieldWrapper>
          </Section>
          <Section padding={false}>
            <FieldWrapper title={language.TYPE}>
              <DropdownNormal
                change={(e) => formik.setFieldValue("type", e)}
                defaultValue={language.EMAIL}
                fields={[
                  { id: 1, label: language.EMAIL, value: "EMAIL" },
                  { id: 2, label: language.SMS, value: "SMS" },
                  { id: 3, label: language.PUSH, value: "PUSH" },
                ]}
              />
            </FieldWrapper>
            {formik.values.type === "PUSH" && (
              <FieldWrapper title={language.IMAGE}>
                <FileUpload change={(e) => formik.setFieldValue("image", e)} />
              </FieldWrapper>
            )}
          </Section>
        </div>
        {formik.values.type !== "EMAIL" && (
          <FieldWrapper title={language.CONTENT}>
            <TextArea
              placeholder={language.CONTENT}
              error={formik.errors.content}
              style={{
                width: 900,
              }}
              rows="20"
              change={(e) => formik.setFieldValue("content", e)}
              value={formik.values.content}
            />
          </FieldWrapper>
        )}
        {formik.values.type === "EMAIL" && (
          <div className="flex flex justify-center items-center">
            <FieldWrapper title={language.EMAIL_CONTENT}>
              <div className="flex flex-col">
                <div className="mb-2">
                  <Button
                    title={showCode === false ? language.SHOW_CODE : language.SHOW_EDITOR}
                    click={() => setShowCode(!showCode)}
                  ></Button>
                </div>
                {showCode ? (
                  <TextArea
                    style={{
                      width: 900,
                    }}
                    rows="20"
                    change={(e) => formik.setFieldValue("content", e)}
                    value={formik.values.content}
                  />
                ) : (
                  <Editor
                    init={{
                      selector: "textarea",
                      skin: false,
                      height: 500,
                      width: 900,
                      paste_data_images: true,
                      images_upload_handler: function (blobInfo, success, failure) {
                        success("data:" + blobInfo.blob().type + ";base64," + blobInfo.base64());
                      },
                      menubar: "file edit view insert format tools table help",
                      plugins:
                        "code uploadimage print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap tinycomments mentions quickbars linkchecker emoticons advtable export",
                      content_css: "dark",
                      toolbar:
                        "undo redo | formatselect | " +
                        "bold italic backcolor | alignleft aligncenter " +
                        "alignright alignjustify | bullist numlist outdent indent | " +
                        "removeformat | code",
                    }}
                    value={formik.values.content}
                    onEditorChange={(e) => formik.setFieldValue("content", e)}
                  />
                )}
              </div>
            </FieldWrapper>
          </div>
        )}
      </div>
    </FormWrapper>
  );
}
