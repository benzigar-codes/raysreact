import React from "react";
import * as yup from "yup";
import axios from "axios";
import { useFormik } from "formik";
import gsap from "gsap/gsap-core";

import NavLinks from "../../../../../../utils/navLinks.json";
import U from "../../../../../../utils/utils.js";
import A from "../../../../../../utils/API.js";

import useAdmin from "../../../../../../hooks/useAdmin";
import useImage from "../../../../../../hooks/useImage";
import useUtils from "../../../../../../hooks/useUtils";
import useLanguage from "../../../../../../hooks/useLanguage";

import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { TextField } from "../../../../../../components/common/TextField";
import { DropdownNormal } from "../../../../../../components/common/DropDownNormal";
import { TextArea } from "../../../../../../components/common/TextArea";
import { FileUpload } from "../../../../../../components/common/FileUpload";
import Flex from "../../../../../../components/common/Flex";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";
import { PopUp } from "../../../../../../components/common/PopUp";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { parseError } = useUtils();
  const { compressImage } = useImage();

  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);

  const formik = useFormik({
    initialValues: {
      name: "",
      for: "",
      detail: "",
      image: "",
      mandatory: 0,
      date: 0,
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      name: yup
        .string()
        .required(language.REQUIRED)
        .min(4, language.MIN + " " + 4),
      for: yup.string().required(language.REQUIRED),
      detail: yup.string().required(language.REQUIRED),
      image: yup.string().required(language.REQUIRED),
      mandatory: yup.number().required(language.REQUIRED),
      date: yup.number().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        let image = await compressImage(e.image);
        const formData = new FormData();
        formData.append("docsName", e.name);
        formData.append("docsFor", e.for);
        formData.append("docsDetail", e.detail);
        formData.append("docsReferImage", image);
        formData.append("docsExpiry", e.date === 1 ? true : false);
        formData.append("docsMandatory", e.mandatory === 1 ? true : false);
        formData.append("status", U.ACTIVE);
        await axios.post(A.HOST + A.ADMIN_DOCUMENT_UPDATE, formData, header);
        history.goBack();
      } catch (err) {
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
      setBtnLoading(false);
    },
  });

  React.useEffect(() => {
    gsap.fromTo(".addDocuments", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, []);

  return (
    <FormWrapper
      animate={"addDocuments"}
      width="3/4"
      btnLoading={btnLoading}
      submit={formik.handleSubmit}
      bread={[
        {
          id: 1,
          title: language.DOCUMENTS,
          path: NavLinks.ADMIN_SETUP_DOCUMENTS,
        },
      ]}
      title={language.ADD}
      submitBtn={true}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <Section>
        <FieldWrapper animate="addDocuments" title={language.DOCUMENT_FOR}>
          <DropdownNormal
            error={formik.errors.for}
            change={(e) => formik.setFieldValue("for", e)}
            fields={[
              { id: 1, label: language.PROFESSIONALS, value: U.PROFILE_DOCUMENTS },
              { id: 2, label: language.VEHICLES, value: U.DRIVER_DOCUMENTS },
              // { id: 3, label: "Response Officer", value: "RESPONSEOFFICER" },
            ]}
          />
        </FieldWrapper>
        <FieldWrapper animate="addDocuments" title={language.DOC_NAME}>
          <TextField
            error={formik.errors.name}
            change={(e) => formik.setFieldValue("name", e)}
            value={formik.values.name}
          />
        </FieldWrapper>
        <FieldWrapper animate="addDocuments" title={language.DOC_DETAIL}>
          <TextArea
            error={formik.errors.detail}
            change={(e) => formik.setFieldValue("detail", e)}
            value={formik.values.detail}
          />
        </FieldWrapper>
        <Flex align={false}>
          <Section padding={false}>
            <FieldWrapper animate="addDocuments" title={language.IS_DOCUMENT_MANDATORY}>
              <ToggleButton
                change={(e) => formik.setFieldValue("mandatory", e)}
                defaultValue={formik.values.mandatory}
              />
            </FieldWrapper>
          </Section>
          <Section padding={false}>
            <FieldWrapper animate="addDocuments" title={language.IS_EXPIRY_DATE_NEEDED}>
              <ToggleButton change={(e) => formik.setFieldValue("date", e)} defaultValue={formik.values.date} />
            </FieldWrapper>
          </Section>
        </Flex>
      </Section>
      <Section>
        <FieldWrapper animate="addDocuments" title={language.DOCUMENT_REFERENCE_IMAGE}>
          <FileUpload change={(e) => formik.setFieldValue("image", e)} crop={false} />
        </FieldWrapper>
      </Section>
    </FormWrapper>
  );
}
