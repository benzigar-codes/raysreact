import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import axios from "axios";
import useAdmin from "../../../../../hooks/useAdmin";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin, header, authFailure } = useAdmin();
  return (
    <Table
      title={language.ONBOARDING_DOCUMENTS}
      startingHeadings={[
        {
          id: 1,
          title: language.NAME,
          key: "name",
          show: true,
        },
        {
          id: 2,
          title: language.FOR,
          key: "for",
          show: true,
        },
        {
          id: 3,
          title: language.EXPIRY,
          key: "expiry",
          show: true,
        },
        {
          id: 4,
          title: language.MANDATORY,
          key: "mandatory",
          show: true,
        },
        {
          id: 5,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_DOCUMENT_LIST}
      assignData={(data) =>
        data.map((operator) => ({
          _id: operator._id,
          name: operator.data.docsName,
          for:
            operator.data.docsFor === U.DRIVER_DOCUMENTS
              ? language.VEHICLES
              : operator.data.docsFor === U.PROFILE_DOCUMENTS
              ? language.PROFESSIONALS
              : "",
          expiry: operator.data.docsExpiry === true ? language.YES : language.NO,
          mandatory: operator.data.docsMandatory === true ? language.YES : language.NO,
          status: operator.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      bread={[]}
      showFilter={true}
      showAdd={true}
      showArchieve={true}
      addClick={() => history.push(NavLinks.ADMIN_SETUP_DOCUMENTS_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_SETUP_DOCUMENTS_EDIT + "/" + e)}
      showBulk={true}
      showEdit={true}
      showSearch={true}
      statusList={A.HOST + A.ADMIN_DOCUMENT_STATUS}
      showAction={admin.privileges.SETUP.DOCUMENTS ? (admin.privileges.SETUP.DOCUMENTS.EDIT ? true : false) : false}
      add={admin.privileges.SETUP.DOCUMENTS ? (admin.privileges.SETUP.DOCUMENTS.ADD ? true : false) : false}
      edit={admin.privileges.SETUP.DOCUMENTS ? (admin.privileges.SETUP.DOCUMENTS.EDIT ? true : false) : false}
    />
  );
}
