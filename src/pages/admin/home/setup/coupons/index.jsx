import React from "react";
import axios from "axios";

import Table from "../../../../../components/common/Table";
import { SmallLoader } from "../../../../../components/common/SmallLoader";

import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin, header, authFailure } = useAdmin();
  const [cities, setCities] = React.useState(null);
  const { morph } = useUtils();
  const fetchCities = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_CITY_LIST_ALL, {}, header);
      setCities(data.data);
    } catch (er) {
      authFailure(er);
    }
  };
  React.useEffect(() => {
    fetchCities();
  }, []);
  return cities === null ? (
    <SmallLoader />
  ) : (
    <Table
      title={language.COUPONS}
      startingHeadings={[
        {
          id: 1,
          title: language.TITLE,
          key: "title",
          show: true,
        },
        {
          id: 2,
          title: language.COUPON_CODE,
          key: "code",
          show: true,
        },
        {
          id: 3,
          title: language.COUPON_TYPE,
          key: "type",
          show: true,
        },
        {
          id: 4,
          title: language.CITY,
          key: "city",
          show: true,
        },
        {
          id: 5,
          title: language.AMOUNT,
          key: "couponAmount",
          show: false,
        },
        {
          id: 6,
          title: language.MINIMUM_AMOUNT,
          key: "minCouponAmount",
          show: false,
        },
        {
          id: 7,
          title: language.MAXIMUM_AMOUNT,
          key: "maxCouponAmount",
          show: false,
        },
        {
          id: 8,
          title: language.TOTAL_USAGE,
          key: "totalUsageCount",
          show: false,
        },
        {
          id: 8,
          title: language.MAXIMUM_USAGE_PER_PERSON,
          key: "usageCountPerPerson",
          show: false,
        },
        {
          id: 8,
          title: language.AUTOMATIC_MANUAL,
          key: "auto/manual",
          show: true,
        },
        {
          id: 9,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_COUPONS_LIST}
      assignData={(data) =>
        data.map((coupon) => ({
          _id: coupon._id,
          title: coupon.data.couponTitle,
          code: coupon.data.couponCode,
          type: language[coupon.data.couponType],
          couponAmount: coupon.data.couponAmount,
          minCouponAmount: coupon.data.minCouponAmount,
          maxCouponAmount: coupon.data.maxCouponAmount,
          totalUsageCount: coupon.data.totalUsageCount,
          usageCountPerPerson: coupon.data.usageCountPerPerson,
          city:
            coupon.data.city && cities.find((each) => each._id === coupon.data.city)
              ? cities.find((each) => each._id === coupon.data.city).locationName
              : "",
          "auto/manual": coupon.data?.isAutomatic ? language.AUTOMATIC : language.MANUAL,
          status: coupon.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      showAdd={admin.privileges.SETUP.COUPONS ? (admin.privileges.SETUP.COUPONS.ADD ? true : false) : false}
      showArchieve={false}
      showEdit={admin.privileges.SETUP.COUPONS ? (admin.privileges.SETUP.COUPONS.EDIT ? true : false) : false}
      addClick={() => history.push(NavLinks.ADMIN_SETUP_COUPONS_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_SETUP_COUPONS_EDIT + "/" + e)}
      rideClick={(e) => history.push(NavLinks.ADMIN_SETUP_COUPONS_BOOKINGS + "/" + e)}
      showBulk={true}
      showRides={true}
      showCityFilter={true}
      showView={false}
      showSearch={true}
      viewClick={(e) => history.push(NavLinks.ADMIN_SETUP_CITY_COMMISION_VIEW + "/" + e)}
      showAction={admin.privileges.SETUP.COUPONS ? (admin.privileges.SETUP.COUPONS.EDIT ? true : false) : false}
      add={admin.privileges.SETUP.COUPONS ? (admin.privileges.SETUP.COUPONS.ADD ? true : false) : false}
      edit={admin.privileges.SETUP.COUPONS ? (admin.privileges.SETUP.COUPONS.EDIT ? true : false) : false}
      statusList={A.HOST + A.ADMIN_COUPONS_STATUS_CHANGE}
    />
  );
}
