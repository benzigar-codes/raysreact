import React from "react";
import { useFormik } from "formik";
import axios from "axios";
import * as yup from "yup";
import gsap from "gsap/gsap-core";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../../components/common/TextField";
import { DropdownNormal } from "../../../../../../components/common/DropDownNormal";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";
import CalenderDatePicker from "../../../../../../components/common/CalenderDatePicker";

import useLanguage from "../../../../../../hooks/useLanguage";
import useSettings from "../../../../../../hooks/useSettings";
import useAdmin from "../../../../../../hooks/useAdmin";
import useUtils from "../../../../../../hooks/useUtils";

import NavLinks from "../../../../../../utils/navLinks.json";
import U from "../../../../../../utils/utils.js";
import A from "../../../../../../utils/API.js";

import { MultiSelect } from "../../../../../../components/common/MultiSelect";
import { PopUp } from "../../../../../../components/common/PopUp";
import { useParams } from "react-router";
import { Button } from "../../../../../../components/common/Button";
import { Border } from "../../../../../../components/common/Border";
import { Heading } from "../../../../../../components/common/Heading";
import Flex from "../../../../../../components/common/Flex";

export default function Index({ history }) {
  // HOOKS
  const { language } = useLanguage();
  const { settings } = useSettings();
  const { id } = useParams();
  const { header, authFailure } = useAdmin();
  const { parseError } = useUtils();

  const [polygons, setPolygons] = React.useState([]);

  const [vehicleCategoryLoading, setVehicleCategoryLoading] = React.useState(false);

  // VEHICLE CATEGORY FROM API
  const [vehicleCategory, setVehicleCategory] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);

  const formik1 = useFormik({
    initialValues: {
      city: "",
      couponTitle: "",
      couponDescription: "",
      couponCodeMessage: "",
      couponCode: "",
      couponType: "FLAT",
      categoryType: "DAILYTRIP",
      couponAmount: 0,
      // minCouponAmount: 0,
      // maxCouponAmount: 10,
      couponMaxAmount: 0,
      totalUsageCount: 1,
      usageCountPerPerson: 1,
      dateLimit: 0,
      vaildFrom: new Date(),
      vaildTo: new Date(),
      consumersType: "EVERYONE",
      isAutomatic: 1,
      isDisplayInApp: 1,
      vehicleCategories: [],
      isProfessionalTolerance: 1,
      couponPaymentType: ["CASH", "WALLET"],
      couponGuideLines: [
        // {
        //   title: "",
        //   points: [""],
        // },
      ],
      status: U.ACTIVE,
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      city: yup.string().required(language.REQUIRED),
      couponTitle: yup.string().required(language.REQUIRED),
      couponDescription: yup.string().required(language.REQUIRED),
      couponCodeMessage: yup.string().required(language.REQUIRED),
      couponCode: yup.string().required(language.REQUIRED),
      couponType: yup.string().required(language.REQUIRED),
      categoryType: yup.string().required(language.REQUIRED),
      couponPaymentType: yup.array().min(1, language.REQUIRED).required(language.REQUIRED),
      couponGuideLines: yup.array().of(
        yup.object().shape({
          title: yup.string().required(language.REQUIRED),
          points: yup.array().of(yup.string().required(language.REQUIRED)),
        })
      ),
      couponAmount: yup
        .number()
        .min(0, language.MIN + " 0")
        .required(language.REQUIRED),
      couponMaxAmount: yup
        .number()
        .min(0, language.MIN + " 0")
        .required(language.REQUIRED),
      // minCouponAmount: yup
      //   .number()
      //   .min(0, language.MIN + " 0")
      //   .required(language.REQUIRED),
      // maxCouponAmount: yup
      //   .number()
      //   .min(0, language.MIN + " 0")
      //   .required(language.REQUIRED),
      totalUsageCount: yup
        .number()
        .min(0, language.MIN + " 0")
        .required(language.REQUIRED),
      usageCountPerPerson: yup
        .number()
        .min(0, language.MIN + " 0")
        .required(language.REQUIRED),
      dateLimit: yup.number().required(language.REQUIRED),
      consumersType: yup.string().required(language.REQUIRED),
      isAutomatic: yup.number().required(language.REQUIRED),
      isDisplayInApp: yup.number().required(language.REQUIRED),
      isProfessionalTolerance: yup.number().required(language.REQUIRED),
      vehicleCategories: yup.array().min(1, language.ERROR_SELECT_VEHICLE),
    }),
    onSubmit: async (e) => {
      if (
        formik1.values.couponType === "FLAT" &&
        parseInt(formik1.values.couponMaxAmount) < parseInt(formik1.values.couponAmount)
      )
        return setPop({ title: language.AMOUNT_SHOULD_BE_LESS_THAN_MAX_APPLY, type: "error" });
      setBtnLoading(true);
      try {
        const data = {
          id,
          data: {
            serviceArea: e.city,
            categoryIds: e.vehicleCategories,
            categoryType: e.categoryType,
            couponTitle: e.couponTitle,
            couponDescription: e.couponDescription,
            couponCode: e.couponCode,
            couponType: e.couponType,
            couponAmount: parseInt(e.couponAmount),
            couponPaymentType: e.couponPaymentType,
            couponMaxAmount: parseInt(e.couponMaxAmount),
            couponGuideLines: e.couponGuideLines,
            // minCouponAmount: parseInt(e.minCouponAmount),
            // maxCouponAmount: parseInt(e.maxCouponAmount),
            totalUsageCount: parseInt(e.totalUsageCount),
            usageCountPerPerson: parseInt(e.usageCountPerPerson),
            vaildFrom: e.dateLimit === 1 ? e.vaildFrom : "",
            vaildTo: e.dateLimit === 1 ? e.vaildTo : "",
            consumersType: e.consumersType,
            isAutomatic: e.isAutomatic === 1 ? true : false,
            isProfessionalTolerance: e.isProfessionalTolerance === 1 ? true : false,
            isDisplayInApp: e.isDisplayInApp === 1 ? true : false,
            couponCodeMessage: e.couponCodeMessage,
            status: e.status,
          },
        };
        await axios.post(A.HOST + A.ADMIN_COUPONS_UPDATE, data, header);
        history.goBack();
      } catch (err) {
        console.log(err);
        setPop({ title: parseError(err), type: "error" });
        authFailure(err);
      }
      setBtnLoading(false);
    },
  });

  const categoryType = ["DAILYTRIP", "OUTSTATION", "RENTAL", "CARPOOL", "AMBULANCE", "PACKAGEDELIVERY"];
  const couponType = ["FLAT", "PERCENTAGE"];
  const consumersType = ["EVERYONE", "FIRSTUSER"];

  React.useEffect(() => {
    const fetchPolygons = async () => {
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_CITY_LIST_ALL,
          {
            categoryName: "ride",
          },
          header
        );
        setPolygons(data.data);
      } catch (err) {
        authFailure(err);
      }
    };

    const fetchDetails = async () => {
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_COUPONS_READ,
          {
            id,
          },
          header
        );
        console.log(JSON.stringify(data, null, 2));
        if (data.data) {
          formik1.setFieldValue("city", data.data.serviceArea);
          formik1.setFieldValue("vehicleCategories", data.data.categoryIds);
          formik1.setFieldValue("categoryType", data.data.categoryType);
          formik1.setFieldValue("consumersType", data.data.consumersType);
          formik1.setFieldValue(
            "couponGuideLines",
            data.data?.couponGuideLines ??
              [
                // {
                //   title: "",
                //   points: [""],
                // },
              ]
          );
          formik1.setFieldValue("couponAmount", data.data.couponAmount);
          formik1.setFieldValue("couponCode", data.data.couponCode);
          formik1.setFieldValue("couponCodeMessage", data.data.couponCodeMessage);
          formik1.setFieldValue("couponDescription", data.data.couponDescription);
          formik1.setFieldValue("couponTitle", data.data.couponTitle);
          formik1.setFieldValue("couponPaymentType", data.data?.couponPaymentType ?? []);
          formik1.setFieldValue("couponType", data.data.couponType);
          formik1.setFieldValue("isAutomatic", data.data.isAutomatic === true ? 1 : 0);
          formik1.setFieldValue("isDisplayInApp", data.data.isDisplayInApp === true ? 1 : 0);
          formik1.setFieldValue("isProfessionalTolerance", data.data.isProfessionalTolerance === true ? 1 : 0);
          formik1.setFieldValue("couponMaxAmount", data.data?.couponMaxAmount ?? 0);
          // formik1.setFieldValue("maxCouponAmount", data.data.maxCouponAmount);
          // formik1.setFieldValue("minCouponAmount", data.data.minCouponAmount);
          formik1.setFieldValue("totalUsageCount", data.data.totalUsageCount);
          formik1.setFieldValue("usageCountPerPerson", data.data.usageCountPerPerson);
          if (data.data.vaildFrom || data.data.vaildTo) {
            formik1.setFieldValue("dateLimit", 1);
            formik1.setFieldValue("validFrom", new Date(data.data.vaildFrom));
            formik1.setFieldValue("vaildTo", new Date(data.data.vaildTo));
          }
          formik1.setFieldValue("status", data.data.status);
        }
        setLoading(false);
      } catch (err) {
        alert(err);
        history.goBack();
        authFailure(err);
      }
    };

    fetchPolygons();
    fetchDetails();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  React.useEffect(() => {
    const fetchVehicleCategories = async () => {
      setVehicleCategoryLoading(true);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_VEHCILE_CATEGORY_BASED_ON_CITY,
          {
            city: formik1.values.city,
          },
          header
        );
        setVehicleCategory(data);
      } catch (err) {
        authFailure(err);
      }
      setVehicleCategoryLoading(false);
    };
    if (formik1.values.city !== "") fetchVehicleCategories();
  }, [formik1.values.city]);

  React.useEffect(() => {
    if (polygons.length > 0 && vehicleCategoryLoading === false)
      gsap.fromTo(".addCoupons", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, [polygons, vehicleCategoryLoading]);

  const couponPaymentType = [
    { id: 1, label: language.WALLET, value: "WALLET" },
    { id: 2, label: language.CASH, value: "CASH" },
    { id: 3, label: language.CARD, value: "CARD" },
  ];

  return loading || polygons.length === 0 || vehicleCategoryLoading ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      bread={[{ id: 1, title: language.COUPONS, path: NavLinks.ADMIN_SETUP_COUPONS }]}
      title={language.EDIT}
      width={"3/4"}
      animate={"addCoupons"}
      submitBtn={true}
      submit={formik1.handleSubmit}
      btnLoading={btnLoading}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <Section>
        <Flex align={false}>
          <Section>
            <FieldWrapper title={language.CITY}>
              <DropdownNormal
                error={formik1.errors.city}
                change={(e) => formik1.setFieldValue("city", e)}
                fields={polygons.map((each) => ({
                  id: each._id,
                  label: each.locationName,
                  value: each._id,
                }))}
                defaultValue={
                  polygons.filter((each) => each._id === formik1.values.city).length > 0
                    ? polygons.filter((each) => each._id === formik1.values.city)[0].locationName
                    : ""
                }
              />
            </FieldWrapper>
            {formik1.values.city !== "" && (
              <>
                <FieldWrapper title={language.TITLE}>
                  <TextField
                    placeholder={language.TITLE}
                    error={formik1.errors.couponTitle}
                    change={(e) => formik1.setFieldValue("couponTitle", e)}
                    value={formik1.values.couponTitle}
                  />
                </FieldWrapper>
                <FieldWrapper title={language.DESCRIPTION}>
                  <TextField
                    placeholder={language.DESCRIPTION}
                    error={formik1.errors.couponDescription}
                    change={(e) => formik1.setFieldValue("couponDescription", e)}
                    value={formik1.values.couponDescription}
                  />
                </FieldWrapper>
                <FieldWrapper title={language.MESSAGE}>
                  <TextField
                    placeholder={language.MESSAGE}
                    error={formik1.errors.couponCodeMessage}
                    change={(e) => formik1.setFieldValue("couponCodeMessage", e)}
                    value={formik1.values.couponCodeMessage}
                  />
                </FieldWrapper>
                <div className="flex">
                  <div className="w-1/2">
                    <FieldWrapper title={language.COUPON_CODE}>
                      <TextField
                        placeholder={language.COUPON_CODE}
                        error={formik1.errors.couponCode}
                        change={(e) => formik1.setFieldValue("couponCode", e)}
                        value={formik1.values.couponCode}
                      />
                    </FieldWrapper>
                  </div>
                  <div className="w-1/2">
                    <FieldWrapper title={language.CATEGORY_TYPE}>
                      <DropdownNormal
                        defaultValue={
                          categoryType.filter((each) => each === formik1.values.categoryType).length > 0
                            ? language[categoryType.filter((each) => each === formik1.values.categoryType)[0]]
                            : false
                        }
                        fields={categoryType.map((each, idx) => ({
                          id: idx,
                          label: language[each],
                          value: each,
                        }))}
                        error={formik1.errors.categoryType}
                        change={(e) => formik1.setFieldValue("categoryType", e)}
                      />
                    </FieldWrapper>
                  </div>
                </div>
                <div className="flex">
                  <Section width="2/5" padding={false}>
                    <FieldWrapper title={language.DATE_LIMIT}>
                      <ToggleButton
                        change={(e) => formik1.setFieldValue("dateLimit", e)}
                        defaultValue={formik1.values.dateLimit}
                      />
                    </FieldWrapper>
                  </Section>
                  <Section width="3/5" padding={false}>
                    <FieldWrapper title={language.COUPON_PAYMENT_TYPE}>
                      <MultiSelect
                        error={formik1.errors.couponPaymentType}
                        change={(e) =>
                          formik1.setFieldValue(
                            "couponPaymentType",
                            e.map((eEach) => couponPaymentType.find((each) => each.label === eEach)?.value)
                          )
                        }
                        defaultValue={formik1.values.couponPaymentType?.map(
                          (each) => couponPaymentType?.find((eachContent) => eachContent?.value === each)?.label
                        )}
                        allFields={couponPaymentType.map((each) => each?.label)}
                      ></MultiSelect>
                    </FieldWrapper>
                  </Section>
                </div>
                <div className="flex"></div>
                {formik1.values.dateLimit === 1 && (
                  <div className="flex">
                    <div className="w-1/2">
                      <FieldWrapper title={language.FROM}>
                        <div className="w-full">
                          <CalenderDatePicker
                            showTime={false}
                            disableBeforeDays={true}
                            change={(e) => formik1.setFieldValue("vaildFrom", e)}
                            defaultValue={formik1.values.vaildFrom}
                          />
                        </div>
                      </FieldWrapper>
                    </div>
                    <div className="w-1/2">
                      <FieldWrapper title={language.FROM}>
                        <div className="w-full">
                          <CalenderDatePicker
                            showTime={false}
                            disableBeforeDays={true}
                            change={(e) => formik1.setFieldValue("vaildTo", e)}
                            defaultValue={formik1.values.vaildTo}
                          />
                        </div>
                      </FieldWrapper>
                    </div>
                  </div>
                )}
              </>
            )}
          </Section>
          <Section>
            {formik1.values.city !== "" && (
              <>
                {vehicleCategory && (
                  <FieldWrapper title={language.VEHICLE_CATEGORYS}>
                    <MultiSelect
                      error={formik1.errors.vehicleCategories}
                      change={(e) =>
                        formik1.setFieldValue(
                          "vehicleCategories",
                          e.map((eEach) => vehicleCategory.filter((each) => each.name === eEach)[0]?._id)
                        )
                      }
                      defaultValue={formik1.values.vehicleCategories.map(
                        (each) => vehicleCategory.filter((vehicle) => vehicle._id === each)[0]?.name
                      )}
                      allFields={vehicleCategory.map((each) => each.name)}
                    ></MultiSelect>
                  </FieldWrapper>
                )}
                <FieldWrapper title={language.CONSUMERS_TYPE}>
                  <DropdownNormal
                    defaultValue={
                      consumersType.filter((each) => each === formik1.values.consumersType).length > 0
                        ? language[consumersType.filter((each) => each === formik1.values.consumersType)[0]]
                        : false
                    }
                    fields={consumersType.map((each, idx) => ({
                      id: idx,
                      label: language[each],
                      value: each,
                    }))}
                    error={formik1.errors.consumersType}
                    change={(e) => formik1.setFieldValue("consumersType", e)}
                  />
                </FieldWrapper>
                <div className="flex">
                  <div className="w-1/2">
                    <FieldWrapper title={language.COUPON_TYPE}>
                      <DropdownNormal
                        defaultValue={
                          couponType.filter((each) => each === formik1.values.couponType).length > 0
                            ? language[couponType.filter((each) => each === formik1.values.couponType)[0]]
                            : false
                        }
                        fields={couponType.map((each, idx) => ({
                          id: idx,
                          label: language[each],
                          value: each,
                        }))}
                        error={formik1.errors.couponType}
                        change={(e) => formik1.setFieldValue("couponType", e)}
                      />
                    </FieldWrapper>
                  </div>
                  <div className="w-1/2">
                    <FieldWrapper title={language.AMOUNT}>
                      <TextField
                        type="number"
                        error={formik1.errors.couponAmount}
                        change={(e) => formik1.setFieldValue("couponAmount", e)}
                        value={formik1.values.couponAmount}
                        icon={
                          formik1.values.couponType === "FLAT"
                            ? polygons.filter((each) => each._id === formik1.values.city).length > 0
                              ? polygons.filter((each) => each._id === formik1.values.city)[0].currencySymbol
                              : ""
                            : "%"
                        }
                      />
                    </FieldWrapper>
                  </div>
                </div>
                <div className="flex">
                  <Section padding={false}>
                    <FieldWrapper title={language.COUPON_MAXIMUM_APPLY_AMOUNT}>
                      <TextField
                        type="number"
                        error={formik1.errors.couponMaxAmount}
                        change={(e) => formik1.setFieldValue("couponMaxAmount", e)}
                        value={formik1.values.couponMaxAmount}
                      />
                    </FieldWrapper>
                  </Section>
                  <Section></Section>
                  {/* <FieldWrapper title={language.MINIMUM_AMOUNT}>
                <TextField
                  type="number"
                  error={formik1.errors.minCouponAmount}
                  change={(e) => formik1.setFieldValue("minCouponAmount", e)}
                  value={formik1.values.minCouponAmount}
                />
              </FieldWrapper>
              <FieldWrapper title={language.MAXIMUM_AMOUNT}>
                <TextField
                  type="number"
                  error={formik1.errors.maxCouponAmount}
                  change={(e) => formik1.setFieldValue("maxCouponAmount", e)}
                  value={formik1.values.maxCouponAmount}
                />
              </FieldWrapper> */}
                </div>
                <div className="flex">
                  <FieldWrapper title={language.TOTAL_USAGE}>
                    <TextField
                      type="number"
                      error={formik1.errors.totalUsageCount}
                      change={(e) => formik1.setFieldValue("totalUsageCount", e)}
                      value={formik1.values.totalUsageCount}
                    />
                  </FieldWrapper>
                  <FieldWrapper title={language.MAXIMUM_USAGE_PER_PERSON}>
                    <TextField
                      type="number"
                      error={formik1.errors.usageCountPerPerson}
                      change={(e) => formik1.setFieldValue("usageCountPerPerson", e)}
                      value={formik1.values.usageCountPerPerson}
                    />
                  </FieldWrapper>
                </div>
                <div className="flex">
                  <FieldWrapper title={language.AUTOMATIC}>
                    <ToggleButton
                      change={(e) => formik1.setFieldValue("isAutomatic", e)}
                      defaultValue={formik1.values.isAutomatic}
                    />
                  </FieldWrapper>
                  <FieldWrapper title={language.DISPLAY_IN_APP}>
                    <ToggleButton
                      change={(e) => formik1.setFieldValue("isDisplayInApp", e)}
                      defaultValue={formik1.values.isDisplayInApp}
                    />
                  </FieldWrapper>
                  <FieldWrapper title={language.PROFESSIONAL_TOLERANCE}>
                    <ToggleButton
                      change={(e) => formik1.setFieldValue("isProfessionalTolerance", e)}
                      defaultValue={formik1.values.isProfessionalTolerance}
                    />
                  </FieldWrapper>
                </div>
              </>
            )}
          </Section>
        </Flex>
        <Flex>
          <Section>
            <div className="mt-10 flex flex-wrap">
              {formik1.values.couponGuideLines?.map((each, idx) => (
                // <Border>
                <div className="w-1/2">
                  <Heading title={language.GUIDELINES + " : " + (parseInt(idx) + 1)} />
                  <FieldWrapper title={language.TITLE}>
                    <TextField
                      placeholder={language.TITLE}
                      error={
                        formik1.errors.couponGuideLines &&
                        formik1.errors.couponGuideLines[idx] &&
                        formik1.errors.couponGuideLines[idx].title
                      }
                      change={(e) => formik1.setFieldValue(`couponGuideLines[${idx}].title`, e)}
                      value={formik1.values.couponGuideLines[idx].title}
                    />
                  </FieldWrapper>
                  <FieldWrapper title={language.POINTS}>
                    <div className="flex flex-col w-full">
                      {formik1.values.couponGuideLines[idx].points.map((each, pointIdx) => (
                        <div className="w-full flex mb-4">
                          <TextField
                            placeholder={language.POINTS}
                            error={
                              formik1.errors.couponGuideLines &&
                              formik1.errors.couponGuideLines[idx] &&
                              formik1.errors.couponGuideLines[idx].points &&
                              formik1.errors.couponGuideLines[idx].points[pointIdx]
                            }
                            change={(e) => formik1.setFieldValue(`couponGuideLines[${idx}].points[${pointIdx}]`, e)}
                            value={formik1.values.couponGuideLines[idx].points[pointIdx]}
                          />
                        </div>
                      ))}
                      <div className="flex justify-end">
                        <Button
                          click={() =>
                            formik1.setFieldValue(`couponGuideLines[${idx}].points`, [
                              ...formik1.values.couponGuideLines[idx].points,
                              "",
                            ])
                          }
                          title={language.ADD}
                        />
                        {formik1.values.couponGuideLines[idx].points.length !== 1 && (
                          <Button
                            click={() =>
                              formik1.setFieldValue(
                                `couponGuideLines[${idx}].points`,
                                formik1.values.couponGuideLines[idx].points.filter(
                                  (each, insideIdx) =>
                                    insideIdx !== formik1.values.couponGuideLines[idx].points.length - 1
                                )
                              )
                            }
                            title={language.REMOVE}
                          />
                        )}
                      </div>
                    </div>
                  </FieldWrapper>
                </div>
                // </Border>
              ))}
            </div>
            {formik1.values.couponGuideLines.length === 0 ? (
              <h1 className="text-xl mb-2">{language.GUIDELINES}</h1>
            ) : null}
            <div className="flex">
              <Button
                click={() =>
                  formik1.setFieldValue("couponGuideLines", [
                    ...formik1.values.couponGuideLines,
                    {
                      title: "",
                      points: [""],
                    },
                  ])
                }
                title={language.ADD}
              />
              {formik1.values.couponGuideLines.length > 1 && (
                <Button
                  click={() =>
                    formik1.setFieldValue(
                      "couponGuideLines",
                      formik1.values.couponGuideLines.filter(
                        (each, idx) => idx !== formik1.values.couponGuideLines.length - 1
                      )
                    )
                  }
                  title={language.REMOVE}
                />
              )}
            </div>
          </Section>
        </Flex>
      </Section>
    </FormWrapper>
  );
}
