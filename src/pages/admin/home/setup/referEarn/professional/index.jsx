import React from "react";
import Table from "../../../../../../components/common/Table";
import useLanguage from "../../../../../../hooks/useLanguage";

import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";
import NavLinks from "../../../../../../utils/navLinks.json";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import axios from "axios";
import useAdmin from "../../../../../../hooks/useAdmin";
import { useParams } from "react-router-dom";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { id } = useParams();
  const { admin } = useAdmin();
  return (
    <Table
      title={language.PROFESSIONALS_REFER_EARN}
      startingHeadings={[
        {
          id: 1,
          title: language.UNIQ_CODE,
          key: "uni",
          show: true,
        },
        {
          id: 2,
          title: language.NAME,
          key: "name",
          show: true,
        },
        {
          id: 3,
          title: language.EMAIL,
          key: "email",
          show: true,
        },
        {
          id: 4,
          title: language.PHONE,
          key: "phone",
          show: true,
        },
        {
          id: 5,
          title: language.JOIN_AMOUNT,
          key: "join",
          show: true,
        },
        {
          id: 6,
          title: language.REFERAL_AMOUNT,
          key: "referal",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_REFER_PROFESSIONAL_LIST}
      assignData={(data) =>
        data.map((refer) => ({
          _id: refer._id,
          name: refer.firstName + " " + refer.lastName,
          email: refer.email,
          uni: refer.uniqueCode,
          phone: refer.phone.code + " " + refer.phone.number,
          join: refer.joinCount,
          referal: refer.referrelTotalAmount,
        }))
      }
      bread={[]}
      showFilter={true}
      viewClick={(e) => history.push(NavLinks.ADMIN_SETUP_REFER_PROFESSIONAL_JOINERS_LIST + "/" + e)}
      showAdd={false}
      showArchieve={false}
      showView={true}
      showBulk={false}
      showEdit={false}
      showRefersFilter={true}
      showSearch={true}
      showStatus={false}
    />
  );
}
