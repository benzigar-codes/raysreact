import axios from "axios";
import React from "react";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";

import useAdmin from "../../../../../hooks/useAdmin";
import useLanguage from "../../../../../hooks/useLanguage";

import Table from "../../../../../components/common/Table";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  return (
    <Table
      title={language.CANCELLATION_REASONS}
      startingHeadings={[
        {
          id: 1,
          title: language.REASON,
          key: "reason",
          show: true,
        },
        {
          id: 1,
          title: language.FOR,
          key: "reasonFor",
          show: true,
        },
        {
          id: 5,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_CANCELLATION_LIST}
      assignData={(data) =>
        data.map((cancellation) => ({
          _id: cancellation._id,
          reason: cancellation.data.reason,
          reasonFor:
            cancellation.data.reasonFor === U.USER
              ? language.USER
              : cancellation.data.reasonFor === U.PROFESSIONAL
              ? language.PROFESSIONALS
              : cancellation.data.reasonFor === U.ADMIN
              ? language.ADMIN
              : "",
          status: cancellation.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      showFilter={true}
      showAdd={true}
      showArchieve={true}
      addClick={() => history.push(NavLinks.ADMIN_SETUP_CANCELLATION_REASONS_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_SETUP_CANCELLATION_REASONS_EDIT + "/" + e)}
      showBulk={true}
      showEdit={true}
      showSearch={true}
      statusList={A.HOST + A.ADMIN_CANCELLATION_STATUS}
      showAction={
        admin.privileges.SETUP.CANCELLATION_REASONS
          ? admin.privileges.SETUP.CANCELLATION_REASONS.EDIT
            ? true
            : false
          : false
      }
      add={
        admin.privileges.SETUP.CANCELLATION_REASONS
          ? admin.privileges.SETUP.CANCELLATION_REASONS.ADD
            ? true
            : false
          : false
      }
      edit={
        admin.privileges.SETUP.CANCELLATION_REASONS
          ? admin.privileges.SETUP.CANCELLATION_REASONS.EDIT
            ? true
            : false
          : false
      }
    />
  );
}
