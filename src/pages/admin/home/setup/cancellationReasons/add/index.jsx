import React from "react";
import gsap from "gsap/gsap-core";
import { useFormik } from "formik";
import * as yup from "yup";
import axios from "axios";

import NavLinks from "../../../../../../utils/navLinks.json";
import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";

import useLanguage from "../../../../../../hooks/useLanguage";
import useAdmin from "../../../../../../hooks/useAdmin";

import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { TextField } from "../../../../../../components/common/TextField";
import { DropdownNormal } from "../../../../../../components/common/DropDownNormal";
import { PopUp } from "../../../../../../components/common/PopUp";
import useUtils from "../../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const [popup, setPop] = React.useState(null);
  const { parseError } = useUtils();
  const [btnLoading, setBtnLoading] = React.useState(false);
  const formik = useFormik({
    initialValues: {},
    validateOnChange: false,
    validationSchema: yup.object().shape({
      for: yup.string().required(language.REQUIRED),
      reason: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        await axios.post(
          A.HOST + A.ADMIN_CANCELLATION_UPDATE,
          {
            data: {
              reason: e.reason,
              reasonFor: e.for,
              status: U.ACTIVE,
            },
          },
          header
        );
        history.push(NavLinks.ADMIN_SETUP_CANCELLATION_REASONS);
      } catch (err) {
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
      setBtnLoading(false);
    },
  });
  React.useEffect(() => gsap.fromTo(".addCancel", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 }), []);
  return (
    <FormWrapper
      animate={"addCancel"}
      bread={[{ id: 1, title: language.CANCELLATION_REASONS, path: NavLinks.ADMIN_SETUP_CANCELLATION_REASONS }]}
      title={language.ADD}
      width={"3/5"}
      submitBtn={true}
      btnLoading={btnLoading}
      submit={formik.handleSubmit}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <Section>
        <FieldWrapper animate={"addCancel"} title={language.FOR}>
          <DropdownNormal
            error={formik.errors.for}
            change={(e) => formik.setFieldValue("for", e)}
            fields={[
              { id: 1, label: language.USER, value: "USER" },
              { id: 2, label: language.PROFESSIONAL, value: "PROFESSIONAL" },
              { id: 3, label: language.ADMIN, value: "ADMIN" },
            ]}
          />
        </FieldWrapper>
        <FieldWrapper animate={"addCancel"} title={language.REASON}>
          <TextField
            placeholder={language.REASON}
            error={formik.errors.reason}
            change={(e) => formik.setFieldValue("reason", e)}
            value={formik.values.reason}
          />
        </FieldWrapper>
      </Section>
    </FormWrapper>
  );
}
