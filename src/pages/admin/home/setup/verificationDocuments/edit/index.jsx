import React from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import axios from "axios";
import gsap from "gsap/gsap-core";

import useLanguage from "../../../../../../hooks/useLanguage";
import useAdmin from "../../../../../../hooks/useAdmin";
import useUtils from "../../../../../../hooks/useUtils";

import NavLinks from "../../../../../../utils/navLinks.json";
import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../../components/common/TextField";
import { PopUp } from "../../../../../../components/common/PopUp";
import { TextArea } from "../../../../../../components/common/TextArea";
import { useParams } from "react-router-dom";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";

export default function Index({ history }) {
  // HOOKS
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { parseError } = useUtils();
  const { id } = useParams();
  // STATES
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);
  const [loading, setLoading] = React.useState(false);
  // FORMIK
  const formik = useFormik({
    initialValues: {
      doc_name: "",
      doc_des: "",
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      doc_name: yup.string().required(language.REQUIRED),
      doc_des: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        await axios.post(
          A.HOST + A.ADMIN_VERIFICATION_UPDATE,
          {
            id,
            data: {
              docName: e.doc_name,
              docDescription: e.doc_des,
              status: U.ACTIVE,
            },
          },
          header
        );
        history.push(NavLinks.ADMIN_SETUP_VERIFICATION_DOCUMENTS);
      } catch (err) {
        authFailure(err);
        setPop({ title: parseError(), type: "error" });
      }
      setBtnLoading(false);
    },
  });

  React.useEffect(() => {
    const fetchVerify = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_VERIFICAIINO_READ, { id }, header);
        formik.setFieldValue("doc_name", data.data.docName);
        formik.setFieldValue("doc_des", data.data.docDescription);
        setLoading(false);
        gsap.fromTo(".addVerifyDoc", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
      } catch (err) {
        authFailure(err);
        history.goBack();
      }
    };
    fetchVerify();
  }, []);
  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      bread={[
        { id: 1, title: language.SETUP },
        { id: 2, title: language.VERIFY_DOCUMENTS_LIST, path: NavLinks.ADMIN_SETUP_VERIFICATION_DOCUMENTS },
      ]}
      btnLoading={btnLoading}
      width={"3/4"}
      animate={"addVerifyDoc"}
      title={language.EDIT + " " + formik.values.doc_name}
      submit={formik.handleSubmit}
      submitBtn={true}
    >
      <Section>
        {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}

        <FieldWrapper animate={"addVerifyDoc"} title={language.DOCUMENT_NAME}>
          <TextField
            error={formik.errors.doc_name}
            change={(e) => formik.setFieldValue("doc_name", e)}
            value={formik.values.doc_name}
            placeholder={language.DOCUMENT_NAME}
          />
        </FieldWrapper>
      </Section>
      <Section>
        <FieldWrapper animate={"addVerifyDoc"} title={language.DOCUMENT_DESCRIPTION}>
          <TextArea
            error={formik.errors.doc_des}
            change={(e) => formik.setFieldValue("doc_des", e)}
            value={formik.values.doc_des}
            placeholder={language.DOCUMENT_DESCRIPTION}
          />
        </FieldWrapper>
      </Section>
    </FormWrapper>
  );
}
