import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  return (
    <Table
      title={language.ONBOARDING_COMMON_DOC_LIST}
      startingHeadings={[
        {
          id: 1,
          title: language.DOCUMENT_NAME,
          key: "doc_name",
          show: true,
        },
        {
          id: 2,
          title: language.DOCUMENT_DESCRIPTION,
          key: "doc_des",
          show: true,
        },
        {
          id: 3,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_VERIFICATION_LIST}
      assignData={(data) =>
        data.map((verify) => ({
          _id: verify._id,
          doc_name: verify.data.docName,
          doc_des: verify.data.docDescription,
          status: verify.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      bread={[]}
      showAdd={
        admin.privileges.SETUP.VERIFICATION_DOCUMENTS
          ? admin.privileges.SETUP.VERIFICATION_DOCUMENTS.ADD
            ? true
            : false
          : false
      }
      showArchieve={false}
      showEdit={
        admin.privileges.SETUP.VERIFICATION_DOCUMENTS
          ? admin.privileges.SETUP.VERIFICATION_DOCUMENTS.EDIT
            ? true
            : false
          : false
      }
      addClick={() => history.push(NavLinks.ADMIN_SETUP_VERIFICATION_DOCUMENTS_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_SETUP_VERIFICATION_DOCUMENTS_EDIT + "/" + e)}
      showBulk={true}
      showSearch={true}
      showAction={
        admin.privileges.SETUP.VERIFICATION_DOCUMENTS
          ? admin.privileges.SETUP.VERIFICATION_DOCUMENTS.EDIT
            ? true
            : false
          : false
      }
      add={
        admin.privileges.SETUP.VERIFICATION_DOCUMENTS
          ? admin.privileges.SETUP.VERIFICATION_DOCUMENTS.ADD
            ? true
            : false
          : false
      }
      edit={
        admin.privileges.SETUP.VERIFICATION_DOCUMENTS
          ? admin.privileges.SETUP.VERIFICATION_DOCUMENTS.EDIT
            ? true
            : false
          : false
      }
      statusList={A.HOST + A.ADMIN_VERIFICATION_STATUS}
    />
  );
}
