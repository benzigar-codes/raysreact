import React from "react";
import * as yup from "yup";
import axios from "axios";
import { useFormik } from "formik";
import gsap from "gsap/gsap-core";

import "tinymce/tinymce";
import "tinymce/icons/default";
import "tinymce/themes/silver";
import "tinymce/plugins/paste";
import "tinymce/plugins/link";
import "tinymce/plugins/image";
import "tinymce/plugins/table";
import "tinymce/skins/ui/oxide/skin.min.css";
import "tinymce/skins/ui/oxide/content.min.css";
import "tinymce/skins/content/default/content.min.css";
import { Editor } from "@tinymce/tinymce-react";

import NavLinks from "../../../../../../utils/navLinks.json";
import U from "../../../../../../utils/utils.js";
import A from "../../../../../../utils/API.js";

import useAdmin from "../../../../../../hooks/useAdmin";
import useImage from "../../../../../../hooks/useImage";
import useUtils from "../../../../../../hooks/useUtils";
import useLanguage from "../../../../../../hooks/useLanguage";

import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { TextField } from "../../../../../../components/common/TextField";
import { DropdownNormal } from "../../../../../../components/common/DropDownNormal";
import { TextArea } from "../../../../../../components/common/TextArea";
import { FileUpload } from "../../../../../../components/common/FileUpload";
import Flex from "../../../../../../components/common/Flex";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";
import { PopUp } from "../../../../../../components/common/PopUp";
import { useParams } from "react-router";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import { Button } from "../../../../../../components/common/Button";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { parseError } = useUtils();
  const { compressImage, imageUrl, isBase64 } = useImage();

  const [showCode, setShowCode] = React.useState(false);

  const [btnLoading, setBtnLoading] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [popup, setPop] = React.useState(null);
  const docsFor = [
    { id: 1, label: language.PROFESSIONALS, value: U.PROFILE_DOCUMENTS },
    { id: 2, label: language.VEHICLES, value: U.DRIVER_DOCUMENTS },
  ];

  const formik = useFormik({
    initialValues: {
      name: "",
      emailSubject: "",
      senderName: "",
      senderEmail: "",
      emailContent: "",
      status: U.ACTIVE,
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      name: yup
        .string()
        .required(language.REQUIRED)
        .min(4, language.MIN + " " + 4),
      emailSubject: yup.string().required(language.REQUIRED),
      senderName: yup.string().required(language.REQUIRED),
      senderEmail: yup.string().email(language.INVALID_EMAIL).required(language.REQUIRED),
      emailContent: yup.string(),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        const data = {
          name: e.name,
          emailSubject: e.emailSubject,
          senderName: e.senderName,
          senderEmail: e.senderEmail,
          emailContent: e.emailContent,
          status: e.status,
        };
        await axios.post(A.HOST + A.ADMIN_SETUP_EMAIL_TEMPLATES_UPDATE, data, header);
        history.goBack();
      } catch (err) {
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
      setBtnLoading(false);
    },
  });

  React.useEffect(() => {
    setLoading(false);
    setTimeout(() => gsap.fromTo(".emailEdit", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 }), 200);
  }, []);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      animate={"emailEdit"}
      width="3/4"
      btnLoading={btnLoading}
      submit={formik.handleSubmit}
      bread={[
        {
          id: 1,
          title: language.EMAIL_TEMPLATES,
          path: NavLinks.ADMIN_SETUP_EMAIL_TEMPLATES,
        },
      ]}
      title={language.ADD}
      submitBtn={true}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <div className="w-full flex-col">
        <div className="flex">
          <Section padding={false}>
            <FieldWrapper animate="emailEdit" title={language.NAME}>
              <TextField
                error={formik.errors.name}
                change={(e) => formik.setFieldValue("name", e)}
                value={formik.values.name}
              />
            </FieldWrapper>
            <FieldWrapper animate="emailEdit" title={language.EMAIL_SUBJECT}>
              <TextField
                error={formik.errors.emailSubject}
                change={(e) => formik.setFieldValue("emailSubject", e)}
                value={formik.values.emailSubject}
              />
            </FieldWrapper>
          </Section>
          <Section padding={false}>
            <FieldWrapper animate="emailEdit" title={language.SENDER_NAME}>
              <TextField
                error={formik.errors.senderName}
                change={(e) => formik.setFieldValue("senderName", e)}
                value={formik.values.senderName}
              />
            </FieldWrapper>
            <FieldWrapper animate="emailEdit" title={language.SENDER_EMAIL}>
              <TextField
                error={formik.errors.senderEmail}
                change={(e) => formik.setFieldValue("senderEmail", e)}
                value={formik.values.senderEmail}
              />
            </FieldWrapper>
          </Section>
        </div>
        <div className="flex flex justify-center items-center">
          <FieldWrapper title={language.EMAIL_CONTENT}>
            <div className="flex flex-col">
              <div className="mb-2">
                <Button
                  title={showCode === false ? language.SHOW_CODE : language.SHOW_EDITOR}
                  click={() => setShowCode(!showCode)}
                ></Button>
              </div>
              {showCode ? (
                <TextArea
                  style={{
                    width: 900,
                  }}
                  rows="20"
                  change={(e) => formik.setFieldValue("emailContent", e)}
                  value={formik.values.emailContent}
                />
              ) : (
                <Editor
                  init={{
                    selector: "textarea",
                    skin: false,
                    height: 500,
                    width: 900,
                    paste_data_images: true,
                    images_upload_handler: function (blobInfo, success, failure) {
                      success("data:" + blobInfo.blob().type + ";base64," + blobInfo.base64());
                    },
                    menubar: "file edit view insert format tools table help",
                    plugins:
                      "code uploadimage print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap tinycomments mentions quickbars linkchecker emoticons advtable export",
                    content_css: "dark",
                    toolbar:
                      "undo redo | formatselect | " +
                      "bold italic backcolor | alignleft aligncenter " +
                      "alignright alignjustify | bullist numlist outdent indent | " +
                      "removeformat | code",
                  }}
                  value={formik.values.emailContent}
                  onEditorChange={(e) => formik.setFieldValue("emailContent", e)}
                />
              )}
            </div>
          </FieldWrapper>
        </div>
      </div>
    </FormWrapper>
  );
}
