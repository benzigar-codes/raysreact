import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import axios from "axios";
import useAdmin from "../../../../../hooks/useAdmin";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin, header, authFailure } = useAdmin();
  return (
    <Table
      title={language.EMAIL_TEMPLATES}
      startingHeadings={[
        {
          id: 1,
          title: language.NAME,
          key: "name",
          show: true,
        },
        {
          id: 2,
          title: language.EMAIL_SUBJECT,
          key: "email_subject",
          show: true,
        },
        {
          id: 3,
          title: language.SENDER_NAME,
          key: "sender_name",
          show: true,
        },
        {
          id: 4,
          title: language.SENDER_EMAIL,
          key: "sender_email",
          show: true,
        },

        {
          id: 5,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_SETUP_EMAIL_TEMPLATES_LISTS}
      assignData={(data) =>
        data.map((template) => ({
          _id: template._id,
          name: template.data.name,
          email_subject: template.data.emailSubject,
          sender_name: template.data.senderName,
          sender_email: template.data.senderEmail,
          status: template.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      bread={[]}
      showFilter={true}
      showAdd={true}
      showStatus={true}
      showActiveInactiveFilter={true}
      addClick={() => history.push(NavLinks.ADMIN_SETUP_EMAIL_TEMPLATES_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_SETUP_EMAIL_TEMPLATES_EDIT + "/" + e)}
      showBulk={false}
      statusList={A.HOST + A.ADMIN_SETUP_EMAIL_TEMPLATES_STATUS_CHANGE}
      showEdit={true}
      showSearch={true}
      showAction={admin.privileges.SETUP.DOCUMENTS ? (admin.privileges.SETUP.DOCUMENTS.EDIT ? true : false) : false}
      add={admin.privileges.SETUP.DOCUMENTS ? (admin.privileges.SETUP.DOCUMENTS.ADD ? true : false) : false}
      edit={admin.privileges.SETUP.DOCUMENTS ? (admin.privileges.SETUP.DOCUMENTS.EDIT ? true : false) : false}
    />
  );
}
