import React from "react";
import axios from "axios";

import Table from "../../../../../components/common/Table";
import { SmallLoader } from "../../../../../components/common/SmallLoader";

import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin, header, authFailure } = useAdmin();
  const [cities, setCities] = React.useState(null);
  const { morph } = useUtils();
  const fetchCities = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_CITY_LIST_ALL, {}, header);
      setCities(data.data);
    } catch (er) {
      authFailure(er);
    }
  };
  React.useEffect(() => {
    fetchCities();
  }, []);
  return cities === null ? (
    <SmallLoader />
  ) : (
    <Table
      title={language.TOLLS}
      startingHeadings={[
        {
          id: 1,
          title: language.TOLL_NAME,
          key: "name",
          show: true,
        },
        {
          id: 3,
          title: language.CITY,
          key: "city",
          show: true,
        },
        // {
        //   id: 2,
        //   title: language.ADDRESS,
        //   key: "address",
        //   show: true,
        // },
        {
          id: 4,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_TOLL_LIST}
      assignData={(data) =>
        data.map((toll) => ({
          _id: toll._id,
          name: toll.data.tollName,
          city:
            toll.data.city && cities.find((each) => each._id === toll.data.city)
              ? cities.find((each) => each._id === toll.data.city).locationName
              : "",
          address: toll.data?.locationAddress?.fullAddress || "",
          status: toll.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      showAdd={admin.privileges.SETUP.TOLLS ? (admin.privileges.SETUP.TOLLS.ADD ? true : false) : false}
      showArchieve={false}
      showEdit={admin.privileges.SETUP.TOLLS ? (admin.privileges.SETUP.TOLLS.EDIT ? true : false) : false}
      addClick={() => history.push(NavLinks.ADMIN_SETUP_TOLLS_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_SETUP_TOLLS_EDIT + "/" + e)}
      showBulk={true}
      showView={false}
      showCityFilter={true}
      showSearch={true}
      viewClick={(e) => history.push(NavLinks.ADMIN_SETUP_CITY_COMMISION_VIEW + "/" + e)}
      showAction={admin.privileges.SETUP.TOLLS ? (admin.privileges.SETUP.TOLLS.EDIT ? true : false) : false}
      add={admin.privileges.SETUP.TOLLS ? (admin.privileges.SETUP.TOLLS.ADD ? true : false) : false}
      edit={admin.privileges.SETUP.TOLLS ? (admin.privileges.SETUP.TOLLS.EDIT ? true : false) : false}
      statusList={A.HOST + A.ADMIN_TOLL_STATUS_CHANGE}
    />
  );
}
