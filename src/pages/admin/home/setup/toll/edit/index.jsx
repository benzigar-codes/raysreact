import React from "react";
import { Loader } from "@googlemaps/js-api-loader";
import { useFormik } from "formik";
import { Helmet } from "react-helmet";
import axios from "axios";
import * as turf from "@turf/turf";
import * as yup from "yup";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../../components/common/TextField";
import { DropdownNormal } from "../../../../../../components/common/DropDownNormal";
import GooglePlaceComplete from "../../../../../../components/common/GooglePlaceComplete";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import EmptyImage from "../../../../../../assets/images/empty.jpeg";

import useLanguage from "../../../../../../hooks/useLanguage";
import useSettings from "../../../../../../hooks/useSettings";

import NavLinks from "../../../../../../utils/navLinks.json";
import U from "../../../../../../utils/utils.js";
import A from "../../../../../../utils/API.js";

import useAdmin from "../../../../../../hooks/useAdmin";
import { MultiSelect } from "../../../../../../components/common/MultiSelect";
import gsap from "gsap/gsap-core";
import { PopUp } from "../../../../../../components/common/PopUp";
import useUtils from "../../../../../../hooks/useUtils";
import { FiInfo } from "react-icons/fi";
import { useParams } from "react-router";

export default function Index({ history }) {
  // HOOKS
  const { language } = useLanguage();
  const { settings } = useSettings();
  const { header, authFailure } = useAdmin();
  const { parseError } = useUtils();
  const { id } = useParams();

  // GOOGLE LOAD DETECTION
  const [googleLoaded, setGoogleLoaded] = React.useState(false);

  // POLYGONS
  const [polygons, setPolygons] = React.useState([]);
  const [polygonsLoaded, setPolygonsLoader] = React.useState(false);
  const [address, setAddress] = React.useState(null);

  // IF CITIES INTERSECT
  const [pointsWithin, setPointsWithin] = React.useState(null);

  // VEHICLE CATEGORY FROM API
  const [vehicleCategory, setVehicleCategory] = React.useState([]);
  const [finalCoOrdinates, setFinalCoOrdinates] = React.useState(null);

  const [firstTime, setFirstTime] = React.useState(true);

  const Map = React.useRef();
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);

  const formik1 = useFormik({
    initialValues: {
      city: "",
      tollName: "",
      vehicleCategories: [],
      tollAmount: {},
      status: U.ACTIVE,
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      city: yup.string().required(language.REQUIRED),
      tollName: yup.string().required(language.REQUIRED),
      vehicleCategories: yup.array().min(1, language.ERROR_SELECT_VEHICLE),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        const data = {
          id,
          data: {
            serviceArea: formik1.values.city,
            tollName: formik1.values.tollName,
            locationAddress: {
              placeId: address.placeId,
              addressType: "OTHER",
              addressName: address.selectedAddress,
              shortAddress: address.selectedAddress,
              fullAddress: address.selectedAddress,
              lat: address.results[0].geometry.location.lat,
              lng: address.results[0].geometry.location.lng,
            },
            locationCoordinates: {
              type: "Polygon",
              coordinates: finalCoOrdinates,
            },
            tollCategorys: formik1.values.vehicleCategories.map((each) => ({
              categoryId: each,
              tollAmount: formik1.values.tollAmount[each] ? parseInt(formik1.values.tollAmount[each]) : 0,
            })),
            status: e.status,
          },
        };
        await axios.post(A.HOST + A.ADMIN_TOLL_UPDATE, data, header);
        history.goBack();
      } catch (err) {
        setPop({ title: parseError(err), type: "error" });
        authFailure(err);
      }
      setBtnLoading(false);
    },
  });

  React.useEffect(() => {
    const fetchPolygons = async () => {
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_CITY_POLYGON_LIST,
          {
            categoryName: "ride",
          },
          header
        );
        setPolygons(data);
        setPolygonsLoader(true);
      } catch (err) {
        authFailure(err);
      }
    };

    fetchPolygons();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  React.useEffect(() => {
    if (formik1.values.vehicleCategories) {
      formik1.values.vehicleCategories.map((each) =>
        formik1.setFieldValue(
          `tollAmount.${each}`,
          formik1.values.tollAmount[each] ? formik1.values.tollAmount[each] : 0
        )
      );
    }
  }, [formik1.values.vehicleCategories]);

  React.useEffect(() => {
    const fetchVehicleCategories = async () => {
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_VEHCILE_CATEGORY_BASED_ON_CITY,
          {
            city: formik1.values.city,
          },
          header
        );
        setVehicleCategory(data);
      } catch (err) {
        authFailure(err);
      }
    };
    if (formik1.values.city !== "") fetchVehicleCategories();
  }, [formik1.values.city]);

  React.useEffect(() => {
    const loader = new Loader({
      apiKey: settings.mapApi.web,
      version: "weekly",
      libraries: ["places"],
    });
    loader
      .load()
      .then(() => {
        setGoogleLoaded(true);
      })
      .catch((e) => {});
  }, []);

  const createMap = ({ target = Map.current, center = { lat: 12.9791551, lng: 80.2007085 }, zoom = 14 }) => {
    const map = new window.google.maps.Map(target, {
      center,
      zoom: zoom,
      zoomControl: true,
      mapTypeControl: false,
      scaleControl: true,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: true,
    });
    return map;
  };

  // MAP AND STUFFFFF
  React.useEffect(() => {
    if (polygonsLoaded === true && address && googleLoaded === true && formik1.values.city !== "") {
      setPointsWithin(null);
      let center = address ? address.results[0].geometry.location : { lat: 12.9791551, lng: 80.2007085 };
      const map = createMap({ target: Map.current, center, zoom: 15 });
      let googlePolygons = [];
      let googleMarkers = [];
      const selectedCity =
        polygons.filter((each) => each._id === formik1.values.city).length > 0
          ? polygons.filter((each) => each._id === formik1.values.city)[0]
          : null;

      if (firstTime === false) {
        formik1.setFieldValue("tollName", "");
        formik1.setFieldValue("vehicleCategories", []);
        formik1.setFieldValue("tollAmount", {});
      }

      // DRAW POLYGONS OF SERVICES
      if (polygons.length !== 0 && selectedCity) {
        const coordinates = selectedCity.locationCoordinates[0];
        const coordinatesForPath = coordinates.map((each) => ({ lat: each[1], lng: each[0] }));
        googlePolygons.push(
          new window.google.maps.Polygon({
            paths: coordinatesForPath,
            map,
            strokeColor: "#262525",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#fc8621",
            fillOpacity: 0.4,
          })
        );
        const centerMass = turf.centerOfMass(turf.polygon(selectedCity.locationCoordinates)).geometry.coordinates;
        googleMarkers.push(
          new window.google.maps.Marker({
            position: { lat: centerMass[1], lng: centerMass[0] },
            label: {
              text: selectedCity.locationName,
              color: "white",
              fontSize: "22px",
              fontWeight: "bold",
            },
            icon: EmptyImage,
            map: map,
          })
        );
      }

      // DRAW POLYGON OF SELECTED AREA
      if (address) {
        let corners = [];
        let cac = address.results[0].geometry.location;
        if (finalCoOrdinates === null || address) {
          let d = 20;

          for (let i = 1; i <= 16; i++) {
            let angle = 22.5 * i;
            let R = 300000;
            let latitude1 = cac.lat * (3.1415926535898 / 180);
            let longitude1 = cac.lng * (3.1415926535898 / 180);
            let brng = angle * (3.1415926535898 / 180);
            let latitude2 = Math.asin(
              Math.sin(latitude1) * Math.cos(d / R) + Math.cos(latitude1) * Math.sin(d / R) * Math.cos(brng)
            );

            let longitude2 =
              longitude1 +
              Math.atan2(
                Math.sin(brng) * Math.sin(d / R) * Math.cos(latitude1),
                Math.cos(d / R) - Math.sin(latitude1) * Math.sin(latitude2)
              );
            latitude2 = latitude2 * (180 / 3.1415926535898);
            longitude2 = longitude2 * (180 / 3.1415926535898);
            corners.push(latitude2, longitude2);
          }

          corners = [
            new window.google.maps.LatLng(corners[0], corners[1]),
            // new window.google.maps.LatLng(corners[4], corners[5]),
            new window.google.maps.LatLng(corners[8], corners[9]),
            // new window.google.maps.LatLng(corners[12], corners[13]),
            new window.google.maps.LatLng(corners[16], corners[17]),
            // new window.google.maps.LatLng(corners[20], corners[21]),
            new window.google.maps.LatLng(corners[24], corners[25]),
            // new window.google.maps.LatLng(corners[28], corners[29]),
            // new window.google.maps.LatLng(corners[0], corners[1]),
          ];
        }

        let myPolygon = new window.google.maps.Polygon({
          paths: firstTime
            ? finalCoOrdinates[0].map((each) => new window.google.maps.LatLng(each[1], each[0]))
            : corners,
          map: map,
          draggable: true,
          editable: true,
          strokeColor: "#262525",
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: "#ba9c9c",
          fillOpacity: 0.4,
        });

        if (firstTime) {
          var bounds = new window.google.maps.LatLngBounds();
          finalCoOrdinates[0].forEach((location) => bounds.extend({ lat: location[1], lng: location[0] }));
          map.fitBounds(bounds);
        }
        // map.setBounds(finalCoOrdinates[0].map((each) => new window.google.maps.LatLng(each[1], each[0])));

        async function getPolygonCoords() {
          myPolygon.setOptions({ fillColor: "green" });
          let len = myPolygon.getPath().getLength();
          cac.htmlStr = [];
          cac.coordi = [];
          cac.coordi.push([
            parseFloat(myPolygon.getPath().getAt(0).toUrlValue(9).split(",")[1]),
            parseFloat(myPolygon.getPath().getAt(0).toUrlValue(9).split(",")[0]),
          ]);

          for (let i = 0; i < len; i++) {
            cac.htmlStr.push([
              parseFloat(myPolygon.getPath().getAt(i).toUrlValue(9).split(",")[1]),
              parseFloat(myPolygon.getPath().getAt(i).toUrlValue(9).split(",")[0]),
            ]);
          }
          cac.final_nodes = cac.htmlStr.concat(cac.coordi);

          // FINDING IF INSIDE

          let cornersArray = [cac.final_nodes];
          let inside = true;

          cornersArray[0].forEach((each) => {
            var intersection = turf.booleanContains(turf.polygon(selectedCity.locationCoordinates), turf.point(each));
            if (intersection === false) inside = false;
          });

          // One Point Outside
          if (inside) {
            googlePolygons[0].setOptions({ fillColor: "#fc8621" });
            myPolygon.setOptions({ fillColor: "green" });
          } else {
            googlePolygons[0].setOptions({ fillColor: "red" });
            myPolygon.setOptions({ fillColor: "red" });
          }

          setPointsWithin(inside === true ? true : false);

          if (inside === true) {
            setFinalCoOrdinates(cornersArray);
            let lat = turf.centerOfMass(turf.polygon(cornersArray)).geometry.coordinates[1];
            let lng = turf.centerOfMass(turf.polygon(cornersArray)).geometry.coordinates[0];
            try {
              const { data } = await axios.get(
                `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${settings.mapApi.web}`
              );
            } catch (err) {}
            // formik1.setFieldValue("city");
          }
        }

        getPolygonCoords();
        // window.google.maps.event.addListener(myPolygon, "drag", getPolygonCoords);
        window.google.maps.event.addListener(myPolygon, "mouseup", getPolygonCoords);
        window.google.maps.event.addListener(map, "zoom_changed", function () {
          var zoom = map.getZoom();
          if (zoom <= 8) {
            googleMarkers.forEach((each) => each.setMap(null));
          } else {
            googleMarkers.forEach((each) => each.setMap(map));
          }
        });
      }
      setFirstTime(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [googleLoaded, formik1.values.city, polygons, polygonsLoaded, address]);

  const fetchDetails = async () => {
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_TOLL_READ,
        {
          id,
        },
        header
      );
      formik1.setFieldValue("city", data?.data?.serviceArea || "");
      formik1.setFieldValue("tollName", data?.data?.tollName || "");
      formik1.setFieldValue("status", data?.data?.status || "");
      formik1.setFieldValue(
        "vehicleCategories",
        data.data.tollCategorys.map((each) => each.categoryId)
      );
      let tollAmount = {};
      data.data.tollCategorys.forEach((each) => {
        tollAmount[each.categoryId] = each.tollAmount;
      });
      setPointsWithin(true);
      formik1.setFieldValue("tollAmount", tollAmount);
      setFinalCoOrdinates(data?.data?.locationCoordinates?.coordinates || null);

      const { status, data: googleData } = await axios.get(
        `https://maps.googleapis.com/maps/api/geocode/json?place_id=${data.data.locationAddress.placeId}&key=${settings.mapApi.web}`
      );
      if (status === 200)
        setAddress({
          ...googleData,
          selectedAddress: data.data.locationAddress.fullAddress,
          placeId: data.data.locationAddress.placeId,
        });
    } catch (err) {
      alert(err);
      authFailure(err);
      history.goBack();
    }
  };

  React.useEffect(() => {
    if (polygons && Array.isArray(polygons) && polygons.length > 0) fetchDetails();
  }, [polygons]);

  React.useEffect(() => {
    if (googleLoaded && vehicleCategory.length > 0 && polygons && address)
      gsap.fromTo(".addCity", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.3 });
  }, [googleLoaded, vehicleCategory, polygons, address]);

  return googleLoaded ? (
    <FormWrapper
      bread={[{ id: 1, title: language.TOLLS, path: NavLinks.ADMIN_SETUP_TOLLS }]}
      title={language.EDIT}
      width={"3/4"}
      animate={"addCity"}
      submitBtn={pointsWithin}
      submit={formik1.handleSubmit}
      btnLoading={btnLoading}
    >
      <Helmet>
        <script
          src={`https://maps.googleapis.com/maps/api/js?key=${settings.mapApi.web}&libraries=places`}
          async
        ></script>
      </Helmet>
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <Section>
        {polygons && (
          <FieldWrapper title={language.CITY}>
            <DropdownNormal
              error={formik1.errors.city}
              change={(e) => formik1.setFieldValue("city", e)}
              fields={polygons.map((each) => ({
                id: each._id,
                label: each.locationName,
                value: each._id,
              }))}
              defaultValue={
                polygons.filter((each) => each._id === formik1.values.city).length > 0
                  ? polygons.filter((each) => each._id === formik1.values.city)[0].locationName
                  : ""
              }
            />
          </FieldWrapper>
        )}
        {formik1.values.city !== "" && (
          <>
            <FieldWrapper title={language.ADDRESS}>
              <GooglePlaceComplete
                change={(e) => {
                  setAddress(e);
                }}
                placeholder={language.ADDRESS}
              />
            </FieldWrapper>
            {pointsWithin === true && (
              // <div className="flex items-center">
              <FieldWrapper>
                <FiInfo className="text-red-500" />
                <p className="mx-2 text-sm text-red-500">{language.CHANGING_ABOVE_FIELDS_TOLL}</p>
              </FieldWrapper>
              // </div>
            )}
            {pointsWithin === false && (
              <FieldWrapper>
                <p className="text-sm text-red-500">{language.TOLL_SHOULD_NOT_FALL_OUTSIDE}</p>
              </FieldWrapper>
            )}
            {pointsWithin === true && (
              <>
                <FieldWrapper title={language.TOLL_NAME}>
                  <TextField
                    error={formik1.errors.tollName}
                    change={(e) => formik1.setFieldValue("tollName", e)}
                    value={formik1.values.tollName}
                  />
                </FieldWrapper>
                {vehicleCategory && vehicleCategory.length > 0 && formik1.values.vehicleCategories && (
                  <FieldWrapper title={language.VEHICLE_CATEGORYS}>
                    <MultiSelect
                      error={formik1.errors.vehicleCategories}
                      change={(e) =>
                        formik1.setFieldValue(
                          "vehicleCategories",
                          e.map((eEach) => vehicleCategory.filter((each) => each.name === eEach)[0]?._id)
                        )
                      }
                      defaultValue={formik1.values.vehicleCategories.map((each) =>
                        vehicleCategory.filter((vehicle) => vehicle._id === each).length > 0
                          ? vehicleCategory.filter((vehicle) => vehicle._id === each)[0]?.name
                          : ""
                      )}
                      allFields={vehicleCategory.map((each) => each.name)}
                    ></MultiSelect>
                  </FieldWrapper>
                )}
                {vehicleCategory &&
                  vehicleCategory.length > 0 &&
                  formik1.values.vehicleCategories &&
                  Array.isArray(formik1.values.vehicleCategories) &&
                  formik1.values.vehicleCategories.length > 0 &&
                  formik1.values.vehicleCategories.map((each) => (
                    <FieldWrapper
                      title={
                        language.TOLL_AMOUNT +
                        " (" +
                        vehicleCategory.filter((eachCat) => eachCat._id === each)[0]?.name +
                        ")"
                      }
                    >
                      <TextField
                        change={(e) => formik1.setFieldValue(`tollAmount.${each}`, e)}
                        value={formik1.values.tollAmount[each]}
                      />
                    </FieldWrapper>
                  ))}
              </>
            )}
          </>
        )}
      </Section>
      <Section>
        {/* {formik1.values.city !== "" && polygonsLoaded === true && (address || finalCoOrdinates !== null) && ( */}
        <FieldWrapper title={language.MAP}>
          <div ref={Map} style={{ width: "100%", height: 400 }}></div>
        </FieldWrapper>
        {/* )} */}
      </Section>
    </FormWrapper>
  ) : (
    <SmallLoader />
  );
}
