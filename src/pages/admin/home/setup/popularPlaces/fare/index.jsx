import React from "react";
import navLinks from "../../../../../../utils/navLinks.json";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../../components/common/TextField";
import { Border } from "../../../../../../components/common/Border";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";

import useLanguage from "../../../../../../hooks/useLanguage";
import { Heading } from "../../../../../../components/common/Heading";
import Flex from "../../../../../../components/common/Flex";

export default function Index() {
  const { language } = useLanguage();
  return (
    <FormWrapper
      bread={[
        {
          id: 1,
          title: language.SubMenu.CITY_COMMISSION,
          path: navLinks.ADMIN_SETUP_CITY_COMMISION,
        },
      ]}
      title={language.City.FARE_SETUP}
      width="3/4"
      submitBtn={true}
    >
      <Section>
        <Border>
          <Heading animate="generalSettings" title={"MACRO"} />
          <Flex>
            <Section padding={false}>
              <FieldWrapper title={language.City.BASE_FARE}>
                <TextField icon={"$"} />
              </FieldWrapper>
            </Section>
            <Section padding={false}>
              <FieldWrapper title={language.City.FARE_PER_KM}>
                <TextField icon={"$"} />
              </FieldWrapper>
            </Section>
          </Flex>
          <Flex>
            <Section padding={false}>
              <FieldWrapper title={language.City.FARE_PER_TIME}>
                <TextField icon={"$"} />
              </FieldWrapper>
            </Section>
            <Section padding={false}>
              <FieldWrapper title={language.City.MINIMUM_METERS}>
                <TextField icon={language.City.METERS} />
              </FieldWrapper>
            </Section>
          </Flex>
          <Flex>
            <Section padding={false}>
              <FieldWrapper title={language.City.MINIMUM_FARE}>
                <TextField icon={"$"} />
              </FieldWrapper>
            </Section>
            <Section padding={false}>
              <FieldWrapper title={language.City.BOOKING_FEE}>
                <TextField icon={"$"} />
              </FieldWrapper>
            </Section>
          </Flex>
          <FieldWrapper title={language.City.CANCELLATION_FEE}>
            <ToggleButton />
          </FieldWrapper>
          <Flex>
            <Section padding={false}>
              <FieldWrapper title={language.City.CANCEL_AMOUNT}>
                <TextField icon={"$"} />
              </FieldWrapper>
            </Section>
            <Section padding={false}>
              <FieldWrapper title={language.City.CANCEL_MINUTES}>
                <TextField icon={language.City.MINUTES} />
              </FieldWrapper>
            </Section>
          </Flex>
        </Border>
      </Section>
      <Section>
        <Border>
          <Heading animate="generalSettings" title={"MACRO"} />
          <Flex>
            <Section padding={false}>
              <FieldWrapper title={language.City.BASE_FARE}>
                <TextField icon={"$"} />
              </FieldWrapper>
            </Section>
            <Section padding={false}>
              <FieldWrapper title={language.City.FARE_PER_KM}>
                <TextField icon={"$"} />
              </FieldWrapper>
            </Section>
          </Flex>
          <Flex>
            <Section padding={false}>
              <FieldWrapper title={language.City.FARE_PER_TIME}>
                <TextField icon={"$"} />
              </FieldWrapper>
            </Section>
            <Section padding={false}>
              <FieldWrapper title={language.City.MINIMUM_METERS}>
                <TextField icon={language.City.METERS} />
              </FieldWrapper>
            </Section>
          </Flex>
          <Flex>
            <Section padding={false}>
              <FieldWrapper title={language.City.MINIMUM_FARE}>
                <TextField icon={"$"} />
              </FieldWrapper>
            </Section>
            <Section padding={false}>
              <FieldWrapper title={language.City.BOOKING_FEE}>
                <TextField icon={"$"} />
              </FieldWrapper>
            </Section>
          </Flex>
          <FieldWrapper title={language.City.CANCELLATION_FEE}>
            <ToggleButton />
          </FieldWrapper>
          <Flex>
            <Section padding={false}>
              <FieldWrapper title={language.City.CANCEL_AMOUNT}>
                <TextField icon={"$"} />
              </FieldWrapper>
            </Section>
            <Section padding={false}>
              <FieldWrapper title={language.City.CANCEL_MINUTES}>
                <TextField icon={language.City.MINUTES} />
              </FieldWrapper>
            </Section>
          </Flex>
        </Border>
      </Section>
    </FormWrapper>
  );
}
