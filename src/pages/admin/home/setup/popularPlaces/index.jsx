import React from "react";
import axios from "axios";

import Table from "../../../../../components/common/Table";
import { SmallLoader } from "../../../../../components/common/SmallLoader";

import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin, header, authFailure } = useAdmin();
  const [cities, setCities] = React.useState(null);
  const { morph } = useUtils();
  const fetchCities = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_CITY_LIST_ALL, {}, header);
      setCities(data.data);
    } catch (er) {
      authFailure(er);
    }
  };
  React.useEffect(() => {
    fetchCities();
  }, []);
  return cities === null ? (
    <SmallLoader />
  ) : (
    <Table
      title={language.POPULAR_PLACES}
      startingHeadings={[
        {
          id: 1,
          title: language.PLACE_NAME,
          key: "name",
          show: true,
        },
        {
          id: 2,
          title: language.DESCRIPTION,
          key: "desc",
          show: true,
        },
        {
          id: 3,
          title: language.CITY,
          key: "city",
          show: true,
        },
        // {
        //   id: 2,
        //   title: language.ADDRESS,
        //   key: "address",
        //   show: true,
        // },
        {
          id: 4,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_POPULAR_PLACES_LIST}
      assignData={(data) =>
        data.map((popular) => ({
          _id: popular._id,
          name: popular.data.placeName,
          desc: popular.data.placeDescription,
          city:
            popular.data.city && cities.find((each) => each._id === popular.data.city)
              ? cities.find((each) => each._id === popular.data.city).locationName
              : "",
          address: popular.data?.locationAddress?.fullAddress || "",
          status: popular.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      showAdd={
        admin.privileges.SETUP.POPULAR_PLACES ? (admin.privileges.SETUP.POPULAR_PLACES.ADD ? true : false) : false
      }
      showArchieve={false}
      showEdit={
        admin.privileges.SETUP.POPULAR_PLACES ? (admin.privileges.SETUP.POPULAR_PLACES.EDIT ? true : false) : false
      }
      addClick={() => history.push(NavLinks.ADMIN_SETUP_POPULAR_PLACES_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_SETUP_POPULAR_PLACES_EDIT + "/" + e)}
      showBulk={true}
      showCityFilter={true}
      showView={false}
      showSearch={true}
      viewClick={(e) => history.push(NavLinks.ADMIN_SETUP_CITY_COMMISION_VIEW + "/" + e)}
      showAction={
        admin.privileges.SETUP.POPULAR_PLACES ? (admin.privileges.SETUP.POPULAR_PLACES.EDIT ? true : false) : false
      }
      add={admin.privileges.SETUP.POPULAR_PLACES ? (admin.privileges.SETUP.POPULAR_PLACES.ADD ? true : false) : false}
      edit={admin.privileges.SETUP.POPULAR_PLACES ? (admin.privileges.SETUP.POPULAR_PLACES.EDIT ? true : false) : false}
      statusList={A.HOST + A.ADMIN_POPULAR_PLACES_STATUS_CHANGE}
    />
  );
}
