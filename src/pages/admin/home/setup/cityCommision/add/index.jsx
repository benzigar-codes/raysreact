import React from "react";
import { Loader } from "@googlemaps/js-api-loader";
import { useFormik } from "formik";
import { Helmet } from "react-helmet";
import axios from "axios";
import * as turf from "@turf/turf";
import * as yup from "yup";

import SericesJSON from "../../../../../../utils/services.json";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../../components/common/TextField";
import { DropdownNormal } from "../../../../../../components/common/DropDownNormal";
import GooglePlaceComplete from "../../../../../../components/common/GooglePlaceComplete";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import EmptyImage from "../../../../../../assets/images/empty.jpeg";

import useLanguage from "../../../../../../hooks/useLanguage";
import useSettings from "../../../../../../hooks/useSettings";

import NavLinks from "../../../../../../utils/navLinks.json";
import U from "../../../../../../utils/utils.js";
import A from "../../../../../../utils/API.js";
import Currency from "../../../../../../utils/currency.json";

import useAdmin from "../../../../../../hooks/useAdmin";
import { MultiSelect } from "../../../../../../components/common/MultiSelect";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";
import Flex from "../../../../../../components/common/Flex";
import { TimePicker } from "../../../../../../components/common/TimePicker";
import { DropDownSearch } from "../../../../../../components/common/DropDownSearch";
import { Border } from "../../../../../../components/common/Border";
import { Heading } from "../../../../../../components/common/Heading";
import gsap from "gsap/gsap-core";
import { PopUp } from "../../../../../../components/common/PopUp";
import useUtils from "../../../../../../hooks/useUtils";
import useDebug from "../../../../../../hooks/useDebug";

export default function Index({ history }) {
  // HOOKS
  const { language } = useLanguage();
  const { settings } = useSettings();
  const { header, authFailure } = useAdmin();
  const { parseError } = useUtils();

  // GOOGLE LOAD DETECTION
  const [googleLoaded, setGoogleLoaded] = React.useState(false);

  // PAGINATION
  const [page, setPage] = React.useState(1);

  // POLYGONS
  const [polygons, setPolygons] = React.useState([]);
  const [polygonsLoaded, setPolygonsLoader] = React.useState(false);
  const [address, setAddress] = React.useState(null);

  // IF CITIES INTERSECT
  const [pointsWithin, setPointsWithin] = React.useState(null);

  // VEHICLE CATEGORY FROM API
  const [vehicleCategory, setVehicleCategory] = React.useState([]);

  const currencyList = Currency;
  const [finalCoOrdinates, setFinalCoOrdinates] = React.useState(null);

  const Map = React.useRef();
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);

  // FORMIK 3 VALUES AND VALIDATION SCHEMA
  const [formik3values, setFormik3Values] = React.useState({});
  const [formik3Validation, setFormik3Validation] = React.useState({});

  const formik1 = useFormik({
    initialValues: {
      serviceCategory: "",
      city: "",
    },
    validationSchema: yup.object().shape({
      serviceCategory: yup
        .string()
        .min(4, language.MIN + " 4")
        .required(language.REQUIRED),
      city: yup
        .string()
        .min(4, language.MIN + " 4")
        .required(language.REQUIRED),
    }),
    onSubmit: (e) => {
      setPage(2);
    },
  });

  const formik2ValidationSchema = {
    peakFareAvailable: yup.number().required(language.REQUIRED),
    nightFareAvailable: yup.number().required(language.REQUIRED),
    peak_startTime: yup.string().required(language.REQUIRED),
    peak_endTime: yup.string().required(language.REQUIRED),
    night_startTime: yup.string().required(language.REQUIRED),
    night_endTime: yup.string().required(language.REQUIRED),
    currencyCode: yup.string().required(language.REQUIRED),
    currencySymbol: yup.string().required(language.REQUIRED),
    // serviceTaxPercentage: yup
    //   .number()
    //   .min(0, language.MIN + " 0")
    //   .max(100, language.MAX + " 100")
    //   .required(language.REQUIRED),
    distanceType: yup.string().required(language.REQUIRED),
    // siteCommission: yup
    //   .number()
    //   .min(0, language.MIN + " 0")
    //   .max(100, language.MAX + " 100")
    //   .required(language.REQUIRED),
  };

  const formik2 = useFormik({
    initialValues: {
      vehicleCategories: [],
      defaultCategory: "",
      peakFareAvailable: 0,
      nightFareAvailable: 0,
      peak_startTime: new Date(new Date().setHours(9, 0, 0)),
      peak_endTime: new Date(new Date().setHours(18, 0, 0)),
      night_startTime: new Date(new Date().setHours(9, 0, 0)),
      night_endTime: new Date(new Date().setHours(18, 0, 0)),
      currencyCode: "INR",
      currencySymbol: "",
      // serviceTaxPercentage: 0,
      distanceType: "",
      // siteCommission: 0,
    },
    validateOnChange: false,
    validationSchema:
      formik1.values.serviceCategory === "ride"
        ? yup.object().shape({
            vehicleCategories: yup.array().min(1, language.ERROR_SELECT_VEHICLE),
            defaultCategory: yup.string().required(language.REQUIRED),
            ...formik2ValidationSchema,
          })
        : yup.object().shape({ ...formik2ValidationSchema }),
    onSubmit: (e) => {
      setPage(3);
    },
  });

  const formik3 = useFormik({
    initialValues: formik3values,
    validationSchema: yup.object().shape(formik3Validation),
    enableReinitialize: true,
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        await axios.post(
          A.HOST + A.ADMIN_CITY_UPDATE,
          {
            data: {
              categoryName: formik1.values.serviceCategory.toLowerCase(),
              locationName: formik1.values.city,
              location: {
                type: "Polygon",
                coordinates: finalCoOrdinates,
              },
              isPeakFareAvailable: {
                status: formik2.values.peakFareAvailable === 1 ? true : false,
                startTime: formik2.values.peakFareAvailable === 1 ? formik2.values.peak_startTime : "",
                endTime: formik2.values.peakFareAvailable === 1 ? formik2.values.peak_endTime : "",
              },
              isNightFareAvailable: {
                status: formik2.values.nightFareAvailable === 1 ? true : false,
                startTime: formik2.values.nightFareAvailable === 1 ? formik2.values.night_startTime : "",
                endTime: formik2.values.nightFareAvailable === 1 ? formik2.values.night_endTime : "",
              },
              categoryData: {
                vehicles: Object.keys(formik3.values).map((category) => ({
                  categoryId: category,
                  baseFare: parseFloat(formik3.values[category].base_fare),
                  farePerDistance: parseFloat(formik3.values[category].fare_per_distance),
                  farePerMinute: parseFloat(formik3.values[category].fare_per_minute),
                  waitingChargePerMin: parseFloat(formik3.values[category].waitingChargePerMin),
                  surchargeFee: parseFloat(formik3.values[category].surchargeFee),
                  peakFare: parseFloat(formik3.values[category].peak_fare),
                  nightFare: parseFloat(formik3.values[category].night_fare),
                  isDefaultCategory: formik2.values.defaultCategory === category ? true : false,
                  forAdminAndMobile: formik3.values[category].forAdminAndMobile === 1 ? true : false,
                  minimumCharge: {
                    status: formik3.values[category].minimumCharge === 1 ? true : false,
                    amount: formik3.values[category].minimumCharge_amount,
                  },
                  cancellation: {
                    status: formik3.values[category].cancellation === 1 ? true : false,
                    amount: parseFloat(formik3.values[category].cancellation_amount),
                    thresholdMinute: parseFloat(formik3.values[category].cancellation_thresholdMinute),
                  },
                  professionalCancellation: {
                    status: formik3.values[category].professional_cancellation === 1 ? true : false,
                    amount: parseFloat(formik3.values[category].professional_cancellation_amount),
                    thresholdMinute: parseFloat(formik3.values[category].professional_cancellation_thresholdMinute),
                  },
                  siteCommission: parseFloat(formik3.values[category].siteCommission),
                  isSubCategoryAvailable: formik3.values[category].isSubCategoryAvailable === 1 ? true : false,
                  isForceAppliedToProfessional:
                    formik3.values[category].isCompulsaryForProfessionals === 1 ? true : false,
                  subCategoryIds: formik3.values[category].subCategoryIds,
                  serviceTaxPercentage: formik3.values[category].serviceTaxPercentage,
                  isPackageSupport: formik3.values[category].isPackageSupport === 1 ? true : false,
                  isScheduleSupport: formik3.values[category].isScheduleSupport === 1 ? true : false,
                })),
              },
              currencyCode: formik2.values.currencyCode,
              currencySymbol: formik2.values.currencySymbol,
              status: U.ACTIVE,
              // serviceTaxPercentage: parseFloat(formik2.values.serviceTaxPercentage),
              distanceType: formik2.values.distanceType,
            },
          },
          header
        );
        history.push(NavLinks.ADMIN_SETUP_CITY_COMMISION);
      } catch (err) {
        authFailure(err);
        page !== 1 && setPop({ title: parseError(err), type: "error" });
      }
      setBtnLoading(false);
    },
  });

  React.useEffect(() => {
    formik2.setFieldValue(
      "defaultCategory",
      formik2.values.vehicleCategories.length === 0 ? "" : formik2.values.vehicleCategories[0]
    );
    if (formik1.values.serviceCategory === "ride") {
      let obj = {};
      let validation = {};
      formik2.values.vehicleCategories.forEach((vehicle) => {
        obj[vehicle] = {
          base_fare:
            formik3.values[vehicle] && formik3.values[vehicle].base_fare ? formik3.values[vehicle].base_fare : 0,
          fare_per_distance:
            formik3.values[vehicle] && formik3.values[vehicle].fare_per_distance
              ? formik3.values[vehicle].fare_per_distance
              : 0,
          fare_per_minute:
            formik3.values[vehicle] && formik3.values[vehicle].fare_per_minute
              ? formik3.values[vehicle].fare_per_minute
              : 0,
          peak_fare:
            formik3.values[vehicle] && formik3.values[vehicle].peak_fare && formik3.values[vehicle].peak_fare >= 0
              ? formik3.values[vehicle].peak_fare
              : 1,
          night_fare:
            formik3.values[vehicle] && formik3.values[vehicle].night_fare && formik3.values[vehicle].night_fare >= 0
              ? formik3.values[vehicle].night_fare
              : 1,
          waitingChargePerMin:
            formik3.values[vehicle] &&
            formik3.values[vehicle].waitingChargePerMin &&
            formik3.values[vehicle].waitingChargePerMin >= 0
              ? formik3.values[vehicle].waitingChargePerMin
              : 0,
          surchargeFee:
            formik3.values[vehicle] && formik3.values[vehicle].surchargeFee && formik3.values[vehicle].surchargeFee >= 0
              ? formik3.values[vehicle].surchargeFee
              : 0,
          cancellation:
            formik3.values[vehicle] && formik3.values[vehicle].cancellation ? formik3.values[vehicle].cancellation : 0,
          minimumCharge:
            formik3.values[vehicle] && formik3.values[vehicle].minimumCharge
              ? formik3.values[vehicle].minimumCharge
              : 0,
          cancellation_amount:
            formik3.values[vehicle] &&
            formik3.values[vehicle].cancellation_amount &&
            formik3.values[vehicle].cancellation_amount >= 0
              ? formik3.values[vehicle].cancellation_amount
              : 0,
          minimumCharge_amount:
            formik3.values[vehicle] &&
            formik3.values[vehicle].minimumCharge_amount &&
            formik3.values[vehicle].minimumCharge_amount >= 0
              ? formik3.values[vehicle].minimumCharge_amount
              : 0,
          cancellation_thresholdMinute:
            formik3.values[vehicle] && formik3.values[vehicle].cancellation_thresholdMinute
              ? formik3.values[vehicle].cancellation_thresholdMinute
              : 0,
          professional_cancellation:
            formik3.values[vehicle] && formik3.values[vehicle].professional_cancellation
              ? formik3.values[vehicle].professional_cancellation
              : 0,
          professional_cancellation_amount:
            formik3.values[vehicle] &&
            formik3.values[vehicle].professional_cancellation_amount &&
            formik3.values[vehicle].professional_cancellation_amount >= 0
              ? formik3.values[vehicle].professional_cancellation_amount
              : 0,
          professional_cancellation_thresholdMinute:
            formik3.values[vehicle] && formik3.values[vehicle].professional_cancellation_thresholdMinute
              ? formik3.values[vehicle].professional_cancellation_thresholdMinute
              : 0,
          siteCommission:
            formik3.values[vehicle] && formik3.values[vehicle].siteCommission
              ? formik3.values[vehicle].siteCommission
              : 0,
          serviceTaxPercentage:
            formik3.values[vehicle] && formik3.values[vehicle].serviceTaxPercentage
              ? formik3.values[vehicle].serviceTaxPercentage
              : 0,
          forAdminAndMobile:
            formik3.values[vehicle] && formik3.values[vehicle].forAdminAndMobile
              ? formik3.values[vehicle].forAdminAndMobile
              : 0,
          isSubCategoryAvailable:
            formik3.values[vehicle] && formik3.values[vehicle].isSubCategoryAvailable
              ? formik3.values[vehicle].isSubCategoryAvailable
              : 0,
          isCompulsaryForProfessionals:
            formik3.values[vehicle] && formik3.values[vehicle].isCompulsaryForProfessionals
              ? formik3.values[vehicle].isCompulsaryForProfessionals
              : 0,
          isScheduleSupport:
            formik3.values[vehicle] && formik3.values[vehicle].isScheduleSupport
              ? formik3.values[vehicle].isScheduleSupport
              : 0,
          isPackageSupport:
            formik3.values[vehicle] && formik3.values[vehicle].isPackageSupport
              ? formik3.values[vehicle].isPackageSupport
              : 0,
          subCategoryIds:
            formik3.values[vehicle] && formik3.values[vehicle].subCategoryIds
              ? formik3.values[vehicle].subCategoryIds.filter((each) => Object.keys(formik3.values).includes(each))
              : [],
        };
        validation[vehicle] = yup.object().shape({
          base_fare: yup
            .number()
            .min(0, language.MIN + " 1")
            .required(language.REQUIRED),
          forAdminAndMobile: yup.string().required(language.REQUIRED),
          fare_per_distance: yup
            .number()
            .min(0, language.MIN + " 1")
            .required(language.REQUIRED),
          fare_per_minute: yup
            .number()
            .min(0, language.MIN + " 1")
            .required(language.REQUIRED),
          peak_fare: yup
            .number()
            .min(1, language.MIN + " 1")
            .required(language.REQUIRED),
          night_fare: yup
            .number()
            .min(1, language.MIN + " 1")
            .required(language.REQUIRED),
          cancellation: yup.string().required(language.REQUIRED),
          cancellation_amount: yup
            .number()
            .min(0, language.MIN + " 0")
            .required(language.REQUIRED),
          cancellation_thresholdMinute: yup
            .number()
            .min(0, language.MIN + " 0")
            .required(language.REQUIRED),
          professional_cancellation: yup.string().required(language.REQUIRED),
          professional_cancellation_amount: yup
            .number()
            .min(0, language.MIN + " 0")
            .required(language.REQUIRED),
          professional_cancellation_thresholdMinute: yup
            .number()
            .min(0, language.MIN + " 0")
            .required(language.REQUIRED),
          siteCommission: yup
            .number()
            .min(0, language.MIN + " 0")
            .max(100, language.MAX + " 100")
            .required(language.REQUIRED),
          serviceTaxPercentage: yup
            .number()
            .min(0, language.MIN + " 0")
            .max(100, language.MAX + " 100")
            .required(language.REQUIRED),
          minimumCharge: yup.string().required(language.REQUIRED),
          minimumCharge_amount: yup
            .number()
            .min(0, language.MIN + " 0")
            .required(language.REQUIRED),
        });
      });
      setFormik3Values(obj);
      setFormik3Validation(validation);
    } else {
      let obj = {
        base_fare: 0,
        fare_per_distance: 0,
        fare_per_minute: 0,
        peak_fare: 0,
        night_fare: 0,
        forAdminAndMobile: 0,
        cancellation: 0,
        cancellation_amount: 0,
        cancellation_thresholdMinute: 0,
        professional_cancellation: 0,
        professional_cancellation_amount: 0,
        professional_cancellation_thresholdMinute: 0,
        siteCommission: 0,
        serviceTaxPercentage: 0,
        subCategoryIds: [],
        isSubCategoryAvailable: 0,
        isCompulsaryForProfessionals: 0,
        isScheduleSupport: 0,
        isPackageSupport: 0,
      };
      setFormik3Values(obj);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    formik1.values.serviceCategory,
    formik2.values.vehicleCategories,
    formik2.values.nightFareAvailable,
    formik2.values.peakFareAvailable,
  ]);

  React.useEffect(() => {
    const fetchPolygons = async () => {
      if (formik1.values.serviceCategory === "") return setPolygons([]);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_CITY_POLYGON_LIST,
          {
            categoryName: formik1.values.serviceCategory,
          },
          header
        );
        setPolygons(data);
        setPolygonsLoader(true);
      } catch (err) {
        authFailure(err);
      }
    };

    const fetchVehicleCategories = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_VEHICLE_CATEGORY_LIST_ALL, {}, header);
        setVehicleCategory(data);
      } catch (err) {
        authFailure(err);
      }
    };

    fetchPolygons();
    fetchVehicleCategories();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formik1.values.serviceCategory]);

  React.useEffect(() => {
    Object.keys(formik3.values).map(
      (each) =>
        formik3.values[each].cancellation === 0 &&
        formik3.setFieldValue(`${each}.cancellation_amount`, 0) &&
        formik3.setFieldValue(`${each}.cancellation_thresholdMinute`, 0)
    );
    console.log(formik3.values);
  }, [formik3.values]);

  React.useEffect(() => {
    const loader = new Loader({
      apiKey: settings.mapApi.web,
      version: "weekly",
      libraries: ["places"],
    });
    loader
      .load()
      .then(() => {
        setGoogleLoaded(true);
        gsap.fromTo(".addCity", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
      })
      .catch((e) => {});
  }, []);

  const createMap = ({ target = Map.current, center = { lat: 12.9791551, lng: 80.2007085 }, zoom = 14 }) => {
    const map = new window.google.maps.Map(target, {
      center,
      zoom: zoom,
      zoomControl: true,
      mapTypeControl: false,
      scaleControl: true,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: true,
    });
    return map;
  };

  // MAP AND STUFFFFF
  React.useEffect(() => {
    if (
      polygonsLoaded === true &&
      address &&
      googleLoaded === true &&
      page === 1 &&
      formik1.values.serviceCategory !== ""
    ) {
      setPointsWithin(null);
      let center = address ? address.results[0].geometry.location : { lat: 12.9791551, lng: 80.2007085 };
      const map = createMap({ target: Map.current, center, zoom: 11 });
      let googlePolygons = [];
      let googleMarkers = [];

      // DRAW POLYGONS OF SERVICES
      if (polygons.length !== 0) {
        polygons.forEach((eachPolygon) => {
          const coordinates = eachPolygon.locationCoordinates[0];
          const coordinatesForPath = coordinates.map((each) => ({ lat: each[1], lng: each[0] }));
          googlePolygons.push(
            new window.google.maps.Polygon({
              paths: coordinatesForPath,
              map,
              strokeColor: "#262525",
              strokeOpacity: 0.8,
              strokeWeight: 2,
              fillColor: "#fc8621",
              fillOpacity: 0.8,
            })
          );
          const centerMass = turf.centerOfMass(turf.polygon(eachPolygon.locationCoordinates)).geometry.coordinates;
          googleMarkers.push(
            new window.google.maps.Marker({
              position: { lat: centerMass[1], lng: centerMass[0] },
              label: {
                text: eachPolygon.locationName,
                color: "white",
                fontSize: "22px",
                fontWeight: "bold",
              },
              // label: eachPolygon.locationName,
              icon: EmptyImage,
              map: map,
            })
          );
        });
      }

      // DRAW POLYGON OF SELECTED AREA
      if (address) {
        let corners = [];
        let cac = address.results[0].geometry.location;
        if (finalCoOrdinates === null || address) {
          let d = 20;

          for (let i = 1; i <= 16; i++) {
            let angle = 22.5 * i;
            let R = 15000;
            let latitude1 = cac.lat * (3.1415926535898 / 180);
            let longitude1 = cac.lng * (3.1415926535898 / 180);
            let brng = angle * (3.1415926535898 / 180);
            let latitude2 = Math.asin(
              Math.sin(latitude1) * Math.cos(d / R) + Math.cos(latitude1) * Math.sin(d / R) * Math.cos(brng)
            );

            let longitude2 =
              longitude1 +
              Math.atan2(
                Math.sin(brng) * Math.sin(d / R) * Math.cos(latitude1),
                Math.cos(d / R) - Math.sin(latitude1) * Math.sin(latitude2)
              );
            latitude2 = latitude2 * (180 / 3.1415926535898);
            longitude2 = longitude2 * (180 / 3.1415926535898);
            corners.push(latitude2, longitude2);
          }

          corners = [
            new window.google.maps.LatLng(corners[0], corners[1]),
            new window.google.maps.LatLng(corners[4], corners[5]),
            new window.google.maps.LatLng(corners[8], corners[9]),
            new window.google.maps.LatLng(corners[12], corners[13]),
            new window.google.maps.LatLng(corners[16], corners[17]),
            new window.google.maps.LatLng(corners[20], corners[21]),
            new window.google.maps.LatLng(corners[24], corners[25]),
            new window.google.maps.LatLng(corners[28], corners[29]),
            new window.google.maps.LatLng(corners[0], corners[1]),
          ];
        }

        let myPolygon = new window.google.maps.Polygon({
          paths: corners,
          map: map,
          draggable: true,
          editable: true,
          strokeColor: "#262525",
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: "#ba9c9c",
          fillOpacity: 0.7,
        });

        async function getPolygonCoords() {
          myPolygon.setOptions({ fillColor: "green" });
          let len = myPolygon.getPath().getLength();
          cac.htmlStr = [];
          cac.coordi = [];
          cac.coordi.push([
            parseFloat(myPolygon.getPath().getAt(0).toUrlValue(9).split(",")[1]),
            parseFloat(myPolygon.getPath().getAt(0).toUrlValue(9).split(",")[0]),
          ]);

          for (let i = 0; i < len; i++) {
            cac.htmlStr.push([
              parseFloat(myPolygon.getPath().getAt(i).toUrlValue(9).split(",")[1]),
              parseFloat(myPolygon.getPath().getAt(i).toUrlValue(9).split(",")[0]),
            ]);
          }
          cac.final_nodes = cac.htmlStr.concat(cac.coordi);

          // FINDING IF INSIDE

          let cornersArray = [cac.final_nodes];
          let inside = false;

          polygons.forEach((poly, index) => {
            var intersection = turf.intersect(turf.polygon(poly.locationCoordinates), turf.polygon(cornersArray));
            if (intersection !== null) {
              inside = true;
              googlePolygons[index].setOptions({ fillColor: "red" });
            } else {
              googlePolygons[index].setOptions({ fillColor: "#fc8621" });
            }
          });

          setPointsWithin(inside === true ? true : false);

          if (inside === false) {
            setFinalCoOrdinates(cornersArray);
            let lat = turf.centerOfMass(turf.polygon(cornersArray)).geometry.coordinates[1];
            let lng = turf.centerOfMass(turf.polygon(cornersArray)).geometry.coordinates[0];
            try {
              const { data } = await axios.get(
                `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${settings.mapApi.web}`
              );
              getCityFromAddress(data);
            } catch (err) {}
            // formik1.setFieldValue("city");
          }
          if (inside === true) myPolygon.setOptions({ fillColor: "red" });
        }

        getPolygonCoords();
        // window.google.maps.event.addListener(myPolygon, "drag", getPolygonCoords);
        window.google.maps.event.addListener(myPolygon, "mouseup", getPolygonCoords);
        window.google.maps.event.addListener(map, "zoom_changed", function () {
          var zoom = map.getZoom();
          if (zoom <= 8) {
            googleMarkers.forEach((each) => each.setMap(null));
          } else {
            googleMarkers.forEach((each) => each.setMap(map));
          }
        });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [googleLoaded, page, formik1.values.serviceCategory, polygons, polygonsLoaded, address]);

  const getCityFromAddress = (fromAddress) => {
    formik1.setFieldValue(
      "city",
      fromAddress.results[0].address_components.filter((each) => each.types[0] === "administrative_area_level_2")
        .length > 0
        ? fromAddress.results[0].address_components.filter((each) => each.types[0] === "administrative_area_level_2")[0]
            .long_name
        : fromAddress.results[0].address_components.filter((each) => each.types[0] === "administrative_area_level_1")
            .length > 0
        ? fromAddress.results[0].address_components.filter((each) => each.types[0] === "administrative_area_level_1")[0]
            .long_name
        : ""
    );
  };

  React.useEffect(() => {
    if (address) {
      getCityFromAddress(address);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [address]);

  React.useEffect(() => {
    formik2.setFieldValue("currencySymbol", currencyList[formik2.values.currencyCode]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formik2.values.currencyCode]);

  return googleLoaded === false ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      bread={[{ id: 1, title: language.CITY_COMMISIONS, path: NavLinks.ADMIN_SETUP_CITY_COMMISION }]}
      title={language.ADD}
      width={"3/4"}
      animate={"addCity"}
      totalPage={"3"}
      page={page}
      showNext={page === 1 || page === 2}
      showPrev={page === 3}
      submitBtn={page === 3}
      submit={formik3.handleSubmit}
      btnLoading={btnLoading}
      nextClick={
        page === 1 && pointsWithin === false ? formik1.handleSubmit : page === 2 ? formik2.handleSubmit : () => {}
      }
      prevClick={() => setPage(page - 1)}
    >
      <Helmet>
        <script
          src={`https://maps.googleapis.com/maps/api/js?key=${settings.mapApi.web}&libraries=places`}
          async
        ></script>
      </Helmet>
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      {page === 1 && (
        <>
          <Section>
            <FieldWrapper title={language.SERVICE_CATEGORY}>
              <DropdownNormal
                error={formik1.errors.serviceCategory}
                change={(e) => formik1.setFieldValue("serviceCategory", e)}
                fields={SericesJSON.map((each) => ({
                  id: each.id,
                  label: language[each.label],
                  value: each.label.toLowerCase(),
                }))}
                defaultValue={
                  SericesJSON.map((each) => ({
                    id: each.id,
                    label: language[each.label],
                    value: each.label.toLowerCase(),
                  })).filter((each) => each.value === formik1.values.serviceCategory).length > 0
                    ? SericesJSON.map((each) => ({
                        id: each.id,
                        label: language[each.label],
                        value: each.label.toLowerCase(),
                      })).filter((each) => each.value === formik1.values.serviceCategory)[0].label
                    : ""
                }
              />
            </FieldWrapper>
            {formik1.values.serviceCategory !== "" && (
              <>
                <FieldWrapper title={language.ADDRESS}>
                  <GooglePlaceComplete
                    change={(e) => {
                      setAddress(e);
                    }}
                    placeholder={language.ADDRESS}
                  />
                </FieldWrapper>
                {pointsWithin === true && (
                  <FieldWrapper>
                    <p className="text-sm text-red-500">{language.ADD_INSIDE}</p>
                  </FieldWrapper>
                )}
                {pointsWithin === false && (
                  <FieldWrapper title={language.CITY_NAME}>
                    <TextField
                      error={formik1.errors.city}
                      change={(e) => formik1.setFieldValue("city", e)}
                      value={formik1.values.city}
                    />
                  </FieldWrapper>
                )}
              </>
            )}
          </Section>
          <Section>
            {formik1.values.serviceCategory !== "" &&
              polygonsLoaded === true &&
              (address || finalCoOrdinates !== null) && (
                <FieldWrapper title={language.MAP}>
                  <div ref={Map} style={{ width: "100%", height: 400 }}></div>
                </FieldWrapper>
              )}
          </Section>
        </>
      )}
      {page === 2 && (
        <>
          <Section>
            {formik1.values.serviceCategory === "ride" && (
              <>
                <FieldWrapper title={language.VEHICLE_CATEGORY}>
                  <MultiSelect
                    error={formik2.errors.vehicleCategories}
                    change={(e) =>
                      formik2.setFieldValue(
                        "vehicleCategories",
                        e.map((eEach) => vehicleCategory.filter((each) => each.vehicleCategory === eEach)[0]._id)
                      )
                    }
                    defaultValue={formik2.values.vehicleCategories.map(
                      (each) => vehicleCategory.filter((vehicle) => vehicle._id === each)[0].vehicleCategory
                    )}
                    allFields={vehicleCategory.map((each) => each.vehicleCategory)}
                  ></MultiSelect>
                </FieldWrapper>
                <FieldWrapper title={language.DEFAULT_VEHICLE_CATEGORY}>
                  <DropdownNormal
                    error={formik2.errors.defaultCategory}
                    change={(e) => formik2.setFieldValue("defaultCategory", e)}
                    defaultValue={
                      vehicleCategory.filter((each) => each._id === formik2.values.defaultCategory).length > 0
                        ? vehicleCategory.filter((each) => each._id === formik2.values.defaultCategory)[0]
                            .vehicleCategory
                        : ""
                    }
                    fields={formik2.values.vehicleCategories.map((each, idx) => ({
                      id: idx,
                      label: vehicleCategory.filter((vehicle) => vehicle._id === each)[0].vehicleCategory,
                      value: each,
                    }))}
                  />
                </FieldWrapper>
              </>
            )}
            <Flex align={false}>
              <Section padding={false}>
                <FieldWrapper title={language.CURRENCY_CODE}>
                  <DropDownSearch
                    change={(e) => {
                      formik2.setFieldValue("currencyCode", e);
                    }}
                    defaultValue={formik2.values.currencyCode}
                    fields={Object.keys(currencyList).map((each, i) => ({
                      id: i,
                      label: each,
                      value: each,
                    }))}
                  />
                </FieldWrapper>
              </Section>
              <Section padding={false}>
                <FieldWrapper title={language.CURRENCY_SYMBOL}>
                  <TextField value={formik2.values.currencySymbol} readOnly={true} />
                </FieldWrapper>
              </Section>
            </Flex>
            <Flex align={false}>
              {/* <Section padding={false}>
                <FieldWrapper title={language.SITE_COMMISION}>
                  <TextField
                    type={"number"}
                    change={(e) => formik2.setFieldValue("siteCommission", e)}
                    value={formik2.values.siteCommission}
                    error={formik2.errors.siteCommission}
                    icon={"%"}
                  />
                </FieldWrapper>
              </Section> */}
              {/* <Section padding={false}>
                <FieldWrapper title={language.SERVICE_TAX_PERCENTAGE}>
                  <TextField
                    change={(e) => formik2.setFieldValue("serviceTaxPercentage", e)}
                    value={formik2.values.serviceTaxPercentage}
                    type={"number"}
                    error={formik2.errors.serviceTaxPercentage}
                    icon={"%"}
                  />
                </FieldWrapper>
              </Section> */}
            </Flex>
            <FieldWrapper title={language.DISTANCE_TYPE}>
              <DropdownNormal
                error={formik2.errors.distanceType}
                change={(e) => formik2.setFieldValue("distanceType", e)}
                defaultValue={language[formik2.values.distanceType]}
                fields={[
                  { id: 1, label: language.KM, value: "KM" },
                  { id: 2, label: language.MILES, value: "MILES" },
                ]}
              />
            </FieldWrapper>
          </Section>
          <Section>
            <Flex>
              <Section>
                <FieldWrapper title={language.PEAK_FARE}>
                  <ToggleButton
                    change={(e) => formik2.setFieldValue("peakFareAvailable", e)}
                    defaultValue={formik2.values.peakFareAvailable}
                  />
                </FieldWrapper>
              </Section>
              {formik2.values.peakFareAvailable === 1 && (
                <>
                  <Section>
                    <FieldWrapper title={language.START_TIME}>
                      <TimePicker
                        defaultValue={formik2.values.peak_startTime}
                        change={(e) => formik2.setFieldValue("peak_startTime", e)}
                      />
                    </FieldWrapper>
                  </Section>
                  <Section>
                    <FieldWrapper title={language.END_TIME}>
                      <TimePicker
                        defaultValue={formik2.values.peak_endTime}
                        change={(e) => formik2.setFieldValue("peak_endTime", e)}
                      />
                    </FieldWrapper>
                  </Section>
                </>
              )}
            </Flex>
            <Flex>
              <Section>
                <FieldWrapper title={language.NIGHT_FARE}>
                  <ToggleButton
                    change={(e) => formik2.setFieldValue("nightFareAvailable", e)}
                    defaultValue={formik2.values.nightFareAvailable}
                  />
                </FieldWrapper>
              </Section>
              {formik2.values.nightFareAvailable === 1 && (
                <>
                  <Section>
                    <FieldWrapper title={language.START_TIME}>
                      <TimePicker
                        defaultValue={formik2.values.night_startTime}
                        change={(e) => formik2.setFieldValue("night_startTime", e)}
                      />
                    </FieldWrapper>
                  </Section>
                  <Section>
                    <FieldWrapper title={language.END_TIME}>
                      <TimePicker
                        defaultValue={formik2.values.night_endTime}
                        change={(e) => formik2.setFieldValue("night_endTime", e)}
                      />
                    </FieldWrapper>
                  </Section>{" "}
                </>
              )}
            </Flex>
          </Section>
        </>
      )}
      {page === 3 && (
        <>
          <div className="flex flex-wrap w-full">
            {formik1.values.serviceCategory === "ride" &&
              formik2.values.vehicleCategories.map((category) => (
                <div className="w-full md:w-1/2 p-2">
                  <Border>
                    <div className="flex justify-between items-center">
                      <h1 className="mx-4 text-lg text-blue-800">
                        {vehicleCategory.filter((each) => each._id === category)[0].vehicleCategory}
                      </h1>
                      <div className="flex items-center mx-2">
                        <p className="mx-2 dark:text-gray-200 text-gray-500 text-sm">
                          {language.SUBCATEGORY_AVAILABLE}
                        </p>
                        <ToggleButton
                          change={(e) => formik3.setFieldValue(`${category}.isSubCategoryAvailable`, e)}
                          defaultValue={formik3.values[category].isSubCategoryAvailable}
                        />
                      </div>
                    </div>
                    {formik3.values[category]?.isSubCategoryAvailable === 1 && (
                      <div className="border-b-2 px-2 py-2 mb-2 w-full">
                        {/* <FieldWrapper title={language.INCLUDES}> */}
                        <div className="flex justify-between items-center mb-2">
                          <p className="mx-2 dark:text-gray-200 text-gray-500 text-sm">{language.INCLUDES}</p>
                          <div className="flex items-center">
                            <p className="mx-2 dark:text-gray-200 text-gray-500 text-sm">{language.COMPULSARY}</p>
                            <ToggleButton
                              change={(e) => formik3.setFieldValue(`${category}.isCompulsaryForProfessionals`, e)}
                              defaultValue={formik3.values[category].isCompulsaryForProfessionals}
                            />
                          </div>
                        </div>
                        <MultiSelect
                          error={formik3.errors[category] && formik3.errors[category].subCategoryIds}
                          change={(e) =>
                            formik3.setFieldValue(
                              `${category}.subCategoryIds`,
                              e.map((eEach) => vehicleCategory.filter((each) => each.vehicleCategory === eEach)[0]._id)
                            )
                          }
                          defaultValue={formik3.values[category].subCategoryIds.map(
                            (each) => vehicleCategory.filter((vehicle) => vehicle._id === each)[0].vehicleCategory
                          )}
                          allFields={formik2.values.vehicleCategories
                            .filter((each) => each !== category)
                            .map(
                              (each) => vehicleCategory.filter((vehicle) => vehicle._id === each)[0].vehicleCategory
                            )}
                        ></MultiSelect>
                        {/* </FieldWrapper> */}
                      </div>
                    )}
                    <Flex>
                      <Section padding={false}>
                        <FieldWrapper title={language.BASE_FARE}>
                          <TextField
                            value={formik3.values[category].base_fare}
                            change={(e) => formik3.setFieldValue(`${category}.base_fare`, e)}
                            error={formik3.errors[category] && formik3.errors[category].base_fare}
                            type={"number"}
                            icon={formik2.values.currencySymbol}
                          />
                        </FieldWrapper>
                      </Section>
                      <Section padding={false}>
                        <FieldWrapper
                          title={formik2.values.distanceType === "KM" ? language.FARE_PER_KM : language.FARE_PER_MILE}
                        >
                          <TextField
                            value={formik3.values[category].fare_per_distance}
                            change={(e) => formik3.setFieldValue(`${category}.fare_per_distance`, e)}
                            error={formik3.errors[category] && formik3.errors[category].fare_per_distance}
                            type={"number"}
                            icon={formik2.values.currencySymbol}
                          />
                        </FieldWrapper>
                      </Section>
                    </Flex>
                    <Flex>
                      <Section padding={false}>
                        <FieldWrapper title={language.FARE_PER_MINUTE}>
                          <TextField
                            value={formik3.values[category].fare_per_minute}
                            change={(e) => formik3.setFieldValue(`${category}.fare_per_minute`, e)}
                            error={formik3.errors[category] && formik3.errors[category].fare_per_minute}
                            type={"number"}
                            icon={formik2.values.currencySymbol}
                          />
                        </FieldWrapper>
                      </Section>
                      <Section padding={false}>
                        <FieldWrapper title={language.SITE_COMMISION}>
                          <TextField
                            type={"number"}
                            change={(e) => formik3.setFieldValue(`${category}.siteCommission`, e)}
                            value={formik3.values[category].siteCommission}
                            error={formik3.errors[category] && formik3.errors[category].siteCommission}
                            icon={"%"}
                          />
                        </FieldWrapper>
                      </Section>
                    </Flex>
                    <Flex>
                      <Section padding={false}>
                        <FieldWrapper title={language.WAITING_CHARGE_PER_MINUTE}>
                          <TextField
                            value={formik3.values[category].waitingChargePerMin}
                            change={(e) => formik3.setFieldValue(`${category}.waitingChargePerMin`, e)}
                            error={formik3.errors[category] && formik3.errors[category].waitingChargePerMin}
                            type={"number"}
                            icon={formik2.values.currencySymbol}
                          />
                        </FieldWrapper>
                      </Section>
                      <Section padding={false}>
                        <FieldWrapper title={language.SURCHARGE_FEE}>
                          <TextField
                            type={"number"}
                            change={(e) => formik3.setFieldValue(`${category}.surchargeFee`, e)}
                            value={formik3.values[category].surchargeFee}
                            error={formik3.errors[category] && formik3.errors[category].surchargeFee}
                            icon={formik2.values.currencySymbol}
                          />
                        </FieldWrapper>
                      </Section>
                    </Flex>
                    <Flex>
                      {formik2.values.peakFareAvailable === 1 && (
                        <Section padding={false}>
                          <FieldWrapper title={language.PEAK_FARE_AMOUNT}>
                            <TextField
                              value={formik3.values[category].peak_fare}
                              change={(e) => formik3.setFieldValue(`${category}.peak_fare`, e)}
                              error={formik3.errors[category] && formik3.errors[category].peak_fare}
                              type={"number"}
                              icon={"X"}
                            />
                          </FieldWrapper>
                        </Section>
                      )}
                      {formik2.values.nightFareAvailable === 1 && (
                        <Section padding={false}>
                          <FieldWrapper title={language.NIGHT_FARE_AMOUNT}>
                            <TextField
                              value={formik3.values[category].night_fare}
                              change={(e) => formik3.setFieldValue(`${category}.night_fare`, e)}
                              error={formik3.errors[category] && formik3.errors[category].night_fare}
                              type={"number"}
                              icon={"X"}
                            />
                          </FieldWrapper>
                        </Section>
                      )}
                    </Flex>

                    <div className="flex">
                      <Section padding={false}>
                        <FieldWrapper title={language.CANCELLATION_FEE}>
                          <ToggleButton
                            change={(e) => formik3.setFieldValue(`${category}.cancellation`, e)}
                            defaultValue={formik3.values[category].cancellation}
                          />
                        </FieldWrapper>
                      </Section>
                      <Section padding={false}>
                        <FieldWrapper title={language.SERVICE_TAX_PERCENTAGE}>
                          <TextField
                            change={(e) => formik3.setFieldValue(`${category}.serviceTaxPercentage`, e)}
                            value={formik3.values[category].serviceTaxPercentage}
                            type={"number"}
                            error={formik3.errors[category] && formik3.errors[category].serviceTaxPercentage}
                            icon={"%"}
                          />
                        </FieldWrapper>
                      </Section>
                    </div>

                    {formik3.values[category].cancellation === 1 && (
                      <Flex>
                        <Section padding={false}>
                          <FieldWrapper title={language.CANCEL_AMOUNT}>
                            <TextField
                              value={formik3.values[category].cancellation_amount}
                              change={(e) => formik3.setFieldValue(`${category}.cancellation_amount`, e)}
                              error={formik3.errors[category] && formik3.errors[category].cancellation_amount}
                              type={"number"}
                              icon={formik2.values.currencySymbol}
                            />
                          </FieldWrapper>
                        </Section>
                        <Section padding={false}>
                          <FieldWrapper title={language.CANCEL_MINUTES}>
                            <TextField
                              value={formik3.values[category].cancellation_thresholdMinute}
                              change={(e) => formik3.setFieldValue(`${category}.cancellation_thresholdMinute`, e)}
                              error={formik3.errors[category] && formik3.errors[category].cancellation_thresholdMinute}
                              type={"number"}
                              icon={"min"}
                            />
                          </FieldWrapper>
                        </Section>
                      </Flex>
                    )}
                    <div className="flex">
                      <Section padding={false}>
                        <FieldWrapper title={language.MINIMUM_CHARGE}>
                          <ToggleButton
                            change={(e) => formik3.setFieldValue(`${category}.minimumCharge`, e)}
                            defaultValue={formik3.values[category].minimumCharge}
                          />
                        </FieldWrapper>
                      </Section>
                    </div>
                    {formik3.values[category].minimumCharge === 1 && (
                      <Flex>
                        <Section padding={false}>
                          <FieldWrapper title={language.MINIMUM_CHARGE_AMOUNT}>
                            <TextField
                              value={formik3.values[category].minimumCharge_amount}
                              change={(e) => formik3.setFieldValue(`${category}.minimumCharge_amount`, e)}
                              error={formik3.errors[category] && formik3.errors[category].minimumCharge_amount}
                              type={"number"}
                              icon={formik2.values.currencySymbol}
                            />
                          </FieldWrapper>
                        </Section>
                        <Section></Section>
                      </Flex>
                    )}
                    <div className="flex">
                      <Section padding={false}>
                        <FieldWrapper title={language.PROFESSIONAL_CANCELLATION}>
                          <ToggleButton
                            change={(e) => formik3.setFieldValue(`${category}.professional_cancellation`, e)}
                            defaultValue={formik3.values[category].professional_cancellation}
                          />
                        </FieldWrapper>
                      </Section>
                    </div>
                    {formik3.values[category].professional_cancellation === 1 && (
                      <Flex>
                        <Section padding={false}>
                          <FieldWrapper title={language.CANCEL_AMOUNT}>
                            <TextField
                              value={formik3.values[category].professional_cancellation_amount}
                              change={(e) => formik3.setFieldValue(`${category}.professional_cancellation_amount`, e)}
                              error={
                                formik3.errors[category] && formik3.errors[category].professional_cancellation_amount
                              }
                              type={"number"}
                              icon={formik2.values.currencySymbol}
                            />
                          </FieldWrapper>
                        </Section>
                        <Section padding={false}>
                          <FieldWrapper title={language.CANCEL_MINUTES}>
                            <TextField
                              value={formik3.values[category].professional_cancellation_thresholdMinute}
                              change={(e) =>
                                formik3.setFieldValue(`${category}.professional_cancellation_thresholdMinute`, e)
                              }
                              error={
                                formik3.errors[category] &&
                                formik3.errors[category].professional_cancellation_thresholdMinute
                              }
                              type={"number"}
                              icon={"min"}
                            />
                          </FieldWrapper>
                        </Section>
                      </Flex>
                    )}
                    <div className="flex">
                      <Section padding={false}>
                        <FieldWrapper title={language.DISPLAY_IN_APP}>
                          <ToggleButton
                            change={(e) => formik3.setFieldValue(`${category}.forAdminAndMobile`, e)}
                            defaultValue={formik3.values[category].forAdminAndMobile}
                          />
                        </FieldWrapper>
                      </Section>
                      <Section padding={false}>
                        <FieldWrapper title={language.SCHEDULE_SUPPORT}>
                          <ToggleButton
                            change={(e) => formik3.setFieldValue(`${category}.isScheduleSupport`, e)}
                            defaultValue={formik3.values[category].isScheduleSupport}
                          />
                        </FieldWrapper>
                      </Section>
                      <Section padding={false}>
                        <FieldWrapper title={language.PACKAGE_SUPPORT}>
                          <ToggleButton
                            change={(e) => formik3.setFieldValue(`${category}.isPackageSupport`, e)}
                            defaultValue={formik3.values[category].isPackageSupport}
                          />
                        </FieldWrapper>
                      </Section>
                    </div>
                  </Border>
                </div>
              ))}
          </div>
        </>
      )}
    </FormWrapper>
  );
}
