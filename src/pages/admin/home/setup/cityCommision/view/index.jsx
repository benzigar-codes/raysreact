import React from "react";
import axios from "axios";
import { useParams } from "react-router";
import { format } from "date-fns";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import JSONViewer from "../../../../../../components/common/JSONViewer";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../../components/common/TextField";

import useAdmin from "../../../../../../hooks/useAdmin";
import useLanguage from "../../../../../../hooks/useLanguage";

import A from "../../../../../../utils/API.js";
import NavLinks from "../../../../../../utils/navLinks.json";
import { DetailsWrapper } from "../../../../../../components/common/DetailsWrapper";
import { Detail } from "../../../../../../components/common/Detail";
import { Border } from "../../../../../../components/common/Border";
import { Heading } from "../../../../../../components/common/Heading";
import { Loader } from "@googlemaps/js-api-loader";
import useSettings from "../../../../../../hooks/useSettings";
import useUtils from "../../../../../../hooks/useUtils";
import { Link } from "react-router-dom";
import { Button } from "../../../../../../components/common/Button";

export default function Index({ history }) {
  const { id } = useParams();
  const { admin, header, authFailure } = useAdmin();
  const { language } = useLanguage();
  const { settings } = useSettings();
  const { morph } = useUtils();

  const [city, setCity] = React.useState(null);
  const mapRef = React.useRef(null);

  const [googleLoaded, setGoogleLoaded] = React.useState(false);

  const [vehicleCategories, setVehicleCategories] = React.useState(null);
  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    const fetchEverything = async () => {
      const fetchCity = async () => {
        try {
          const { data } = await axios.post(A.HOST + A.ADMIN_CITY_READ, { id }, header);
          setCity(data);
          document.title = data.locationName;
        } catch (err) {
          authFailure(err);
        }
      };
      const fetchVehicleCategories = async () => {
        try {
          const { data } = await axios.post(A.HOST + A.ADMIN_VEHICLE_CATEGORY_LIST_ALL, {}, header);
          setVehicleCategories(data);
        } catch (err) {
          authFailure(err);
        }
      };
      await fetchCity();
      await fetchVehicleCategories();
      setLoading(false);
    };
    googleLoaded === true && fetchEverything();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [googleLoaded]);

  React.useEffect(() => {
    if (loading === false) {
      const googleMap = new window.google.maps.Map(mapRef.current, {
        center: { lat: 12.9975729, lng: 80.2638949 },
        zoom: 14,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: true,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: true,
      });
      const coordinates = city.location.coordinates;
      const coordinatesForPath = coordinates[0].map((each) => ({ lat: each[1], lng: each[0] }));
      new window.google.maps.Polygon({
        paths: coordinatesForPath,
        map: googleMap,
        strokeColor: "#262525",
        strokeOpacity: 0.8,
        strokeWeight: 5,
        fillColor: "#fc8621",
        fillOpacity: 0.5,
      });
      var bounds = new window.google.maps.LatLngBounds();
      coordinatesForPath.forEach((poly) => bounds.extend(poly));
      googleMap.fitBounds(bounds);
    }
  }, [loading]);

  React.useEffect(() => {
    const loader = new Loader({
      apiKey: settings.mapApi.web,
      version: "weekly",
      libraries: ["places"],
    });
    loader
      .load()
      .then(() => {
        setGoogleLoaded(true);
      })
      .catch((e) => {});
  });

  return googleLoaded === false || loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      title={morph(city.locationName)}
      bread={[
        {
          id: 1,
          title: language.CITY_COMMISIONS,
          path: NavLinks.ADMIN_SETUP_CITY_COMMISION,
        },
      ]}
      width={"3/4"}
      editBtnClick={() => history.push(NavLinks.ADMIN_SETUP_CITY_COMMISION_EDIT + "/" + id)}
      editBtn={true}
    >
      <Section width={"1/2"}>
        <Border>
          <FieldWrapper>
            <DetailsWrapper>
              <Detail title={language.CITY_NAME} value={morph(city.locationName)} />
              <Detail title={language.CURRENCY_CODE} value={city.currencyCode} />
              <Detail title={language.CURRENCY_SYMBOL} value={city.currencySymbol} />
              <Detail title={language.DISTANCE_TYPE} value={city.distanceType} />
              <Detail
                title={language.PEAK_FARE}
                value={
                  city.isPeakFareAvailable.status
                    ? `${format(new Date(city.isPeakFareAvailable.startTime), "hh : mm aa")} - ${format(
                        new Date(city.isPeakFareAvailable.endTime),
                        "hh : mm aa"
                      )}`
                    : language.NOT_AVAILABLE
                }
              />
              <Detail
                title={language.NIGHT_FARE}
                value={
                  city.isNightFareAvailable.status
                    ? `${format(new Date(city.isNightFareAvailable.startTime), "hh : mm aa")} - ${format(
                        new Date(city.isNightFareAvailable.endTime),
                        "hh : mm aa"
                      )}`
                    : language.NOT_AVAILABLE
                }
              />
              <Detail title={language.DISTANCE_TYPE} value={language[city.distanceType]} />
              <Detail
                title={language.VEHICLE_CATEGORYS}
                value={city.categoryData.vehicles
                  .map((vehicle) => vehicleCategories.find((each) => each._id === vehicle.categoryId).vehicleCategory)
                  .join(", ")}
              />
              <Detail
                title={language.DEFAULT_VEHICLE_CATEGORY}
                value={
                  vehicleCategories.find(
                    (each) =>
                      city.categoryData.vehicles.find((vehicle) => vehicle.isDefaultCategory === true).categoryId ===
                      each._id
                  ).vehicleCategory
                }
              />
            </DetailsWrapper>
          </FieldWrapper>
        </Border>
        <div
          style={{
            height: 400,
          }}
          ref={mapRef}
        ></div>

        {admin.userType === "DEVELOPER" && <JSONViewer data={city} />}
      </Section>
      <Section width={"1/2"}>
        {city.categoryData.vehicles.map((each) => (
          <Border>
            <div className="flex justify-between items-center">
              <Heading title={vehicleCategories.find((category) => category._id === each.categoryId).vehicleCategory} />
              <div className="flex">
                <div className="w-full flex justify-end">
                  <Link to={NavLinks.ADMIN_SETUP_CITY_COMMISION_FARE + "/" + id + "/" + each.categoryId}>
                    <Button title={language.VIEW_ADVANCED_INFO} />
                  </Link>
                </div>
              </div>
            </div>
            <FieldWrapper>
              <DetailsWrapper>
                <Detail title={language.BASE_FARE} value={each.baseFare + " " + city.currencySymbol} />
                {each.siteCommission !== undefined && (
                  <Detail title={language.SITE_COMMISION} value={each.siteCommission + "%"} />
                )}
                {each.isSubCategoryAvailable !== undefined && each.isSubCategoryAvailable === true && (
                  <Detail
                    title={language.SUB_CATEGORY}
                    value={each.subCategoryIds
                      .map((cate) => vehicleCategories.find((category) => category._id === cate).vehicleCategory)
                      .join(", ")}
                  />
                )}
                <Detail title={language.FARE_PER_KM} value={each.farePerDistance + " " + city.currencySymbol} />
                <Detail title={language.FARE_PER_MINUTE} value={each.farePerMinute + " " + city.currencySymbol} />
                {city.isPeakFareAvailable.status && (
                  <Detail title={language.PEAK_FARE} value={each.peakFare + " " + city.currencySymbol} />
                )}
                {city.isNightFareAvailable.status && (
                  <Detail title={language.NIGHT_FARE} value={each.nightFare + " " + city.currencySymbol} />
                )}
                <Detail
                  title={language.CANCELLATION_FEE}
                  value={
                    each.cancellation.status
                      ? `${each.cancellation.amount} ${city.currencySymbol} / ${each.cancellation.thresholdMinute} ${language.MINUTES}`
                      : language.NOT_AVAILABLE
                  }
                />
                {each.serviceTaxPercentage ? (
                  <Detail title={language.SERVICE_TAX_PERCENTAGE} value={each.serviceTaxPercentage + " %"} />
                ) : null}
                {each.isScheduleSupport !== undefined ? (
                  <Detail
                    title={language.SCHEDULE_SUPPORT}
                    value={each.isScheduleSupport ? language.YES : language.NO}
                  />
                ) : null}
                {each.isPackageSupport !== undefined ? (
                  <Detail title={language.PACKAGE_SUPPORT} value={each.isPackageSupport ? language.YES : language.NO} />
                ) : null}
              </DetailsWrapper>
            </FieldWrapper>
          </Border>
        ))}
      </Section>
    </FormWrapper>
  );
}
