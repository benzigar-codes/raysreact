import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <Table
      title={language.CITY_COMMISSION}
      startingHeadings={[
        {
          id: 1,
          title: language.CITY_NAME,
          key: "location",
          show: true,
        },
        {
          id: 1,
          title: language.CATEGORY,
          key: "category",
          show: true,
        },
        {
          id: 2,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_CITY_TABLE_LIST}
      assignData={(data) =>
        data.map((city) => ({
          _id: city._id,
          location: morph(city.locationName),
          category: language[city.categoryName.toUpperCase()],
          status: city.status === U.ACTIVE ? 1 : 0,
        }))
      }
      showAdd={
        admin.privileges.SETUP.CITY_COMMISIONS ? (admin.privileges.SETUP.CITY_COMMISIONS.ADD ? true : false) : false
      }
      showArchieve={false}
      showEdit={
        admin.privileges.SETUP.CITY_COMMISIONS ? (admin.privileges.SETUP.CITY_COMMISIONS.EDIT ? true : false) : false
      }
      addClick={() => history.push(NavLinks.ADMIN_SETUP_CITY_COMMISION_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_SETUP_CITY_COMMISION_EDIT + "/" + e)}
      showBulk={true}
      showView={true}
      showSearch={true}
      viewClick={(e) => history.push(NavLinks.ADMIN_SETUP_CITY_COMMISION_VIEW + "/" + e)}
      showAction={
        admin.privileges.SETUP.CITY_COMMISIONS ? (admin.privileges.SETUP.CITY_COMMISIONS.EDIT ? true : false) : false
      }
      add={admin.privileges.SETUP.CITY_COMMISIONS ? (admin.privileges.SETUP.CITY_COMMISIONS.ADD ? true : false) : false}
      edit={
        admin.privileges.SETUP.CITY_COMMISIONS ? (admin.privileges.SETUP.CITY_COMMISIONS.EDIT ? true : false) : false
      }
      statusList={A.HOST + A.ADMIN_CITY_STATUS}
    />
  );
}
