import React from "react";
import axios from "axios";
import gsap from "gsap";
import { useParams } from "react-router-dom";
import { useFormik } from "formik";
import * as yup from "yup";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../../components/common/TextField";
import { Border } from "../../../../../../components/common/Border";
import { Button } from "../../../../../../components/common/Button";
import { FileUpload } from "../../../../../../components/common/FileUpload";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";

import useLanguage from "../../../../../../hooks/useLanguage";
import useAdmin from "../../../../../../hooks/useAdmin";

import navLinks from "../../../../../../utils/navLinks.json";
import A from "../../../../../../utils/API";

import { Heading } from "../../../../../../components/common/Heading";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import useImage from "../../../../../../hooks/useImage";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { cityId, categoryId } = useParams();
  const { admin, header, authFailure } = useAdmin();
  const { imageUrl, compressImage } = useImage();
  const [loading, setLoading] = React.useState(true);

  const [btnLoading, setBtnLoading] = React.useState(false);

  const formik = useFormik({
    initialValues: {
      title: "",
      description: "",
      fareMessage: "",
      amenities: [
        {
          title: "",
          image: "",
        },
      ],
      fleets: [
        {
          title: "",
          image: "",
        },
      ],
      guidelines: [
        {
          title: "",
          points: [""],
        },
      ],
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      title: yup.string(),
      description: yup.string(),
      fareMessage: yup.string(),
      amenities: yup.array().of(
        yup.object().shape({
          title: yup.string(),
          image: yup.string(),
        })
      ),
      fleets: yup.array().of(
        yup.object().shape({
          title: yup.string(),
          image: yup.string(),
        })
      ),
      guidelines: yup.array().of(
        yup.object().shape({
          title: yup.string(),
          points: yup.array().of(yup.string()),
        })
      ),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        const sendData = {
          cityId: cityId,
          id: categoryId,
          title: formik.values.title,
          fareMessage: formik.values.fareMessage,
          description: formik.values.description,
          amenities: formik.values.amenities,
          fleets: formik.values.fleets,
          guidelines: formik.values.guidelines,
        };
        const { data } = await axios.post(A.HOST + A.ADMIN_SETUP_CITY_MORE_INFO_UPDATE, sendData, header);
        history.goBack();
      } catch (err) {}
      setBtnLoading(false);
    },
  });

  const [cityDetails, setCityDetails] = React.useState(null);

  const uploadImage = async (image, formikString) => {
    try {
      formik.setFieldValue(formikString, "");

      const compressedImage = await compressImage(image);

      const formData = new FormData();
      formData.append("files", compressedImage);
      formData.append("filesName", `${cityId}.${categoryId}.${formikString}`);
      const { data } = await axios.post(A.HOST + A.ADMIN_OTHERS_UPLOAD_IMAGE, formData, header);

      formik.setFieldValue(formikString, data);
    } catch (err) {
      authFailure(err);
      alert(err);
    }
  };

  const fetchCityDetails = async () => {
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_CITY_READ,
        {
          id: cityId,
        },
        header
      );
      setCityDetails(data);
    } catch (err) {}
  };
  React.useEffect(() => {
    fetchCityDetails();
    fetchCategory();
  }, []);

  const fetchCategory = async () => {
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_SETUP_CITY_MORE_INFO_READ,
        {
          cityId: cityId,
          id: categoryId,
        },
        header
      );
      if (data) {
        formik.setFieldValue("title", data.title ? data.title : "");
        formik.setFieldValue("description", data.description ? data.description : "");
        formik.setFieldValue("fareMessage", data.fareMessage ? data.fareMessage : "");
        formik.setFieldValue("amenities", data.amenities ? data.amenities : formik.values.amenities);
        formik.setFieldValue("fleets", data.fleets ? data.fleets : formik.values.fleets);
        formik.setFieldValue("guidelines", data.guidelines ? data.guidelines : formik.values.guidelines);
      }
      setLoading(false);
    } catch (err) {
      authFailure(err);
      alert(err);
    }
  };

  // Animation
  React.useEffect(() => {
    if (cityDetails && loading === false) gsap.fromTo(".advanced", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, [cityDetails, loading]);

  return cityDetails && loading === false ? (
    <FormWrapper
      bread={[
        {
          id: 1,
          title: language.CITY_COMMISSION,
          path: navLinks.ADMIN_SETUP_CITY_COMMISION,
        },
        {
          id: 2,
          title: cityDetails && cityDetails.locationName,
          path: navLinks.ADMIN_SETUP_CITY_COMMISION_VIEW + "/" + cityId,
        },
      ]}
      animate="advanced"
      title={language.ADVANCED_INFO}
      submit={formik.handleSubmit}
      width="4/4"
      btnLoading={btnLoading}
      submitBtn={true}
    >
      <Section>
        <Border>
          <Heading title={language.BASIC_DETAILS} />
          <FieldWrapper title={language.TITLE}>
            <TextField
              placeholder={language.TITLE}
              error={formik.errors.title}
              change={(e) => formik.setFieldValue("title", e)}
              value={formik.values.title}
            />
          </FieldWrapper>
          <FieldWrapper title={language.DESCRIPTION}>
            <TextField
              placeholder={language.DESCRIPTION}
              error={formik.errors.description}
              change={(e) => formik.setFieldValue("description", e)}
              value={formik.values.description}
            />
          </FieldWrapper>
          <FieldWrapper title={language.FARE_MESSAGE}>
            <TextField
              placeholder={language.MESSAGE}
              error={formik.errors.fareMessage}
              change={(e) => formik.setFieldValue("fareMessage", e)}
              value={formik.values.fareMessage}
            />
          </FieldWrapper>
        </Border>
        {formik.values.guidelines.map((each, idx) => (
          <Border>
            <Heading title={language.GUIDELINES + " : " + (parseInt(idx) + 1)} />
            <FieldWrapper title={language.TITLE}>
              <TextField
                placeholder={language.TITLE}
                error={formik.errors.guidelines && formik.errors.guidelines[idx] && formik.errors.guidelines[idx].title}
                change={(e) => formik.setFieldValue(`guidelines[${idx}].title`, e)}
                value={formik.values.guidelines[idx].title}
              />
            </FieldWrapper>
            <FieldWrapper title={language.POINTS}>
              <div className="flex flex-col w-full">
                {formik.values.guidelines[idx].points.map((each, pointIdx) => (
                  <div className="w-full flex mb-4">
                    <TextField
                      placeholder={language.POINTS}
                      error={
                        formik.errors.guidelines &&
                        formik.errors.guidelines[idx] &&
                        formik.errors.guidelines[idx].points &&
                        formik.errors.guidelines[idx].points[pointIdx]
                      }
                      change={(e) => formik.setFieldValue(`guidelines[${idx}].points[${pointIdx}]`, e)}
                      value={formik.values.guidelines[idx].points[pointIdx]}
                    />
                  </div>
                ))}
                <div className="flex justify-end">
                  <Button
                    click={() =>
                      formik.setFieldValue(`guidelines[${idx}].points`, [...formik.values.guidelines[idx].points, ""])
                    }
                    title={language.ADD}
                  />
                  {formik.values.guidelines[idx].points.length !== 1 && (
                    <Button
                      click={() =>
                        formik.setFieldValue(
                          `guidelines[${idx}].points`,
                          formik.values.guidelines[idx].points.filter(
                            (each, insideIdx) => insideIdx !== formik.values.guidelines[idx].points.length - 1
                          )
                        )
                      }
                      title={language.REMOVE}
                    />
                  )}
                </div>
              </div>
            </FieldWrapper>
          </Border>
        ))}
        <div className="flex">
          <Button
            click={() =>
              formik.setFieldValue("guidelines", [
                ...formik.values.guidelines,
                {
                  title: "",
                  points: [""],
                },
              ])
            }
            title={language.ADD}
          />
          {formik.values.guidelines.length !== 1 && (
            <Button
              click={() =>
                formik.setFieldValue(
                  "guidelines",
                  formik.values.guidelines.filter((each, idx) => idx !== formik.values.guidelines.length - 1)
                )
              }
              title={language.REMOVE}
            />
          )}
        </div>
      </Section>
      <Section>
        {formik.values.amenities.map((each, idx) => (
          <Border>
            <Heading title={language.AMENITIES + " : " + (parseInt(idx) + 1)} />
            <FieldWrapper title={language.TITLE}>
              <TextField
                placeholder={language.TITLE}
                error={formik.errors.amenities && formik.errors.amenities[idx] && formik.errors.amenities[idx].title}
                change={(e) => formik.setFieldValue(`amenities[${idx}].title`, e)}
                value={each.title}
              />
            </FieldWrapper>
            <FieldWrapper>
              <FileUpload
                change={(e) => uploadImage(e, `amenities[${idx}].image`)}
                crop={false}
                {...(formik.values.amenities[idx].image !== ""
                  ? {
                      defaultValue: imageUrl(formik.values.amenities[idx].image),
                    }
                  : {})}
                // accept={["image/jpeg", "image/jpg", "image/png"]}
              />
            </FieldWrapper>
          </Border>
        ))}
        <div className="flex">
          <Button
            click={() =>
              formik.setFieldValue("amenities", [
                ...formik.values.amenities,
                {
                  title: "",
                  image: "",
                },
              ])
            }
            title={language.ADD}
          />
          {formik.values.amenities.length !== 1 && (
            <Button
              click={() =>
                formik.setFieldValue(
                  "amenities",
                  formik.values.amenities.filter((each, idx) => idx !== formik.values.amenities.length - 1)
                )
              }
              title={language.REMOVE}
            />
          )}
        </div>
      </Section>
      <Section>
        {formik.values.fleets.map((each, idx) => (
          <Border>
            <Heading title={language.FLEETS + " : " + (parseInt(idx) + 1)} />
            <FieldWrapper title={language.TITLE}>
              <TextField
                placeholder={language.TITLE}
                error={formik.errors.fleets && formik.errors.fleets[idx] && formik.errors.fleets[idx].title}
                change={(e) => formik.setFieldValue(`fleets[${idx}].title`, e)}
                value={each.title}
              />
            </FieldWrapper>
            <FieldWrapper>
              <FileUpload
                change={(e) => uploadImage(e, `fleets[${idx}].image`)}
                crop={false}
                {...(formik.values.fleets[idx].image !== ""
                  ? {
                      defaultValue: imageUrl(formik.values.fleets[idx].image),
                    }
                  : {})}
              />
            </FieldWrapper>
          </Border>
        ))}
        <div className="flex">
          <Button
            click={() =>
              formik.setFieldValue("fleets", [
                ...formik.values.fleets,
                {
                  title: "",
                  image: "",
                },
              ])
            }
            title={language.ADD}
          />
          {formik.values.fleets.length !== 1 && (
            <Button
              click={() =>
                formik.setFieldValue(
                  "fleets",
                  formik.values.fleets.filter((each, idx) => idx !== formik.values.fleets.length - 1)
                )
              }
              title={language.REMOVE}
            />
          )}
        </div>
      </Section>
    </FormWrapper>
  ) : (
    <SmallLoader />
  );
}
