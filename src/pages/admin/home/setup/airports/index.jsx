import React from "react";
import axios from "axios";

import Table from "../../../../../components/common/Table";
import { SmallLoader } from "../../../../../components/common/SmallLoader";

import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin, header, authFailure } = useAdmin();
  const [cities, setCities] = React.useState(null);
  const { morph } = useUtils();
  const fetchCities = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_CITY_LIST_ALL, {}, header);
      setCities(data.data);
    } catch (er) {
      authFailure(er);
    }
  };
  React.useEffect(() => {
    fetchCities();
  }, []);
  return cities === null ? (
    <SmallLoader />
  ) : (
    <Table
      title={language.AIRPORTS}
      startingHeadings={[
        {
          id: 1,
          title: language.SHORT_NAME,
          key: "airportShortName",
          show: true,
        },
        {
          id: 2,
          title: language.LONG_NAME,
          key: "airportLongName",
          show: true,
        },
        {
          id: 2,
          title: language.DESCRIPTION,
          key: "airportDescription",
          show: true,
        },
        {
          id: 3,
          title: language.CITY,
          key: "city",
          show: true,
        },
        {
          id: 4,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_AIRPORTS_LIST}
      assignData={(data) =>
        data.map((airport) => ({
          _id: airport._id,
          airportLongName: airport.data.airportLongName,
          airportShortName: airport.data.airportShortName,
          airportDescription: airport.data.airportDescription,
          city:
            airport.data.serviceArea && cities.find((each) => each._id === airport.data.serviceArea)
              ? cities.find((each) => each._id === airport.data.serviceArea).locationName
              : "",
          status: airport.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      showAdd={admin.privileges.SETUP.AIRPORTS ? (admin.privileges.SETUP.AIRPORTS.ADD ? true : false) : false}
      showArchieve={false}
      showEdit={admin.privileges.SETUP.AIRPORTS ? (admin.privileges.SETUP.AIRPORTS.EDIT ? true : false) : false}
      addClick={() => history.push(NavLinks.ADMIN_SETUP_AIRPORTS_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_SETUP_AIRPORTS_EDIT + "/" + e)}
      showBulk={false}
      showCityFilter={true}
      showView={false}
      showStops={true}
      vehicleClick={(e) => history.push(NavLinks.ADMIN_SETUP_AIRPORTS + e + NavLinks.ADMIN_SETUP_AIRPORTS_STOPS_LISTS)}
      showSearch={true}
      showAction={admin.privileges.SETUP.AIRPORTS ? (admin.privileges.SETUP.AIRPORTS.EDIT ? true : false) : false}
      add={admin.privileges.SETUP.AIRPORTS ? (admin.privileges.SETUP.AIRPORTS.ADD ? true : false) : false}
      edit={admin.privileges.SETUP.AIRPORTS ? (admin.privileges.SETUP.AIRPORTS.EDIT ? true : false) : false}
      statusList={A.HOST + A.ADMIN_AIRPORTS_CHANGE_STATUS}
    />
  );
}
