import React from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import { Loader } from "@googlemaps/js-api-loader";
import { Helmet } from "react-helmet";
import * as turf from "@turf/turf";
import { useFormik } from "formik";
import * as yup from "yup";

import { SmallLoader } from "../../../../../../../components/common/SmallLoader";

import A from "../../../../../../../utils/API";
import U from "../../../../../../../utils/utils";
import NavLinks from "../../../../../../../utils/navLinks.json";

import useAdmin from "../../../../../../../hooks/useAdmin";
import { FormWrapper } from "../../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../../../components/common/TextField";
import useUtils from "../../../../../../../hooks/useUtils";
import useLanguage from "../../../../../../../hooks/useLanguage";
import useSettings from "../../../../../../../hooks/useSettings";
import GooglePlaceComplete from "../../../../../../../components/common/GooglePlaceComplete";

export default function Index({ history }) {
  const { airportID, stopID } = useParams();
  const { authFailure, header } = useAdmin();
  const { language } = useLanguage();
  const [airport, setAirport] = React.useState(null);
  const [loading, setLoading] = React.useState(false);
  const { settings } = useSettings();
  const [googleLoaded, setGoogleLoaded] = React.useState(false);
  const [dataLoading, setDataLoading] = React.useState(true);
  const Map = React.useRef(null);
  const MapData = React.useRef(null);
  const polygon = React.useRef(null);

  const [address, setAddress] = React.useState(null);
  const [inside, setInside] = React.useState(null);

  const latLng = React.useRef(null);

  const formik = useFormik({
    initialValues: {
      name: "",
      status: U.ACTIVE,
    },
    validationSchema: yup.object().shape({
      name: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setLoading(true);
      try {
        const sendData = {
          id: stopID,
          airportId: airportID,
          status: e.status,
          name: e.name,
          location: {
            lat: address.results[0].geometry.location.lat,
            lng: address.results[0].geometry.location.lng,
          },
        };
        const { data } = await axios.post(A.HOST + A.ADMIN_AIRPORTS_STOPS_UPDATE, sendData, header);
        history.goBack();
      } catch (err) {
        alert(err);
        authFailure(err);
      }
      setLoading(false);
    },
  });

  const fetchAirport = async () => {
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_AIRPORTS_READ,
        {
          id: airportID,
        },
        header
      );
      setAirport(data.data);
    } catch (err) {
      authFailure(err);
    }
  };

  React.useEffect(() => {
    if (googleLoaded && Map.current && airport && dataLoading === false) {
      const map = new window.google.maps.Map(Map.current, {
        center: { lat: 12.9791551, lng: 80.2007085 },
        zoom: 14,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: true,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: true,
      });

      MapData.current = map;

      if (airport.locationCoordinates && airport.locationCoordinates.coordinates) {
        polygon.current = new window.google.maps.Polygon({
          paths: airport.locationCoordinates.coordinates[0].map(
            (each) => new window.google.maps.LatLng(each[1], each[0])
          ),
          map: MapData.current,
          strokeColor: "#262525",
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: "#3B6BCC",
          fillOpacity: 0.5,
        });
        var bounds = new window.google.maps.LatLngBounds();
        airport.locationCoordinates.coordinates[0].forEach((location) =>
          bounds.extend({ lat: location[1], lng: location[0] })
        );
        MapData.current.fitBounds(bounds);
      }

      if (
        address &&
        airport.locationCoordinates &&
        airport.locationCoordinates.coordinates &&
        Array.isArray(airport.locationCoordinates.coordinates) &&
        airport.locationCoordinates.coordinates.length > 0 &&
        polygon.current &&
        MapData.current
      ) {
        console.log(MapData.current);
        try {
          latLng.current.setMap(null);
        } catch (err) {}
        latLng.current = new window.google.maps.Marker({
          position: address.results[0].geometry.location,
          map: map,
        });
        var points = turf.points([
          [address.results[0].geometry.location.lng, address.results[0].geometry.location.lat],
        ]);
        var searchWithin = turf.polygon(airport.locationCoordinates.coordinates);
        var ptsWithin = turf.pointsWithinPolygon(points, searchWithin);
        if (ptsWithin.features.length > 0) {
          polygon.current.setOptions({
            fillColor: "green",
          });
          setInside(true);
        } else {
          latLng.current.setMap(null);
          polygon.current.setOptions({
            fillColor: "red",
          });
          setInside(false);
        }
      }
    }
  }, [googleLoaded, airport, address, dataLoading]);

  React.useEffect(() => {
    const loader = new Loader({
      apiKey: settings.mapApi.web,
      version: "weekly",
      libraries: ["places"],
    });
    loader
      .load()
      .then(() => {
        setGoogleLoaded(true);
        // gsap.fromTo(".addHub", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
      })
      .catch((e) => {});
  }, []);

  const fetchDetails = async () => {
    setDataLoading(true);
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_AIRPORTS_STOPS_READ,
        {
          airportId: airportID,
          id: stopID,
        },
        header
      );
      formik.setFieldValue("name", data.name);
      formik.setFieldValue("status", data.status);
      if (data.location) {
        const { status, data: googleData } = await axios.get(
          `https://maps.googleapis.com/maps/api/geocode/json?latlng=${data.location.lat},${data.location.lng}&key=${settings.mapApi.web}`
        );
        if (status === 200)
          setAddress({
            ...googleData,
          });
      }
    } catch (err) {
      alert(err);
      authFailure(err);
    }
    setDataLoading(false);
  };

  React.useEffect(() => {
    fetchAirport();
    fetchDetails();
  }, []);

  React.useEffect(() => {}, [address]);

  return airport && airport && googleLoaded && dataLoading === false ? (
    <FormWrapper
      bread={[
        { id: 1, title: language.AIRPORTS, path: NavLinks.ADMIN_SETUP_AIRPORTS },
        {
          id: 2,
          title: language.STOPS,
          path: NavLinks.ADMIN_SETUP_AIRPORTS + airportID + NavLinks.ADMIN_SETUP_AIRPORTS_STOPS_LISTS,
        },
      ]}
      title={language.EDIT}
      submit={() => inside && formik.handleSubmit()}
      submitBtn={true}
      btnLoading={loading}
      width={"3/4"}
    >
      <Helmet>
        <script
          src={`https://maps.googleapis.com/maps/api/js?key=${settings.mapApi.web}&libraries=places`}
          async
        ></script>
      </Helmet>
      <>
        <Section>
          <FieldWrapper title={language.NAME}>
            <TextField
              error={formik.errors.name}
              change={(e) => formik.setFieldValue("name", e)}
              value={formik.values.name}
              placeholder={language.NAME}
            />
          </FieldWrapper>
          <FieldWrapper title={language.ADDRESS}>
            <GooglePlaceComplete
              change={(e) => {
                setAddress(e);
              }}
              placeholder={language.ADDRESS}
            />
          </FieldWrapper>
          {inside === false && (
            <FieldWrapper>
              <p className="text-sm text-red-500">{language.SHOULD_NOT_FALL_OUTSIDE_POLYGON}</p>
            </FieldWrapper>
          )}
        </Section>
        <Section>
          <FieldWrapper title={language.MAP}>
            <div ref={Map} style={{ width: "100%", height: 400 }}></div>
          </FieldWrapper>
        </Section>
      </>
    </FormWrapper>
  ) : (
    <SmallLoader />
  );
}
