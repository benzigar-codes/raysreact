import React from "react";
import axios from "axios";

import { useParams } from "react-router-dom";

import Table from "../../../../../../components/common/Table";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";

import useLanguage from "../../../../../../hooks/useLanguage";

import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";
import NavLinks from "../../../../../../utils/navLinks.json";
import useAdmin from "../../../../../../hooks/useAdmin";
import useUtils from "../../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { airportID } = useParams();
  const { admin } = useAdmin();
  return (
    <Table
      title={language.STOPS}
      startingHeadings={[
        {
          id: 1,
          title: language.NAME,
          key: "name",
          show: true,
        },
        {
          id: 4,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      bread={[{ id: 1, title: language.AIRPORTS, path: NavLinks.ADMIN_SETUP_AIRPORTS }]}
      list={A.HOST + A.ADMIN_AIRPORTS_STOPS_LISTS + "/" + airportID}
      assignData={(data) =>
        data.map((stops) => ({
          _id: stops._id,
          name: stops.name,
          status: stops.status === U.ACTIVE ? 1 : 0,
        }))
      }
      showAdd={admin.privileges.SETUP.AIRPORTS ? (admin.privileges.SETUP.AIRPORTS.ADD ? true : false) : false}
      showArchieve={false}
      showEdit={admin.privileges.SETUP.AIRPORTS ? (admin.privileges.SETUP.AIRPORTS.EDIT ? true : false) : false}
      addClick={() => history.push(NavLinks.ADMIN_SETUP_AIRPORTS + airportID + NavLinks.ADMIN_SETUP_AIRPORTS_STOPS_ADD)}
      editClick={(e) =>
        history.push(NavLinks.ADMIN_SETUP_AIRPORTS + airportID + NavLinks.ADMIN_SETUP_AIRPORTS_STOPS_EDIT + "/" + e)
      }
      showBulk={false}
      showView={false}
      showSearch={true}
      showAction={admin.privileges.SETUP.AIRPORTS ? (admin.privileges.SETUP.AIRPORTS.EDIT ? true : false) : false}
      add={admin.privileges.SETUP.AIRPORTS ? (admin.privileges.SETUP.AIRPORTS.ADD ? true : false) : false}
      edit={admin.privileges.SETUP.AIRPORTS ? (admin.privileges.SETUP.AIRPORTS.EDIT ? true : false) : false}
      statusList={A.HOST + A.ADMIN_AIRPORTS_STOPS_CHANGE_STATUS + "/" + airportID}
    />
  );
}
