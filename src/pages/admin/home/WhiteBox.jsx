import React from "react";

export default function WhiteBox({ children, className, ...rest }) {
  return <div className={"bg-white p-4 " + className} {...rest}>{children}</div>;
}
