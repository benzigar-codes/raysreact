import React from "react";

const IconPing = ({ icon, ping = true, ...rest }) => {
  return (
    <div
      {...rest}
      className="relative cursor-pointer bg-gray-300 dark:bg-black dark:text-white p-3 rounded-full text-black"
    >
      {icon}
      {ping === true && (
        <div className="animate-ping absolute h-3 w-3 top-0 right-0 bg-blue-800 rounded-full"></div>
      )}
      {ping === true && (
        <div className="absolute h-3 w-3 top-0 right-0 bg-blue-800 rounded-full"></div>
      )}
    </div>
  );
};

export default IconPing;
