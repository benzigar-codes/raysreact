import React from "react";

const DropDownWrapper = ({ children = "", ...rest }) => {
  const direction = document.getElementsByTagName("html")[0].getAttribute("dir");
  return (
    <div className="relative">
      <div
        {...rest}
        className="hideOnClick border-t-2 absolute dark:bg-gray-800 dark:text-white mt-2 rounded-xl text-sm w-40 bg-white shadow-lg p-3 flex flex-col space-y-3"
        style={direction === "ltr" ? { right: 0, top: 20 } : { left: 0, top: 20 }}
      >
        {children}
      </div>
    </div>
  );
};

export default DropDownWrapper;
