import React, { useState } from "react";
import OutSideClick from "react-outside-click-handler";
import { FiAlignLeft, FiAlignRight, FiBell, FiLoader, FiMoon, FiSun, FiUser } from "react-icons/fi";
import { AiFillCar, AiFillWarning } from "react-icons/ai";
import { HiTranslate } from "react-icons/hi";
import { FaHome, FaSatellite } from "react-icons/fa";
import { GiEagleEmblem } from "react-icons/gi";
import axios from "axios";
import gsap from "gsap";

import MainMenuIcons from "./MainMenuIcons";
import MainMenuWrapper from "./MainMenuWrapper";
import ImageIcon from "./ImageIcon";
import TextIcon from "./TextIcon";
import IconPing from "./IconPing";
import Text from "./Text";
import Logo from "./Logo";
import DropDownItem from "./DropDownItem";
import Menu from "./Menu";
import TopMenuWrapper from "./TopMenuWrapper";
import DropDownContainer from "./DropDownContainer";
import DropDownWrapper from "./DropDownWrapper";
import Notification from "../../../components/Notifications";

import Flex from "../../../components/common/Flex";

import A from "../../../utils/API.js";
import U from "../../../utils/utils.js";
import NavLinks from "../../../utils/navLinks.json";

import useAdmin from "../../../hooks/useAdmin";
import useSettings from "../../../hooks/useSettings";
import useTheme from "../../../hooks/useTheme";
import useAlign from "../../../hooks/useAlign";
import useLanguage from "../../../hooks/useLanguage";
import useImage from "../../../hooks/useImage";
import { useLocation } from "react-router";

export default function AdminTopMenu({ nav, toggleNav, history }) {
  // HOOKS
  const { settings, setSettings } = useSettings();
  const { align, toggleAlign } = useAlign();
  const { theme, toggleTheme } = useTheme();
  const { language, setLanguage } = useLanguage();
  const { imageUrl } = useImage();
  const { alignLeft, alignRight } = useAlign();
  const location = useLocation();
  const { admin, setAdmin, adminProfileFetch, adminLogout, header } = useAdmin();

  const [imageError, setImageError] = useState(true);
  const [langList, setLangList] = React.useState([]);
  const [langLoading, setLangLoading] = React.useState(false);
  const [showNotification, setNotification] = React.useState(false);

  const direction = document.getElementsByTagName("html")[0].getAttribute("dir");

  // STATES
  const [profileDrop, setProfileDrop] = useState(false);
  const [translateDrop, setTranslateDrop] = useState(false);

  React.useEffect(() => {
    const langFetch = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_LANGUAGE_LIST_ALL, {}, header);
        setLangList(data.response);
      } catch (err) {
        alert(err);
      }
    };
    const profileFetch = async () => {
      await adminProfileFetch();
      setImageError(false);
    };
    profileFetch();
    langFetch();
  }, []);

  const setDefaultLanguage = async (lang) => {
    setLangLoading(true);
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_LANGUAGE_USER_SET, { languageCode: lang }, header);
      data.languageDirection === "RTL" ? alignRight() : alignLeft();
      setLanguage(data.languageKeys);
      setAdmin({ ...admin, languageCode: lang, languageDirection: data.languageDirection });
    } catch (err) {
      setLangLoading(false);
    }
  };

  React.useEffect(() => setLangLoading(false), [language]);

  return (
    <TopMenuWrapper>
      <Flex>
        <Logo
          src={
            document.getElementsByTagName("html")[0].classList.contains("dark")
              ? imageUrl(settings.darkLogo)
              : imageUrl(settings.lightLogo)
          }
        />
        {admin.userType !== U.CORPORATE && <Menu onClick={() => toggleNav(!nav)} />}
        {location.pathname !== NavLinks.ADMIN_HOME && (
          <MainMenuIcons
            onClick={() => history.push(NavLinks.ADMIN_HOME)}
            title={language.DASHBOARD}
            icon={<FaHome />}
          />
        )}
        {admin.userType === "DEVELOPER" && <p className="mx-3">{U.mode}</p>}
      </Flex>

      <Flex>
        <MainMenuWrapper>
          {admin &&
            admin.privileges &&
            admin.privileges.OTHERS &&
            admin.privileges.OTHERS.SATELLITE_VIEW &&
            admin.privileges.OTHERS.SATELLITE_VIEW.VIEW &&
            admin.userType !== U.CORPORATE && (
              <a href={NavLinks.ADMIN_SATILLITE_VIEW} target={"_blank"} rel="noreferrer">
                <MainMenuIcons title={language.SATELLITE_VIEW} icon={<FaSatellite />} />
              </a>
            )}
          {admin.privileges &&
            admin.privileges.OTHERS &&
            admin.privileges.OTHERS.EAGLE_VIEW &&
            admin.privileges.OTHERS.EAGLE_VIEW.VIEW &&
            admin.userType !== U.CORPORATE && (
              <a href={NavLinks.ADMIN_EAGLE_VIEW} target={"_blank"} rel="noreferrer">
                <MainMenuIcons title={language.EAGLE_VIEW} icon={<GiEagleEmblem />} />
              </a>
            )}
          {/* {admin.privileges &&
            admin.privileges.OTHERS &&
            admin.privileges.OTHERS.BOOK_RIDE &&
            admin.privileges.OTHERS.BOOK_RIDE.VIEW && (
              <a href={NavLinks.ADMIN_BOOK_RIDE} target={"_blank"} rel={"noreferrer"}>
                <MainMenuIcons title={language.BOOK_RIDE} icon={<AiFillCar />} />
              </a>
            )} */}
          {admin.privileges &&
            admin.privileges.OTHERS &&
            admin.privileges.OTHERS.DISPATCH &&
            admin.privileges.OTHERS.DISPATCH.VIEW && (
              <a href={NavLinks.ADMIN_DISPATCH} target={"_blank"} rel={"noreferrer"}>
                <MainMenuIcons title={language.DISPATCH} icon={<AiFillCar />} />
              </a>
            )}
        </MainMenuWrapper>
        <MainMenuWrapper>
          <MainMenuIcons
            onClick={() => setTranslateDrop(true)}
            // title={language.TRANSLATE}
            icon={langLoading ? <FiLoader className="animate-spin" /> : <HiTranslate />}
          />
          {translateDrop === true && langList.length > 0 && (
            <>
              <OutSideClick onOutsideClick={() => setTranslateDrop(false)}>
                <DropDownWrapper>
                  {langList.length > 0 &&
                    langList.map((eachLang) => (
                      <DropDownItem
                        onClick={() => setDefaultLanguage(eachLang.data.languageCode)}
                        className="fade"
                        checked={
                          admin.languageCode !== undefined
                            ? admin.languageCode === eachLang.data.languageCode
                            : settings.languageCode === eachLang.data.languageCode
                        }
                        title={eachLang.data.languageName}
                      />
                    ))}
                </DropDownWrapper>
              </OutSideClick>
            </>
          )}
          <MainMenuIcons
            onClick={toggleTheme}
            // title={language.TOGGLE_THEME}
            icon={theme === "light" ? <FiSun /> : <FiMoon />}
          />

          <MainMenuIcons onClick={toggleAlign} icon={align === "left" ? <FiAlignLeft /> : <FiAlignRight />} />
        </MainMenuWrapper>

        {admin.userType !== U.CORPORATE && (
          <MainMenuWrapper>
            <Notification />
          </MainMenuWrapper>
        )}

        <Flex className={`${direction === "ltr" ? "ml-2" : "mr-2"} flex items-center`}>
          {/* <IconPing icon={<FiBell />} ping={false} /> */}
          <DropDownContainer onClick={() => setProfileDrop(!profileDrop)}>
            {imageError === false && admin.avatar ? (
              <ImageIcon
                src={imageUrl(admin.avatar)}
                onClick={() => setProfileDrop(!profileDrop)}
                onError={() => setImageError(true)}
              />
            ) : (
              <TextIcon
                title={
                  admin.firstName !== undefined
                    ? admin.firstName
                    : admin.hubsName !== undefined
                    ? admin.hubsName
                    : admin.officeName
                }
              />
            )}
            <Text
              title={
                admin.firstName !== undefined
                  ? admin.firstName
                  : admin.hubsName !== undefined
                  ? admin.hubsName
                  : admin.officeName
              }
            />
            {profileDrop === true && (
              <>
                <OutSideClick onOutsideClick={() => setProfileDrop(false)}>
                  <DropDownWrapper>
                    <DropDownItem className="fade" link={NavLinks.ADMIN_PROFILE} title={language.PROFILE} />
                    {(admin.userType === "DEVELOPER" || admin.userType === "ADMIN") && (
                      <DropDownItem className="fade" link={NavLinks.ADMIN_GENERAL} title={language.GENERAL} />
                    )}
                    {/* <DropDownItem
                      className="fade"
                      link={NavLinks.ADMIN_CHANGE_PASSWORD}
                      title={language.CHANGE_PASSWORD}
                    /> */}
                    <DropDownItem className="fade" onClick={adminLogout} title={language.SIGN_OUT} />
                  </DropDownWrapper>
                </OutSideClick>
              </>
            )}
          </DropDownContainer>
        </Flex>
      </Flex>
    </TopMenuWrapper>
  );
}
