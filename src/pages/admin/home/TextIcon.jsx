import React from 'react'

const TextIcon = ({ title, ...rest }) => {
  return (
    <div
      {...rest}
      className="h-10 flex justify-center items-center bg-gray-200 dark:bg-black dark:text-white rounded-full w-10 object-cover cursor-pointer"
    >
      <p>{title.split("")[0].toUpperCase()}</p>
    </div>
  );
};

export default TextIcon