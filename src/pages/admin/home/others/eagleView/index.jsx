import React from "react";
import { FiCalendar, FiCheck } from "react-icons/fi";
import axios from "axios";
import { format } from "date-fns";
import { FaRegMoneyBillAlt } from "react-icons/fa";
import { BsCreditCard, BsWallet } from "react-icons/bs";
import { BiMap } from "react-icons/bi";
import { AiOutlineCar, AiOutlineHome } from "react-icons/ai";

import useLanguage from "../../../../../hooks/useLanguage";
import useAdmin from "../../../../../hooks/useAdmin";

import { SmallLoader } from "../../../../../components/common/SmallLoader";
import { ToggleButton } from "../../../../../components/common/ToggleButton";
import { DropdownNormal } from "../../../../../components/common/DropDownNormal";

import A from "../../../../../utils/API.js";
import NavLinks from "../../../../../utils/navLinks.json";
import { Link } from "react-router-dom";
import useImage from "../../../../../hooks/useImage";
import useSettings from "../../../../../hooks/useSettings";
import useUtils from "../../../../../hooks/useUtils";

const EachSide = ({ heading = "New Rides", count = 10, children }) => (
  <div className="w-1/6 px-1 h-full">
    <div className={"bg-gray-100 dark:bg-gray-900 h-full p-2 pb-20"}>
      <h1 className={"pb-2 text-center text-gray-400 font-semibold"}>
        {heading} ( {count} )
      </h1>
      <div className={"h-full overflow-y-scroll pb-10"}>{children}</div>
    </div>
  </div>
);

const EachCard = ({
  id = 0,
  bookingID,
  date = "",
  status,
  city = "",
  color = "",
  currencyCode = "",
  bookingType = "",
  amount = "",
  paymentType = "",
  click = () => {},
  paid = false,
  distance = 0,
  vehicleCategory = "",
}) => {
  const { language } = useLanguage();
  const { truncate, morph } = useUtils();
  return (
    <Link to={NavLinks.ADMIN_RIDES_VIEW + "/" + id}>
      <div
        onClick={click}
        className={
          "card bg-white dark:bg-black dark:text-white rounded-md p-2 cursor-pointer hover:bg-gray-50 mt-3 border-t-2"
        }
        style={{
          borderColor: color,
        }}
      >
        <div className="flex justify-between items-center mb-2">
          <h1 className={"text-sm truncate"}>{bookingID}</h1>
          <p
            className={"mx-1 px-1 text-white text-sm text-center truncate"}
            style={{
              color,
            }}
          >
            {truncate(language[status], 12)}
          </p>
        </div>
        <div className={"flex items-center mb-2"}>
          <FiCalendar className={"text-xl"} style={{ color }} />
          <p className={"text-sm mx-2 text-gray-600 truncate dark:text-white"}>
            {format(Date.parse(date), "dd LLL, hh : mm a")}
          </p>
        </div>
        {paymentType === "CASH" && (
          <div className="flex justify-between items-center">
            <div className={"flex items-center mb-2"}>
              <FaRegMoneyBillAlt className={"text-xl"} style={{ color }} />
              <p className={"text-sm mx-2 text-gray-600 truncate dark:text-white"}>
                {currencyCode} {amount && amount.toFixed(2)}
              </p>
              {paid === true && <FiCheck />}
            </div>
            <div className={"flex items-center mb-2"}>
              <AiOutlineCar className={"text-xl"} style={{ color }} />
              <p className={"text-sm mx-2 text-gray-600 truncate dark:text-white"}>{truncate(vehicleCategory, 7)}</p>
            </div>
          </div>
        )}
        {paymentType === "CREDIT" && (
          <div className="flex justify-between items-center">
            <div className={"flex items-center mb-2"}>
              <FaRegMoneyBillAlt className={"text-xl"} style={{ color }} />
              <p className={"text-sm mx-2 text-gray-600 truncate dark:text-white"}>
                {currencyCode} {amount && amount.toFixed(2)}
              </p>
              {paid === true && <FiCheck />}
            </div>
            <div className={"flex items-center mb-2"}>
              <AiOutlineCar className={"text-xl"} style={{ color }} />
              <p className={"text-sm mx-2 text-gray-600 truncate dark:text-white"}>{truncate(vehicleCategory, 7)}</p>
            </div>
          </div>
        )}
        {paymentType === "CARD" && (
          <div className="flex justify-between items-center">
            <div className={"flex items-center mb-2"}>
              <BsCreditCard className={"text-xl"} style={{ color }} />
              <p className={"text-sm mx-2 text-gray-600 truncate dark:text-white"}>
                {currencyCode} {amount && amount.toFixed(2)}
              </p>
              {paid === true && <FiCheck />}
            </div>
            <div className={"flex items-center mb-2"}>
              <AiOutlineCar className={"text-xl"} style={{ color }} />
              <p className={"text-sm mx-2 text-gray-600 dark:text-white"}>{truncate(vehicleCategory, 7)}</p>
            </div>
          </div>
        )}
        {paymentType === "WALLET" && (
          <div className="flex justify-between items-center">
            <div className={"flex items-center mb-2"}>
              <BsWallet className={"text-xl"} style={{ color }} />
              <p className={"text-sm mx-2 text-gray-600 dark:text-white"}>
                {currencyCode} {amount && amount.toFixed(2)}
              </p>
              {paid === true && <FiCheck />}
            </div>
            <div className={"flex items-center mb-2"}>
              <AiOutlineCar className={"text-xl"} style={{ color }} />
              <p className={"text-sm mx-2 text-gray-600 truncate dark:text-white"}>{truncate(vehicleCategory, 7)}</p>
            </div>
          </div>
        )}
        <div className={"flex justify-between items-center mb-2"}>
          <div className={"flex items-center"}>
            <BiMap className={"text-xl"} style={{ color }} />
            <p className={"text-sm mx-2 text-gray-600  dark:text-white"}>{(distance / 1000).toFixed(2)} KM</p>
          </div>
        </div>
        <div className={"flex justify-between items-center mb-2"}>
          <div className={"flex items-center"}>
            <AiOutlineHome className={"text-xl"} style={{ color }} />
            <p className={"text-sm mx-2 text-gray-600  dark:text-white truncate"}>{morph(truncate(city, 22))}</p>
          </div>
        </div>
        <div className="flex justify-start">
          <p
            className={"mx-1 px-1 truncate text-white text-sm text-center"}
            style={{
              backgroundColor:
                bookingType === "INSTANT" ? "#608EF5" : "SCHEDULE" ? "#F17588" : "SHARE" ? "#05174F" : null,
            }}
          >
            {language[bookingType]}
          </p>
        </div>
      </div>
    </Link>
  );
};

export default function Index({ history }) {
  const [loading, setLoading] = React.useState(true);
  const [views, setViews] = React.useState([]);
  const [time, setTime] = React.useState(3);
  const { language } = useLanguage();
  const { imageUrl } = useImage();
  const { settings } = useSettings();

  document.title = language.EAGLE_VIEW;

  const viewInterval = React.useRef();
  const [categories, setCategories] = React.useState(null);
  const { header, authFailure } = useAdmin();
  const [live, setLive] = React.useState(false);
  const [cities, setCities] = React.useState(null);

  const [selectedCity, setSelectedCity] = React.useState("");

  const timeOptions = [
    {
      label: language.LAST_1_HOUR,
      value: 1,
    },
    {
      label: language.LAST_3_HOURS,
      value: 3,
    },
    {
      label: language.LAST_8_HOURS,
      value: 8,
    },
    {
      label: language.LAST_1_DAY,
      value: 24,
    },
    {
      label: language.LAST_3_DAYS,
      value: 72,
    },
    {
      label: language.LAST_1_WEEK,
      value: 168,
    },
    {
      label: language.LAST_3_WEEKS,
      value: 504,
    },
  ];

  const fetchEagle = async () => {
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_EAGLE_VIEW,
        {
          time,
          cityId: selectedCity,
        },
        header
      );
      setViews(data);
      setLoading(false);
    } catch (err) {
      authFailure(err);
      viewInterval !== null && clearInterval(viewInterval.current);
    }
  };

  React.useEffect(() => {
    const fetchEverything = async () => {
      const fetchVehicleCategory = async () => {
        try {
          const { data } = await axios.post(A.HOST + A.ADMIN_EAGLE_VEHICLE_CATEGORY, {}, header);
          setCategories(data);
        } catch (err) {
          authFailure(err);
          viewInterval !== null && clearInterval(viewInterval.current);
        }
      };
      const fetchCities = async () => {
        try {
          const { data } = await axios.post(A.HOST + A.ADMIN_CITY_LIST, {}, header);
          setCities(data.response);
          if (data.response[0]._id) setSelectedCity(data.response[0]._id);
        } catch (err) {
          authFailure(err);
          if (err.response && err.response.status === 401) {
            clearInterval(viewInterval.current);
          }
          viewInterval !== null && clearInterval(viewInterval.current);
        }
      };
      await fetchVehicleCategory();
      await fetchCities();
      setLive(1);
    };
    fetchEverything();
  }, []);

  React.useEffect(() => {
    if (time === 3 || time === 1) {
      fetchEagle();
      clearInterval(viewInterval.current);
      viewInterval.current = setInterval(fetchEagle, 5000);
      // setViewInterval(interval);
      setLive(1);
    } else {
      fetchEagle();
      if (viewInterval !== null) clearInterval(viewInterval.current);
      setLive(0);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [live, time, selectedCity]);

  React.useEffect(() => console.log(selectedCity), [selectedCity]);

  return loading === false && cities && categories ? (
    <div className="fixed inset-0 bg-white dark:bg-gray-800 dark:text-white flex flex-col shadow-xl h-screen w-full">
      <div className={"py-2 px-10 flex shadow-xl justify-between items-center"}>
        <div className="flex items-center">
          <Link to="/admin/home">
            <img
              className="h-8"
              src={
                document.getElementsByTagName("html")[0].classList.contains("dark")
                  ? imageUrl(settings.darkLogo)
                  : imageUrl(settings.lightLogo)
              }
              alt=""
            />
          </Link>
          {/* <FiHome className={"cursor-pointer hover:text-blue-800"} onClick={() => history.push(NavLinks.ADMIN_HOME)} /> */}
          <h1 className={"mx-2"}>{language.EAGLE_VIEW}</h1>
        </div>
        {live === 1 && (
          <div className="flex items-center">
            <h1>{language.LIVE} </h1>
            {live === 0 && (
              <>
                <ToggleButton className={"mx-2"} />
              </>
            )}
            {live === 1 && (
              <>
                <ToggleButton defaultValue={live} className={"absolute animate-ping ml-11"} />
                <ToggleButton defaultValue={live} className={"mx-2"} />
              </>
            )}
          </div>
        )}
        {live === 0 && <div className={"text-red-500 text-sm"}>{language.EAGLE_VIEW_LIVE_INFO}</div>}
        <div className="flex items-center">
          <div className={"text-black dark:text-white w-32 mx-4"}>
            <DropdownNormal
              defaultValue={
                [
                  { id: 1, locationName: language.ALL, value: "" },
                  ...cities.map((each) => ({ id: each._id, locationName: each.locationName, value: each._id })),
                ].filter((each) => each.value === selectedCity)[0].locationName
              }
              change={(e) => {
                setSelectedCity(e);
              }}
              fields={
                cities && [
                  { id: 1, label: language.ALL, value: "" },
                  ...cities.map((each) => ({ id: each._id, label: each.locationName, value: each._id })),
                ]
              }
            />
          </div>
          <div className={"text-black dark:text-white w-32 mx-4"}>
            <DropdownNormal
              defaultValue={timeOptions.filter((each) => each.value === time)[0].label}
              change={(e) => {
                setLive(0);
                setTime(parseInt(e));
              }}
              fields={timeOptions}
            />
          </div>
        </div>
      </div>
      <div className="h-full flex bg-white dark:bg-black">
        <EachSide
          heading={"New Rides"}
          count={views.filter((eachView) => eachView.bookingStatus === "AWAITING").length}
        >
          {views.length > 0 &&
            views
              .filter((eachView) => eachView.bookingStatus === "AWAITING")
              .map((eachView) => (
                <EachCard
                  color={eachView.bookingType === "INSTANT" ? "#00adb5" : "#f38181"}
                  id={eachView._id}
                  bookingID={eachView.bookingId}
                  status={eachView.bookingStatus}
                  paid={eachView.payment.paid}
                  bookingType={eachView.bookingType}
                  distance={eachView.estimation.distance}
                  paymentType={eachView.payment.option}
                  vehicleCategory={
                    categories.filter((category) => category._id === eachView.categoryId).length > 0
                      ? categories.filter((category) => category._id === eachView.categoryId)[0].name
                      : ""
                  }
                  city={
                    cities && cities.filter((city) => city._id === eachView.cityId).length > 0
                      ? cities.filter((city) => city._id === eachView.cityId)[0].locationName
                      : ""
                  }
                  amount={eachView.amount}
                  date={eachView.bookingDate}
                  currencyCode={eachView.currencySymbol}
                />
              ))}
          {views.filter((eachView) => eachView.bookingStatus === "AWAITING").length === 0 && (
            <p className={"text-sm text-gray-500 text-center px-2"}>{language.NO_DATA_FOUND_IN_GIVEN_TIME}</p>
          )}
        </EachSide>
        <EachSide
          heading={"Accepted Rides"}
          count={views.filter((eachView) => eachView.bookingStatus === "ACCEPTED").length}
        >
          {views.length > 0 &&
            views
              .filter((eachView) => eachView.bookingStatus === "ACCEPTED")
              .map((eachView) => (
                <EachCard
                  color={"#3282b8"}
                  id={eachView._id}
                  bookingID={eachView.bookingId}
                  status={eachView.bookingStatus}
                  paid={eachView.payment.paid}
                  vehicleCategory={
                    categories.filter((category) => category._id === eachView.categoryId).length > 0
                      ? categories.filter((category) => category._id === eachView.categoryId)[0].name
                      : ""
                  }
                  city={
                    cities && cities.filter((city) => city._id === eachView.cityId).length > 0
                      ? cities.filter((city) => city._id === eachView.cityId)[0].locationName
                      : ""
                  }
                  bookingType={eachView.bookingType}
                  distance={eachView.estimation.distance}
                  paymentType={eachView.payment.option}
                  amount={eachView.amount}
                  date={eachView.bookingDate}
                  currencyCode={eachView.currencySymbol}
                />
              ))}
          {views.filter((eachView) => eachView.bookingStatus === "ACCEPTED").length === 0 && (
            <p className={"text-sm text-gray-500 text-center px-2"}>{language.NO_DATA_FOUND_IN_GIVEN_TIME}</p>
          )}
        </EachSide>
        <EachSide
          heading={"Arrived Rides"}
          count={views.filter((eachView) => eachView.bookingStatus === "ARRIVED").length}
        >
          {views.length > 0 &&
            views
              .filter((eachView) => eachView.bookingStatus === "ARRIVED")
              .map((eachView) => (
                <EachCard
                  color={"#f85f73"}
                  id={eachView._id}
                  bookingID={eachView.bookingId}
                  status={eachView.bookingStatus}
                  paid={eachView.payment.paid}
                  vehicleCategory={
                    categories.filter((category) => category._id === eachView.categoryId).length > 0
                      ? categories.filter((category) => category._id === eachView.categoryId)[0].name
                      : ""
                  }
                  city={
                    cities && cities.filter((city) => city._id === eachView.cityId).length > 0
                      ? cities.filter((city) => city._id === eachView.cityId)[0].locationName
                      : ""
                  }
                  bookingType={eachView.bookingType}
                  distance={eachView.estimation.distance}
                  paymentType={eachView.payment.option}
                  amount={eachView.amount}
                  date={eachView.bookingDate}
                  currencyCode={eachView.currencySymbol}
                />
              ))}
          {views.filter((eachView) => eachView.bookingStatus === "ARRIVED").length === 0 && (
            <p className={"text-sm text-gray-500 text-center px-2"}>{language.NO_DATA_FOUND_IN_GIVEN_TIME}</p>
          )}
        </EachSide>
        <EachSide
          heading={"Ongoing Rides"}
          count={views.filter((eachView) => eachView.bookingStatus === "STARTED").length}
        >
          {views.length > 0 &&
            views
              .filter((eachView) => eachView.bookingStatus === "STARTED")
              .map((eachView) => (
                <EachCard
                  color={"#7d5a5a"}
                  id={eachView._id}
                  bookingID={eachView.bookingId}
                  status={eachView.bookingStatus}
                  paid={eachView.payment.paid}
                  vehicleCategory={
                    categories.filter((category) => category._id === eachView.categoryId).length > 0
                      ? categories.filter((category) => category._id === eachView.categoryId)[0].name
                      : ""
                  }
                  city={
                    cities && cities.filter((city) => city._id === eachView.cityId).length > 0
                      ? cities.filter((city) => city._id === eachView.cityId)[0].locationName
                      : ""
                  }
                  bookingType={eachView.bookingType}
                  distance={eachView.estimation.distance}
                  paymentType={eachView.payment.option}
                  amount={eachView.amount}
                  date={eachView.bookingDate}
                  currencyCode={eachView.currencySymbol}
                />
              ))}
          {views.filter((eachView) => eachView.bookingStatus === "STARTED").length === 0 && (
            <p className={"text-sm text-gray-500 text-center px-2"}>{language.NO_DATA_FOUND_IN_GIVEN_TIME}</p>
          )}
        </EachSide>
        <EachSide heading={"Ended Rides"} count={views.filter((eachView) => eachView.bookingStatus === "ENDED").length}>
          {views.length > 0 &&
            views
              .filter((eachView) => eachView.bookingStatus === "ENDED")
              .map((eachView) => (
                <EachCard
                  color={"#81b214"}
                  id={eachView._id}
                  bookingID={eachView.bookingId}
                  status={eachView.bookingStatus}
                  paid={eachView.payment.paid}
                  vehicleCategory={
                    categories.filter((category) => category._id === eachView.categoryId).length > 0
                      ? categories.filter((category) => category._id === eachView.categoryId)[0].name
                      : ""
                  }
                  city={
                    cities && cities.filter((city) => city._id === eachView.cityId).length > 0
                      ? cities.filter((city) => city._id === eachView.cityId)[0].locationName
                      : ""
                  }
                  bookingType={eachView.bookingType}
                  distance={eachView.estimation.distance}
                  paymentType={eachView.payment.option}
                  amount={eachView.amount}
                  date={eachView.bookingDate}
                  currencyCode={eachView.currencySymbol}
                />
              ))}
          {views.filter((eachView) => eachView.bookingStatus === "ENDED").length === 0 && (
            <p className={"text-sm text-gray-500 text-center px-2"}>{language.NO_DATA_FOUND_IN_GIVEN_TIME}</p>
          )}
        </EachSide>
        <EachSide
          heading={"Cancelled Rides"}
          count={
            views.filter(
              (eachView) =>
                eachView.bookingStatus === "EXPIRED" ||
                eachView.bookingStatus === "USERCANCELLED" ||
                eachView.bookingStatus === "PROFESSIONALCANCELLED" ||
                eachView.bookingStatus === "USERDENY"
            ).length
          }
        >
          {views.length > 0 &&
            views
              .filter(
                (eachView) =>
                  eachView.bookingStatus === "EXPIRED" ||
                  eachView.bookingStatus === "USERCANCELLED" ||
                  eachView.bookingStatus === "PROFESSIONALCANCELLED" ||
                  eachView.bookingStatus === "USERDENY"
              )
              .map((eachView) => (
                <EachCard
                  color={
                    eachView.bookingStatus === "USERCANCELLED" ||
                    eachView.bookingStatus === "USERDENY" ||
                    eachView.bookingStatus === "PROFESSIONALCANCELLED "
                      ? "#ce1212"
                      : "#e97878"
                  }
                  id={eachView._id}
                  bookingID={eachView.bookingId}
                  status={eachView.bookingStatus}
                  paid={eachView.payment.paid}
                  bookingType={eachView.bookingType}
                  vehicleCategory={
                    categories.filter((category) => category._id === eachView.categoryId).length > 0
                      ? categories.filter((category) => category._id === eachView.categoryId)[0].name
                      : ""
                  }
                  city={
                    cities && cities.filter((city) => city._id === eachView.cityId).length > 0
                      ? cities.filter((city) => city._id === eachView.cityId)[0].locationName
                      : ""
                  }
                  distance={eachView.estimation.distance}
                  paymentType={eachView.payment.option}
                  amount={eachView.amount}
                  date={eachView.bookingDate}
                  currencyCode={eachView.currencySymbol}
                />
              ))}
          {views.filter(
            (eachView) =>
              eachView.bookingStatus === "EXPIRED" ||
              eachView.bookingStatus === "USERCANCELLED" ||
              eachView.bookingStatus === "PROFESSIONALCANCELLED" ||
              eachView.bookingStatus === "USERDENY"
          ).length === 0 && (
            <p className={"text-sm text-gray-500 text-center px-2"}>{language.NO_DATA_FOUND_IN_GIVEN_TIME}</p>
          )}
        </EachSide>
      </div>
    </div>
  ) : (
    <SmallLoader />
  );
}
