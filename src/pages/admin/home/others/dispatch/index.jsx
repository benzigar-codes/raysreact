import { Loader } from "@googlemaps/js-api-loader";
import React from "react";
import OutsideClick from "react-outside-click-handler";
import axios from "axios";
import { v4 as uuid } from "uuid";

// ICONS
import { FiCheck, FiMinus, FiPhone, FiPlus, FiRefreshCcw, FiX } from "react-icons/fi";
import { CgSandClock } from "react-icons/cg";
import carIcon from "../../../../../assets/images/car-icon.png";

// COMPONENTS
import BookRideTable from "../../../../../components/bookRideTable";
import { Button } from "../../../../../components/common/Button";
import { CountryCodesPicker } from "../../../../../components/common/CountryCodesPicker";
import { DropdownNormal } from "../../../../../components/common/DropDownNormal";
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import GooglePlaceComplete from "../../../../../components/common/GooglePlaceComplete";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import { TextField } from "../../../../../components/common/TextField";
import { ToggleButton } from "../../../../../components/common/ToggleButton";

// HOOKS
import useLanguage from "../../../../../hooks/useLanguage";
import useSettings from "../../../../../hooks/useSettings";
import useUtils from "../../../../../hooks/useUtils";
import useAdmin from "../../../../../hooks/useAdmin";
import useDebug from "../../../../../hooks/useDebug";

//JSON
import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import currency from "../../../../../utils/currency.json";
import CalenderDatePicker from "../../../../../components/common/CalenderDatePicker";
import ManualAssign from "../../../../../components/dispatch/ManualAssign";
import { PopUp } from "../../../../../components/common/PopUp";
import AwaitingContent from "../../../../../components/dispatch/AwaitingContent";
import { DropDownSearch } from "../../../../../components/common/DropDownSearch";

export default function Index() {
  // CUSTOM HOOKS
  const { header, authFailure, admin } = useAdmin();
  const { settings } = useSettings();
  const { parseError } = useUtils();
  const [expandFromParent, setExpandFromParent] = React.useState(false);
  const { language } = useLanguage();

  const GENDERFIELDS = [
    {
      id: 1,
      label: language.FEMALE,
      value: "FEMALE",
    },
    {
      id: 2,
      label: language.MALE,
      value: "MALE",
    },
    {
      id: 3,
      label: language.OTHER,
      value: "OTHER",
    },
  ];

  // REF
  const mapArea = React.useRef();

  // STATES
  const [popup, setPop] = React.useState(null);
  const [showAwaiting, setShowAwaiting] = React.useState(false);
  const [googleLoaded, setGoogleLoaded] = React.useState(false);
  const [showAssignDrivers, setShowAssignDrivers] = React.useState(false);
  const [selectableCategory, setSelectableCategory] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const [corporateCategoryIds, setCorporateCategoryIds] = React.useState([]);

  const [corporate, setCorporate] = React.useState({
    status: admin.userType === U.CORPORATE ? 1 : 0,
    id: "",
  });

  const [allCorporates, setAllCorporates] = React.useState([]);

  const [stops, setStops] = React.useState([]);

  const [guestUser, setGuestUser] = React.useState(true);
  const [noDrivers, setNoDrivers] = React.useState(null);
  const [pickUpOutside, setPickUpOutside] = React.useState(false);
  const [dropOutside, setDropOutside] = React.useState(false);
  const [categories, setCategories] = React.useState(null);
  const [tableFetch, fetchTable] = React.useState(0);
  const [manualAssign, setManualAssign] = React.useState(null);
  const [numberAcceptable, setNumberAcceptable] = React.useState(null);

  const [awaitingBookings, setAwaitingBookings] = React.useState([]);

  // MAP STUFF
  const [markers, setMarkers] = React.useState({
    pickUp: null,
    drop: null,
  });
  const [map, setMap] = React.useState(null);
  const [pickUp, setPickUp] = React.useState(null);
  const [drop, setDrop] = React.useState(null);
  const [directionData, setDirectionData] = React.useState(null);
  const [direction, setDirection] = React.useState(null);
  const [driverMarkers, setDriverMarkers] = React.useState([]);

  // LOADERS
  const [driverLoading, setDriverLoading] = React.useState(false);
  const [bookingLoading, setBookingLoading] = React.useState(false);

  const [newUser, setNewUser] = React.useState({
    firstName: null,
    lastName: null,
    email: null,
    gender: null,
  });

  const [bookingDetails, setBookingDetails] = React.useState({
    phoneCode: null,
    phoneNumber: null,
    bookingType: "INSTANT",
    paymentMethod: "CASH",
    category: null,
    date: new Date(new Date().setMinutes(new Date().getMinutes() + 70)),
    manualAssign: false,
    walletBook: false,
    time: new Date(new Date().setMinutes(new Date().getMinutes() + 70)),
  });

  const [user, setUser] = React.useState(null);
  const [drivers, setDrivers] = React.useState([]);

  const resetTable = () => {
    if (markers.pickUp !== null) markers.pickUp.setMap(null);
    if (markers.drop !== null) markers.drop.setMap(null);
    if (direction !== null) direction.setMap(null);
    setPop(null);
    setDriverLoading(false);
    setSelectableCategory([]);
    setGuestUser(true);
    setNoDrivers(null);
    setPickUpOutside(false);
    setDropOutside(false);
    setCategories(null);
    setUser(null);
    setDriverMarkers([]);
    setDirection(null);
    setDirectionData(null);
    setDrop(null);
    setPickUp(null);
    setMarkers({
      pickUp: null,
      drop: null,
    });
    setNewUser({
      firstName: null,
      lastName: null,
      email: null,
      gender: null,
    });
    setNumberAcceptable(null);
    setBookingDetails({
      phoneCode: null,
      phoneNumber: null,
      bookingType: "INSTANT",
      paymentMethod: "CASH",
      category: null,
      date: new Date(new Date().setMinutes(new Date().getMinutes() + 70)),
      manualAssign: false,
      walletBook: false,
      time: new Date(new Date().setMinutes(new Date().getMinutes() + 70)),
    });
  };

  const fetchUser = async () => {
    // setUserLoading(true);
    if (
      bookingDetails.phoneCode &&
      bookingDetails.phoneNumber &&
      bookingDetails.phoneNumber.toString().length > 4 &&
      bookingDetails.phoneNumber.toString().length < 16
    ) {
      try {
        setUser(null);
        const { data } = await axios.post(
          A.HOST + A.ADMIN_BOOK_CHECK_USER,
          {
            phoneCode: bookingDetails.phoneCode,
            phoneNumber: bookingDetails.phoneNumber,
          },
          header
        );
        if (data.code === 200) {
          if (data.data.bookingInfo.ongoingBooking === null) {
            setUser(data.data);
            setGuestUser(false);
            setNumberAcceptable(true);
          } else {
            // setPop({ title: language.NUMBER_ON_RIDE, type: "error" });
            setNumberAcceptable(language.NUMBER_ON_RIDE);
          }
        } else if (data.code === 101) {
          // setPop({ title: language.NUMBER_IN_ANOTHER_DIAL_CODE, type: "error" });
          setNumberAcceptable(language.NUMBER_IN_ANOTHER_DIAL_CODE);
        } else {
          setGuestUser(true);
          setNumberAcceptable(true);
        }
      } catch (err) {
        authFailure(err);
      }
    }
    // setForm1Loading(false);
  };

  const fetchCorporates = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_CORPORATE_LIST__ALL, {}, header);
      setAllCorporates(data);
      setLoading(false);
    } catch (err) {
      alert(err);
    }
  };

  const fetchCorporateIds = async () => {
    setLoading(true);
    if (admin.userType === U.CORPORATE || (corporate.status === 1 && corporate.id !== "")) {
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_CORPORATE_READ,
          {
            id: admin.userType === U.CORPORATE ? admin._id : corporate.id,
          },
          header
        );
        if (data && data.data.categoryIds && Array.isArray(data.data.categoryIds))
          setCorporateCategoryIds(data.data.categoryIds);
        else setCorporateCategoryIds([]);
        setLoading(false);
      } catch (err) {
        alert(err);
      }
    } else setCorporateCategoryIds([]);
  };

  React.useEffect(() => {
    if (googleLoaded === true) {
      fetchCorporates();
      if (admin.userType === U.CORPORATE) fetchCorporateIds();
      const googleMap = new window.google.maps.Map(mapArea.current, {
        center: U.mode === "pamworld" ? { lat: 9.476411, lng: 6.600669 } : { lat: 8.9956441, lng: 38.7816384 },
        zoom: 14,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: true,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: true,
      });
      setMap(googleMap);
    }
  }, [googleLoaded]);

  // React.useEffect(() => {
  //   if (user) setGuestUser(false);
  //   else setGuestUser(true);
  // }, [user]);

  const checkAvailableDrivers = async () => {
    setDriverLoading(true);
    setBookingDetails({ ...bookingDetails, category: null });
    try {
      driverMarkers.length > 0 && driverMarkers.map((each) => each.setMap(null));
      setSelectableCategory([]);
      const { data } = await axios.post(
        A.HOST + A.ADMIN_BOOK_DRIVERS_LIST,
        {
          lat: pickUp.results[0].geometry.location.lat,
          lng: pickUp.results[0].geometry.location.lng,
        },
        header
      );
      setDriverMarkers(
        data.map(
          (driver) =>
            new window.google.maps.Marker({
              icon: {
                url: carIcon,
                scaledSize: new window.google.maps.Size(35, 35),
              },
              position: {
                lat: driver.location.coordinates[1],
                lng: driver.location.coordinates[0],
              },
              map: map,
            })
        )
      );
      setDrivers(data);
      let driverAcceptableCategories = [];
      data.forEach((driver) => {
        driver.vehicles.forEach((vehicle) => {
          if (vehicle.defaultVehicle === true) {
            // driverAcceptableCategories.filter((each) => each === vehicle.vehicleCategoryId).length === 0 &&
            driverAcceptableCategories.push(vehicle.vehicleCategoryId);
            if (vehicle.isSubCategoryAvailable === true && vehicle.applySubCategory === true) {
              // driverAcceptableCategories.filter((each) => each.contains(vehicle.vehicleCategoryId)).length === 0 &&
              vehicle.subCategoryIds.map((each) => driverAcceptableCategories.push(each));
            }
          }
        });
      });
      driverAcceptableCategories = [...new Set(driverAcceptableCategories)];
      // if (corporateCategoryIds)
      //   driverAcceptableCategories = driverAcceptableCategories.filter((each) =>
      //     corporateCategoryIds.length === 0 ? true : corporateCategoryIds.includes(each)
      //   );
      // driverAcceptableCategories.filter(each =>)
      setSelectableCategory(driverAcceptableCategories);
      setPickUpOutside(false);
      setDropOutside(false);
      setDriverLoading(false);
      if (driverAcceptableCategories.length > 0) setNoDrivers(false);
      else setNoDrivers(true);
      return true;
    } catch (err) {
      authFailure(err);
      setNoDrivers(true);
      setDriverLoading(false);
      return false;
    }
  };

  React.useEffect(
    () =>
      directionData !== null &&
      pickUp !== null &&
      drop !== null &&
      (noDrivers === false || bookingDetails.bookingType === "SCHEDULE") &&
      fetchCategories(),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [directionData, bookingDetails.bookingType, pickUp, drop, stops, corporate.status, corporate.id]
  );

  // React.useEffect(() => {
  //   if (categories && categories.categories && Array.isArray(categories.categories)) {
  //     setCategories({
  //       ...categories,
  //       categories: categories.categories.filter((each) =>
  //         corporateCategoryIds.length === 0 ? true : corporateCategoryIds.includes(each.categoryID)
  //       ),
  //     });
  //   }
  // }, [corporateCategoryIds]);

  // React.useEffect(() => alert(JSON.stringify(bookingDetails, null, 2)), [bookingDetails]);
  // React.useEffect(() => alert(JSON.stringify(categories, null, 2)), [categories]);
  // React.useEffect(() => alert(JSON.stringify(corporateCategoryIds, null, 2)), [corporateCategoryIds]);
  // React.useEffect(() => alert(JSON.stringify(corporate, null, 2)), [corporate]);

  const fetchCategories = async () => {
    // setTopBtnLoading(true);
    await fetchCorporateIds();
    // if (bookingDetails.bookingType === "INSTANT" && selectableCategory.length > 0) {
    //   setBookingDetails({ ...bookingDetails, category: selectableCategory[0] });
    // } else if (categories && categories.categories) {
    //   setBookingDetails({ ...bookingDetails, category: categories.categories[0].categoryID, manualAssign: false });
    // }
    setBookingDetails({ ...bookingDetails, category: null, manualAssign: false });
    // bookingDetails.bookingType === "INSTANT" && selectableCategory.length > 0
    //   ? setBookingDetails({ ...bookingDetails, category: selectableCategory[0] })
    //   : categories &&
    //     categories.categories &&
    //     setBookingDetails({ ...bookingDetails, category: categories.categories[0].categoryID, manualAssign: false });
    const driversAvailable = await checkAvailableDrivers();
    if (driversAvailable || bookingDetails.bookingType === U.SCHEDULE) {
      let bookingDate = new Date();
      // if (bookingDetails.bookingType === U.SCHEDULE) {
      //   const timeString = new Date(bookingDetails.time).getHours() + ":" + new Date(selectedTime).getMinutes() + ":00";
      //   var year = selectedDate.getFullYear();
      //   var month = selectedDate.getMonth() + 1; // Jan is 0, dec is 11
      //   var day = selectedDate.getDate();
      //   var dateString = "" + year + "-" + month + "-" + day;
      //   bookingDate = new Date(dateString + " " + timeString);
      // }
      try {
        const dataToSend = {
          userType: admin.userType === U.CORPORATE || corporate.status === 1 ? U.CORPORATE : "ALL",
          pickupLat: pickUp.results[0].geometry.location.lat,
          pickupLng: pickUp.results[0].geometry.location.lng,
          dropLat: drop.results[0].geometry.location.lat,
          dropLng: drop.results[0].geometry.location.lng,
          estimationDistance: directionData.routes[0].legs.map((each) => each.distance.value).reduce((a, b) => a + b),
          estimationTime: directionData.routes[0].legs.map((each) => each.duration.value).reduce((a, b) => a + b),
          bookingDate: bookingDate,
          bookingType: bookingDetails.bookingType,
        };
        const { data } = await axios.post(A.HOST + A.ADMIN_BOOK_CATEGORIES_GET, dataToSend, header);
        if (data.status === "CATEGORYAVAILABLE") {
          // data.data.categories = data.data.categories.filter((each) =>
          //   corporateCategoryIds.length === 0 ? true : corporateCategoryIds.includes(each.categoryID)
          // );
          setCategories(data.data);
          // let defaultCategory =
          //   data.data.categories.filter((category) => category.defaultCategory === true).length > 0
          //     ? data.data.categories.filter((category) => category.defaultCategory === true)[0].categoryID
          //     : null;
          // if (defaultCategory === null) {
          //   defaultCategory =
          //     bookingDetails.bookingType === U.INSTANT && drivers.length > 0
          //       ? drivers[0].vehicles.filter((vehicle) => vehicle.defaultVehicle === true)[0].vehicleCategoryId
          //       : bookingDetails.bookingType === U.SCHEDULE
          //       ? data.data.categories[0].categoryID
          //       : null;
          // }
          // setSelectedCategory(defaultCategory);
        } else if (data.status === "PICKUPLOCATIONOUT") {
          setPickUpOutside(true);
        } else if (data.status === "DROPLOCATIONOUT") {
          setDropOutside(true);
        } else setNoDrivers(true);
      } catch (err) {
        alert(err);
      }
    }
    // setTopBtnLoading(false);
  };

  React.useEffect(() => {
    const pickUpFunc = async () => {
      if (googleLoaded === true && pickUp !== null) {
        driverMarkers.length > 0 && driverMarkers.forEach((each) => each.setMap(null));
        setNoDrivers(false);
        setPickUpOutside(false);
        setDropOutside(false);
        checkAvailableDrivers();
        if (markers.pickUp !== null) markers.pickUp.setMap(null);
        const pickUpMarker = new window.google.maps.Marker({
          position: { lat: pickUp.results[0].geometry.location.lat, lng: pickUp.results[0].geometry.location.lng },
          map: map,
        });
        map.setCenter({ lat: pickUp.results[0].geometry.location.lat, lng: pickUp.results[0].geometry.location.lng });
        setMarkers({ ...markers, pickUp: pickUpMarker });
      }
    };
    pickUpFunc();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [googleLoaded, pickUp]);

  React.useEffect(() => {
    if (googleLoaded === true && drop !== null) {
      if (markers.drop !== null) markers.drop.setMap(null);
      const dropMarker = new window.google.maps.Marker({
        position: { lat: drop.results[0].geometry.location.lat, lng: drop.results[0].geometry.location.lng },
        map: map,
      });
      setMarkers({ ...markers, drop: dropMarker });
    }
  }, [googleLoaded, drop]);

  React.useEffect(() => {
    const check = async () => {
      if (pickUp !== null && drop !== null) {
        if (markers.pickUp !== null) markers.pickUp.setMap(null);
        if (markers.drop !== null) markers.drop.setMap(null);
        if (direction !== null) direction.setMap(null);

        //MARKER
        const coordinatesForPath = [
          { lat: pickUp.results[0].geometry.location.lat, lng: pickUp.results[0].geometry.location.lng },
          { lat: drop.results[0].geometry.location.lat, lng: drop.results[0].geometry.location.lng },
        ];
        var bounds = new window.google.maps.LatLngBounds();
        coordinatesForPath.forEach((poly) => bounds.extend(poly));
        map.fitBounds(bounds);

        //CREATING DIRECTIONS
        const directionsService = new window.google.maps.DirectionsService();
        const directionsRenderer = new window.google.maps.DirectionsRenderer();
        var request = {
          origin: { lat: pickUp.results[0].geometry.location.lat, lng: pickUp.results[0].geometry.location.lng },
          destination: { lat: drop.results[0].geometry.location.lat, lng: drop.results[0].geometry.location.lng },
          travelMode: "DRIVING",
        };
        if (stops.filter((stop) => stop.updated === true).length > 0) {
          request["waypoints"] = stops
            .filter((stop) => stop.updated === true)
            .map((stop) => ({
              location: new window.google.maps.LatLng(stop.lat, stop.lng),
              stopover: true,
            }));
        }
        directionsService.route(request, function (result, status) {
          if (status === "OK") {
            directionsRenderer.setDirections(result);
            setDirectionData(result);
          }
        });

        directionsRenderer.setMap(map);
        setDirection(directionsRenderer);
      }
    };
    check();
  }, [pickUp, drop, stops]);

  React.useEffect(() => {
    const loader = new Loader({
      apiKey: settings.mapApi.web,
      version: "weekly",
      libraries: ["places"],
    });
    loader
      .load()
      .then(() => {
        setGoogleLoaded(true);
      })
      .catch((e) => {});
  }, []);

  const bookRide = async () => {
    if (corporate.status === 1 && admin.userType !== U.CORPORATE && corporate.id === "")
      return alert(language.CORPORATE_IS_EMPTY);
    let driverAvailable = true;
    let bookUser = user;
    setBookingLoading(true);
    const emailRegex =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (user === null && guestUser === false) {
      if (newUser.firstName && newUser.lastName && newUser.email && newUser.gender) {
        if (emailRegex.test(String(newUser.email).toLowerCase())) {
          try {
            const { data } = await axios.post(
              A.HOST + A.ADMIN_BOOK_CREATE_USER,
              {
                phoneCode: bookingDetails.phoneCode,
                phoneNumber: bookingDetails.phoneNumber,
                firstName: newUser.firstName,
                lastName: newUser.lastName,
                email: newUser.email,
                gender: newUser.gender,
              },
              header
            );
            if (data.code === 200) {
              setUser(data.data);
              bookUser = data.data;
            } else alert(language.ACCOUNT_ALREADY_EXISTS);
          } catch (err) {
            authFailure(err);
          }
        } else {
          alert("Enter a Valid Email");
        }
      } else {
        alert("Fields are Missing");
        return;
      }
    }
    if (pickUp === null || drop === null || categories === null || (bookUser === null && guestUser === false))
      return alert("Complete Previous Steps Correctly");
    let apiLink = bookingDetails.bookingType === U.INSTANT ? A.ADMIN_BOOK_RIDE_INSTANT : A.ADMIN_BOOK_RIDE_SCHEDULE;
    if (bookingDetails.bookingType === U.INSTANT) {
      driverAvailable = await checkAvailableDrivers();
    }
    if (driverAvailable) {
      let bookingDate = new Date();
      if (bookingDetails.bookingType === U.SCHEDULE) {
        bookingDate = bookingDetails.date;
        // const timeString =
        //   new Date(bookingDetails.time).getHours() + ":" + new Date(bookingDetails.time).getMinutes() + ":00";
        // var year = bookingDetails.date.getFullYear();
        // var month = bookingDetails.date.getMonth() + 1; // Jan is 0, dec is 11
        // var day = bookingDetails.date.getDate();
        // var dateString = "" + year + "-" + month + "-" + day;
        // bookingDate = new Date(dateString + " " + timeString);
      }
      let category = null;
      if (categories.categories.filter((each) => each.categoryID === bookingDetails.category).length > 0) {
        category = categories.categories.filter((each) => each.categoryID === bookingDetails.category)[0];
      } else {
        alert(language.COMPLETE_PREVIOUS_STEP);
        // setPage(6);
      }
      const bookingLocations = [];
      bookingLocations.push({
        addressName: pickUp.selectedAddress,
        fullAddress: pickUp.selectedAddress,
        shortAddress: pickUp.selectedAddress,
        lat: pickUp.results[0].geometry.location.lat,
        lng: pickUp.results[0].geometry.location.lng,
        type: "ORIGIN",
      });
      if (stops.filter((stop) => stop.updated === true).length > 0)
        stops
          .filter((stop) => stop.updated === true)
          .map((stop) =>
            bookingLocations.push({
              addressName: stop.address,
              fullAddress: stop.address,
              shortAddress: stop.address,
              lat: stop.lat,
              lng: stop.lng,
              type: "STOP",
            })
          );
      bookingLocations.push({
        addressName: drop.selectedAddress,
        fullAddress: drop.selectedAddress,
        shortAddress: drop.selectedAddress,
        lat: drop.results[0].geometry.location.lat,
        lng: drop.results[0].geometry.location.lng,
        type: "DESTINATION",
      });
      try {
        let apiData = {
          categoryId: categories.serviceAreaID,
          bookingDate,
          pickUpAddressName: pickUp.selectedAddress,
          paymentOption: bookingDetails.walletBook ? "WALLET" : "CASH",
          dropShortAddress: drop.selectedAddress,
          pickUpLat: pickUp.results[0].geometry.location.lat,
          estimationDistance: directionData.routes[0].legs.map((each) => each.distance.value).reduce((a, b) => a + b),
          bookingLocations: bookingLocations,
          dropFullAddress: drop.selectedAddress,
          dropAddressName: drop.selectedAddress,
          pickUpFullAddress: pickUp.selectedAddress,
          dropLng: drop.results[0].geometry.location.lng,
          estimationTime: directionData.routes[0].legs.map((each) => each.duration.value).reduce((a, b) => a + b),
          estimationAmount: category.cost,
          bookingFor: {
            name: guestUser ? "GUEST" : bookUser === null ? "GUEST" : bookUser.firstName + " " + bookUser.lastName,
            phoneCode: guestUser
              ? bookingDetails.phoneCode
              : bookUser === null
              ? bookingDetails.phoneCode
              : bookUser.phone.code,
            phoneNumber: guestUser
              ? bookingDetails.phoneNumber
              : bookUser === null
              ? bookingDetails.phoneNumber
              : bookUser.phone.number,
          },
          isNotify: bookingDetails.manualAssign ? false : true,
          dropLat: drop.results[0].geometry.location.lat,
          serviceTax: category.serviceTax,
          pickUpShortAddress: pickUp.selectedAddress,
          pickUpLng: pickUp.results[0].geometry.location.lng,
          siteCommisionValue: category.siteCommisionValue,
          travelCharge: category.travelCharge,
          timeFare: category.timeFare,
          distanceFare: category.distanceFare,
          vehicleCategoryId: bookingDetails.category,
          userId: guestUser ? "" : bookUser === null ? "" : bookUser?._id,
        };
        if (corporate.status === 1) {
          apiData["corperateId"] = corporate.id;
          apiData["paymentOption"] = "CREDIT";
          apiLink =
            bookingDetails.bookingType === U.INSTANT
              ? A.ADMIN_BOOK_RIDE_CORPORATE_INSTANT
              : A.ADMIN_BOOK_RIDE_CORPORATE_SCHEDULE;
        }
        const { data } = await axios.post(A.HOST + apiLink, apiData, header);
        if (data.code !== 200) {
          // setPage(6);
        } else {
          if (bookingDetails.bookingType === "SCHEDULE") setPop({ title: language.SCHEDULE_PLACED, type: "success" });
          if (bookingDetails.manualAssign) setManualAssign(data.data._id);
          // else window.open(NavLinks.ADMIN_BOOK_RIDE_ACCEPT_DETAILS + "/" + data.data._id, "", "height=500,width=500");
          if (bookingDetails.manualAssign === false && bookingDetails.bookingType === "INSTANT") {
            setShowAwaiting(true);
            setAwaitingBookings([...awaitingBookings, data.data._id]);
          }
          fetchTable((table) => table + 1);
          // resetTable();
          // bookAnother();
        }
      } catch (err) {
        // fetchUser();
        if (err.response.data?.message === "USER ALREADY HAVE RIDE")
          setPop({ title: language.NUMBER_ON_RIDE, type: "error" });

        // console.log(err);
      }
    }
    setBookingLoading(false);
  };

  React.useEffect(() => {
    console.log(guestUser);
  }, [guestUser]);

  const retryRide = async (obj) => {
    if (obj.bookingStatus === "EXPIRED") {
      try {
        await axios.post(
          A.HOST + A.ADMIN_BOOK_RETRY_AWAITING,
          {
            bookingId: obj.id,
          },
          header
        );
        setAwaitingBookings([...awaitingBookings, obj.id]);
        fetchTable((data) => data + 1);
        setExpandFromParent(true);
        setExpandFromParent(false);
      } catch (err) {
        alert(parseError(err) === "NO PROFESSIONAL FOUND" ? language.NO_PROFESSIONALS_AVAILABLE : err);
      }
    }
    if (obj.bookingStatus === "AWAITING") {
      try {
        await axios.post(
          A.HOST + A.ADMIN_BOOK_RETRY,
          {
            bookingId: obj.id,
          },
          header
        );
        setAwaitingBookings([...awaitingBookings, obj.id]);
        fetchTable((data) => data + 1);
        setExpandFromParent(true);
        setExpandFromParent(false);
      } catch (err) {
        alert(parseError(err) === "NO PROFESSIONAL FOUND" ? language.NO_PROFESSIONALS_AVAILABLE : err);
      }
    }
  };

  const phoneNumberValid =
    bookingDetails.phoneCode &&
    bookingDetails.phoneNumber &&
    bookingDetails.phoneNumber.toString().length > 4 &&
    bookingDetails.phoneNumber.toString().length < 15;

  // eslint-disable-next-line react-hooks/exhaustive-deps
  React.useEffect(() => fetchUser(), [bookingDetails.phoneCode, bookingDetails.phoneNumber]);

  const CategoriesDropDown = () => (
    <FieldWrapper>
      <DropdownNormal
        change={(e) => setBookingDetails({ ...bookingDetails, category: e })}
        defaultValue={
          categories.categories &&
          categories.categories.filter((each) => each.categoryID === bookingDetails.category).length > 0
            ? `${categories.categories.find((each) => each.categoryID === bookingDetails.category).categoryName} - ${
                categories.currencySymbol
              } ${categories.categories.find((each) => each.categoryID === bookingDetails.category).cost}`
            : ""
        }
        selectText={language.VEHICLE_CATEGORY}
        fields={
          bookingDetails.bookingType === "SCHEDULE"
            ? categories.categories
                .filter((each) =>
                  corporateCategoryIds.length === 0 ? true : corporateCategoryIds.includes(each.categoryID)
                )
                .map((category, idx) => ({
                  id: idx,
                  label: `${category.categoryName} - ${categories.currencySymbol} ${category.cost}`,
                  value: category.categoryID,
                }))
            : selectableCategory
                .filter((each) => (corporateCategoryIds.length === 0 ? true : corporateCategoryIds.includes(each)))
                .filter((each) =>
                  categories &&
                  categories.categories &&
                  categories.categories.filter((category) => category.categoryID === each).length > 0
                    ? true
                    : false
                )
                .map((each, idx) => ({
                  id: idx,
                  label:
                    categories.categories.filter((cate) => cate.categoryID === each).length > 0
                      ? categories.categories.find((cate) => cate.categoryID === each).categoryName +
                        " - " +
                        categories.currencySymbol +
                        " " +
                        categories.categories.find((cate) => cate.categoryID === each).cost
                      : "",
                  value: each,
                }))
        }
      />
    </FieldWrapper>
  );
  return googleLoaded === false ? (
    <SmallLoader />
  ) : (
    <>
      {manualAssign && (
        <ManualAssign
          completedBookingId={(e) => {
            setAwaitingBookings(awaitingBookings.filter((each) => each !== e));
            fetchTable((table) => table + 1);
          }}
          manualAssign={manualAssign}
          fetchTable={fetchTable}
          setManualAssign={setManualAssign}
        />
      )}
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <div className="flex">
        <div className="w-1/3 px-2 py-1 dark:text-white h-screen">
          <div className="flex justify-between items-center">
            <h1 className="my-2 mx-4 text-2xl dark:text-white">{language.BOOK_RIDE}</h1>
            <div className={`relative z-10`}>
              {/* {awaitingBookings.length > 0 && (
                <div
                  className="h-3 w-3 bg-white bg-red-500 rounded-full absolute"
                  style={{ zIndex: 10, top: -10, right: -10 }}
                ></div>
              )}
              {awaitingBookings.length > 0 && (
                <div
                  className="animate-ping h-3 w-3 bg-red-500 rounded-full absolute"
                  style={{ zIndex: 10, top: -10, right: -10 }}
                ></div>
              )} */}
              <CgSandClock
                onClick={() => setShowAwaiting(true)}
                className={`${
                  awaitingBookings.length > 0 ? "bg-red-500 animate-spin" : "bg-blue-800"
                } rounded-full text-4xl p-2 cursor-pointer text-gray-100 dark:text-white`}
              />
              {awaitingBookings.length > 0 && (
                <CgSandClock
                  onClick={() => setShowAwaiting(true)}
                  className={`${
                    awaitingBookings.length > 0 ? "animate-ping bg-red-500" : "bg-blue-800"
                  } rounded-full text-4xl p-2 cursor-pointer absolute top-0 text-gray-100 dark:text-white`}
                />
              )}
              <OutsideClick
                onOutsideClick={() => {
                  showAssignDrivers === false && setShowAwaiting(false);
                }}
              >
                <div
                  className="shadow-xl transition-all duration-300 rounded-xl top-0 absolute bg-white overflow-y-scroll"
                  style={{
                    zIndex: 15,
                    height: 300,
                    width: 600,
                    left: showAwaiting ? "100%" : "0%",
                    top: showAwaiting ? 0 : "-100vh",
                  }}
                >
                  {awaitingBookings.length === 0 && (
                    <p className="text-sm text-center text-gray-500" style={{ marginTop: 20 }}>
                      {language.NO_BOOKINGS_TO_SHOW}
                    </p>
                  )}
                  {awaitingBookings.map((each) => (
                    <AwaitingContent
                      completed={(e) => {
                        // alert(e);
                        console.log(e);
                        setAwaitingBookings(awaitingBookings.filter((each) => each !== e));
                        fetchTable((table) => table + 1);
                      }}
                      setManualAssign={setManualAssign}
                      id={each}
                    />
                  ))}
                  {/* <Content type="timer" />
                    <Content type="timer" />
                    <Content setShowAssignDrivers={setShowAssignDrivers} />
                    <Content type="timer" />
                    <Content setShowAssignDrivers={setShowAssignDrivers} />
                    <Content setShowAssignDrivers={setShowAssignDrivers} />
                    <Content type="timer" />
                    <Content setShowAssignDrivers={setShowAssignDrivers} /> */}
                </div>
              </OutsideClick>
            </div>
          </div>
          <div className="overflow-y-scroll" style={{ height: "90%" }}>
            {admin.userType !== U.CORPORATE && (
              <div className="px-2 flex items-center">
                <div className="w-1/2">
                  <div className="flex items-center">
                    <p className="mx-2 text-sm">{language.CORPORATE}</p>
                    <ToggleButton
                      change={(e) => setCorporate({ id: "", status: e })}
                      defaultValue={corporate.status ? 1 : 0}
                    />
                  </div>
                </div>
                {corporate.status === 1 && (
                  <div className="w-1/2">
                    <DropDownSearch
                      // error={formik.errors.currencyCode}
                      placeholder={language.CORPORATE}
                      change={(e) => {
                        setCorporate({ ...corporate, id: e });
                      }}
                      defaultValue={corporate.id}
                      fields={allCorporates.map((each, i) => ({
                        id: i,
                        label: each.officeName,
                        value: each._id,
                      }))}
                    />
                  </div>
                )}
              </div>
            )}
            <FieldWrapper>
              <CountryCodesPicker
                change={(e) => setBookingDetails({ ...bookingDetails, phoneCode: e })}
                defaultValue={bookingDetails.phoneCode ?? ""}
                // error={formik1.errors.phoneCode}
                placeholder={language.DIAL_CODE}
                margin={3}
                width="4/12"
              />
              <TextField
                change={(e) => setBookingDetails({ ...bookingDetails, phoneNumber: e })}
                value={bookingDetails.phoneNumber}
                // value={bookingDetails.phoneNumber}
                // error={formik1.errors.phoneNumber}
                placeholder={language.PHONE}
                width="8/12"
                type="number"
                icon={<FiPhone />}
              />
            </FieldWrapper>
            {user && numberAcceptable === true && (
              <FieldWrapper>
                <div className="flex justify-between items-center">
                  <p className="mx-2 text-sm flex items-center">
                    <FiCheck style={{ color: "green" }} />
                    <p className="mx-2">{language.USER_DATA_FOUND}</p>
                  </p>
                </div>
              </FieldWrapper>
            )}
            {user === null && numberAcceptable === true && phoneNumberValid && (
              <FieldWrapper>
                <div className="flex justify-between items-center">
                  <p className="mx-2 text-sm">{language.GUEST_USER}</p>
                  <ToggleButton change={(e) => setGuestUser(e === 1 ? true : false)} defaultValue={guestUser ? 1 : 0} />
                </div>
              </FieldWrapper>
            )}
            {guestUser === false && numberAcceptable === true && user === null && (
              <>
                <FieldWrapper>
                  <TextField
                    change={(e) => user === null && setNewUser({ ...newUser, firstName: e })}
                    value={newUser.firstName}
                    width="2/4"
                    type="text"
                    margin={3}
                    placeholder={language.FIRSTNAME}
                  />
                  <TextField
                    change={(e) => user === null && setNewUser({ ...newUser, lastName: e })}
                    value={newUser.lastName}
                    width="2/4"
                    type="text"
                    placeholder={language.LASTNAME}
                  />
                </FieldWrapper>
                <FieldWrapper>
                  <div className="w-2/4 mr-2">
                    <TextField
                      change={(e) => user === null && setNewUser({ ...newUser, email: e })}
                      value={newUser.email}
                      placeholder={language.EMAIL}
                    />
                  </div>
                  <div className="w-2/4">
                    <DropdownNormal
                      // change={(e) => formik2.setFieldValue("gender", e)}
                      // error={formik2.errors.gender}
                      defaultValue={
                        GENDERFIELDS.find((each) => each.value === newUser.gender)
                          ? GENDERFIELDS.find((each) => each.value === newUser.gender).label
                          : ""
                      }
                      change={(e) => user === null && setNewUser({ ...newUser, gender: e })}
                      selectText={language.GENDER}
                      fields={GENDERFIELDS}
                    />
                  </div>
                </FieldWrapper>
              </>
            )}
            {phoneNumberValid && numberAcceptable === true && (
              <>
                <FieldWrapper title={language.PICKUP_LOCATION}>
                  <div className="w-2/4 w-full mr-2">
                    <GooglePlaceComplete
                      // clearText={clearText}
                      change={(e) => setPickUp(e)}
                      placeholder={language.PICKUP_LOCATION}
                    />
                  </div>
                </FieldWrapper>
                {stops.length > 0 && <p className="mx-4 text-sm mt-4">{language.STOPS}</p>}
                {stops.map((each, idx) => (
                  <div className="flex m-3 items-center">
                    <GooglePlaceComplete
                      bounds={
                        pickUp
                          ? {
                              lat: pickUp.results[0].geometry.location.lat,
                              lng: pickUp.results[0].geometry.location.lng,
                            }
                          : false
                      }
                      defaultValue={each.address}
                      // clearText={clearText}
                      change={(e) =>
                        setStops([
                          ...stops.filter((stop) => stop.id !== each.id),
                          {
                            id: each.id,
                            lat: e.results[0].geometry.location.lat,
                            lng: e.results[0].geometry.location.lng,
                            address: e.selectedAddress,
                            updated: true,
                          },
                        ])
                      }
                      placeholder={language.STOP + " " + (idx + 1)}
                    />
                    {/* <FiX
                      onClick={() => setStops(stops.filter((stop) => stop.id !== each.id))}
                      className="text-4xl mx-3 bg-red-500 cursor-pointer p-2 rounded-full"
                    /> */}
                  </div>
                ))}
                <div className="flex m-2 items-center">
                  {stops.length < settings.stopLimit && (
                    <div>
                      <Button
                        icon={<FiPlus />}
                        onClick={() =>
                          setStops([...stops, { id: uuid(), address: "", lat: 0, lng: 0, updated: false }])
                        }
                        title={language.ADD_STOPS}
                      />
                    </div>
                  )}
                  {stops.length > 0 && (
                    <div className="mx-2">
                      <Button
                        icon={<FiMinus />}
                        onClick={() => setStops([...stops.filter((each, idx) => stops.length !== idx + 1)])}
                        title={language.REMOVE_STOPS}
                      />
                    </div>
                  )}
                </div>
                <FieldWrapper title={language.DROP_LOCATION}>
                  <div className="mr-2 w-full">
                    <GooglePlaceComplete
                      bounds={
                        pickUp
                          ? {
                              lat: pickUp.results[0].geometry.location.lat,
                              lng: pickUp.results[0].geometry.location.lng,
                            }
                          : false
                      }
                      // clearText={clearText}
                      change={(e) => setDrop(e)}
                      placeholder={language.DROP_LOCATION}
                    />
                  </div>
                </FieldWrapper>
              </>
            )}
            {phoneNumberValid &&
              numberAcceptable === true &&
              pickUp &&
              drop &&
              pickUpOutside === false &&
              // categories &&
              stops.filter((each) => each.updated === false).length === 0 &&
              dropOutside === false && (
                <FieldWrapper>
                  <div className="flex justify-between items-center">
                    <p className="mx-2 text-sm">{language.SCHEDULE}</p>
                    <ToggleButton
                      change={(e) =>
                        setBookingDetails({ ...bookingDetails, bookingType: e === 1 ? "SCHEDULE" : "INSTANT" })
                      }
                      defaultValue={bookingDetails.bookingType === "SCHEDULE" ? 1 : 0}
                    />
                  </div>
                  {bookingDetails.bookingType === "SCHEDULE" && (
                    <>
                      <div className="px-2 w-full">
                        <CalenderDatePicker
                          startTime={new Date(new Date().setMinutes(new Date().getMinutes() + 70))}
                          afterDays={5}
                          showTime={true}
                          disableBeforeDays={true}
                          change={(e) => setBookingDetails({ ...bookingDetails, date: e })}
                          defaultValue={bookingDetails.date}
                        />
                      </div>
                      {/* <div className="px-2 w-full">
                      <TimePicker
                        // className={"w-full dark:text-white dark:bg-gray-900 border-2"}
                        change={(e) => setBookingDetails({ ...bookingDetails, time: e })}
                        defaultValue={bookingDetails.time}
                      />
                    </div> */}
                    </>
                  )}
                </FieldWrapper>
              )}
            {noDrivers === false &&
              pickUpOutside === false &&
              dropOutside === false &&
              numberAcceptable === true &&
              stops.filter((each) => each.updated === false).length === 0 &&
              bookingDetails.bookingType === "INSTANT" && (
                <div className="flex justify-center items-center" style={{ marginTop: 20 }}>
                  <p className="text-sm text-center text-gray-500">{language.REFRESH_VEHICLE_CATEGORIES}</p>
                  <FiRefreshCcw
                    onClick={checkAvailableDrivers}
                    className={`${
                      driverLoading && "animate-spin"
                    } mx-2 cursor-pointer text-3xl bg-blue-800 text-white rounded-full p-2`}
                  />
                </div>
              )}
            {phoneNumberValid &&
              numberAcceptable === true &&
              dropOutside === false &&
              pickUpOutside === false &&
              categories &&
              bookingDetails.category !== null &&
              stops.filter((each) => each.updated === false).length === 0 &&
              (bookingDetails.bookingType === "SCHEDULE" ||
                selectableCategory.filter((each) =>
                  corporateCategoryIds.length === 0 ? true : corporateCategoryIds.includes(each)
                ).length > 0) &&
              (noDrivers === false || bookingDetails.bookingType === "SCHEDULE") && <CategoriesDropDown />}
            {phoneNumberValid &&
              numberAcceptable === true &&
              dropOutside === false &&
              pickUpOutside === false &&
              categories &&
              bookingDetails.category === null &&
              stops.filter((each) => each.updated === false).length === 0 &&
              (bookingDetails.bookingType === "SCHEDULE" ||
                selectableCategory.filter((each) =>
                  corporateCategoryIds.length === 0 ? true : corporateCategoryIds.includes(each)
                ).length > 0) &&
              (noDrivers === false || bookingDetails.bookingType === "SCHEDULE") && <CategoriesDropDown />}
            {pickUpOutside && numberAcceptable === true && (
              <p className="text-sm text-center text-gray-500" style={{ marginTop: 20 }}>
                {language.PICKUP_OUTSIDE}
              </p>
            )}
            {dropOutside && numberAcceptable === true && (
              <p className="text-sm text-center text-gray-500" style={{ marginTop: 20 }}>
                {language.DROP_OUTSIDE}
              </p>
            )}
            {noDrivers && numberAcceptable === true && bookingDetails.bookingType === "INSTANT" && (
              <div className="flex justify-center items-center" style={{ marginTop: 20 }}>
                <p className="text-sm text-center text-gray-500">{language.NO_PROFESSIONAL_AVAILABLE}</p>
                <FiRefreshCcw
                  onClick={checkAvailableDrivers}
                  className={`${
                    driverLoading && "animate-spin"
                  } mx-2 cursor-pointer text-3xl bg-blue-800 text-white rounded-full p-2`}
                />
              </div>
            )}
            {phoneNumberValid &&
              dropOutside === false &&
              pickUpOutside === false &&
              numberAcceptable === true &&
              stops.filter((each) => each.updated === false).length === 0 &&
              bookingDetails.category &&
              (noDrivers === false || bookingDetails.bookingType === "SCHEDULE") && (
                <div className="px-3 mt-3 flex justify-between">
                  {corporate.status === 0 &&
                    user &&
                    categories &&
                    categories.categories.filter((category) => category.categoryID === bookingDetails.category).length >
                      0 &&
                    user.wallet.availableAmount >=
                      parseInt(
                        categories.categories.find((category) => category.categoryID === bookingDetails.category).cost
                      ) && (
                      <div className="flex justify-between items-center">
                        <p className="mx-2 text-sm">{language.BOOK_FROM_WALLET}</p>
                        <ToggleButton
                          change={(e) => setBookingDetails({ ...bookingDetails, walletBook: e === 1 ? true : false })}
                          defaultValue={bookingDetails.walletBook === true ? 1 : 0}
                        />
                      </div>
                    )}
                  {bookingDetails.bookingType !== "SCHEDULE" && (
                    <div className="flex justify-between items-center">
                      <p className="mx-2 text-sm">{language.MANUAL_ASSIGN_DRIVERS}</p>
                      <ToggleButton
                        change={(e) => setBookingDetails({ ...bookingDetails, manualAssign: e === 1 ? true : false })}
                        defaultValue={bookingDetails.manualAssign === true ? 1 : 0}
                      />
                    </div>
                  )}
                </div>
              )}
            {numberAcceptable !== true && (
              <p className="text-sm text-center text-gray-500" style={{ marginTop: 20 }}>
                {numberAcceptable}
              </p>
            )}
            {directionData && (
              <p className="m-5 text-sm">
                {language.DISTANCE} :{" "}
                {parseFloat(
                  directionData.routes[0].legs.map((each) => each.distance.value).reduce((a, b) => a + b) / 1000
                ).toFixed(2) + " KM"}
              </p>
            )}
            {phoneNumberValid &&
            dropOutside === false &&
            numberAcceptable === true &&
            stops.filter((each) => each.updated === false).length === 0 &&
            pickUpOutside === false &&
            bookingDetails.category &&
            (noDrivers === false || bookingDetails.bookingType === "SCHEDULE") ? (
              <FieldWrapper>
                <Button
                  click={() => (bookingLoading ? null : bookRide())}
                  loading={bookingLoading}
                  title={language.BOOK_RIDE}
                />
              </FieldWrapper>
            ) : noDrivers || numberAcceptable !== true || pickUpOutside || dropOutside ? null : (
              <p className="text-sm text-center text-gray-500" style={{ marginTop: 20 }}>
                {language.FILL_ABOVE_STEP_TO_BOOK_RIDE}
              </p>
            )}
            <div className="my-5" style={{ marginBottom: 100 }}></div>
          </div>
        </div>
        <div ref={mapArea} className="bg-white ml-4 w-2/3 h-screen"></div>
      </div>
      <BookRideTable
        expandFromParent={expandFromParent}
        setManualAssign={setManualAssign}
        tableFetch={tableFetch}
        retryRide={retryRide}
      />
    </>
  );
}
