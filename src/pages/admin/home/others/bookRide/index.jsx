import React from "react";
import {
  FiArrowUp,
  FiCheck,
  FiInfo,
  FiLoader,
  FiMap,
  FiPhone,
  FiRefreshCw,
  FiSearch,
  FiUser,
  FiUsers,
  FiX,
} from "react-icons/fi";
import { BiCar } from "react-icons/bi";
import { FcApproval, FcClock, FcHighPriority, FcMoneyTransfer, FcPackage, FcSportsMode } from "react-icons/fc";
import * as yup from "yup";
import { Button } from "../../../../../components/common/Button";
import { CountryCodesPicker } from "../../../../../components/common/CountryCodesPicker";

import carIcon from "../../../../../assets/images/car-icon.png";

import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../components/common/TextField";
import { Heading } from "../../../../../components/common/Heading";
import { DropdownNormal } from "../../../../../components/common/DropDownNormal";
import GooglePlaceComplete from "../../../../../components/common/GooglePlaceComplete";
import useLanguage from "../../../../../hooks/useLanguage";
import DayPicker from "react-day-picker";
import { Loader } from "@googlemaps/js-api-loader";
import useSettings from "../../../../../hooks/useSettings";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import A from "../../../../../utils/API.js";
import NavLinks from "../../../../../utils/navLinks.json";
import U from "../../../../../utils/utils.js";
import { useFormik } from "formik";
import axios from "axios";
import useAdmin from "../../../../../hooks/useAdmin";
import { Helmet } from "react-helmet";
import { TimePicker } from "../../../../../components/common/TimePicker";
import Clock from "react-clock";
import { addDays, differenceInDays, differenceInHours, differenceInMinutes, format } from "date-fns";
import { parse } from "postcss";

import LoadingGif from "../../../../../assets/gifs/loading.gif";

import useImage from "../../../../../hooks/useImage";
import useUtils from "../../../../../hooks/useUtils";
import useDebug from "../../../../../hooks/useDebug";
import useSocket from "../../../../../hooks/useSocket";
import { Link } from "react-router-dom";
import { PopUp } from "../../../../../components/common/PopUp";
import { FaAngleDoubleUp } from "react-icons/fa";
import BookRideTable from "../../../../../components/bookRideTable";

const Page = ({ page, height, width, currentPage, children, ...rest }) => (
  <div
    className={`absolute transition-all duration-300 ${currentPage !== page && "border-l-2"}`}
    style={{
      marginLeft: (currentPage - page) * width,
      width,
      height,
    }}
    {...rest}
  >
    {children}
  </div>
);

export default function Index() {
  const { language } = useLanguage();
  const { settings } = useSettings();
  const { header, authFailure, token } = useAdmin();
  const { imageUrl } = useImage();
  const { truncate, diffMinutes } = useUtils();
  const [popup, setPop] = React.useState(null);
  // const { notificationSocket } = useSocket();

  const [form1Loading, setForm1Loading] = React.useState(false);
  const [form2Loading, setForm2Loading] = React.useState(false);
  const [topBtnLoading, setTopBtnLoading] = React.useState(false);
  // let retryInterval = 0;
  // let progressBarInterval = 0;
  // const [retryInterval, setRetryInterval] = React.useState(null);
  // const [progressBarInterval, setProgressBarInterval] = React.useState(null);
  const retryInterval = React.useRef();
  const progressBarInterval = React.useRef();
  const extraTime = 5000;
  const progressBarDivider =
    (parseInt(settings.bookingRetryCount) + 1) * (parseInt(settings.driverRequestTimeout) * 1000 + extraTime);

  // (parseInt(settings.bookingRetryCount) + 1 )* parseInt(settings.driverRequestTimeout

  const [clearText, setClearText] = React.useState(false);
  const [barWidth, setBarWidth] = React.useState(0);

  const [noDrivers, setNoDrivers] = React.useState(false);
  const [pickUpOutside, setPickUpOutside] = React.useState(false);
  const [dropOutside, setDropOutside] = React.useState(false);

  const [markers, setMarkers] = React.useState({
    pickUp: null,
    drop: null,
  });
  const [pickUp, setPickUp] = React.useState(null);
  const [drop, setDrop] = React.useState(null);

  const [guestUser, setGuestUser] = React.useState(false);

  const [user, setUser] = React.useState(null);
  const [bookingType, setBookingType] = React.useState(null);
  const [direction, setDirection] = React.useState(null);
  const [drivers, setDrivers] = React.useState([]);
  const [driverMarkers, setDriverMarkers] = React.useState([]);

  const [paymentMethod, setPaymentMethod] = React.useState(null);
  const [selectedDate, setSelectedDate] = React.useState(null);
  const [selectedTime, setSelectedTime] = React.useState(new Date(new Date().setMinutes(new Date().getMinutes() + 70)));
  const [selectedCategory, setSelectedCategory] = React.useState(null);
  const [bookingAccepted, setBookingAccepted] = React.useState(false);

  const [directionData, setDirectionData] = React.useState(null);

  const [categories, setCategories] = React.useState(null);
  const [bookingDetails, setBookingDetails] = React.useState(null);

  const [map, setMap] = React.useState(null);
  const mapArea = React.useRef(null);
  const [height, setHeight] = React.useState(400);
  const [width, setWidth] = React.useState(500);
  const [page, setPage] = React.useState(1);
  const [googleLoaded, setGoogleLoaded] = React.useState(false);

  const [walletOrCash, setWalletOrCash] = React.useState(null);

  const bookAnother = () => {
    formik1.setFieldValue("phoneNumber", "");
    setGuestUser(false);
    markers.pickUp.setMap(null);
    markers.drop.setMap(null);
    direction.setMap(null);
    setTopBtnLoading(false);
    setNoDrivers(false);
    setPickUpOutside(false);
    setDropOutside(false);
    setMarkers({
      pickUp: null,
      drop: null,
    });
    setPickUp(null);
    setDrop(null);
    setUser(null);
    setBookingType(null);
    setDirection(null);
    setDrivers([]);
    setDriverMarkers([]);
    setPaymentMethod(null);
    setSelectedCategory(null);
    setSelectedDate(null);
    setSelectedTime(null);
    setDirectionData(null);
    setCategories(null);
    setBookingDetails(null);
    setClearText(true);
    setTimeout(() => setClearText(false), 100);
    setBookingAccepted(false);
    setPage(1);
  };

  const formik1 = useFormik({
    initialValues: {
      phoneCode: "",
      phoneNumber: "",
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      phoneCode: yup.string().required(language.REQUIRED),
      phoneNumber: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setForm1Loading(true);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_BOOK_CHECK_USER,
          {
            phoneCode: e.phoneCode,
            phoneNumber: e.phoneNumber,
          },
          header
        );
        if (data.code === 200) {
          if (data.data.bookingInfo.ongoingBooking === null) {
            setUser(data.data);
            setPage(3);
          } else setPop({ title: language.NUMBER_ON_RIDE, type: "error" });
        } else if (data.code === 101) {
          setPop({ title: language.NUMBER_IN_ANOTHER_DIAL_CODE, type: "error" });
        } else {
          formik2.setFieldValue("phoneCode", e.phoneCode);
          formik2.setFieldValue("phoneNumber", e.phoneNumber);
          setPage(2);
        }
      } catch (err) {
        authFailure(err);
      }
      setForm1Loading(false);
    },
  });

  const formik2 = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      gender: "",
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      firstName: yup.string().required(language.REQUIRED),
      lastName: yup.string().required(language.REQUIRED),
      email: yup.string().required(language.REQUIRED).email(language.INVALID_EMAIL),
      gender: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setForm2Loading(true);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_BOOK_CREATE_USER,
          {
            phoneCode: formik1.values.phoneCode,
            phoneNumber: formik1.values.phoneNumber,
            firstName: e.firstName,
            lastName: e.lastName,
            email: e.email,
            gender: e.gender,
          },
          header
        );
        if (data.code === 200) {
          setUser(data.data);
          setPage(3);
        } else alert(language.ACCOUNT_ALREADY_EXISTS);
      } catch (err) {
        authFailure(err);
      }
      setForm2Loading(false);
    },
  });

  React.useEffect(() => {
    // USER DETAILS
    if (page === 1) {
      setHeight(230);
      setWidth(340);
    }
    // USER CREATE
    if (page === 2) {
      setHeight(480);
      setWidth(450);
    }
    //INSTANT OR SCHEDULE
    if (page === 3) {
      setHeight(250);
      setWidth(300);
    }
    // SCHEDULE CALENDER SELECT
    if (page === 4) {
      setHeight(450);
      setWidth(300);
    }
    // SCHEDULE TIME CLICK
    if (page === 5) {
      setHeight(390);
      setWidth(300);
    }
    // PICKUP DROP SELECT
    if (page === 6) {
      setHeight(350);
      setWidth(300);
    }
    // VEHICLE CATEGORIES
    if (page === 7) {
      setHeight(392);
      setWidth(300);
    }
    // DETAILS PAGE
    if (page === 8) {
      setHeight(352);
      setWidth(300);
    }
    //Retry and Expire and Wait for Professional
    if (page === 9) {
      setHeight(300);
      setWidth(400);
    }
    //NO PROFESSIONAL ACCEPTED
    if (page === 10) {
      setHeight(280);
      setWidth(380);
    }
    //BOOK COMPLETED
    if (page === 11) {
      setHeight(280);
      setWidth(380);
    }
  }, [page]);

  React.useEffect(() => {
    const loader = new Loader({
      apiKey: settings.mapApi.web,
      version: "weekly",
      libraries: ["places"],
    });
    loader
      .load()
      .then(() => {
        setGoogleLoaded(true);
      })
      .catch((e) => {});
  }, []);

  // React.useEffect(() => {
  //   notificationSocket.current.on(token, (data) => {
  //     if (data.action === "ACCEPTBOOKING") {
  //       setBarWidth(0);
  //       setBookingAccepted(true);
  //       setPage(11);
  //       clearInterval(retryInterval.current);
  //       clearInterval(progressBarInterval.current);
  //       setBarWidth(0);
  //     }
  //   });
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [notificationSocket]);

  const cancelRide = async () => {
    try {
      await axios.post(
        A.HOST + A.ADMIN_BOOK_EXPIRY,
        {
          bookingId: bookingDetails._id,
          denyType: "USERCANCELLED",
        },
        header
      );
      setPage(6);
      clearInterval(retryInterval.current);
      clearInterval(progressBarInterval.current);
      setBarWidth(0);
    } catch (err) {
      authFailure(err);
    }
  };

  React.useEffect(() => {
    if (googleLoaded === true) {
      const googleMap = new window.google.maps.Map(mapArea.current, {
        center: { lat: 12.9791551, lng: 80.2007085 },
        zoom: 14,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: true,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: true,
      });
      setMap(googleMap);
    }
  }, [googleLoaded]);

  const checkAvailableDrivers = async () => {
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_BOOK_DRIVERS_LIST,
        {
          lat: pickUp.results[0].geometry.location.lat,
          lng: pickUp.results[0].geometry.location.lng,
        },
        header
      );
      setDriverMarkers(
        data.map(
          (driver) =>
            new window.google.maps.Marker({
              icon: {
                url: carIcon,
                scaledSize: new window.google.maps.Size(35, 35),
              },
              position: {
                lat: driver.location.coordinates[1],
                lng: driver.location.coordinates[0],
              },
              map: map,
            })
        )
      );
      setDrivers(data);
      setPickUpOutside(false);
      setDropOutside(false);
      return true;
    } catch (err) {
      authFailure(err);
      setNoDrivers(true);
      setPage(6);
      return false;
    }
  };

  React.useEffect(() => {
    const pickUpFunc = async () => {
      if (googleLoaded === true && pickUp !== null) {
        driverMarkers.length > 0 && driverMarkers.forEach((each) => each.setMap(null));
        setTopBtnLoading(true);
        setNoDrivers(false);
        setPickUpOutside(false);
        setDropOutside(false);
        if (bookingType === U.INSTANT) checkAvailableDrivers();
        setTopBtnLoading(false);
        if (markers.pickUp !== null) markers.pickUp.setMap(null);
        const pickUpMarker = new window.google.maps.Marker({
          position: { lat: pickUp.results[0].geometry.location.lat, lng: pickUp.results[0].geometry.location.lng },
          map: map,
        });
        map.setCenter({ lat: pickUp.results[0].geometry.location.lat, lng: pickUp.results[0].geometry.location.lng });
        setMarkers({ ...markers, pickUp: pickUpMarker });
      }
    };
    pickUpFunc();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [googleLoaded, pickUp]);

  React.useEffect(() => {
    if (googleLoaded === true && drop !== null) {
      if (markers.drop !== null) markers.drop.setMap(null);
      const dropMarker = new window.google.maps.Marker({
        position: { lat: drop.results[0].geometry.location.lat, lng: drop.results[0].geometry.location.lng },
        map: map,
      });
      setMarkers({ ...markers, drop: dropMarker });
    }
  }, [googleLoaded, drop]);

  React.useEffect(() => {
    const check = async () => {
      if (pickUp !== null && drop !== null) {
        if (markers.pickUp !== null) markers.pickUp.setMap(null);
        if (markers.drop !== null) markers.drop.setMap(null);
        if (direction !== null) direction.setMap(null);

        //MARKER
        const coordinatesForPath = [
          { lat: pickUp.results[0].geometry.location.lat, lng: pickUp.results[0].geometry.location.lng },
          { lat: drop.results[0].geometry.location.lat, lng: drop.results[0].geometry.location.lng },
        ];
        var bounds = new window.google.maps.LatLngBounds();
        coordinatesForPath.forEach((poly) => bounds.extend(poly));
        map.fitBounds(bounds);

        //CREATING DIRECTIONS
        const directionsService = new window.google.maps.DirectionsService();
        const directionsRenderer = new window.google.maps.DirectionsRenderer();
        var request = {
          origin: { lat: pickUp.results[0].geometry.location.lat, lng: pickUp.results[0].geometry.location.lng },
          destination: { lat: drop.results[0].geometry.location.lat, lng: drop.results[0].geometry.location.lng },
          travelMode: "DRIVING",
        };
        directionsService.route(request, function (result, status) {
          if (status === "OK") {
            directionsRenderer.setDirections(result);
            setDirectionData(result);
          }
        });
        directionsRenderer.setMap(map);
        setDirection(directionsRenderer);
      }
    };
    check();
  }, [pickUp, drop]);

  const fetchCategories = async () => {
    setTopBtnLoading(true);
    const driversAvailable = await checkAvailableDrivers();
    if (driversAvailable || bookingType === U.SCHEDULE) {
      let bookingDate = new Date();
      if (bookingType === U.SCHEDULE) {
        const timeString = new Date(selectedTime).getHours() + ":" + new Date(selectedTime).getMinutes() + ":00";
        var year = selectedDate.getFullYear();
        var month = selectedDate.getMonth() + 1; // Jan is 0, dec is 11
        var day = selectedDate.getDate();
        var dateString = "" + year + "-" + month + "-" + day;
        bookingDate = new Date(dateString + " " + timeString);
      }
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_BOOK_CATEGORIES_GET,
          {
            pickupLat: pickUp.results[0].geometry.location.lat,
            pickupLng: pickUp.results[0].geometry.location.lng,
            dropLat: drop.results[0].geometry.location.lat,
            dropLng: drop.results[0].geometry.location.lng,
            estimationDistance: directionData.routes[0].legs[0].distance.value,
            estimationTime: directionData.routes[0].legs[0].duration.value,
            bookingDate: bookingDate,
            bookingType: bookingType,
          },
          header
        );
        if (data.status === "CATEGORYAVAILABLE") {
          setCategories(data.data);
          setPage(7);
          let defaultCategory =
            data.data.categories.filter((category) => category.defaultCategory === true).length > 0
              ? data.data.categories.filter((category) => category.defaultCategory === true)[0].categoryID
              : null;
          if (defaultCategory === null) {
            defaultCategory =
              bookingType === U.INSTANT && drivers.length > 0
                ? drivers[0].vehicles.filter((vehicle) => vehicle.defaultVehicle === true)[0].vehicleCategoryId
                : bookingType === U.SCHEDULE
                ? data.data.categories[0].categoryID
                : null;
          }
          setSelectedCategory(defaultCategory);
        } else if (data.status === "PICKUPLOCATIONOUT") {
          setPickUpOutside(true);
        } else if (data.status === "DROPLOCATIONOUT") {
          setDropOutside(true);
        } else setNoDrivers(true);
      } catch (err) {
        alert(err);
      }
    }
    setTopBtnLoading(false);
  };

  const bookRide = async (paymentType) => {
    setWalletOrCash(paymentType);
    let driverAvailable = true;
    setTopBtnLoading(true);
    setPaymentMethod(paymentType);
    if (
      pickUp === null ||
      drop === null ||
      categories === null ||
      (user === null && guestUser === false) ||
      selectedCategory === null
    )
      return alert("Complete Previous Steps Correctly");
    const apiLink = bookingType === U.INSTANT ? A.ADMIN_BOOK_RIDE_INSTANT : A.ADMIN_BOOK_RIDE_SCHEDULE;
    if (bookingType === U.INSTANT) {
      driverAvailable = await checkAvailableDrivers();
    }
    if (driverAvailable) {
      let bookingDate = new Date();
      if (bookingType === U.SCHEDULE) {
        const timeString = new Date(selectedTime).getHours() + ":" + new Date(selectedTime).getMinutes() + ":00";
        var year = selectedDate.getFullYear();
        var month = selectedDate.getMonth() + 1; // Jan is 0, dec is 11
        var day = selectedDate.getDate();
        var dateString = "" + year + "-" + month + "-" + day;
        bookingDate = new Date(dateString + " " + timeString);
      }
      let category = null;
      if (categories.categories.filter((each) => each.categoryID === selectedCategory).length > 0) {
        category = categories.categories.filter((each) => each.categoryID === selectedCategory)[0];
      } else {
        alert(language.COMPLETE_PREVIOUS_STEP);
        setPage(6);
      }
      try {
        const { data } = await axios.post(
          A.HOST + apiLink,
          {
            categoryId: categories.serviceAreaID,
            bookingDate,
            pickUpAddressName: pickUp.results[0].formatted_address,
            paymentOption: paymentType,
            dropShortAddress: drop.results[0].formatted_address,
            pickUpLat: pickUp.results[0].geometry.location.lat,
            estimationDistance: directionData.routes[0].legs[0].distance.value,
            bookingLocations: [
              {
                addressName: pickUp.results[0].formatted_address,
                fullAddress: pickUp.results[0].formatted_address,
                shortAddress: pickUp.results[0].formatted_address,
                lat: pickUp.results[0].geometry.location.lat,
                lng: pickUp.results[0].geometry.location.lng,
                type: "ORIGIN",
              },
              {
                addressName: drop.results[0].formatted_address,
                fullAddress: drop.results[0].formatted_address,
                shortAddress: drop.results[0].formatted_address,
                lat: drop.results[0].geometry.location.lat,
                lng: drop.results[0].geometry.location.lng,
                type: "DESTINATION",
              },
            ],
            dropFullAddress: drop.results[0].formatted_address,
            dropAddressName: drop.results[0].formatted_address,
            pickUpFullAddress: drop.results[0].formatted_address,
            dropLng: drop.results[0].geometry.location.lng,
            estimationTime: directionData.routes[0].legs[0].duration.value,
            estimationAmount: category.cost,
            bookingFor: {
              name: guestUser ? "" : user.firstName + " " + user.lastName,
              phoneCode: guestUser ? formik1.values.phoneCode : user.phone.code,
              phoneNumber: guestUser ? formik1.values.phoneNumber : user.phone.number,
            },
            dropLat: drop.results[0].geometry.location.lat,
            serviceTax: category.serviceTax,
            pickUpShortAddress: pickUp.results[0].formatted_address,
            pickUpLng: pickUp.results[0].geometry.location.lng,
            siteCommisionValue: category.siteCommisionValue,
            travelCharge: category.travelCharge,
            vehicleCategoryId: selectedCategory,
            userId: guestUser ? "" : user._id,
          },
          header
        );
        if (data.code !== 200) {
          setPage(6);
        } else {
          setBookingDetails(data.data);
          window.open(NavLinks.ADMIN_BOOK_RIDE_ACCEPT_DETAILS + "/" + data.data._id, "", "height=500,width=500");
          bookAnother();
          // if (bookingType === U.INSTANT) {
          //   setPage(9);
          //   let initialCount = 0;
          //   const timeWait = +settings.driverRequestTimeout * 1000 + extraTime;

          //   const proBarInterval = setInterval(() => {
          //     setBarWidth((barW) => barW + 1000);
          //   }, 1000);

          //   progressBarInterval.current = proBarInterval;

          //   // RETRY INTERVAL
          //   const reInterval = setInterval(async () => {
          //     const retry = +settings.bookingRetryCount;
          //     if (initialCount === retry) {
          //       clearInterval(retryInterval.current);
          //       clearInterval(progressBarInterval.current);
          //       setBarWidth(0);
          //       if (bookingAccepted === false) {
          //         try {
          //           await axios.post(
          //             A.HOST + A.ADMIN_BOOK_EXPIRY,
          //             {
          //               bookingId: data.data._id,
          //               denyType: "EXPIRED",
          //             },
          //             header
          //           );
          //           setPage(10);
          //         } catch (err) {
          //           authFailure(err);
          //         }
          //       }
          //     } else {
          //       try {
          //         await axios.post(
          //           A.HOST + A.ADMIN_BOOK_RETRY,
          //           {
          //             bookingId: data.data._id,
          //           },
          //           header
          //         );
          //       } catch (err) {
          //         authFailure(err);
          //       }
          //       initialCount = initialCount + 1;
          //     }
          //   }, timeWait);
          //   retryInterval.current = reInterval;
          //   // setTimeout(() => {
          //   //   clearInterval(retryInterval.current);
          //   //   clearInterval(progressBarInterval.current);
          //   //   setBarWidth(0);
          //   // }, timeWait * +settings.bookingRetryCount);
          // } else {
          //   setPage(11);
          // }
        }
      } catch (err) {
        // console.log(err);
      }
    }
    setTopBtnLoading(false);
  };

  return googleLoaded === false ? (
    <SmallLoader />
  ) : (
    <div
      className={
        "fixed top-0 left-0 right-0 flex justify-center items-center bg-gradient-to-t from-blue-800 to-green-800"
      }
      style={{
        height: "90vh",
      }}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <div
        className="fixed left-0 right-0 flex justify-center transition-all"
        style={{
          top: topBtnLoading === true ? 0 : "-100%",
          zIndex: 20,
        }}
      >
        <p className={"bg-red-500 text-white px-3 py-2"}>{language.LOADING}</p>
      </div>
      <BookRideTable />
      {/* MAP  */}
      <div
        className="fixed overflow-hidden inset-0 h-screen flex justify-center items-center transition-all duration-300"
        style={{
          top: page === 6 || page === 7 ? "-5%" : page === 3 ? "-5%" : page === 4 ? "-5%" : page === 5 ? "-5%" : "75%",
          marginLeft: (page === 6 || page === 7) && pickUp !== null && drop !== null ? 130 : 0,
        }}
      >
        <div
          ref={mapArea}
          className="bg-white rounded-xl"
          style={{
            height: 400,
            width: 600,
          }}
        ></div>
        {/* NO DRIVERS AVAILABLE  */}
        {noDrivers === true && bookingType === U.INSTANT && (
          <p
            className={
              "fixed transition-all flex justify-center justify-center items-center bg-green-800 rounded-xl px-3 py-2 text-center text-xl text-white"
            }
            style={{
              maxWidth: 200,
              zIndex: 10,
            }}
          >
            <FcHighPriority className={"text-5xl"} />
            <p className={"text-sm"}>No Professionals Available in Pick Up Location</p>
          </p>
        )}
        {dropOutside === true && bookingType === U.INSTANT && (
          <p
            className={
              "fixed transition-all flex justify-center justify-center items-center bg-green-800 rounded-xl px-3 py-2 text-center text-xl text-white"
            }
            style={{
              maxWidth: 200,
              zIndex: 10,
            }}
          >
            <FcHighPriority className={"text-5xl"} />
            <p className={"text-sm"}>Drop Location is not avaialeble in service area</p>
          </p>
        )}
        {pickUpOutside === true && bookingType === U.INSTANT && (
          <p
            className={
              "fixed transition-all flex justify-center justify-center items-center bg-green-800 rounded-xl px-3 py-2 text-center text-xl text-white"
            }
            style={{
              maxWidth: 200,
              zIndex: 10,
            }}
          >
            <FcHighPriority className={"text-5xl"} />
            <p className={"text-sm"}>Pick Up Location is not avaialeble in service area</p>
          </p>
        )}
        {/* ERROR RED BACKGROUND  */}
        {(noDrivers === true || pickUpOutside === true || dropOutside === true) && bookingType === U.INSTANT && (
          <div
            className="fixed rounded-xl opacity-25 h-full w-full bg-red-500"
            style={{
              height: 600,
              width: 600,
            }}
          ></div>
        )}
      </div>
      <div
        className="relative rounded-xl flex overflow-hidden transition-all duration-300 shadow-xl"
        style={{
          marginLeft:
            page === 6 || page === 7
              ? (page === 6 || page === 7) && pickUp !== null && drop !== null
                ? "-720px"
                : (page === 6 || page === 7) && pickUp !== null
                ? "-620px"
                : "0"
              : 0,
          height,
          width,
        }}
      >
        {/* PHONE CHECK  */}
        <Page page={page} currentPage={1} width={width} height={height} setPage={setPage}>
          <div className="flex flex-col bg-gray-50 dark:bg-gray-800 dark:text-white h-full w-full justify-between">
            <div className={"bg-blue-800 text-white p-3"}>
              <h1 className={"flex items-center"}>
                <FiUser className={"mr-2"} /> {language.USER_DETAILS}
              </h1>
            </div>

            <form onSubmit={formik1.handleSubmit} className="pb-3">
              <FieldWrapper title={language.MOBILE}>
                <CountryCodesPicker
                  change={(e) => formik1.setFieldValue("phoneCode", e)}
                  defaultValue={formik1.values.phoneCode}
                  error={formik1.errors.phoneCode}
                  placeholder={language.DIAL_CODE}
                  margin={3}
                  width="4/12"
                />
                <TextField
                  change={(e) => formik1.setFieldValue("phoneNumber", e)}
                  value={formik1.values.phoneNumber}
                  error={formik1.errors.phoneNumber}
                  placeholder={language.PHONE}
                  width="8/12"
                  type="number"
                  icon={<FiPhone />}
                />
              </FieldWrapper>
              <FieldWrapper>
                <div className="flex w-full justify-end">
                  <Button
                    loading={form1Loading}
                    onClick={() => {
                      formik1.handleSubmit();
                    }}
                    title={language.CONTINUE}
                  />
                </div>
              </FieldWrapper>
            </form>
          </div>
        </Page>{" "}
        {/* USER CREATE  */}
        <Page page={page} currentPage={2} width={width} height={height} setPage={setPage}>
          <form
            onSubmit={formik2.handleSubmit}
            className="flex flex-col bg-gray-50 dark:bg-gray-800 dark:text-white h-full w-full justify-between"
          >
            <div className={"bg-red-500 text-white p-3"}>
              <h1 className={"flex items-center justify-between"}>
                <h1 className={"cursor-pointer"} onClick={() => setPage(1)}>
                  {language.GO_BACK}
                </h1>
              </h1>
            </div>
            <div className="pb-3 overflow-y-scroll">
              <FieldWrapper>
                <h1 className={"text-red-500 text-sm flex items-center"}>
                  <FiInfo />
                  <span className={"mx-2"}>{language.USER_NOT_FOUND}</span>
                </h1>
              </FieldWrapper>
              <hr className={"my-3"} />
              <Heading title={formik1.values.phoneCode + " " + formik1.values.phoneNumber} />
              <FieldWrapper title={language.FULLNAME}>
                <TextField
                  change={(e) => formik2.setFieldValue("firstName", e)}
                  error={formik2.errors.firstName}
                  value={formik2.values.firstName}
                  width="2/4"
                  type="text"
                  margin={3}
                  placeholder={language.FIRSTNAME}
                />
                <TextField
                  change={(e) => formik2.setFieldValue("lastName", e)}
                  error={formik2.errors.lastName}
                  value={formik2.values.lastName}
                  width="2/4"
                  type="text"
                  placeholder={language.LASTNAME}
                />
              </FieldWrapper>
              <FieldWrapper title={language.EMAIL}>
                <TextField
                  change={(e) => formik2.setFieldValue("email", e)}
                  error={formik2.errors.email}
                  value={formik2.values.email}
                  placeholder={language.EMAIL}
                />
              </FieldWrapper>
              <FieldWrapper title={language.GENDER}>
                <DropdownNormal
                  change={(e) => formik2.setFieldValue("gender", e)}
                  error={formik2.errors.gender}
                  fields={[
                    {
                      id: 1,
                      label: language.FEMALE,
                      value: "FEMALE",
                    },
                    {
                      id: 2,
                      label: language.MALE,
                      value: "MALE",
                    },
                    {
                      id: 3,
                      label: language.OTHER,
                      value: "OTHER",
                    },
                  ]}
                />
              </FieldWrapper>
              <FieldWrapper>
                <div className="flex w-full justify-end">
                  <Button
                    loading={form2Loading}
                    onClick={(e) => {
                      e.preventDefault();
                      setGuestUser(true);
                      setPage(3);
                    }}
                    title={language.SKIP}
                  />
                  <Button loading={form2Loading} onClick={formik2.handleSubmit} title={language.CONTINUE} />
                </div>
              </FieldWrapper>
            </div>
          </form>
        </Page>
        {/* BOOKING TYPE  */}
        <Page page={page} currentPage={3} width={width} height={height}>
          <div className="flex flex-col h-full w-full bg-white dark:bg-gray-900 justify-between">
            <div className={"bg-green-800 text-white p-3"}>
              <h1 className={"flex items-center justify-between"}>
                <h1 className={"cursor-pointer"} onClick={() => setPage(1)}>
                  {language.GO_BACK}
                </h1>
              </h1>
            </div>
            <div className="flex flex-col mb-3">
              <button
                onClick={() => {
                  setBookingType(U.INSTANT);
                  setDrivers([]);
                  setPage(6);
                }}
                className={
                  "hover:border-blue-800 bg-white dark:bg-gray-800 dark:text-white transition focus:outline-none px-3 py-3 rounded-xl mx-3 flex border-2 items-center justify-between"
                }
              >
                <div className="text-3xl font-bold">
                  <p>{language.INSTANT}</p>
                  <p className={"text-left text-sm font-normal text-gray-500"}>{language.RIDE_NOW}</p>
                </div>
                <div className="text-5xl">
                  <FcSportsMode />
                </div>
              </button>{" "}
              <button
                onClick={() => {
                  setBookingType(U.SCHEDULE);
                  setDrivers([]);
                  setPage(4);
                }}
                className={
                  "hover:border-blue-800 bg-white dark:bg-gray-800 dark:text-white transition focus:outline-none mt-2 px-3 py-3 rounded-xl mx-3 flex border-2 items-center justify-between"
                }
              >
                <div className="text-3xl font-bold">
                  <p>{language.SCHEDULE}</p>
                  <p className={"text-left text-sm font-normal text-gray-500"}>{language.RIDE_LATER}</p>
                </div>
                <div className="text-5xl">
                  <FcClock />
                </div>
              </button>
            </div>
          </div>
        </Page>
        {/* CALENDER  */}
        <Page page={page} currentPage={4} width={width} height={height} setPage={setPage}>
          <div className="flex flex-col h-full w-full bg-white dark:bg-gray-800 justify-between">
            <div className={"bg-green-800 text-white dark:text-white p-3"}>
              <h1 className={"flex items-center justify-between"}>
                <h1 className={"cursor-pointer"} onClick={() => setPage(3)}>
                  {language.GO_BACK}
                </h1>
              </h1>
            </div>
            <div className="flex flex-col mb-3">
              <h1 className={"text-center mt-3 text-sm text-lg text-green-800 dark:text-white border-b-2 pb-2"}>
                {language.SELECT_A_DATE}
              </h1>
              <DayPicker
                disabledDays={[{ before: new Date(), after: addDays(new Date(), 30) }]}
                selectedDays={selectedDate === null ? false : [selectedDate]}
                className={"dark:text-white"}
                onDayClick={(e) => {
                  if (differenceInDays(new Date(), new Date(e)) <= 0) {
                    setSelectedDate(e);
                    setPage(5);
                    setSelectedTime(new Date(new Date().setMinutes(new Date().getMinutes() + 75)));
                  }
                }}
              />
            </div>
          </div>
        </Page>
        {/* CLOCK  */}
        <Page page={page} currentPage={5} width={width} height={height} setPage={setPage}>
          <div className="flex flex-col h-full w-full bg-white dark:bg-gray-900 justify-between">
            <div className={"bg-green-800 text-white p-3"}>
              <h1 className={"flex items-center justify-between"}>
                <h1 className={"cursor-pointer"} onClick={() => setPage(4)}>
                  {language.GO_BACK}
                </h1>
              </h1>
            </div>
            <div className="flex flex-col mb-3">
              <h1 className={"text-center mt-3 text-sm text-lg text-green-800 dark:text-white border-b-2 pb-2"}>
                {language.PICK_A_TIME}
              </h1>
              <div className="flex justify-center items-center mt-3">
                <Clock
                  renderSecondHand={false}
                  className={"bg-gray-50 rounded-full border-4 border-blue-800 shadow-xl"}
                  value={new Date(selectedTime)}
                />
              </div>
              <FieldWrapper>
                <TimePicker
                  className={"dark:text-white dark:bg-gray-900 border-2"}
                  change={(e) => setSelectedTime(e)}
                  defaultValue={selectedTime}
                />
              </FieldWrapper>
              <FieldWrapper>
                <div className="flex w-full justify-end mt-2">
                  <Button
                    loading={form2Loading}
                    onClick={() => {
                      if (differenceInDays(new Date(), new Date(selectedDate)) === 0) {
                        if (differenceInMinutes(new Date(), new Date(selectedDate)) > 0) {
                          if (differenceInMinutes(new Date(selectedTime), new Date()) > 70) setPage(6);
                          else alert(language.TIME_SHOULD_BE_ABOVE_1_HOUR);
                        } else setPage(6);
                      } else setPage(6);
                    }}
                    title={language.CONTINUE}
                  />
                </div>
              </FieldWrapper>
            </div>
          </div>
        </Page>
        {/* PICKUP DROP  */}
        <Page page={page} currentPage={6} width={width} height={height} setPage={setPage}>
          <div className="flex flex-col bg-gray-50 dark:bg-gray-800 dark:text-white h-full w-full justify-between">
            <div className={"bg-blue-800 text-white p-2"}>
              <h1 className={"flex items-center justify-between"}>
                <h1
                  className={"cursor-pointer p-1"}
                  onClick={() => {
                    setPage(3);
                    setNoDrivers(false);
                    setPickUpOutside(false);
                    setDropOutside(false);
                    // markers.pickUp && markers.pickUp.setMap(null);
                    // markers.drop && markers.drop.setMap(null);
                    // setMarkers({ pickUp: null, drop: null });
                    // direction !== null && direction.setMap(null);
                    // setClearText(true);
                    // setTimeout(() => setClearText(false), 100);
                  }}
                >
                  {language.GO_BACK}
                </h1>
                {directionData !== null && (
                  <div className="relative">
                    <p className={"absolute animate-ping bg-green-800 p-1 text-white rounded-full text-sm"}>
                      {directionData.routes[0].legs[0].distance.text || ""}
                    </p>
                    <p className={"bg-green-800 p-1 px-3 text-white rounded-full text-sm"}>
                      {directionData.routes[0].legs[0].distance.text || ""}
                    </p>
                  </div>
                )}
              </h1>
            </div>
            <div className="pb-3">
              <FieldWrapper>
                <h1 className={"text-blue-800 text-sm flex items-center"}>
                  <FiInfo />
                  <span className={"mx-2"}>
                    <span className={"text-blue-800 dark:text-white"}>
                      {guestUser === true ? language.GUEST_USER : user !== null && user.email}
                    </span>
                  </span>
                </h1>
              </FieldWrapper>
              <hr className={"my-3"} />
              <FieldWrapper title={language.PICKUP_LOCATION}>
                <GooglePlaceComplete
                  clearText={clearText}
                  change={(e) => setPickUp(e)}
                  placeholder={language.ADDRESS}
                />
              </FieldWrapper>
              <FieldWrapper title={language.DROP_LOCATION}>
                <GooglePlaceComplete
                  clearText={clearText}
                  change={(e) => setDrop(e)}
                  placeholder={language.ADDRESS}
                  readOnly={pickUp === null || (bookingType === U.INSTANT ? noDrivers : false)}
                />
              </FieldWrapper>
              <FieldWrapper>
                <div className="flex w-full justify-end">
                  <Button
                    onClick={() =>
                      bookingType === U.INSTANT &&
                      noDrivers === false &&
                      pickUp !== null &&
                      drop !== null &&
                      directionData !== null
                        ? fetchCategories()
                        : bookingType === U.SCHEDULE && pickUp !== null && drop !== null && directionData !== null
                        ? fetchCategories()
                        : null
                    }
                    style={{
                      opacity:
                        bookingType === U.INSTANT &&
                        noDrivers === false &&
                        pickUp !== null &&
                        drop !== null &&
                        directionData !== null
                          ? 1
                          : bookingType === U.SCHEDULE && pickUp !== null && drop !== null && directionData !== null
                          ? 1
                          : "0.2",
                    }}
                    title={language.CONTINUE}
                  />
                </div>
              </FieldWrapper>
            </div>
          </div>
        </Page>
        {/* VEHICLE CATEGORIES */}
        <Page currentPage={7} height={height} width={width} page={page}>
          <div className={"bg-white dark:bg-gray-800 overflow-hidden"}>
            <div className={"bg-green-800 text-white p-3"}>
              <h1 className={"flex items-center justify-between"}>
                <h1 className={"cursor-pointer"} onClick={() => setPage(6)}>
                  {language.GO_BACK}
                </h1>
              </h1>
            </div>
            <div
              className={"bg-white transition-all overflow-y-scroll"}
              style={{
                height: 305,
              }}
            >
              {categories !== null &&
                categories.categories.map((category) => (
                  <div
                    onClick={() =>
                      drivers.filter(
                        (driver) =>
                          driver.vehicles.filter((vehicle) => vehicle.defaultVehicle === true)[0].vehicleCategoryId ===
                          category.categoryID
                      ).length > 0 ||
                      drivers.filter(
                        (driver) =>
                          driver.vehicles.filter(
                            (vehicle) =>
                              vehicle.defaultVehicle === true &&
                              vehicle.applySubCategory === true &&
                              vehicle.isSubCategoryAvailable === true
                          ).length > 0 &&
                          driver.vehicles
                            .filter(
                              (vehicle) =>
                                vehicle.defaultVehicle === true &&
                                vehicle.applySubCategory === true &&
                                vehicle.isSubCategoryAvailable === true
                            )[0]
                            .subCategoryIds.filter((each) => each === category.categoryID).length > 0
                      ).length > 0 ||
                      bookingType === U.SCHEDULE
                        ? setSelectedCategory(category.categoryID)
                        : null
                    }
                    className={`w-full flex justify-between p-3 transition cursor-pointer border-b-2 bg-white dark:bg-gray-900 dark:border-gray-600 dark:text-white ${
                      selectedCategory === category.categoryID &&
                      "bg-gray-200 dark:bg-gray-800 border-y-2 border-blue-800 dark:border-blue-800"
                    } ${
                      !(
                        drivers.filter(
                          (driver) =>
                            driver.vehicles.filter((vehicle) => vehicle.defaultVehicle === true)[0]
                              .vehicleCategoryId === category.categoryID
                        ).length > 0 ||
                        drivers.filter(
                          (driver) =>
                            driver.vehicles.filter(
                              (vehicle) =>
                                vehicle.defaultVehicle === true &&
                                vehicle.applySubCategory === true &&
                                vehicle.isSubCategoryAvailable === true
                            ).length > 0 &&
                            driver.vehicles
                              .filter(
                                (vehicle) =>
                                  vehicle.defaultVehicle === true &&
                                  vehicle.applySubCategory === true &&
                                  vehicle.isSubCategoryAvailable === true
                              )[0]
                              .subCategoryIds.filter((each) => each === category.categoryID).length > 0
                        ).length > 0 ||
                        bookingType === U.SCHEDULE
                      ) && "opacity-50"
                    }`}
                  >
                    <div className={"flex"}>
                      {selectedCategory === category.categoryID && (
                        <FiCheck className={"absolute text-xl -ml-2 bg-blue-800 p-1 rounded-full text-white"} />
                      )}
                      <img
                        className={"object-cover"}
                        style={{
                          height: 50,
                          width: 50,
                        }}
                        src={imageUrl(category.categoryImage)}
                        alt=""
                      />
                      <div className="flex flex-col px-2">
                        <h1 className={"font-bold text-xl"}>{category.categoryName}</h1>
                        <p className={"text-sm text-gray-500 flex"}>
                          {bookingType === U.INSTANT && (
                            <div className="flex text-md items-center">
                              <BiCar className={"text-blue-800"} />
                              <p className={"mx-2"}>
                                {drivers.filter(
                                  (driver) =>
                                    driver.vehicles.filter((vehicle) => vehicle.defaultVehicle === true)[0]
                                      .vehicleCategoryId === category.categoryID
                                ).length +
                                  drivers.filter(
                                    (driver) =>
                                      driver.vehicles.filter(
                                        (vehicle) =>
                                          vehicle.defaultVehicle === true &&
                                          vehicle.applySubCategory === true &&
                                          vehicle.isSubCategoryAvailable === true
                                      ).length > 0 &&
                                      driver.vehicles
                                        .filter(
                                          (vehicle) =>
                                            vehicle.defaultVehicle === true &&
                                            vehicle.applySubCategory === true &&
                                            vehicle.isSubCategoryAvailable === true
                                        )[0]
                                        .subCategoryIds.filter((each) => each === category.categoryID).length > 0
                                  ).length}
                              </p>
                            </div>
                          )}
                          {/* <div className="mx-2 flex text-md items-center">
                            <FiUsers className={"text-blue-800"} />
                            <p className={"mx-2"}>{category.seatCount}</p>
                          </div> */}
                        </p>
                      </div>
                    </div>
                    <div>
                      <p className={"text-xl font-bold"}>
                        {categories.currencySymbol}
                        {category.cost}
                      </p>
                    </div>
                  </div>
                ))}
            </div>
            <div
              onClick={() => setPage(8)}
              className={
                "bg-blue-800 px-4 py-2 flex justify-center items-center text-white cursor-pointer hover:bg-green-800"
              }
            >
              {language.CONTINUE}
            </div>
          </div>
        </Page>
        {/* CASH OR WALLET  */}
        <Page page={page} currentPage={8} width={width} height={height} setPage={setPage}>
          <div className="bg-white overflow-hidden h-full">
            <div className={"bg-green-800 text-white p-3"}>
              <h1 className={"flex items-center justify-between"}>
                <h1 className={"cursor-pointer"} onClick={() => setPage(7)}>
                  {language.GO_BACK}
                </h1>
              </h1>
            </div>
            <FieldWrapper>
              <div className="bg-blue-800 w-full p-2 flex flex-col rounded-xl">
                <div className="flex items-center">
                  <p className={"text-sm text-white opacity-75"}>{language.FROM}</p>
                  <p className={"text-white mx-2 truncate"}>{pickUp !== null && pickUp.results[0].formatted_address}</p>
                </div>
                <div className="flex items-center">
                  <p className={"text-sm text-white opacity-75"}>{language.TO}</p>
                  <p className={"text-white mx-2 truncate"}>{drop !== null && drop.results[0].formatted_address}</p>
                </div>
              </div>
            </FieldWrapper>
            <FieldWrapper>
              <div className="flex justify-between w-full px-3 items-center">
                <div className={"flex items-center"}>
                  <img
                    className={"object-cover"}
                    style={{
                      height: 50,
                      width: 50,
                    }}
                    src={imageUrl(
                      categories !== null &&
                        selectedCategory !== null &&
                        categories.categories.filter((category) => category.categoryID === selectedCategory)[0] !==
                          undefined &&
                        categories.categories.filter((category) => category.categoryID === selectedCategory)[0]
                          .categoryImage
                    )}
                    alt=""
                  />
                  <h1 className={"text-lg font-bold mx-2 truncate"}>
                    {truncate(
                      categories !== null &&
                        selectedCategory !== null &&
                        categories.categories.filter((category) => category.categoryID === selectedCategory)[0] !==
                          undefined &&
                        categories.categories.filter((category) => category.categoryID === selectedCategory)[0]
                          .categoryName,
                      10
                    )}
                  </h1>
                </div>
                <p className={"text-2xl font-bold"}>
                  {categories !== null && categories.currencySymbol}{" "}
                  {categories !== null &&
                    selectedCategory !== null &&
                    categories.categories.filter((category) => category.categoryID === selectedCategory)[0] !==
                      undefined &&
                    categories.categories.filter((category) => category.categoryID === selectedCategory)[0].cost}
                </p>
              </div>
            </FieldWrapper>
            <FieldWrapper title={language.PAYMENT_METHOD_CONTINUE}>
              <div className="flex w-full">
                <div
                  onClick={() =>
                    user !== null &&
                    categories !== null &&
                    selectedCategory !== null &&
                    categories.categories.filter((category) => category.categoryID === selectedCategory)[0].cost <=
                      user.wallet.availableAmount
                      ? bookRide("WALLET")
                      : null
                  }
                  className="w-1/2"
                >
                  <div
                    className={`${
                      user !== null &&
                      categories !== null &&
                      selectedCategory !== null &&
                      categories.categories.filter((category) => category.categoryID === selectedCategory)[0] !==
                        undefined &&
                      categories.categories.filter((category) => category.categoryID === selectedCategory)[0].cost <=
                        user.wallet.availableAmount
                        ? "hover:border-blue-800 cursor-pointer hover:bg-gray-100"
                        : "cursor-not-allowed opacity-50"
                    } bg-gray-100 border-2 px-2 py-2 mx-1 rounded-xl text-gray-500 flex flex-col items-center`}
                  >
                    <FcPackage className={"text-5xl"} />
                    {/* <h1 className={"text-sm"}>{language.WALLET}</h1> */}
                    <h1 className={"text-sm"}>
                      {user !== null &&
                      categories !== null &&
                      selectedCategory !== null &&
                      categories.categories.filter((category) => category.categoryID === selectedCategory)[0] !==
                        undefined &&
                      categories.categories.filter((category) => category.categoryID === selectedCategory)[0].cost <=
                        user.wallet.availableAmount
                        ? language.WALLET
                        : language.WALLET}
                    </h1>
                  </div>
                </div>
                <div onClick={() => bookRide("CASH")} className="w-1/2">
                  <div className="hover:border-blue-800 cursor-pointer hover:bg-gray-100 bg-gray-100 border-2 px-2 py-2 mx-1 rounded-xl text-gray-500 flex flex-col items-center">
                    <FcMoneyTransfer className={"text-5xl"} />
                    <h1 className={"text-sm"}>{language.CASH}</h1>
                  </div>
                </div>
              </div>
            </FieldWrapper>
            <p
              className={"text-center text-white bg-red-500"}
              style={{
                fontSize: 13,
              }}
            >
              {user !== null &&
              categories !== null &&
              selectedCategory !== null &&
              categories.categories.filter((category) => category.categoryID === selectedCategory)[0] !== undefined &&
              categories.categories.filter((category) => category.categoryID === selectedCategory)[0].cost <=
                user.wallet.availableAmount
                ? ""
                : language.INSUFFICIENT_WALLET_CHOOSE_CASH}
            </p>
          </div>
        </Page>
        {/* CHECK FOR PROFESSIONAL */}
        <Page page={page} currentPage={9} width={width} height={height} setPage={setPage}>
          <div className="bg-white dark:bg-gray-900 dark:text-white overflow-hidden w-full h-full flex flex-col justify-between">
            <div className={"bg-blue-800 px-4 py-3 text-center text-white flex items-center justify-center"}>
              <p>{language.YOUR_BOOKING_IS_PLACED}</p> <FiInfo className={"mx-2"} />
            </div>
            <div className={"h-full w-full"}>
              <p
                className={
                  "text-gray-600 dark:text-gray-200 text-white text-center text-sm mt-5 flex justify-center items-center"
                }
              >
                <p>{language.WAITING_FOR_PROFESSIONAL}</p> <FiLoader className={"animate-spin text-xl mx-3"} />
              </p>
              <div className="flex w-full justify-center items-center my-3">
                <img src={LoadingGif} alt="loading" style={{ height: 100 }} />
              </div>
            </div>
            <div className="w-full">
              <div className="bg-gray-200 absolute" style={{ height: 10, width: "100%" }}></div>
              <div
                className="bg-green-800 absolute transition-all"
                style={{
                  height: 10,
                  width: (barWidth / progressBarDivider) * 100 + "%",
                }}
              ></div>
            </div>{" "}
            <div onClick={cancelRide} className={"bg-red-500 px-4 py-3 text-center text-white cursor-pointer"}>
              {language.CANCEL}
            </div>
          </div>
        </Page>
        {/* NO PROFESSIONAL ACCEPTED */}
        <Page page={page} currentPage={10} width={width} height={height} setPage={setPage}>
          <div className="bg-white dark:bg-gray-800 dark:text-white flex flex-col justify-center items-center overflow-hidden h-full">
            <div className="flex items-center">
              <FcHighPriority className={"text-4xl"} />
              <h1 className={"mx-2 text-lg"}>{language.BOOKING_EXPIRED}</h1>
            </div>
            <h1 className={"mt-2 mb-3 text-sm"}>{language.NO_PROFESSIONAL_ACCEPTED}</h1>
            <div className="flex mt-4">
              <button
                onClick={bookAnother}
                className={"w-full bg-blue-800 text-white rounded-md mx-2 text-sm px-4 py-2"}
              >
                {language.BOOK_ANOTHER_RIDE}
              </button>
              <button
                onClick={() => bookRide(walletOrCash)}
                className={"w-full bg-blue-800 text-white rounded-md mx-2 text-sm px-4 py-2"}
              >
                {language.RETRY}
              </button>
            </div>
          </div>
        </Page>
        {/* BOOKING COMPLETED  */}
        <Page page={page} currentPage={11} width={width} height={height} setPage={setPage}>
          <div className="bg-green-800 flex flex-col justify-center items-center overflow-hidden h-full">
            <div className="flex items-center">
              <FcApproval className={"text-4xl"} />
              <h1 className={"mx-2 text-white text-lg"}>{language.BOOKING_COMPLETED}</h1>
            </div>
            <h1 className={"my-5 text-white text-5xl"}>{bookingDetails !== null ? bookingDetails.bookingId : ""}</h1>
            <div className="flex mt-4">
              <Link
                to={bookingDetails !== null && NavLinks.ADMIN_RIDES_VIEW + "/" + bookingDetails._id}
                className={
                  "w-1/2 bg-white rounded-md mx-2 text-sm text-gray-600 px-4 py-2 flex justify-center items-center"
                }
              >
                <button>{language.VIEW_DETAILS}</button>
              </Link>
              <button
                onClick={bookAnother}
                className={"w-1/2 bg-white rounded-md mx-2 text-sm text-gray-600 px-4 py-2"}
              >
                {language.BOOK_ANOTHER_RIDE}
              </button>
            </div>
          </div>
        </Page>
      </div>
    </div>
  );
}
