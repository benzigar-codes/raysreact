import React from "react";
import { Link, useParams } from "react-router-dom";
import axios from "axios";

import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";
import NavLinks from "../../../../../../utils/navLinks.json";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import useAdmin from "../../../../../../hooks/useAdmin";
import useSocket from "../../../../../../hooks/useSocket";
import useSettings from "../../../../../../hooks/useSettings";
import useLanguage from "../../../../../../hooks/useLanguage";
import { FiInfo, FiLoader, FiX } from "react-icons/fi";

import LoadingGif from "../../../../../../assets/gifs/loading.gif";
import { FcApproval, FcHighPriority } from "react-icons/fc";

const Page = ({ page, height, width, currentPage, children, ...rest }) => (
  <div
    className={`absolute transition-all duration-300 ${currentPage !== page && "border-l-2"}`}
    style={{
      marginLeft: (currentPage - page) * width,
      width,
      height,
    }}
    {...rest}
  >
    {children}
  </div>
);

export default function Index() {
  const { id } = useParams();
  const { authFailure, header, token } = useAdmin();
  const [loading, setLoading] = React.useState(true);
  const [ride, setRide] = React.useState(null);
  const { notificationSocket } = useSocket();
  const { settings } = useSettings();
  const [barWidth, setBarWidth] = React.useState(0);
  const extraTime = 5000;
  const [page, setPage] = React.useState(1);

  const retryInterval = React.useRef();
  const progressBarInterval = React.useRef();
  const [bookingAccepted, setBookingAccepted] = React.useState(false);
  const { language } = useLanguage();

  const [height, setHeight] = React.useState(300);
  const [width, setWidth] = React.useState(400);

  const progressBarDivider =
    (parseInt(settings.bookingRetryCount) + 1) * (parseInt(settings.driverRequestTimeout) * 1000 + extraTime);

  const cancelRide = async () => {
    try {
      await axios.post(
        A.HOST + A.ADMIN_BOOK_EXPIRY,
        {
          bookingId: id,
          denyType: "USERCANCELLED",
        },
        header
      );
      setPage(3);
      clearInterval(retryInterval.current);
      clearInterval(progressBarInterval.current);
      setBarWidth(0);
    } catch (err) {
      authFailure(err);
    }
  };

  const fetchDetails = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_RIDES_READ, { bookingId: id }, header);
      setRide(data);
      if (data.bookingType === U.INSTANT && data.bookingStatus === U.AWAITING) {
        setPage(1);
        let initialCount = 0;
        const timeWait = +settings.driverRequestTimeout * 1000 + extraTime;

        const proBarInterval = setInterval(() => {
          setBarWidth((barW) => barW + 1000);
        }, 1000);

        progressBarInterval.current = proBarInterval;

        // RETRY INTERVAL
        const reInterval = setInterval(async () => {
          const retry = +settings.bookingRetryCount;
          if (initialCount === retry) {
            clearInterval(retryInterval.current);
            clearInterval(progressBarInterval.current);
            setBarWidth(0);
            if (bookingAccepted === false) {
              setPage(2);
              //   try {
              //     // await axios.post(
              //     //   A.HOST + A.ADMIN_BOOK_EXPIRY,
              //     //   {
              //     //     bookingId: id,
              //     //     denyType: "EXPIRED",
              //     //   },
              //     //   header
              //     // );
              //   } catch (err) {
              //     authFailure(err);
              //     alert(err);
              //   }
            }
          } else {
            try {
              await axios.post(
                A.HOST + A.ADMIN_BOOK_RETRY,
                {
                  bookingId: id,
                },
                header
              );
            } catch (err) {
              clearInterval(retryInterval.current);
              clearInterval(progressBarInterval.current);
              setPage(2);
              authFailure(err);
            }
            initialCount = initialCount + 1;
          }
        }, timeWait);
        retryInterval.current = reInterval;
        // setTimeout(() => {
        //   clearInterval(retryInterval.current);
        //   clearInterval(progressBarInterval.current);
        //   setBarWidth(0);
        // }, timeWait * +settings.bookingRetryCount);
      } else if (data.bookingType === U.SCHEDULE) setPage(4);
      else if (data.bookingType === U.INSTANT && data.bookingStatus !== U.AWAITING) setPage(4);
      setLoading(false);
    } catch (err) {
      authFailure(err);
      window.close();
    }
  };

  React.useEffect(() => {
    //Retry and Expire and Wait for Professional
    if (page === 1) {
      setHeight(300);
      setWidth(400);
    }
    //NO PROFESSIONAL ACCEPTED
    if (page === 2) {
      setHeight(280);
      setWidth(380);
    }
    //CANCELLED
    if (page === 3) {
      setHeight(280);
      setWidth(380);
    }
    //BOOK COMPLETED
    if (page === 4) {
      setHeight(280);
      setWidth(380);
    }
  }, [page]);

  React.useEffect(() => {
    notificationSocket.current.on(token, (data) => {
      // console.log(data);
      if (data.action === "ACCEPTBOOKING") {
        setBarWidth(0);
        setBookingAccepted(true);
        setPage(4);
        clearInterval(retryInterval.current);
        clearInterval(progressBarInterval.current);
        setBarWidth(0);
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [notificationSocket]);

  React.useEffect(() => {
    fetchDetails();
  }, []);
  return loading === true ? (
    <SmallLoader />
  ) : ride !== null ? (
    <div className="h-screen flex justify-center items-center bg-white dark:bg-gray-900 dark:text-white">
      <div
        className="relative rounded-xl flex overflow-hidden transition-all duration-300 shadow-xl"
        style={{
          marginLeft: 0,
          height,
          width,
        }}
      >
        {/* CHECK FOR PROFESSIONAL */}
        <Page page={page} currentPage={1} width={width} height={height} setPage={setPage}>
          <div className="bg-white dark:bg-gray-900 dark:text-white overflow-hidden w-full h-full flex flex-col justify-between">
            <div className={"bg-blue-800 px-4 py-3 text-center text-white flex items-center justify-center"}>
              <p>{language.YOUR_BOOKING_IS_PLACED}</p> <FiInfo className={"mx-2"} />
            </div>
            <div className={"h-full w-full"}>
              <p
                className={
                  "text-gray-600 dark:text-gray-200 text-white text-center text-sm mt-5 flex justify-center items-center"
                }
              >
                <p>{language.WAITING_FOR_PROFESSIONAL}</p> <FiLoader className={"animate-spin text-xl mx-3"} />
              </p>
              <div className="flex w-full justify-center items-center my-3">
                <img src={LoadingGif} alt="loading" style={{ height: 100 }} />
              </div>
            </div>
            <div className="w-full">
              <div className="bg-gray-200 absolute" style={{ height: 10, width: "100%" }}></div>
              <div
                className="bg-green-800 absolute transition-all"
                style={{
                  height: 10,
                  width: (barWidth / progressBarDivider) * 100 + "%",
                }}
              ></div>
            </div>{" "}
            <div onClick={cancelRide} className={"bg-red-500 px-4 py-3 text-center text-white cursor-pointer"}>
              {language.CANCEL}
            </div>
          </div>
        </Page>
        {/* NO PROFESSIONAL ACCEPTED */}
        <Page page={page} currentPage={2} width={width} height={height} setPage={setPage}>
          <div className="bg-white dark:bg-gray-800 dark:text-white flex flex-col justify-center items-center overflow-hidden h-full">
            <div className="flex items-center">
              <FcHighPriority className={"text-4xl"} />
              <h1 className={"mx-2 text-lg"}>{language.BOOKING_EXPIRED}</h1>
            </div>
            <h1 className={"mt-2 mb-3 text-sm"}>{language.NO_PROFESSIONAL_ACCEPTED}</h1>
            <div className="flex mt-4">
              <button
                onClick={() => fetchDetails()}
                className={"w-full bg-blue-800 text-white rounded-md mx-2 text-sm px-4 py-2"}
              >
                {language.RETRY}
              </button>
              <button
                onClick={() => {
                  window.close();
                  window.open(NavLinks.ADMIN_RIDES_SCHEDULED_ASSIGN_DRIVER + "/" + ride._id);
                }}
                className={"w-full bg-blue-800 text-white rounded-md mx-2 text-sm px-4 py-2"}
              >
                {language.ASSIGN}
              </button>
              <button
                onClick={cancelRide}
                className={"w-full bg-blue-800 text-white rounded-md mx-2 text-sm px-4 py-2"}
              >
                {language.CANCEL}
              </button>
            </div>
          </div>
        </Page>
        {/* CANCELLED  */}
        <Page page={page} currentPage={3} width={width} height={height} setPage={setPage}>
          <div className="bg-red-500 flex flex-col justify-center items-center overflow-hidden h-full">
            <div className="flex items-center">
              <FiX className={"text-4xl"} />
              <h1 className={"mx-2 text-white text-lg"}>{language.BOOKING_CANCELLED}</h1>
            </div>
            <h1 className={"my-5 text-white text-5xl"}>{ride.bookingId || ""}</h1>
            <div className="flex mt-4">
              <div
                onClick={() => {
                  window.open(NavLinks.ADMIN_RIDES_VIEW + "/" + id);
                  window.close();
                }}
                className={
                  "w-full bg-white rounded-md mx-2 text-sm text-gray-600 px-4 py-2 flex justify-center items-center"
                }
              >
                <button>{language.VIEW_DETAILS}</button>
              </div>
            </div>
            <p className="my-5 text-sm text-center">You can close this window</p>
          </div>
        </Page>
        {/* BOOKING COMPLETED  */}
        <Page page={page} currentPage={4} width={width} height={height} setPage={setPage}>
          <div className="bg-green-800 flex flex-col justify-center items-center overflow-hidden h-full">
            <div className="flex items-center">
              <FcApproval className={"text-4xl"} />
              <h1 className={"mx-2 text-white text-lg"}>{language.BOOKING_COMPLETED}</h1>
            </div>
            <h1 className={"my-5 text-white text-5xl"}>{ride.bookingId || ""}</h1>
            <div className="flex mt-4">
              <div
                onClick={() => {
                  window.open(NavLinks.ADMIN_RIDES_VIEW + "/" + id);
                  window.close();
                }}
                className={
                  "w-full bg-white rounded-md mx-2 text-sm text-gray-600 px-4 py-2 flex justify-center items-center"
                }
              >
                <button>{language.VIEW_DETAILS}</button>
              </div>
            </div>
            <p className="my-5 text-sm text-center">You can close this window</p>
          </div>
        </Page>
      </div>
    </div>
  ) : null;
}
