import React from "react";
import { FiArrowUp, FiHome, FiLoader, FiSave } from "react-icons/fi";
import axios from "axios";
import { Loader } from "@googlemaps/js-api-loader";
import MarkerCluster from "@googlemaps/markerclustererplus";
import * as turf from "@turf/turf";
import { Link } from "react-router-dom";

import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../components/common/TextField";
import carIcon from "../../../../../assets/images/car-icon.png";

import useAdmin from "../../../../../hooks/useAdmin";

import NavLinks from "../../../../../utils/navLinks.json";
import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import { DropdownNormal } from "../../../../../components/common/DropDownNormal";
import { ToggleButton } from "../../../../../components/common/ToggleButton";
import { Button } from "../../../Login/Button";
import useLanguage from "../../../../../hooks/useLanguage";
import useSettings from "../../../../../hooks/useSettings";
import GooglePlaceComplete from "../../../../../components/common/GooglePlaceComplete";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import { PopUp } from "../../../../../components/common/PopUp";
import useImage from "../../../../../hooks/useImage";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const [cities, setCities] = React.useState([]);
  const { language } = useLanguage();
  const { settings } = useSettings();
  const { morph } = useUtils();
  const [popup, setPop] = React.useState(null);
  const [iconData, setIconData] = React.useState(null);
  const { header, authFailure } = useAdmin();
  const { imageUrl } = useImage();
  const [map, setMap] = React.useState(null);
  const mapRef = React.useRef(null);
  const [loading, setLoading] = React.useState(true);
  const [driversLoading, setDriversLoading] = React.useState(false);
  const [categories, setCategories] = React.useState(null);

  const [cluster, setCluster] = React.useState(null);
  const [mapMarkers, setMapMarkers] = React.useState(null);
  const [selectedCity, setSelectedCity] = React.useState(null);
  const [googleLoaded, setGoogleLoaded] = React.useState(false);
  const [polygon, setPolygon] = React.useState(null);

  const [showFilter, setShowFilter] = React.useState(true);

  const [status, setStatus] = React.useState("ALL");
  const [onlineOffline, setOnlineOffline] = React.useState("ALL");
  const [selectedCategory, setSelectedCategory] = React.useState("ALL");
  const [onGoing, setOnGoing] = React.useState(0);

  const activeStatusList = [
    {
      label: language.ALL,
      value: "ALL",
    },
    {
      label: language.ACTIVE,
      value: U.ACTIVE,
    },
    {
      label: language.INACTIVE,
      value: U.INACTIVE,
    },
  ];

  const onlineStatusList = [
    {
      label: language.ALL,
      value: "ALL",
    },
    {
      label: language.ONLINE,
      value: "ONLINE",
    },
    {
      label: language.OFFLINE,
      value: "OFFLINE",
    },
  ];

  document.title = language.SATELLITE_VIEW;

  React.useEffect(() => {
    const fetchCities = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_CITY_LIST, {}, header);
        setCities(data.response);
        setSelectedCity(data.response[0]._id);
      } catch (err) {
        authFailure(err);
      }
    };

    fetchCities();
    const loader = new Loader({
      apiKey: settings.mapApi.web,
      version: "weekly",
      libraries: ["places"],
    });
    loader
      .load()
      .then(() => {
        setGoogleLoaded(true);
      })
      .catch((e) => {});
  }, []);

  React.useEffect(() => {
    if (cities !== null) {
      setLoading(false);
    }
  }, [cities]);

  React.useEffect(() => {
    if (googleLoaded === true && loading === false) {
      const googleMap = new window.google.maps.Map(mapRef.current, {
        center: { lat: 12.9975729, lng: 80.2638949 },
        zoom: 14,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: true,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: true,
      });
      setMap(googleMap);
    }
  }, [googleLoaded, loading]);

  const zoomInCity = (geoJSON) => {
    const location = geoJSON.results[0].geometry.location;
    const coordinates = cities.filter((city) => city._id === selectedCity)[0].location.coordinates;
    var points = turf.points([[location.lng, location.lat]]);
    var searchWithin = turf.polygon(coordinates);
    var ptsWithin = turf.pointsWithinPolygon(points, searchWithin);
    if (ptsWithin.features.length > 0) {
      map.setCenter({ lat: location.lat, lng: location.lng });
      map.setZoom(14);
    } else {
      setPop({ title: language.SEARCH_NOT_IN_CITY, type: "error" });
    }
  };

  const drawMap = async ({ fetch = false }) => {
    if (map !== null && selectedCity !== null) {
      if (polygon !== null) {
        polygon.setMap(null);
      }
      if (cluster !== null || mapMarkers !== null) {
        cluster.setMap(null);
        mapMarkers.forEach((eachMarker) => eachMarker.setMap(null));
      }
      const city = cities.filter((each) => each._id === selectedCity)[0];
      const coordinates = city.location.coordinates;
      const coordinatesForPath = coordinates[0].map((each) => ({ lat: each[1], lng: each[0] }));
      const newPolygon = new window.google.maps.Polygon({
        paths: coordinatesForPath,
        map,
        strokeColor: "#262525",
        strokeOpacity: 0.8,
        strokeWeight: 5,
        fillColor: "#fc8621",
        fillOpacity: 0.5,
      });
      var bounds = new window.google.maps.LatLngBounds();
      coordinatesForPath.forEach((poly) => bounds.extend(poly));
      fetch === true && map.fitBounds(bounds);
      setPolygon(newPolygon);
      setDriversLoading(true);
      try {
        let target = iconData;
        if (fetch === true) {
          const { data } = await axios.post(
            A.HOST + A.ADMIN_SATELLITE_VIEW,
            {
              city: selectedCity,
            },
            header
          );
          target = data;
          setIconData(data);
          setCategories([
            {
              category: language.ALL,
              _id: "ALL",
            },
            ...data.vehicleCategory,
          ]);
          setSelectedCategory("ALL");
        }
        setDriversLoading(false);
        if (target.professional.length > 0) {
          let finalMarkers = target.professional;
          if (selectedCategory !== "ALL")
            finalMarkers = finalMarkers.filter((each) => each.vehicleCategoryId === selectedCategory);
          if (status === U.ACTIVE) finalMarkers = finalMarkers.filter((each) => each.status === U.ACTIVE);
          if (status === U.INACTIVE) finalMarkers = finalMarkers.filter((each) => each.status === U.INACTIVE);
          if (onlineOffline === "ONLINE") finalMarkers = finalMarkers.filter((each) => each.onlineStatus === true);
          if (onlineOffline === "OFFLINE") finalMarkers = finalMarkers.filter((each) => each.onlineStatus === false);
          if (onGoing) finalMarkers = finalMarkers.filter((each) => each.isOngoingBookingStatus !== null);
          const markers = finalMarkers.map((professional) => {
            const marker = new window.google.maps.Marker({
              position: { lat: professional.location.coordinates[1], lng: professional.location.coordinates[0] },
              icon: {
                url: carIcon,
                scaledSize: new window.google.maps.Size(35, 35),
              },
            });
            const infowindow = new window.google.maps.InfoWindow({
              content: professional.firstName + " " + professional.lastName + " " + professional.phone.number,
            });
            marker.addListener("click", () => {
              infowindow.open(map, marker);
            });
            return marker;
          });
          setMapMarkers(markers);
          const clusterMarker = new MarkerCluster(map, markers, {
            imagePath: "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
          });
          setCluster(clusterMarker);
        }
      } catch (err) {}
    }
  };

  React.useEffect(() => {
    drawMap({ fetch: true });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cities, map, selectedCity]);

  React.useEffect(() => {
    if (status === U.INACTIVE) {
      setOnlineOffline("ALL");
      setOnGoing(false);
    }
  }, [status]);

  React.useEffect(() => {
    drawMap({ fetch: false });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status, onlineOffline, onGoing, selectedCategory]);

  return loading === true || googleLoaded === false ? (
    <SmallLoader />
  ) : (
    <div className="h-screen bg-gray-100 dark:bg-gray-900 flex flex-col">
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <div className={"w-full flex flex-col justify-between"}>
        <div className="flex justify-between items-center bg-white dark:bg-gray-800 dark:text-white p-2 px-10 w-full">
          <div className={"flex items-center cursor-pointer"}>
            <Link to="/admin/home">
              <img
                className="h-8"
                src={
                  document.getElementsByTagName("html")[0].classList.contains("dark")
                    ? imageUrl(settings.darkLogo)
                    : imageUrl(settings.lightLogo)
                }
                alt=""
              />
            </Link>
            {/* <FiHome onClick={() => history.push(NavLinks.ADMIN_HOME)} className={"mx-2 hover:text-blue-800"} /> */}
          </div>
          <div>
            <h1>{language.SATELLITE_VIEW}</h1>
          </div>
          <div className={"flex items-center dark:text-white"}>
            {driversLoading === true && <FiLoader className={"text-xl animate-spin mx-4"} />}
          </div>
        </div>
      </div>
      <div className="flex h-full">
        <div
          className={`${
            showFilter === true ? "w-1/4" : "w-0"
          } h-full bg-gray-50 dark:bg-gray-800 border-t-2 dark:text-white`}
        >
          <FieldWrapper title={language.CITY}>
            <DropdownNormal
              change={(e) => setSelectedCity(e)}
              defaultValue={
                cities.filter((city) => city._id === selectedCity).length > 0
                  ? cities.find((city) => city._id === selectedCity).locationName
                  : ""
              }
              fields={cities.map((city) => ({ label: morph(city.locationName), value: city._id }))}
            />
          </FieldWrapper>
          {categories !== null && (
            <FieldWrapper title={language.VEHICLE_CATEGORY}>
              <DropdownNormal
                change={(e) => setSelectedCategory(e)}
                defaultValue={
                  categories.filter((category) => category._id === selectedCategory).length > 0
                    ? categories.find((category) => category._id === selectedCategory).category
                    : ""
                }
                fields={categories.map((each) => ({ label: each.category, value: each._id }))}
              />
            </FieldWrapper>
          )}
          <FieldWrapper title={language.ACTIVE_INACTIVE}>
            <DropdownNormal
              fields={activeStatusList}
              change={(e) => setStatus(e)}
              defaultValue={activeStatusList.find((each) => each.value === status).label}
            ></DropdownNormal>
          </FieldWrapper>
          {(status === U.ACTIVE || status === "ALL") && (
            <>
              <FieldWrapper title={language.ONLINE_OFFLINE}>
                <DropdownNormal
                  fields={onlineStatusList}
                  change={(e) => setOnlineOffline(e)}
                  defaultValue={onlineStatusList.find((each) => each.value === onlineOffline).label}
                ></DropdownNormal>
              </FieldWrapper>
              {(onlineOffline === "ALL" || onlineOffline === "ONLINE") && (
                <FieldWrapper title={language.SHOW_ONLY_ONGOING_DRIVERS}>
                  <ToggleButton defaultValue={onGoing} change={(e) => setOnGoing(e)} />
                </FieldWrapper>
              )}
            </>
          )}
          <hr className={"mt-5 dark:bg-gray-900"} />
          <FieldWrapper title={language.ZOOM_IN_AREA}>
            <GooglePlaceComplete
              placeholder={language.ADDRESS}
              change={zoomInCity}
              style={{
                width: "full",
              }}
            />
          </FieldWrapper>
        </div>
        {showFilter === true && (
          <p
            onClick={() => setShowFilter(false)}
            className={"bg-blue-800 p-3 cursor-pointer fixed text-white z-10 flex items-center shadow-xl"}
            style={{ top: "45%", left: "25%", writingMode: "tb-rl", transform: "rotateZ(-180deg)" }}
          >
            <FiArrowUp className={"mb-2"} style={{ transform: "rotate(90deg)" }} />
            {language.HIDE_FILTERS}
          </p>
        )}{" "}
        {showFilter === false && (
          <p
            onClick={() => setShowFilter(true)}
            className={"bg-blue-800 p-3 cursor-pointer fixed text-white z-10 flex items-center shadow-xl"}
            style={{ top: "45%", left: "0", writingMode: "tb-rl", transform: "rotateZ(-180deg)" }}
          >
            <FiArrowUp className={"mb-2"} style={{ transform: "rotate(270deg)" }} />
            {language.SHOW_FILTERS}
          </p>
        )}
        <div
          ref={mapRef}
          className={`${showFilter === true ? "w-3/4" : "w-full"} overflow-y-hidden h-full bg-white dark:bg-gray-900`}
          id="#godsViewMap"
        ></div>
      </div>
    </div>
  );
}
