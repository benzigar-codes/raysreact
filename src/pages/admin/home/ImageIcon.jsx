import React from 'react'

const ImageIcon = ({ src, ...rest }) => {
  return (
    <img
      {...rest}
      className="h-10 w-10 object-cover rounded-full cursor-pointer"
      src={src}
      alt=""
    />
  );
};

export default ImageIcon