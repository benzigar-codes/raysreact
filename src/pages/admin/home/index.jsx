import React, { useState } from "react";
import { useLocation } from "react-router-dom";
import io from "socket.io-client";
import useLanguage from "../../../hooks/useLanguage";
import { AdminHomeRoutes } from "../../../routes/Routes";
import AdminNavBar from "./AdminNavBar";
import AdminTopMenu from "./AdminTopMenu";

import { useHotkeys } from "react-hotkeys-hook";

import NavLinks from "../../../utils/navLinks.json";

import A from "../../../utils/API.js";
import { Support } from "../../../components/Support";
import useAdmin from "../../../hooks/useAdmin";

const Home = ({ match, history }) => {
  const [nav, toggleNav] = useState(false);
  const [selected, setSelected] = React.useState(1);
  const { admin } = useAdmin();
  const { language } = useLanguage();
  const { pathname } = useLocation();

  useHotkeys("ctrl+m", () => toggleNav(true));

  const fullScreenPages = [
    NavLinks.ADMIN_SATILLITE_VIEW,
    NavLinks.ADMIN_EAGLE_VIEW,
    NavLinks.ADMIN_BOOK_RIDE,
    NavLinks.ADMIN_DISPATCH,
  ];
  const showPageOnMobile = [NavLinks.ADMIN_BOOK_RIDE_ACCEPT_DETAILS];

  // alert(pathname);
  document.title = language.HOME;

  return showPageOnMobile.filter((each) => pathname.includes(each)).length > 0 ? (
    <div className="h-screen overflow-y-scroll">
      <AdminHomeRoutes match={{ match }} />
    </div>
  ) : fullScreenPages.includes(pathname) ? (
    <>
      <div className="h-screen overflow-y-scroll">
        <AdminHomeRoutes match={{ match }} />
      </div>
      <div className="fixed inset-0 z-10 bg-white lg:hidden dark:text-white flex justify-center items-center h-screen text-center p-10">
        {language.SCREEN_SUPPORT}
      </div>
    </>
  ) : (
    <>
      {nav === true && (
        <AdminNavBar selected={selected} setSelected={(id) => setSelected(id)} closeNavBar={() => toggleNav(false)} />
      )}
      <div className="hidden lg:block bg-white dark:bg-gray-800 dark:text-white">
        <AdminTopMenu history={history} nav={nav} toggleNav={toggleNav} />
        <div style={{ marginTop: "70px" }}>
          <AdminHomeRoutes match={{ match }} />
        </div>
      </div>
      <div className="fixed inset-0 z-10 lg:hidden bg-white flex justify-center items-center h-screen text-center p-10">
        {language.SCREEN_SUPPORT}
      </div>
      {admin && admin.privileges && admin.privileges.OTHERS && admin.privileges.OTHERS.SUPPORT.VIEW === true && (
        <Support />
      )}
    </>
  );
};

export default Home;
