import React from "react";

export default function Heading({title,className,...rest}) {
    return <h1 className={`text-2xl m-5 ${className}`} {...rest}>The Heading</h1>;
}
