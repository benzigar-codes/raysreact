import React from "react";
import gsap from "gsap";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import useLanguage from "../../../../../../hooks/useLanguage";

import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";
import NavLinks from "../../../../../../utils/navLinks.json";

import axios from "axios";
import useAdmin from "../../../../../../hooks/useAdmin";
import { useParams } from "react-router";
import { Border } from "../../../../../../components/common/Border";
import { Heading } from "../../../../../../components/common/Heading";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { Prompt } from "../../../../../../components/common/Prompt";
import { Detail } from "../../../../../../components/common/Detail";
import { DetailsWrapper } from "../../../../../../components/common/DetailsWrapper";
import { TextArea } from "../../../../../../components/common/TextArea";
import { Button } from "../../../../../../components/common/Button";
import { FiAlertCircle, FiCheckCircle } from "react-icons/fi";
import usePrompt from "../../../../../../hooks/usePrompt";

export default function Index() {
  const { language } = useLanguage();
  const { id } = useParams();
  const { header, authFailure } = useAdmin();
  const [feedback, setFeedback] = React.useState(null);
  const { prompt, showPrompt } = usePrompt();
  const [loading, setLoading] = React.useState(true);
  const [notesText, setNotesText] = React.useState("");
  const [notesLoading, setNotesLoading] = React.useState(false);
  const fetchFeedback = async () => {
    try {
      setLoading(true);
      const { data } = await axios.post(A.HOST + A.ADMIN_REPORTS_FEEDBACK_VIEW, { id }, header);
      setFeedback(data.response);
      setNotesText(data.response?.adminNotes);
      setLoading(false);
    } catch (err) {
      authFailure(err);
    }
  };
  const noteSave = async () => {
    setNotesLoading(true);
    try {
      await axios.post(
        A.HOST + A.ADMIN_REPORTS_FEEDBACK_NOTES_ADD,
        {
          id: id,
          notes: notesText,
        },
        header
      );
    } catch (err) {}
    setNotesLoading(false);
  };

  const statusChange = async () => {
    try {
      await axios.post(
        A.HOST + A.ADMIN_REPORTS_FEEBACK_STATUS_CHANGE,
        {
          ids: [id],
          status: feedback.status === U.INCOMPLETE || feedback.status === U.NEW ? U.ATTENDED : U.INCOMPLETE,
        },
        header
      );
      fetchFeedback();
    } catch (err) {
      authFailure(err);
    }
  };

  React.useEffect(() => fetchFeedback(), [id]);

  React.useEffect(() => {
    loading === false && gsap.fromTo(".viewFeedback", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, [loading]);
  return loading ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      bread={[{ id: 1, title: language.FEEDBACK, path: NavLinks.ADMIN_REPORTS_FEEDBACK }]}
      width="3/4"
      title={feedback.firstName + " " + feedback.lastName}
    >
      {prompt.show === true && (
        <Prompt
          title={prompt.title}
          message={prompt.message}
          type={prompt.type}
          close={prompt.close}
          click={prompt.click}
        />
      )}
      {/* <Section>{JSON.stringify(feedback)}</Section> */}
      <Section width="1/2">
        <Border animate="viewFeedback">
          <Heading title={feedback.userType === "USER" ? language.USER_DETAILS : language.PROFESSIONAL_DETAILS} />
          <FieldWrapper>
            <DetailsWrapper>
              <Detail title={language.FIRSTNAME} value={feedback.firstName} />
              <Detail title={language.LASTNAME} value={feedback.lastName} />
              <Detail title={language.PHONE} value={feedback.phone?.code + " " + feedback.phone?.number} />
              <Detail
                title={language.STATUS}
                value={
                  feedback.status === U.ATTENDED ? (
                    <div className="flex items-center mt-1">
                      <FiCheckCircle className="text-xl text-purple-800 dark:text-purple-500" />
                      <p className="mx-2">{language.ATTENDED}</p>
                    </div>
                  ) : (
                    <div className="flex items-center mt-1">
                      <FiAlertCircle className="text-xl text-red-500" />
                      <p className="mx-2">{language.INCOMPLETE}</p>
                      <button
                        onClick={() =>
                          showPrompt(language.ATTEND, language.PROMPT_ATTEND, "status", () => statusChange())
                        }
                        className="bg-blue-800 mx-3 rounded-xl p-2 text-white"
                        style={{ fontSize: 12 }}
                      >
                        {language.MARK_AS_ATTENDED}
                      </button>
                    </div>
                  )
                }
              />
            </DetailsWrapper>
          </FieldWrapper>
        </Border>
        <Border animate="viewFeedback">
          <Heading title={language.NOTES} />
          <FieldWrapper>
            <TextArea change={(e) => setNotesText(e)} value={notesText} />
          </FieldWrapper>
          <FieldWrapper>
            <Button onClick={noteSave} loading={notesLoading} title={language.SAVE} />
          </FieldWrapper>
        </Border>
      </Section>
      <Section width="1/2">
        <Border animate="viewFeedback">
          <Heading title={language.COMMENT} />
          <p className="px-4 text-sm my-3">{feedback.comment}</p>
        </Border>
      </Section>
    </FormWrapper>
  );
}
