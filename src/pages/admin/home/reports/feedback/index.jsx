import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import { format } from "date-fns";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { truncate, morph } = useUtils();
  return (
    <>
      <Table
        title={language.FEEDBACK}
        startingHeadings={[
          {
            id: 4,
            title: language.USER_TYPE,
            key: "userType",
            show: true,
          },
          {
            id: 1,
            title: language.NAME,
            key: "name",
            show: true,
          },
          {
            id: 2,
            title: language.PHONE,
            key: "phone",
            show: true,
          },
          {
            id: 3,
            title: language.COMMENT,
            key: "command",
            show: true,
          },
          {
            id: 5,
            title: language.STATUS,
            key: "unregister_status",
            show: true,
          },
        ]}
        list={A.HOST + A.ADMIN_REPORTS_FEEDBACK}
        assignData={(data) =>
          data.map((feedback) => ({
            _id: feedback._id,
            name: morph(feedback.data.firstName + " " + feedback.data.lastName),
            phone: morph(feedback.data.phone.code) + " " + morph(feedback.data.phone.number),
            command: truncate(feedback.data.comment, 30),
            userType: language[feedback.data.userType],
            notes: feedback.data.adminNotes,
            unregister_status: feedback.data.status,
          }))
        }
        bread={[{ id: 1, title: language.REPORTS }]}
        showAdd={false}
        showView={true}
        showArchieve={false}
        viewClick={(e) => history.push(NavLinks.ADMIN_REPORTS_FEEDBACK_VIEW + "/" + e)}
        showEdit={false}
        showFilter={false}
        showBulk={false}
        showSearch={true}
        showAction={admin.privileges.REPORTS && admin.privileges.REPORTS.FEEDBACK.VIEW === true ? true : false}
        unregStatus={A.HOST + A.ADMIN_REPORTS_FEEBACK_STATUS_CHANGE}
        showStatus={false}
        showNotes={admin.privileges.REPORTS && admin.privileges.REPORTS.FEEDBACK.VIEW === true ? true : false}
        notesLink={A.HOST + A.ADMIN_REPORTS_FEEDBACK_NOTES_ADD}
      />
    </>
  );
}
