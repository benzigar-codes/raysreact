import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import { format } from "date-fns";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <Table
      title={language.REFUNDS}
      startingHeadings={[
        {
          id: 1,
          title: language.NAME,
          key: "name",
          show: true,
        },
        {
          id: 2,
          title: language.CARD_NUMBER,
          key: "cardNumber",
          show: true,
        },
        {
          id: 3,
          title: language.AMOUNT,
          key: "amount",
          show: true,
        },
        {
          id: 3,
          title: language.TIME,
          key: "time",
          show: true,
        },
        {
          id: 4,
          title: language.STATUS,
          key: "verifyStatus",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_ASSET_REFUND}
      assignData={(data) =>
        data.map((refund) => ({
          _id: refund._id,
          name: refund.name,
          cardNumber: refund.cardNumber,
          amount: refund.amount,
          time: format(new Date(refund.time), "do MMM, yyyy p"),
          verifyStatus: (
            <p
              style={{
                color:
                  refund.status === "SUCCESS"
                    ? "green"
                    : refund.status === "FAILED"
                    ? "red"
                    : undefined,
              }}
            >
              {language[refund.status]}
            </p>
          ),
        }))
      }
      bread={[]}
      showAdd={false}
      showUserProfessionalFilter={true}
      showStatus={true}
      showView={true}
      viewClick={(e) => history.push(NavLinks.ADMIN_ASSET_TRANSACTIONS_VIEW + "/" + e)}
      showActiveInactiveFilter={false}
      showAction={false}
      showReimburseFilter={false}
      showArchieve={false}
      defaultShowField={language.USER + " " + language.ALL + " " + language.REFUNDS}
      showDateFilter={false}
      editClick={(e) => history.push(NavLinks.ADMIN_ADMIN_ROLE_EDIT + "/" + e)}
      showBulk={false}
      walletDashboard={false}
      showRefundStatusFilter={true}
      showSearch={true}
      statusList={A.HOST + A.ADMIN_ROLE_STATUS}
    />
  );
}
