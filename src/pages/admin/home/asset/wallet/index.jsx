import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import { format } from "date-fns";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <Table
      title={language.WALLET}
      startingHeadings={[
        {
          id: 2,
          title: language.TNX_ID,
          key: "tnx_id",
          show: true,
        },
        {
          id: 3,
          title: language.FROM,
          key: "oneByOne",
          show: true,
        },
        {
          id: 4,
          title: language.TO,
          key: "oneByOne2",
          show: true,
        },
        {
          id: 5,
          title: language.TNX_DATE,
          key: "oneByOne3",
          show: true,
        },
        {
          id: 6,
          title: language.TNX_AMOUNT,
          key: "tnx_amount",
          show: true,
        },
        {
          id: 7,
          title: language.TXN_TYPE,
          key: "txn_type",
          show: true,
        },
        {
          id: 8,
          title: language.REIMBURSEMENT,
          key: "isReimbursement",
          show: true,
        },
        {
          id: 9,
          title: language.STATUS,
          key: "tnx_status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_ASSET_WALLET}
      assignData={(data) =>
        data.map((wallet) => ({
          _id: wallet._id,
          tnx_id: wallet.transactionId,
          oneByOne: wallet.from.name
            ? [morph(wallet.from.name), morph(wallet.from.phoneNumber)]
            : [""],
          oneByOne2: wallet.to.name
            ? [morph(wallet.to.name), morph(wallet.to.phoneNumber)]
            : [""],
          oneByOne3: [
            format(new Date(wallet.transactionDate), "do MMM yyyy"),
            format(new Date(wallet.transactionDate), "hh : mm aa"),
          ],
          isReimbursement: wallet.isReimbursement ? language.YES : language.NO,
          txn_type: wallet.transactionType,
          tnx_amount: wallet.transactionAmount,
          tnx_status: language.SUCCESS,
        }))
      }
      bread={[]}
      showAdd={false}
      showUserProfessionalFilter={true}
      showStatus={true}
      showActiveInactiveFilter={false}
      showColumnHeadings={false}
      showReimburseFilter={true}
      showArchieve={false}
      defaultShowField={language.USER + " " + language.SUCCESS}
      showDateFilter={true}
      showView={true}
      viewClick={(e) => history.push(NavLinks.ADMIN_ASSET_TRANSACTIONS_VIEW + "/" + e)}
      showBulk={false}
      walletDashboard={true}
      showWalletActiveInactive={true}
      showSearch={true}
      showAction={false}
      statusList={A.HOST + A.ADMIN_ROLE_STATUS}
    />
  );
}
