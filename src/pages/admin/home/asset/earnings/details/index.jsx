import React from "react";
import { Waypoint } from "react-waypoint";
import gsap from "gsap";

import useLanguage from "../../../../../../hooks/useLanguage";
import useAdmin from "../../../../../../hooks/useAdmin";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";

import NavLinks from "../../../../../../utils/navLinks.json";
import A from "../../../../../../utils/API.js";
import axios from "axios";
import { format } from "date-fns";
import useUtils from "../../../../../../hooks/useUtils";

export default function Index({ history, location }) {
  const { language } = useLanguage();
  const state = location.state ? location.state : history.goBack();
  const { header, authFailure } = useAdmin();
  const { paymentSectionFields } = useUtils();
  const [loading, setLoading] = React.useState(true);
  const [content, setContent] = React.useState(null);
  const [paymentSectionFilter, setPaymentSectionFitler] = React.useState(
    location?.state?.paymentSectionFilter ?? "ALL"
  );
  React.useEffect(() => {
    const fetchSummary = async () => {
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_EARNINGS_SUMMARY,
          { ...state, paymentSectionFilter: paymentSectionFilter },
          header
        );
        setContent(data);
        setLoading(false);
      } catch (err) {
        authFailure(err);
      }
    };
    fetchSummary();
  }, [paymentSectionFilter]);
  React.useEffect(() => {
    loading === false && gsap.fromTo(".earnings", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, [loading]);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <>
      <div className="flex justify-between px-5 py-3 items-center">
        <div className="flex">
          <h1>{language.REVENUE_SUMMARY}</h1>
        </div>
        {/* <DropDown */}
        {/* <div className="flex">
          <h1 className="mx-2 bg-blue-800 px-4 py-2 text-white cursor-pointer">Payment Summary</h1>
          <h1 className="mx-2 bg-gray-200 px-4 py-2 text-gray-900 cursor-pointer">Settlement History</h1>
        </div> */}
        <div className="flex">
          {paymentSectionFields.map((each) => (
            <h1
              onClick={() => setPaymentSectionFitler(each.value)}
              className={`mx-2 ${
                paymentSectionFilter === each.value ? "bg-blue-800 text-gray-100" : "bg-gray-200 text-gray-900"
              } px-4 py-2 cursor-pointer`}
            >
              {each.label}
            </h1>
          ))}
        </div>
      </div>
      <div className="px-5">
        <div
          className="w-full shadow-md mt-2 rounded-lg overflow-y-scroll opacity-0 earnings"
          style={{ maxHeight: "90vh" }}
        >
          <table
            cellPadding="12"
            className="w-full relative text-gray-800 text-left dark:bg-gray-800 z-10 overflow-y-scroll"
            style={{ fontSize: 13 }}
          >
            <thead className="z-10">
              <tr>
                <td className="text-center sticky top-0 bg-blue-800 text-white">{language.TABLE_NO}</td>

                <td className="sticky top-0 bg-blue-800 text-white">{language.BOOKING_ID}</td>
                <td className="sticky top-0 bg-blue-800 text-white">{language.DATE}</td>
                <td className="sticky top-0 bg-blue-800 text-white">
                  <p>{language.ACTUAL_TRIP}</p>
                  <p>{language.AMOUNT}</p>
                </td>
                <td className="sticky top-0 bg-blue-800 text-white">{language.DISCOUNT}</td>
                <td className="sticky top-0 bg-blue-800 text-white">
                  <p>{language.TOTAL_TRIP}</p>
                  <p>{language.AMOUNT}</p>
                </td>
                <td className="sticky top-0 bg-blue-800 text-white">{language.SITE_EARNINGS}</td>
                <td className="sticky top-0 bg-blue-800 text-white">{language.TIPS}</td>
                <td className="sticky top-0 bg-blue-800 text-white">
                  <p>{language.REIMBURSEMENT}</p>
                  <p>{language.FOR_DISCOUNT}</p>
                </td>
                <td className="sticky top-0 bg-blue-800 text-white">{language.DRIVER_EARNINGS}</td>
              </tr>
            </thead>
            <tbody className="text-sm text-gray-600 dark:bg-gray-800">
              {content?.response?.map((each, index) => (
                <tr className="bg-white dark:bg-gray-800 dark:text-gray-200 dark:border-gray-900 hover:bg-gray-100 cursor-pointer border-b-4 border-gray-100">
                  <td onClick={() => {}} className="text-center">
                    {index + 1}
                  </td>
                  <td onClick={() => {}}>{each.bookingId}</td>
                  <td onClick={() => {}}>{format(Date.parse(each.bookingDate), "dd LLL, hh : mm a")}</td>
                  <td onClick={() => {}}>{parseFloat(each.estimationPayableAmount).toFixed(2)}</td>
                  <td onClick={() => {}}>{parseFloat(each.discountAmount).toFixed(2)}</td>
                  <td onClick={() => {}}>{parseFloat(each.totalFare).toFixed(2)}</td>
                  <td onClick={() => {}}>{parseFloat(each.siteCommission).toFixed(2)}</td>
                  <td onClick={() => {}}>{parseFloat(each.tipsAmount).toFixed(2)}</td>
                  <td onClick={() => {}}>{parseFloat(each.professionalTolerenceAmount).toFixed(2)}</td>
                  <td onClick={() => {}}>{parseFloat(each.professionalCommision).toFixed(2)}</td>
                </tr>
              ))}

              {/* <Waypoint onEnter={() => {}}>
          <tr>
            <td align="center">
              <FiLoader className="animate-spin" />
            </td>
          </tr>
        </Waypoint> */}
              {content.summaryResponse.estimationPayableAmount ? (
                <tr className="bg-white dark:bg-gray-800 dark:text-gray-200 dark:border-gray-900 hover:bg-gray-100 cursor-pointer border-b-4 border-gray-100">
                  <td onClick={() => {}} colSpan="3">
                    {language.TRIPS_SUMMARY}
                  </td>
                  <td onClick={() => {}}>{parseFloat(content.summaryResponse.estimationPayableAmount).toFixed(2)}</td>
                  <td onClick={() => {}}>{parseFloat(content.summaryResponse.discountAmount).toFixed(2)}</td>
                  <td onClick={() => {}}>{parseFloat(content.summaryResponse.totalFare).toFixed(2)}</td>
                  <td onClick={() => {}}>{parseFloat(content.summaryResponse.siteCommission).toFixed(2)}</td>
                  <td onClick={() => {}}>{parseFloat(content.summaryResponse.tipsAmount).toFixed(2)}</td>
                  <td onClick={() => {}}>
                    {parseFloat(content.summaryResponse.professionalTolerenceAmount).toFixed(2)}
                  </td>
                  <td onClick={() => {}}>{parseFloat(content.summaryResponse.professionalCommision).toFixed(2)}</td>
                </tr>
              ) : (
                <tr className="bg-white dark:bg-gray-800 dark:text-gray-200 dark:border-gray-900 hover:bg-gray-100 cursor-pointer border-b-4 border-gray-100">
                  <td onClick={() => {}} align="center" colSpan="10">
                    {language.NO_DATA}
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}
