import React, { forwardRef } from "react";
import { AiFillCar, AiOutlineFileExcel } from "react-icons/ai";
import { FiArrowDown, FiDownloadCloud, FiEye, FiLoader, FiRefreshCw, FiSearch } from "react-icons/fi";
import { IoMdCash } from "react-icons/io";
import { Waypoint } from "react-waypoint";
import gsap from "gsap";
import * as XL from "xlsx";
import { saveAs } from "file-saver";

import { DropdownNormal } from "../../../../../components/common/DropDownNormal";
import { TextField } from "../../../../../components/common/TextField";
import { TextFormat } from "../../../../../components/common/TextFormat";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import { NoData } from "../../../../../components/common/NoData";
import useLanguage from "../../../../../hooks/useLanguage";

import NavLinks from "../../../../../utils/navLinks.json";
import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils";
import axios from "axios";
import useAdmin from "../../../../../hooks/useAdmin";
import useSettings from "../../../../../hooks/useSettings";
import { format, addDays } from "date-fns";
import { parse } from "postcss";
import ReactDatePicker from "react-datepicker";
// import { addDays } from "date-fns";
import useUtils from "../../../../../hooks/useUtils";
import { Modal } from "../../../../../components/common/Modal";
import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { CgNotes } from "react-icons/cg";
import { Section } from "../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import CalenderDatePicker from "../../../../../components/common/CalenderDatePicker";
import OutsideClickHandler from "react-outside-click-handler";

const EachCard = ({ bg, title, number, Icon }) => {
  const [hovered, setHovered] = React.useState(false);
  return (
    <div
      className="w-1/4 p-5 cursor-pointer opacity-0 earnings"
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
    >
      <div
        className={"rounded-xl w-full flex justify-between relative text-white transition"}
        style={{
          backgroundColor: bg,
          transform: hovered ? "scale(0.95)" : "scale(1)",
        }}
      >
        <div className="p-4 flex flex-col justify-start">
          <h1 className={"text-3xl transition"}>{number}</h1>
          <h1 className="text-sm">{title}</h1>
        </div>
        <Icon
          className={`m-4 p-1 text-5xl transition ${hovered ? "opacity-0" : "opacity-100"}`}
          style={{
            transform: hovered ? "translateY(-30px)" : "translate(0)",
          }}
        />
        <div
          className="absolute h-10 w-10 bg-gray-200 rounded-full transition"
          style={{
            bottom: -10,
            right: -10,
            transform: hovered ? "translateX(-50px)" : "translate(0)",
          }}
        ></div>
      </div>
    </div>
  );
};

const CustomDateComponent = forwardRef(({ value, onClick }, ref) => (
  <button
    className={`w-full dark:bg-gray-900 bg-gray-100 outline-none rounded-xl px-4 py-3 border-b-2 border-gray-100 dark:border-gray-800 focus:border-blue-800 text-sm dark:focus:border-blue-800 cursor-pointer`}
    onClick={onClick}
    ref={ref}
  >
    {value}
  </button>
));

export default function Index({ history }) {
  const [showMoreFilters, setShowMoreFilters] = React.useState(false);
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { settings } = useSettings();
  const { morph, paymentSectionFields } = useUtils();
  const [pageLoading, setPageLoading] = React.useState(true);
  const [categories, setCategories] = React.useState(null);
  const [cities, setCitites] = React.useState(null);
  const [billingCycle, setBillingCycle] = React.useState(null);
  const [tableLoading, setTableLoading] = React.useState(true);
  const [dashboardData, setDashboardData] = React.useState({});
  const [searchText, setSearchText] = React.useState("");
  const [customDates, setCustomDates] = React.useState([new Date(), new Date()]);
  const [customDateChanged, setCustomDateChanged] = React.useState(false);
  const [pagination, setPagination] = React.useState({
    skip: 0,
    limit: settings.pageViewLimits[0],
  });
  const [filters, setFilters] = React.useState({
    city: "",
    category: "",
    billingCycle: "",
    paymentSectionFilter: settings?.paymentSection,
  });
  const [fields, setFields] = React.useState([]);

  const [downloadDates, setDownloadDates] = React.useState([new Date(), new Date()]);
  const [showDownloadPop, setDownloadPop] = React.useState(false);
  const [btnLoading, setBtnLoading] = React.useState(false);

  const [showDownloadOption, setShowDownloadOption] = React.useState(false);

  const getDownloadData = async () => {
    try {
      const query = {
        skip: pagination.skip,
        limit: pagination.limit,
        search: searchText,
        billingId: "",
        city: filters.city,
        vehicleCategory: filters.category,
        paymentSectionFilter: filters.paymentSectionFilter,
        fromDate: downloadDates[0],
        toDate: downloadDates[1],
      };
      const { data } = await axios.post(A.HOST + A.ADMIN_ASSETS_EARNINGS, query, header);
      const fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
      const fileExtension = ".xlsx";

      function fitToColumn(arrayOfArray) {
        return arrayOfArray[0].map((a, i) => ({
          wch: Math.max(
            ...arrayOfArray.filter((a2) => a2[i] !== undefined && a2[i] !== null).map((a2) => a2[i].toString().length)
          ),
        }));
      }

      const headings = [
        {
          id: 1,
          title: language.PROFESSIONALS_INFO,
          key: "professional",
          show: true,
        },
        {
          id: 2,
          title: language.TIPS_AMOUNT,
          key: "tipAmount",
          show: true,
        },
        {
          id: 3,
          title: language.TOTAL,
          key: "totalAmount",
          show: true,
        },
        {
          id: 4,
          title: language.TRIPS,
          key: "totalBookings",
          show: true,
        },
        {
          id: 5,
          title: language.SITE_EARNINGS,
          key: "siteEarnings",
          show: true,
        },
        {
          id: 6,
          title: language.PROFESSIONAL_EARNINGS,
          key: "professionalEarnings",
          show: true,
        },
        {
          id: 7,
          title: language.CASH_IN_SITE,
          key: "totalCashInSite",
          show: true,
        },
        {
          id: 8,
          title: language.CASH_IN_DRIVER,
          key: "totalCashInProfessional",
          show: true,
        },
        {
          id: 9,
          title: language.PAYOUT,
          key: "payoutAmount",
          show: true,
        },
      ];

      const fields = data.response.map((user) => ({
        _id: user._id,
        professional: `${user.professional && user.professional.firstName} ${
          user.professional && user.professional.lastName
        } ${user.professional && user.professional.email} ${
          user.professional && user.professional.phone && user.professional.phone.code
        }`,
        tipAmount: user.tipAmount,
        totalAmount: user.totalAmount,
        totalBookings: user.totalBookings,
        siteEarnings: user.siteEarnings,
        professionalEarnings: user.professionalEarnings,
        totalCashInSite: user.totalCashInSite,
        totalCashInProfessional: user.totalCashInProfessional,
        payoutAmount: user.payoutAmount,
      }));

      if (fields.length > 0) {
        console.log(JSON.stringify(fields, null, 2));
        const sheetHeadings = [
          language.NO,
          ...Object.keys(fields[0])
            .filter((each) => each !== "_id" || each !== "notes")
            .map((each) =>
              headings.filter((heading) => heading.key === each).length > 0
                ? headings.filter((heading) => heading.key === each)[0].title
                : ""
            )
            .filter((each) => each !== ""),
        ];
        const sheetData = [
          sheetHeadings,
          ...fields.map((each, idx) => [
            idx + 1,
            ...Object.keys(each)
              .filter((each) => each !== "_id")
              .map((key) =>
                key === "status"
                  ? each[key] === 1
                    ? "ACTIVE"
                    : "INACTIVE"
                  : key === "oneByOne" || key === "oneByOne2" || key === "oneByOne3"
                  ? each[key].join(" ")
                  : each[key]
              ),
          ]),
        ];
        const ws = XL.utils.aoa_to_sheet(sheetData);
        ws["!cols"] = fitToColumn(sheetData);
        ws["!rows"] = [...new Array(sheetData.length)].map((each) => ({
          hpt: 20,
        }));
        const wb = { Sheets: { export: ws }, SheetNames: ["export"] };
        const excelBuffer = XL.write(wb, { bookType: "xlsx", type: "array" });
        const data = new Blob([excelBuffer], { type: fileType });
        saveAs(data, document.title + fileExtension);
      }
    } catch (err) {
      alert(err);
      authFailure(err);
    }
    setDownloadPop(false);
  };
  const fetchTable = async () => {
    setTableLoading(true);
    if (!JSON.stringify(customDates).includes("null")) {
      let start =
        filters.billingCycle === "CUSTOM" || filters.paymentSectionFilter !== "ALL"
          ? customDateChanged
            ? addDays(new Date(new Date(customDates[0]).toISOString().split("T")[0]), 1)
            : new Date()
          : "";
      let end =
        filters.billingCycle === "CUSTOM" || filters.paymentSectionFilter !== "ALL"
          ? customDateChanged
            ? addDays(new Date(new Date(customDates[1]).toISOString().split("T")[0]), 1)
            : new Date()
          : "";

      try {
        const query = {
          skip: pagination.skip,
          limit: pagination.limit,
          search: searchText,
          billingId: filters.billingCycle === "CUSTOM" ? "" : filters.billingCycle,
          city: filters.city,
          vehicleCategory: filters.category,
          paymentSectionFilter: filters.paymentSectionFilter,
          fromDate: start,
          toDate: end,
        };
        const { data } = await axios.post(A.HOST + A.ADMIN_ASSETS_EARNINGS, query, header);
        setFields(data.response);
        setDashboardData(data.dashboardData);
        setTableLoading(false);
      } catch (err) {
        authFailure(err);
      }
    }
  };

  React.useEffect(
    () => pageLoading === false && gsap.fromTo(".earnings", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 }),
    [pageLoading]
  );

  React.useEffect(() => {
    const fetchCategories = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_VEHICLE_CATEGORY_LIST_ALL, {}, header);
        setCategories(data);
      } catch (err) {
        authFailure(err);
      }
    };
    const fetchBillPeriod = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_EARNINGS_BILLING_CYCLE, {}, header);
        let reversedBilling = [];
        for (var i = data.length - 1; i >= 0; i--) reversedBilling.push(data[i]);
        setBillingCycle(reversedBilling);
      } catch (err) {
        authFailure(err);
      }
    };
    const fetchCities = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_CITY_LIST, {}, header);
        setCitites(data.response);
      } catch (err) {
        authFailure(err);
      }
    };
    const fetchEverything = async () => {
      fetchCategories();
      fetchBillPeriod();
      fetchCities();
    };
    fetchEverything();
  }, []);

  React.useEffect(() => fetchTable(), [filters, pagination, customDates]);

  React.useEffect(() => {
    if (categories !== null && billingCycle !== null && cities !== null) setPageLoading(false);
  }, [categories, billingCycle, cities]);

  return pageLoading === true ? (
    <SmallLoader />
  ) : (
    <>
      <div className="flex justify-between px-4 py-3">
        {showDownloadPop && (
          <Modal>
            <FormWrapper
              width="2/4"
              enterSubmit={false}
              btnLoading={btnLoading}
              model={true}
              icon={<CgNotes />}
              title={document.title + " " + language.EXCEL}
              closeBtn={true}
              downloadBtn={true}
              submit={getDownloadData}
              closeClick={() => setDownloadPop(false)}
            >
              <Section>
                <FieldWrapper title={language.FROM}>
                  <CalenderDatePicker
                    startTime={new Date(new Date().setMinutes(new Date().getMinutes() + 70))}
                    // afterDays={5}
                    showTime={false}
                    disableBeforeDays={false}
                    change={(e) => setDownloadDates([e, downloadDates[1]])}
                    defaultValue={downloadDates[0]}
                  />
                </FieldWrapper>
              </Section>
              <Section>
                <FieldWrapper title={language.TO}>
                  <CalenderDatePicker
                    startTime={new Date(new Date().setMinutes(new Date().getMinutes() + 70))}
                    // afterDays={5}
                    showTime={false}
                    disableBeforeDays={false}
                    change={(e) => setDownloadDates([downloadDates[0], e])}
                    defaultValue={downloadDates[1]}
                  />
                </FieldWrapper>
              </Section>
            </FormWrapper>
          </Modal>
        )}
        {filters.paymentSectionFilter === "ALL" ? (
          <div className="w-1/5 flex items-center">
            <p className={"mx-2 flex"} style={{ whiteSpace: "pre" }}>
              {language.BILL_PERIOD}
            </p>
            <div className={"w-full"}>
              <DropdownNormal
                change={(e) => setFilters({ ...filters, billingCycle: e })}
                defaultValue={format(new Date(settings.lastBilledDate), "dd/LL/yyyy") + " - " + language.TILL_NOW}
                fields={[
                  {
                    id: "  ",
                    label: language.CUSTOM_START_END_DATE,
                    value: "CUSTOM",
                  },
                  {
                    id: "",
                    label: format(new Date(settings.lastBilledDate), "dd/LL/yyyy") + " - " + language.TILL_NOW,
                    value: "",
                  },
                  ...billingCycle
                    .sort((a, b) => a.billingcycle - b.billingcycle)
                    .map((billing, idx) => ({
                      id: idx,
                      value: billing._id,
                      label: billing.billingcycle,
                    })),
                ]}
              />
            </div>
          </div>
        ) : (
          <div className="w-1/5 flex items-center">
            <p className={"w-2/5 mx-2 flex"} style={{ whiteSpace: "pre" }}>
              {language.CUSTOM_DATE}
            </p>
            <div className={"w-3/5"}>
              <ReactDatePicker
                dateFormat="dd/MM/yyyy"
                selectsRange={true}
                startDate={customDates[0]}
                endDate={customDates[1]}
                customInput={<CustomDateComponent />}
                onChange={(update) => {
                  setCustomDateChanged(true);
                  setCustomDates([update[0], update[1]]);
                }}
                withPortal
              />
            </div>
          </div>
        )}
        <div className="w-1/5 flex items-center mx-2 opacity-0 earnings">
          <p className={"mx-2 flex"} style={{ whiteSpace: "pre" }}>
            {language.VEHICLE_CATEGORY}
          </p>
          <div className={"w-full"}>
            <DropdownNormal
              change={(e) => setFilters({ ...filters, category: e })}
              defaultValue={language.ALL}
              fields={[
                {
                  id: "",
                  value: "",
                  label: language.ALL,
                },
                ...categories.map((category, idx) => ({
                  id: idx,
                  value: category._id,
                  label: category.vehicleCategory,
                })),
              ]}
            />
          </div>
        </div>
        <div className="w-1/5 flex items-center opacity-0 earnings">
          <p className={"mx-2 flex"}>{language.CITY}</p>
          <div className={"w-full"}>
            <DropdownNormal
              change={(e) => setFilters({ ...filters, city: e })}
              defaultValue={language.ALL}
              fields={[
                {
                  id: "",
                  value: "",
                  label: language.ALL,
                },
                ...cities.map((city, idx) => ({
                  id: idx,
                  value: city._id,
                  label: morph(city.locationName),
                })),
              ]}
            />
          </div>
        </div>
        <div className="w-1/5 flex items-center mx-2 opacity-0 earnings">
          <p className={"mx-2 flex"} style={{ whiteSpace: "pre" }}>
            {language.PAYMENT_SECTION}
          </p>
          <div className={"w-full"}>
            <DropdownNormal
              change={(e) => setFilters({ ...filters, paymentSectionFilter: e })}
              defaultValue={
                paymentSectionFields.filter((each) => each.value === filters.paymentSectionFilter)[0]?.label
              }
              fields={paymentSectionFields}
            />
          </div>
        </div>
        <div className="w-1/5 flex items-center">
          <div className="w-1/6 flex justify-center mx-2">
            <button
              // onClick={refreshClick}
              data-title={language.REFRESH}
              onClick={fetchTable}
              className={`${
                tableLoading === true && "animate-spin"
              } bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2`}
            >
              <FiRefreshCw />
            </button>
          </div>
          {true && (
            <div className="relative">
              <button
                onClick={() => setShowDownloadOption(true)}
                data-title={language.DOWNLOAD}
                className={`bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                  document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                }`}
              >
                <FiDownloadCloud />
              </button>
              {showDownloadOption && (
                <OutsideClickHandler onOutsideClick={() => setShowDownloadOption(false)}>
                  <div
                    className="absolute shadow-xl flex flex-col right-0 bg-white dark:bg-gray-900 rounded-xl"
                    style={{ top: "130%", zIndex: 10 }}
                  >
                    <div
                      onClick={() => {
                        setDownloadPop(true);
                        // downloadClick("EXCEL");
                        setShowDownloadOption(false);
                      }}
                      className="cursor-pointer hover:bg-gray-500 px-3 py-3 bg-white dark:bg-gray-900 flex items-center"
                    >
                      <AiOutlineFileExcel className="text-4xl" />
                      <h1 className="text-sm mx-2">Excel</h1>
                    </div>
                  </div>
                </OutsideClickHandler>
              )}
            </div>
          )}
          {/* <div className="w-5/6 flex items-center opacity-0 earnings">
            <div className={"w-full"}>
              <TextField
                padding={false}
                onKeyDown={(e) => {
                  if (e.key === "Enter") fetchTable();
                }}
                iconSelect={fetchTable}
                change={(e) => setSearchText(e)}
                icon={<FiSearch />}
                placeholder={language.SEARCH}
              />
            </div>
          </div> */}
        </div>
      </div>
      {filters.billingCycle === "CUSTOM" && (
        <div className="flex px-4 py-3">
          <div className="w-1/4 flex items-center">
            <p className={"w-2/5 mx-2 flex"} style={{ whiteSpace: "pre" }}>
              {language.CUSTOM_DATE}
            </p>
            <div className={"w-3/5"}>
              <ReactDatePicker
                dateFormat="dd/MM/yyyy"
                selectsRange={true}
                startDate={customDates[0]}
                endDate={customDates[1]}
                customInput={<CustomDateComponent />}
                onChange={(update) => {
                  setCustomDateChanged(true);
                  setCustomDates([update[0], update[1]]);
                }}
                withPortal
              />
            </div>
          </div>
        </div>
      )}
      {/* <div
        className="absolute left-0 right-0 flex justify-center items-center opacity-0 earnings"
        style={{ zIndex: 10, transform: "translateY(-12px)" }}
      >
        <div
          onClick={() => setShowMoreFilters(!showMoreFilters)}
          className="px-4 py-1 transition rounded-xl bg-blue-800 text-sm flex items-center cursor-pointer hover:bg-green-800"
        >
          <p className={"text-white"}>{showMoreFilters ? "Hide Filters" : "More Filters"}</p>
          <FiArrowDown
            className={"mx-2 transition text-white"}
            style={{ transform: showMoreFilters ? "rotateZ(180deg)" : "rotateZ(0)" }}
          />
        </div>
      </div> */}
      <div className="flex bg-gray-100 dark:bg-gray-900 opacity-0 earnings">
        <EachCard bg="#335EFD" title={language.TOTAL_RIDES} number={dashboardData.totalBookings} Icon={AiFillCar} />
        <EachCard
          bg="#F69534"
          title={language.TOTAL_REVENUE}
          number={parseFloat(dashboardData.tipRevenue).toFixed(2)}
          Icon={IoMdCash}
        />
        <EachCard
          bg="#E44E33"
          title={language.SITE_EARNINGS}
          number={parseFloat(dashboardData.totalSiteEarnings).toFixed(2)}
          Icon={IoMdCash}
        />
        <EachCard
          bg="#2E6249"
          title={language.DRIVER_EARNINGS}
          number={parseFloat(dashboardData.totalProfessionalEarnings).toFixed(2)}
          Icon={IoMdCash}
        />
      </div>
      {fields.length === 0 && (
        <div className="mt-4">
          <NoData />
        </div>
      )}
      {fields.length !== 0 && (
        <div className="w-full shadow-md mt-4 rounded-lg overflow-y-scroll" style={{ maxHeight: "90vh" }}>
          <table
            cellPadding="12"
            className="w-full relative text-gray-800 text-left dark:bg-gray-800 z-10 overflow-y-scroll"
            style={{ fontSize: 13 }}
          >
            <thead className="z-10">
              <tr>
                <td className="text-center sticky top-0 bg-blue-800 text-white">{language.TABLE_NO}</td>

                <td className="sticky top-0 bg-blue-800 text-white">{language.PROFESSIONALS_INFO}</td>
                <td className="sticky top-0 bg-blue-800 text-white">{language.TRIPS}</td>
                <td className="sticky top-0 bg-blue-800 text-white">
                  <p>{language.ACTUAL_TRIP}</p>
                  <p>{language.AMOUNT}</p>
                </td>
                <td className="sticky top-0 bg-blue-800 text-white">{language.DISCOUNT}</td>
                <td className="sticky top-0 bg-blue-800 text-white">
                  <p>{language.TOTAL_TRIP}</p>
                  <p>{language.AMOUNT}</p>
                </td>
                <td className="sticky top-0 bg-blue-800 text-white">{language.SITE_EARNINGS}</td>
                <td className="sticky top-0 bg-blue-800 text-white">{language.TIPS}</td>
                <td className="sticky top-0 bg-blue-800 text-white">
                  <p>{language.REIMBURSEMENT}</p>
                  <p>{language.FOR_DISCOUNT}</p>
                </td>
                <td className="sticky top-0 bg-blue-800 text-white">{language.DRIVER_EARNINGS}</td>
                {/* <td className="sticky top-0 bg-blue-800 text-white">{language.CASH_IN_SITE}</td>
                <td className="sticky top-0 bg-blue-800 text-white">{language.CASH_IN_DRIVER}</td> */}
                {/* <td className="sticky top-0 bg-blue-800 text-white">{language.PAYOUT}</td> */}

                <td className="text-center sticky top-0 bg-blue-800 text-white">{language.ACTION}</td>
              </tr>
            </thead>
            {tableLoading === false && (
              <tbody className="text-sm text-gray-600 dark:bg-gray-800">
                {fields.map((each, index) => (
                  <tr className="bg-white dark:bg-gray-800 dark:text-gray-200 dark:border-gray-900 hover:bg-gray-100 cursor-pointer border-b-4 border-gray-100">
                    <td onClick={() => {}} className="text-center">
                      {index + 1}
                    </td>
                    <td onClick={() => {}}>
                      <p>
                        {morph(each.professional.firstName)} {morph(each.professional.lastName)}
                      </p>
                      <p className="mt-2">{morph(each.professional.email)}</p>
                      <p className="mt-2">
                        {morph(each.professional.phone.code)} {morph(each.professional.phone.number)}
                      </p>
                    </td>
                    <td onClick={() => {}}>{each.totalBookings}</td>
                    <td onClick={() => {}}>{parseFloat(each.estimationPayableAmount).toFixed(2)}</td>
                    <td onClick={() => {}}>{parseFloat(each.discountAmount).toFixed(2)}</td>
                    <td onClick={() => {}}>{parseFloat(each.payoutAmount).toFixed(2)}</td>
                    <td onClick={() => {}}>{parseFloat(each.siteEarnings).toFixed(2)}</td>
                    <td onClick={() => {}}>{parseFloat(each.tipAmount).toFixed(2)}</td>
                    <td onClick={() => {}}>{parseFloat(each.professionalTolerenceAmount).toFixed(2)}</td>
                    <td onClick={() => {}}>{parseFloat(each.professionalEarnings).toFixed(2)}</td>
                    {/* <td onClick={() => {}}>{parseFloat(each.totalCashInSite).toFixed(2)}</td>
                    <td onClick={() => {}}>{parseFloat(each.totalCashInProfessional).toFixed(2)}</td> */}
                    {/* <td onClick={() => {}}>{parseFloat(each.totalAmount).toFixed(2)}</td> */}
                    <td className="w-28">
                      <div className="flex items-center justify-center">
                        <button
                          onClick={() =>
                            history.push({
                              pathname: NavLinks.ADMIN_ASSET_EARNINGS_DETAILS,
                              state: {
                                skip: pagination.skip,
                                limit: pagination.limit,
                                search: searchText,
                                billingId: filters.billingCycle === "CUSTOM" ? "" : filters.billingCycle,
                                city: filters.city,
                                vehicleCategory: filters.category,
                                fromDate:
                                  filters.billingCycle === "CUSTOM" || filters.paymentSectionFilter !== "ALL"
                                    ? customDateChanged
                                      ? addDays(new Date(new Date(customDates[0]).toISOString().split("T")[0]), 1)
                                      : new Date()
                                    : "",
                                toDate:
                                  filters.billingCycle === "CUSTOM" || filters.paymentSectionFilter !== "ALL"
                                    ? customDateChanged
                                      ? addDays(new Date(new Date(customDates[1]).toISOString().split("T")[0]), 1)
                                      : new Date()
                                    : "",
                                professionalId: each.professional._id,
                                paymentSectionFilter: filters.paymentSectionFilter,
                              },
                            })
                          }
                          title={language.VIEW}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                          }`}
                        >
                          <FiEye />
                        </button>
                      </div>
                    </td>
                  </tr>
                ))}
                {/* <Waypoint onEnter={() => {}}>
          <tr>
            <td align="center">
              <FiLoader className="animate-spin" />
            </td>
          </tr>
        </Waypoint> */}
              </tbody>
            )}
          </table>
        </div>
      )}
    </>
  );
}
