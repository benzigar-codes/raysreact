import React from "react";
import Table from "../../../../../../components/common/Table";
import useLanguage from "../../../../../../hooks/useLanguage";

import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";
import NavLinks from "../../../../../../utils/navLinks.json";

import useAdmin from "../../../../../../hooks/useAdmin";
import useUtils from "../../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();

  return (
    <Table
      title={language.AMOUNT_IN_WALLET_PROFESSIONALS}
      startingHeadings={[
        {
          id: 1,
          title: language.NAME,
          key: "name",
          show: true,
        },
        {
          id: 2,
          title: language.EMAIL,
          key: "email",
          show: true,
        },
        {
          id: 3,
          title: language.MOBILE,
          key: "phone",
          show: true,
        },
        {
          id: 4,
          title: language.WALLET_AMOUNT,
          key: "wallet",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_DRIVERS_LIST}
      assignData={(data) =>
        data.map((driver) => ({
          _id: driver._id,
          name: morph(driver.data.firstName) + " " + morph(driver.data.lastName),
          email: morph(driver.data.email),
          phone: morph(driver.data.phone.code) + " " + morph(driver.data.phone.number),
          wallet:
            driver?.data?.wallet?.availableAmount !== undefined && driver?.data?.wallet?.availableAmount !== null
              ? parseFloat(driver?.data?.wallet?.availableAmount).toFixed(2)
              : "",
        }))
      }
      bread={[]}
      // CLICKS
      vehicleClick={(e) =>
        history.push({
          pathname: NavLinks.ADMIN_DRIVERS_LIST + e + NavLinks.ADMIN_VEHICLES_LIST,
        })
      }
      viewClick={(e) => history.push(NavLinks.ADMIN_DRIVERS_TRANSACTION + "/" + e)}
      // SHOW
      showFilter={true}
      showAdd={false}
      showArchieve={false}
      showBulk={false}
      showEdit={false}
      showStatus={false}
      showSearch={true}
      showView={true}
      showAmountInWalletFilter={true}
      statusList={A.HOST + A.ADMIN_DRIVERS_STATUS}
      showAction={
        admin.privileges.DRIVERS.DRIVERS_LIST ? (admin.privileges.DRIVERS.DRIVERS_LIST.EDIT ? true : false) : false
      }
      add={admin.privileges.DRIVERS.DRIVERS_LIST ? (admin.privileges.DRIVERS.DRIVERS_LIST.ADD ? true : false) : false}
      edit={admin.privileges.DRIVERS.DRIVERS_LIST ? (admin.privileges.DRIVERS.DRIVERS_LIST.EDIT ? true : false) : false}
    />
  );
}
