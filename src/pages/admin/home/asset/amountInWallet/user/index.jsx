import React from "react";
import useLanguage from "../../../../../../hooks/useLanguage";

import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";
import Table from "../../../../../../components/common/Table";
import NavLinks from "../../../../../../utils/navLinks.json";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import axios from "axios";
import useAdmin from "../../../../../../hooks/useAdmin";
import useUtils from "../../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <Table
      title={language.AMOUNT_IN_WALLET_USER}
      startingHeadings={[
        {
          id: 1,
          title: language.NAME,
          key: "name",
          show: true,
        },
        {
          id: 2,
          title: language.EMAIL,
          key: "email",
          show: true,
        },
        {
          id: 4,
          title: language.MOBILE,
          key: "phone",
          show: true,
        },
        {
          id: 5,
          title: language.WALLET_AMOUNT,
          key: "wallet",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_USERS_LIST}
      assignData={(data) =>
        data.map((user) => ({
          _id: user._id,
          name: morph(user.data.firstName) + " " + morph(user.data.lastName),
          email: morph(user.data.email),
          phone: morph(user.data.phone.code) + " " + morph(user.data.phone.number),
          wallet: user?.data?.wallet?.availableAmount,
        }))
      }
      bread={[]}
      showFilter={true}
      showAdd={false}
      showAmountInWalletFilter={true}
      showArchieve={false}
      addClick={() => history.push(NavLinks.ADMIN_USERS_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_USERS_EDIT + "/" + e)}
      showBulk={false}
      showView={true}
      viewClick={(e) => history.push(NavLinks.ADMIN_USERS_TRANSACTION + "/" + e)}
      showEdit={false}
      showStatus={false}
      showSearch={true}
      statusList={A.HOST + A.ADMIN_USERS_STATUS}
      showAction={admin.privileges.USERS.USERS_LIST ? (admin.privileges.USERS.USERS_LIST.EDIT ? true : false) : false}
      add={admin.privileges.USERS.USERS_LIST ? (admin.privileges.USERS.USERS_LIST.ADD ? true : false) : false}
      edit={admin.privileges.USERS.USERS_LIST ? (admin.privileges.USERS.USERS_LIST.EDIT ? true : false) : false}
    />
  );
}
