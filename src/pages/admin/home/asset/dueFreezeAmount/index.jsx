import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <Table
      title={language.DUE_AMOUNT}
      startingHeadings={[
        {
          id: 1,
          title: language.FIRSTNAME,
          key: "firstName",
          show: true,
        },
        {
          id: 2,
          title: language.LASTNAME,
          key: "lastName",
          show: true,
        },
        {
          id: 3,
          title: language.EMAIL,
          key: "email",
          show: true,
        },
        {
          id: 4,
          title: language.PHONE,
          key: "phone",
          show: true,
        },
        {
          id: 5,
          title: language.DUE_AMOUNT,
          key: "balance",
          show: true,
        },
        // {
        //   id: 6,
        //   title: language.DUE_AMOUNT,
        //   key: "due",
        //   show: true,
        // },
        // {
        //   id: 7,
        //   title: language.FREEZED_AMOUNT,
        //   key: "freezed",
        //   show: true,
        // },
      ]}
      list={A.HOST + A.ADMIN_ASSET_DUE_FREEEZE}
      assignData={(data) =>
        data.map((dueFreeze) => ({
          _id: dueFreeze._id,
          firstName: morph(dueFreeze.firstName),
          lastName: morph(dueFreeze.lastName),
          email: morph(dueFreeze.email),
          phone: morph(dueFreeze.phone.code) + " " + morph(dueFreeze.phone.number),
          balance: dueFreeze.wallet.availableAmount,
          due: dueFreeze.wallet.dueAmount,
          freezed: dueFreeze.wallet.freezedAmount,
        }))
      }
      bread={[{ id: 1, title: language.ASSET }]}
      showAdd={false}
      showUserTypeFilter={true}
      showStatus={false}
      showActiveInactiveFilter={false}
      showArchieve={false}
      showView={true}
      editClick={(e) => history.push(NavLinks.ADMIN_ADMIN_ROLE_EDIT + "/" + e)}
      showBulk={false}
      showDueFreezedFilter={false}
      showSearch={true}
      showAction={false}
      statusList={A.HOST + A.ADMIN_ROLE_STATUS}
    />
  );
}
