import React from "react";
import { useFormik } from "formik";
import { useParams } from "react-router";
import axios from "axios";
import gsap from "gsap/gsap-core";
import _ from "lodash";

import {
  AiFillCaretDown,
  AiFillCaretRight,
  AiOutlinePlusSquare,
  AiOutlineMinusSquare,
} from "react-icons/ai";

import useAdmin from "../../../../../../hooks/useAdmin";
import useLanguage from "../../../../../../hooks/useLanguage";
import A from "../../../../../../utils/API.js";
import NavLinks from "../../../../../../utils/navLinks.json";
import U from "../../../../../../utils/utils.js";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import Flex from "../../../../../../components/common/Flex";
import { Border } from "../../../../../../components/common/Border";
import { TextField } from "../../../../../../components/common/TextField";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import { DropdownNormal } from "../../../../../../components/common/DropDownNormal";
import { PopUp } from "../../../../../../components/common/PopUp";
import { Heading } from "../../../../../../components/common/Heading";
import { TextArea } from "../../../../../../components/common/TextArea";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";
import useUtils from "../../../../../../hooks/useUtils";

const Wrapper = ({ title = "", children }) => {
  const [show, setShow] = React.useState(false);
  return (
    <div>
      <FieldWrapper>
        <div className="flex items-center">
          <p
            onClick={() => setShow(!show)}
            className="mx-4 cursor-pointer dark:text-gray-200 text-gray-500 text-sm"
          >
            {title}
          </p>
          {show ? <AiFillCaretDown /> : <AiFillCaretRight />}
        </div>
      </FieldWrapper>
      {show ? (
        <div className="ml-4">
          <Border className="border-0">{children}</Border>
        </div>
      ) : null}
    </div>
  );
};

export default function Index({ history }) {
  // STATES
  const [loading, setLoading] = React.useState(true);
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [editable, setEditable] = React.useState(false);
  const [formData, setFormData] = React.useState({
    _id: "",
    status: U.ACTIVE,
    mode: "SANDBOX",
    paymentType: "CARD",
    name: "CARD",
    gateWay: "",
    testSecretKey: "",
    isEditNeeded: true,
    availableCurrencies: [],
    bankDetails: [],
    testPublicKey: "",
    testEncryptionKey: "",
    liveEncryptionKey: "",
  });
  const [uneditableData, setUneditableData] = React.useState({});
  const [popup, setPop] = React.useState(null);

  // HOOKS
  const { id } = useParams();
  const { header, authFailure } = useAdmin();
  const { language } = useLanguage();
  const { parseError } = useUtils();

  const paymentGateWays = ["FLUTTERWAVE", "PEACH", "STRIPE"];

  const formik = useFormik({
    initialValues: formData,
    enableReinitialize: true,
    onSubmit: async (e) => {
      try {
        setBtnLoading(true);
        await axios.post(
          A.HOST + A.ADMIN_PAYMENT_UPDATE,
          {
            _id: id,
            data: formik.values,
          },
          header
        );
        history.push(NavLinks.ADMIN_SETUP_PAYMENTS);
      } catch (err) {
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
      setBtnLoading(true);
    },
  });

  React.useEffect(() => {
    const fetchPayment = async () => {
      try {
        let { data } = await axios.post(A.HOST + A.ADMIN_PAYMENT_READ, { id }, header);
        // setEditable(data.isEditNeeded === true ? true : false);
        // setUneditableData({
        //   id: data._id,
        //   status: data.status,
        //   mode: data.mode,
        //   paymentType: data.paymentType,
        //   isEditNeeded: data.isEditNeeded,
        // });
        // data = _.omit(data, [
        //   "paymentType",
        //   "_id",
        //   "$init",
        //   "createdAt",
        //   "isEditNeeded",
        //   "mode",
        //   "status",
        //   "updatedAt",
        // ]);
        setFormData({ ...data.data, bankDetails: data?.data?.bankDetails ?? [] });
        setLoading(false);
        gsap.fromTo(".editPayments", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
      } catch (err) {
        authFailure(err);
      }
    };
    fetchPayment();
  }, []);

  React.useEffect(() => {
    console.log(formik.values);
  }, [formik.values]);

  const textFieldFilters = ["NUMBER", "ALPHANUMERIC", "PHONEPAD"];
  const textTypeFilters = ["TEXT", "DROPDOWN"];

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      bread={[{ id: 1, title: language.PAYMENTS, path: NavLinks.ADMIN_SETUP_PAYMENTS }]}
      title={language.EDIT + " " + formik.values.name}
      submitBtn={true}
      btnLoading={btnLoading}
      info={language.PAYMENT_GATEWAY_EDIT_PAGE_INFO}
      submit={formik.handleSubmit}
      animate={"editPayments"}
      width={"5/5"}
    >
      <Section>
        <Border className="border-0">
          <Heading title={language.BASIC_INFO} />
          <Flex align={false}>
            <Section>
              {popup != null && (
                <PopUp
                  unmount={() => setPop(null)}
                  title={popup.title}
                  type={popup.type}
                />
              )}
              <FieldWrapper title={language.MODE}>
                <DropdownNormal
                  change={(e) => formik.setFieldValue("mode", e)}
                  defaultValue={
                    [
                      { id: 1, label: language.LIVE, value: U.LIVE },
                      { id: 2, label: language.SANDBOX, value: U.SANDBOX },
                    ]?.filter((data) => data.value === formik.values.mode)?.[0]?.label
                  }
                  fields={[
                    { id: 1, label: language.LIVE, value: U.LIVE },
                    { id: 2, label: language.SANDBOX, value: U.SANDBOX },
                  ]}
                />
              </FieldWrapper>
            </Section>
            <Section>
              <FieldWrapper title={language.GATE_WAY}>
                <TextField editable="false" value={formik.values.gateWay} />
              </FieldWrapper>
            </Section>
          </Flex>
        </Border>
        <Border className="border-0">
          <Heading title={language.KEYS} />
          <Flex align={false}>
            <Section>
              <FieldWrapper title={language.TEST_PUBLIC_KEY}>
                <TextField
                  change={(e) => formik.setFieldValue("testPublicKey", e)}
                  value={formik.values.testPublicKey}
                />
              </FieldWrapper>
              <FieldWrapper title={language.TEST_SECRET_KEY}>
                <TextField
                  change={(e) => formik.setFieldValue("testSecretKey", e)}
                  value={formik.values.testSecretKey}
                />
              </FieldWrapper>
              <FieldWrapper title={language.TEST_ENCRYPTION_KEY}>
                <TextField
                  change={(e) => formik.setFieldValue("testEncryptionKey", e)}
                  value={formik.values.testEncryptionKey}
                />
              </FieldWrapper>
              <FieldWrapper title={language.ACCOUNT_ID}>
                <TextField
                  change={(e) => formik.setFieldValue("accountId", e)}
                  value={formik.values.accountId}
                />
              </FieldWrapper>
            </Section>
            <Section>
              <FieldWrapper title={language.LIVE_PUBLIC_KEY}>
                <TextField
                  change={(e) => formik.setFieldValue("livePublicKey", e)}
                  value={formik.values.livePublicKey}
                />
              </FieldWrapper>
              <FieldWrapper title={language.LIVE_SECRET_KEY}>
                <TextField
                  change={(e) => formik.setFieldValue("liveSecretKey", e)}
                  value={formik.values.liveSecretKey}
                />
              </FieldWrapper>
              <FieldWrapper title={language.LIVE_ENCRYPTION_KEY}>
                <TextField
                  change={(e) => formik.setFieldValue("liveEncryptionKey", e)}
                  value={formik.values.liveEncryptionKey}
                />
              </FieldWrapper>
            </Section>
          </Flex>
        </Border>
        <Border className="border-0">
          <Heading title={language.BANK_DETAILS} />
          {formik.values?.bankDetails?.map((each, idx) => (
            <Wrapper title={each.title + " " + (each?.isGenericField ? "*" : "")}>
              <Border className="border-0 border-l-2 ml-3">
                {/* <Heading /> */}
                <Flex align={false}>
                  <Section padding={false}>
                    <FieldWrapper title={language.FIELD_TYPE}>
                      <DropdownNormal
                        defaultValue={each.fieldType}
                        change={(e) =>
                          formik.setFieldValue(`bankDetails[${idx}].fieldType`, e)
                        }
                        fields={textTypeFilters?.map((each, idx) => ({
                          id: idx,
                          label: each,
                          value: each,
                        }))}
                      />
                    </FieldWrapper>
                    {each.fieldType === "TEXT" ? (
                      <FieldWrapper title={language.TEXT_TYPE}>
                        <DropdownNormal
                          defaultValue={each.textType}
                          change={(e) =>
                            formik.setFieldValue(`bankDetails[${idx}].textType`, e)
                          }
                          fields={textFieldFilters?.map((each, idx) => ({
                            id: idx,
                            label: each,
                            value: each,
                          }))}
                        />
                      </FieldWrapper>
                    ) : null}
                    {each.fieldType === "TEXT" && (
                      <FieldWrapper title={language.MAX_TEXT_COUNT}>
                        <TextField
                          type="number"
                          change={(e) =>
                            formik.setFieldValue(`bankDetails[${idx}].maxTextCount`, e)
                          }
                          value={each.maxTextCount}
                        />
                      </FieldWrapper>
                    )}
                    <FieldWrapper title={language.HINT}>
                      <TextField
                        change={(e) =>
                          formik.setFieldValue(`bankDetails[${idx}].hint`, e)
                        }
                        value={each.hint}
                      />
                    </FieldWrapper>
                    <FieldWrapper title={language.VALIDATION_MESSAGE}>
                      <TextField
                        change={(e) =>
                          formik.setFieldValue(`bankDetails[${idx}].validationMessage`, e)
                        }
                        value={each.validationMessage}
                      />
                    </FieldWrapper>
                    {each.fieldType === "DROPDOWN" &&
                      formik.values?.bankDetails[idx]?.fieldOptions && (
                        <FieldWrapper title={language.FIELD_OPTIONS}>
                          <div>
                            {formik.values?.bankDetails[idx]?.fieldOptions?.map(
                              (eachField, fieldIdx) => (
                                <>
                                  <div className="mt-2">
                                    <Wrapper title={eachField.displayName}>
                                      <div className="border-l-2">
                                        <Flex align={false}>
                                          <Section padding={false}>
                                            <FieldWrapper title={language.CODE}>
                                              <TextField
                                                change={(e) =>
                                                  formik.setFieldValue(
                                                    `bankDetails[${idx}].fieldOptions[${fieldIdx}].code`,
                                                    e
                                                  )
                                                }
                                                value={eachField.code}
                                              />
                                            </FieldWrapper>
                                            <FieldWrapper title={language.DISPLAY_NAME}>
                                              <TextField
                                                change={(e) =>
                                                  formik.setFieldValue(
                                                    `bankDetails[${idx}].fieldOptions[${fieldIdx}].displayName`,
                                                    e
                                                  )
                                                }
                                                value={eachField.displayName}
                                              />
                                            </FieldWrapper>
                                            <FieldWrapper title={language.TEXT_TYPE}>
                                              <DropdownNormal
                                                defaultValue={eachField.textType}
                                                change={(e) =>
                                                  formik.setFieldValue(
                                                    `bankDetails[${idx}].fieldOptions[${fieldIdx}].textType`,
                                                    e
                                                  )
                                                }
                                                fields={textFieldFilters?.map(
                                                  (each, idx) => ({
                                                    id: idx,
                                                    label: each,
                                                    value: each,
                                                  })
                                                )}
                                              />
                                            </FieldWrapper>
                                            {/* </Section>
                                          <Section padding={false} width="1/2"> */}
                                            <FieldWrapper title={language.KEY}>
                                              <TextField
                                                change={(e) =>
                                                  formik.setFieldValue(
                                                    `bankDetails[${idx}].fieldOptions[${fieldIdx}].key`,
                                                    e
                                                  )
                                                }
                                                value={eachField.key}
                                              />
                                            </FieldWrapper>
                                            <FieldWrapper
                                              title={language.VALIDATION_MESSAGE}
                                            >
                                              <TextArea
                                                change={(e) =>
                                                  formik.setFieldValue(
                                                    `bankDetails[${idx}].fieldOptions[${fieldIdx}].validationMessage`,
                                                    e
                                                  )
                                                }
                                                value={eachField.validationMessage}
                                              />
                                            </FieldWrapper>
                                          </Section>
                                        </Flex>
                                      </div>
                                    </Wrapper>
                                  </div>
                                </>
                              )
                            )}
                            <FieldWrapper>
                              <div
                                onClick={() => {
                                  formik.setFieldValue(
                                    `bankDetails[${idx}].fieldOptions`,
                                    [
                                      ...(formik.values?.bankDetails[idx]?.fieldOptions ??
                                        []),
                                      {
                                        code: "UNTILED",
                                        key: "untitles",
                                        displayName: "Edit This Key",
                                        validationMessage: "Untitled",
                                        textType: "ALPHANUMERIC",
                                      },
                                    ]
                                  );
                                }}
                                className="mx-3 text-2xl"
                              >
                                <AiOutlinePlusSquare className="cursor-pointer text-black dark:text-white" />
                              </div>
                              {formik.values?.bankDetails?.[idx]?.fieldOptions?.length >
                              0 ? (
                                <div
                                  onClick={() => {
                                    formik.setFieldValue(
                                      `bankDetails[${idx}].fieldOptions`,
                                      formik.values?.bankDetails[
                                        idx
                                      ]?.fieldOptions.filter(
                                        (each, eachIdx) =>
                                          eachIdx !==
                                          formik.values?.bankDetails[idx]?.fieldOptions
                                            ?.length -
                                            1
                                      )
                                    );
                                  }}
                                  className="mx-3 text-2xl"
                                >
                                  <AiOutlineMinusSquare className="cursor-pointer text-black dark:text-white" />
                                </div>
                              ) : null}
                            </FieldWrapper>
                          </div>
                        </FieldWrapper>
                      )}
                  </Section>
                  <Section padding={false}>
                    <FieldWrapper title={language.KEY}>
                      <TextField
                        change={(e) =>
                          each?.isGenericField
                            ? null
                            : formik.setFieldValue(`bankDetails[${idx}].key`, e)
                        }
                        value={each.key}
                      />
                    </FieldWrapper>
                    <FieldWrapper title={language.TITLE}>
                      <TextField
                        change={(e) =>
                          formik.setFieldValue(`bankDetails[${idx}].title`, e)
                        }
                        value={each.title}
                      />
                    </FieldWrapper>
                    <FieldWrapper title={language.PLACEHOLDER}>
                      <TextField
                        change={(e) =>
                          formik.setFieldValue(`bankDetails[${idx}].placeHolder`, e)
                        }
                        value={each.placeHolder}
                      />
                    </FieldWrapper>
                    <div className="flex">
                      {/* <FieldWrapper title={language.GENERIC_FIELD}>
                        <ToggleButton defaultValue={each.isGenericField ? 1 : 0} />
                      </FieldWrapper> */}
                      <FieldWrapper title={language.QUESTINONAIRE}>
                        <ToggleButton
                          change={(e) =>
                            formik.setFieldValue(
                              `bankDetails[${idx}].isQuestionnaire`,
                              e === 1 ? true : false
                            )
                          }
                          defaultValue={each?.isQuestionnaire ? 1 : 0}
                        />
                      </FieldWrapper>
                      <FieldWrapper title={language.MANDATORY}>
                        <ToggleButton
                          change={(e) =>
                            formik.setFieldValue(
                              `bankDetails[${idx}].isMandatory`,
                              e === 1 ? true : false
                            )
                          }
                          defaultValue={each?.isMandatory ? 1 : 0}
                        />
                      </FieldWrapper>
                      <FieldWrapper title={language.DISPLAY_REQUIRED}>
                        <ToggleButton
                          change={(e) =>
                            formik.setFieldValue(
                              `bankDetails[${idx}].isDisplayRequired`,
                              e === 1 ? true : false
                            )
                          }
                          defaultValue={each?.isDisplayRequired ? 1 : 0}
                        />
                      </FieldWrapper>
                    </div>
                  </Section>
                </Flex>
              </Border>
            </Wrapper>
          ))}
          <FieldWrapper>
            <div
              onClick={() => {
                formik.setFieldValue("bankDetails", [
                  ...(formik.values?.bankDetails ?? []),
                  {
                    key: "untitles",
                    fieldType: "TEXT",
                    title: "Touch to Edit",
                    placeHolder: "Untitled",
                    hint: "Untitled",
                    validationMessage: "Untitled",
                    isMandatory: false,
                    textType: "ALPHANUMERIC",
                    maxTextCount: 10,
                    isQuestionnaire: false,
                    isGenericField: false,
                    isDisplayRequired: false,
                    fieldOptions: [],
                  },
                ]);
              }}
              className="mx-3 text-2xl"
            >
              <AiOutlinePlusSquare className="cursor-pointer text-black dark:text-white" />
            </div>
            {formik.values?.bankDetails[formik.values?.bankDetails?.length - 1]
              ?.isGenericField === false ? (
              <div
                onClick={() => {
                  formik.setFieldValue(
                    "bankDetails",
                    formik.values?.bankDetails.filter((each, idx) =>
                      each?.isGenericField === false &&
                      idx === formik.values?.bankDetails?.length - 1
                        ? false
                        : true
                    )
                  );
                }}
                className="mx-3 text-2xl"
              >
                <AiOutlineMinusSquare className="cursor-pointer text-black dark:text-white" />
              </div>
            ) : null}
          </FieldWrapper>
          <Section width="1/2">
            <FieldWrapper title={language.DEFAULT_BANK_KEY_IN_APP}>
              <TextField
                change={(e) => formik.setFieldValue("isDefaultBankDisplayKeyInApp", e)}
                value={formik.values.isDefaultBankDisplayKeyInApp}
              />
            </FieldWrapper>
          </Section>
        </Border>
      </Section>
      <Section width="2/5">
        {formik.values.availableCurrencies ? (
          <FieldWrapper title={language.SUPPORTED_CURRENCIES}>
            <div className="">
              {formik.values?.availableCurrencies?.map((each) => (
                <div className="mx-2 flex items-center">
                  <h1 className="text-sm">{each.currencyCode}</h1>
                  <p className="mx-4">-</p>
                  <p className="text-sm">{each.minimumAmount}</p>
                </div>
              ))}
            </div>
          </FieldWrapper>
        ) : null}
        {formik.values.bankList ? (
          <FieldWrapper title={language.BANK_LIST}>
            <div className="">
              <div className="mx-2 flex justify-between items-center">
                <div className="flex">
                  <h1 className="text-sm">{" " + language.ID}</h1>
                  <p className="mx-4">-</p>
                  <h1 className="text-sm">{language.NAME}</h1>
                </div>
                <p className="text-sm">{language.CODE}</p>
              </div>
              {formik.values?.bankList?.map((each) => (
                <div className="mx-2 flex justify-between items-center">
                  <div className="flex">
                    <h1 className="text-sm">{each.id}</h1>
                    <p className="mx-4">-</p>
                    <h1 className="text-sm">{each.name}</h1>
                  </div>
                  <p className="text-sm">{each.code}</p>
                </div>
              ))}
            </div>
          </FieldWrapper>
        ) : null}
      </Section>
    </FormWrapper>
  );
}
