// "payment_type"
import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const startingHeadings = [
    {
      id: 1,
      title: language.TYPE,
      key: "type",
      show: true,
    },
    {
      id: 2,
      title: language.GATE_WAY,
      key: "gateway",
      show: true,
    },
    {
      id: 3,
      title: language.MODE,
      key: "mode",
      show: true,
    },
    {
      id: 4,
      title: language.STATUS,
      key: "status",
      show: true,
    },
  ];
  if (admin.userType === "DEVELOPER") {
    startingHeadings.push({
      id: 5,
      title: language.EDIT,
      key: "payment_type",
      show: true,
    });
  }
  return (
    <Table
      title={language.PAYMENT_SETUP}
      startingHeadings={startingHeadings}
      list={A.HOST + A.ADMIN_PAYMENT_LIST}
      assignData={(data) =>
        data.map((payment) => ({
          _id: payment._id,
          type: payment.data.name,
          gateway: payment.data.gateWay,
          mode: language[payment.data.mode],
          payment_type: payment.data.isEditNeeded === true ? 1 : 0,
          status: payment.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      showAdd={false}
      showBulk={false}
      showSearch={true}
      showArchieve={false}
      editClick={(e) => history.push(NavLinks.ADMIN_SETUP_PAYMENTS_EDIT + "/" + e)}
      showAction={false}
      showEdit={false}
      showStatus={admin.userType === "DEVELOPER"}
      statusList={A.HOST + A.ADMIN_PAYMENT_STATUS}
    />
  );
}
