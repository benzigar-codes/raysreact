import React from "react";
import { AiFillCar } from "react-icons/ai";
import { FiArrowDown, FiEye, FiLoader, FiSearch } from "react-icons/fi";
import { IoMdCash } from "react-icons/io";
import { Waypoint } from "react-waypoint";
import gsap from "gsap";

import { DropdownNormal } from "../../../../../components/common/DropDownNormal";
import { TextField } from "../../../../../components/common/TextField";
import { TextFormat } from "../../../../../components/common/TextFormat";
import useLanguage from "../../../../../hooks/useLanguage";

import NavLinks from "../../../../../utils/navLinks.json";

const EachCard = ({ bg, title, number, Icon }) => {
  const [hovered, setHovered] = React.useState(false);
  return (
    <div
      className="w-1/4 p-5 cursor-pointer"
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
    >
      <div
        className={"rounded-xl w-full flex justify-between relative text-white transition"}
        style={{
          backgroundColor: bg,
          transform: hovered ? "scale(0.95)" : "scale(1)",
        }}
      >
        <div className="p-4 flex flex-col justify-start">
          <h1 className={"text-3xl transition"}>{number}</h1>
          <h1 className="text-sm">{title}</h1>
        </div>
        <Icon
          className={`m-4 p-1 text-5xl transition ${hovered ? "opacity-0" : "opacity-100"}`}
          style={{
            transform: hovered ? "translateY(-30px)" : "translate(0)",
          }}
        />
        <div
          className="absolute h-10 w-10 bg-gray-200 rounded-full transition"
          style={{
            bottom: -10,
            right: -10,
            transform: hovered ? "translateX(-50px)" : "translate(0)",
          }}
        ></div>
      </div>
    </div>
  );
};

export default function Index({ history }) {
  const [showMoreFilters, setShowMoreFilters] = React.useState(false);
  const { language } = useLanguage();

  React.useEffect(() => {
    gsap.fromTo(".earnings", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, []);

  return (
    <>
      <div className="flex justify-between px-4 py-3">
        <div className="mx-2 w-1/4 flex items-center opacity-0 earnings">
          <p className={"mx-2 flex"} style={{ whiteSpace: "pre" }}>
            User / Professional
          </p>
          <div className={"w-full"}>
            <DropdownNormal fields={[{ label: "All" }, { label: "User" }, { label: "Driver" }]} />
          </div>
        </div>
        <div className="mx-2 w-1/4 flex items-center opacity-0 earnings">
          <p className={"mx-3 flex"} style={{ whiteSpace: "pre" }}>
            Credit / Debit
          </p>
          <div className={"w-full"}>
            <DropdownNormal fields={[{ label: "All" }, { label: "Credit" }, { label: "Debit" }]} />
          </div>
        </div>
        <div className="mx-2 w-1/4 flex items-center opacity-0 earnings">
          <p className={"mx-2 flex"} style={{ whiteSpace: "pre" }}>
            Amount
          </p>
          <div className={"w-full"}>
            <DropdownNormal fields={[{ label: "All" }, { label: "Due Amount" }, { label: "Freezed Amount" }]} />
          </div>
        </div>
        <div className="mx-2 w-1/4 flex items-center mx-2 opacity-0 earnings">
          <div className={"w-full"}>
            <TextField
              padding={false}
              // onKeyDown={(e) => {
              //   if (e.key === "Enter") searchSubmit();
              // }}
              // iconSelect={searchSubmit}
              // change={(e) => searchChange(e)}
              icon={<FiSearch />}
              placeholder={language.SEARCH}
            />
          </div>
        </div>
      </div>
      {showMoreFilters && (
        <div className="flex px-4 py-3">
          <div className="mx-2 w-1/4 flex items-center">
            <p className={"w-1/4 mx-2 flex"}>Start Date</p>
            <div className={"w-3/4"}>
              <TextFormat
                format={"##-##-####"}
                placeholder="MM-DD-YYYY"
                mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
              />
            </div>
          </div>
          <div className="w-1/4 flex items-center">
            <p className={"mx-2 w-1/4 mx-2 flex"}>End Date</p>
            <div className={"w-3/4"}>
              <TextFormat
                format={"##-##-####"}
                placeholder="MM-DD-YYYY"
                mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
              />
            </div>
          </div>
        </div>
      )}
      <div
        className="absolute left-0 right-0 flex justify-center items-center opacity-0 earnings"
        style={{ zIndex: 10, transform: "translateY(-12px)" }}
      >
        <div
          onClick={() => setShowMoreFilters(!showMoreFilters)}
          className="px-4 py-1 transition rounded-xl bg-blue-800 text-sm flex items-center cursor-pointer hover:bg-green-800"
        >
          <p className={"text-white"}>{showMoreFilters ? "Hide Filters" : "More Filters"}</p>
          <FiArrowDown
            className={"mx-2 transition text-white"}
            style={{ transform: showMoreFilters ? "rotateZ(180deg)" : "rotateZ(0)" }}
          />
        </div>
      </div>
      <div className="flex bg-gray-100 dark:bg-gray-900 opacity-0 earnings">
        <EachCard bg="#335EFD" title="Total Transaction Amount" number="2" Icon={IoMdCash} />
        <EachCard bg="#F69534" title="Zervx Wallet" number="$2" Icon={IoMdCash} />
        <EachCard bg="#E44E33" title="Due Amount" number="$2" Icon={IoMdCash} />
        <EachCard bg="#2E6249" title="Freezed Amount" number="$2" Icon={IoMdCash} />
      </div>
      <div
        className="w-full shadow-md mt-4 rounded-lg overflow-y-scroll opacity-0 earnings"
        style={{ maxHeight: "90vh" }}
      >
        <table
          cellPadding="12"
          className="w-full relative text-gray-800 text-left dark:bg-gray-800 z-10 overflow-y-scroll"
          style={{ fontSize: 13 }}
        >
          <thead className="z-10">
            <tr>
              <td className="text-center sticky top-0 bg-blue-800 text-white">{language.TABLE_NO}</td>

              <td className="sticky top-0 bg-blue-800 text-white">User Info</td>
              <td className="sticky top-0 bg-blue-800 text-white">Tnx Date</td>
              <td className="sticky top-0 bg-blue-800 text-white">Transaction Info</td>
              <td className="sticky top-0 bg-blue-800 text-white">Tnx Amount</td>
              <td className="sticky top-0 bg-blue-800 text-white">Txn Type</td>
              <td className="sticky top-0 bg-blue-800 text-white">Txn Mode</td>

              <td className="text-center sticky top-0 bg-blue-800 text-white">{language.ACTION}</td>
            </tr>
          </thead>
          <tbody className="text-sm text-gray-600 dark:bg-gray-800">
            {[...new Array(30)].map((each, index) => (
              <tr className="bg-white dark:bg-gray-800 dark:text-gray-200 dark:border-gray-900 hover:bg-gray-100 cursor-pointer border-b-4 border-gray-100">
                <td onClick={() => {}} className="text-center">
                  {index + 1}
                </td>
                <td onClick={() => {}}>
                  <div className="flex flex-col">
                    <p>Japahar Jose</p>
                    <p>+91 88837 23423</p>
                    <p>example@gmail.com</p>
                  </div>
                </td>
                <td onClick={() => {}}>2015-05-18 01:52 A.M</td>
                <td onClick={() => {}}>Zervx</td>
                <td onClick={() => {}}>7.00</td>
                <td onClick={() => {}}>Zervx</td>
                <td onClick={() => {}}>CR</td>
                <td className="w-28">
                  <div className="flex items-center justify-center">
                    <button
                      // onClick={() => history.push(NavLinks.ADMIN_ASSET_EARNINGS_DETAILS + "/" + "20342938042380")}
                      title={language.VIEW}
                      className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                        document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                      }`}
                    >
                      <FiEye />
                    </button>
                  </div>
                </td>
              </tr>
            ))}
            {/* <Waypoint onEnter={() => {}}>
          <tr>
            <td align="center">
              <FiLoader className="animate-spin" />
            </td>
          </tr>
        </Waypoint> */}
          </tbody>
        </table>
      </div>
    </>
  );
}
