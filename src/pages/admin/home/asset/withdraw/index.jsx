import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import { format } from "date-fns";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  const formBankDetails = (data) => {
    const dataToParse = data ? (Array.isArray(data) ? data[0] : data) : {};
    return (
      <div>
        {Object.keys(dataToParse)
          ?.filter((each) => each !== "_id")
          ?.map((each) =>
            dataToParse[each] ? <p>{`${each} : ${dataToParse[each]}`}</p> : ""
          )}
      </div>
    );
  };
  return (
    <Table
      title={language.REFUNDS}
      startingHeadings={[
        {
          id: 1,
          title: language.NAME,
          key: "name",
          show: true,
        },
        {
          id: 3,
          title: language.AMOUNT,
          key: "amount",
          show: true,
        },
        {
          id: 3,
          title: language.TIME,
          key: "time",
          show: true,
        },
        {
          id: 4,
          title: language.STATUS,
          key: "verifyStatus",
          show: true,
        },
        {
          id: 5,
          title: language.PAYOUT_OPTION,
          key: "payOutOption",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_ASSET_WITHDRAW}
      assignData={(data) =>
        data.map((withdraw) => ({
          _id: withdraw._id,
          name: withdraw.name + " (" + withdraw.phoneNumber + ") ",
          amount: withdraw.amount,
          time: format(new Date(withdraw.time), "do MMM, yyyy p"),
          verifyStatus: language[withdraw.status],
          payOutOption: language[withdraw.payoutOption],
          infoText: formBankDetails(withdraw?.bankDetails),
        }))
      }
      bread={[]}
      showAdd={false}
      showUserProfessionalFilter={true}
      showStatus={true}
      showAction={false}
      showActiveInactiveFilter={false}
      showReimburseFilter={false}
      showArchieve={false}
      defaultShowField={language.USER + " " + language.ALL + " " + language.WITHDRAWS}
      showDateFilter={false}
      showView={true}
      viewClick={(e) => history.push(NavLinks.ADMIN_ASSET_TRANSACTIONS_VIEW + "/" + e)}
      showBulk={false}
      walletDashboard={false}
      showRefundStatusFilter={true}
      showSearch={true}
      showInfo={true}
      verifyClick={(e) => alert(e)}
      statusList={A.HOST + A.ADMIN_ROLE_STATUS}
    />
  );
}
