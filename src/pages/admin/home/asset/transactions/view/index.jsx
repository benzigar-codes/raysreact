import React from "react";
import gsap from "gsap/gsap-core";
import { useParams } from "react-router";
import axios from "axios";
import { format } from "date-fns";
import Lottie from "react-lottie";

import NavLinks from "../../../../../../utils/navLinks.json";
import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import JSONViewer from "../../../../../../components/common/JSONViewer";
import WalletRecharge from "../../../../../../components/WalletRecharge";
import { Section } from "../../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { Border } from "../../../../../../components/common/Border";
import TrackingMap from "../../../../../../components/TrackingMap";
import { Heading } from "../../../../../../components/common/Heading";
import { TextArea } from "../../../../../../components/common/TextArea";
import { Button } from "../../../../../../components/common/Button";
import { DetailsWrapper } from "../../../../../../components/common/DetailsWrapper";
import { Detail } from "../../../../../../components/common/Detail";
import { Documents } from "../../../../../../components/common/Documents";
import ImagePreview from "../../../../../../components/common/ImagePreview";
import { PopUp } from "../../../../../../components/common/PopUp";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";

import useLanguage from "../../../../../../hooks/useLanguage";
import useAdmin from "../../../../../../hooks/useAdmin";
import useImage from "../../../../../../hooks/useImage";
import useUtils from "../../../../../../hooks/useUtils";
import { Link } from "react-router-dom";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, authFailure, admin } = useAdmin();
  const { id } = useParams();
  const { morph } = useUtils();
  const [showRechargePage, setShowRechargePage] = React.useState(false);
  const { imageUrl } = useImage();

  const [note, setNote] = React.useState("");
  const [loading, setLoading] = React.useState(true);
  const [popup, setPop] = React.useState(null);
  const [data, setData] = React.useState(null);
  const [imageModel, setImagModel] = React.useState(false);

  const fetchDetails = async () => {
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_ASSET_TRANSACTION_VIEW + "/" + id,
        {},
        header
      );
      setData(data?.data);
    } catch (err) {
      authFailure(err);
      history.goBack();
    }
    setLoading(false);
  };

  const verifyStatus = async () => {
    try {
      setLoading(true);
      const { data } = await axios.post(
        A.HOST + A.ADMIN_ASSET_WITHDRAW + "/verify/" + id,
        {},
        header
      );
    } catch (err) {}
    fetchDetails();
    setLoading(false);
  };

  React.useEffect(() => {
    fetchDetails();
  }, []);

  React.useEffect(() => {
    if (loading === false)
      gsap.fromTo(".viewDriver", 0.8, { opacity: 0 }, { opacity: 1, stagger: 0.2 });
  }, [loading]);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      animate="viewDriver"
      width="3/4"
      bread={[{ id: 1, title: language.TRANSACTIONS }]}
      title={data?.transactionId}
    >
      <Section width="1/2">
        {popup != null && (
          <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />
        )}
        <Border>
          <Heading title={language.TRANSACTION_INFO} />
          <FieldWrapper>
            <DetailsWrapper>
              {/* <Detail
                title={language.FROM}
                value={
                  data?.from?.userType === "ADMIN"
                    ? language.ADMIN
                    : `${data?.from?.name} (${data?.from?.phoneNumber})`
                }
              />
              <Detail
                title={language.TO}
                value={
                  data?.to?.userType === "ADMIN"
                    ? language.ADMIN
                    : `${data?.to?.name} (${data?.to?.phoneNumber})`
                }
              /> */}
              <Detail title={language.TYPE} value={language[data?.transactionType]} />
              <Detail title={language.STATUS} value={language[data?.transactionStatus]} />
              <Detail title={language.AMOUNT} value={data?.transactionAmount} />
              <Detail
                title={language.ACTION}
                value={
                  <>
                    {data.transactionType === "WALLETWITHDRAWAL" ? (
                      <div className="flex">
                        <div
                          onClick={verifyStatus}
                          className="bg-blue-800 text-sm text-white px-2 py-1 rounded-xl cursor-pointer"
                        >
                          {language.CHECK_CURRENT_STATUS}
                        </div>
                        {/* <div className="mx-2 bg-blue-800 text-sm px-2 py-1 rounded-xl">
                      {language.CHECK_CURRENT_STATUS}
                    </div> */}
                      </div>
                    ) : null}
                  </>
                }
              />
              {/* <Detail
                title={language.TIME}
                value={format(new Date(data?.transactionDate), "Pp")}
              /> */}
            </DetailsWrapper>
          </FieldWrapper>
        </Border>
        {/* <Border animate="viewDriver">
          <Heading title={language.NOTES} />
          <FieldWrapper>
            <TextArea change={(e) => setNote(e)} value={note} />
          </FieldWrapper>
        </Border> */}
        {admin.userType === "DEVELOPER" && <JSONViewer data={data} />}
      </Section>
      <Section width="1/2">
        <Border>
          <Heading title={language.FROM} />
          <FieldWrapper>
            <DetailsWrapper>
              <Detail title={language.USER_TYPE} value={language[data?.from?.userType]} />
            </DetailsWrapper>
          </FieldWrapper>
        </Border>
        <Border>
          <Heading title={language.TO} />
          <FieldWrapper>
            <DetailsWrapper>
              <Detail title={language.USER_TYPE} value={language[data?.to?.userType]} />
            </DetailsWrapper>
          </FieldWrapper>
        </Border>
        {data?.accountsDetail && (
          <Border animate="viewDriver">
            {/* <div className="flex justify-between items-center">
            <Heading title={language.PERSONAL_DETAILS} />
            <div className="flex">
              <div className="w-full flex justify-end">
                <Link to={NavLinks.ADMIN_RIDES_DRIVERS + "/" + id}>
                  <Button title={language.VIEW_RECENT_RIDES} />
                </Link>
              </div>
            </div>
          </div> */}
            <Heading title={language.ACCOUNT_DETAILS} />
            <FieldWrapper>
              <DetailsWrapper>
                {Object.keys(data?.accountsDetail)?.map((each) => (
                  <Detail title={each} value={data?.accountsDetail[each]} />
                ))}
              </DetailsWrapper>
            </FieldWrapper>
          </Border>
        )}
      </Section>
    </FormWrapper>
  );
}
