// DEPENDENCIES
import React from "react";
import { format } from "date-fns";

// COMPONENTS
import Table from "../../../../../components/common/Table";

// JSON
import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";

// HOOKS
import useLanguage from "../../../../../hooks/useLanguage";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  // HOOKS
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <Table
      title={language.CORPORATES}
      startingHeadings={[
        {
          id: 1,
          title: language.ID,
          key: "officeID",
          show: true,
        },
        {
          id: 2,
          title: language.NAME,
          key: "name",
          show: true,
        },
        {
          id: 3,
          title: language.EMAIL,
          key: "email",
          show: true,
        },
        {
          id: 4,
          title: language.ADDRESS,
          key: "line",
          show: true,
        },
        {
          id: 5,
          title: language.CITY,
          key: "city",
          show: true,
        },
        {
          id: 6,
          title: language.STATE,
          key: "state",
          show: false,
        },
        {
          id: 7,
          title: language.COUNTRY,
          key: "country",
          show: false,
        },
        {
          id: 8,
          title: language.PHONE,
          key: "phone",
          show: true,
        },
        {
          id: 9,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_CORPORATE_LIST}
      assignData={(data) =>
        data.map((office) => ({
          _id: office._id,
          name: morph(office.data.officeName),
          email: morph(office.data.email),
          line: morph(office.data.locationAddress.line1),
          city: morph(office.data.locationAddress.city),
          state: morph(office.data.locationAddress.state),
          country: morph(office.data.locationAddress.country),
          officeID: morph(office.data.officeId),
          phone: morph(office.data.phone.code) + " " + morph(office.data.phone.number),
          status: office.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      bread={[{ id: 1, title: language.CORPORATES }]}
      showAdd={admin.privileges.CORPORATE.CORPORATE ? (admin.privileges.CORPORATE.CORPORATE.ADD ? true : false) : false}
      showArchieve={
        admin.privileges.CORPORATE.CORPORATE ? (admin.privileges.CORPORATE.CORPORATE.EDIT ? true : false) : false
      }
      showEdit={
        admin.privileges.CORPORATE.CORPORATE ? (admin.privileges.CORPORATE.CORPORATE.EDIT ? true : false) : false
      }
      addClick={() => history.push(NavLinks.ADMIN_CORPORATE_CORPORATE_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_CORPORATE_CORPORATE_EDIT + "/" + e)}
      statusList={A.HOST + A.ADMIN_CORPORATE_STATUS}
      showBulk={true}
      showPrivilege={false}
      showCorporatePaidFilter={true}
      showSearch={true}
      showAction={admin.privileges.CORPORATE.CORPORATE && admin.privileges.CORPORATE.CORPORATE.VIEW ? true : false}
      // showMembers={
      //   admin.privileges.CORPORATE.CORPORATE_EMPLOYEES ? (admin.privileges.CORPORATE.CORPORATE_EMPLOYEES.VIEW ? true : false) : false
      // }
      showView={
        admin.privileges.CORPORATE.CORPORATE ? (admin.privileges.CORPORATE.CORPORATE.VIEW ? true : false) : false
      }
      viewClick={(e) => history.push(NavLinks.ADMIN_CORPORATE_CORPORATE_VIEW + "/" + e)}
      add={admin.privileges.CORPORATE.CORPORATE ? (admin.privileges.CORPORATE.CORPORATE.ADD ? true : false) : false}
      edit={admin.privileges.CORPORATE.CORPORATE ? (admin.privileges.CORPORATE.CORPORATE.EDIT ? true : false) : false}
      privilegeClick={() => history.push(NavLinks.ADMIN_CORPORATE_CORPORATE_PRIVILEGES)}
    />
  );
}
