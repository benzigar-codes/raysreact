import React from "react";
import Table from "../../../../../../components/common/Table";
import useLanguage from "../../../../../../hooks/useLanguage";

import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";
import NavLinks from "../../../../../../utils/navLinks.json";
import useAdmin from "../../../../../../hooks/useAdmin";
import { format } from "date-fns";
import useUtils from "../../../../../../hooks/useUtils";
import { useParams } from "react-router";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { id } = useParams();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  const startingHeadings = [
    {
      id: 1,
      title: language.DATE,
      key: "billedDate",
      show: true,
    },
    {
      id: 2,
      title: language.RIDES,
      key: "ridesCount",
      show: true,
    },
    {
      id: 3,
      title: language.PAYMENT,
      key: "corporatePayment",
      show: true,
    },
    {
      id: 4,
      title: language.PAID_DATE,
      key: "paidDate",
      show: true,
    },
  ];
  if (admin.userType !== U.CORPORATE) {
    startingHeadings.push({
      id: 5,
      title: language.PAYMENT_NOTES,
      key: "paidNotes",
      show: true,
    });
  }
  startingHeadings.push({
    id: 6,
    title: language.AMOUNT,
    key: "corporateBillingAmountButton",
    show: true,
  });
  startingHeadings.push({
    id: 7,
    title: language.DOWNLOAD,
    key: "downloadPdf",
    show: true,
  });
  return (
    <Table
      title={language.BILLING_LISTS}
      startingHeadings={startingHeadings}
      list={A.HOST + A.ADMIN_CORPORATE_BILLING_LIST + "/" + id}
      assignData={(data) =>
        data.map((ride) => ({
          _id: ride._id,
          billedDate: format(new Date(ride.billedDate), "do MMM yyyy"),
          ridesCount: ride.ridesCount,
          corporatePayment: ride.paidStatus,
          paidNotes: ride.paidNotes && ride.paidNotes !== "" ? ride.paidNotes : "",
          paidDate: ride.paidDate ? format(new Date(ride.paidDate), "PP p") : language.NOT_AVAILABLE,
          corporateBillingAmountButton: ride.ridesList,
          downloadPdf: ride.invoiceUrl,
        }))
      }
      bread={[{ id: 1, title: language.CORPORATES, path: NavLinks.ADMIN_CORPORATE_CORPORATE_LIST }]}
      showArchieve={false}
      showBulk={false}
      showSearch={false}
      showAdd={false}
      showStatus={false}
      corporateID={id}
      corporateConfirmPayLink={A.HOST + A.ADMIN_CORPORATE_CONFIRM_PAYMENT}
      showEdit={false}
      showView={false}
      showAction={false}
    />
  );
}
