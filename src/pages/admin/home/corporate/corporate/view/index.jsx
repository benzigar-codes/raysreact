import React from "react";
import gsap from "gsap/gsap-core";
import axios from "axios";
import { format } from "date-fns";

import NavLinks from "../../../../../../utils/navLinks.json";
import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { Border } from "../../../../../../components/common/Border";
import { Heading } from "../../../../../../components/common/Heading";
import { TextArea } from "../../../../../../components/common/TextArea";
import { Button } from "../../../../../../components/common/Button";
import { DetailsWrapper } from "../../../../../../components/common/DetailsWrapper";
import { Detail } from "../../../../../../components/common/Detail";
import { Documents } from "../../../../../../components/common/Documents";

import useLanguage from "../../../../../../hooks/useLanguage";
import useAdmin from "../../../../../../hooks/useAdmin";
import { useParams } from "react-router";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import useImage from "../../../../../../hooks/useImage";
import useDebug from "../../../../../../hooks/useDebug";
import ImagePreview from "../../../../../../components/common/ImagePreview";
import { PopUp } from "../../../../../../components/common/PopUp";
import useUtils from "../../../../../../hooks/useUtils";
import { Link } from "react-router-dom";
import JSONViewer from "../../../../../../components/common/JSONViewer";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin, header, authFailure } = useAdmin();
  const { id } = useParams();

  const [loading, setLoading] = React.useState(true);
  const [popup, setPop] = React.useState(null);
  const [corporate, setCorporate] = React.useState({});
  const [imageModel, setImagModel] = React.useState(false);

  const fetchCorporate = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_CORPORATE_READ, { id }, header);
      setCorporate(data.data);
      setLoading(false);
    } catch (err) {
      //   authFailure(err);
      //   history.goBack();
      alert(err);
    }
  };

  React.useEffect(() => {
    fetchCorporate();
  }, []);

  React.useEffect(() => {
    if (loading === false) gsap.fromTo(".viewUser", 0.8, { opacity: 0 }, { opacity: 1, stagger: 0.2 });
  }, [loading]);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <>
      <FormWrapper
        animate="viewUser"
        width="3/4"
        info={
          corporate.billingData && Array.isArray(corporate.billingData) && corporate.billingData.length > 0
            ? language.CORPORATE_BENDING_PAYMENTS
            : null
        }
        bread={[{ id: 1, title: language.CORPORATES, path: NavLinks.ADMIN_CORPORATE_CORPORATE_LIST }]}
        title={corporate.officeName}
      >
        <Section width="1/2">
          {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
          <ImagePreview close={() => setImagModel(false)} show={imageModel} />
          <Border animate="viewUser">
            <div className="flex justify-between items-center">
              <Heading title={language.PERSONAL_DETAILS} />
              <div className="flex">
                <div className="w-full flex justify-end">
                  <Link to={NavLinks.ADMIN_CORPORATE_CORPORATE_RIDES + "/" + id}>
                    <Button title={language.VIEW_RECENT_RIDES} />
                  </Link>
                </div>
              </div>
            </div>
            {/* <Heading title={language.PERSONAL_DETAILS} /> */}
            <FieldWrapper>
              <DetailsWrapper>
                <Detail title={language.OFFICE_NAME} value={corporate.officeName} />
                <Detail title={language.OFFICE_ID} value={corporate.officeId} />
                <Detail title={language.EMAIL} value={corporate.email} />
                <Detail title={language.MOBILE} value={corporate.phone.code + " " + corporate.phone.number} />
                <Detail title={language.CONTACT_PERSON_NAME} value={corporate.contactPersonName} />
                <Detail title={language.FINANCE_PERSON_NAME} value={corporate.financePersonName} />
              </DetailsWrapper>
            </FieldWrapper>
          </Border>
          {/* <Border animate="viewUser">
          <Heading title={language.NOTES} />
          <FieldWrapper>
            <TextArea change={(e) => setNote(e)} value={note} />
          </FieldWrapper>
          <FieldWrapper>
            <Button onClick={noteSave} loading={notesLoading} title={language.SAVE} />
          </FieldWrapper>
        </Border> */}
          {admin.userType === "DEVELOPER" && <JSONViewer data={corporate} />}
        </Section>
        <Section width="1/2">
          <Border animate="viewUser">
            <div className="flex justify-between items-center">
              <Heading title={language.ADDRESS_DETAILS} />
              <div className="flex">
                <div className="w-full flex justify-end">
                  <Link to={NavLinks.ADMIN_CORPORATE_CORPORATE_BILLINGS + "/" + id}>
                    <Button title={language.VIEW_BILLING_LIST} />
                  </Link>
                </div>
              </div>
            </div>
            <FieldWrapper>
              <DetailsWrapper>
                <Detail title={language.FULL_ADDRESS} value={corporate.locationAddress.fulladdress} />
                <Detail title={language.ADDRESS_LINE_1} value={corporate.locationAddress.line1} />
                <Detail title={language.CITY} value={corporate.locationAddress.city} />
                <Detail title={language.STATE} value={corporate.locationAddress.state} />
                <Detail title={language.COUNTRY} value={corporate.locationAddress.country} />
                <Detail title={language.ZIPCODE} value={corporate.locationAddress.zipcode} />
              </DetailsWrapper>
            </FieldWrapper>
          </Border>
        </Section>
      </FormWrapper>
    </>
  );
}
