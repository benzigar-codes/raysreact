import React from "react";

const TopMenuWrapper = ({ children, ...rest }) => {
  return (
    <div className="z-20 fixed top-0 right-0 left-0 bg-white dark:bg-gray-800 dark:text-white text-black shadow-md p-3 flex justify-between items-center" style={{ height: "70px" }} {...rest}>
      {children}
    </div>
  );
};

export default TopMenuWrapper;
