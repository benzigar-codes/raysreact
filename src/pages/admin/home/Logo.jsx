import React from "react";
import { Link } from "react-router-dom";

const Logo = ({ src, ...rest }) => {
  return (
    <Link to="/admin/home">
      <img {...rest} className="h-8" src={src} alt="" />
    </Link>
  );
};

export default Logo;
