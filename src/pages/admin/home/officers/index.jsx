// DEPENDENCIES
import React from "react";

// COMPONENTS
import Table from "../../../../components/common/Table";

// JSON
import A from "../../../../utils/API.js";
import U from "../../../../utils/utils.js";
import NavLinks from "../../../../utils/navLinks.json";

// HOOKS
import useLanguage from "../../../../hooks/useLanguage";
import useAdmin from "../../../../hooks/useAdmin";
import useUtils from "../../../../hooks/useUtils";

export default function Index({ history, location }) {
  // HOOKS
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <Table
      title={language.RESPONSE_OFFICERS}
      startingHeadings={[
        {
          id: 1,
          title: language.NAME,
          key: "name",
          show: true,
        },
        {
          id: 2,
          title: language.EMAIL,
          key: "email",
          show: true,
        },
        {
          id: 3,
          title: language.PHONE,
          key: "phone",
          show: true,
        },
        {
          id: 4,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_RESPONSE_OFFICERS_LIST}
      assignData={(data) =>
        data.map((officers) => ({
          _id: officers._id,
          name: morph(officers.data.firstName) + " " + morph(officers.data.lastName),
          email: morph(officers.data.email),
          phone: morph(officers.data.phone.code) + " " + morph(officers.data.phone.number),
          status: officers.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      bread={[
        {
          id: 1,
          title: language.RESPONSE_OFFICERS,
        },
      ]}
      showAdd={
        admin.privileges.ADMIN.HUBS_EMPLOYEES ? (admin.privileges.ADMIN.HUBS_EMPLOYEES.ADD ? true : false) : false
      }
      showArchieve={false}
      showEdit={
        admin.privileges.ADMIN.HUBS_EMPLOYEES ? (admin.privileges.ADMIN.HUBS_EMPLOYEES.EDIT ? true : false) : false
      }
      addClick={() => history.push(NavLinks.ADMIN_RESPONSE_OFFICERS_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_RESPONSE_OFFICERS_EDIT + "/" + e)}
      showBulk={true}
      showSearch={true}
      statusList={A.HOST + A.ADMIN_RESPONSE_OFFICERS_STATUS}
      showAction={
        admin.privileges.RESPONSE_OFFICERS.RESPONSE_OFFICERS_LIST
          ? admin.privileges.RESPONSE_OFFICERS.RESPONSE_OFFICERS_LIST.EDIT
            ? true
            : false
          : false
      }
      add={
        admin.privileges.RESPONSE_OFFICERS.RESPONSE_OFFICERS_LIST
          ? admin.privileges.RESPONSE_OFFICERS.RESPONSE_OFFICERS_LIST.ADD
            ? true
            : false
          : false
      }
      edit={
        admin.privileges.RESPONSE_OFFICERS.RESPONSE_OFFICERS_LIST
          ? admin.privileges.RESPONSE_OFFICERS.RESPONSE_OFFICERS_LIST.EDIT
            ? true
            : false
          : false
      }
    />
  );
}
