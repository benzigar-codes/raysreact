import React from "react";
import { useFormik } from "formik";
import gsap from "gsap/gsap-core";
import * as yup from "yup";
import axios from "axios";

import useLanguage from "../../../../../hooks/useLanguage";

import NavLinks from "../../../../../utils/navLinks.json";
import U from "../../../../../utils/utils.js";
import A from "../../../../../utils/API.js";

import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { Section } from "../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../components/common/TextField";
import { CountryCodesPicker } from "../../../../../components/common/CountryCodesPicker";
import { DropdownNormal } from "../../../../../components/common/DropDownNormal";
import { FileUpload } from "../../../../../components/common/FileUpload";
import useAdmin from "../../../../../hooks/useAdmin";
import useImage from "../../../../../hooks/useImage";
import { PopUp } from "../../../../../components/common/PopUp";
import useUtils from "../../../../../hooks/useUtils";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import { TextFormat } from "../../../../../components/common/TextFormat";
import useDebug from "../../../../../hooks/useDebug";
import { useParams } from "react-router";

export default function Index({ history }) {
  // HOOKS
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { compressImage, imageUrl, isBase64 } = useImage();
  const { id } = useParams();
  const { parseError } = useUtils();
  // STATES
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [offices, setOffices] = React.useState([]);
  const [popup, setPop] = React.useState(null);
  React.useEffect(() => gsap.fromTo(".profileSettings", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 }), []);
  const formik = useFormik({
    initialValues: {
      office: "",
      firstName: "",
      lastName: "",
      email: "",
      phoneCode: "",
      phoneNumber: "",
      avatar: "",
      gender: "",
      dob: "",
      status: "",
    },
    validationSchema: yup.object().shape({
      office: yup.string().required(language.REQUIRED),
      firstName: yup
        .string()
        .min(2, language.MIN + " 2")
        .max(16, language.MAX + " 16")
        .required(language.REQUIRED),
      dob: yup.string().required(language.REQUIRED),
      lastName: yup
        .string()
        .min(2, language.MIN + " 2")
        .max(16, language.MAX + " 16")
        .required(language.REQUIRED),
      email: yup.string().required(language.REQUIRED).email(language.INVALID_EMAIL),
      phoneCode: yup.string().required(language.REQUIRED),
      phoneNumber: yup
        .string()
        .min(6, language.MIN + " 6")
        .max(15, language.MAX + " 15")
        .required(language.REQUIRED),
      avatar: yup.string().required(language.REQUIRED),
      gender: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        const formData = new FormData();

        let image;
        if (isBase64(formik.values.avatar)) {
          image = await compressImage(formik.values.avatar);
        } else image = formik.values.avatar;

        formData.append("phoneCode", formik.values.phoneCode);
        formData.append("id", id);
        formData.append("phoneNumber", formik.values.phoneNumber);
        formData.append("firstName", formik.values.firstName);
        formData.append("lastName", formik.values.lastName);
        formData.append("email", formik.values.email);
        formData.append("gender", formik.values.gender);
        formData.append("dob", formik.values.dob);
        formData.append("officeId", formik.values.office);
        formData.append("status", formik.values.status);
        formData.append("avatar", image);

        await axios.post(A.HOST + A.ADMIN_RESPONSE_OFFICERS_UPDATE, formData, header);
        history.push(NavLinks.ADMIN_RESPONSE_OFFICERS_LIST);
      } catch (err) {
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
      setBtnLoading(false);
    },
  });

  React.useEffect(() => {
    const fetchEverything = async () => {
      const fetchOffices = async () => {
        try {
          const { data } = await axios.post(A.HOST + A.ADMIN_RESPONSE_OFFICE_ALL, {}, header);
          setOffices(data);
        } catch (err) {
          authFailure(err);
        }
      };
      const fetchOfficer = async () => {
        try {
          const { data } = await axios.post(A.HOST + A.ADMIN_RESPONSE_OFFICERS_READ, { id }, header);
          formik.setFieldValue("firstName", data.data.firstName);
          formik.setFieldValue("lastName", data.data.lastName);
          formik.setFieldValue("gender", data.data.gender);
          formik.setFieldValue("office", data.data.officeId);
          formik.setFieldValue("dob", data.data.dob);
          formik.setFieldValue("email", data.data.email);
          formik.setFieldValue("phoneCode", data.data.phone.code);
          formik.setFieldValue("phoneNumber", data.data.phone.number);
          formik.setFieldValue("avatar", data.data.avatar);
          formik.setFieldValue("status", data.data.status);
          setLoading(false);
          gsap.fromTo(".addOfficer", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
        } catch (err) {
          authFailure(err);
        }
      };
      await fetchOffices();
      fetchOfficer();
    };
    fetchEverything();
  }, []);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      width={"4/5"}
      bread={[{ id: 1, title: language.RESPONSE_OFFICERS, path: NavLinks.ADMIN_RESPONSE_OFFICERS_LIST }]}
      title={language.EDIT}
      animate={"addOfficer"}
      submit={formik.handleSubmit}
      submitBtn={true}
      btnLoading={btnLoading}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}

      <Section>
        <FieldWrapper animate={"addOfficer"} title={language.EMERGENCY_RESPONSE_OFFICE}>
          <DropdownNormal
            defaultValue={
              offices.filter((office) => office.officeId === formik.values.office).length > 0
                ? offices.filter((office) => office.officeId === formik.values.office)[0].officeName
                : ""
            }
            error={formik.errors.office}
            change={(e) => formik.setFieldValue("office", e)}
            fields={offices.map((each, index) => ({ id: index, label: each.officeName, value: each.officeId }))}
          />
        </FieldWrapper>
        <FieldWrapper animate={"addOfficer"} title={language.FULLNAME}>
          <TextField
            value={formik.values.firstName}
            width="2/4"
            type="text"
            margin={3}
            error={formik.errors.firstName}
            placeholder={language.FIRSTNAME}
            change={(e) => formik.setFieldValue("firstName", e.trim(), true)}
          />
          <TextField
            value={formik.values.lastName}
            width="2/4"
            type="text"
            placeholder={language.LASTNAME}
            error={formik.errors.lastName}
            change={(e) => formik.setFieldValue("lastName", e.trim(), true)}
          />
        </FieldWrapper>
        <FieldWrapper animate={"addOfficer"} title={language.EMAIL}>
          <TextField
            error={formik.errors.email}
            change={(e) => formik.setFieldValue("email", e)}
            value={formik.values.email}
            placeholder={language.EMAIL}
          />
        </FieldWrapper>
        <FieldWrapper animate={"addOfficer"} title={language.DATE_OF_BIRTH}>
          <TextFormat
            format={"##-##-####"}
            value={formik.values.dob}
            error={formik.errors.dob}
            change={(e) => formik.setFieldValue("dob", e)}
            placeholder="MM-DD-YYYY"
            mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
          />
        </FieldWrapper>
        <FieldWrapper animate={"addOfficer"} title={language.MOBILE}>
          <CountryCodesPicker
            placeholder={language.DIAL_CODE}
            error={formik.errors.phoneCode}
            defaultValue={formik.values.phoneCode}
            margin={3}
            change={(e) => formik.setFieldValue("phoneCode", e, true)}
            width="4/12"
          />
          <TextField
            width="8/12"
            type="number"
            value={formik.values.phoneNumber}
            placeholder={language.PHONE}
            error={formik.errors.phoneNumber}
            change={(e) => formik.setFieldValue("phoneNumber", e, true)}
          />
        </FieldWrapper>
        <FieldWrapper animate={"addOfficer"} title={language.GENDER}>
          <DropdownNormal
            error={formik.errors.gender}
            change={(e) => formik.setFieldValue("gender", e)}
            defaultValue={language[formik.values.gender]}
            fields={[
              {
                id: 1,
                label: language.FEMALE,
                value: "FEMALE",
              },
              {
                id: 2,
                label: language.MALE,
                value: "MALE",
              },
              {
                id: 3,
                label: language.OTHER,
                value: "OTHER",
              },
            ]}
          />
        </FieldWrapper>
      </Section>
      <Section>
        <FieldWrapper animate={"addOfficer"} title={language.AVATAR}>
          <FileUpload
            defaultValue={imageUrl(formik.values.avatar)}
            crop={true}
            ratio={1 / 1}
            change={(e) => formik.setFieldValue("avatar", e)}
          />
        </FieldWrapper>
      </Section>
    </FormWrapper>
  );
}
