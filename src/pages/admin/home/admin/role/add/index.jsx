// DEPENDENCIES
import React from "react";
import gsap from "gsap/gsap-core";
import { Formik, useFormik } from "formik";
import * as yup from "yup";
// COMPONENTS
import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../../components/common/TextField";
import { Border } from "../../../../../../components/common/Border";
import { Heading } from "../../../../../../components/common/Heading";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";
import Flex from "../../../../../../components/common/Flex";
// JSON
import NavLinks from "../../../../../../utils/navLinks.json";
import A from "../../../../../../utils/API.js";
import Privileges from "../../../../../../utils/privileges.js";
import U from "../../../../../../utils/utils.js";
// HOOKS
import useLanguage from "../../../../../../hooks/useLanguage";
import axios from "axios";
import useAdmin from "../../../../../../hooks/useAdmin";
import { PopUp } from "../../../../../../components/common/PopUp";

export default function Index({ history }) {
  // HOOKS
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  // STATES
  const [popup, setPop] = React.useState(null);
  const [selectAll, setSelectAll] = React.useState(false);
  const [btnLoading, setBtnLoading] = React.useState(false);
  const role = useFormik({
    initialValues: {
      role: "",
      status: U.ACTIVE,
      privileges: Privileges,
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      role: yup.string().required(language.REQUIRED),
      status: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        await axios.post(
          A.HOST + A.ADMIN_ROLE_UPDATE,
          {
            data: {
              role: e.role,
              forceUpdate: false,
              status: e.status,
              privileges: e.privileges,
            },
          },
          header
        );
        setBtnLoading(false);
        history.push(NavLinks.ADMIN_ADMIN_ROLE_LIST);
      } catch (err) {
        authFailure(err);
        setBtnLoading(false);
        setPop({ title: language.ERROR, type: "error" });
      }
    },
  });
  React.useEffect(() => {
    gsap.fromTo(".addRole", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, []);
  React.useEffect(() => {
    if (selectAll === false) {
      Object.keys(role.values.privileges).map((eachHeading) => {
        Object.keys(role.values.privileges[eachHeading]).map((eachField) => {
          Object.keys(role.values.privileges[eachHeading][eachField]).map((eachValue) => {
            role.setFieldValue(`privileges.${eachHeading}.${eachField}.${eachValue}`, false);
          });
        });
      });
    }
    if (selectAll === true) {
      Object.keys(role.values.privileges).map((eachHeading) => {
        Object.keys(role.values.privileges[eachHeading]).map((eachField) => {
          Object.keys(role.values.privileges[eachHeading][eachField]).map((eachValue) => {
            role.setFieldValue(`privileges.${eachHeading}.${eachField}.${eachValue}`, true);
          });
        });
      });
    }
  }, [selectAll]);
  return (
    <FormWrapper
      width={"3/4"}
      bread={[
        {
          id: 1,
          title: language.ROLE_LIST,
          path: NavLinks.ADMIN_ADMIN_ROLE_LIST,
        },
      ]}
      btnLoading={btnLoading}
      title={language.ADD_ROLE}
      submitBtn={true}
      animate={"addRole"}
      submit={role.handleSubmit}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <Section>
        <Flex>
          <Section padding={false}>
            <FieldWrapper animate={"addRole"} title={language.ROLE}>
              <TextField
                change={(e) => role.setFieldValue("role", e.toUpperCase())}
                value={role.values.role}
                placeholder={language.ROLE}
                error={role.errors.role}
              />
            </FieldWrapper>
          </Section>
          <Section padding={false}>
            <FieldWrapper animate={"addRole"} title={language.SELECT_ALL}>
              <ToggleButton change={() => setSelectAll(!selectAll)} defaultValue={selectAll === true ? 1 : 0} />
            </FieldWrapper>
          </Section>
        </Flex>
        <FieldWrapper animate={"addRole"} flex={false} title={language.PRIVILIGES}>
          <div className="flex flex-wrap">
            <div className="w-full md:w-1/2 px-2">
              {Object.keys(role.values.privileges)
                .filter((navHeading, index) => index % 2 === 0)
                .map((navHeading) => (
                  <Border animate={"addRole"}>
                    {/* <Heading title={language[navHeading]} /> */}
                    <table
                      cellPadding="5"
                      className="w-full relative text-gray-800 dark:text-gray-300 text-left dark:bg-gray-800 z-10"
                      style={{ fontSize: 13 }}
                    >
                      <thead>
                        <th className="text-blue-800 text-base">{language[navHeading]}</th>
                        <th>{language.VIEW}</th>
                        <th>{language.ADD}</th>
                        <th>{language.EDIT}</th>
                      </thead>
                      {Object.keys(role.values.privileges[navHeading]).map((eachSection) => (
                        <tr>
                          <td>{language[eachSection]}</td>
                          <td>
                            {role.values.privileges[navHeading][eachSection] ? (
                              role.values.privileges[navHeading][eachSection].VIEW !== undefined && (
                                <ToggleButton
                                  change={(e) => {
                                    role.values.privileges[navHeading][eachSection].VIEW === false
                                      ? Object.keys(role.values.privileges[navHeading][eachSection]).map((eachValue) =>
                                          role.setFieldValue(
                                            `privileges.${navHeading}.${eachSection}.${eachValue}`,
                                            true
                                          )
                                        )
                                      : Object.keys(role.values.privileges[navHeading][eachSection]).map((eachValue) =>
                                          role.setFieldValue(
                                            `privileges.${navHeading}.${eachSection}.${eachValue}`,
                                            false
                                          )
                                        );
                                  }}
                                  defaultValue={role.values.privileges[navHeading][eachSection].VIEW === true ? 1 : 0}
                                />
                              )
                            ) : (
                              <div></div>
                            )}
                          </td>
                          <td>
                            {role.values.privileges[navHeading][eachSection] ? (
                              role.values.privileges[navHeading][eachSection].ADD !== undefined && (
                                <ToggleButton
                                  change={(e) => {
                                    role.setFieldValue(
                                      `privileges.${navHeading}.${eachSection}.ADD`,
                                      e === 1 ? true : false
                                    );
                                  }}
                                  defaultValue={role.values.privileges[navHeading][eachSection].ADD === true ? 1 : 0}
                                />
                              )
                            ) : (
                              <div></div>
                            )}
                          </td>
                          <td>
                            {role.values.privileges[navHeading][eachSection] ? (
                              role.values.privileges[navHeading][eachSection].EDIT !== undefined && (
                                <ToggleButton
                                  change={(e) =>
                                    role.setFieldValue(
                                      `privileges.${navHeading}.${eachSection}.EDIT`,
                                      e === 1 ? true : false
                                    )
                                  }
                                  defaultValue={role.values.privileges[navHeading][eachSection].EDIT === true ? 1 : 0}
                                />
                              )
                            ) : (
                              <div></div>
                            )}
                          </td>
                        </tr>
                      ))}
                    </table>
                  </Border>
                ))}
            </div>
            <div className="w-full md:w-1/2 px-2">
              {Object.keys(Privileges)
                .filter((navHeading, index) => index % 2 !== 0)
                .map((navHeading) => (
                  <Border animate={"addRole"}>
                    <table
                      cellPadding="5"
                      className="w-full relative text-gray-800 dark:text-gray-300 text-left dark:bg-gray-800 z-10"
                      style={{ fontSize: 13 }}
                    >
                      <thead>
                        <th className="text-blue-800 text-base">{language[navHeading]}</th>
                        <th>View</th>
                        <th>Add</th>
                        <th>Edit</th>
                      </thead>
                      {Object.keys(role.values.privileges[navHeading]).map((eachSection) => (
                        <tr>
                          <td>{language[eachSection]}</td>
                          <td>
                            {role.values.privileges[navHeading][eachSection] ? (
                              role.values.privileges[navHeading][eachSection].VIEW !== undefined && (
                                <ToggleButton
                                  change={(e) => {
                                    role.values.privileges[navHeading][eachSection].VIEW === false
                                      ? Object.keys(role.values.privileges[navHeading][eachSection]).map((eachValue) =>
                                          role.setFieldValue(
                                            `privileges.${navHeading}.${eachSection}.${eachValue}`,
                                            true
                                          )
                                        )
                                      : Object.keys(role.values.privileges[navHeading][eachSection]).map((eachValue) =>
                                          role.setFieldValue(
                                            `privileges.${navHeading}.${eachSection}.${eachValue}`,
                                            false
                                          )
                                        );
                                  }}
                                  defaultValue={role.values.privileges[navHeading][eachSection].VIEW === true ? 1 : 0}
                                />
                              )
                            ) : (
                              <div></div>
                            )}
                          </td>
                          <td>
                            {role.values.privileges[navHeading][eachSection] ? (
                              role.values.privileges[navHeading][eachSection].ADD !== undefined && (
                                <ToggleButton
                                  change={(e) => {
                                    role.setFieldValue(
                                      `privileges.${navHeading}.${eachSection}.ADD`,
                                      e === 1 ? true : false
                                    );
                                  }}
                                  defaultValue={role.values.privileges[navHeading][eachSection].ADD === true ? 1 : 0}
                                />
                              )
                            ) : (
                              <div></div>
                            )}
                          </td>
                          <td>
                            {role.values.privileges[navHeading][eachSection] ? (
                              role.values.privileges[navHeading][eachSection].EDIT !== undefined && (
                                <ToggleButton
                                  change={(e) =>
                                    role.setFieldValue(
                                      `privileges.${navHeading}.${eachSection}.EDIT`,
                                      e === 1 ? true : false
                                    )
                                  }
                                  defaultValue={role.values.privileges[navHeading][eachSection].EDIT === true ? 1 : 0}
                                />
                              )
                            ) : (
                              <div></div>
                            )}
                          </td>
                        </tr>
                      ))}
                    </table>
                  </Border>
                ))}
            </div>
          </div>
        </FieldWrapper>
      </Section>
    </FormWrapper>
  );
}
