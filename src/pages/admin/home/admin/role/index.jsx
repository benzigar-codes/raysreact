import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <Table
      title={language.ROLE_LIST}
      startingHeadings={[
        {
          id: 1,
          title: language.ROLE,
          key: "role",
          show: true,
        },
        {
          id: 2,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_ROLE_LIST}
      assignData={(data) =>
        data.map((role) => ({
          _id: role._id,
          role: morph(role.data.role),
          status: role.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      bread={[{ id: 1, title: language.ROLE_LIST }]}
      showAdd={admin.privileges.ADMIN.ROLES ? (admin.privileges.ADMIN.ROLES.ADD ? true : false) : false}
      showArchieve={false}
      showEdit={admin.privileges.ADMIN.ROLES ? (admin.privileges.ADMIN.ROLES.EDIT ? true : false) : false}
      addClick={() => history.push(NavLinks.ADMIN_ADMIN_ROLE_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_ADMIN_ROLE_EDIT + "/" + e)}
      showBulk={true}
      showSearch={true}
      showAction={admin.privileges.ADMIN.ROLES ? (admin.privileges.ADMIN.ROLES.EDIT ? true : false) : false}
      add={admin.privileges.ADMIN.ROLES ? (admin.privileges.ADMIN.ROLES.ADD ? true : false) : false}
      edit={admin.privileges.ADMIN.ROLES ? (admin.privileges.ADMIN.ROLES.EDIT ? true : false) : false}
      statusList={A.HOST + A.ADMIN_ROLE_STATUS}
    />
  );
}
