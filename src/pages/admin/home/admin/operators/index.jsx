import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import axios from "axios";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin, header, authFailure } = useAdmin();
  const [roles, setRoles] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const { morph } = useUtils();
  const fetchRoles = async () => {
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_ROLE_LIST,
        { skip: 0, limit: 100, search: "", filter: "" },
        header
      );
      setRoles(data.response);
      setLoading(false);
    } catch (err) {
      authFailure(err);
    }
  };
  React.useEffect(() => fetchRoles(), []);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <Table
      title={language.OPERATORS}
      startingHeadings={[
        {
          id: 1,
          title: language.NAME,
          key: "name",
          show: true,
        },
        {
          id: 2,
          title: language.EMAIL,
          key: "email",
          show: true,
        },
        {
          id: 3,
          title: language.ROLE,
          key: "role",
          show: true,
        },
        {
          id: 4,
          title: language.MOBILE,
          key: "phone",
          show: true,
        },
        {
          id: 5,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_OPERATOR_LIST}
      assignData={(data) =>
        data.map((operator) => ({
          _id: operator._id,
          name: morph(operator.data.firstName) + " " + morph(operator.data.lastName),
          email: morph(operator.data.email),
          role:
            (roles.filter((eachRole) => operator.data.operatorRole === eachRole._id).length > 0
              ? morph(roles.filter((eachRole) => operator.data.operatorRole === eachRole._id)[0].data.role)
              : "____") +
            (operator.data.extraPrivileges
              ? Object.keys(operator.data.extraPrivileges).length === 0
                ? ""
                : " *"
              : ""),
          phone: morph(operator.data.phone.code) + " " + morph(operator.data.phone.number),
          status: operator.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      bread={[{ id: 1, title: language.OPERATORS }]}
      showFilter={true}
      showAdd={true}
      showArchieve={true}
      addClick={() => history.push(NavLinks.ADMIN_ADMIN_OPERATORS_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_ADMIN_OPERATORS_EDIT + "/" + e)}
      showBulk={true}
      showEdit={true}
      showSearch={true}
      statusList={A.HOST + A.ADMIN_OPERATOR_STATUS}
      showAction={admin.privileges.ADMIN.OPERATORS ? (admin.privileges.ADMIN.OPERATORS.EDIT ? true : false) : false}
      add={admin.privileges.ADMIN.OPERATORS ? (admin.privileges.ADMIN.OPERATORS.ADD ? true : false) : false}
      edit={admin.privileges.ADMIN.OPERATORS ? (admin.privileges.ADMIN.OPERATORS.EDIT ? true : false) : false}
    />
  );
}
