import React from "react";
import gsap from "gsap/gsap-core";
import * as yup from "yup";
import { useFormik } from "formik";
import axios from "axios";
import _ from "lodash";

import { TextField } from "../../../../../../components/common/TextField";
import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { DropdownNormal } from "../../../../../../components/common/DropDownNormal";
import Flex from "../../../../../../components/common/Flex";
import { DropDownSearch } from "../../../../../../components/common/DropDownSearch";

import NavLinks from "../../../../../../utils/navLinks.json";
import U from "../../../../../../utils/utils.js";
import A from "../../../../../../utils/API.js";
import privileges from "../../../../../../utils/privileges.js";

import useLanguage from "../../../../../../hooks/useLanguage";
import { CountryCodesPicker } from "../../../../../../components/common/CountryCodesPicker";
import { FileUpload } from "../../../../../../components/common/FileUpload";
import useAdmin from "../../../../../../hooks/useAdmin";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import useImage from "../../../../../../hooks/useImage";
import { Heading } from "../../../../../../components/common/Heading";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";
import { Border } from "../../../../../../components/common/Border";
import useUtils from "../../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { compressImage } = useImage();
  const { header, authFailure } = useAdmin();
  const { mergePrivilege } = useUtils();

  document.title = language.ADD_OPERATORS;
  // STATES
  const [loading, setLoading] = React.useState(true);
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [roles, setRoles] = React.useState([]);
  const [selectedRole, setSelectedRole] = React.useState(privileges);
  const [extraPrivileges, setExtraPrivileges] = React.useState({});
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      gender: "",
      phoneCode: "",
      phoneNumber: "",
      status: U.ACTIVE,
      privileges: {},
      role: "",
      avatar: "",
      password: "",
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      firstName: yup
        .string()
        .min(2, language.MIN + " 2")
        .max(16, language.MAX + " 16")
        .required(language.REQUIRED),
      lastName: yup
        .string()
        .min(1, language.MIN + " 1")
        .max(16, language.MAX + " 16")
        .required(language.REQUIRED),
      email: yup.string().required(language.REQUIRED).email(language.INVALID_EMAIL),
      gender: yup.string().required(language.REQUIRED),
      phoneCode: yup.string().required(language.REQUIRED),
      phoneNumber: yup
        .string()
        .min(6, language.MIN + " 6")
        .max(15, language.MAX + " 15")
        .required(language.REQUIRED),
      password: yup
        .string()
        .min(6, language.EXACT + " 6")
        .max(6, language.EXACT + " 15")
        .required(language.REQUIRED),
      role: yup.string().required(language.REQUIRED),
      avatar: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      if (e.avatar.length > 100) {
        let image = await compressImage(e.avatar);
        e = { ...e, avatar: image };
      }
      const formData = new FormData();
      formData.append("firstName", e.firstName);
      formData.append("lastName", e.lastName);
      formData.append("email", e.email);
      formData.append("gender", e.gender);
      formData.append("status", U.ACTIVE);
      formData.append("operatorRole", e.role);
      formData.append("avatar", e.avatar);
      formData.append("password", e.password);
      formData.append("phoneNumber", e.phoneNumber);
      formData.append("phoneCode", e.phoneCode);
      formData.append("extraPrivileges", JSON.stringify(extraPrivileges));
      try {
        await axios.post(A.HOST + A.ADMIN_OPERATOR_UPDATE, formData, header);
        setBtnLoading(false);
        history.push(NavLinks.ADMIN_ADMIN_OPERATORS_LIST);
      } catch (err) {
        alert(err?.response?.data?.message);
        authFailure(err);
        setBtnLoading(false);
      }
    },
  });

  const setRole = (roleId) => {
    const fromRole =
      roles.filter((eachRole) => eachRole._id === roleId).length > 0
        ? roles.filter((eachRole) => eachRole._id === roleId)[0].data.privileges
        : {};
    formik.setFieldValue("privileges", mergePrivilege(selectedRole, fromRole));
  };
  const fetchRole = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_ROLE_LIST_ALL, {}, header);
      if (data.response.length === 0) history.push(NavLinks.ADMIN_ADMIN_ROLE_ADD);
      setRoles(data.response);
      setLoading(false);
      gsap.fromTo(".addOperators", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
    } catch (err) {
      authFailure(err);
    }
  };

  const mergeExtras = (navHeading, eachSection, target = "VIEW", value = true) => {
    const extraRoleToBeAdded = JSON.parse(`{
             "${navHeading}": {
               "${eachSection}": {
                 "${target}": ${value}
               }
             }
           }`);
    setExtraPrivileges(_.merge(extraPrivileges, extraRoleToBeAdded));
    formik.setFieldValue(`privileges.${navHeading}.${eachSection}.${target}`, value);
  };

  React.useEffect(() => {
    fetchRole();
  }, []);
  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      animate={"addOperators"}
      submitBtn={true}
      btnLoading={btnLoading}
      submit={formik.handleSubmit}
      width="6/8"
      bread={[{ id: 1, title: language.OPERATORS, path: NavLinks.ADMIN_ADMIN_OPERATORS_LIST }]}
      title={language.ADD_OPERATORS}
    >
      <div className="w-full">
        <Flex align={false} wrap={true}>
          <div className="w-full lg:w-1/3">
            <FieldWrapper animate="addOperators" title={language.FIRSTNAME}>
              <TextField
                error={formik.errors.firstName}
                change={(e) => formik.setFieldValue("firstName", e)}
                value={formik.values.firstName}
                placeholder={language.FIRSTNAME}
              />
            </FieldWrapper>
            <FieldWrapper animate="addOperators" title={language.EMAIL}>
              <TextField
                error={formik.errors.email}
                change={(e) => formik.setFieldValue("email", e)}
                value={formik.values.email}
                placeholder={language.EMAIL}
              />
            </FieldWrapper>
            <FieldWrapper animate="addOperators" title={language.ROLE}>
              <DropdownNormal
                error={formik.errors.role}
                change={(e) => {
                  formik.setFieldValue("role", e);
                  setRole(e);
                }}
                selectText={language.SELECT_ROLE}
                fields={roles.map((eachRole, index) => ({
                  id: index,
                  label: eachRole.data.role,
                  value: eachRole._id,
                }))}
              />
              {/* <DropDownSearch
                defaultValue={roles.filter((eachRole) => eachRole._id === formik.values.role)[0].data.role}
                fields={roles.map((eachRole, index) => ({ id: index, label: eachRole.data.role, value: eachRole._id }))}
              /> */}
            </FieldWrapper>
            <FieldWrapper animate="addOperators" title={language.PIN}>
              <TextField
                type="password"
                change={(e) => (e.match(/^[0-9]+$/) != null || e === "") && formik.setFieldValue("password", e)}
                value={formik.values.password}
                error={formik.errors.password}
                // placeholder={"######"}
              />
            </FieldWrapper>
          </div>
          <div className="w-full lg:w-1/3">
            <FieldWrapper animate="addOperators" title={language.LASTNAME}>
              <TextField
                error={formik.errors.lastName}
                change={(e) => formik.setFieldValue("lastName", e)}
                value={formik.values.lastName}
                placeholder={language.LASTNAME}
              />
            </FieldWrapper>
            <FieldWrapper animate="addOperators" title={language.GENDER}>
              <DropdownNormal
                selectText={language.SELECT_GENDER}
                error={formik.errors.gender}
                change={(e) => formik.setFieldValue("gender", e)}
                fields={[
                  {
                    id: 1,
                    label: language.MALE,
                    value: "MALE",
                  },
                  {
                    id: 2,
                    label: language.FEMALE,
                    value: "FEMALE",
                  },
                  {
                    id: 3,
                    label: language.OTHER,
                    value: "OTHER",
                  },
                ]}
              />
            </FieldWrapper>
            <FieldWrapper animate="addOperators" title={language.MOBILE}>
              <CountryCodesPicker
                change={(e) => formik.setFieldValue("phoneCode", e)}
                defaultValue={formik.values.phoneCode}
                error={formik.errors.phoneCode}
                placeholder={"code"}
                margin={3}
                width="4/12"
              />
              <TextField
                change={(e) => formik.setFieldValue("phoneNumber", e)}
                value={formik.values.phoneNumber}
                error={formik.errors.phoneNumber}
                width="8/12"
                type="number"
                placeholder="number"
              />
            </FieldWrapper>
          </div>
          <div className="w-full lg:w-1/3">
            <FieldWrapper animate="addOperators" title={language.PRO_IMG}>
              <FileUpload ratio={1 / 1} change={(e) => formik.setFieldValue("avatar", e)} />
            </FieldWrapper>
          </div>
        </Flex>
        {/* Privileges Lists  */}
        {formik.values.role != "" && (
          <Flex>
            <Section>
              <FieldWrapper
                flex={false}
                title={Object.keys(extraPrivileges).length === 0 ? language.PRIVILIGES : language.PRIVILIGES + " *"}
              >
                <div className="flex flex-wrap">
                  <div className="w-full lg:w-1/2 px-2">
                    {Object.keys(formik.values.privileges)
                      .filter((navHeading, index) => index % 2 === 0)
                      .map((navHeading) => (
                        <Border>
                          {/* <Heading title={language[navHeading]} /> */}
                          <table
                            cellPadding="5"
                            className="w-full relative text-gray-800 dark:text-gray-300 text-left dark:bg-gray-800 z-10"
                            style={{ fontSize: 13 }}
                          >
                            <thead>
                              <th className="text-blue-800 text-base">{language[navHeading]}</th>
                              <th>{language.VIEW}</th>
                              <th>{language.ADD}</th>
                              <th>{language.EDIT}</th>
                            </thead>
                            {Object.keys(formik.values.privileges[navHeading]).map((eachSection) => (
                              <tr>
                                <td>{language[eachSection]}</td>
                                <td>
                                  {formik.values.privileges[navHeading][eachSection] ? (
                                    formik.values.privileges[navHeading][eachSection].VIEW !== undefined && (
                                      <ToggleButton
                                        change={(e) =>
                                          mergeExtras(
                                            navHeading,
                                            eachSection,
                                            "VIEW",
                                            !formik.values.privileges[navHeading][eachSection].VIEW
                                          )
                                        }
                                        defaultValue={
                                          formik.values.privileges[navHeading][eachSection].VIEW === true ? 1 : 0
                                        }
                                      />
                                    )
                                  ) : (
                                    <div></div>
                                  )}
                                </td>
                                <td>
                                  {formik.values.privileges[navHeading][eachSection] ? (
                                    formik.values.privileges[navHeading][eachSection].ADD !== undefined && (
                                      <ToggleButton
                                        change={(e) =>
                                          mergeExtras(
                                            navHeading,
                                            eachSection,
                                            "ADD",
                                            !formik.values.privileges[navHeading][eachSection].ADD
                                          )
                                        }
                                        defaultValue={
                                          formik.values.privileges[navHeading][eachSection].ADD === true ? 1 : 0
                                        }
                                      />
                                    )
                                  ) : (
                                    <div></div>
                                  )}
                                </td>
                                <td>
                                  {formik.values.privileges[navHeading][eachSection] ? (
                                    formik.values.privileges[navHeading][eachSection].EDIT !== undefined && (
                                      <ToggleButton
                                        change={(e) =>
                                          mergeExtras(
                                            navHeading,
                                            eachSection,
                                            "EDIT",
                                            !formik.values.privileges[navHeading][eachSection].EDIT
                                          )
                                        }
                                        defaultValue={
                                          formik.values.privileges[navHeading][eachSection].EDIT === true ? 1 : 0
                                        }
                                      />
                                    )
                                  ) : (
                                    <div></div>
                                  )}
                                </td>
                              </tr>
                            ))}
                          </table>
                        </Border>
                      ))}
                  </div>
                  <div className="w-full lg:w-1/2 px-2">
                    {Object.keys(formik.values.privileges)
                      .filter((navHeading, index) => index % 2 !== 0)
                      .map((navHeading) => (
                        <Border>
                          {/* <Heading title={language[navHeading]} /> */}
                          <table
                            cellPadding="5"
                            className="w-full relative text-gray-800 dark:text-gray-300 text-left dark:bg-gray-800 z-10"
                            style={{ fontSize: 13 }}
                          >
                            <thead>
                              <th className="text-blue-800 text-base">{language[navHeading]}</th>
                              <th>View</th>
                              <th>Add</th>
                              <th>Edit</th>
                            </thead>
                            {Object.keys(formik.values.privileges[navHeading]).map((eachSection) => (
                              <tr>
                                <td>{language[eachSection]}</td>
                                <td>
                                  {formik.values.privileges[navHeading][eachSection] ? (
                                    formik.values.privileges[navHeading][eachSection].VIEW !== undefined && (
                                      <ToggleButton
                                        change={(e) =>
                                          mergeExtras(
                                            navHeading,
                                            eachSection,
                                            "VIEW",
                                            !formik.values.privileges[navHeading][eachSection].VIEW
                                          )
                                        }
                                        defaultValue={
                                          formik.values.privileges[navHeading][eachSection].VIEW === true ? 1 : 0
                                        }
                                      />
                                    )
                                  ) : (
                                    <div></div>
                                  )}
                                </td>
                                <td>
                                  {formik.values.privileges[navHeading][eachSection] ? (
                                    formik.values.privileges[navHeading][eachSection].ADD !== undefined && (
                                      <ToggleButton
                                        change={(e) =>
                                          mergeExtras(
                                            navHeading,
                                            eachSection,
                                            "ADD",
                                            !formik.values.privileges[navHeading][eachSection].ADD
                                          )
                                        }
                                        defaultValue={
                                          formik.values.privileges[navHeading][eachSection].ADD === true ? 1 : 0
                                        }
                                      />
                                    )
                                  ) : (
                                    <div></div>
                                  )}
                                </td>
                                <td>
                                  {formik.values.privileges[navHeading][eachSection] ? (
                                    formik.values.privileges[navHeading][eachSection].EDIT !== undefined && (
                                      <ToggleButton
                                        change={(e) =>
                                          mergeExtras(
                                            navHeading,
                                            eachSection,
                                            "EDIT",
                                            !formik.values.privileges[navHeading][eachSection].EDIT
                                          )
                                        }
                                        defaultValue={
                                          formik.values.privileges[navHeading][eachSection].EDIT === true ? 1 : 0
                                        }
                                      />
                                    )
                                  ) : (
                                    <div></div>
                                  )}
                                </td>
                              </tr>
                            ))}
                          </table>
                        </Border>
                      ))}
                  </div>
                </div>
              </FieldWrapper>
            </Section>
          </Flex>
        )}
      </div>
    </FormWrapper>
  );
}
