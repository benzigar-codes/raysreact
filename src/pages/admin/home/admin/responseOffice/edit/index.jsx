// DEPENDENCIES
import React from "react";
import { Loader } from "@googlemaps/js-api-loader";
import * as yup from "yup";
import { useFormik } from "formik";
import * as turf from "@turf/turf";
import gsap from "gsap/gsap-core";
import axios from "axios";
import { Helmet } from "react-helmet";
import { useParams } from "react-router";

import "react-day-picker/lib/style.css";

// JSON
import NavLinks from "../../../../../../utils/navLinks.json";
import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";

// HOOKS
import useLanguage from "../../../../../../hooks/useLanguage";
import useSettings from "../../../../../../hooks/useSettings";
import useAdmin from "../../../../../../hooks/useAdmin";
import useUtils from "../../../../../../hooks/useUtils";

// COMPONENTS
import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../../components/common/TextField";
import { CountryCodesPicker } from "../../../../../../components/common/CountryCodesPicker";
import { DropdownNormal } from "../../../../../../components/common/DropDownNormal";
import GooglePlaceComplete from "../../../../../../components/common/GooglePlaceComplete";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import { PopUp } from "../../../../../../components/common/PopUp";

export default function Index({ history }) {
  // HOOKS
  const { language } = useLanguage();
  const { settings } = useSettings();
  const { header, authFailure } = useAdmin();
  const { parseError } = useUtils();
  const { id } = useParams();

  // STATES
  const [googleLoaded, setGoogleLoaded] = React.useState(false);
  const [address, setAddress] = React.useState(null);
  const [page, setPage] = React.useState(1);
  const [citites, setCities] = React.useState([]);
  const [pointsWithin, setPointsWithin] = React.useState(null);
  const [loading, setLoading] = React.useState(true);
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);
  const [dateError, setDateError] = React.useState(null);

  // REF
  const Map = React.useRef(null);

  // FORMIKS
  const formik1 = useFormik({
    initialValues: {
      officeName: "",
      officeId: "",
      email: "",
      phoneCode: "",
      phoneNumber: "",
      password: "",
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      officeName: yup.string().required(language.REQUIRED),
      officeId: yup.string().required(language.REQUIRED),
      email: yup.string().required(language.REQUIRED).email(language.INVALID_EMAIL),
      phoneCode: yup.string().required(language.REQUIRED),
      phoneNumber: yup
        .string()
        .min(6, language.MIN + " 6")
        .max(15, language.MAX + " 15")
        .required(language.REQUIRED),
    }),
    onSubmit: (e) => {
      if (e.password !== "" && e.password.length !== 6) {
        setBtnLoading(false);
        return alert(language.PIN_LENGTH_DIALOG);
      }
      setPage(2);
    },
  });

  const formik2 = useFormik({
    initialValues: {
      city: "",
      address: "",
      accurateAddress: "",
      location: null,
      line1: "",
      cityName: "",
      state: "",
      country: "",
      zipcode: "",
    },
    validationSchema: yup.object().shape({
      city: yup.string().required(language.REQUIRED),
      accurateAddress: yup.string().required(language.REQUIRED),
      line1: yup.string().required(language.REQUIRED),
      cityName: yup.string().required(language.REQUIRED),
      state: yup.string().required(language.REQUIRED),
      country: yup.string().required(language.REQUIRED),
      zipcode: yup.string().required(language.REQUIRED),
    }),
    validateOnChange: false,
    onSubmit: async () => {
      setBtnLoading(true);
      try {
        await axios.post(
          A.HOST + A.ADMIN_RESPONSE_OFFICE_UPDATE,
          {
            id,
            data: {
              email: formik1.values.email,
              officeName: formik1.values.officeName,
              serviceArea: formik2.values.city,
              password: formik1.values.password,
              locationAddress: {
                line1: formik2.values.line1,
                city: formik2.values.cityName,
                state: formik2.values.state,
                country: formik2.values.country,
                zipcode: formik2.values.zipcode,
                fulladdress: address.results[0].formatted_address,
              },
              showAddress:
                formik2.values.line1 +
                ", " +
                formik2.values.cityName +
                ", " +
                formik2.values.state +
                ", " +
                formik2.values.country +
                " " +
                formik2.values.zipcode,
              phone: {
                code: formik1.values.phoneCode,
                number: formik1.values.phoneNumber,
              },
              location: {
                lat: formik2.values.location.lat,
                lng: formik2.values.location.lng,
              },
              officeId: formik1.values.officeId,
              status: "ACTIVE",
            },
          },
          header
        );
        setBtnLoading(false);
        history.push(NavLinks.ADMIN_ADMIN_RESPONSE_OFFICE_LIST);
      } catch (err) {
        setPop({ title: parseError(err), type: "error" });
        authFailure(err);
        setBtnLoading(false);
      }
    },
  });

  const assignAddress = (e) => {
    formik2.setFieldValue("line1", "");
    let line1set = false;
    e.results[0].address_components.forEach((address) =>
      address.types.forEach((add) => {
        if (add === "postal_code") formik2.setFieldValue("zipcode", address.long_name);
        if (add === "country") formik2.setFieldValue("country", address.long_name);
        if (add === "administrative_area_level_1") formik2.setFieldValue("state", address.long_name);
        if (add === "sublocality_level_1") {
          formik2.setFieldValue("line1", address.long_name);
          line1set = true;
        }
        if (add === "locality" && line1set === false) formik2.setFieldValue("line1", address.long_name);
        if (add === "administrative_area_level_2") formik2.setFieldValue("cityName", address.long_name);
      })
    );
  };
  // Cities Fetch
  const fetchCities = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_CITY_LIST, {}, header);
      setCities(data.response);
    } catch (err) {
      authFailure(err);
    }
  };

  const createMap = ({ target = Map.current, center = { lat: 12.9975729, lng: 80.2638949 }, zoom = 14 }) => {
    const map = new window.google.maps.Map(target, {
      center,
      zoom: zoom,
      zoomControl: true,
      mapTypeControl: false,
      scaleControl: true,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: true,
    });
    return map;
  };

  const nextClick = () => {
    if (page === 1) formik1.handleSubmit();
    else if (page === 2 && pointsWithin === true) formik2.handleSubmit();
    else return null;
  };

  const fetchOffice = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_RESPONSE_OFFICE_READ, { id }, header);
      //   Formik1
      formik1.setFieldValue("officeName", data.data.officeName);
      formik1.setFieldValue("officeId", data.data.officeId);
      formik1.setFieldValue("email", data.data.email);
      formik1.setFieldValue("phoneCode", data.data.phone.code);
      formik1.setFieldValue("phoneNumber", data.data.phone.number);
      //   Formik2
      formik2.setFieldValue("city", data.data.serviceArea);
      formik2.setFieldValue("address", data.data.locationAddress.fulladdress);
      setAddress({
        results: [
          {
            formatted_address: data.data.locationAddress.fulladdress,
          },
        ],
      });
      formik2.setFieldValue("accurateAddress", data.data.showAddress);
      formik2.setFieldValue("location", data.data.location);
      formik2.setFieldValue("line1", data.data.locationAddress.line1);
      formik2.setFieldValue("cityName", data.data.locationAddress.city);
      formik2.setFieldValue("state", data.data.locationAddress.state);
      formik2.setFieldValue("country", data.data.locationAddress.country);
      formik2.setFieldValue("zipcode", data.data.locationAddress.zipcode);
      setLoading(false);
    } catch (err) {}
  };
  React.useEffect(() => {
    const fetchEverything = async () => {
      await fetchOffice();
      await fetchCities();
      const loader = new Loader({
        apiKey: settings.mapApi.web,
        version: "weekly",
        libraries: ["places"],
      });
      loader
        .load()
        .then(() => {
          setGoogleLoaded(true);
          gsap.fromTo(".addResponseOffice", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
        })
        .catch((e) => {});
    };
    fetchEverything();
  }, []);
  React.useEffect(() => {
    if (
      googleLoaded === true &&
      page === 2 &&
      formik2.values.city !== "" &&
      citites.filter((city) => city._id === formik2.values.city).length > 0
    ) {
      setPointsWithin(null);
      const map = createMap({ target: Map.current });
      const coordinates = citites.filter((city) => city._id === formik2.values.city)[0].location.coordinates[0];
      var bounds = new window.google.maps.LatLngBounds();
      const coordinatesForPath = coordinates.map((each) => ({ lat: each[1], lng: each[0] }));
      let myPolygon = new window.google.maps.Polygon({
        paths: coordinatesForPath,
        map,
        strokeColor: "#262525",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#ba9c9c",
        fillOpacity: 0.35,
      });
      if (formik2.values.location !== null) {
        var points = turf.points([[formik2.values.location.lng, formik2.values.location.lat]]);
        var searchWithin = turf.polygon([coordinates]);
        var ptsWithin = turf.pointsWithinPolygon(points, searchWithin);
        if (ptsWithin.features.length > 0) {
          const marker = new window.google.maps.Marker({
            map,
            draggable: true,
            position: formik2.values.location,
          });
          marker.addListener("dragend", async (e) => {
            const latLng = JSON.parse(JSON.stringify(e)).latLng;
            formik2.setFieldValue("location", latLng);
            try {
              const { data } = await axios.get(
                `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latLng.lat},${latLng.lng}&key=${settings.mapApi.web}`
              );
              setAddress(data);
              assignAddress(data);
              formik2.setFieldValue("accurateAddress", data.results[0].formatted_address);
            } catch (err) {}
          });
          setPointsWithin(true);
        } else {
          setPointsWithin(false);
        }
      }
      coordinatesForPath.forEach((poly) => bounds.extend(poly));
      map.fitBounds(bounds);
    }
  }, [googleLoaded, page, formik2.values.city, citites, formik2.values.location]);
  return googleLoaded === false || loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      bread={[{ id: 1, title: language.EMERGENCY_RESPONSE_OFFICE, path: NavLinks.ADMIN_ADMIN_RESPONSE_OFFICE_LIST }]}
      title={language.EDIT + formik1.values.officeName}
      animate={"addResponseOffice"}
      showNext={page !== 2}
      page={page}
      totalPage={"2"}
      showPrev={page !== 1}
      prevClick={(e) => setPage(page - 1)}
      width={"3/4"}
      nextClick={nextClick}
      submitBtn={page === 2}
      submit={page === 1 ? formik1.handleSubmit : page === 2 ? formik2.handleSubmit : () => {}}
      btnLoading={btnLoading}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <Helmet>
        <script
          src={`https://maps.googleapis.com/maps/api/js?key=${settings.mapApi.web}&libraries=places`}
          async
        ></script>
      </Helmet>
      {page === 1 && (
        <div className="w-full flex flex-col md:flex-row">
          <div className="w-full md:w-1/2">
            <FieldWrapper title={language.OFFICE_NAME}>
              <TextField
                change={(e) => formik1.setFieldValue("officeName", e)}
                value={formik1.values.officeName}
                error={formik1.errors.officeName}
                placeholder={language.OFFICE_NAME}
              />
            </FieldWrapper>
            <FieldWrapper title={language.OFFICE_ID}>
              <TextField
                change={(e) => formik1.setFieldValue("officeId", e)}
                value={formik1.values.officeId}
                error={formik1.errors.officeId}
                placeholder={language.OFFICE_ID}
              />
            </FieldWrapper>
            <FieldWrapper title={language.EMAIL}>
              <TextField
                change={(e) => formik1.setFieldValue("email", e)}
                value={formik1.values.email}
                error={formik1.errors.email}
                placeholder={language.EMAIL}
              />
            </FieldWrapper>
            <FieldWrapper title={language.MOBILE}>
              <CountryCodesPicker
                change={(e) => formik1.setFieldValue("phoneCode", e)}
                placeholder={language.DIAL_CODE}
                error={formik1.errors.phoneCode}
                margin={3}
                width="4/12"
                defaultValue={formik1.values.phoneCode}
              />
              <TextField
                change={(e) => formik1.setFieldValue("phoneNumber", e)}
                error={formik1.errors.phoneNumber}
                width="8/12"
                type="number"
                placeholder="number"
                value={formik1.values.phoneNumber}
              />
            </FieldWrapper>
            <FieldWrapper title={language.PIN_CHANGE}>
              <TextField
                type="password"
                change={(e) => (e.match(/^[0-9]+$/) != null || e === "") && formik1.setFieldValue("password", e)}
                value={formik1.values.password}
                // placeholder={"######"}
              />
            </FieldWrapper>
          </div>
        </div>
      )}
      {page === 2 && (
        <div className="w-full flex flex-col md:flex-row">
          <div className="w-full md:w-1/2">
            <FieldWrapper title={language.CITY}>
              <DropdownNormal
                defaultValue={
                  formik2.values.city !== "" && citites.filter((city) => city._id === formik2.values.city).length > 0
                    ? citites.filter((city) => city._id === formik2.values.city)[0].locationName
                    : ""
                }
                change={(e) => formik2.setFieldValue("city", e)}
                fields={citites.map((city, index) => ({
                  id: index,
                  label: city.locationName,
                  value: city._id,
                }))}
                error={formik2.errors.city}
              />
            </FieldWrapper>
            {formik2.values.city !== "" && (
              <>
                <FieldWrapper title={language.ADDRESS}>
                  <GooglePlaceComplete
                    change={(e) => {
                      setAddress(e);
                      assignAddress(e);
                      formik2.setFieldValue("address", e.results[0].formatted_address);
                      formik2.setFieldValue("location", e.results[0].geometry.location);
                      formik2.setFieldValue("accurateAddress", e.results[0].formatted_address);
                    }}
                    placeholder={language.ADDRESS}
                  />
                </FieldWrapper>
                {pointsWithin === false && (
                  <FieldWrapper>
                    <p className="text-sm text-red-500">{language.HUB_INSIDE}</p>
                  </FieldWrapper>
                )}
                {formik2.values.address !== "" && pointsWithin === true && (
                  <>
                    <hr className="mt-5" />
                    <FieldWrapper title={language.STREET}>
                      <TextField
                        change={(e) => formik2.setFieldValue("line1", e)}
                        value={formik2.values.line1}
                        error={formik2.errors.line1}
                      />
                    </FieldWrapper>
                    <FieldWrapper title={language.CITY}>
                      <TextField
                        change={(e) => formik2.setFieldValue("cityName", e)}
                        value={formik2.values.cityName}
                        error={formik2.errors.cityName}
                      />
                    </FieldWrapper>
                    <FieldWrapper title={language.STATE}>
                      <TextField
                        change={(e) => formik2.setFieldValue("state", e)}
                        value={formik2.values.state}
                        error={formik2.errors.state}
                      />
                    </FieldWrapper>
                    <FieldWrapper title={language.COUNTRY}>
                      <TextField
                        change={(e) => formik2.setFieldValue("country", e)}
                        value={formik2.values.country}
                        error={formik2.errors.country}
                      />
                    </FieldWrapper>
                    <FieldWrapper title={language.ZIPCODE}>
                      <TextField
                        change={(e) => formik2.setFieldValue("zipcode", e)}
                        value={formik2.values.zipcode}
                        error={formik2.errors.zipcode}
                      />
                    </FieldWrapper>
                  </>
                )}
              </>
            )}
          </div>
          <div className="w-full md:w-1/2">
            {formik2.values.city !== "" && (
              <FieldWrapper title={language.MAP}>
                <div ref={Map} className="w-full" style={{ height: 600 }}></div>
              </FieldWrapper>
            )}
          </div>
        </div>
      )}
    </FormWrapper>
  );
}
