// DEPENDENCIES
import React from "react";

// COMPONENTS
import Table from "../../../../../../components/common/Table";

// JSON
import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";
import NavLinks from "../../../../../../utils/navLinks.json";

// HOOKS
import useLanguage from "../../../../../../hooks/useLanguage";
import useAdmin from "../../../../../../hooks/useAdmin";
import { useParams } from "react-router";

export default function Index({ history, location }) {
  // HOOKS
  const { hubID } = useParams();
  const { language } = useLanguage();
  const { admin } = useAdmin();
  return (
    <Table
      title={language.EMPLOYEES}
      startingHeadings={[
        {
          id: 1,
          title: language.NAME,
          key: "name",
          show: true,
        },
        {
          id: 2,
          title: language.EMAIL,
          key: "email",
          show: true,
        },
        {
          id: 3,
          title: language.PHONE,
          key: "phone",
          show: true,
        },
        {
          id: 4,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_HUB_EMPLOYEE_LIST + "/" + hubID}
      assignData={(data) =>
        data.map((employee) => ({
          _id: employee._id,
          name: employee.firstName + " " + employee.lastName,
          email: employee.email,
          phone: employee.phone.code + " " + employee.phone.number,
          status: employee.status === U.ACTIVE ? 1 : 0,
        }))
      }
      bread={
        location.state
          ? [
              {
                id: 1,
                title: language.HUBS_LIST,
                path: NavLinks.ADMIN_ADMIN_HUBS_LIST,
              },
              {
                id: 2,
                title: location.state ? location.state.hubsName : "",
                // path: NavLinks.ADMIN_ADMIN_HUBS_EDIT + "/" + hubID,
              },
            ]
          : [
              {
                id: 1,
                title: language.HUBS_LIST,
                path: NavLinks.ADMIN_ADMIN_HUBS_LIST,
              },
            ]
      }
      showAdd={
        admin.privileges.ADMIN.HUBS_EMPLOYEES ? (admin.privileges.ADMIN.HUBS_EMPLOYEES.ADD ? true : false) : false
      }
      showArchieve={
        admin.privileges.ADMIN.HUBS_EMPLOYEES ? (admin.privileges.ADMIN.HUBS_EMPLOYEES.EDIT ? true : false) : false
      }
      showEdit={
        admin.privileges.ADMIN.HUBS_EMPLOYEES ? (admin.privileges.ADMIN.HUBS_EMPLOYEES.EDIT ? true : false) : false
      }
      addClick={() => history.push(NavLinks.ADMIN_ADMIN_HUBS_LIST + hubID + NavLinks.ADMIN_ADMIN_HUBS_EMPLOYESS_ADD)}
      editClick={(e) =>
        history.push(NavLinks.ADMIN_ADMIN_HUBS_LIST + hubID + NavLinks.ADMIN_ADMIN_HUBS_EMPLOYESS_EDIT + "/" + e)
      }
      showBulk={true}
      showSearch={true}
      statusList={A.HOST + A.ADMIN_HUB_EMPLOYEE_STATUS}
      showAction={
        admin.privileges.ADMIN.HUBS_EMPLOYEES ? (admin.privileges.ADMIN.HUBS_EMPLOYEES.EDIT ? true : false) : false
      }
      add={admin.privileges.ADMIN.HUBS_EMPLOYEES ? (admin.privileges.ADMIN.HUBS_EMPLOYEES.ADD ? true : false) : false}
      edit={admin.privileges.ADMIN.HUBS_EMPLOYEES ? (admin.privileges.ADMIN.HUBS_EMPLOYEES.EDIT ? true : false) : false}
    />
  );
}
