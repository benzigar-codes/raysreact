import React from "react";
import _ from "lodash";
import gsap from "gsap/gsap-core";
import { useFormik } from "formik";
import axios from "axios";

import { FormWrapper } from "../../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../../../components/common/TextField";
import { ToggleButton } from "../../../../../../../components/common/ToggleButton";

import A from "../../../../../../../utils/API.js";
import Privileges from "../../../../../../../utils/privileges.js";
import NavLinks from "../../../../../../../utils/navLinks.json";

import useLanguage from "../../../../../../../hooks/useLanguage";
import { SmallLoader } from "../../../../../../../components/common/SmallLoader";
import useAdmin from "../../../../../../../hooks/useAdmin";
import { Border } from "../../../../../../../components/common/Border";
import { PopUp } from "../../../../../../../components/common/PopUp";
import useUtils from "../../../../../../../hooks/useUtils";

export default function Index({ history }) {
  // HOOKS
  const { language } = useLanguage();
  const { mergePrivilege } = useUtils();
  const { header, authFailure } = useAdmin();
  // STATES
  const [loading, setLoading] = React.useState(true);
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);
  const formik = useFormik({
    initialValues: {
      privileges: Privileges,
    },
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        await axios.post(
          A.HOST + A.ADMIN_RESPONSE_OFFICE_PRIVILEGE_UPDATE,
          {
            data: {
              privileges: e.privileges,
            },
          },
          header
        );
        setBtnLoading(false);
        history.goBack();
      } catch (err) {
        setBtnLoading(false);
        setPop({ title: language.ERROR, type: "error" });
      }
    },
  });
  const fetchPrivilege = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_RESPONSE_OFFICE_PRIVILEGE_READ, {}, header);
      formik.setFieldValue("privileges", mergePrivilege(Privileges, data.data.privileges));
      setLoading(false);
      gsap.fromTo(".addformik", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
    } catch (err) {
      alert(JSON.stringify(err));
      authFailure(err);
    }
  };
  React.useEffect(() => {
    fetchPrivilege();
  }, []);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      bread={[{ id: 1, title: language.EMERGENCY_RESPONSE_OFFICE, path: NavLinks.ADMIN_ADMIN_RESPONSE_OFFICE_LIST }]}
      title={language.PRIVILIGES}
      submitBtn={true}
      width="2/3"
      submit={formik.handleSubmit}
      btnLoading={btnLoading}
    >
      <Section>
        {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
        <div className="flex flex-wrap">
          <div className="w-full md:w-1/2 px-2">
            {Object.keys(formik.values.privileges)
              .filter((navHeading, index) => index % 2 === 0)
              .map((navHeading) => (
                <Border animate={"addformik"}>
                  {/* <Heading title={language[navHeading]} /> */}
                  <table
                    cellPadding="5"
                    className="w-full relative text-gray-800 dark:text-gray-300 text-left dark:bg-gray-800 z-10"
                    style={{ fontSize: 13 }}
                  >
                    <thead>
                      <th className="text-blue-800 text-base">{language[navHeading]}</th>
                      <th>{language.VIEW}</th>
                      <th>{language.ADD}</th>
                      <th>{language.EDIT}</th>
                    </thead>
                    {Object.keys(formik.values.privileges[navHeading]).map((eachSection) => (
                      <tr>
                        <td>{language[eachSection]}</td>
                        <td>
                          {formik.values.privileges[navHeading][eachSection] ? (
                            formik.values.privileges[navHeading][eachSection].VIEW !== undefined && (
                              <ToggleButton
                                change={(e) => {
                                  formik.values.privileges[navHeading][eachSection].VIEW === false
                                    ? Object.keys(formik.values.privileges[navHeading][eachSection]).map((eachValue) =>
                                        formik.setFieldValue(
                                          `privileges.${navHeading}.${eachSection}.${eachValue}`,
                                          true
                                        )
                                      )
                                    : Object.keys(formik.values.privileges[navHeading][eachSection]).map((eachValue) =>
                                        formik.setFieldValue(
                                          `privileges.${navHeading}.${eachSection}.${eachValue}`,
                                          false
                                        )
                                      );
                                }}
                                defaultValue={formik.values.privileges[navHeading][eachSection].VIEW === true ? 1 : 0}
                              />
                            )
                          ) : (
                            <div></div>
                          )}
                        </td>
                        <td>
                          {formik.values.privileges[navHeading][eachSection] ? (
                            formik.values.privileges[navHeading][eachSection].ADD !== undefined && (
                              <ToggleButton
                                change={(e) => {
                                  formik.setFieldValue(
                                    `privileges.${navHeading}.${eachSection}.ADD`,
                                    e === 1 ? true : false
                                  );
                                }}
                                defaultValue={formik.values.privileges[navHeading][eachSection].ADD === true ? 1 : 0}
                              />
                            )
                          ) : (
                            <div></div>
                          )}
                        </td>
                        <td>
                          {formik.values.privileges[navHeading][eachSection] ? (
                            formik.values.privileges[navHeading][eachSection].EDIT !== undefined && (
                              <ToggleButton
                                change={(e) =>
                                  formik.setFieldValue(
                                    `privileges.${navHeading}.${eachSection}.EDIT`,
                                    e === 1 ? true : false
                                  )
                                }
                                defaultValue={formik.values.privileges[navHeading][eachSection].EDIT === true ? 1 : 0}
                              />
                            )
                          ) : (
                            <div></div>
                          )}
                        </td>
                      </tr>
                    ))}
                  </table>
                </Border>
              ))}
          </div>
          <div className="w-full md:w-1/2 px-2">
            {Object.keys(formik.values.privileges)
              .filter((navHeading, index) => index % 2 !== 0)
              .map((navHeading) => (
                <Border animate={"addformik"}>
                  <table
                    cellPadding="5"
                    className="w-full relative text-gray-800 dark:text-gray-300 text-left dark:bg-gray-800 z-10"
                    style={{ fontSize: 13 }}
                  >
                    <thead>
                      <th className="text-blue-800 text-base">{language[navHeading]}</th>
                      <th>View</th>
                      <th>Add</th>
                      <th>Edit</th>
                    </thead>
                    {Object.keys(formik.values.privileges[navHeading]).map((eachSection) => (
                      <tr>
                        <td>{language[eachSection]}</td>
                        <td>
                          {formik.values.privileges[navHeading][eachSection] ? (
                            formik.values.privileges[navHeading][eachSection].VIEW !== undefined && (
                              <ToggleButton
                                change={(e) => {
                                  formik.values.privileges[navHeading][eachSection].VIEW === false
                                    ? Object.keys(formik.values.privileges[navHeading][eachSection]).map((eachValue) =>
                                        formik.setFieldValue(
                                          `privileges.${navHeading}.${eachSection}.${eachValue}`,
                                          true
                                        )
                                      )
                                    : Object.keys(formik.values.privileges[navHeading][eachSection]).map((eachValue) =>
                                        formik.setFieldValue(
                                          `privileges.${navHeading}.${eachSection}.${eachValue}`,
                                          false
                                        )
                                      );
                                }}
                                defaultValue={formik.values.privileges[navHeading][eachSection].VIEW === true ? 1 : 0}
                              />
                            )
                          ) : (
                            <div></div>
                          )}
                        </td>
                        <td>
                          {formik.values.privileges[navHeading][eachSection] ? (
                            formik.values.privileges[navHeading][eachSection].ADD !== undefined && (
                              <ToggleButton
                                change={(e) => {
                                  formik.setFieldValue(
                                    `privileges.${navHeading}.${eachSection}.ADD`,
                                    e === 1 ? true : false
                                  );
                                }}
                                defaultValue={formik.values.privileges[navHeading][eachSection].ADD === true ? 1 : 0}
                              />
                            )
                          ) : (
                            <div></div>
                          )}
                        </td>
                        <td>
                          {formik.values.privileges[navHeading][eachSection] ? (
                            formik.values.privileges[navHeading][eachSection].EDIT !== undefined && (
                              <ToggleButton
                                change={(e) =>
                                  formik.setFieldValue(
                                    `privileges.${navHeading}.${eachSection}.EDIT`,
                                    e === 1 ? true : false
                                  )
                                }
                                defaultValue={formik.values.privileges[navHeading][eachSection].EDIT === true ? 1 : 0}
                              />
                            )
                          ) : (
                            <div></div>
                          )}
                        </td>
                      </tr>
                    ))}
                  </table>
                </Border>
              ))}
          </div>
        </div>
      </Section>
    </FormWrapper>
  );
}
