import React from "react";
import * as yup from "yup";
import { useParams } from "react-router";
import { useFormik } from "formik";
import { FiPhone } from "react-icons/fi";

import { FormWrapper } from "../../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../../../components/common/TextField";
import { CountryCodesPicker } from "../../../../../../../components/common/CountryCodesPicker";
import { FileUpload } from "../../../../../../../components/common/FileUpload";

import NavLinks from "../../../../../../../utils/navLinks.json";
import A from "../../../../../../../utils/API.js";
import U from "../../../../../../../utils/utils.js";

import useLanguage from "../../../../../../../hooks/useLanguage";
import { DropdownNormal } from "../../../../../../../components/common/DropDownNormal";
import axios from "axios";
import useAdmin from "../../../../../../../hooks/useAdmin";
import useImage from "../../../../../../../hooks/useImage";
import { PopUp } from "../../../../../../../components/common/PopUp";
import gsap from "gsap/gsap-core";
import { SmallLoader } from "../../../../../../../components/common/SmallLoader";

export default function Index({ history }) {
  // HOOKS
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { hubID, id } = useParams();
  const { compressImage, imageUrl, isBase64 } = useImage();
  // STATES
  const [loading, setLoading] = React.useState(true);
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);

  const formik = useFormik({
    initialValues: {
      id,
      firstName: "",
      lastName: "",
      email: "",
      phoneCode: "",
      phoneNumber: "",
      gender: "",
      avatar: "",
      password: "",
    },
    validationSchema: yup.object().shape({
      firstName: yup.string().required(language.REQUIRED),
      lastName: yup.string().required(language.REQUIRED),
      email: yup.string().required(language.REQUIRED).email(language.INVALID_EMAIL),
      phoneCode: yup.string().required(language.REQUIRED),
      phoneNumber: yup
        .string()
        .min(6, language.MIN + " 6")
        .max(15, language.MAX + " 15")
        .required(language.REQUIRED),
      gender: yup.string().required(language.REQUIRED),
      avatar: yup.string().required(language.REQUIRED),
    }),
    validateOnChange: false,
    onSubmit: async (e) => {
      if (e.password !== "" && e.password.length !== 6) {
        setBtnLoading(false);
        return alert(language.PIN_LENGTH_DIALOG);
      }
      if (isBase64(e.avatar)) {
        const compressedImage = await compressImage(e.avatar);
        e = { ...e, avatar: compressedImage };
      }
      const formData = new FormData();
      formData.append("avatar", e.avatar);
      formData.append("hubsId", hubID);
      formData.append("firstName", e.firstName);
      formData.append("lastName", e.lastName);
      formData.append("email", e.email);
      formData.append("phoneCode", e.phoneCode);
      formData.append("password", e.password);
      formData.append("phoneNumber", e.phoneNumber);
      formData.append("gender", e.gender);
      formData.append("status", U.ACTIVE);
      formData.append("id", id);
      setBtnLoading(true);
      try {
        await axios.post(A.HOST + A.ADMIN_HUB_EMPLOYEE_UPDATE, formData, header);
        setBtnLoading(false);
        history.push(NavLinks.ADMIN_ADMIN_HUBS_LIST + hubID + NavLinks.ADMIN_ADMIN_HUBS_EMPLOYESS_LIST);
      } catch (err) {
        authFailure(err);
        setPop({ title: language.ERROR, type: "error" });
        setBtnLoading(false);
      }
    },
  });
  const fetchEmployee = async () => {
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_HUB_EMPLOYEE_READ,
        {
          hubsId: hubID,
          id,
        },
        header
      );
      formik.setFieldValue("firstName", data.data.firstName);
      formik.setFieldValue("lastName", data.data.lastName);
      formik.setFieldValue("gender", data.data.gender);
      formik.setFieldValue("email", data.data.email);
      formik.setFieldValue("phoneCode", data.data.phone.code);
      formik.setFieldValue("phoneNumber", data.data.phone.number);
      formik.setFieldValue("avatar", data.data.avatar);
      setLoading(false);
      gsap.fromTo(".employeeAdd", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
    } catch (err) {
      authFailure(err);
    }
  };
  React.useEffect(() => {
    fetchEmployee();
  }, []);
  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      bread={[
        { id: 1, title: language.HUBS_LIST, path: NavLinks.ADMIN_ADMIN_HUBS_LIST },
        {
          id: 2,
          title: language.EMPLOYEES,
          path: NavLinks.ADMIN_ADMIN_HUBS_LIST + hubID + NavLinks.ADMIN_ADMIN_HUBS_EMPLOYESS_LIST,
        },
      ]}
      title={language.EDIT + " " + formik.values.firstName + " " + formik.values.lastName}
      width="4/5"
      submit={formik.handleSubmit}
      submitBtn={true}
      btnLoading={btnLoading}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <Section>
        <FieldWrapper animate="employeeAdd" title={language.NAME}>
          <TextField
            error={formik.errors.firstName}
            change={(e) => formik.setFieldValue("firstName", e)}
            value={formik.values.firstName}
            placeholder={language.FIRSTNAME}
            margin={3}
          />
          <TextField
            error={formik.errors.lastName}
            change={(e) => formik.setFieldValue("lastName", e)}
            value={formik.values.lastName}
            placeholder={language.LASTNAME}
          />
        </FieldWrapper>
        <FieldWrapper animate="employeeAdd" title={language.EMAIL}>
          <TextField
            error={formik.errors.email}
            change={(e) => formik.setFieldValue("email", e)}
            value={formik.values.email}
            placeholder={language.EMAIL}
          />
        </FieldWrapper>
        <FieldWrapper animate="employeeAdd" title={language.GENDER}>
          <DropdownNormal
            defaultValue={language[formik.values.gender]}
            change={(e) => formik.setFieldValue("gender", e)}
            fields={[
              {
                id: 1,
                label: "Female",
                value: "FEMALE",
              },
              {
                id: 2,
                label: "Male",
                value: "MALE",
              },
              {
                id: 3,
                label: "Rather Not To Say",
                value: "OTHER",
              },
            ]}
            error={formik.errors.gender}
          />
        </FieldWrapper>
        <FieldWrapper animate="employeeAdd" title={language.MOBILE}>
          <CountryCodesPicker
            placeholder={language.DIAL_CODE}
            defaultValue={formik.values.phoneCode}
            margin={3}
            error={formik.errors.phoneCode}
            change={(e) => formik.setFieldValue("phoneCode", e, true)}
            width="4/12"
          />
          <TextField
            width="8/12"
            type="number"
            value={formik.values.phoneNumber}
            placeholder={language.PHONE}
            icon={<FiPhone />}
            error={formik.errors.phoneNumber}
            change={(e) => formik.setFieldValue("phoneNumber", e, true)}
          />
        </FieldWrapper>
        <FieldWrapper animate="employeeAdd" title={language.PIN_CHANGE}>
          <TextField
            type="password"
            change={(e) => (e.match(/^[0-9]+$/) != null || e === "") && formik.setFieldValue("password", e)}
            value={formik.values.password}
            // placeholder={"######"}
          />
        </FieldWrapper>
      </Section>
      <Section>
        <FieldWrapper animate="employeeAdd" title={language.PRO_IMG}>
          <FileUpload
            defaultValue={imageUrl(formik.values.avatar)}
            crop={true}
            ratio={1 / 1}
            change={(e) => formik.setFieldValue("avatar", e)}
          />
        </FieldWrapper>
      </Section>
    </FormWrapper>
  );
}
