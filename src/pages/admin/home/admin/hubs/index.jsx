// DEPENDENCIES
import React from "react";
import { format } from "date-fns";

// COMPONENTS
import Table from "../../../../../components/common/Table";

// JSON
import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";

// HOOKS
import useLanguage from "../../../../../hooks/useLanguage";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  // HOOKS
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <Table
      title={language.HUBS}
      startingHeadings={[
        {
          id: 1,
          title: language.HUBS_NAME,
          key: "hubsName",
          show: true,
        },
        {
          id: 2,
          title: language.AREA,
          key: "fulladdress",
          show: false,
        },
        {
          id: 3,
          title: language.CITY,
          key: "city",
          show: false,
        },
        {
          id: 4,
          title: language.STATE,
          key: "state",
          show: false,
        },
        {
          id: 5,
          title: language.EMAIL,
          key: "email",
          show: true,
        },
        {
          id: 6,
          title: language.PHONE,
          key: "phone",
          show: true,
        },
        {
          id: 7,
          title: language.START_TIME,
          key: "startTime",
          show: true,
        },
        {
          id: 8,
          title: language.END_TIME,
          key: "endTime",
          show: true,
        },
        {
          id: 9,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_HUB_LIST}
      assignData={(data) =>
        data.map((hub) => ({
          _id: hub._id,
          hubsName: hub.data.hubsName ? morph(hub.data.hubsName) : "",
          city: hub.data.locationAddress.city ? morph(hub.data.locationAddress.city) : "",
          fulladdress: morph(hub.data.locationAddress.line1),
          // employees: hub.data.employeeList.length === 0 ? language.NO_EMPLOYEE : hub.data.employeeList.length + " " + language.EMPLOYEES,
          state: hub.data.locationAddress.state,
          email: morph(hub.data.email),
          phone: morph(hub.data.phone.code) + " " + morph(hub.data.phone.number),
          startTime: format(new Date(hub.data.timeSettings.startTime), "p"),
          endTime: format(new Date(hub.data.timeSettings.endTime), "p"),
          status: hub.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      bread={[{ id: 1, title: language.HUBS_LIST }]}
      showAdd={admin.privileges.ADMIN.HUBS ? (admin.privileges.ADMIN.HUBS.ADD ? true : false) : false}
      showArchieve={admin.privileges.ADMIN.HUBS ? (admin.privileges.ADMIN.HUBS.EDIT ? true : false) : false}
      showEdit={admin.privileges.ADMIN.HUBS ? (admin.privileges.ADMIN.HUBS.EDIT ? true : false) : false}
      addClick={() => history.push(NavLinks.ADMIN_ADMIN_HUBS_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_ADMIN_HUBS_EDIT + "/" + e)}
      statusList={A.HOST + A.ADMIN_HUB_STATUS}
      showBulk={true}
      showSearch={true}
      showPrivilege={admin.privileges.ADMIN.HUBS ? (admin.privileges.ADMIN.HUBS.EDIT ? true : false) : false}
      showAction={
        admin.privileges.ADMIN.HUBS_EMPLOYEES
          ? admin.privileges.ADMIN.HUBS_EMPLOYEES.VIEW
            ? true
            : false
          : admin.privileges.ADMIN_HUBS
          ? admin.privileges.ADMIN.HUBS.EDIT
            ? true
            : false
          : false
      }
      showMembers={
        admin.privileges.ADMIN.HUBS_EMPLOYEES ? (admin.privileges.ADMIN.HUBS_EMPLOYEES.VIEW ? true : false) : false
      }
      add={admin.privileges.ADMIN.HUBS ? (admin.privileges.ADMIN.HUBS.ADD ? true : false) : false}
      edit={admin.privileges.ADMIN.HUBS ? (admin.privileges.ADMIN.HUBS.EDIT ? true : false) : false}
      membersClick={(id, hubsName) =>
        history.push(NavLinks.ADMIN_ADMIN_HUBS_LIST + id + NavLinks.ADMIN_ADMIN_HUBS_EMPLOYESS_LIST, { hubsName })
      }
      privilegeClick={() => history.push(NavLinks.ADMIN_ADMIN_HUBS_PRIVILEGES)}
    />
  );
}
