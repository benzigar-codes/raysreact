import React from "react";

const MainMenuIcons = ({ title = null, icon, children, alert = false, ...rest }) => {
  const [hovered, setHovered] = React.useState(false);
  return (
    <div className="relative">
      {alert && (
        <div className="h-3 w-3 bg-white bg-red-500 rounded-full absolute right-0" style={{ zIndex: 10 }}></div>
      )}
      {alert && (
        <div className="animate-ping h-3 w-3 bg-red-500 rounded-full absolute right-0" style={{ zIndex: 10 }}></div>
      )}
      <div
        className={"relative overflow-hidden"}
        onMouseEnter={() => setHovered(true)}
        onMouseLeave={() => setHovered(false)}
      >
        <div
          // title={title}
          className={`mx-1 transition duration-500 flex items-center ${
            alert ? "bg-blue-800" : "bg-green-800"
          } dark:text-gray-100 relative cursor-pointer text-white p-3 rounded-full hover:bg-blue-800`}
          {...rest}
        >
          <span
            className={"transition duration-300"}
            style={{
              transform: hovered ? "translateY(30px)" : "",
            }}
          >
            {icon}
          </span>
          <span
            className={"absolute transition duration-300"}
            style={{
              opacity: hovered ? 1 : 0,
              transform: hovered ? "translateY(0px)" : "translateY(-30px)",
            }}
          >
            {icon}
          </span>
          {title !== null && (
            <span
              className={"transition duration-300 absolute mx-2 ml-6 text-sm"}
              style={{
                opacity: hovered ? 1 : 0,
                transform: hovered ? "translateY(0px)" : "translateY(30px)",
              }}
            >
              {title}
            </span>
          )}
          {title !== null && (
            <span
              className={"transition duration-300 mx-2 text-sm"}
              style={{
                transform: hovered ? "translateY(-30px)" : "",
              }}
            >
              {title}
            </span>
          )}
        </div>
      </div>
      {children}
    </div>
  );
};

export default MainMenuIcons;
