import React from "react";
import { FiArrowDown, FiArrowUp, FiHome, FiRefreshCw, FiUsers } from "react-icons/fi";
import { AiOutlineCar } from "react-icons/ai";
import { FaCity } from "react-icons/fa";
import { RiUserStarLine, RiUserVoiceLine } from "react-icons/ri";
import { FaMoneyBill } from "react-icons/fa";
import { ResponsiveContainer, AreaChart, Area, Tooltip, XAxis, YAxis, CartesianGrid } from "recharts";
import axios from "axios";
import Clock from "react-clock";
import { addDays, format } from "date-fns";
import DayPicker from "react-day-picker";

import useAdmin from "../../../../hooks/useAdmin";
import useLanguage from "../../../../hooks/useLanguage";
import useSettings from "../../../../hooks/useSettings";

import A from "../../../../utils/API.js";
import U from "../../../../utils/utils";
import NavLinks from "../../../../utils/navLinks.json";
import CardPlaceHolder from "../../../../components/dashboard/CardPlaceHolder";
import { gsap } from "gsap";
import { SmallLoader } from "../../../../components/common/SmallLoader";

import "react-clock/dist/Clock.css";
import "react-day-picker/lib/style.css";
import useUtils from "../../../../hooks/useUtils";
import { Button } from "../../../../components/common/Button";
import { Link } from "react-router-dom";
import ReactDatePicker from "react-datepicker";

const CustomDateComponent = React.forwardRef(({ value, onClick }, ref) => {
  const { language } = useLanguage();
  return (
    // <button
    //   className={`dark:bg-gray-900 dark:text-white text-sm bg-white outline-none rounded-xl px-3 py-2 border-b-2 border-gray-100 dark:border-gray-800 focus:border-blue-800 text-sm dark:focus:border-blue-800 cursor-pointer`}
    //   onClick={onClick}
    //   style={{ fontSize: 12 }}
    //   ref={ref}
    // >
    //   {value}
    // </button>
    <Button click={onClick} title={language.SELECT_DATE} />
  );
});

const Wrapper = ({ top = "", children, animate = null }) => {
  return (
    <div
      className={`${top === 0 ? "" : "mt-5 "} ${
        animate !== null && `opacity-0 ${animate}`
      } bg-gray-100 dark:bg-black p-4 pl-5 rounded-xl`}
    >
      {children}
    </div>
  );
};

const Header = ({ title, loading = false, reloadClick = () => {}, children }) => {
  const { language } = useLanguage();
  return (
    <div className="flex justify-between">
      <div className="flex items-center">
        <p className="text-2xl text-blue-800">{title}</p>
      </div>
      <div className="flex items-center">
        <button
          onClick={reloadClick}
          data-title={language.REFRESH}
          className={`${
            loading === true && "animate-spin"
          } bg-blue-800 mt-1 cursor-pointer text-sm focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
            document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "ml-2" : "mr-2"
          }`}
        >
          <FiRefreshCw />
        </button>
        {children}
      </div>
    </div>
  );
};

const Select = ({ defaultValue = "", values = [{}], change = () => {}, ...props }) => {
  return (
    <select
      onChange={(e) => change(e.target.value)}
      className={
        "hover:border-blue-800 mx-2 px-2 py-2 rounded-xl bg-gray-50 border-t-2 text-sm text-gray-500 w-full focus:outline-none dark:bg-gray-900 dark:text-gray-200"
      }
      style={{ width: 90 }}
      {...props}
    >
      {values.map((value, index) => (
        <option selected={defaultValue === value.value} key={index} value={value.value}>
          {value.label}
        </option>
      ))}
    </select>
  );
};

const Field = ({ title, value }) => {
  return (
    <div className="flex justify-between items-center dark:border-gray-800 border-gray-100">
      <h1 className={"text-gray-500 dark:text-gray-300"} style={{ fontSize: 13 }}>
        {title}
      </h1>
      <h1 className={"font-bold mt-2 text-green-800 dark:text-white"}>{value}</h1>
    </div>
  );
};

const Card = ({
  animate = null,
  graphData = [{}],
  width = "1/4",
  title = "Users",
  children,
  showChart = true,
  icon,
  direction = "UP",
}) => {
  return (
    <div className={`w-${width} p-2 ${animate !== null && `opacity-0 ${animate}`}`}>
      <div className="bg-white dark:bg-gray-900 border-t-2 hover:border-blue-800 hover:bg-gray-50 cursor-pointer rounded-xl">
        <div className={"p-3"}>
          <div className="flex justify-between items-center text-md dark:text-gray-200">
            <h1>{title}</h1>
            <div className={"text-blue-800"}>{icon}</div>
          </div>
          <div className={"overflow-y-scroll"} style={{ height: showChart ? 125 : 175 }}>
            {children}
          </div>
        </div>
        {showChart === true && (
          <div className="w-full">
            <ResponsiveContainer width="100%" height={50}>
              <AreaChart margin={{ left: 0, right: 0, top: 10, bottom: 0 }} height={50} width={100} data={graphData}>
                <defs>
                  <linearGradient id="color" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="5%" stopColor={direction === "UP" ? "#206a5d" : "#206a5d"} stopOpacity={0.8} />
                    <stop offset="95%" stopColor={direction === "UP" ? "#206a5d" : "#206a5d"} stopOpacity={0} />
                  </linearGradient>
                </defs>
                <Area
                  type="monotone"
                  dataKey="count"
                  stroke={direction === "UP" ? "#206a5d" : "#206a5d"}
                  strokeWidth={3}
                  fillOpacity={0.5}
                  legendType={"diamond"}
                  fill="url(#color)"
                ></Area>
                <Tooltip />
                <XAxis hide={true} dataKey={"date"} />
              </AreaChart>
            </ResponsiveContainer>
          </div>
        )}
        {false && (
          <div
            className="p-2 text-gray-100 text-center"
            style={{
              fontSize: 13,
              backgroundColor: direction === "UP" ? "#5ACD69" : "#E43C45",
            }}
          >
            <div className={"flex items-center justify-center"}>
              {direction === "UP" ? <FiArrowUp className={"mx-1"} /> : <FiArrowDown className={"mx-1"} />} 20 %{" "}
              <p className={"mx-1 opacity-75"}>Since last 30 days</p>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin, header, authFailure } = useAdmin();
  const { morph } = useUtils();
  const [loading, setLoading] = React.useState(true);
  const { settings } = useSettings();

  const [cities, setCities] = React.useState(null);

  const [time, setTime] = React.useState(new Date());

  //Data
  const [users, setUsers] = React.useState(null);
  const [drivers, setDrivers] = React.useState(null);
  const [operators, setOperators] = React.useState(null);
  const [officers, setOfficers] = React.useState(null);
  const [rideCounts, setRideCounts] = React.useState(null);
  const [hubs, setHubs] = React.useState(null);
  const [vehicles, setVehicles] = React.useState(null);
  const [vehicleCategories, setVehicleCategories] = React.useState(null);
  const [billingDates, setBillingDates] = React.useState([
    settings.lastCoorperateBilledDate ? new Date(settings.lastCoorperateBilledDate) : new Date(),
    new Date(),
  ]);
  const [dateModified, setDateModified] = React.useState(false);

  const [serviceBookings, setServiceBookings] = React.useState(null);
  const [serviceHubs, setServiceHubs] = React.useState(null);

  const [corporateBookings, setCorporateBookings] = React.useState(null);
  const [rides, setRides] = React.useState(null);

  //Common Section
  const [commonDays, setCommonDays] = React.useState(7);
  const [commonLoading, setCommonLoading] = React.useState(true);

  //Service Section
  const [serviceDays, setServiceDays] = React.useState(7);
  const [serviceCity, setServiceCity] = React.useState("");
  const [serviceLoading, setServiceLoading] = React.useState(true);

  //Ride Section
  const [rideDays, setRideDays] = React.useState(7);
  const [rideCity, setRideCity] = React.useState("");
  const [rideLoading, setRideLoading] = React.useState(true);

  //Corporate Section
  const [corporateBookingDays, setCorporateBookingDays] = React.useState(7);
  const [corporateBookingLoading, setCorporateLoading] = React.useState(false);

  const [fareData, setFareData] = React.useState(null);
  const [fareLoading, setFareLoading] = React.useState(false);

  const [city, setCity] = React.useState("");

  const daysLists = [
    {
      label: language.TODAY,
      value: 1,
    },
    {
      label: language.LAST_7_DAYS,
      value: 7,
    },
    {
      label: language.LAST_3_WEEKS,
      value: 21,
    },
    {
      label: language.LAST_1_MONTH,
      value: 30,
    },
    {
      label: language.LIFETIME,
      value: 0,
    },
  ];

  const fetchCorporateBookings = async () => {
    setCorporateLoading(true);
    const fetchBookings = async () => {
      setCorporateBookings(null);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_DASHBOARD_CORPORATE_BOOKINGS,
          {
            type: corporateBookingDays === "0" ? "LIFETIME" : "FILTER",
            daysCount: corporateBookingDays,
            city: serviceCity,
            operatorId: admin._id,
          },
          header
        );
        let totalCount = 0,
          newCount = 0,
          ongoingCount = 0,
          expiredCount = 0,
          cancelCount = 0,
          endedCount = 0,
          userdenyCount = 0,
          scheduleCount = 0;
        data.forEach((eachBooking) => {
          newCount += parseInt(eachBooking.data.newCount);
          scheduleCount += parseInt(eachBooking.data.scheduleCount);
          totalCount += parseInt(eachBooking.data.totalCount);
          ongoingCount += parseInt(eachBooking.data.ongoingCount);
          expiredCount += parseInt(eachBooking.data.expiredCount);
          cancelCount += parseInt(eachBooking.data.cancelCount);
          endedCount += parseInt(eachBooking.data.endedCount);
          userdenyCount += parseInt(eachBooking.data.userdenyCount);
        });
        const bookings = {
          newCount,
          scheduleCount,
          totalCount,
          ongoingCount,
          expiredCount,
          cancelCount,
          endedCount,
          userdenyCount,
        };
        setCorporateBookings(bookings);
      } catch (err) {
        authFailure(err);
      }
      setCorporateLoading(false);
    };
    const fetchCategoryBookings = async () => {
      setVehicleCategories(null);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_DASHBOARD_VEHICLE_CATEGORY,
          {
            type: commonDays === "0" ? "LIFETIME" : "FILTER",
            daysCount: commonDays,
          },
          header
        );
        setVehicleCategories(data);
      } catch (err) {
        authFailure(err);
      }
    };
    fetchBookings();
    fetchCategoryBookings();
  };

  const fetchCommonData = async () => {
    setCommonLoading(true);
    const fetchUsers = async () => {
      setUsers(null);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_DASHBOARD_USERS,
          {
            type: commonDays === "0" ? "LIFETIME" : "FILTER",
            daysCount: commonDays,
          },
          header
        );
        setUsers(data);
      } catch (err) {
        authFailure(err);
      }
    };
    const fetchDrivers = async () => {
      setDrivers(null);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_DASHBOARD_DRIVERS,
          {
            type: commonDays === "0" ? "LIFETIME" : "FILTER",
            daysCount: commonDays,
          },
          header
        );
        setDrivers(data);
      } catch (err) {
        authFailure(err);
      }
    };
    const fetchOperators = async () => {
      setOperators(null);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_DASHBOARD_OPERATORS,
          {
            type: commonDays === "0" ? "LIFETIME" : "FILTER",
            daysCount: commonDays,
          },
          header
        );
        setOperators(data);
      } catch (err) {
        authFailure(err);
      }
    };
    const fetchOfficers = async () => {
      setOfficers(null);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_DASHBOARD_OFFICERS,
          {
            type: commonDays === "0" ? "LIFETIME" : "FILTER",
            daysCount: commonDays,
          },
          header
        );
        setOfficers(data);
      } catch (err) {
        authFailure(err);
      }
    };
    fetchUsers();
    const fetchRideCount = async () => {
      setRideCounts(null);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_DASHBOARD_RIDE_WITH_CITIES_COUNT,
          {
            type: commonDays === "0" ? "LIFETIME" : "FILTER",
            daysCount: commonDays,
          },
          header
        );
        setRideCounts(data);
      } catch (err) {
        authFailure(err);
      }
    };
    const fetchHubs = async () => {
      setHubs(null);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_DASHBOARD_HUBS,
          {
            type: commonDays === "0" ? "LIFETIME" : "FILTER",
            daysCount: commonDays,
          },
          header
        );
        setHubs(data);
      } catch (err) {
        authFailure(err);
      }
    };
    const fetchVehicles = async () => {
      setVehicles(null);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_DASHBOARD_VEHICLES,
          {
            type: commonDays === "0" ? "LIFETIME" : "FILTER",
            daysCount: commonDays,
          },
          header
        );
        setVehicles(data);
      } catch (err) {
        authFailure(err);
      }
    };
    const fetchVehicleCategories = async () => {
      setVehicleCategories(null);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_DASHBOARD_VEHICLE_CATEGORY,
          {
            type: commonDays === "0" ? "LIFETIME" : "FILTER",
            daysCount: commonDays,
          },
          header
        );
        setVehicleCategories(data);
      } catch (err) {
        authFailure(err);
      }
    };

    fetchUsers();
    setTimeout(() => fetchDrivers(), 0);
    setTimeout(() => fetchOperators(), 0);
    setTimeout(() => fetchOfficers(), 0);
    setTimeout(() => fetchHubs(), 0);
    setTimeout(() => fetchRideCount(), 0);
    setTimeout(() => fetchVehicles(), 0);
    setTimeout(() => fetchVehicles(), 0);
    setTimeout(() => fetchVehicleCategories(), 0);
  };

  const fetchCorporateFair = async () => {
    setFareLoading(true);
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_DASHBOARD_CORPORATE_FARE,
        {
          operaterId: admin._id,
          billingId: "",
          city: "",
          vehicleCategory: "",
          fromDate: dateModified ? addDays(new Date(billingDates[0]), 1) : new Date(billingDates[0]),
          toDate: dateModified ? addDays(new Date(billingDates[1]), 1) : new Date(billingDates[1]),
        },
        header
      );
      setFareData(data);
    } catch (err) {
      authFailure(err);
    }
    setFareLoading(false);
  };

  React.useEffect(() => {
    if (admin.userType === U.CORPORATE) fetchCorporateBookings();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [corporateBookingDays, serviceCity]);

  React.useEffect(() => {
    if (admin.userType === U.CORPORATE) fetchCorporateFair();
  }, [billingDates]);

  React.useEffect(() => {
    if (admin.userType !== U.CORPORATE) fetchCommonData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [commonDays]);

  const fetchService = async () => {
    setServiceLoading(true);
    const fetchHubs = async () => {
      setServiceHubs(null);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_DASHBOARD_SERVICE_BASED_HUBS,
          {
            type: serviceDays === "0" ? "LIFETIME" : "FILTER",
            daysCount: serviceDays,
            city: serviceCity,
          },
          header
        );
        setServiceHubs(data);
      } catch (err) {
        authFailure(err);
      }
    };
    const fetchBookings = async () => {
      setServiceBookings(null);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_DASHBOARD_RIDES,
          {
            type: serviceDays === "0" ? "LIFETIME" : "FILTER",
            daysCount: serviceDays,
            city: serviceCity,
          },
          header
        );
        let totalCount = 0,
          newCount = 0,
          ongoingCount = 0,
          expiredCount = 0,
          cancelCount = 0,
          endedCount = 0,
          userdenyCount = 0,
          scheduleCount = 0;
        data.forEach((eachBooking) => {
          newCount += parseInt(eachBooking.data.newCount);
          scheduleCount += parseInt(eachBooking.data.scheduleCount);
          totalCount += parseInt(eachBooking.data.totalCount);
          ongoingCount += parseInt(eachBooking.data.ongoingCount);
          expiredCount += parseInt(eachBooking.data.expiredCount);
          cancelCount += parseInt(eachBooking.data.cancelCount);
          endedCount += parseInt(eachBooking.data.endedCount);
          userdenyCount += parseInt(eachBooking.data.userdenyCount);
        });
        const bookings = {
          newCount,
          scheduleCount,
          totalCount,
          ongoingCount,
          expiredCount,
          cancelCount,
          endedCount,
          userdenyCount,
        };
        setServiceBookings(bookings);
      } catch (err) {
        authFailure(err);
      }
    };
    fetchHubs();
    fetchBookings();
    // setTimeout(() => fetchDrivers(), 0);
  };

  React.useEffect(() => {
    if (serviceCity !== "" && admin.userType !== U.CORPORATE) {
      fetchService();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [serviceDays, serviceCity]);

  // FETCH RIDES
  const fetchRideGraph = () => {
    setRideLoading(true);
    const fetchRides = async () => {
      setRides(null);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_DASHBOARD_RIDES,
          {
            type: rideDays === "0" ? "LIFETIME" : "FILTER",
            daysCount: rideDays,
            city: rideCity,
          },
          header
        );
        setRides(
          data.map((each) => ({
            date: each.date,
            totalCount: each.data.totalCount,
            newCount: each.data.newCount,
            ongoingCount: data.data.ongoingCount,
            expiredCount: data.data.expiredCount,
            cancelCount: data.data.cancelCount,
            endedCount: data.data.endedCount,
          }))
        );
      } catch (err) {
        authFailure(err);
      }
    };
    fetchRides();
  };

  React.useEffect(() => {
    if (rideCity !== "") fetchRideGraph();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rideDays, rideCity]);

  // CITY FETCH
  React.useEffect(() => {
    const fetchCities = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_CITY_LIST_ALL, {}, header);
        setCities(data.data);
        if (admin.userType === U.CORPORATE) setServiceCity("");
        else setServiceCity(data.data[0]._id);
        setRideCity(data.data[0]._id);
      } catch (err) {
        authFailure(err);
      }
    };
    fetchCities();
  }, []);

  React.useEffect(() => {
    if (cities !== null) {
      setLoading(false);
    }
  }, [cities]);

  React.useEffect(() => {
    if (loading === false)
      setTimeout(() => gsap.fromTo(".sides", 0.5, { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.3 }), [100]);
  }, [loading]);

  // ANIMATIONS FROM HERE

  React.useEffect(() => {
    if (commonLoading === false) gsap.fromTo(".common", 0.2, { opacity: 0, y: 2 }, { opacity: 1, y: 0, stagger: 0.1 });
  }, [commonLoading]);

  //SERVICE
  React.useEffect(() => {
    if (serviceBookings !== null && serviceHubs !== null) {
      setServiceLoading(false);
      setTimeout(() => gsap.fromTo(".service", 0.2, { opacity: 0, y: 2 }, { opacity: 1, y: 0, stagger: 0.1 }), [100]);
    }
  }, [serviceBookings, serviceHubs]);

  //RIDE
  React.useEffect(() => {
    if (rides !== null) {
      setRideLoading(false);
      setTimeout(() => gsap.fromTo(".rides", 0.2, { opacity: 0, y: 2 }, { opacity: 1, y: 0, stagger: 0.1 }), [100]);
    }
  }, [rides]);

  // TIME UPDATE FOR CLOCK
  React.useEffect(() => {
    const interval = setInterval(() => setTime(new Date()), 1000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  const [commonLoaded, setCommonLoaded] = React.useState(false);

  React.useEffect(() => {
    if (
      users !== null &&
      drivers !== null &&
      operators !== null &&
      officers !== null &&
      hubs !== null &&
      vehicles !== null &&
      vehicleCategories !== null &&
      rideCounts !== null
    )
      setCommonLoaded(true);
    else setCommonLoaded(false);
  }, [users, drivers, operators, officers, hubs, vehicles, vehicleCategories, rideCounts]);

  //COMMON
  // const commonLoaded =
  //   users !== null &&
  //   drivers !== null &&
  //   operators !== null &&
  //   officers !== null &&
  //   hubs !== null &&
  //   vehicles !== null &&
  //   vehicleCategories !== null &&
  //   rideCounts !== null;

  React.useEffect(() => {
    if (commonLoaded) {
      setCommonLoading(false);
    }
  }, [commonLoaded]);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <div className="flex flex-wrap bg-gray-100 dark:bg-gray-800" style={{ minHeight: "90vh" }}>
      {/* LEFT SIDE   */}
      <div
        className="w-3/4 p-2 px-10 py-5 bg-gray-200 dark:bg-gray-800 h-full overflow-y-scroll"
        style={{ overflowX: "hidden" }}
      >
        {/* Billing Alert */}
        {admin.userType === U.CORPORATE &&
          admin.billingData &&
          Array.isArray(admin.billingData) &&
          admin.billingData.length > 0 && (
            <p
              onClick={() => history.push(NavLinks.ADMIN_CORPORATE_CORPORATE_BILLINGS + "/" + admin._id)}
              className="cursor-pointer bg-red-500 text-white p-3 rounded-xl text-center text-sm"
            >
              {language.YOU_HAVE_UNPAID_BILLS_CORPORATE}
            </p>
          )}
        {admin.userType !== U.CORPORATE &&
          (admin.privileges.SETUP.VEHICLE_CATEGORYS.VIEW ||
            admin.privileges.RIDES.RIDES_LIST.VIEW ||
            admin.privileges.ADMIN.HUBS.VIEW ||
            admin.privileges.USERS.USERS_LIST.VIEW ||
            admin.privileges.DRIVERS.DRIVERS_LIST.VIEW) && (
            <Wrapper>
              <Header loading={commonLoading} reloadClick={fetchCommonData} title={language.DASHBOARD}>
                <Select change={(e) => setCommonDays(e)} defaultValue={commonDays} values={daysLists} />
              </Header>

              <div className="flex -ml-2 flex-wrap">
                {commonLoaded && users && admin.privileges.USERS.USERS_LIST.VIEW ? (
                  <Card
                    graphData={users.graphData}
                    direction={"DOWN"}
                    icon={<FiUsers />}
                    showChart={commonDays !== "1" && users.graphData.length > 1}
                    title={language.USERS}
                  >
                    <Field title={language.TOTAL} value={users.totalCount} />
                    <Field title={language.ACTIVE} value={users.activeCount} />
                    <Field title={language.INACTIVE} value={users.inactiveCount} />
                    <Field title={language.TODAY_REGISTERED} value={users.todayCount} />
                    <Field title={language.TRIED_BUT_FAILED} value={users.triedCount} />
                    <Field title={language.ARCHIEVE} value={users.archiveCount} />
                  </Card>
                ) : admin.privileges.USERS.USERS_LIST.VIEW ? (
                  <CardPlaceHolder />
                ) : null}
                {commonLoaded && admin.privileges.DRIVERS.DRIVERS_LIST.VIEW ? (
                  <Card
                    graphData={drivers.graphData}
                    direction={"DOWN"}
                    icon={<RiUserStarLine />}
                    showChart={commonDays !== "1" && drivers.graphData.length > 1}
                    title={language.DRIVERS}
                  >
                    <Field title={language.TOTAL} value={drivers.totalCount} />
                    <Field title={language.ACTIVE} value={drivers.activeCount} />
                    <Field title={language.UNVERIFIED} value={drivers.unverifiedCount} />
                    <Field title={language.TODAY_REGISTERED} value={drivers.todayCount} />
                    <Field title={language.TRIED_BUT_FAILED} value={drivers.triedCount} />
                    <Field title={language.INACTIVE} value={drivers.inactiveCount} />
                    <Field title={language.ARCHIEVE} value={drivers.archiveCount} />
                  </Card>
                ) : admin.privileges.DRIVERS.DRIVERS_LIST.VIEW ? (
                  <CardPlaceHolder />
                ) : null}
                {commonLoaded && admin.privileges.DRIVERS.DRIVERS_LIST.VIEW ? (
                  <Card
                    graphData={operators.graphData}
                    direction={"DOWN"}
                    icon={<RiUserVoiceLine />}
                    showChart={commonDays !== "1" && operators.graphData.length > 1}
                    title={language.OPERATORS}
                  >
                    <Field title={language.TOTAL} value={operators.totalCount} />
                    <Field title={language.ACTIVE} value={operators.activeCount} />
                    <Field title={language.TODAY_REGISTERED} value={operators.todayCount} />
                    {/* <Field title={"Special Privilege"} value={"3"} /> */}
                    <Field title={language.INACTIVE} value={operators.inactiveCount} />
                    <Field title={language.ARCHIEVE} value={operators.archiveCount} />
                  </Card>
                ) : admin.privileges.DRIVERS.DRIVERS_LIST.VIEW ? (
                  <CardPlaceHolder />
                ) : null}
                {/* {commonLoaded ? (
              <Card
                animate={"common"}
                graphData={officers.graphData}
                direction={"DOWN"}
                icon={<RiUserVoiceLine />}
                showChart={commonDays !== "1" && officers.graphData.length > 1}
                title={language.RESPONSE_OFFICERS}
              >
                <Field title={language.TOTAL} value={officers.totalCount} />
                <Field title={language.TODAY_REGISTERED} value={officers.todayCount} />
                <Field title={language.TRIED_BUT_FAILED} value={officers.triedCount} />
                <Field title={language.ACTIVE} value={officers.activeCount} />
                <Field title={language.INACTIVE} value={officers.inactiveCount} />
                <Field title={language.ARCHIEVE} value={officers.archiveCount} />
              </Card>
            ) : (
              <CardPlaceHolder />
            )} */}
                {commonLoaded && admin.privileges.ADMIN.HUBS.VIEW ? (
                  <Card
                    graphData={hubs.graphData}
                    direction={"DOWN"}
                    icon={<FiHome />}
                    showChart={commonDays !== "1" && hubs.graphData.length > 1}
                    title={language.HUBS}
                  >
                    <Field title={language.TOTAL} value={hubs.totalCount} />
                    <Field title={language.ACTIVE} value={hubs.activeCount} />
                    <Field title={language.TODAY_REGISTERED} value={hubs.todayCount} />
                    <Field title={language.INACTIVE} value={hubs.inactiveCount} />
                    <Field title={language.ARCHIEVE} value={hubs.archiveCount} />
                  </Card>
                ) : admin.privileges.ADMIN.HUBS.VIEW ? (
                  <CardPlaceHolder />
                ) : null}
                {commonLoaded && admin.privileges.SETUP.CITY_COMMISIONS.VIEW ? (
                  <Card
                    graphData={hubs.graphData}
                    direction={"DOWN"}
                    icon={<AiOutlineCar />}
                    showChart={commonDays !== "1" && vehicles.graphData !== undefined && vehicles.graphData.length > 1}
                    title={language.VEHICLES}
                  >
                    <Field title={language.TOTAL} value={vehicles.totalCount} />
                    <Field title={language.ACTIVE} value={vehicles.activeCount} />
                    <Field title={language.UNVERIFIED} value={vehicles.unverifiedCount} />
                    <Field title={language.INACTIVE} value={vehicles.inactiveCount} />
                  </Card>
                ) : admin.privileges.SETUP.CITY_COMMISIONS.VIEW ? (
                  <CardPlaceHolder />
                ) : null}
                {/* <Card direction={"DOWN"} icon={<AiOutlineCar />} title={"Vehicles"}>
              <Field title={"Total"} value={"10"} />
              <Field title={"Today Registered"} value={"2"} />
              <Field title={"Unverified"} value={"2"} />
              <Field title={"Active"} value={"3"} />
              <Field title={"Inactive"} value={"3"} />
            </Card> */}
                {commonLoaded && admin.privileges.RIDES.RIDES_LIST.VIEW ? (
                  <Card direction={"DOWN"} icon={<FaCity />} showChart={false} title={language.REQUEST_MADE}>
                    {rideCounts.length === 0 && (
                      <p className={"text-sm text-gray-500 my-1"}>{language.NO_DATA_FOUND_IN_GIVEN_TIME}</p>
                    )}
                    {rideCounts.length > 0 &&
                      rideCounts.map((ride) => <Field title={morph(ride._id)} value={ride.count} />)}
                  </Card>
                ) : admin.privileges.RIDES.RIDES_LIST.VIEW ? (
                  <CardPlaceHolder />
                ) : null}

                {commonLoaded && admin.privileges.SETUP.VEHICLE_CATEGORYS.VIEW ? (
                  <Card direction={"DOWN"} icon={<FaCity />} showChart={false} title={language.VEHICLE_CATEGORY}>
                    {vehicleCategories.length === 0 && (
                      <p className={"text-sm text-gray-500 my-1"}>{language.NO_DATA_FOUND_IN_GIVEN_TIME}</p>
                    )}
                    {vehicleCategories.length > 0 &&
                      vehicleCategories.map((category) => <Field title={category.name} value={category.count} />)}
                  </Card>
                ) : admin.privileges.SETUP.VEHICLE_CATEGORYS.VIEW ? (
                  <CardPlaceHolder />
                ) : null}
              </div>
            </Wrapper>
          )}
        {admin.userType !== U.CORPORATE &&
          cities &&
          cities.length > 0 &&
          city !== undefined &&
          city !== null &&
          cities.filter((city) => city._id === serviceCity)[0]?.locationName &&
          admin.privileges.RIDES.RIDES_LIST.VIEW && (
            <Wrapper>
              <Header
                loading={serviceLoading}
                reloadClick={fetchService}
                title={morph(cities.filter((city) => city._id === serviceCity)[0].locationName)}
              >
                <Select
                  change={(e) => setServiceCity(e)}
                  values={cities.map((city) => ({
                    label: morph(city.locationName),
                    value: city._id,
                  }))}
                />
                <Select change={(e) => setServiceDays(e)} defaultValue={serviceDays} values={daysLists} />
              </Header>
              <div className="flex -ml-2 flex-wrap">
                <div className="flex -ml-2 flex-wrap">
                  {serviceHubs !== null && admin.privileges.RIDES.RIDES_LIST.VIEW && serviceBookings !== null ? (
                    <Card showChart={false} icon={<AiOutlineCar />} title={language.TOTAL_RIDES}>
                      <div className="h-full flex flex-col justify-center items-center">
                        <h1 className="text-5xl font-bold ">{serviceBookings.totalCount}</h1>
                      </div>
                    </Card>
                  ) : (
                    <CardPlaceHolder />
                  )}
                  {serviceHubs !== null && admin.privileges.RIDES.RIDES_LIST.VIEW && serviceBookings !== null ? (
                    <Card showChart={false} icon={<AiOutlineCar />} title={language.NEW_RIDES}>
                      <div className="h-full flex flex-col justify-center items-center">
                        <h1 className="text-5xl font-bold ">{serviceBookings.newCount}</h1>
                      </div>
                    </Card>
                  ) : (
                    <CardPlaceHolder />
                  )}
                  {serviceHubs !== null && admin.privileges.RIDES.RIDES_LIST.VIEW && serviceBookings !== null ? (
                    <Card showChart={false} icon={<AiOutlineCar />} title={language.SCHEDULED_RIDES}>
                      <div className="h-full flex flex-col justify-center items-center">
                        <h1 className="text-5xl font-bold ">{serviceBookings.scheduleCount}</h1>
                      </div>
                    </Card>
                  ) : (
                    <CardPlaceHolder />
                  )}
                  {serviceHubs !== null && admin.privileges.RIDES.RIDES_LIST.VIEW && serviceBookings !== null ? (
                    <Card showChart={false} icon={<AiOutlineCar />} title={language.ONGOING_RIDES}>
                      <div className="h-full flex flex-col justify-center items-center">
                        <h1 className="text-5xl font-bold ">{serviceBookings.ongoingCount}</h1>
                      </div>
                    </Card>
                  ) : (
                    <CardPlaceHolder />
                  )}
                  {serviceHubs !== null && admin.privileges.RIDES.RIDES_LIST.VIEW && serviceBookings !== null ? (
                    <Card showChart={false} icon={<AiOutlineCar />} title={language.EXPIRED_RIDES}>
                      <div className="h-full flex flex-col justify-center items-center">
                        <h1 className="text-5xl font-bold ">{serviceBookings.expiredCount}</h1>
                      </div>
                    </Card>
                  ) : (
                    <CardPlaceHolder />
                  )}
                  {serviceHubs !== null && admin.privileges.RIDES.RIDES_LIST.VIEW && serviceBookings !== null ? (
                    <Card showChart={false} icon={<AiOutlineCar />} title={language.CANCEL_RIDES}>
                      <div className="h-full flex flex-col justify-center items-center">
                        <h1 className="text-5xl font-bold ">{serviceBookings.cancelCount}</h1>
                      </div>
                    </Card>
                  ) : (
                    <CardPlaceHolder />
                  )}
                  {serviceHubs !== null && admin.privileges.RIDES.RIDES_LIST.VIEW && serviceBookings !== null ? (
                    <Card showChart={false} icon={<AiOutlineCar />} title={language.ENDED_RIDES}>
                      <div className="h-full flex flex-col justify-center items-center">
                        <h1 className="text-5xl font-bold ">{serviceBookings.endedCount}</h1>
                      </div>
                    </Card>
                  ) : (
                    <CardPlaceHolder />
                  )}
                  {serviceHubs !== null && admin.privileges.RIDES.RIDES_LIST.VIEW && serviceBookings !== null ? (
                    <Card showChart={false} icon={<AiOutlineCar />} title={language.USER_DENIED}>
                      <div className="h-full flex flex-col justify-center items-center">
                        <h1 className="text-5xl font-bold ">{serviceBookings.userdenyCount}</h1>
                      </div>
                    </Card>
                  ) : (
                    <CardPlaceHolder />
                  )}
                </div>
              </div>
            </Wrapper>
          )}
        {admin.userType !== U.CORPORATE &&
          city !== undefined &&
          city !== null &&
          // Array.isArray(city) &&
          // city.length > 0 &&
          cities &&
          cities.length > 0 &&
          admin.privileges.ADMIN &&
          admin.privileges.ADMIN.HUBS &&
          admin.privileges.ADMIN.HUBS.VIEW && (
            <Wrapper>
              <Header
                loading={serviceLoading}
                reloadClick={fetchService}
                title={morph(cities.filter((city) => city._id === serviceCity)[0].locationName)}
              >
                <Select
                  change={(e) => setServiceCity(e)}
                  values={cities.map((city) => ({
                    label: morph(city.locationName),
                    value: city._id,
                  }))}
                />
                <Select change={(e) => setServiceDays(e)} defaultValue={serviceDays} values={daysLists} />
              </Header>
              <div className="flex -ml-2 flex-wrap">
                {serviceHubs !== null && admin.privileges.ADMIN.HUBS.VIEW && serviceBookings !== null ? (
                  <Card
                    animate={"service"}
                    graphData={serviceHubs.graphData}
                    direction={"DOWN"}
                    icon={<FiHome />}
                    showChart={serviceDays !== "1" && serviceHubs.graphData.length > 1}
                    title={language.HUBS}
                  >
                    <Field title={language.TOTAL} value={serviceHubs.totalCount} />
                    <Field title={language.ACTIVE} value={serviceHubs.activeCount} />
                    <Field title={language.TODAY_REGISTERED} value={serviceHubs.todayCount} />
                    <Field title={language.INACTIVE} value={serviceHubs.inactiveCount} />
                  </Card>
                ) : admin.privileges.ADMIN.HUBS.VIEW ? (
                  <CardPlaceHolder />
                ) : null}
              </div>
            </Wrapper>
          )}
        {/* RIDES  */}
        {/* <Wrapper>
          <div className="w-full">
            <Header title={"Rides"}>
              <Select
                change={(e) => setRideCity(e)}
                values={cities.map((city) => ({
                  label: city.locationName,
                  value: city._id,
                }))}
              />
              <Select change={(e) => setRideDays(e)} defaultValue={rideDays} values={daysLists} />
            </Header>
            {
              <>
                <div className="mt-3 mr-2 mb-2 bg-white dark:bg-gray-900 border-t-2 hover:border-blue-800 hover:bg-gray-50 cursor-pointer rounded-xl">
                  <div className="w-full">
                    <ResponsiveContainer width="100%" height={350}>
                      <AreaChart margin={{ left: 0, right: 0, top: 10, bottom: 0 }} data={rides}>
                        <defs>
                          <linearGradient id="color" x1="0" y1="0" x2="0" y2="1">
                            <stop offset="5%" stopColor="#542e71" stopOpacity={0.8} />
                            <stop offset="95%" stopColor="#542e71" stopOpacity={0} />
                          </linearGradient>
                          <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                            <stop offset="5%" stopColor="#fb3640" stopOpacity={0.8} />
                            <stop offset="95%" stopColor="#fb3640" stopOpacity={0} />
                          </linearGradient>
                          <linearGradient id="colorCv" x1="0" y1="0" x2="0" y2="1">
                            <stop offset="5%" stopColor="#fdca40" stopOpacity={0.8} />
                            <stop offset="95%" stopColor="#fdca40" stopOpacity={0} />
                          </linearGradient>
                        </defs>
                        <Area type="monotone" dataKey="totalCount" stroke={"#542e71"} strokeWidth={3} fillOpacity={0.5} legendType={"diamond"} fill="url(#color)"></Area>
                        {/* <Area
                          type="monotone"
                          dataKey="new"
                          stroke={"#fb3640"}
                          strokeWidth={3}
                          fillOpacity={0.5}
                          legendType={"diamond"}
                          fill="url(#colorPv)"
                        ></Area>
                        <Area
                          type="monotone"
                          dataKey="two"
                          stroke={"#fdca40"}
                          strokeWidth={3}
                          fillOpacity={0.5}
                          legendType={"diamond"}
                          fill="url(#colorCv)"
                        ></Area> */}
        {/* <Tooltip />
                        <XAxis dataKey={"date"} />
                        <YAxis />
                        <CartesianGrid />
                      </AreaChart>
                    </ResponsiveContainer>
                  </div>
                </div>
                <div className="flex justify-center mt-3">
                  <div className="flex items-center">
                    <div className="h-3 w-5" style={{ backgroundColor: "#542e71" }}></div>
                    <p className={"mx-2 text-sm text-gray-500"}>New Ride</p>
                  </div>
                  <div className="flex items-center ml-2">
                    <div className="h-3 w-5" style={{ backgroundColor: "#fb3640" }}></div>
                    <p className={"mx-2 text-sm text-gray-500"}>Completed Ride</p>
                  </div>
                  <div className="flex items-center ml-2">
                    <div className="h-3 w-5" style={{ backgroundColor: "#fdca40" }}></div>
                    <p className={"mx-2 text-sm text-gray-500"}>Cancelled Ride</p>
                  </div>
                </div>
              </>
            }
          </div>
        </Wrapper> */}{" "}
        {admin.userType === U.CORPORATE && fareData && fareData.dashboardData && (
          <Wrapper>
            <Header
              reloadClick={fetchCorporateFair}
              loading={fareLoading}
              title={
                <div>
                  {language.TOTAL_FARE} :{" "}
                  <span className="text-3xl font-bold">{fareData?.dashboardData?.totalFare}</span>
                </div>
              }
            >
              <span className="mx-2">
                {format(new Date(billingDates[0]), "do MMM, yyyy") +
                  " - " +
                  format(new Date(billingDates[1]), "do MMM, yyyy")}
              </span>
              <ReactDatePicker
                selectsRange={true}
                startDate={billingDates[0]}
                endDate={billingDates[1]}
                customInput={<CustomDateComponent />}
                onChange={(update) => {
                  setDateModified(true);
                  setBillingDates(update);
                }}
                withPortal
              />
            </Header>
          </Wrapper>
        )}
        {admin.userType === U.CORPORATE && (
          <Wrapper>
            <Header
              loading={corporateBookingLoading}
              reloadClick={fetchCorporateBookings}
              title={language.BOOKING_DETAILS}
            >
              <Select
                change={(e) => setServiceCity(e)}
                values={[{ locationName: language.ALL, _id: "" }, ...cities].map((city) => ({
                  label: morph(city.locationName),
                  value: city._id,
                }))}
              />
              <Select
                change={(e) => setCorporateBookingDays(e)}
                defaultValue={corporateBookingDays}
                values={daysLists}
              />
              {admin.userType === U.CORPORATE && (
                <>
                  <div className="w-full flex justify-end">
                    <Link to={NavLinks.ADMIN_CORPORATE_CORPORATE_RIDES + "/" + admin._id}>
                      <Button title={language.VIEW_RECENT_RIDES} />
                    </Link>
                    <Link to={NavLinks.ADMIN_CORPORATE_CORPORATE_BILLINGS + "/" + admin._id}>
                      <Button title={language.VIEW_BILLING_LIST} />
                    </Link>
                  </div>{" "}
                </>
              )}
            </Header>

            <div className="flex -ml-2 flex-wrap">
              {corporateBookings !== null ? (
                <Card showChart={false} icon={<AiOutlineCar />} title={language.TOTAL_RIDES}>
                  <div className="h-full flex flex-col justify-center items-center">
                    <h1 className="text-5xl font-bold ">{corporateBookings.totalCount}</h1>
                  </div>
                </Card>
              ) : (
                <CardPlaceHolder />
              )}
              {corporateBookings !== null ? (
                <Card showChart={false} icon={<AiOutlineCar />} title={language.COMPLETED_RIDES}>
                  <div className="h-full flex flex-col justify-center items-center">
                    <h1 className="text-5xl font-bold ">{corporateBookings.endedCount}</h1>
                  </div>
                </Card>
              ) : (
                <CardPlaceHolder />
              )}
              {corporateBookings !== null ? (
                <Card showChart={false} icon={<AiOutlineCar />} title={language.NEW_RIDES}>
                  <div className="h-full flex flex-col justify-center items-center">
                    <h1 className="text-5xl font-bold ">{corporateBookings.newCount}</h1>
                  </div>
                </Card>
              ) : (
                <CardPlaceHolder />
              )}
              {corporateBookings !== null ? (
                <Card showChart={false} icon={<AiOutlineCar />} title={language.SCHEDULED_RIDES}>
                  <div className="h-full flex flex-col justify-center items-center">
                    <h1 className="text-5xl font-bold ">{corporateBookings.scheduleCount}</h1>
                  </div>
                </Card>
              ) : (
                <CardPlaceHolder />
              )}
              {corporateBookings !== null ? (
                <Card showChart={false} icon={<AiOutlineCar />} title={language.ONGOING_RIDES}>
                  <div className="h-full flex flex-col justify-center items-center">
                    <h1 className="text-5xl font-bold ">{corporateBookings.ongoingCount}</h1>
                  </div>
                </Card>
              ) : (
                <CardPlaceHolder />
              )}
              {corporateBookings !== null ? (
                <Card showChart={false} icon={<AiOutlineCar />} title={language.EXPIRED_RIDES}>
                  <div className="h-full flex flex-col justify-center items-center">
                    <h1 className="text-5xl font-bold ">{corporateBookings.expiredCount}</h1>
                  </div>
                </Card>
              ) : (
                <CardPlaceHolder />
              )}
              {corporateBookings !== null ? (
                <Card showChart={false} icon={<AiOutlineCar />} title={language.CANCEL_RIDES}>
                  <div className="h-full flex flex-col justify-center items-center">
                    <h1 className="text-5xl font-bold ">{corporateBookings.cancelCount}</h1>
                  </div>
                </Card>
              ) : (
                <CardPlaceHolder />
              )}
              {corporateBookings !== null ? (
                <Card showChart={false} icon={<AiOutlineCar />} title={language.USER_DENIED}>
                  <div className="h-full flex flex-col justify-center items-center">
                    <h1 className="text-5xl font-bold ">{corporateBookings.userdenyCount}</h1>
                  </div>
                </Card>
              ) : (
                <CardPlaceHolder />
              )}
            </div>
          </Wrapper>
        )}
        {/* {admin.userType === U.CORPORATE && (
          <Wrapper>
            <Header
              loading={fareLoading}
              reloadClick={fetchCorporateFair}
              title={
                settings.lastCoorperateBilledDate
                  ? "" +
                    format(new Date(billingDates[0]), "do MMM, yyyy") +
                    " - " +
                    format(new Date(billingDates[1]), "do MMM, yyyy") +
                    ""
                  : ""
              }
            >
              <div className="w-full flex justify-end">
                <ReactDatePicker
                  selectsRange={true}
                  startDate={billingDates[0]}
                  endDate={billingDates[1]}
                  customInput={<CustomDateComponent />}
                  onChange={(update) => {
                    setDateModified(true);
                    setBillingDates(update);
                  }}
                  withPortal
                />
                <Link to={NavLinks.ADMIN_CORPORATE_CORPORATE_BILLINGS + "/" + admin._id}>
                  <Button title={language.VIEW_BILLING_LIST} />
                </Link>
              </div>
            </Header>

            <div className="flex -ml-2 flex-wrap">
              {fareData ? (
                <Card showChart={false} icon={<FaMoneyBill />} title={language.TOTAL_FARE}>
                  <div className="h-full flex flex-col justify-center items-center">
                    <h1 className="text-5xl font-bold ">{fareData?.dashboardData?.totalFare}</h1>
                  </div>
                </Card>
              ) : (
                <CardPlaceHolder />
              )} */}
        {/* {fareData ? (
                <Card showChart={false} icon={<FaMoneyBill />} title={language.PROFESSIONAL_EARNINGS}>
                  <div className="h-full flex flex-col justify-center items-center">
                    <h1 className="text-5xl font-bold ">{fareData?.dashboardData?.totalProfessionalEarnings}</h1>
                  </div>
                </Card>
              ) : (
                <CardPlaceHolder />
              )}
              {fareData ? (
                <Card showChart={false} icon={<FaMoneyBill />} title={language.SITE_EARNINGS}>
                  <div className="h-full flex flex-col justify-center items-center">
                    <h1 className="text-5xl font-bold ">{fareData?.dashboardData?.totalSiteEarnings}</h1>
                  </div>
                </Card>
              ) : (
                <CardPlaceHolder />
              )} */}
        {/* {fareData ? (
                <Card showChart={false} icon={<AiOutlineCar />} title={language.TOTAL_BOOKINGS}>
                  <div className="h-full flex flex-col justify-center items-center">
                    <h1 className="text-5xl font-bold ">{fareData?.dashboardData?.totalBookings}</h1>
                  </div>
                </Card>
              ) : (
                <CardPlaceHolder />
              )} */}
        {/* </div>
          </Wrapper>
        )} */}
      </div>
      <div className="w-1/4 p-10 px-5 bg-gray-300 dark:bg-gray-900">
        <div className={"opacity-0 sides bg-gray-100 dark:bg-gray-800 rounded-xl p-5"}>
          <h1 className={"border-b-2 pb-2"}>
            <span className={"text-2xl font-bold text-blue-800"}>{format(time, "LLLL do")}</span>,{" "}
            {format(time, "yyyy")}
          </h1>
          <div className={"flex flex-col justify-center items-center pt-5"}>
            <Clock className={"bg-gray-50 rounded-full border-4 border-blue-800 shadow-xl"} value={time} />
            <h1 className={"mt-3 font-bold"}>{format(time, "hh : mm aa")}</h1>
          </div>
        </div>
        <div
          className={"opacity-0 sides bg-gray-100 dark:bg-gray-800 mt-4 rounded-xl flex justify-center items-center"}
        >
          <DayPicker className={"focus:outline-none"} />
        </div>
      </div>
    </div>
  );
}
