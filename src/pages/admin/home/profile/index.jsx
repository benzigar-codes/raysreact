// DEPENDENCIES
import React from "react";
import gsap from "gsap/gsap-core";
import { FiMail, FiPhone, FiSettings } from "react-icons/fi";
import { useFormik } from "formik";
import * as yup from "yup";
import axios from "axios";

// JSON
import A from "../../../../utils/API.js";

// COMPONENTS
import { FormWrapper } from "../../../../components/common/FormWrapper";
import { Section } from "../../../../components/common/Section";
import { FieldWrapper } from "../../../../components/common/FieldWrapper";
import { TextField } from "../../../../components/common/TextField";
import { CountryCodesPicker } from "../../../../components/common/CountryCodesPicker";
import { FileUpload } from "../../../../components/common/FileUpload";
import { SmallLoader } from "../../../../components/common/SmallLoader";
import { PopUp } from "../../../../components/common/PopUp";
import { DropdownNormal } from "../../../../components/common/DropDownNormal";

// HOOKS
import useLanguage from "../../../../hooks/useLanguage";
import useImage from "../../../../hooks/useImage";
import useAdmin from "../../../../hooks/useAdmin";

export default function Index({ history }) {
  // HOOKS
  const { admin, header, refreshImage, authFailure } = useAdmin();
  const { compressImage, imageUrl, isBase64 } = useImage();
  const { language } = useLanguage();

  // STATES
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);
  const [profile, setProfile] = React.useState({
    firstName: "",
    lastName: "",
    hubsName: "",
    officeName: "",
    email: "",
    phone: {
      code: "",
      number: "",
    },
    gender: "Male",
    avatar: "",
    password: "",
  });
  const [loading, setLoading] = React.useState(true);

  document.title = language.PROFILE + " " + language.SETTINGS;
  // window.scrollTo(0, 0);

  const validationSchema = yup.object().shape({
    firstName:
      admin.userType === "OPERATORS" ||
      admin.userType === "ADMIN" ||
      admin.userType === "DEVELOPER" ||
      admin.userType === "HUBSEMPLOYEE"
        ? yup.string().required(language.REQUIRED)
        : null,
    lastName:
      admin.userType === "OPERATORS" ||
      admin.userType === "ADMIN" ||
      admin.userType === "DEVELOPER" ||
      admin.userType === "HUBSEMPLOYEE"
        ? yup.string().required(language.REQUIRED)
        : null,
    hubsName: admin.userType === "HUBS" ? yup.string().required(language.REQUIRED) : null,
    officeName:
      admin.userType === "RESPONSEOFFICE" || admin.userType === "CORPORATEOFFICE"
        ? yup.string().required(language.REQUIRED)
        : null,
    email: yup.string().required(language.REQUIRED).email(language.INVALID_EMAIL),
    phone: yup.object().shape({
      code: yup.string().required(language.REQUIRED),
      number: yup.string().required(language.REQUIRED),
    }),
    gender:
      admin.userType === "OPERATORS" ||
      admin.userType === "ADMIN" ||
      admin.userType === "DEVELOPER" ||
      admin.userType === "HUBSEMPLOYEE"
        ? yup.string().required(language.REQUIRED)
        : null,
    avatar:
      admin.userType === "OPERATORS" ||
      admin.userType === "ADMIN" ||
      admin.userType === "DEVELOPER" ||
      admin.userType === "HUBSEMPLOYEE"
        ? yup.string().required(language.REQUIRED)
        : null,
  });

  const submit = async (e) => {
    setBtnLoading(true);
    if (e.password !== "" && e.password.length !== 6) {
      setBtnLoading(false);
      return alert(language.PIN_LENGTH_DIALOG);
    }
    if (isBase64(e.avatar)) {
      const compressedImage = await compressImage(e.avatar);
      e.avatar = compressedImage;
    }
    try {
      const formData = new FormData();
      formData.append("email", e.email);
      formData.append("phoneCode", e.phone.code);
      formData.append("phoneNumber", e.phone.number);
      formData.append("password", e.password);
      if (
        admin.userType === "OPERATORS" ||
        admin.userType === "ADMIN" ||
        admin.userType === "DEVELOPER" ||
        admin.userType === "HUBSEMPLOYEE"
      ) {
        formData.append("firstName", e.firstName);
        formData.append("lastName", e.lastName);
        formData.append("gender", e.gender);
        formData.append("avatar", e.avatar);
      }
      if (admin.userType === "HUBS") {
        formData.append("hubsName", e.hubsName);
      }
      if (admin.userType === "RESPONSEOFFICE" || admin.userType === "COORPERATEOFFICE") {
        formData.append("officeName", e.officeName);
      }
      await axios.post(A.HOST + A.ADMIN_PROFILE_UPDATE, formData, header);
      refreshImage();
      setPop({ title: language.UPDATE_SUCCESS });
      // window.location.reload();
    } catch (err) {
      authFailure(err);
      setPop({ title: language.ERROR, type: "error" });
    }
    setBtnLoading(false);
  };

  const formik = useFormik({
    initialValues: profile,
    enableReinitialize: true,
    validationSchema,
    onSubmit: btnLoading === false ? submit : () => {},
  });

  const fetchProfile = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_PROFILE_GET, {}, header);
      setProfile({ ...profile, ...data });
      setLoading(false);
      console.clear();
      gsap.fromTo(".profileSettings", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
    } catch (err) {
      authFailure(err);
      history.push("/admin");
    }
  };

  React.useEffect(() => {
    fetchProfile();
  }, []);

  const GenderLists = [
    {
      id: 1,
      label: language.FEMALE,
      value: "FEMALE",
    },
    {
      id: 2,
      label: language.MALE,
      value: "MALE",
    },
    {
      id: 3,
      label: language.OTHER,
      value: "OTHER",
    },
  ];

  return loading === false ? (
    <FormWrapper
      submit={formik.handleSubmit}
      animate="profileSettings"
      width="5/6"
      submitBtn={true}
      btnLoading={btnLoading}
      title={language.PROFILE + " " + language.SETTINGS}
      icon={<FiSettings />}
    >
      <Section animate="profileSettings">
        {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
        {/* Full Name  */}
        {(admin.userType === "OPERATORS" ||
          admin.userType === "ADMIN" ||
          admin.userType === "DEVELOPER" ||
          admin.userType === "HUBSEMPLOYEE") && (
          <FieldWrapper animate="profileSettings" title={language.FULLNAME}>
            {/* First Name  */}
            <TextField
              value={formik.values.firstName}
              width="2/4"
              type="text"
              margin={3}
              error={formik.errors.firstName}
              placeholder={language.FIRSTNAME}
              change={(e) => formik.setFieldValue("firstName", e.trim(), true)}
            />
            {/* Last Name  */}
            <TextField
              value={formik.values.lastName}
              width="2/4"
              type="text"
              placeholder={language.LASTNAME}
              error={formik.errors.lastName}
              change={(e) => formik.setFieldValue("lastName", e.trim(), true)}
            />
          </FieldWrapper>
        )}
        {admin.userType === "RESPONSEOFFICE" && (
          <FieldWrapper title={language.OFFICE_NAME}>
            <TextField
              value={formik.values.officeName}
              placeholder="Email"
              icon={<FiMail />}
              error={formik.errors.officeName}
              change={(e) => formik.setFieldValue("officeName", e, true)}
            />
          </FieldWrapper>
        )}
        {admin.userType === "COORPERATEOFFICE" && (
          <FieldWrapper title={language.NAME}>
            <TextField
              value={formik.values.officeName}
              placeholder="Name"
              icon={<FiMail />}
              error={formik.errors.officeName}
              change={(e) => formik.setFieldValue("officeName", e, true)}
            />
          </FieldWrapper>
        )}
        {admin.userType === "HUBS" && (
          <FieldWrapper title={language.HUBS_NAME}>
            <TextField
              value={formik.values.hubsName}
              placeholder={language.HUBS_NAME}
              icon={<FiMail />}
              error={formik.errors.hubsName}
              change={(e) => formik.setFieldValue("hubsName", e, true)}
            />
          </FieldWrapper>
        )}
        {/* Email */}
        <FieldWrapper animate="profileSettings" title={language.EMAIL}>
          <TextField
            value={formik.values.email}
            placeholder="Email"
            icon={<FiMail />}
            error={formik.errors.email}
            change={(e) => formik.setFieldValue("email", e, true)}
          />
        </FieldWrapper>
        {/* Phone  */}
        <FieldWrapper animate="profileSettings" title={language.MOBILE}>
          <CountryCodesPicker
            placeholder={language.DIAL_CODE}
            defaultValue={formik.values.phone.code}
            margin={3}
            change={(e) => formik.setFieldValue("phone.code", e, true)}
            width="4/12"
          />
          <TextField
            width="8/12"
            type="number"
            value={formik.values.phone.number}
            placeholder={language.PHONE}
            icon={<FiPhone />}
            error={formik.errors.phone && formik.errors.phone.number}
            change={(e) => formik.setFieldValue("phone.number", e, true)}
            readOnly={true}
          />
        </FieldWrapper>
        {/* Gender  */}
        {(admin.userType === "OPERATORS" ||
          admin.userType === "ADMIN" ||
          admin.userType === "DEVELOPER" ||
          admin.userType === "HUBSEMPLOYEE") && (
          <FieldWrapper animate="profileSettings" title={language.GENDER}>
            <DropdownNormal
              defaultValue={language[formik.values.gender]}
              fields={GenderLists}
              error={formik.errors.gender}
              change={(e) => formik.setFieldValue("gender", e, true)}
            />
          </FieldWrapper>
        )}
        <FieldWrapper animate="profileSettings" title={language.PIN_CHANGE}>
          <TextField
            type="password"
            change={(e) => (e.match(/^[0-9]+$/) != null || e === "") && formik.setFieldValue("password", e)}
            value={formik.values.password}
            // placeholder={"######"}
          />
        </FieldWrapper>
      </Section>
      <Section>
        <FieldWrapper animate="profileSettings">
          {/* Image Upload  */}
          {(admin.userType === "OPERATORS" ||
            admin.userType === "DEVELOPER" ||
            admin.userType === "ADMIN" ||
            admin.userType === "HUBSEMPLOYEE") && (
            <FileUpload
              change={(e) => formik.setFieldValue("avatar", e, true)}
              crop={true}
              ratio={1 / 1}
              defaultValue={imageUrl(profile.avatar)}
              accept={["image/jpeg", "image/jpg", "image/png"]}
            />
          )}
        </FieldWrapper>
      </Section>
    </FormWrapper>
  ) : (
    <SmallLoader></SmallLoader>
  );
}
