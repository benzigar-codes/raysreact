// DEPENDENCIES
import { useFormik } from "formik";
import React from "react";
import * as yup from "yup";
import gsap from "gsap/gsap-core";
// COMPONENTS
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { Section } from "../../../../../components/common/Section";
import { TextField } from "../../../../../components/common/TextField";
// HOOKS
import useAdmin from "../../../../../hooks/useAdmin";
import useLanguage from "../../../../../hooks/useLanguage";

// MAIN EXPORT
export default function Index() {
  // HOOKS
  const { language } = useLanguage();
  const { admin } = useAdmin();
  // STATES
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [done, setDone] = React.useState(false);
  // ANIMATION
  React.useEffect(() => {
    gsap.fromTo(".changePassword", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, []);

  // FORMIK
  const formik = useFormik({
    initialValues: {
      password: "",
      cpassword: "",
      _id: admin.id,
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      password: yup
        .string()
        .min(6, language.COMMON.MIN)
        .max(12, language.COMMON.MAX)
        .required(language.COMMON.REQUIRED),
      cpassword: yup
        .string()
        .min(6, language.COMMON.MIN)
        .max(12, language.COMMON.MAX)
        .oneOf([yup.ref("password"), null], language.COMMON.PWD_SHOULD_SAME)
        .required(language.COMMON.REQUIRED),
      _id: yup.string().required(language.COMMON.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      // const data = await fetchAPIChangePassword(e);
      // if (data.status === 200) {
      //   setBtnLoading(false);
      //   setDone(true);
      //   setTimeout(() => setDone(false), 1000);
      // }
    },
  });

  return (
    <FormWrapper
      animate="changePassword"
      submit={formik.handleSubmit}
      submitBtn={true}
      width="2/4"
      btnLoading={btnLoading}
      done={done}
      title={language.SUBMENU.CHANGE_PWD}
    >
      <Section>
        <FieldWrapper animate="changePassword" title={language.COMMON.NEW_PWD}>
          <TextField
            change={(e) => formik.setFieldValue("password", e, true)}
            value={formik.values.password}
            error={formik.errors.password}
            placeholder="******"
            type="password"
          />
        </FieldWrapper>
        <FieldWrapper animate="changePassword" title={language.COMMON.CONFIRM_PWD}>
          <TextField
            change={(e) => formik.setFieldValue("cpassword", e, true)}
            value={formik.values.cpassword}
            error={formik.errors.cpassword}
            placeholder="******"
            type="password"
          />
        </FieldWrapper>
      </Section>
    </FormWrapper>
  );
}
