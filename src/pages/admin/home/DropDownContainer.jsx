import React from "react";

const DropDownContainer = ({ children, ...rest }) => {
  return (
    <div {...rest} className="relative mx-3 flex items-center">
      {children}
    </div>
  );
};

export default DropDownContainer