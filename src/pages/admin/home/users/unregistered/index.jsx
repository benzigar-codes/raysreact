import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import useAdmin from "../../../../../hooks/useAdmin";
import { format } from "date-fns";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <>
      <Table
        title={language.USERS_FAILED_TO_REGISTER}
        startingHeadings={[
          {
            id: 1,
            title: language.MOBILE,
            key: "phone",
            show: true,
          },
          {
            id: 2,
            title: language.CREATED_DATE,
            key: "created_date",
            show: true,
          },
          {
            id: 3,
            title: language.UPDATED_DATE,
            key: "updated_date",
            show: true,
          },
          {
            id: 4,
            title: language.STATUS,
            key: "unregister_status",
            show: true,
          },
        ]}
        list={A.HOST + A.ADMIN_UNREGISTERED_USERS_LIST}
        assignData={(data) =>
          data.map((unreg) => ({
            _id: unreg._id,
            created_date: format(new Date(unreg.data.createdAt), "do MMM yyyy (p)"),
            updated_date: format(new Date(unreg.data.updatedAt), "do MMM yyyy (p)"),
            phone: morph(unreg.data.phone.code) + " " + morph(unreg.data.phone.number),
            notes: unreg.data.notes,
            unregister_status: unreg.data.status,
          }))
        }
        bread={[{ id: 1, title: language.USERS }]}
        showAdd={false}
        showArchieve={false}
        showEdit={false}
        showFilter={false}
        showBulk={false}
        showUnregisteredDelete={
          admin.privileges.USERS && admin.privileges.USERS.UNREGISTERED_USER.EDIT === true ? true : false
        }
        showSearch={true}
        showAction={admin.privileges.USERS && admin.privileges.USERS.UNREGISTERED_USER.EDIT === true ? true : false}
        unregStatus={A.HOST + A.ADMIN_UNREGISTERED_USERS_STATUS}
        deleteLink={A.HOST + A.ADMIN_UNREGISTERED_USERS_CLOSE}
        showStatus={false}
        showNotes={admin.privileges.USERS && admin.privileges.USERS.UNREGISTERED_USER.EDIT === true ? true : false}
        notesLink={A.HOST + A.ADMIN_UNREGISTERED_USERS_ADD_NOTES}
      />
    </>
  );
}
