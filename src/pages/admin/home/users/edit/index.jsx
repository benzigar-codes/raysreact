import React from "react";
import { useFormik } from "formik";
import gsap from "gsap/gsap-core";
import * as yup from "yup";
import axios from "axios";

import useLanguage from "../../../../../hooks/useLanguage";

import NavLinks from "../../../../../utils/navLinks.json";
import U from "../../../../../utils/utils.js";
import A from "../../../../../utils/API.js";

import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { Section } from "../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../components/common/TextField";
import { CountryCodesPicker } from "../../../../../components/common/CountryCodesPicker";
import { DropdownNormal } from "../../../../../components/common/DropDownNormal";
import { FileUpload } from "../../../../../components/common/FileUpload";
import useAdmin from "../../../../../hooks/useAdmin";
import useImage from "../../../../../hooks/useImage";
import { PopUp } from "../../../../../components/common/PopUp";
import useUtils from "../../../../../hooks/useUtils";
import useSettings from "../../../../../hooks/useSettings";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import { useParams } from "react-router";

export default function Index({ history }) {
  // HOOKS
  const { language } = useLanguage();
  const { settings } = useSettings();
  const { header, authFailure } = useAdmin();
  const { compressImage, imageUrl, isBase64 } = useImage();
  const { parseError } = useUtils();
  const { id } = useParams();
  // STATES
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [popup, setPop] = React.useState(null);
  React.useEffect(() => gsap.fromTo(".profileSettings", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 }), []);
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      phoneCode: "",
      phoneNumber: "",
      avatar: "",
      gender: "",
    },
    validationSchema: yup.object().shape({
      firstName: yup
        .string()
        .min(2, language.MIN + " 2")
        .max(16, language.MAX + " 16")
        .required(language.REQUIRED),
      lastName: yup
        .string()
        .min(2, language.MIN + " 2")
        .max(16, language.MAX + " 16")
        .required(language.REQUIRED),
      email: yup.string().required(language.REQUIRED).email(language.INVALID_EMAIL),
      phoneCode: yup.string().required(language.REQUIRED),
      phoneNumber: yup
        .string()
        .min(6, language.MIN + " 6")
        .max(15, language.MAX + " 15")
        .required(language.REQUIRED),
      avatar: yup.string().required(language.REQUIRED),
      gender: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        let image = isBase64(formik.values.avatar) ? await compressImage(formik.values.avatar) : formik.values.avatar;

        const formData = new FormData();
        formData.append("avatar", image);
        formData.append("status", U.ACTIVE);
        formData.append("email", formik.values.email);
        formData.append("firstName", formik.values.firstName);
        formData.append("gender", formik.values.gender);
        formData.append("lastName", formik.values.lastName);
        formData.append("languageCode", settings.languageCode);
        formData.append("phoneCode", formik.values.phoneCode);
        formData.append("phoneNumber", formik.values.phoneNumber);
        formData.append("phoneNumber", formik.values.phoneNumber);
        formData.append("id", id);
        await axios.post(A.HOST + A.ADMIN_USERS_UPDATE, formData, header);
        history.push(NavLinks.ADMIN_USERS_VIEW);
      } catch (err) {
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
      setBtnLoading(false);
    },
  });

  React.useEffect(() => {
    const fetchUser = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_USERS_READ, { id }, header);
        formik.setFieldValue("firstName", data.data.firstName);
        formik.setFieldValue("lastName", data.data.lastName);
        formik.setFieldValue("email", data.data.email);
        formik.setFieldValue("gender", data.data.gender);
        formik.setFieldValue("phoneCode", data.data.phone.code);
        formik.setFieldValue("phoneNumber", data.data.phone.number);
        formik.setFieldValue("avatar", data.data.avatar);
        formik.setFieldValue("status", data.data.status);
        setLoading(false);
      } catch (err) {}
    };
    fetchUser();
  }, []);

  React.useEffect(() => {
    if (loading === false) gsap.fromTo(".addUser", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, [loading]);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      width={"4/5"}
      bread={[{ id: 1, title: language.USERS, path: NavLinks.ADMIN_USERS_VIEW }]}
      title={language.EDIT + " " + formik.values.firstName}
      animate={"addUser"}
      submit={formik.handleSubmit}
      submitBtn={true}
      btnLoading={btnLoading}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}

      <Section>
        <FieldWrapper animate={"addUser"} title={language.FULLNAME}>
          <TextField
            value={formik.values.firstName}
            width="2/4"
            type="text"
            margin={3}
            error={formik.errors.firstName}
            placeholder={language.FIRSTNAME}
            change={(e) => formik.setFieldValue("firstName", e.trim(), true)}
          />
          <TextField
            value={formik.values.lastName}
            width="2/4"
            type="text"
            placeholder={language.LASTNAME}
            error={formik.errors.lastName}
            change={(e) => formik.setFieldValue("lastName", e.trim(), true)}
          />
        </FieldWrapper>
        <FieldWrapper animate={"addUser"} title={language.EMAIL}>
          <TextField
            error={formik.errors.email}
            change={(e) => formik.setFieldValue("email", e)}
            value={formik.values.email}
            placeholder={language.EMAIL}
          />
        </FieldWrapper>
        <FieldWrapper animate={"addUser"} title={language.MOBILE}>
          <CountryCodesPicker
            placeholder={language.DIAL_CODE}
            error={formik.errors.phoneCode}
            defaultValue={formik.values.phoneCode}
            margin={3}
            change={(e) => formik.setFieldValue("phoneCode", e, true)}
            width="4/12"
          />
          <TextField
            width="8/12"
            type="number"
            value={formik.values.phoneNumber}
            placeholder={language.PHONE}
            error={formik.errors.phoneNumber}
            change={(e) => formik.setFieldValue("phoneNumber", e, true)}
          />
        </FieldWrapper>
        <FieldWrapper animate={"addUser"} title={language.GENDER}>
          <DropdownNormal
            error={formik.errors.gender}
            change={(e) => formik.setFieldValue("gender", e)}
            value={formik.values.gender}
            defaultValue={language[formik.values.gender]}
            fields={[
              {
                id: 1,
                label: language.FEMALE,
                value: "FEMALE",
              },
              {
                id: 2,
                label: language.MALE,
                value: "MALE",
              },
              {
                id: 3,
                label: language.OTHER,
                value: "OTHER",
              },
            ]}
          />
        </FieldWrapper>
      </Section>
      <Section>
        <FieldWrapper animate={"addUser"} title={language.AVATAR}>
          <FileUpload
            defaultValue={imageUrl(formik.values.avatar)}
            crop={true}
            ratio={1 / 1}
            change={(e) => formik.setFieldValue("avatar", e)}
          />
        </FieldWrapper>
      </Section>
    </FormWrapper>
  );
}
