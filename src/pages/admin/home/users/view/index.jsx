import React from "react";
import gsap from "gsap/gsap-core";
import axios from "axios";
import { format } from "date-fns";

import NavLinks from "../../../../../utils/navLinks.json";
import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";

import { FormWrapper } from "../../../../../components/common/FormWrapper";
import WalletRecharge from "../../../../../components/WalletRecharge";
import { Section } from "../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { Border } from "../../../../../components/common/Border";
import { Heading } from "../../../../../components/common/Heading";
import { TextArea } from "../../../../../components/common/TextArea";
import { Button } from "../../../../../components/common/Button";
import { DetailsWrapper } from "../../../../../components/common/DetailsWrapper";
import { Detail } from "../../../../../components/common/Detail";
import { Documents } from "../../../../../components/common/Documents";

import useLanguage from "../../../../../hooks/useLanguage";
import useAdmin from "../../../../../hooks/useAdmin";
import { useParams } from "react-router";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import useImage from "../../../../../hooks/useImage";
import useDebug from "../../../../../hooks/useDebug";
import ImagePreview from "../../../../../components/common/ImagePreview";
import { PopUp } from "../../../../../components/common/PopUp";
import useUtils from "../../../../../hooks/useUtils";
import { Link } from "react-router-dom";
import JSONViewer from "../../../../../components/common/JSONViewer";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin, header, authFailure } = useAdmin();
  const { id } = useParams();
  const [showRechargePage, setShowRechargePage] = React.useState(false);
  const { imageUrl } = useImage();
  const { morph } = useUtils();

  const [note, setNote] = React.useState("");
  const [verifyLoading, setVerifyLoading] = React.useState(null);
  const [loading, setLoading] = React.useState(true);
  const [popup, setPop] = React.useState(null);
  const [user, setUser] = React.useState({});
  const [notesLoading, setNotesLoading] = React.useState(false);
  const [imageModel, setImagModel] = React.useState(false);

  const noteSave = async () => {
    setNotesLoading(true);
    try {
      await axios.post(
        A.HOST + A.ADMIN_UNREGISTERED_USERS_ADD_NOTES,
        {
          professionalId: id,
          notes: note,
        },
        header
      );
    } catch (err) {}
    setNotesLoading(false);
  };

  const fetchUser = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_USERS_VIEW, { id }, header);
      setUser(data.response);
      setNote(data.response?.notes ? data.response?.notes : "");
      setLoading(false);
    } catch (err) {
      authFailure(err);
      history.goBack();
    }
  };

  const updateByPass = async (value) => {
    setLoading(true);
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_USERS_UPDATE_BYPASS,
        {
          id: id,
          isOtpBypass: value,
        },
        header
      );
      fetchUser();
    } catch (err) {
      authFailure(err);
      setLoading(false);
    }
  };

  React.useEffect(() => {
    fetchUser();
  }, []);

  React.useEffect(() => {
    if (loading === false)
      gsap.fromTo(".viewUser", 0.8, { opacity: 0 }, { opacity: 1, stagger: 0.2 });
  }, [loading]);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      animate="viewUser"
      width="3/4"
      bread={[{ id: 1, title: language.USERS, path: NavLinks.ADMIN_USERS_VIEW }]}
      title={morph(user.firstName) + " " + morph(user.lastName)}
    >
      {showRechargePage && (
        <WalletRecharge
          fetchData={fetchUser}
          type={showRechargePage}
          link={A.HOST + A.ADMIN_USERS_RECHARGE_WALLET}
          wallet={user?.wallet?.availableAmount}
          userDetails={{
            userType: "USER",
            id: user._id,
            phone: user.phone?.code + " " + user.phone?.number,
          }}
          setShowRechargePage={setShowRechargePage}
        />
      )}
      <Section width="1/2">
        {popup != null && (
          <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />
        )}
        <ImagePreview close={() => setImagModel(false)} show={imageModel} />
        <Border animate="viewUser">
          <div className="flex justify-between items-center">
            <Heading title={language.PERSONAL_DETAILS} />
            <div className="flex">
              <div className="w-full flex justify-end">
                <Link to={NavLinks.ADMIN_RIDES_USERS + "/" + id}>
                  <Button title={language.VIEW_RECENT_RIDES} />
                </Link>
              </div>
            </div>
          </div>
          {/* <Heading title={language.PERSONAL_DETAILS} /> */}
          <FieldWrapper>
            <DetailsWrapper>
              <Detail title={language.FIRSTNAME} value={morph(user.firstName)} />
              <Detail title={language.LASTNAME} value={morph(user.lastName)} />
              <Detail title={language.EMAIL} value={morph(user.email)} />
              <Detail
                title={language.MOBILE}
                value={morph(user.phone.code) + " " + morph(user.phone.number)}
              />
              <Detail title={language.GENDER} value={language[user.gender]} />
              <Detail title={language.CURRENCY_CODE} value={user.currencyCode} />
              {user.languageCode && user.languageCode !== null && (
                <Detail title={language.LANG_CODE} value={user.languageCode} />
              )}
              {user.review && user.review !== null && (
                <Detail
                  title={language.AVG_RATINGS}
                  value={parseFloat(user.review?.avgRating).toFixed(2)}
                />
              )}
              {admin.userType === "DEVELOPER" && (
                <Detail
                  title={language.OTP_BYPASS}
                  value={
                    <div
                      onClick={() => updateByPass(!user.isOtpBypass)}
                      className="cursor-pointer"
                    >
                      {user.isOtpBypass ? language.TRUE : language.FALSE}
                    </div>
                  }
                />
              )}
              <Detail
                title={language.PRO_IMG}
                value={
                  <img className="w-full" src={imageUrl(user.avatar)} alt={"Driver"} />
                }
              />
            </DetailsWrapper>
          </FieldWrapper>
        </Border>
        {/* <Border animate="viewUser">
          <Heading title={language.NOTES} />
          <FieldWrapper>
            <TextArea change={(e) => setNote(e)} value={note} />
          </FieldWrapper>
          <FieldWrapper>
            <Button onClick={noteSave} loading={notesLoading} title={language.SAVE} />
          </FieldWrapper>
        </Border> */}
      </Section>
      <Section width="1/2">
        <Border animate="viewUser">
          <div className="flex justify-between items-center">
            <Heading title={language.WALLET} />
            <div className="flex">
              <Button
                click={() => setShowRechargePage("CREDIT")}
                title={language.RECHARGE_WALLET}
              />
              <Button
                click={() => setShowRechargePage("DEBIT")}
                title={language.DEBIT_WALLET}
              />
            </div>
          </div>
          <FieldWrapper>
            <DetailsWrapper>
              <Detail
                title={language.BALANCE}
                value={parseFloat(user.wallet?.availableAmount).toFixed(2)}
              />
              <Detail title={language.DUE_AMOUNT} value={user.wallet?.dueAmount} />
              <Detail
                title={language.FREEZED_AMOUNT}
                value={user.wallet?.freezedAmount}
              />
              <Detail
                // title={language.TRANSACTION_HISTORY}
                value={
                  <div className="w-full flex justify-end">
                    <Link to={NavLinks.ADMIN_USERS_TRANSACTION + "/" + id}>
                      <Button title={language.VIEW_TRANSACTION_HISTORY} />
                    </Link>
                  </div>
                }
              />
            </DetailsWrapper>
          </FieldWrapper>
        </Border>
        <JSONViewer data={user} />
      </Section>
    </FormWrapper>
  );
}
