import React from "react";
import { FiMenu } from "react-icons/fi";

const Menu = ({ ...rest }) => {
  return (
    <FiMenu
      {...rest}
      className="mx-5 cursor-pointer hover:text-blue-800 text-2xl" />
  );
};

export default Menu