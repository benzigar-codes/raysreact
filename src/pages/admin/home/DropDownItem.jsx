import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import gsap from "gsap";
import { FiCheck } from "react-icons/fi";

const DropDownItem = ({ title, link, className, checked = false, ...rest }) => {
  return (
    <Link to={link} {...rest} className={"cursor-pointer " + className}>
      <p className={`opacity-50 hover:opacity-100 flex items-center ${checked && "text-blue-800 opacity-100"}`}>
        {title} {checked && <FiCheck className="ml-2 text-blue-800" />}
      </p>
    </Link>
  );
};

export default DropDownItem;
