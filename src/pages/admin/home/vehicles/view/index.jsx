import React from "react";
import gsap from "gsap/gsap-core";
import axios from "axios";
import { format } from "date-fns";

import NavLinks from "../../../../../utils/navLinks.json";
import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";

import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { Section } from "../../../../../components/common/Section";
import JSONViewer from "../../../../../components/common/JSONViewer";
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { Border } from "../../../../../components/common/Border";
import { Heading } from "../../../../../components/common/Heading";
import { TextArea } from "../../../../../components/common/TextArea";
import { Button } from "../../../../../components/common/Button";
import { DetailsWrapper } from "../../../../../components/common/DetailsWrapper";
import { Detail } from "../../../../../components/common/Detail";
import { Documents } from "../../../../../components/common/Documents";

import useLanguage from "../../../../../hooks/useLanguage";
import useAdmin from "../../../../../hooks/useAdmin";
import { useParams } from "react-router";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import useImage from "../../../../../hooks/useImage";
import ImagePreview from "../../../../../components/common/ImagePreview";
import { PopUp } from "../../../../../components/common/PopUp";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { vehicleId, driverID } = useParams();
  const { imageUrl } = useImage();
  const [imageModel, setImagModel] = React.useState(false);

  const [note, setNote] = React.useState("");
  const [verifyLoading, setVerifyLoading] = React.useState(null);
  const [popup, setPop] = React.useState(null);
  const [loading, setLoading] = React.useState(true);
  const [vehicle, setVehicle] = React.useState({});
  const [notesLoading, setNotesLoading] = React.useState(false);
  const [categories, setCategories] = React.useState(null);

  const noteSave = async () => {
    setNotesLoading(true);
    try {
      await axios.post(
        A.HOST + A.ADMIN_VEHICLES_NOTES,
        {
          professionalId: driverID,
          id: vehicleId,
          notes: note,
        },
        header
      );
    } catch (err) {}
    setNotesLoading(false);
  };

  const verify = async (docid, docName, docStatus, docExpiry) => {
    setVerifyLoading(docName);
    try {
      await axios.post(
        A.HOST + A.ADMIN_VEHICLES_DOC_VERIFY,
        {
          professionalId: driverID,
          id: vehicle._id || "",
          status: docStatus === U.VERIFIED ? U.UNVERIFIED : U.VERIFIED,
          documentName: docName,
          expiryDate: docExpiry,
        },
        header
      );
      const vehicleDocuments = vehicle.vehicleDocuments.map((each) => {
        if (each.documentName === docName) {
          return { ...each, status: docStatus === U.VERIFIED ? U.UNVERIFIED : U.VERIFIED };
        } else return each;
      });
      setVehicle({ ...vehicle, vehicleDocuments });
    } catch (err) {
      setPop({ title: language.DOCUMENT_UNVERIFIED_ERROR, type: "error" });
    }
    setVerifyLoading(null);
  };

  React.useEffect(() => {
    const fetchCategories = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_VEHICLE_CATEGORY_LIST_ALL, {}, header);
        setCategories(data);
      } catch (err) {
        authFailure(err);
        alert(err);
        // history.goBack();
      }
    };
    const fetchVehicle = async () => {
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_VEHICLES_READ,
          { id: vehicleId, professionalId: driverID },
          header
        );
        setVehicle(data);
        setNote(data.notes ? data.notes : "");
        setLoading(false);
      } catch (err) {
        authFailure(err);
        alert(err);
        // history.goBack();
      }
    };
    fetchCategories();
    fetchVehicle();
  }, []);

  React.useEffect(() => {
    if (loading === false) gsap.fromTo(".viewVehicle", 0.8, { opacity: 0 }, { opacity: 1, stagger: 0.2 });
  }, [loading]);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      animate="viewVehicle"
      width="3/4"
      bread={[
        { id: 1, title: language.DRIVERS, path: NavLinks.ADMIN_DRIVERS_LIST },
        {
          id: 2,
          title: language.VEHICLES,
          path: NavLinks.ADMIN_DRIVERS_LIST + driverID + NavLinks.ADMIN_VEHICLES_LIST,
        },
      ]}
      title={vehicle.plateNumber}
    >
      <Section>
        {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
        <ImagePreview close={() => setImagModel(false)} show={imageModel} />
        <Border animate="viewVehicle">
          <Heading title={language.VEHICLE_DETAILS} />
          <FieldWrapper>
            <DetailsWrapper>
              <Detail title={language.MAKER} value={vehicle.maker} />
              <Detail title={language.MODEL} value={vehicle.model} />
              <Detail title={language.YEAR} value={vehicle.year} />
              <Detail title={language.COLOR} value={vehicle.color} />
              <Detail title={language.TYPE} value={vehicle.type} />
              <Detail title={language.DOOR_NO} value={vehicle.noOfDoors} />
              <Detail title={language.SEATS_NO} value={vehicle.noOfSeats} />
              <Detail title={language.VIN_NUMBER} value={vehicle.vinNumber} />
              <Detail title={language.PLATE_NUMBER} value={vehicle.plateNumber} />
              {categories && categories.filter((category) => category._id === vehicle.vehicleCategoryId).length > 0 && (
                <Detail
                  title={language.VEHICLE_CATEGORY}
                  value={categories.filter((category) => category._id === vehicle.vehicleCategoryId)[0].vehicleCategory}
                />
              )}
              {categories && vehicle.applySubCategory === true && vehicle.isSubCategoryAvailable === true && (
                <Detail
                  title={language.VEHICLE_CATEGORY}
                  value={vehicle.subCategoryIds.map((each) =>
                    categories.filter((category) => category._id === each).length > 0
                      ? categories.filter((category) => category._id === each)[0].vehicleCategory + ", "
                      : ""
                  )}
                />
              )}
              {vehicle.handicapAvailable !== undefined && (
                <Detail
                  title={language.HANDICAP}
                  value={vehicle.handicapAvailable === true ? language.YES : language.NO}
                />
              )}
              {vehicle.isCompanyVehicle !== undefined && (
                <Detail
                  title={language.COMPANY_VEHICLE}
                  value={vehicle.isCompanyVehicle === true ? language.YES : language.NO}
                />
              )}
              {vehicle.childseatAvailable !== undefined && (
                <Detail
                  title={language.CHILD_SEAT}
                  value={vehicle.childseatAvailable === true ? language.YES : language.NO}
                />
              )}
              <Detail
                title={language.VEHICLE_IMAGE_FRONT}
                value={<img className="w-full" src={imageUrl(vehicle.frontImage)} alt={"Front_Image"} />}
              />
              <Detail
                title={language.VEHICLE_IMAGE_BACK}
                value={<img className="w-full" src={imageUrl(vehicle.backImage)} alt={"Back_Image"} />}
              />
              <Detail
                title={language.VEHICLE_IMAGE_LEFT}
                value={<img className="w-full" src={imageUrl(vehicle.leftImage)} alt={"Left_Image"} />}
              />
              <Detail
                title={language.VEHICLE_IMAGE_RIGHT}
                value={<img className="w-full" src={imageUrl(vehicle.rightImage)} alt={"Right_Image"} />}
              />
            </DetailsWrapper>
          </FieldWrapper>
        </Border>
      </Section>
      <Section>
        <Border animate="viewVehicle">
          <Heading title={language.DOCUMENT_DETAILS} />
          {vehicle.vehicleDocuments.map((doc) => (
            <Documents
              title={doc.documentName}
              showVerifyBtn={false}
              imageClick={(docImage) => setImagModel(docImage)}
              verified={doc.status === U.VERIFIED}
              loading={verifyLoading === doc.documentName}
              verifyClick={() => verify(doc._id, doc.documentName, doc.status, doc.expiryDate)}
              expireDate={doc.expiryDate !== "" ? format(new Date(doc.expiryDate), "do MMM yyyy") : null}
              image={imageUrl(doc.documents[0])}
            />
          ))}
        </Border>
        <Border animate="viewVehicle">
          <Heading title={language.NOTES} />
          <FieldWrapper>
            <TextArea change={(e) => setNote(e)} value={note} />
          </FieldWrapper>
          <FieldWrapper>
            <Button onClick={noteSave} loading={notesLoading} title={language.SAVE} />
          </FieldWrapper>
        </Border>
        <JSONViewer data={vehicle} />
      </Section>
    </FormWrapper>
  );
}
