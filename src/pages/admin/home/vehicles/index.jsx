import React from "react";
import Table from "../../../../components/common/Table";
import useLanguage from "../../../../hooks/useLanguage";

import A from "../../../../utils/API.js";
import U from "../../../../utils/utils.js";
import NavLinks from "../../../../utils/navLinks.json";

import useAdmin from "../../../../hooks/useAdmin";
import { useParams } from "react-router";

export default function Index({ history }) {
  const { driverID } = useParams();
  const { language } = useLanguage();
  const { admin } = useAdmin();

  return (
    <Table
      title={language.VEHICLES}
      startingHeadings={[
        {
          id: 1,
          title: language.PLATE_NUMBER,
          key: "plateNumber",
          show: true,
        },
        {
          id: 2,
          title: language.TYPE,
          key: "type",
          show: true,
        },
        {
          id: 3,
          title: language.MODEL,
          key: "model",
          show: true,
        },
        {
          id: 4,
          title: language.YEAR,
          key: "year",
          show: true,
        },
        // {
        //   id: 5,
        //   title: language.STATUS,
        //   key: "status",
        //   show: true,
        // },
      ]}
      list={A.HOST + A.ADMIN_VEHICLES_LIST}
      assignData={(data) =>
        data.map((driver) => ({
          _id: driver._id,
          plateNumber: driver.plateNumber,
          model: driver.model,
          type: language[driver.type],
          year: driver.year,
          // status: driver.status === U.ACTIVE || driver.status === U.VERIFIED ? 1 : 0,
        }))
      }
      bread={[{ id: 1, title: language.DRIVERS, path: NavLinks.ADMIN_DRIVERS_LIST }]}
      // CLICKS
      addClick={() => history.push(NavLinks.ADMIN_DRIVERS_LIST + driverID + NavLinks.ADMIN_VEHICLES_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_DRIVERS_LIST + driverID + NavLinks.ADMIN_VEHICLES_EDIT + "/" + e)}
      viewClick={(e) => history.push(NavLinks.ADMIN_DRIVERS_LIST + driverID + NavLinks.ADMIN_VEHICLES_VIEW + "/" + e)}
      driverID={driverID}
      // SHOW
      showFilter={true}
      showAdd={false}
      showStatus={false}
      showArchieve={false}
      showBulk={true}
      showEdit={true}
      showSearch={true}
      showView={true}
      statusList={A.HOST + A.ADMIN_VEHICLES_STATUS}
      // showAction={
      //   admin.privileges.DRIVERS.DRIVERS_LIST ? (admin.privileges.DRIVERS.DRIVERS_LIST.EDIT ? true : false) : false
      // }
      add={admin.privileges.DRIVERS.DRIVERS_LIST ? (admin.privileges.DRIVERS.DRIVERS_LIST.ADD ? true : false) : false}
      edit={admin.privileges.DRIVERS.DRIVERS_LIST ? (admin.privileges.DRIVERS.DRIVERS_LIST.EDIT ? true : false) : false}
    />
  );
}
