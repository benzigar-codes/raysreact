import React from "react";
import * as yup from "yup";
import { useFormik } from "formik";
import gsap from "gsap/gsap-core";
import { FiLoader, FiPhone } from "react-icons/fi";
import axios from "axios";
import { parse } from "date-fns";

import NavLinks from "../../../../../utils/navLinks.json";
import CountryCodes from "../../../../../utils/countryCodes.json";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";

import useLanguage from "../../../../../hooks/useLanguage";
import useDebug from "../../../../../hooks/useDebug";

import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { Section } from "../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { TextField } from "../../../../../components/common/TextField";
import Flex from "../../../../../components/common/Flex";
import { CountryCodesPicker } from "../../../../../components/common/CountryCodesPicker";
import { ToggleButton } from "../../../../../components/common/ToggleButton";
import { DatePicker } from "../../../../../components/common/DatePicker";
import { DropdownNormal } from "../../../../../components/common/DropDownNormal";
import { DropDownSearch } from "../../../../../components/common/DropDownSearch";
import { FileUpload } from "../../../../../components/common/FileUpload";
import { TextFormat } from "../../../../../components/common/TextFormat";
import { Heading } from "../../../../../components/common/Heading";
import { Border } from "../../../../../components/common/Border";
import { Line } from "../../../../../components/common/Line";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";
import { PopUp } from "../../../../../components/common/PopUp";
import { useParams } from "react-router-dom";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, token, authFailure } = useAdmin();
  const { hasAlphabet, parseError } = useUtils();
  const { driverID } = useParams();
  const [page, setPage] = React.useState(1);
  const [loading, setLoading] = React.useState(true);
  const [loadingHubs, setLoadingHubs] = React.useState(false);
  const [formik4Values, setFormik4Values] = React.useState({});

  const [vehicleId, setVehicleId] = React.useState(null);

  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);

  // LISTS
  const [docs, setDocs] = React.useState(false);
  const [cities, setCities] = React.useState([]);
  const [hubs, setHubs] = React.useState([]);
  const [vehicleCategory, setVehicleCategory] = React.useState([]);

  const formik3 = useFormik({
    initialValues: {
      vehicleCategory: "",
      defaultVehicle: 0,
      city: "",
      hubs: "",
      vinNumber: "",
      plateNumber: "",
      type: "PETROL",
      maker: "",
      model: "",
      year: "",
      color: "",
      noOfDoors: "",
      noOfSeats: "",
      imageFront: "",
      imageBack: "",
      imageLeft: "",
      imageRight: "",
    },
    validateOnChange: true,
    validationSchema: yup.object().shape({
      vehicleCategory: yup.string().required(language.REQUIRED),
      city: yup.string().required(language.REQUIRED),
      hubs: yup.string().required(language.REQUIRED),
      vinNumber: yup.string().required(language.REQUIRED),
      plateNumber: yup.string().required(language.REQUIRED),
      type: yup.string().required(language.REQUIRED),
      maker: yup.string().required(language.REQUIRED),
      model: yup.string().required(language.REQUIRED),
      year: yup
        .number()
        .min(999, language.NOT_A_VALID_YEAR)
        .max(9999, language.NOT_A_VALID_YEAR)
        .required(language.REQUIRED),
      color: yup.string().required(language.REQUIRED),
      noOfDoors: yup
        .number()
        .min(1, language.MIN + " 1")
        .max(8, language.MAX + " 8")
        .required(language.REQUIRED),
      noOfSeats: yup
        .number()
        .min(1, language.MIN + " 1")
        .max(8, language.MAX + " 8")
        .required(language.REQUIRED),
      imageFront: yup.string().required(language.REQUIRED),
      imageBack: yup.string().required(language.REQUIRED),
      imageLeft: yup.string().required(language.REQUIRED),
      imageRight: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      try {
        const formData = new FormData();
        formData.append("professionalId", driverID);
        formData.append("vehicleCategoryId", formik3.values.vehicleCategory);
        formData.append("vinNumber", formik3.values.vinNumber);
        formData.append("plateNumber", formik3.values.plateNumber);
        formData.append("type", formik3.values.type);
        formData.append("serviceCategory", cities.filter((each) => each.locationName)[0]._id);
        formData.append("maker", formik3.values.maker);
        formData.append("model", formik3.values.model);
        formData.append("year", formik3.values.year);
        formData.append("color", formik3.values.color);
        formData.append("noOfDoors", formik3.values.noOfDoors);
        formData.append("noOfSeats", formik3.values.noOfSeats);
        formData.append("hub", formik3.values.hubs);
        formData.append("defaultVehicle", true);
        formData.append("scheduleDate", new Date());
        formData.append("status", U.UNVERIFIED);

        let front = await fetch(formik3.values.imageFront);
        front = await front.blob();

        let back = await fetch(formik3.values.imageBack);
        back = await back.blob();

        let left = await fetch(formik3.values.imageLeft);
        left = await left.blob();

        let right = await fetch(formik3.values.imageRight);
        right = await right.blob();

        formData.append("frontImage", front);
        formData.append("backImage", back);
        formData.append("leftImage", left);
        formData.append("rightImage", right);

        const { data } = await axios.post(A.HOST + A.ADMIN_DRIVERS_ADD_STEP_3, formData, {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/form-data",
          },
        });
        setVehicleId(data.data._id);
        setPage(2);
      } catch (err) {
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
      setBtnLoading(false);
    },
  });

  const formik4 = useFormik({
    initialValues: formik4Values,
    enableReinitialize: true,
    onSubmit: (e) => {
      const ifLoading = Object.keys(formik4.values).filter(
        (key) => formik4.values[key].uploading === language.UPLOADING
      ).length;
      if (ifLoading) {
        setPop({ title: language.DOCS_UPLOADING, type: "error" });
      } else {
        const mandatory_list = Object.keys(formik4.values).filter(
          (key) => formik4.values[key].mandatory === true
        ).length;
        const uploaded_images = Object.keys(formik4.values).filter(
          (key) => formik4.values[key].doc_id !== null && formik4.values[key].mandatory === true
        ).length;
        if (uploaded_images >= mandatory_list) history.goBack();
        else setPop({ title: language.UPLOAD_MANDATORY, type: "error" });
      }
    },
  });

  const fetchVehicleCategory = async () => {
    try {
      setLoadingHubs(true);
      const { data } = await axios.post(
        A.HOST + A.ADMIN_VEHCILE_CATEGORY_BASED_ON_CITY,
        {
          city:
            cities.filter((each) => each.locationName === formik3.values.city).length > 0
              ? cities.filter((each) => each.locationName === formik3.values.city)[0]._id
              : cities.filter((each) => each._id === formik3.values.city).length > 0
              ? formik3.values.city
              : "",
        },
        header
      );
      setVehicleCategory(data);
      formik3.setFieldValue("vehicleCategory", data[0]._id);
      setLoadingHubs(false);
    } catch (err) {
      alert(err);
    }
  };

  React.useEffect(() => {
    formik3.values.city !== "" && fetchVehicleCategory();
  }, [formik3.values.city]);

  React.useEffect(() => {
    const fetchDocs = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_DRIVERS_DOC_LIST, {}, header);
        setDocs(data.data);
        let forForm4 = {};
        data.data.DRIVERDOCUMENTS.forEach((doc) => {
          forForm4[doc.docsName] = {
            doc_id: null,
            doc_name: doc.docsName,
            expiry: "",
            expiry_mandatory: doc.docsExpiry,
            mandatory: doc.docsMandatory,
            uploading: false,
            image: null,
          };
        });
        setFormik4Values(forForm4);
        setLoading(false);
        gsap.fromTo(".addDriver", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
      } catch (err) {
        alert(err);
        history.goBack();
      }
    };

    const fetchVehicleCategory = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_VEHICLE_CATEGORY_LIST_ALL, {}, header);
        setVehicleCategory(data);
        formik3.setFieldValue("vehicleCategory", data[0]._id);
      } catch (err) {}
    };

    const fetchCities = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_CITY_POLYGON_LIST, { categoryName: "ride" }, header);
        setCities(data);
      } catch (err) {}
    };

    fetchDocs();
    fetchCities();
  }, []);

  React.useEffect(() => {
    if (formik3.values.city !== "") {
      setLoadingHubs(true);
      const fetchHubs = async () => {
        try {
          const { data } = await axios.post(
            A.HOST + A.ADMIN_HUB_READ_FROM_CITY,
            {
              serviceArea: cities.filter((each) => each.locationName === formik3.values.city)[0]._id,
            },
            header
          );
          setHubs(data.data);
          if (data.data.length > 0) formik3.setFieldValue("hubs", data.data[0]._id);
          else formik3.setFieldValue("hubs", "");
        } catch (err) {
          alert(err);
        }
        setLoadingHubs(false);
      };
      fetchHubs();
    }
  }, [formik3.values.city]);

  const vehicleDocUpload = async (docName, image) => {
    formik4.setFieldValue(`${docName}.image`, image);
    formik4.setFieldValue(`${docName}.uploading`, language.UPLOADING);
    try {
      const formData = new FormData();

      const driverDocument = {
        professionalId: driverID,
        documentName: formik4.values[docName].doc_name,
        vehicleId: vehicleId,
        expiryDate: "",
      };

      if (formik4.values[docName].expiry_mandatory) {
        driverDocument.expiryDate = parse(formik4.values[docName].expiry, "MM-dd-yyyy", new Date());
      }

      const img = await fetch(image);
      const imageFinal = await img.blob();

      driverDocument[`${docName}[0]`] = imageFinal;
      for (let key in driverDocument) {
        formData.append(key, driverDocument[key]);
      }
      const { data } = await axios.post(A.HOST + A.ADMIN_DRIVERS_ADD_STEP_4, formData, {
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/form-data",
        },
      });
      formik4.setFieldValue(`${docName}.uploading`, language.UPLOADED);
      formik4.setFieldValue(`${docName}.doc_id`, data.data._id);
    } catch (err) {
      setPop({ title: parseError(err), type: "error" });
      formik4.setFieldValue(`${docName}.uploading`, null);
    }
  };

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      title={language.ADD}
      bread={[
        { id: 1, title: language.DRIVERS, path: NavLinks.ADMIN_DRIVERS_LIST },
        {
          id: 2,
          title: language.VEHICLES,
          path: NavLinks.ADMIN_DRIVERS_LIST + driverID + NavLinks.ADMIN_VEHICLES_LIST,
        },
      ]}
      width="full"
      showNext={page === 1}
      nextClick={page === 1 ? formik3.handleSubmit : null}
      btnLoading={btnLoading}
      prevClick={() => setPage(page - 1)}
      page={page}
      totalPage={2}
      submitBtn={page === 2}
      submit={formik4.handleSubmit}
      animate={"addDriver"}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      {page === 1 && (
        <>
          <Section>
            <FieldWrapper title={language.CITY}>
              <DropdownNormal
                change={(e) => formik3.setFieldValue("city", e)}
                fields={cities.map((each) => ({ id: each._id, label: each.locationName }))}
              />
            </FieldWrapper>
            {loadingHubs === true && (
              <FieldWrapper>
                <p className={"text-sm flex items-center mx-2"}>
                  {language.HUBS_LOADING} <FiLoader className={"mx-3 animate-spin"} />
                </p>
              </FieldWrapper>
            )}
            {loadingHubs === false && formik3.values.city !== "" && hubs.length === 0 && (
              <FieldWrapper>
                <p className={"text-sm flex items-center mx-2 text-red-500"}>{language.NO_HUBS_FOR_CITY}</p>
              </FieldWrapper>
            )}
            {formik3.values.city !== "" && hubs.length > 0 && loadingHubs === false && (
              <>
                <FieldWrapper title={language.HUBS}>
                  <DropdownNormal
                    change={(e) => formik3.setFieldValue("hubs", e)}
                    defaultValue={
                      hubs.filter((each) => each._id === formik3.values.hubs).length > 0
                        ? hubs.filter((each) => each._id === formik3.values.hubs)[0].hubsName
                        : ""
                    }
                    fields={hubs.map((each) => ({ id: each._id, label: each.hubsName, value: each._id }))}
                  />
                </FieldWrapper>
                <FieldWrapper title={language.VEHICLE_CATEGORY}>
                  <DropdownNormal
                    error={formik3.errors.vehicleCategory}
                    defaultValue={
                      vehicleCategory.filter((each) => each._id === formik3.values.vehicleCategory).length > 0
                        ? vehicleCategory.filter((each) => each._id === formik3.values.vehicleCategory)[0].name
                        : ""
                    }
                    change={(e) => formik3.setFieldValue("vehicleCategory", e)}
                    fields={vehicleCategory.map((each) => ({
                      id: each._id,
                      label: each.name,
                      value: each._id,
                    }))}
                  />
                </FieldWrapper>
                {formik3.values.vehicleCategory !== "" && (
                  <>
                    <FieldWrapper title={language.MAKER}>
                      <TextField
                        change={(e) => formik3.setFieldValue("maker", e)}
                        error={formik3.errors.maker}
                        value={formik3.values.maker}
                        placeholder={language.MAKER}
                      />
                    </FieldWrapper>
                    <FieldWrapper title={language.MODEL}>
                      <TextField
                        change={(e) => formik3.setFieldValue("model", e)}
                        error={formik3.errors.model}
                        value={formik3.values.model}
                        placeholder={language.MODEL}
                      />
                    </FieldWrapper>
                    <FieldWrapper title={language.YEAR}>
                      <TextField
                        type={"number"}
                        change={(e) => formik3.setFieldValue("year", e)}
                        error={formik3.errors.year}
                        value={formik3.values.year}
                        placeholder={language.YEAR}
                      />
                    </FieldWrapper>
                  </>
                )}
              </>
            )}
          </Section>
          <Section>
            {formik3.values.city !== "" &&
              formik3.values.vehicleCategory !== "" &&
              hubs.length > 0 &&
              loadingHubs === false && (
                <>
                  <FieldWrapper title={language.VIN_NUMBER}>
                    <TextField
                      change={(e) => formik3.setFieldValue("vinNumber", e)}
                      error={formik3.errors.vinNumber}
                      value={formik3.values.vinNumber}
                      placeholder={language.VIN_NUMBER}
                    />
                  </FieldWrapper>
                  <FieldWrapper title={language.PLATE_NUMBER}>
                    <TextField
                      change={(e) => formik3.setFieldValue("plateNumber", e)}
                      error={formik3.errors.plateNumber}
                      value={formik3.values.plateNumber}
                      placeholder={language.PLATE_NUMBER}
                    />
                  </FieldWrapper>
                  <FieldWrapper title={language.TYPE}>
                    <DropdownNormal
                      defaultValue={language[formik3.values.type]}
                      error={formik3.errors.type}
                      change={(e) => formik3.setFieldValue("type", e)}
                      fields={[
                        { id: 1, label: language.PETROL, value: "PETROL" },
                        { id: 2, label: language.DIESEL, value: "DIESEL" },
                        { id: 3, label: language.ELECTRIC, value: "ELECTRIC" },
                        { id: 4, label: language.CNG, value: "CNG" },
                        { id: 5, label: language.OTHERS, value: "OTHER" },
                      ]}
                    />
                  </FieldWrapper>
                  <FieldWrapper title={language.COLOR}>
                    <TextField
                      change={(e) => formik3.setFieldValue("color", e)}
                      error={formik3.errors.color}
                      value={formik3.values.color}
                      placeholder={language.COLOR}
                    />
                  </FieldWrapper>
                  <Flex>
                    <Section padding={false}>
                      <FieldWrapper title={language.DOOR_NO}>
                        <TextField
                          type={"number"}
                          change={(e) => formik3.setFieldValue("noOfDoors", e)}
                          error={formik3.errors.noOfDoors}
                          value={formik3.values.noOfDoors}
                          placeholder={language.DOOR_NO}
                        />
                      </FieldWrapper>
                    </Section>
                    <Section padding={false}>
                      <FieldWrapper title={language.SEATS_NO}>
                        <TextField
                          type={"number"}
                          change={(e) => formik3.setFieldValue("noOfSeats", e)}
                          error={formik3.errors.noOfSeats}
                          value={formik3.values.noOfSeats}
                          placeholder={language.SEATS_NO}
                        />
                      </FieldWrapper>
                    </Section>
                  </Flex>
                  {/* <FieldWrapper title={language.DEFAULT_VEHICLE}>
                    <ToggleButton
                      change={(e) => formik3.setFieldValue("defaultVehicle", e)}
                      defaultValue={formik3.values.defaultVehicle}
                    />
                  </FieldWrapper> */}
                </>
              )}
          </Section>
          <Section>
            {formik3.values.city !== "" &&
              formik3.values.vehicleCategory !== "" &&
              hubs.length > 0 &&
              loadingHubs === false && (
                <>
                  <FieldWrapper title={language.VEHICLE_IMAGE_FRONT}>
                    <FileUpload change={(e) => formik3.setFieldValue("imageFront", e)} crop={false} />
                  </FieldWrapper>
                  <FieldWrapper title={language.VEHICLE_IMAGE_BACK}>
                    <FileUpload change={(e) => formik3.setFieldValue("imageBack", e)} crop={false} />
                  </FieldWrapper>
                  <FieldWrapper title={language.VEHICLE_IMAGE_LEFT}>
                    <FileUpload change={(e) => formik3.setFieldValue("imageLeft", e)} crop={false} />
                  </FieldWrapper>
                  <FieldWrapper title={language.VEHICLE_IMAGE_RIGHT}>
                    <FileUpload change={(e) => formik3.setFieldValue("imageRight", e)} crop={false} />
                  </FieldWrapper>
                </>
              )}
          </Section>
        </>
      )}
      {page === 2 && (
        <>
          <Section>
            {docs.DRIVERDOCUMENTS.filter((doc) => doc.status === U.ACTIVE)
              .filter((doc, index) => index % 3 === 0)
              .map((doc) => (
                <>
                  <Border>
                    <Heading title={doc.docsMandatory === true ? `${doc.docsName} *` : `${doc.docsName}`} />

                    {doc.docsExpiry && (
                      <FieldWrapper title={language.EXPIRY_DATE}>
                        <TextFormat
                          format={"##-##-####"}
                          value={formik4.values[doc.docsName].expiry}
                          change={(e) => formik4.setFieldValue(`${doc.docsName}.expiry`, e)}
                          placeholder="MM-DD-YYYY"
                          mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
                        />
                      </FieldWrapper>
                    )}
                    {formik4.values[doc.docsName].expiry_mandatory === false && (
                      <FieldWrapper title={language.IMAGE}>
                        <FileUpload change={(e) => vehicleDocUpload(`${doc.docsName}`, e)} crop={false} />
                      </FieldWrapper>
                    )}
                    {formik4.values[doc.docsName].expiry_mandatory === true &&
                      formik4.values[doc.docsName].expiry !== "" &&
                      hasAlphabet(formik4.values[doc.docsName].expiry) === false && (
                        <FieldWrapper title={language.IMAGE}>
                          <FileUpload change={(e) => vehicleDocUpload(`${doc.docsName}`, e)} crop={false} />
                        </FieldWrapper>
                      )}
                    <FieldWrapper>
                      <div className="flex w-full justify-between items-center">
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {doc.docsDetail}
                        </p>
                        <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                          {formik4.values[doc.docsName].uploading === false
                            ? ""
                            : formik4.values[doc.docsName].uploading}
                        </p>
                      </div>
                    </FieldWrapper>
                  </Border>
                </>
              ))}
          </Section>
          <Section>
            {docs.DRIVERDOCUMENTS.filter((doc, index) => index % 3 === 1).map((doc) => (
              <>
                <Border>
                  <Heading title={doc.docsMandatory === true ? `${doc.docsName} *` : `${doc.docsName}`} />

                  {doc.docsExpiry && (
                    <FieldWrapper title={language.EXPIRY_DATE}>
                      <TextFormat
                        format={"##-##-####"}
                        value={formik4.values[doc.docsName].expiry}
                        change={(e) => formik4.setFieldValue(`${doc.docsName}.expiry`, e)}
                        placeholder="MM-DD-YYYY"
                        mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
                      />
                    </FieldWrapper>
                  )}
                  {formik4.values[doc.docsName].expiry_mandatory === false && (
                    <FieldWrapper title={language.IMAGE}>
                      <FileUpload change={(e) => vehicleDocUpload(`${doc.docsName}`, e)} crop={false} />
                    </FieldWrapper>
                  )}
                  {formik4.values[doc.docsName].expiry_mandatory === true &&
                    formik4.values[doc.docsName].expiry !== "" &&
                    hasAlphabet(formik4.values[doc.docsName].expiry) === false && (
                      <FieldWrapper title={language.IMAGE}>
                        <FileUpload change={(e) => vehicleDocUpload(`${doc.docsName}`, e)} crop={false} />
                      </FieldWrapper>
                    )}
                  <FieldWrapper>
                    <div className="flex w-full justify-between items-center">
                      <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                        {doc.docsDetail}
                      </p>
                      <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                        {formik4.values[doc.docsName].uploading}
                      </p>
                    </div>
                  </FieldWrapper>
                </Border>
              </>
            ))}
          </Section>
          <Section>
            {docs.DRIVERDOCUMENTS.filter((doc, index) => index % 3 === 2).map((doc) => (
              <>
                <Border>
                  <Heading title={doc.docsMandatory === true ? `${doc.docsName} *` : `${doc.docsName}`} />

                  {doc.docsExpiry && (
                    <FieldWrapper title={language.EXPIRY_DATE}>
                      <TextFormat
                        format={"##-##-####"}
                        value={formik4.values[doc.docsName].expiry}
                        change={(e) => formik4.setFieldValue(`${doc.docsName}.expiry`, e)}
                        placeholder="MM-DD-YYYY"
                        mask={["M", "M", "D", "D", "Y", "Y", "Y", "Y"]}
                      />
                    </FieldWrapper>
                  )}
                  {formik4.values[doc.docsName].expiry_mandatory === false && (
                    <FieldWrapper title={language.IMAGE}>
                      <FileUpload change={(e) => vehicleDocUpload(`${doc.docsName}`, e)} crop={false} />
                    </FieldWrapper>
                  )}
                  {formik4.values[doc.docsName].expiry_mandatory === true &&
                    formik4.values[doc.docsName].expiry !== "" &&
                    hasAlphabet(formik4.values[doc.docsName].expiry) === false && (
                      <FieldWrapper title={language.IMAGE}>
                        <FileUpload change={(e) => vehicleDocUpload(`${doc.docsName}`, e)} crop={false} />
                      </FieldWrapper>
                    )}
                  <FieldWrapper>
                    <div className="flex w-full justify-between items-center">
                      <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                        {doc.docsDetail}
                      </p>
                      <p className={"text-gray-400 mx-1"} style={{ fontSize: 13 }}>
                        {formik4.values[doc.docsName].uploading}
                      </p>
                    </div>
                  </FieldWrapper>
                </Border>
              </>
            ))}
          </Section>
        </>
      )}
    </FormWrapper>
  );
}
