import React from 'react'

const MainMenuWrapper = ({ children, ...rest }) => {
  const direction = document
    .getElementsByTagName("html")[0]
    .getAttribute("dir");
  return (
    <div
      {...rest}
      className={`mx-1 px-2 flex items-center border-gray-100 dark:border-gray-600 ${
        direction === "ltr" ? "border-r-2" : "border-l-2"
      }`}
    >
      {children}
    </div>
  );
};

export default MainMenuWrapper