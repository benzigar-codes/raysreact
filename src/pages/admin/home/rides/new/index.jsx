import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import { format } from "date-fns";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <Table
      title={language.NEW_INSTANT_RIDES_INITIATED}
      startingHeadings={[
        {
          id: 1,
          title: language.BOOKING_ID,
          key: "booking_id",
          show: true,
        },
        {
          id: 2,
          title: language.BOOKING_TYPE,
          key: "booking_type",
          show: true,
        },
        {
          id: 10,
          title: language.BOOKED_BY,
          key: "bookedBy",
          show: true,
        },
        {
          id: 3,
          title: language.USER_DETAILS,
          key: "oneByOne",
          show: true,
        },
        {
          id: 4,
          title: language.BOOKING_TIME,
          key: "date",
          show: true,
        },
        {
          id: 5,
          title: language.BOOKING_FOR,
          key: "oneByOne2",
          show: true,
        },
        {
          id: 6,
          title: language.ESTIMATION_AMOUNT,
          key: "payableAmount",
          show: false,
        },
      ]}
      list={A.HOST + A.ADMIN_RIDES_NEW_LIST}
      assignData={(data) =>
        data.map((ride) => ({
          _id: ride._id,
          booking_id: ride.bookingId,
          oneByOne: [morph(ride.user.firstName) + " " + morph(ride.user.lastName), morph(ride.user.email)],
          booking_type: language[ride.bookingType],
          bookedBy: language[ride.bookingBy],
          payableAmount: ride.invoice && ride.invoice.payableAmount ? ride.invoice.payableAmount.toFixed(2) : "",
          oneByOne2: [
            morph(ride.bookingFor.name),
            morph(ride.bookingFor.phoneCode) + " " + morph(ride.bookingFor.phoneNumber),
          ],
          date: format(new Date(ride.bookingDate), "do MMM yyyy (p)"),
        }))
      }
      bread={[{ id: 1, title: language.RIDES_LIST }]}
      rideFilterClick={(e) => history.push(e)}
      showArchieve={false}
      showBulk={false}
      showBookingByFiler={true}
      showRideFilter={true}
      showSearch={true}
      showAdd={false}
      viewClick={(e) => history.push(NavLinks.ADMIN_RIDES_VIEW + "/" + e)}
      showStatus={false}
      showEdit={false}
      showView={true}
      showAction={admin.privileges.RIDES.RIDES_LIST ? (admin.privileges.RIDES.RIDES_LIST.VIEW ? true : false) : false}
    />
  );
}
