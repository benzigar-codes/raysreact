import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import { format } from "date-fns";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <Table
      title={language.METERS}
      startingHeadings={[
        {
          id: 1,
          title: language.BOOKING_ID,
          key: "booking_id",
          show: true,
        },
        {
          id: 2,
          title: language.GUEST_DETAILS,
          key: "oneByOne2",
          show: true,
        },
        {
          id: 3,
          title: language.DRIVER_DETAILS,
          key: "oneByOne3",
          show: true,
        },
        {
          id: 4,
          title: language.BOOKING_TIME,
          key: "date",
          show: true,
        },
        {
          id: 5,
          title: language.BOOKING_TYPE,
          key: "booking_type",
          show: true,
        },
        {
          id: 6,
          title: language.STATUS,
          key: "booking_status",
          show: true,
        },
        {
          id: 7,
          title: language.AMOUNT,
          key: "payableAmount",
          show: false,
        },
      ]}
      list={A.HOST + A.ADMIN_RIDES_METER_LIST}
      assignData={(data) =>
        data.map((ride) => ({
          _id: ride._id,
          booking_id: ride.bookingId,
          oneByOne3: ride.professional.firstName
            ? [
                morph(ride.professional.firstName) + " " + morph(ride.professional.lastName),
                morph(ride.professional.email),
              ]
            : [language.UNASSIGNED],
          booking_type: language[ride.bookingType],
          payableAmount: ride.invoice && ride.invoice.payableAmount ? ride.invoice.payableAmount.toFixed(2) : "",
          oneByOne2: [morph(ride.bookingFor.phoneCode) + " " + morph(ride.bookingFor.phoneNumber)],
          booking_status: language[ride.bookingStatus],
          date: format(new Date(ride.bookingDate), "do MMM yyyy (p)"),
        }))
      }
      bread={[{ id: 1, title: language.RIDES_LIST }]}
      // rideFilterClick={(e) => history.push(e)}
      showArchieve={false}
      showBulk={false}
      showGuestRideFilter={true}
      // showRideFilter={true}
      showSearch={true}
      showAdd={false}
      showStatus={false}
      viewClick={(e) => history.push(NavLinks.ADMIN_RIDES_VIEW + "/" + e)}
      showEdit={false}
      showView={true}
      showAction={admin.privileges.RIDES.RIDES_LIST ? (admin.privileges.RIDES.RIDES_LIST.VIEW ? true : false) : false}
    />
  );
}
