import React from "react";
import axios from "axios";

import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { Section } from "../../../../../components/common/Section";
import { TextField } from "../../../../../components/common/TextField";
import { PopUp } from "../../../../../components/common/PopUp";

import NavLinks from "../../../../../utils/navLinks.json";
import A from "../../../../../utils/API.js";

import useAdmin from "../../../../../hooks/useAdmin";
import useLanguage from "../../../../../hooks/useLanguage";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const [id, setID] = React.useState("");
  const { parseError } = useUtils();
  const { header } = useAdmin();
  const [popup, setPop] = React.useState(null);
  const [btnLoading, setBtnLoading] = React.useState(false);

  const submit = async () => {
    setBtnLoading(true);
    if (id === "") setPop({ title: language.PROMPT_BOOKING_ID_EMPTY, type: "error" });
    else
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_RIDES_SEARCH,
          {
            bookingId: id.trim(),
          },
          header
        );
        history.push({
          pathname: NavLinks.ADMIN_RIDES_VIEW + "/" + data._id,
        });
      } catch (err) {
        setPop({ title: parseError(err), type: "error" });
      }
    setBtnLoading(false);
  };

  return (
    <FormWrapper
      title={language.SEARCH_RIDE}
      width={"1/2"}
      bread={[{ id: 1, title: language.RIDES, path: NavLinks.ADMIN_RIDES_NEW }]}
      submitBtn={true}
      submit={submit}
      btnLoading={btnLoading}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}

      <Section>
        <FieldWrapper title={language.BOOKING_ID}>
          <TextField change={(e) => setID(e)} />
        </FieldWrapper>
      </Section>
    </FormWrapper>
  );
}
