import React from "react";
import gsap from "gsap/gsap-core";
import axios from "axios";
import { format } from "date-fns";
import decoder from "decode-google-map-polyline";

import NavLinks from "../../../../../utils/navLinks.json";
import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";

import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { Section } from "../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { Border } from "../../../../../components/common/Border";
import { Heading } from "../../../../../components/common/Heading";
import { DetailsWrapper } from "../../../../../components/common/DetailsWrapper";
import { Detail } from "../../../../../components/common/Detail";

import useLanguage from "../../../../../hooks/useLanguage";
import useSocket from "../../../../../hooks/useSocket";
import useAdmin from "../../../../../hooks/useAdmin";
import useSettings from "../../../../../hooks/useSettings";
import { useParams } from "react-router";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import { FiLoader, FiRefreshCcw } from "react-icons/fi";
import { AiFillStar } from "react-icons/ai";
import TrackingMap from "../../../../../components/TrackingMap";
import useUtils from "../../../../../hooks/useUtils";
import { Loader } from "@googlemaps/js-api-loader";
import JSONViewer from "../../../../../components/common/JSONViewer";
import { Button } from "../../../../../components/common/Button";
import { Link } from "react-router-dom";

const WaypointMap = ({ smallPointers = [], largePointers = [], encodedPolyline = false, startEndColor = false }) => {
  const [googleLoaded, setGoogleLoaded] = React.useState(false);
  const { language } = useLanguage();
  const [map, setMap] = React.useState(null);
  const mapRef = React.useRef(null);
  const { settings } = useSettings();

  const colorMarkers = [
    "http://maps.google.com/mapfiles/ms/icons/green-dot.png",
    "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
    "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png",
    "http://maps.google.com/mapfiles/ms/icons/pink-dot.png",
    "http://maps.google.com/mapfiles/ms/icons/purple-dot.png",
    "http://maps.google.com/mapfiles/ms/icons/green-dot.png",
    "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
    "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png",
    "http://maps.google.com/mapfiles/ms/icons/pink-dot.png",
    "http://maps.google.com/mapfiles/ms/icons/purple-dot.png",
  ];

  const colors = ["#5D8233", "#3DB2FF", "#5E454B", "#7D1935", "#865439", "#297F87", "#DF2E2E"];

  React.useEffect(() => {
    const loader = new Loader({
      apiKey: settings.mapApi.web,
      version: "weekly",
      libraries: ["places"],
    });
    loader
      .load()
      .then(() => {
        setGoogleLoaded(true);
      })
      .catch((e) => {
        // alert(e);
      });
  }, []);

  React.useEffect(() => {
    let redLabel = 1;
    let greenLabel = 1;
    if (map && googleLoaded && encodedPolyline && encodedPolyline.length > 0 && encodedPolyline[0] !== "") {
      // alert(JSON.stringify(decoder(encodedPolyline)));
      encodedPolyline.map((each, idx) => {
        const polylines = new window.google.maps.Polyline({
          path: decoder(each),
          geodesic: true,
          strokeColor: colors[idx % 5],
          strokeOpacity: 1,
          strokeWeight: 5,
        });
        polylines.setMap(map);
        if (polylines) {
          var bounds = new window.google.maps.LatLngBounds();
          decoder(each).forEach((each) => bounds.extend({ lat: each.lat, lng: each.lng }));
          map.fitBounds(bounds);
        }
      });
    }
    if (map) {
      smallPointers.map((each) => {
        const marker = new window.google.maps.Marker({
          map,
          icon: {
            url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png",
          },
          position: { lat: each.lat, lng: each.lng },
        });
        const infowindow = new window.google.maps.InfoWindow({
          content: `
              <div>
              <h1 style="color:black;font-size:15px;No: ">${redLabel++}, Time:  ${format(
            new Date(each.time * 1000),
            "h:mm:s aa"
          )}</h1></div>
              `,
        });
        marker.addListener("click", () => {
          infowindow.open(map, marker);
        });
      });
      setTimeout(() => {
        largePointers.map((inside, idx) => {
          if (Array.isArray(inside))
            inside.map((each, i) => {
              const marker = new window.google.maps.Marker({
                map,
                icon: {
                  url: colorMarkers[idx % 5],
                  scaledSize: new window.google.maps.Size(10, 10),
                },
                position: { lat: each.lat, lng: each.lng },
              });
              const infowindow = new window.google.maps.InfoWindow({
                content: `
                <div>
                <h1 style="color:black;font-size:15px;">No: ${greenLabel++}, Time:  ${format(
                  new Date(each.timeStamp * 1000),
                  "h:mm:s aa"
                )}</h1></div>
                `,
              });
              if (startEndColor && (i === 0 || i === inside.length - 1)) infowindow.open(map, marker);
              marker.addListener("click", () => {
                infowindow.open(map, marker);
              });
            });
          else {
            const marker = new window.google.maps.Marker({
              map,
              icon: {
                url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png",
                scaledSize: new window.google.maps.Size(10, 10),
              },
              position: { lat: inside.lat, lng: inside.lng },
            });
            const infowindow = new window.google.maps.InfoWindow({
              content: `
              <div>
              ${idx === 0 ? `<h1 style="color:black;font-size:18px;">Started Here</h1>` : ``}
              ${idx === largePointers.length - 1 ? `<h1 style="color:black;font-size:18px;">Ended Here</h1>` : ``}
              <h1 style="color:black;font-size:15px;">No: ${greenLabel++}, Time:  ${format(
                new Date(inside.timeStamp * 1000),
                "h:mm:s aa"
              )}</h1></div>
              `,
            });
            if (startEndColor && (idx === 0 || idx === largePointers.length - 1)) infowindow.open(map, marker);
            marker.addListener("click", () => {
              infowindow.open(map, marker);
            });
          }
        });
      }, [2000]);
      if (smallPointers.length > 1 || largePointers.length > 1) {
        var bounds = new window.google.maps.LatLngBounds();
        // [...smallPointers].forEach((each) => bounds.extend({ lat: each.lat, lng: each.lng }));
        largePointers.map((inside) =>
          Array.isArray(inside)
            ? inside.map((each) => bounds.extend({ lat: each.lat, lng: each.lng }))
            : bounds.extend({ lat: inside.lat, lng: inside.lng })
        );
        map.fitBounds(bounds);
      }
    }
  }, [map, googleLoaded]);

  React.useEffect(() => {
    if (googleLoaded) {
      const googleMap = new window.google.maps.Map(mapRef.current, {
        center: { lat: 12.9791551, lng: 80.2007085 },
        zoom: 16,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: true,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: true,
      });
      googleMap.setZoom(16);
      setMap(googleMap);
    }
  }, [googleLoaded]);

  return googleLoaded ? (
    <div
      className="rounded-xl bg-gray-100 dark:bg-gray-900 flex justify-center items-center"
      ref={mapRef}
      style={{ height: 400 }}
    >
      <h1>{language.NO_LOCATION_INFO}</h1>
    </div>
  ) : null;
};

export default function Index({ history }) {
  const { language } = useLanguage();
  const { header, authFailure, admin } = useAdmin();
  const { id } = useParams();
  const { morph } = useUtils();
  // const { notificationSocket } = useSocket();

  const [loading, setLoading] = React.useState(true);
  const [categories, setCategories] = React.useState(null);
  const [ride, setRide] = React.useState({});
  const [endClicked, setEndClicked] = React.useState(false);
  const [statusLoading, setStatusLoading] = React.useState(false);

  const changeStatus = async (link) => {
    setStatusLoading(true);
    try {
      await axios.post(
        A.HOST + link,
        {
          bookingId: id,
        },
        header
      );
      const { data } = await axios.post(A.HOST + A.ADMIN_RIDES_READ, { bookingId: id }, header);
      setRide(data);
    } catch (err) {
      authFailure(err);
      alert(err);
    }
    setStatusLoading(false);
  };

  const endRide = async (type) => {
    setStatusLoading(true);
    try {
      await axios.post(
        A.HOST + A.ADMIN_RIDES_CHANGE_STATUS_ENDED,
        {
          bookingId: id,
          isDistanceCalculate: type,
        },
        header
      );
      const { data } = await axios.post(A.HOST + A.ADMIN_RIDES_READ, { bookingId: id }, header);
      setRide(data);
    } catch (err) {
      authFailure(err);
      alert(err);
    }
    setStatusLoading(false);
  };

  const fetchCategories = async () => {
    setLoading(true);
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_VEHICLE_CATEGORY_LIST_ALL, {}, header);
      setCategories(data);
    } catch (err) {
      authFailure(err);
      history.goBack();
    }
  };

  const fetchDriver = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_RIDES_READ, { bookingId: id }, header);
      setRide(data);
      await fetchCategories();
    } catch (err) {
      authFailure(err);
      history.goBack();
    }
    setLoading(false);
  };

  React.useEffect(() => {
    fetchDriver();
  }, []);

  // React.useEffect(() => {
  //   notificationSocket.current.on(id, (data) => {
  //     fetchDriver();
  //     console.log(data);
  //   });
  // }, []);

  React.useEffect(() => {
    if (loading === false) gsap.fromTo(".viewRide", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, [loading]);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <>
      <FormWrapper
        animate="viewRide"
        width="full"
        bread={[{ id: 1, title: language.RIDES, path: NavLinks.ADMIN_RIDES_NEW }]}
        title={ride.bookingId}
      >
        <Section width="1/2">
          {ride.security && (
            <Border animate="viewRide">
              <div className="flex justify-between items-center">
                <Heading title={language.SECURITY + " : " + language[ride.security.securityServiceName]} />
                <Link to={NavLinks.ADMIN_SECURITY_REQUESTS_VIEW + "/" + ride.security._id + "?type=NEW"}>
                  <Button title={language.VIEW_DETAILS} />
                </Link>
              </div>
              <FieldWrapper>
                <DetailsWrapper>
                  {ride.security.securityThreadId && (
                    <Detail title={language.SECURITY_THREAD_ID} value={ride.security.securityThreadId} />
                  )}
                  {ride.security.ticketId && <Detail title={language.TICKER_ID} value={ride.security.ticketId} />}
                  {ride.security.status && <Detail title={language.STATUS} value={language[ride.security.status]} />}
                  {ride.security.priority && (
                    <Detail title={language.PRIORITY} value={language[ride.security.priority]} />
                  )}
                  {ride.security.userType && (
                    <Detail title={language.INITIATED_BY} value={language[ride.security.userType]} />
                  )}
                  {ride.security.escortStatus && (
                    <Detail title={language.ESCORT_STATUS} value={language[ride.security.escortStatus]} />
                  )}
                </DetailsWrapper>
              </FieldWrapper>
            </Border>
          )}
          {ride.user && (
            <Border animate="viewRide">
              <div className="flex justify-between items-center">
                <Heading title={language.USER_DETAILS} />
              </div>
              <FieldWrapper>
                <DetailsWrapper>
                  <Detail title={language.FIRSTNAME} value={morph(ride.user.firstName)} />
                  <Detail title={language.LASTNAME} value={morph(ride.user.lastName)} />
                  <Detail title={language.EMAIL} value={morph(ride.user.email)} />
                  <Detail title={language.GENDER} value={language[ride.user.gender]} />
                </DetailsWrapper>
              </FieldWrapper>
            </Border>
          )}
          <Border
            style={
              ride.bookingStatus === "USERCANCELLED" || ride.bookingStatus === "PROFESSIONALCANCELLED"
                ? {
                    borderColor: "#E43C45",
                  }
                : {}
            }
            animate="viewRide"
          >
            <div className="flex items-center">
              <Heading
                title={language.RIDE_DETAILS}
                style={
                  ride.bookingStatus === "USERCANCELLED" || ride.bookingStatus === "PROFESSIONALCANCELLED"
                    ? {
                        color: "#E43C45",
                      }
                    : {}
                }
              />
              <FiRefreshCcw className={`cursor-pointer ${loading && "animate-spin"}`} onClick={fetchDriver} />
            </div>
            <FieldWrapper>
              <DetailsWrapper>
                {ride.user && <Detail title={language.NAME} value={morph(ride.bookingFor.name)} />}
                <Detail
                  title={language.PHONE}
                  value={morph(ride.bookingFor.phoneCode) + " " + morph(ride.bookingFor.phoneNumber)}
                />
                <Detail
                  title={language.STATUS}
                  value={language[ride.bookingStatus]}
                  style={
                    ride.bookingStatus === "USERCANCELLED" || ride.bookingStatus === "PROFESSIONALCANCELLED"
                      ? {
                          color: "#E43C45",
                        }
                      : {}
                  }
                />
                {ride.cancelReasonDescription && (
                  <Detail title={language.CANCELLATION_REASON} value={ride.cancelReasonDescription} />
                )}
                {ride.bookingStatus !== U.ENDED &&
                  ride.bookingStatus !== U.EXPIRED &&
                  ride.bookingStatus !== U.USERCANCELLED &&
                  ride.bookingStatus !== U.PROFESSIONALCANCELLED &&
                  !(ride.bookingStatus === U.AWAITING && ride.bookingType === U.INSTANT) && (
                    <Detail
                      title={language.ACTION}
                      value={
                        <div className={"flex items-center"}>
                          {ride.bookingStatus === U.ARRIVED && (
                            <button
                              onClick={() => changeStatus(A.ADMIN_RIDES_CHANGE_STATUS_STARTED)}
                              className={
                                "bg-blue-800 mr-2 text-white rounded-full hover:bg-green-800 px-3 py-1 text-sm"
                              }
                            >
                              {language.START}
                            </button>
                          )}
                          {ride.bookingStatus === U.ACCEPTED && (
                            <button
                              onClick={() => changeStatus(A.ADMIN_RIDES_CHANGE_STATUS_ARRIVED)}
                              className={
                                "bg-blue-800 mr-2 text-white rounded-full hover:bg-green-800 px-3 py-1 text-sm"
                              }
                            >
                              {language.ARRIVE}
                            </button>
                          )}
                          {(ride.bookingStatus === U.ACCEPTED ||
                            (ride.bookingType === U.SCHEDULE && ride.bootingStatus === U.AWAITING)) && (
                            <button
                              onClick={() => changeStatus(A.ADMIN_RIDES_CHANGE_STATUS_CANCEL)}
                              className={"bg-red-500 mr-2 text-white rounded-full hover:bg-green-800 px-3 py-1 text-sm"}
                            >
                              {language.CANCEL}
                            </button>
                          )}
                          {ride.bookingStatus === U.STARTED && (
                            <>
                              {endClicked === false && (
                                <button
                                  onClick={() => setEndClicked(true)}
                                  className={
                                    "truncate bg-blue-800 mr-2 text-white rounded-full hover:bg-green-800 px-3 py-1 text-sm"
                                  }
                                >
                                  {language.END}
                                </button>
                              )}
                              {endClicked === true && (
                                <>
                                  <button
                                    className={
                                      "bg-blue-800 mr-2 text-white rounded-xl hover:bg-green-800 px-3 py-1 text-sm"
                                    }
                                    onClick={() => endRide(true)}
                                  >
                                    {language.END_AT_DRIVERS_LOCATION}
                                  </button>
                                  <button
                                    className={
                                      "bg-blue-800 mr-2 text-white rounded-xl hover:bg-green-800 px-3 py-1 text-sm"
                                    }
                                    onClick={() => endRide(false)}
                                  >
                                    {language.END_AT_USERS_LOCATION}
                                  </button>{" "}
                                </>
                              )}
                            </>
                          )}
                          {statusLoading && (
                            <FiLoader className={"animate-spin text-xl text-white bg-green-800 rounded-full p-1"} />
                          )}
                        </div>
                      }
                    />
                  )}
                {/* {(ride.bookingStatus === "USERCANCELLED" || ride.bookingStatus === "PROFESSIONALCANCELLED") && (
                <Detail title={language.CANCELLATION_REASON} value={ride.cancellationReason} />
              )} */}
                <Detail title={language.CITY} value={morph(ride.category.locationName)} />
                <Detail
                  title={language.PAYMENT}
                  value={ride.payment.option + " | " + (ride.payment.paid ? language.PAID : language.NOT_PAID)}
                />
                <Detail title={language.BOOKED_BY} value={language[ride.bookingBy]} />
                {ride.bookingBy === "ADMIN" && (
                  <Detail title={language.FORCE_ASSIGN} value={ride.isAdminForceAssign ? language.YES : language.NO} />
                )}
              </DetailsWrapper>
            </FieldWrapper>
          </Border>
          {ride.vehicle && ride.vehicle.vehicleCategoryId && (
            <Border animate="viewRide">
              <Heading title={language.VEHICLE_DETAILS} />
              <FieldWrapper>
                <DetailsWrapper>
                  {ride.vehicle.plateNumber && (
                    <Detail title={language.PLATE_NUMBER} value={ride.vehicle.plateNumber} />
                  )}
                  {ride.vehicle.model && <Detail title={language.MODEL} value={ride.vehicle.model} />}
                  {ride.vehicle.vinNumber && <Detail title={language.VIN_NUMBER} value={ride.vehicle.vinNumber} />}
                  {ride.vehicle.type && <Detail title={language.TYPE} value={language[ride.vehicle.type]} />}
                  {ride.vehicle.noOfDoors && <Detail title={language.DOOR_NO} value={ride.vehicle.noOfDoors} />}
                  {ride.vehicle.noOfSeats && <Detail title={language.SEATS_NO} value={ride.vehicle.noOfSeats} />}
                  {categories !== null && ride.vehicle.vehicleCategoryId && (
                    <Detail
                      title={language.CATEGORY}
                      value={
                        categories.filter((category) => category._id === ride.vehicle.vehicleCategoryId).length > 0
                          ? categories.filter((category) => category._id === ride.vehicle.vehicleCategoryId)[0]
                              .vehicleCategory
                          : ""
                      }
                    />
                  )}
                </DetailsWrapper>
              </FieldWrapper>
            </Border>
          )}
          <Border animate="viewRide">
            <Heading title={language.ESTIMATION_BEFOR_END} />
            <FieldWrapper>
              <DetailsWrapper>
                {ride.invoice && (
                  <Detail
                    title={language.AMOUNT}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.estimationAmount).toFixed(2)}
                  />
                )}
                {ride.bookedEstimation && (
                  <Detail title={language.DISTANCE} value={ride.bookedEstimation.distance / 1000 + " KM"} />
                )}
                {ride.bookedEstimation && (
                  <Detail
                    title={language.TIME}
                    value={parseFloat(ride.bookedEstimation.time / 60).toFixed(2) + " " + language.MINUTES}
                  />
                )}
                {ride.origin && ride.origin.fullAddress && (
                  <Detail title={language.PICKUP_LOCATION} value={ride.origin.fullAddress} />
                )}
                {ride.destination && ride.destination.fullAddress && (
                  <Detail title={language.DROP_LOCATION} value={ride.destination.fullAddress} />
                )}
              </DetailsWrapper>
            </FieldWrapper>
          </Border>
          {ride.bookingStatus === "ENDED" && (
            <Border>
              <Heading title={language.FINAL_AFTER_RIDE_ENDS} />
              <FieldWrapper>
                <DetailsWrapper>
                  {ride.invoice && (
                    <Detail
                      title={language.AMOUNT}
                      value={ride.currencySymbol + " " + parseFloat(ride.invoice.payableAmount).toFixed(2)}
                    />
                  )}
                  {ride.estimation ? (
                    <Detail title={language.DISTANCE} value={ride.estimation.distance / 1000 + " KM"} />
                  ) : null}
                  {ride.estimation.time && (
                    <Detail
                      title={language.TIME}
                      value={parseFloat(ride.estimation.time / 60).toFixed(2) + " " + language.MINUTES}
                    />
                  )}
                  {ride.origin && ride.origin.fullAddress && (
                    <Detail title={language.PICKUP_LOCATION} value={ride.origin.fullAddress} />
                  )}
                  {ride.destination && ride.destination.fullAddress && (
                    <Detail title={language.DROP_LOCATION} value={ride.destination.fullAddress} />
                  )}
                </DetailsWrapper>
              </FieldWrapper>
            </Border>
          )}
          {((ride.finalWayData && ride.finalWayData.length > 0) ||
            (ride.encodedPolyline && ride.encodedPolyline.length > 0 && ride.encodedPolyline[0] !== "")) &&
            admin.userType === "DEVELOPER" && (
              <>
                <Heading title={language.FINAL_WAYPOINT} />
                <WaypointMap
                  encodedPolyline={ride.encodedPolyline ? ride.encodedPolyline : false}
                  smallPointers={
                    // ride.wayData && ride.wayData !== null && ride.wayData !== undefined && ride.wayData.length > 0
                    //   ? ride.wayData.map((each) => ({ lat: each.lat, lng: each.lng, time: each.timeStamp }))
                    //   : []
                    []
                  }
                  largePointers={
                    ride.finalWayData &&
                    ride.finalWayData !== null &&
                    ride.finalWayData !== undefined &&
                    ride.finalWayData.length > 0
                      ? ride.finalWayData
                      : []
                  }
                />
              </>
            )}
          {ride.manualWayData && ride.manualWayData.length > 0 && admin.userType === "DEVELOPER" && (
            <>
              <div className="mt-3"></div>
              <Heading title={language.MANUAL_WAYPOINT} />
              <WaypointMap
                startEndColor={true}
                encodedPolyline={false}
                smallPointers={[]}
                largePointers={
                  ride.manualWayData &&
                  ride.manualWayData !== null &&
                  ride.manualWayData !== undefined &&
                  ride.manualWayData.length > 0
                    ? ride.manualWayData
                    : []
                }
              />
            </>
          )}
          <JSONViewer data={ride} />
        </Section>
        <Section width="1/2">
          {ride.bookingStatus !== "ENDED" ? (
            ride.bookingStatus === "STARTED" ||
            ride.bookingStatus === "ARRIVED" ||
            ride.bookingStatus === "ACCEPTED" ? (
              <TrackingMap
                user={false}
                professional={
                  ride.professional
                    ? {
                        _id: ride.professional._id,
                        firstName: ride.professional.firstName,
                        lastName: ride.professional.lastName,
                        phone: {
                          code: ride.professional.phone.code,
                          number: ride.professional.phone.number,
                        },
                      }
                    : false
                }
                responseOfficer={false}
              />
            ) : null
          ) : (
            <img className="mb-4 rounded-xl w-full" src={ride.rideMapRouteImage} alt="" />
          )}
          {ride.coorperate && ride.coorperate.data && (
            <Border animate="viewRide">
              <Heading title={language.CORPORATE_DETAILS} />
              <FieldWrapper>
                <DetailsWrapper>
                  {ride.coorperate.data.officeId && (
                    <Detail title={language.OFFICE_ID} value={morph(ride.coorperate.data.officeId)} />
                  )}
                  {ride.coorperate.data.officeName && (
                    <Detail title={language.NAME} value={morph(ride.coorperate.data.officeName)} />
                  )}
                  {ride.coorperate.data.phone && ride.coorperate.data.phone.number && (
                    <Detail
                      title={language.PHONE}
                      value={morph(ride.coorperate.data.phone.code + " " + ride.coorperate.data.phone.number)}
                    />
                  )}
                  {ride.coorperate.data.email && (
                    <Detail title={language.EMAIL} value={morph(ride.coorperate.data.email)} />
                  )}
                </DetailsWrapper>
              </FieldWrapper>
            </Border>
          )}
          {ride.professional && (
            <Border animate="viewRide">
              <Heading title={language.DRIVER_DETAILS} />
              <FieldWrapper>
                <DetailsWrapper>
                  <Detail title={language.FIRSTNAME} value={morph(ride.professional.firstName)} />
                  <Detail title={language.LASTNAME} value={morph(ride.professional.lastName)} />
                  <Detail
                    title={language.PHONE}
                    value={morph(ride.professional?.phone?.code) + " " + morph(ride.professional?.phone?.number)}
                  />
                  <Detail title={language.EMAIL} value={morph(ride.professional.email)} />
                  <Detail title={language.GENDER} value={language[ride.professional.gender]} />
                </DetailsWrapper>
              </FieldWrapper>
            </Border>
          )}
          <Border animate="viewRide">
            <Heading title={language.BOOKING_DETAILS} />
            <FieldWrapper>
              <DetailsWrapper>
                <Detail title={language.BOOKING_ID} value={ride.bookingId} />
                <Detail title={language.BOOKING_TYPE} value={language[ride.bookingType]} />
                {ride.bookingType === U.INSTANT && (
                  <Detail
                    title={language.BOOKING_TIME}
                    value={format(new Date(ride.activity.bookingTime), "do MMM yyyy (p)")}
                  />
                )}{" "}
                {ride.bookingType === U.SCHEDULE && (
                  <>
                    <Detail title={language.BOOKING_TIME} value={format(new Date(ride.createdAt), "do MMM yyyy (p)")} />
                    <Detail
                      title={language.SCHEDULE_TIME}
                      value={format(new Date(ride.bookingDate), "do MMM yyyy (p)")}
                    />
                  </>
                )}
                {ride.activity.acceptTime !== null && (
                  <Detail
                    title={language.ACCEPTED_AT}
                    value={format(new Date(ride.activity.acceptTime), "do MMM yyyy (p)")}
                  />
                )}
                {ride.activity.arriveTime !== null && (
                  <Detail
                    title={language.ARRIVED_AT}
                    value={format(new Date(ride.activity.arriveTime), "do MMM yyyy (p)")}
                  />
                )}
                {ride.activity.pickUpTime !== null && (
                  <Detail
                    title={language.PICKED_AT}
                    value={format(new Date(ride.activity.pickUpTime), "do MMM yyyy (p)")}
                  />
                )}
                {ride.activity.dropTime !== null && (
                  <Detail
                    title={language.DROPED_AT}
                    value={format(new Date(ride.activity.dropTime), "do MMM yyyy (p)")}
                  />
                )}
                {ride.activity.cancelTime !== null && (
                  <Detail
                    title={language.CANCELLED_AT}
                    value={format(new Date(ride.activity.cancelTime), "do MMM yyyy (p)")}
                  />
                )}
                {ride.activity.rideStops.map((eachRide) => (
                  <Detail
                    title={
                      eachRide.type === "ORIGIN"
                        ? language.START_ADDRESS
                        : eachRide.type === "DESTINATION"
                        ? language.END_ADDRESS
                        : ""
                    }
                    value={morph(eachRide.fullAddress)}
                  />
                ))}
                {ride.bookingStatus === U.ENDED && (
                  <>
                    {ride.bookingReview.userReview.rating !== null && (
                      <Detail
                        title={language.USER_RATING}
                        value={
                          <div className={"flex items-center"}>
                            <AiFillStar className={"mx-2"} /> {ride.bookingReview.userReview.rating}{" "}
                            {ride.bookingReview.userReview.comment}
                          </div>
                        }
                      />
                    )}
                    {ride.bookingReview.professionalReview.rating !== null && (
                      <Detail
                        title={language.PROFESSIONAL_RATING}
                        value={
                          <div className={"flex items-center"}>
                            <AiFillStar className={"mx-2"} /> {ride.bookingReview.professionalReview.rating}{" "}
                            {ride.bookingReview.professionalReview.comment}
                          </div>
                        }
                      />
                    )}
                  </>
                )}
              </DetailsWrapper>
            </FieldWrapper>
          </Border>
          <Border>
            <Heading title={language.CALCULATION_SETTINGS} />
            <FieldWrapper>
              <DetailsWrapper>
                <Detail
                  title={language.MINIMUM_CHARGE}
                  value={ride.invoice.minimumChargeStatus ? language.YES : language.NO}
                />
                <Detail
                  title={language.MINIMUM_CHARGE + " " + language.AMOUNT}
                  value={ride.currencySymbol + " " + parseFloat(ride.invoice.minimumChargeAmount).toFixed(2)}
                />
                <Detail
                  title={language.BASE_FARE}
                  value={ride.currencySymbol + " " + parseFloat(ride.invoice.baseFare).toFixed(2)}
                />
                <Detail
                  title={language.FARE_PER_DISTANCE}
                  value={ride.currencySymbol + " " + parseFloat(ride.invoice.farePerDistance).toFixed(2)}
                />
                <Detail
                  title={language.FARE_PER_MINUTE}
                  value={ride.currencySymbol + " " + parseFloat(ride.invoice.farePerMinute).toFixed(2)}
                />
                <Detail title={language.PEAK_FARE} value={parseFloat(ride.invoice.peakFare).toFixed(2) + " X"} />

                {parseFloat(ride.invoice.peakFare).toFixed(2) > 1 && (
                  <Detail
                    title={language.PEAK_TIME}
                    value={
                      format(new Date(ride?.category?.isPeakFareAvailable?.startTime), "h:mm aa") +
                      " - " +
                      format(new Date(ride?.category?.isPeakFareAvailable?.endTime), "h:mm aa")
                    }
                  />
                )}

                <Detail title={language.NIGHT_FARE} value={parseFloat(ride.invoice.nightFare).toFixed(2) + " X"} />
                {parseFloat(ride.invoice.nightFare).toFixed(2) > 1 && (
                  <Detail
                    title={language.NIGHT_TIME}
                    value={
                      format(new Date(ride?.category?.isNightFareAvailable?.startTime), "h:mm aa") +
                      " - " +
                      format(new Date(ride?.category?.isNightFareAvailable?.endTime), "h:mm aa")
                    }
                  />
                )}
                <Detail
                  title={language.USER_CANCELLATION_STATUS}
                  value={ride.invoice.cancellationStatus ? language.YES : language.NO}
                />
                {ride.invoice.cancellationStatus && (
                  <>
                    <Detail
                      title={language.USER_CANCELLATION_THRESHOLD_MINUTE}
                      value={ride.invoice.cancellationMinute + " " + language.MINUTES}
                    />
                    <Detail
                      title={language.USER_CANCELLATION_AMOUNT}
                      value={ride.currencySymbol + " " + parseFloat(ride.invoice.cancellationAmount).toFixed(2)}
                    />
                  </>
                )}
                <Detail
                  title={language.PROFESSIONAL_CANCELLATION_STATUS}
                  value={ride.invoice.professionalCancellationStatus ? language.YES : language.NO}
                />
                {ride.invoice.professionalCancellationStatus && (
                  <>
                    <Detail
                      title={language.PROFESSIONAL_CANCELLATION_THRESHOLD + " " + language.MINUTES}
                      value={ride.invoice.professionalCancellationMinute + " " + language.MINUTES}
                    />
                    <Detail
                      title={language.PROFESSIONAL_CANCELLATION + " " + language.AMOUNT}
                      value={
                        ride.currencySymbol + " " + parseFloat(ride.invoice.professionalCancellationAmount).toFixed(2)
                      }
                    />
                  </>
                )}
                <Detail
                  title={language.SURCHARGE_FEE}
                  value={ride.currencySymbol + " " + parseFloat(ride.invoice.surchargeFee).toFixed(2)}
                />

                <Detail
                  title={language.SITE_COMMISION_PERCENTAGE}
                  value={parseFloat(ride.invoice.siteCommissionPercentage).toFixed(2) + " %"}
                />

                <Detail
                  title={language.SERVICE_TAX_PERCENTAGE}
                  value={parseFloat(ride.invoice.serviceTaxPercentage).toFixed(2) + " %"}
                />
              </DetailsWrapper>
            </FieldWrapper>
          </Border>
          {ride.invoice && (
            <Border animate="viewRide">
              <Heading title={language.FARE_CALCULATIONS} />
              <FieldWrapper>
                <DetailsWrapper>
                  {ride.invoice.minimumChargeStatus ? (
                    <Detail
                      title={language.MINIMUM_CHARGE}
                      value={ride.currencySymbol + " " + parseFloat(ride.invoice.minimumChargeAmount).toFixed(2)}
                    />
                  ) : (
                    <>
                      <Detail
                        title={language.BASE_FARE}
                        value={ride.currencySymbol + " " + parseFloat(ride.invoice.baseFare).toFixed(2)}
                      />
                      <Detail
                        title={language.FARE_PER_DISTANCE}
                        value={ride.currencySymbol + " " + parseFloat(ride.invoice.farePerDistance).toFixed(2)}
                      />
                      <Detail
                        title={language.FARE_PER_MINUTE}
                        value={ride.currencySymbol + " " + parseFloat(ride.invoice.farePerMinute).toFixed(2)}
                      />
                      <Detail
                        title={language.TRAVEL_CHARGE_WITH_INFO}
                        value={ride.currencySymbol + " " + parseFloat(ride.invoice.travelCharge).toFixed(2)}
                      />
                    </>
                  )}

                  {ride?.invoice?.couponApplied && (
                    <Detail
                      title={language.COUPON_AMOUNT}
                      value={ride.currencySymbol + " " + parseFloat(ride?.invoice?.couponAmount || 0).toFixed(2)}
                    />
                  )}

                  {ride?.totalTollPassed > 0 && (
                    <Detail
                      title={language.TOLL_AMOUNT}
                      value={ride.currencySymbol + " " + parseFloat(ride?.invoice?.tollFareAmount || 0).toFixed(2)}
                    />
                  )}

                  {ride.invoice?.waitingCharge > 0 && (
                    <Detail
                      title={language.WAITING_CHARGE}
                      value={ride.currencySymbol + " " + parseFloat(ride.invoice.waitingCharge).toFixed(2)}
                    />
                  )}

                  <Detail
                    title={language.SERVICE_TAX}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.serviceTax).toFixed(2)}
                  />

                  <Detail
                    title={language.SITE_COMMISION}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.siteCommissionWithoutTax).toFixed(2)}
                  />
                  {ride?.invoice?.professionalTolerenceAmount > 0 && (
                    <Detail
                      title={language.PROFESSIONAL_TOLERANCE + " " + language.AMOUNT}
                      value={
                        ride.currencySymbol +
                        " " +
                        parseFloat(ride?.invoice?.professionalTolerenceAmount || 0).toFixed(2)
                      }
                    />
                  )}
                  {/* <Detail
                    title={language.SHARE_PERCENT}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.sharePercent).toFixed(2)}
                  /> */}

                  {/* <Detail
                    title={language.ESTIMATION_AMOUNT}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.estimationAmount).toFixed(2)}
                  />

                  <Detail
                    title={language.DISTANCE_FARE}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.distanceFare).toFixed(2)}
                  />
                  <Detail
                    title={language.TIME_FARE}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.timeFare).toFixed(2)}
                  />
                  <Detail
                    title={language.TIPS_AMOUNT}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.tipsAmount).toFixed(2)}
                  />
                  <Detail
                    title={language.PAYABLE_AMOUNT}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.payableAmount).toFixed(2)}
                  />
                  <Detail
                    title={language.SITE_COMMISION}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.siteCommission).toFixed(2)}
                  />
                  <Detail
                    title={language.PROFESSIONAL_COMMISION}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.professionalCommision).toFixed(2)}
                  />
                  <Detail
                    title={language.PROFESSIONAL_COMMISION_WITHOUT_TIPS}
                    value={
                      ride.currencySymbol + " " + parseFloat(ride.invoice.professionalCommisionWithoutTips).toFixed(2)
                    }
                  />
                  <Detail
                    title={language.AMOUNT_IN_DRIVER}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.amountInDriver).toFixed(2)}
                  />
                  <Detail
                    title={language.AMOUNT_IN_SITE}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.amountInSite).toFixed(2)}
                  />
                  <Detail
                    title={language.DISTANCE_FARE}
                    value={ride.currencySymbol + " " + parseFloat(ride?.invoice?.distanceFare || 0).toFixed(2)}
                  />
                  <Detail
                    title={language.TIME_FARE}
                    value={ride.currencySymbol + " " + parseFloat(ride?.invoice?.timeFare || 0).toFixed(2)}
                  />
                  <Detail
                    title={language.ESTIMATION_PAYABLE + " " + language.AMOUNT}
                    value={
                      ride.currencySymbol + " " + parseFloat(ride?.invoice?.estimationPayableAmount || 0).toFixed(2)
                    }
                  />
                  <Detail
                    title={language.ACTUAL_ESTIMATION + " " + language.AMOUNT}
                    value={
                      ride.currencySymbol + " " + parseFloat(ride?.invoice?.actualEstimationAmount || 0).toFixed(2)
                    }
                  /> */}
                </DetailsWrapper>
              </FieldWrapper>
            </Border>
          )}
          {ride?.invoice?.couponApplied && (
            <Border>
              <Heading title={language.COUPON_DETAILS} />
              <FieldWrapper>
                <DetailsWrapper>
                  <Detail
                    title={language.COUPON_APPLIED}
                    value={ride?.invoice?.couponApplied ? language.YES : language.NO}
                  />
                  <Detail title={language.COUPON_TYPE} value={ride?.invoice?.couponType} />
                  {ride?.invoice?.couponType === "PERCENTAGE" ? (
                    <Detail
                      title={language.COUPON_VALUE}
                      value={parseFloat(ride?.invoice?.couponValue || 0).toFixed(2) + "%"}
                    />
                  ) : (
                    <Detail
                      title={language.COUPON_VALUE}
                      value={ride.currencySymbol + " " + parseFloat(ride?.invoice?.couponValue || 0).toFixed(2)}
                    />
                  )}
                  <Detail
                    title={language.COUPON_AMOUNT}
                    value={ride.currencySymbol + " " + parseFloat(ride?.invoice?.couponAmount || 0).toFixed(2)}
                  />
                  <Detail
                    title={language.PROFESSIONAL_TOLERENCE}
                    value={ride?.invoice?.isProfessionalTolerance ? language.YES : language.NO}
                  />
                </DetailsWrapper>
              </FieldWrapper>
            </Border>
          )}
          {ride?.invoice?.professionalTolerenceAmount > 0 && (
            <Border>
              <Heading title={language.REIMBURSEMENT} />
              <FieldWrapper>
                <DetailsWrapper>
                  <Detail
                    title={language.PROFESSIONAL + " " + language.AMOUNT}
                    value={
                      ride.currencySymbol + " " + parseFloat(ride?.invoice?.professionalTolerenceAmount || 0).toFixed(2)
                    }
                  />
                </DetailsWrapper>
              </FieldWrapper>
            </Border>
          )}
          {ride?.totalTollPassed > 0 && (
            <Border>
              <Heading title={language.TOLL_DETAILS} />
              <FieldWrapper>
                <DetailsWrapper>
                  {ride?.passedTollList?.map((each) => (
                    <Detail
                      title={each?.toll_name}
                      value={ride.currencySymbol + " " + parseFloat(each?.toll_amount || 0).toFixed(2)}
                    />
                  ))}
                  <Detail
                    title={language.TOTAL_AMOUNT}
                    value={ride.currencySymbol + " " + parseFloat(ride?.invoice?.tollFareAmount || 0).toFixed(2)}
                  />
                </DetailsWrapper>
              </FieldWrapper>
            </Border>
          )}
          {ride.invoice.waitingCharge > 0 ? (
            <Border>
              <Heading title={language.WAITING_CHARGE} />
              <FieldWrapper>
                <DetailsWrapper>
                  <Detail
                    title={language.WAITING_CHARGE_PER_MINUTE}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.waitingChargePerMin).toFixed(2)}
                  />
                  <Detail
                    title={language.WAITING_CHARGE}
                    value={ride.currencySymbol + " " + parseFloat(ride.invoice.waitingCharge).toFixed(2)}
                  />
                </DetailsWrapper>
              </FieldWrapper>
            </Border>
          ) : null}
        </Section>
      </FormWrapper>
    </>
  );
}
