import React from "react";
import { useLocation } from "react-router";
import axios from "axios";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";

import useLanguage from "../../../../../../hooks/useLanguage";

import NavLinks from "../../../../../../utils/navLinks.json";
import A from "../../../../../../utils/API.js";
import useAdmin from "../../../../../../hooks/useAdmin";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { Button } from "../../../../../../components/common/Button";
import { Heading } from "../../../../../../components/common/Heading";
import { FiLoader, FiMail, FiPhone, FiStar, FiUser } from "react-icons/fi";
import { AiOutlineCar } from "react-icons/ai";
import useSettings from "../../../../../../hooks/useSettings";
import { HiOutlineLocationMarker } from "react-icons/hi";
import gsap from "gsap/gsap-core";
import usePrompt from "../../../../../../hooks/usePrompt";
import { Prompt } from "../../../../../../components/common/Prompt";
import { PopUp } from "../../../../../../components/common/PopUp";
import useUtils from "../../../../../../hooks/useUtils";
import { useParams } from "react-router-dom";

export default function Index({ history }) {
  const location = useLocation();
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { prompt, showPrompt } = usePrompt();
  const { parseError } = useUtils();
  const { settings } = useSettings();

  const distanceFromSettings = settings.retryRequestDistance;

  const availableDistance = [0, 500, 1000, 1500, 2000];
  const [selectedDistance, setSelectedDistance] = React.useState(0);

  const [popup, setPop] = React.useState(null);
  const [loading, setLoading] = React.useState(true);
  const [drivers, setDrivers] = React.useState([]);

  document.title = language.ASSIGN_DRIVER;

  const { id } = useParams();

  React.useEffect(() => {
    const fetchDrivers = async () => {
      setLoading(true);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_RIDES_SCHEDULED_ASSIGN_DRIVER_LIST,
          {
            bookingId: id,
            checkRadius: selectedDistance,
          },
          header
        );
        setDrivers(data.response);
        setLoading(false);
        gsap.fromTo(".assignDriver", 0.5, { opacity: 0, y: 5 }, { opacity: 1, y: 0, stagger: 0.2 });
      } catch (err) {
        alert(err);
        authFailure(err);
        history.goBack();
      }
    };
    fetchDrivers();
  }, [selectedDistance]);

  const assignDriver = async (driverId) => {
    setLoading(true);
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_RIDES_SCHEDULED_ASSIGN_DRIVER_REQUEST,
        {
          bookingId: id,
          professionalId: driverId,
        },
        header
      );
      history.push(NavLinks.ADMIN_RIDES_VIEW + "/" + id);
    } catch (err) {
      authFailure(err);
      history.push(NavLinks.ADMIN_RIDES_VIEW + "/" + id);
      setPop({ title: parseError(err), type: "error" });
    }
    setLoading(false);
  };

  React.useEffect(() => gsap.fromTo(".assignDriverPage", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 }), []);

  return (
    <FormWrapper
      width={"3/4"}
      animate={"assignDriverPage"}
      info={language.ASSIGN_DRIVER_INFO}
      bread={[{ id: 1, title: language.RIDES }]}
      title={language.ASSIGN_DRIVER}
    >
      <Section>
        {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
        {prompt.show === true && (
          <Prompt
            title={prompt.title}
            message={prompt.message}
            type={prompt.type}
            close={prompt.close}
            click={prompt.click}
          />
        )}
        <FieldWrapper animate={"assignDriverPage"} title={language.SEARCH_RADIUS}>
          {availableDistance.map((distance) => (
            <Button
              disabled={loading}
              onClick={() => setSelectedDistance(distance)}
              className={distance === selectedDistance && "bg-blue-800 text-white border-blue-800"}
              title={(distanceFromSettings + distance) / 1000 + " KM"}
            />
          ))}
        </FieldWrapper>
        <br />
        {loading === true ? (
          <Heading
            title={
              <div className={"flex items-center"}>
                {language.LOADING_PROFESSIONALS}
                <FiLoader className={"animate-spin mx-2"} />
              </div>
            }
          />
        ) : (
          <>
            <Heading title={language.DRIVERS + " (" + drivers.length + ")"} />
            <div className="flex flex-wrap">
              {drivers.length > 0 &&
                drivers.map((driver) => (
                  <div
                    onClick={() =>
                      showPrompt(language.ASSIGN_DRIVER, language.PROMPT_ASSIGN_DRIVER, "status", () =>
                        assignDriver(driver.professionalId)
                      )
                    }
                    className="w-1/3 opacity-0 assignDriver"
                  >
                    <div className="flex flex-wrap items-center cursor-pointer transition m-3 p-3 border-2 hover:border-green-800 rounded-xl dark:border-gray-500">
                      <div className="w-1/5">
                        <FiUser className={"text-blue-800"} />
                      </div>
                      <div className="w-4/5">{driver.firstName + " " + driver.lastName}</div>
                      <div className="w-1/5 mt-2 truncate" title={driver.firstName + " " + driver.lastName}>
                        <FiMail className={"text-blue-800"} />
                      </div>
                      <div className="w-4/5 mt-2 truncate" title={driver.email}>
                        {driver.email}
                      </div>
                      <div className="w-1/5 mt-2">
                        <FiPhone className={"text-blue-800"} />
                      </div>
                      <div className="w-4/5 mt-2 truncate" title={driver.phone.code + " " + driver.phone.number}>
                        {driver.phone.code + " " + driver.phone.number}
                      </div>
                      <div className="w-1/5 mt-2">
                        <AiOutlineCar className={"text-blue-800"} />
                      </div>
                      <div className="w-4/5 mt-2 truncate" title={driver.vehicleCategoryName}>
                        {driver.vehicleCategoryName}
                      </div>
                      <div className="w-1/5 mt-2">
                        <FiStar className={"text-blue-800"} />
                      </div>
                      <div className="w-4/5 mt-2 truncate" title={driver.avgRating}>
                        {driver.avgRating}
                      </div>
                      <div className="w-1/5 mt-2">
                        <HiOutlineLocationMarker className={"text-blue-800"} />
                      </div>
                      <div className="w-4/5 mt-2 truncate" title={driver.avgRating}>
                        {driver.distance + " KM"}
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </>
        )}
      </Section>
    </FormWrapper>
  );
}
