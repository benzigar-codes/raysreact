import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import { format } from "date-fns";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <Table
      title={language.SCHEDULED_RIDES}
      startingHeadings={[
        {
          id: 1,
          title: language.BOOKING_ID,
          key: "booking_id",
          show: true,
        },
        {
          id: 2,
          title: language.USER_DETAILS,
          key: "oneByOne",
          show: true,
        },
        {
          id: 3,
          title: language.BOOKING_TIME,
          key: "date",
          show: true,
        },
        {
          id: 10,
          title: language.BOOKED_BY,
          key: "bookedBy",
          show: true,
        },
        {
          id: 7,
          title: language.ESTIMATION_AMOUNT,
          key: "payableAmount",
          show: false,
        },
        {
          id: 4,
          title: language.BOOKING_FOR,
          key: "oneByOne2",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_RIDES_SCHEDULED_LIST}
      assignData={(data) =>
        data.map((ride) => ({
          _id: ride._id,
          booking_id: ride.bookingId,
          oneByOne: [morph(ride.user.firstName) + " " + morph(ride.user.lastName), morph(ride.user.email)],
          bookedBy: language[ride.bookingBy],
          date: format(new Date(ride.bookingDate), "do MMM yyyy (p)"),
          payableAmount: ride.invoice.payableAmount ? ride.invoice.payableAmount.toFixed(2) : "",
          isAssignAllowed: true,
          oneByOne2: [
            morph(ride.bookingFor.name),
            morph(ride.bookingFor.phoneCode) + " " + morph(ride.bookingFor.phoneNumber),
          ],
        }))
      }
      bread={[{ id: 1, title: language.RIDES_LIST }]}
      showArchieve={false}
      rideFilterClick={(e) => history.push(e)}
      showBulk={false}
      showRideFilter={true}
      assignDriverClick={(e) => history.push({ pathname: NavLinks.ADMIN_RIDES_SCHEDULED_ASSIGN_DRIVER + "/" + e })}
      showSearch={true}
      showBookingByFiler={true}
      viewClick={(e) => history.push(NavLinks.ADMIN_RIDES_VIEW + "/" + e)}
      showAdd={false}
      showStatus={false}
      showAssignDriver={true}
      showEdit={false}
      showView={true}
      showAction={admin.privileges.RIDES.RIDES_LIST ? (admin.privileges.RIDES.RIDES_LIST.VIEW ? true : false) : false}
    />
  );
}
