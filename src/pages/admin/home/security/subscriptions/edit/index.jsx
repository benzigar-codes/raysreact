import React from "react";
import axios from "axios";
import * as yup from "yup";
import { useFormik } from "formik";

import A from "../../../../../../utils/API.js";
import U from "../../../../../../utils/utils.js";
import NavLinks from "../../../../../../utils/navLinks.json";
import currency from "../../../../../../utils/currency.json";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { TextField } from "../../../../../../components/common/TextField";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { ToggleButton } from "../../../../../../components/common/ToggleButton";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import { MultiSelect } from "../../../../../../components/common/MultiSelect";
import { TextArea } from "../../../../../../components/common/TextArea";
import Flex from "../../../../../../components/common/Flex";
import { DropDownSearch } from "../../../../../../components/common/DropDownSearch";

import useLanguage from "../../../../../../hooks/useLanguage";
import useAdmin from "../../../../../../hooks/useAdmin";
import { PopUp } from "../../../../../../components/common/PopUp";
import useUtils from "../../../../../../hooks/useUtils";
import gsap from "gsap/gsap-core";
import { useParams } from "react-router";

export default function Index({ history }) {
  const { language } = useLanguage();
  const [loading, setLoading] = React.useState(true);
  const [services, setServices] = React.useState([]);
  const { header, authFailure } = useAdmin();
  const { parseError } = useUtils();
  const { id } = useParams();
  const [popup, setPop] = React.useState(null);
  const [selectedServices, setSelectedServices] = React.useState([]);
  const [btnLoading, setBtnLoading] = React.useState(false);

  const formik = useFormik({
    initialValues: {
      name: "",
      hint: "",
      description: "",
      price: 0,
      expireDays: 0,
      expireInText: "",
      currencyCode: "INR",
      currencySymbol: "",
      markAsPopular: 0,
      status: U.ACTIVE,
    },
    validateOnChange: false,
    validationSchema: yup.object().shape({
      name: yup.string().required(language.REQUIRED),
      hint: yup.string().required(language.REQUIRED),
      description: yup.string().required(language.REQUIRED),
      price: yup.number().required(language.REQUIRED),
      expireDays: yup.number().required(language.REQUIRED),
      expireInText: yup.string().required(language.REQUIRED),
      currencyCode: yup.string().required(language.REQUIRED),
      currencySymbol: yup.string().required(language.REQUIRED),
    }),
    onSubmit: async (e) => {
      setBtnLoading(true);
      if (selectedServices.length === 0)
        return setPop({
          title: language.ERROR_SERVICE_EMPTY,
          type: "error",
        });
      const selectedKeys = selectedServices.map(
        (selected) => Object.keys(language).filter((key) => language[key] === selected)[0]
      );
      const finalServicesArray = services
        .filter((service) => selectedKeys.includes(service.name))
        .map((service) => ({ ...service, isCommonService: true }));
      try {
        await axios.post(
          A.HOST + A.ADMIN_SECURITY_SUBSCRIPTIONS_UPDATE,
          {
            id,
            name: formik.values.name,
            hint: formik.values.hint,
            description: formik.values.description,
            price: formik.values.price,
            expireDays: formik.values.expireDays,
            expireInText: formik.values.expireInText,
            currencyCode: formik.values.currencyCode,
            currencySymbol: formik.values.currencySymbol,
            markAsPopular: formik.values.markAsPopular === 1 ? true : false,
            services: finalServicesArray,
          },
          header
        );
        history.push(NavLinks.ADMIN_SECURITY_SUBSCRIPTIONS);
      } catch (err) {
        authFailure(err);
        setPop({ title: parseError(err), type: "error" });
      }
      setBtnLoading(false);
    },
  });

  React.useEffect(() => {
    formik.setFieldValue("currencySymbol", currency[formik.values.currencyCode]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formik.values.currencyCode]);

  React.useEffect(() => {
    const fetchAll = async () => {
      const fetchSubscription = async () => {
        try {
          const { data } = await axios.post(
            A.HOST + A.ADMIN_SECURITY_SUBSCRIPTIONS_READ,
            {
              id,
            },
            header
          );
          formik.setFieldValue("name", data.name);
          formik.setFieldValue("hint", data.hint);
          formik.setFieldValue("description", data.description);
          formik.setFieldValue("expireInText", data.expireInText);
          formik.setFieldValue("currencyCode", data.currencyCode);
          formik.setFieldValue("price", data.price);
          formik.setFieldValue("expireDays", data.expireDays);
          formik.setFieldValue("markAsPopular", data.markAsPopular === true ? 1 : 0);
          setSelectedServices(data.services.map((service) => language[service.name]));
          setLoading(false);
        } catch (err) {}
      };

      const fetchServices = async () => {
        try {
          const { data } = await axios.post(A.HOST + A.ADMIN_SECURITY_SERVICES_LIST, {}, header);
          setServices([...data?.data?.emergency, ...data?.data?.tracking, ...data?.data?.sos]);
        } catch (err) {
          alert(err);
        }
      };

      await fetchServices();
      fetchSubscription();
    };

    fetchAll();
  }, []);

  React.useEffect(() => {
    if (loading === false) {
      gsap.fromTo(".addSubscriptions", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
    }
  }, [loading]);

  return loading === true ? (
    <SmallLoader />
  ) : (
    <form onSubmit={formik.handleSubmit}>
      <FormWrapper
        submitBtn={true}
        animate={"addSubscriptions"}
        btnLoading={btnLoading}
        width={"3/4"}
        title={language.EDIT}
        bread={[
          {
            id: 1,
            title: language.SUBSCRIPTIONS,
            path: NavLinks.ADMIN_SECURITY_SUBSCRIPTIONS,
          },
        ]}
      >
        {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
        <Section>
          <FieldWrapper animate="addSubscriptions" title={language.NAME}>
            <TextField
              error={formik.errors.name}
              change={(e) => formik.setFieldValue("name", e)}
              value={formik.values.name}
            />
          </FieldWrapper>
          <FieldWrapper animate="addSubscriptions" title={language.HINT}>
            <TextField
              error={formik.errors.hint}
              change={(e) => formik.setFieldValue("hint", e)}
              value={formik.values.hint}
            />
          </FieldWrapper>
          <FieldWrapper animate="addSubscriptions" title={language.DESCRIPTION}>
            <TextArea
              error={formik.errors.description}
              change={(e) => formik.setFieldValue("description", e)}
              value={formik.values.description}
            />
          </FieldWrapper>
          <Flex>
            <Section padding={false}>
              <FieldWrapper animate="addSubscriptions" title={language.PRICE}>
                <TextField
                  error={formik.errors.price}
                  change={(e) => formik.setFieldValue("price", e)}
                  value={formik.values.price}
                  type={"number"}
                />
              </FieldWrapper>
            </Section>
            <Section padding={false}>
              <FieldWrapper animate="addSubscriptions" title={language.EXPIRE_DAYS}>
                <TextField
                  error={formik.errors.expireDays}
                  change={(e) => formik.setFieldValue("expireDays", e)}
                  value={formik.values.expireDays}
                  type={"number"}
                />
              </FieldWrapper>
            </Section>
          </Flex>
          <FieldWrapper animate="addSubscriptions" title={language.EXPIRE_IN_TEXT}>
            <TextField
              error={formik.errors.expireInText}
              change={(e) => formik.setFieldValue("expireInText", e)}
              value={formik.values.expireInText}
            />
          </FieldWrapper>
        </Section>
        <Section>
          <FieldWrapper animate="addSubscriptions" title={language.MARK_AS_POPULAR}>
            <ToggleButton
              change={(e) => formik.setFieldValue("markAsPopular", e)}
              defaultValue={formik.values.markAsPopular}
            />
          </FieldWrapper>
          <Flex align={false}>
            <Section padding={false}>
              <FieldWrapper animate="addSubscriptions" title={language.CURRENCY_CODE}>
                <DropDownSearch
                  error={formik.errors.currencyCode}
                  change={(e) => {
                    formik.setFieldValue("currencyCode", e);
                  }}
                  defaultValue={formik.values.currencyCode}
                  fields={Object.keys(currency).map((each, i) => ({
                    id: i,
                    label: each,
                    value: each,
                  }))}
                />
              </FieldWrapper>
            </Section>
            <Section padding={false}>
              <FieldWrapper animate="addSubscriptions" title={language.CURRENCY_SYMBOL}>
                <TextField error={formik.errors.currencySymbol} value={formik.values.currencySymbol} readOnly={true} />
              </FieldWrapper>
            </Section>
          </Flex>
          <FieldWrapper animate="addSubscriptions" title={language.SERVICES}>
            <MultiSelect
              change={(e) => setSelectedServices(e)}
              // error={formik.errors.pageViewLimits}
              allFields={services.filter((service) => service.status === true).map((service) => language[service.code])}
              defaultValue={selectedServices}
            ></MultiSelect>
          </FieldWrapper>
        </Section>
      </FormWrapper>
    </form>
  );
}
