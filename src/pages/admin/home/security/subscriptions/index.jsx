import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  return (
    <Table
      title={language.SUBSCRIPTIONS_SETUP}
      startingHeadings={[
        {
          id: 1,
          title: language.NAME,
          key: "name",
          show: true,
        },
        {
          id: 2,
          title: language.EXPIRY_DATE,
          key: "expiryDate",
          show: true,
        },
        {
          id: 3,
          title: language.PRICE,
          key: "price",
          show: true,
        },
        {
          id: 4,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_SECURITY_SUBSCRIPTIONS_LIST}
      assignData={(data) =>
        data.map((role) => ({
          _id: role._id,
          name: role.name,
          expiryDate: role.expireInText,
          price: `${role.currencySymbol} ${role.price}`,
          status: role.status === U.ACTIVE ? 1 : 0,
        }))
      }
      bread={[]}
      showAdd={
        admin.privileges.SECURITY.SUBSCRIPTIONS ? (admin.privileges.SECURITY.SUBSCRIPTIONS.ADD ? true : false) : false
      }
      showArchieve={false}
      showEdit={
        admin.privileges.SECURITY.SUBSCRIPTIONS ? (admin.privileges.SECURITY.SUBSCRIPTIONS.EDIT ? true : false) : false
      }
      addClick={() => history.push(NavLinks.ADMIN_SECURITY_SUBSCRIPTIONS_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_SECURITY_SUBSCRIPTIONS_EDIT + "/" + e)}
      showBulk={true}
      showSearch={true}
      showAction={
        admin.privileges.SECURITY.SUBSCRIPTIONS ? (admin.privileges.SECURITY.SUBSCRIPTIONS.EDIT ? true : false) : false
      }
      add={
        admin.privileges.SECURITY.SUBSCRIPTIONS ? (admin.privileges.SECURITY.SUBSCRIPTIONS.ADD ? true : false) : false
      }
      edit={
        admin.privileges.SECURITY.SUBSCRIPTIONS ? (admin.privileges.SECURITY.SUBSCRIPTIONS.EDIT ? true : false) : false
      }
      statusList={A.HOST + A.ADMIN_SECURITY_SUBSCRIPTIONS_STATUS}
    />
  );
}
