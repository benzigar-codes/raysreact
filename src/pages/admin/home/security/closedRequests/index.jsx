import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  const startingHeadings = U.SHOW_SECURITY
    ? [
        {
          id: 1,
          title: language.TICKER_ID,
          key: "ticketId",
          show: true,
        },
        {
          id: 2,
          title: language.USER_TYPE,
          key: "userType",
          show: true,
        },
        {
          id: 3,
          title: language.USER_DETAILS,
          key: "oneByOne",
          show: true,
        },
        {
          id: 4,
          title: language.OPERATORS,
          key: "oneByOne3",
          show: true,
        },
        {
          id: 5,
          title: language.RESPONSE_OFFICERS,
          key: "oneByOne2",
          show: true,
        },
        {
          id: 6,
          title: language.PRIORITY,
          key: "priority",
          show: true,
        },
      ]
    : [
        {
          id: 1,
          title: language.TICKER_ID,
          key: "ticketId",
          show: true,
        },
        {
          id: 2,
          title: language.USER_TYPE,
          key: "userType",
          show: true,
        },
        {
          id: 3,
          title: language.USER_DETAILS,
          key: "oneByOne",
          show: true,
        },
        {
          id: 4,
          title: language.OPERATORS,
          key: "oneByOne3",
          show: true,
        },
        // {
        //   id: 5,
        //   title: language.RESPONSE_OFFICERS,
        //   key: "oneByOne2",
        //   show: true,
        // },
        {
          id: 6,
          title: language.PRIORITY,
          key: "priority",
          show: true,
        },
      ];
  return (
    <Table
      title={language.CLOSED_REQUESTS}
      startingHeadings={startingHeadings}
      list={A.HOST + A.ADMIN_SECURITY_CLOSED_REQUESTS}
      assignData={(data) =>
        data.map((request) => ({
          _id: request.ticketId,
          ticketId: request.ticketId,
          userType: language[request.userType],
          oneByOne:
            request.userType === "USER"
              ? [
                  morph(request?.user?.firstName) + " " + morph(request?.user?.lastName),
                  morph(request?.user?.email),
                  morph(request?.user?.phone?.code) + " " + morph(request?.user?.phone?.number),
                ]
              : [
                  morph(request?.professional?.firstName) + " " + morph(request?.professional?.lastName),
                  morph(request?.professional?.email),
                  morph(request?.professional?.phone?.code) + " " + morph(request?.professional?.phone?.number),
                ],
          priority: language[request.priority],
          oneByOne2: request.officer
            ? [
                morph(request?.officer?.firstName) + " " + morph(request?.officer?.lastName),
                morph(request?.officer?.email),
                morph(request?.officer?.phone?.code) + " " + morph(request?.officer?.phone?.number),
              ]
            : [language.UNASSIGNED],
          oneByOne3: request.operator
            ? [
                morph(request?.operator?.firstName) + " " + morph(request?.operator?.lastName),
                morph(request?.operator?.email),
                morph(request?.operator?.phone?.code) + " " + morph(request?.operator?.phone?.number),
              ]
            : [language.UNASSIGNED],
          // notes: request.adminNotes || "",
        }))
      }
      bread={[{ id: 1, title: language.EMERGENCY }]}
      showAdd={false}
      // notesLink={A.HOST + A.ADMIN_SECURITY_NOTES_ADD}
      // showNotes={true}
      showStatus={false}
      userTypeFilter={true}
      priorityTypeFilter={true}
      requestFilterClick={(e) => history.push(e)}
      showRequestsFilter={true}
      unregStatus={A.HOST + A.ADMIN_SECURITY_REQUESTS_STATUS}
      showArchieve={false}
      showView={true}
      viewClick={(e) => history.push(NavLinks.ADMIN_SECURITY_REQUESTS_VIEW + "/" + e + "?type=CLOSED")}
      showBulk={false}
      enableFreshClick={false}
      showSearch={true}
      showAction={
        admin.privileges.SECURITY.CLOSED_REQUESTS
          ? admin.privileges.SECURITY.CLOSED_REQUESTS.VIEW
            ? true
            : false
          : false
      }
    />
  );
}
