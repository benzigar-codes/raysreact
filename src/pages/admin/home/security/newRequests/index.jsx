import React from "react";
import Table from "../../../../../components/common/Table";
import useLanguage from "../../../../../hooks/useLanguage";

import A from "../../../../../utils/API.js";
import U from "../../../../../utils/utils.js";
import NavLinks from "../../../../../utils/navLinks.json";
import useAdmin from "../../../../../hooks/useAdmin";
import useUtils from "../../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  const startingHeadings = U.SHOW_SECURITY
    ? [
        {
          id: 1,
          title: language.USER_DETAILS,
          key: "oneByOne",
          show: true,
        },
        {
          id: 2,
          title: language.ALERT_TYPE,
          key: "alert",
          show: true,
        },
        // {
        //   id: 3,
        //   title: language.SERVICE_TYPE,
        //   key: "service",
        //   show: true,
        // },
        {
          id: 4,
          title: language.ONRIDE,
          key: "onRide",
          show: true,
        },
        {
          id: 5,
          title: language.PRIORITY,
          key: "priority",
          show: true,
        },
        {
          id: 6,
          title: language.STATUS,
          key: "unregister_status",
          show: true,
        },
        {
          id: 7,
          title: language.RESPONSE_OFFICER_ASSIGNED,
          key: "oneByOne2",
          show: false,
        },
        {
          id: 8,
          title: language.OPERATOR_ASSIGNED,
          key: "oneByOne3",
          show: true,
        },
      ]
    : [
        {
          id: 1,
          title: language.USER_DETAILS,
          key: "oneByOne",
          show: true,
        },
        {
          id: 2,
          title: language.ALERT_TYPE,
          key: "alert",
          show: true,
        },
        // {
        //   id: 3,
        //   title: language.SERVICE_TYPE,
        //   key: "service",
        //   show: true,
        // },
        {
          id: 4,
          title: language.ONRIDE,
          key: "onRide",
          show: true,
        },
        {
          id: 5,
          title: language.PRIORITY,
          key: "priority",
          show: true,
        },
        {
          id: 6,
          title: language.STATUS,
          key: "unregister_status",
          show: true,
        },
        // {
        //   id: 7,
        //   title: language.RESPONSE_OFFICER_ASSIGNED,
        //   key: "oneByOne2",
        //   show: false,
        // },
        {
          id: 7,
          title: language.OPERATOR_ASSIGNED,
          key: "oneByOne3",
          show: true,
        },
      ];
  return (
    <Table
      title={language.NEW_ATTENDED_REQUESTS}
      startingHeadings={startingHeadings}
      list={A.HOST + A.ADMIN_SECURITY_NEW_REQUESTS}
      assignData={(data) =>
        data.map((request) => ({
          _id: request._id,
          oneByOne: [
            morph(request.firstName) + " " + morph(request.lastName),
            morph(request.phone?.code) + " " + morph(request.phone?.number),
          ],
          alert: language[request.alertType],
          onRide: request.booking ?? language.UNASSIGNED,
          service: language[request.securityServiceName],
          priority: language[request.priority],
          unregister_status: request.status,
          oneByOne2: request.isResponseOfficerAssign
            ? [
                morph(request?.responseOfficer?.firstName) + " " + morph(request?.responseOfficer?.lastName),
                morph(request?.responseOfficer?.email),
              ]
            : [language.UNASSIGNED],
          oneByOne3: request.isOperatorAssign
            ? [
                morph(request?.operator?.firstName) + " " + morph(request?.operator?.lastName),
                morph(request?.operator?.email),
              ]
            : [language.UNASSIGNED],
          // notes: request.adminNotes || "",
        }))
      }
      bread={[{ id: 1, title: language.EMERGENCY }]}
      showAdd={false}
      // notesLink={A.HOST + A.ADMIN_SECURITY_NOTES_ADD}
      // showNotes={true}
      showStatus={false}
      userTypeFilter={true}
      priorityTypeFilter={true}
      requestFilterClick={(e) => history.push(e)}
      showRequestsFilter={true}
      unregStatus={A.HOST + A.ADMIN_SECURITY_REQUESTS_STATUS}
      showArchieve={false}
      showView={true}
      viewClick={(e) => history.push(NavLinks.ADMIN_SECURITY_REQUESTS_VIEW + "/" + e + "?type=NEW")}
      showBulk={false}
      enableFreshClick={false}
      showSearch={true}
      showAction={
        admin.privileges.SECURITY.CLOSED_REQUESTS
          ? admin.privileges.SECURITY.CLOSED_REQUESTS.VIEW
            ? true
            : false
          : false
      }
    />
  );
}
