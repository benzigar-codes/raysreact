import React from "react";
import gsap from "gsap";
import { Loader } from "@googlemaps/js-api-loader";
import { Link, useParams } from "react-router-dom";
import { format } from "date-fns/esm";
import axios from "axios";
import { FiAlertCircle, FiCheck, FiCheckCircle, FiInfo, FiLoader, FiRefreshCw, FiX } from "react-icons/fi";

import { FormWrapper } from "../../../../../../components/common/FormWrapper";
import { Section } from "../../../../../../components/common/Section";
import { Border } from "../../../../../../components/common/Border";
import { Heading } from "../../../../../../components/common/Heading";
import { TextArea } from "../../../../../../components/common/TextArea";
import { Detail } from "../../../../../../components/common/Detail";
import { FieldWrapper } from "../../../../../../components/common/FieldWrapper";
import { DetailsWrapper } from "../../../../../../components/common/DetailsWrapper";
import { Button } from "../../../../../../components/common/Button";
import { SmallLoader } from "../../../../../../components/common/SmallLoader";
import TrackingMap from "../../../../../../components/TrackingMap";

import LoadingGif from "../../../../../../assets/gifs/loading.gif";

import U from "../../../../../../utils/utils.js";
import A from "../../../../../../utils/API.js";
import NavLinks from "../../../../../../utils/navLinks.json";

import useLanguage from "../../../../../../hooks/useLanguage";
import useUtils from "../../../../../../hooks/useUtils";
import useSocket from "../../../../../../hooks/useSocket";
import useDebug from "../../../../../../hooks/useDebug";
import useSettings from "../../../../../../hooks/useSettings";
import useAdmin from "../../../../../../hooks/useAdmin";
import useImage from "../../../../../../hooks/useImage";

export default function Index({ history }) {
  const params = new URLSearchParams(window.location.search);
  const type = params.get("type");
  const { notificationSocket } = useSocket();

  const { language } = useLanguage();
  const { id } = useParams();
  const [responseOfficerShow, setResponseOfficerShow] = React.useState(false);
  const [requestData, setRequestData] = React.useState([]);
  const { admin, header, authFailure, token } = useAdmin();
  const [loading, setLoading] = React.useState(true);
  const [firstLoad, setFirstLoad] = React.useState(true);
  const [preview, setPreview] = React.useState(false);
  const { videoUrl, morph } = useUtils();
  const { imageUrl } = useImage();
  const [notes, setNotes] = React.useState(null);
  const { settings } = useSettings();
  const [socketConnected, setSocketConnected] = React.useState(false);

  const [bookings, setBookings] = React.useState([]);
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [basedOnEscortClose, setBasedOnEscortClose] = React.useState(true);
  const [notificationLoading, setNotificationLoading] = React.useState(false);

  const [bookingStatusLoading, setBookingStatusLoading] = React.useState(false);

  // const requestTimer = +settings.driverRequestTimeout * 1000;
  const requestTimer = 50000;

  const [barWidth, setBarWidth] = React.useState(0);
  const retryInterval = React.useRef();
  const progressBarInterval = React.useRef();
  // const progressBarDivider =
  //   (parseInt(settings.bookingRetryCount) + 1) * (parseInt(settings.driverRequestTimeout) * 1000 + extraTime);

  // Users
  const [user, setUser] = React.useState(false);
  const [professional, setProfessional] = React.useState(false);
  const [opearator, setOperator] = React.useState(false);
  const [responseOfficer, setResponseOfficer] = React.useState(false);

  const [showSearching, setShowSearching] = React.useState(false);

  const [opeartorAssignLoading, setOperatorAssignLoading] = React.useState(false);

  const [availableOfficers, setAvailableOfficers] = React.useState([]);

  const fetchData = async () => {
    setBtnLoading(true);
    try {
      let URL = "";
      let Query = {};

      if (type === "NEW") {
        URL = A.HOST + A.ADMIN_SECURITY_REQUESTS_READ;
        Query = {
          escortBookingId: id,
        };
      }

      if (type === "CLOSED") {
        URL = A.HOST + A.ADMIN_SECURITY_CLOSED_REQUESTS_READ;
        Query = {
          ticketId: id,
        };
      }

      if (type === "OPERATOR") {
        URL = A.HOST + A.ADMIN_SECURITY_OPERATOR_VIEW;
        Query = {
          ticketId: id,
        };
      }

      firstLoad === true && setLoading(true);

      const { data } = await axios.post(URL, Query, header);
      if (data.length < 1) {
        history.goBack();
      }

      setNotes(data[0].security.adminNotes);
      // Settings Users
      setUser(data[0].security.userType === "USER" ? data[0].user : false);
      // data.forEach((each) =>
      //   each.officer._id !== "" && responseOfficer === false
      //     ? setResponseOfficer({ ...each.officer, security: each.security })
      //     : setResponseOfficer(false)
      // );
      setBasedOnEscortClose(true);
      data.forEach(
        (each) =>
          (each.security.securityServiceName === "FULL_SOS" ||
            each.security.securityServiceName === "PERSONAL_WATCH" ||
            each.security.securityServiceName === "TRIP_WATCH") &&
          setResponseOfficer({ ...each.officer, officer: each.officer, security: each.security })
      );
      data.forEach(
        (each) =>
          (each.security.securityServiceName === "FULL_SOS" ||
            each.security.securityServiceName === "PERSONAL_WATCH" ||
            each.security.securityServiceName === "TRIP_WATCH") &&
          each.security.escortStatus !== "AWAITING" &&
          each.security.escortStatus !== "ENDED" &&
          each.security.escortStatus !== "CANCELLED" &&
          each.security.escortStatus !== "USERDENY" &&
          each.security.escortStatus !== "EXPIRED" &&
          setBasedOnEscortClose(false)
      );
      setProfessional(data[0].security.userType !== "USER" ? data[0].professional : false);
      setOperator(data[0].operator._id !== "" ? data[0].operator : false);
      // setResponseOfficer(data[0].officer._id !== "" ? data[0].officer : false);
      setRequestData(data.sort((a, b) => new Date(b.security.alertDate) - new Date(a.security.alertDate)));

      let bookingData = [];
      data
        .sort((a, b) => new Date(b.security.alertDate) - new Date(a.security.alertDate))
        .forEach(
          (each) => bookingData.includes(each.booking.bookingId) === false && bookingData.push(each.booking.bookingId)
        );
      setBookings(
        bookingData.map((eachBooking) =>
          data.filter((eachData) => eachData.booking.bookingId === eachBooking).length > 0
            ? {
                ...data.filter((eachData) => eachData.booking.bookingId === eachBooking)[0].booking,
                contents: data
                  .sort((a, b) => new Date(b.security.alertDate) - new Date(a.security.alertDate))
                  .filter((each) => each.booking.bookingId === eachBooking)
                  .map((each) => each.security),
              }
            : {}
        )
      );

      firstLoad === true && setLoading(false);
      setFirstLoad(false);
      setBtnLoading(false);
    } catch (err) {
      console.log(err);
      // alert(err);
      authFailure(err);
    }
  };

  React.useEffect(() => {
    if (requestData && requestData.length > 0 && responseOfficer === false) {
      notificationSocket.current.on("securityAcceptNotify", (data) => {
        // console.log(data);
        if (data.id === requestData[0]._id) {
          fetchData();
          setShowSearching(false);
        }
      });
    }
  }, [requestData, responseOfficer]);

  React.useEffect(() => {
    loading === true && gsap.fromTo(".viewSecurity", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, [loading]);

  const closeStatus = async () => {
    try {
      setLoading(true);
      await axios.post(
        A.HOST + A.ADMIN_SECURITY_REQUESTS_STATUS,
        {
          id: requestData[0]?.security._id,
          status: U.CLOSED,
        },
        header
      );
      if (type === "NEW") {
        history.replace(
          NavLinks.ADMIN_SECURITY_REQUESTS_VIEW + "/" + requestData[0].security.ticketId + "?type=OPERATOR"
        );
      } else {
        fetchData();
      }
    } catch (err) {
      authFailure(err);
      setLoading(false);
    }
  };

  const priorityChange = async (priority) => {
    try {
      await axios.post(
        A.HOST + A.ADMIN_SECURITY_REQUESTS_PRIORITY,
        {
          escortBookingId: requestData[0]?.security._id,
          priority,
        },
        header
      );
      fetchData();
    } catch (err) {
      authFailure(err);
    }
  };

  const bookingStatusChange = async (status) => {
    setBookingStatusLoading(true);
    try {
      await axios.post(
        A.HOST + A.ADMIN_SECURITY_BOOKING_STATUS_CHANGE,
        {
          escortBookingId: responseOfficer.security._id,
          escortStatus: status,
        },
        header
      );
      fetchData();
    } catch (err) {
      authFailure(err);
    }
    setBookingStatusLoading(false);
  };

  const cancelNotification = async () => {
    try {
      await axios.post(
        A.HOST + A.ADMIN_SECURITY_CANCEL_REQUEST,
        {
          escortBookingId: requestData[0].security._id,
        },
        header
      );
      setShowSearching(false);
    } catch (er) {
      authFailure(er);
      setShowSearching(false);
    }
  };

  const saveNotes = async () => {
    try {
      setLoading(true);
      await axios.post(
        A.HOST + A.ADMIN_SECURITY_NOTES_ADD,
        {
          id: requestData[0]?.security._id,
          notes,
        },
        header
      );
      fetchData();
    } catch (err) {
      authFailure(err);
    }
  };

  const assignOperator = async () => {
    try {
      setOperatorAssignLoading(true);
      await axios.post(
        A.HOST + A.ADMIN_SECURITY_REQUESTS_ACCEPT_OPERATOR,
        {
          id: requestData[0]?.security._id,
        },
        header
      );
      fetchData();
    } catch (err) {
      authFailure(err);
    }
    setOperatorAssignLoading(false);
  };

  const assignOfficer = async (officerId, securityId) => {
    try {
      await axios.post(
        A.HOST + A.ADMIN_SECURITY_ASSIGN_DRIVER_REQUEST_ACCEPT,
        {
          escortBookingId: securityId,
          officerId: officerId,
        },
        header
      );
      fetchData();
      setAvailableOfficers([]);
    } catch (err) {
      authFailure(err);
    }
    fetchData();
  };

  const fetchOfficers = async (id) => {
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_SECURITY_REQUESTS_VIEW_LIST_OFFICERS,
        {
          escortBookingId: id,
        },
        header
      );
      if (data.length === 0) alert(language.NO_OFFICERS_AVAILABLE);
      setAvailableOfficers(data);
    } catch (err) {
      alert(language.NO_OFFICERS_AVAILABLE);
      authFailure(err);
    }
  };

  const sendNotifications = async (id) => {
    setNotificationLoading(true);
    try {
      setShowSearching(false);
      await axios.post(
        A.HOST + A.ADMIN_SECURITY_SEND_REQUEST,
        {
          escortBookingId: id,
        },
        header
      );
      setShowSearching(true);
      setTimeout(() => {
        setShowSearching(false);
        // responseOfficer === false && alert(language.NO_OFFICER_ACCEPTED);
      }, requestTimer);
    } catch (err) {
      authFailure(err);
      // alert(err);
      alert(language.NO_OFFICERS_AVAILABLE);
    }
    setNotificationLoading(false);
  };

  const commonData = () => {
    return (
      <>
        <Detail
          title={language.STATUS}
          value={
            requestData[0].security.status === "ATTENDED" ? (
              <div className="flex items-center">
                <div className="flex items-center mt-1">
                  <FiCheckCircle className="text-xl text-purple-800 dark:text-purple-500" />
                  <p className="mx-2">{language.ATTENDED}</p>
                </div>
                {admin.phone?.number === opearator?.phone?.number &&
                  admin.userType === "OPERATORS" &&
                  basedOnEscortClose && (
                    <p
                      onClick={closeStatus}
                      className="cursor-pointer bg-red-500 text-white p-1 rounded-full text-sm mx-2 px-2"
                    >
                      {language.CLOSE_THIS_REQUEST}
                    </p>
                  )}
              </div>
            ) : requestData[0].security.status === "CLOSED" ? (
              <div className="flex items-center mt-1">
                <FiCheck className="text-xl text-blue-800 text-white" />
                <p className="mx-2">{language.CLOSED}</p>
              </div>
            ) : (
              <div className="flex items-center mt-1">
                <FiAlertCircle className="text-xl text-red-500 text-white" />
                <p className="mx-2">{language.INCOMPLETE}</p>
              </div>
            )
          }
        />
        <Detail
          title={language.PRIORITY}
          value={
            <div className="">
              {/* <p>
                            {requestData &&
                              requestData.length > 0 &&
                              requestData[0].security &&
                              requestData[0].security.priority &&
                              language[requestData[0].security.priority]}
                          </p> */}
              {admin.phone?.number === opearator?.phone?.number &&
              admin.userType === "OPERATORS" &&
              requestData[0].security.status !== "CLOSED" ? (
                <div className="flex">
                  <div className="flex">
                    {requestData[0].security.priority === "NOTASSIGNED" && (
                      <p
                        className={`cursor-pointer dark:text-white ${
                          requestData[0].security.priority === "NOTASSIGNED" ? "bg-red-500" : ""
                        } p-1 rounded-full text-sm mx-2 px-2 text-white`}
                      >
                        {language.UNASSIGNED}
                      </p>
                    )}
                    <p
                      onClick={() => priorityChange("MINIMAL")}
                      className={`cursor-pointer dark:text-white ${
                        requestData[0].security.priority === "MINIMAL" ? "bg-red-500 text-white" : ""
                      } p-1 rounded-full text-sm mx-2 px-2 text-white`}
                    >
                      {language.MINIMAL}
                    </p>
                    <p
                      onClick={() => priorityChange("MEDIUM")}
                      className={`cursor-pointer dark:text-white ${
                        requestData[0].security.priority === "MEDIUM" ? "bg-red-500 text-white" : ""
                      } p-1 rounded-full text-sm mx-2 px-2 text-white`}
                    >
                      {language.MEDIUM}
                    </p>
                    <p
                      onClick={() => priorityChange("CRITICAL")}
                      className={`cursor-pointer dark:text-white ${
                        requestData[0].security.priority === "CRITICAL" ? "bg-red-500 text-white" : ""
                      } p-1 rounded-full text-sm mx-2 px-2 text-white`}
                    >
                      {language.CRITICAL}
                    </p>
                  </div>
                </div>
              ) : (
                <p className={`cursor-pointer dark:text-white p-1 rounded-full text-sm`}>
                  {language[requestData[0].security.priority]}
                </p>
              )}
            </div>
          }
        />
      </>
    );
  };

  React.useEffect(() => {
    fetchData();
  }, [id]);

  React.useEffect(() => {
    if ((user !== false || professional !== false) && socketConnected === false) {
      notificationSocket.current.on("securityUpdates", (data) => {
        // console.log(data);
        if (user && user._id === data.id) fetchData();
        if (professional && professional._id === data.id) fetchData();
      });
      setSocketConnected(true);
    }
  }, [user, professional]);

  return loading && requestData && requestData.length === 0 ? (
    <SmallLoader />
  ) : (
    <FormWrapper
      width="full"
      title={
        user
          ? morph(user.firstName + " " + user.lastName)
          : professional
          ? morph(professional.firstName + " " + professional.lastName)
          : ""
      }
      bread={[
        {
          id: 1,
          title:
            type === "NEW"
              ? language.NEW_REQUESTS
              : type === "OPERATOR"
              ? language.MY_REQUESTS
              : language.CLOSED_REQUESTS,
          path:
            type === "NEW"
              ? NavLinks.ADMIN_SECURITY_REQUESTS
              : type === "OPERATOR"
              ? NavLinks.ADMIN_SECURITY_MY_REQUESTS
              : NavLinks.ADMIN_SECURITY_CLOSED_REQUESTS,
        },
      ]}
    >
      {preview && (
        <div className="fixed inset-0 flex justify-center items-center" style={{ zIndex: 30 }}>
          <div onClick={() => setPreview(null)} className="fixed inset-0 bg-black bg-opacity-75"></div>
          <div className="fixed top-0 right-0 p-10">
            <FiX onClick={() => setPreview(null)} className="text-4xl cursor-pointer" />
          </div>
          {preview?.type === "IMAGE" && (
            <div style={{ zIndex: 40 }}>
              {/* <image style={{ height: 500 }} src={imageUrl(preview?.url)} /> */}
              <img alt={""} style={{ height: 500 }} src={imageUrl(preview?.url)} />
            </div>
          )}
          {preview?.type === "VIDEO" && (
            <div style={{ zIndex: 40 }}>
              <video autoPlay={true} controls={true} style={{ height: 500 }} src={videoUrl(preview?.url)} />
            </div>
          )}
          {preview?.type === "AUDIO" && (
            <div style={{ zIndex: 40 }}>
              <audio autoPlay={true} controls={true} style={{ height: 500 }} src={videoUrl(preview?.url)} />
            </div>
          )}
        </div>
      )}
      <div className="w-full">
        <div className="flex w-full">
          <Section width="1/2">
            {/* No Operator ASSIGNED  */}
            {opearator === false && requestData[0].security.status !== "CLOSED" && (
              <Border>
                <div className="flex justify-between items-center">
                  <Heading title={language.NO_OPERATOR_ASSIGNED} />
                  {admin.userType === "OPERATORS" && (
                    <Button
                      loading={opeartorAssignLoading}
                      click={() => assignOperator()}
                      title={language.ASSIGN_YOURSELF}
                    />
                  )}
                </div>
              </Border>
            )}
            {/* No Response Officer ASSIGNED  */}
            {/* {U.mode === "development" &&
              responseOfficer === false &&
              showSearching === false &&
              availableOfficers.length === 0 &&
              requestData[0].security.status !== "CLOSED" &&
              responseOfficerShow === false && (
                <Border>
                  <div className="flex justify-between items-center">
                    <Heading title={language.NO_RESPONSE_OFFICER_ASSIGNED} />
                    {admin.userType === "OPERATORS" && admin.phone?.number === opearator?.phone?.number && (
                      <div className="flex">
                        <Button click={sendNotifications} title={language.SEND_NOTIFICATIONS} />
                        <Button click={fetchOfficers} title={language.FORCE_ASSIGN} />
                      </div>
                    )}
                  </div>
                </Border>
              )} */}
            {U.mode === "development" && showSearching && (
              <div></div>
              // <Border>
              //   <div className="flex justify-between items-center">
              //     <Heading title={language.WAITING_FOR_OFFICER} />
              //     <Button click={cancelNotification} title={language.CANCEL} />
              //   </div>
              //   <div className="bg-white dark:bg-gray-800 dark:text-white overflow-hidden w-full h-full flex flex-col justify-between p-3">
              //     <div className={"bg-blue-800 px-4 py-3 text-center text-white flex items-center justify-center"}>
              //       <p>{language.REQUEST_SENT}</p> <FiInfo className={"mx-2"} />
              //     </div>
              //     <div className={"h-full w-full"}>
              //       <p
              //         className={
              //           "text-gray-600 dark:text-gray-200 text-white text-center text-sm mt-5 flex justify-center items-center"
              //         }
              //       >
              //         <p>{language.WAITING_FOR_OFFICER}</p> <FiLoader className={"animate-spin text-xl mx-3"} />
              //       </p>
              //       <div className="flex w-full justify-center items-center my-3">
              //         <img src={LoadingGif} alt="loading" style={{ height: 100 }} />
              //       </div>
              //     </div>
              //     {/* <div className="w-full mt-2 relative">
              //       <div className="bg-gray-200 absolute" style={{ height: 10, width: "100%" }}></div>
              //       <div
              //         className="bg-green-800 absolute transition-all"
              //         style={{
              //           height: 10,
              //           // width: (barWidth / progressBarDivider) * 100 + "%",
              //           width: barWidth + "%",
              //         }}
              //       ></div>
              //     </div>{" "} */}
              //     {/* <div onClick={cancelRide} className={"bg-red-500 px-4 py-3 text-center text-white cursor-pointer"}>
              //       {language.CANCEL}
              //     </div> */}
              //   </div>
              // </Border>
            )}
            {user !== false && (
              <Border>
                <div className="flex justify-between items-center">
                  <Heading title={language.USER_DETAILS} />
                  <div className="flex">
                    <button
                      onClick={fetchData}
                      title={language.REFRESH}
                      className={`${
                        btnLoading === true && "animate-spin"
                      } bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 px-3 ${
                        document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                      }`}
                    >
                      <FiRefreshCw />
                    </button>
                    <Link to={NavLinks.ADMIN_USERS_VIEW_EACH + "/" + user._id}>
                      <Button title={language.VIEW_DETAILS} />
                    </Link>
                  </div>
                </div>
                <FieldWrapper style={{ marginTop: 5 }}>
                  <DetailsWrapper>
                    <Detail title={language.NAME} value={morph(user?.firstName) + " " + morph(user?.lastName)} />
                    {/* <Detail title={language.EMAIL} value={user?.email} /> */}
                    <Detail
                      title={language.PHONE}
                      value={morph(user?.phone?.code) + " " + morph(user?.phone?.number)}
                    />
                    {commonData()}
                  </DetailsWrapper>
                </FieldWrapper>
                {responseOfficer && responseOfficer.security && responseOfficer.security.acknowledge && (
                  <div className="mt-4">
                    <Heading title={language.ACKNOWLEDGEMENT_DETAILS} />
                    <div className="mx-3 w-full mt-2">
                      <DetailsWrapper>
                        <Detail
                          title={language.ESCORT_ARRIVED}
                          value={
                            <p
                              className={
                                responseOfficer.security.acknowledge.isArrivedStatus === "NO" ? "text-red-500" : ""
                              }
                            >
                              {responseOfficer.security.acknowledge.isArrivedStatus === "YES"
                                ? language.CONFIRMED_BY_USER
                                : responseOfficer.security.acknowledge.isArrivedStatus === "NO"
                                ? language.USER_DENIED_TO_CONFIRM
                                : language.NOT_YET_CONFIRMED}
                            </p>
                          }
                        />
                        <Detail
                          title={language.ESCORT_ENDED}
                          value={
                            <p
                              className={
                                responseOfficer.security.acknowledge.isEndedStatus === "NO" ? "text-red-500" : ""
                              }
                            >
                              {responseOfficer.security.acknowledge.isEndedStatus === "YES"
                                ? language.CONFIRMED_BY_USER
                                : responseOfficer.security.acknowledge.isEndedStatus === "NO"
                                ? language.USER_DENIED_TO_CONFIRM
                                : language.NOT_YET_CONFIRMED}
                            </p>
                          }
                        />
                      </DetailsWrapper>
                    </div>
                  </div>
                )}
              </Border>
            )}
            {professional !== false && (
              <Border>
                <div className="flex justify-between items-center">
                  <Heading title={language.PROFESSIONAL_DETAILS} />
                  <div className="flex">
                    <button
                      onClick={fetchData}
                      title={language.REFRESH}
                      className={`${
                        btnLoading === true && "animate-spin"
                      } bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 px-3 ${
                        document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                      }`}
                    >
                      <FiRefreshCw />
                    </button>
                    <Link to={NavLinks.ADMIN_DRIVERS_VIEW + "/" + professional._id}>
                      <Button title={language.VIEW_DETAILS} />
                    </Link>
                  </div>
                </div>
                <FieldWrapper style={{ marginTop: 5 }}>
                  <DetailsWrapper>
                    <Detail
                      title={language.NAME}
                      value={morph(professional?.firstName) + " " + morph(professional?.lastName)}
                    />
                    {/* <Detail title={language.EMAIL} value={user?.email} /> */}
                    <Detail
                      title={language.PHONE}
                      value={morph(professional?.phone?.code) + " " + morph(professional?.phone?.number)}
                    />
                    {commonData()}
                  </DetailsWrapper>
                </FieldWrapper>
              </Border>
            )}
            {/* OPERATOR ASSIGNED  */}
            {opearator && (
              <Border>
                <div className="flex justify-between items-center">
                  <Heading title={language.OPERATOR_ASSIGNED} />
                </div>
                <FieldWrapper style={{ marginTop: 5 }}>
                  <DetailsWrapper>
                    <Detail title={language.NAME} value={morph(opearator.firstName + " " + opearator.lastName)} />
                    <Detail
                      title={language.PHONE}
                      value={morph(opearator.phone?.code) + " " + morph(opearator.phone?.number)}
                    />
                  </DetailsWrapper>
                </FieldWrapper>
              </Border>
            )}
            {/* responseOfficer SECTION  */}
            {U.mode === "development" &&
            responseOfficer &&
            responseOfficer.security &&
            admin.userType === "OPERATORS" &&
            availableOfficers.length > 0 &&
            admin.phone?.number === opearator?.phone?.number ? (
              <Border>
                <div className="flex justify-between items-center">
                  <Heading title={language.ASSIGN_RESPONSE_OFFICER} />
                  <Button click={() => setAvailableOfficers([])} title={language.CANCEL} />
                </div>
                <FieldWrapper>
                  <div className="overflow-y-scroll w-full" style={{ height: 200 }}>
                    {availableOfficers.length === 0 && (
                      <p className="text-sm text-center text-gray-500">{language.NO_OFFICERS_AVAILABLE}</p>
                    )}
                    <table
                      // onClick={() => {
                      //   setResponseOfficer(true);
                      //   setResponseOfficerShow(false);
                      // }}
                      className="w-full"
                    >
                      {availableOfficers.length > 0 &&
                        availableOfficers.map((each, idx) => (
                          <tr className="border-b-2 dark:border-gray-600 text-sm">
                            <td>
                              <p className="px-2">{idx + 1}</p>
                            </td>
                            <td className="py-2">{each.firstName + " " + each.lastName}</td>
                            <td className="mt-2 truncate">
                              {each?.phone?.code} {each?.phone?.number}
                            </td>
                            <td className="px-2 mt-2 truncate">{each.distance} KM</td>
                            <td>
                              <FiCheck
                                onClick={() => assignOfficer(each._id, responseOfficer.security._id)}
                                className="text-2xl text-white bg-blue-800 p-1 cursor-pointer rounded-full"
                              />
                            </td>
                          </tr>
                        ))}
                    </table>
                  </div>
                </FieldWrapper>
              </Border>
            ) : responseOfficer ? (
              <Border>
                <div className="flex justify-between items-center">
                  <Heading
                    title={
                      responseOfficer.officer._id !== ""
                        ? language.RESPONSE_OFFICER_ASSIGNED
                        : language.RESPONSE_OFFICER_UNASSIGNED
                    }
                  />
                </div>
                <FieldWrapper style={{ marginTop: 5 }}>
                  <DetailsWrapper>
                    <Detail
                      title={language.ESCORT_TYPE}
                      value={language[responseOfficer.security.securityServiceName]}
                    />
                    {responseOfficer && responseOfficer.security && responseOfficer.security.alertDate && (
                      <Detail
                        title={language.INITIATED_TIME}
                        value={format(new Date(responseOfficer.security.alertDate), "PP p")}
                      />
                    )}
                    {responseOfficer.officer._id !== "" && (
                      <Detail
                        title={language.NAME}
                        value={morph(responseOfficer.officer.firstName + " " + responseOfficer.officer.lastName)}
                      />
                    )}
                    {responseOfficer.officer._id !== "" && (
                      <Detail
                        title={language.PHONE}
                        value={morph(responseOfficer.phone?.code) + " " + morph(responseOfficer.phone?.number)}
                      />
                    )}
                    {responseOfficer && responseOfficer.security && (
                      <Detail title={language.ESCORT_STATUS} value={language[responseOfficer.security.escortStatus]} />
                    )}
                    {admin.userType === "OPERATORS" &&
                      admin.phone?.number === opearator?.phone?.number &&
                      responseOfficer &&
                      responseOfficer.security &&
                      responseOfficer.security.escortStatus !== U.ENDED &&
                      responseOfficer.security.escortStatus !== U.EXPIRED &&
                      responseOfficer.security.escortStatus !== U.USERDENIED &&
                      responseOfficer.security.escortStatus !== U.USERCANCELLED &&
                      responseOfficer.security.escortStatus !== U.AWAITING && (
                        <Detail
                          title={language.CHANGE_STATUS_TO}
                          value={
                            <>
                              {responseOfficer.security.escortStatus === U.ACCEPTED && (
                                <div className="flex items-center">
                                  <p
                                    className="cursor-pointer bg-red-500 text-white text-md rounded-xl px-2"
                                    onClick={() => bookingStatusChange(U.ARRIVED)}
                                  >
                                    {language.ARRIVED}
                                  </p>
                                  {bookingStatusLoading && <FiLoader className="animate-spin mx-2" />}
                                </div>
                              )}
                              {responseOfficer.security.escortStatus === U.ARRIVED && (
                                <div className="flex items-center">
                                  <p
                                    className="cursor-pointer bg-red-500 text-white text-md rounded-xl px-2"
                                    onClick={() => bookingStatusChange(U.ENDED)}
                                  >
                                    {language.ENDED}
                                  </p>
                                  {bookingStatusLoading && <FiLoader className="animate-spin mx-2" />}
                                </div>
                              )}
                            </>
                          }
                        />
                      )}
                    {responseOfficer &&
                      responseOfficer.security.escortStatus === "AWAITING" &&
                      admin.userType === "OPERATORS" &&
                      admin.phone?.number === opearator?.phone?.number && (
                        <Detail
                          title={language.ACTION}
                          value={
                            <div className="flex items-center">
                              {showSearching ? (
                                <div className="flex items-center">
                                  <p>{language.NOTIFICATION_SENT}</p>
                                  <FiCheck className="mx-3" />
                                </div>
                              ) : (
                                <div className="flex items-center">
                                  <p
                                    onClick={() => sendNotifications(responseOfficer.security._id)}
                                    className="mr-3 cursor-pointer"
                                  >
                                    {language.SEND_NOTIFICATIONS}
                                  </p>
                                  {notificationLoading && <FiLoader className="animate-spin mr-3" />}
                                </div>
                              )}
                              |
                              {showSearching === false && (
                                <p
                                  onClick={() => fetchOfficers(responseOfficer.security._id)}
                                  className="mx-3 cursor-pointer"
                                >
                                  {language.FORCE_ASSIGN}
                                </p>
                              )}
                            </div>
                          }
                        />
                      )}
                    {/* <Detail
                      title={language.PRIORITY}
                      value={
                        <div className="">
                          {admin.phone?.number === opearator?.phone?.number &&
                          admin.userType === "OPERATORS" &&
                          responseOfficer.security && 
                          requestData[0].security.status !== "CLOSED" && (
                            <div className="flex">
                              <div className="flex"> 
                                <p className={`cursor-pointer dark:text-white p-1 rounded-full text-sm px-2`}>
                                  {language[responseOfficer.security.escortStatus]}
                                </p>
                              </div>
                            </div>
                          )}
                        </div>
                      }
                    /> */}
                  </DetailsWrapper>
                </FieldWrapper>
                {responseOfficer &&
                  responseOfficer.security &&
                  ((responseOfficer.security.mediaReportImage &&
                    Array.isArray(responseOfficer.security.mediaReportImage) &&
                    responseOfficer.security.mediaReportImage.length > 0) ||
                    responseOfficer.security.mediaReportAudio ||
                    responseOfficer.security.mediaReportMessage) && (
                    <>
                      <div className="mt-2"></div>
                      <Heading title={language.RESPONSE_OFFICER_REPORT} />
                      <div className="my-1"></div>
                    </>
                  )}
                <div className="mx-3">
                  <DetailsWrapper>
                    {responseOfficer &&
                      responseOfficer.security &&
                      responseOfficer.security.mediaReportImage &&
                      responseOfficer.security.mediaReportImage.map((each, idx) => (
                        <Detail
                          title={idx === 0 ? language.IMAGES : ""}
                          value={
                            <p
                              onClick={() =>
                                setPreview({
                                  url: each,
                                  type: "IMAGE",
                                })
                              }
                              className={"text-blue-800 cursor-pointer hover:text-black"}
                            >
                              {language.CLICK_TO_VIEW}
                            </p>
                          }
                        />
                      ))}
                    {responseOfficer && responseOfficer.security && responseOfficer.security.mediaReportVideo && (
                      <Detail
                        title={language.VIDEO}
                        value={
                          <p
                            onClick={() =>
                              setPreview({
                                url: responseOfficer.security.mediaReportVideo,
                                type: "VIDEO",
                              })
                            }
                            className={"text-blue-800 cursor-pointer hover:text-black"}
                          >
                            {language.CLICK_TO_VIEW}
                          </p>
                        }
                      />
                    )}
                    {responseOfficer && responseOfficer.security && responseOfficer.security.mediaReportAudio && (
                      <Detail
                        title={language.AUDIO}
                        value={
                          <p
                            onClick={() =>
                              setPreview({
                                url: responseOfficer.security.mediaReportAudio,
                                type: "AUDIO",
                              })
                            }
                            className={"text-blue-800 cursor-pointer hover:text-black"}
                          >
                            {language.CLICK_TO_VIEW}
                          </p>
                        }
                      />
                    )}
                    {responseOfficer && responseOfficer.security && responseOfficer.security.mediaReportMessage && (
                      <Detail title={language.MESSAGE} value={responseOfficer.security?.mediaReportMessage} />
                    )}
                  </DetailsWrapper>
                </div>
              </Border>
            ) : null}
          </Section>
          <Section width="1/2">
            {(user !== null || professional !== null || responseOfficer !== null) && U.mode === "development" && (
              <div className="rounded-xl mb-4">
                <TrackingMap user={user} professional={professional} responseOfficer={responseOfficer} />
              </div>
            )}
            <Border>
              <Heading title={language.NOTES} />
              <FieldWrapper>
                <TextArea change={(e) => setNotes(e)} value={notes} />
              </FieldWrapper>
              {admin.phone?.number === opearator?.phone?.number && (
                <FieldWrapper>
                  <Button onClick={() => notes !== null && notes.length > 0 && saveNotes()} title={language.SAVE} />
                </FieldWrapper>
              )}
            </Border>
          </Section>
        </div>

        {bookings &&
          bookings.length > 0 &&
          bookings.map((booking) => (
            <>
              <h1 className="text-center text-3xl font-bold my-3">
                {booking.bookingId === "" ? language.WITHOUT_BOOKING : booking.bookingId}
              </h1>
              {booking.bookingId !== "" && (
                <div className="flex">
                  <Section>
                    <Border>
                      <div className="flex justify-between items-center">
                        <Heading title={language.RIDE_DETAILS} />
                        <Link to={NavLinks.ADMIN_RIDES_VIEW + "/" + booking._id}>
                          <Button title={language.VIEW_DETAILS} />
                        </Link>
                      </div>
                      <FieldWrapper style={{ marginTop: 5 }}>
                        <DetailsWrapper>
                          <Detail title={language.BOOKING_ID} value={booking.bookingId} />
                          <Detail title={language.BOOKING_TYPE} value={language[booking.bookingType]} />
                          <Detail
                            title={language.BOOKING_TIME}
                            value={format(new Date(booking.activity.bookingTime), "PP p")}
                          />
                          <Detail title={language.START_ADDRESS} value={morph(booking.origin.fullAddress)} />
                          <Detail title={language.END_ADDRESS} value={morph(booking.destination.fullAddress)} />
                        </DetailsWrapper>
                      </FieldWrapper>
                    </Border>
                  </Section>
                  <Section>
                    {booking.vehicle && (
                      <Border>
                        <Heading title={language.VEHICLE_DETAILS} />
                        <FieldWrapper style={{ marginTop: 5 }}>
                          <DetailsWrapper>
                            <Detail title={language.PLATE_NUMBER} value={booking.vehicle?.plateNumber} />
                            <Detail title={language.MODEL} value={booking.vehicle?.model} />
                            <Detail title={language.VIN_NUMBER} value={booking.vehicle?.vinNumber} />
                            <Detail title={language.TYPE} value={language[booking.vehicle?.type]} />
                            <Detail title={language.DOOR_NO} value={booking.vehicle?.noOfDoors} />
                            <Detail title={language.SEATS_NO} value={booking.vehicle?.noOfSeats} />
                          </DetailsWrapper>
                        </FieldWrapper>
                      </Border>
                    )}
                  </Section>
                </div>
              )}
              <div className="flex flex-wrap">
                {booking.contents.map((content) => (
                  <Section width="1/3">
                    <Border>
                      <div className="flex justify-between items-center">
                        <h1 className="mx-4 text-md text-blue-800">{language[content.securityServiceName]}</h1>
                        <p className="text-sm text-gray-500 mx-2">{format(new Date(content.alertDate), "PP p")}</p>
                      </div>
                      <FieldWrapper>
                        <DetailsWrapper>
                          {(content?.securityServiceName === "SEND_VOICE_NOTE" ||
                            content?.securityServiceName === "SEND_MESSAGE" ||
                            content?.securityServiceName === "RECORD_LIVE_STREAM_VIDEO" ||
                            content?.securityServiceName === "SEND_SNAP_PICTURE") && (
                            <Detail
                              title={language.CONTENT}
                              value={
                                content?.securityServiceName === "SEND_MESSAGE" ? (
                                  <p>{content?.threadMessage}</p>
                                ) : (
                                  <p
                                    onClick={() =>
                                      setPreview({
                                        url: content?.threadMessage,
                                        type:
                                          content?.securityServiceName === "RECORD_LIVE_STREAM_VIDEO"
                                            ? "VIDEO"
                                            : content?.securityServiceName === "SEND_SNAP_PICTURE"
                                            ? "IMAGE"
                                            : content?.securityServiceName === "SEND_VOICE_NOTE"
                                            ? "AUDIO"
                                            : "",
                                      })
                                    }
                                    className={"text-blue-800 cursor-pointer hover:text-black"}
                                  >
                                    {language.CLICK_TO_VIEW}
                                  </p>
                                )
                              }
                            />
                          )}
                          {content.securityThreadId && (
                            <Detail title={language.SECURITY_THREAD_ID} value={morph(content?.securityThreadId)} />
                          )}
                          <Detail
                            title={language.INITIATED_ADDRESS}
                            value={morph(content?.alertLocation?.fullAddress)}
                          />
                        </DetailsWrapper>
                      </FieldWrapper>
                    </Border>
                  </Section>
                ))}
              </div>
            </>
          ))}
      </div>
    </FormWrapper>
  );
}
