import React from "react";
import axios from "axios";

import { FormWrapper } from "../../../../../components/common/FormWrapper";
import { Section } from "../../../../../components/common/Section";
import { FieldWrapper } from "../../../../../components/common/FieldWrapper";
import { MultiSelect } from "../../../../../components/common/MultiSelect";

import A from "../../../../../utils/API.js";

import useLanguage from "../../../../../hooks/useLanguage";
import useAdmin from "../../../../../hooks/useAdmin";
import { SmallLoader } from "../../../../../components/common/SmallLoader";
import gsap from "gsap/gsap-core";
import { PopUp } from "../../../../../components/common/PopUp";
import useUtils from "../../../../../hooks/useUtils";

export default function Index() {
  const { language } = useLanguage();
  const { header } = useAdmin();
  const { parseError } = useUtils();
  const [services, setServices] = React.useState({});
  const [loading, setLoading] = React.useState(true);
  const [selectedServices, setSelectedServices] = React.useState({});
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [popup, setPop] = React.useState(null);

  React.useEffect(() => {
    const fetchServices = async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_SECURITY_SERVICES_LIST, {}, header);
        setServices({
          id: data._id,
          services: data.data,
        });

        setSelectedServices({
          emergency: data.data.emergency
            .filter((service) => service.status === true)
            .map((service) => language[service.name]),
          tracking: data.data.tracking
            .filter((service) => service.status === true)
            .map((service) => language[service.name]),
          sos: data.data.sos.filter((service) => service.status === true).map((service) => language[service.name]),
        });
      } catch (err) {
        alert(err);
      }
    };
    fetchServices();
  }, []);

  React.useEffect(() => {
    if (services.id !== undefined) {
      setLoading(false);
    }
  }, [services]);

  React.useEffect(() => {
    if (loading === false) {
      gsap.fromTo(".services", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
    }
  }, [loading]);

  const submit = async () => {
    setBtnLoading(true);
    try {
      // EMERGENCY
      const emergencySelectedKeys = selectedServices.emergency.map(
        (selected) => Object.keys(language).filter((key) => language[key] === selected)[0]
      );
      const emergencyFinal = services.services.emergency.map((service) =>
        emergencySelectedKeys.includes(service.name) ? { ...service, status: true } : { ...service, status: false }
      );
      // SOS
      const sosSelectedKeys = selectedServices.sos.map(
        (selected) => Object.keys(language).filter((key) => language[key] === selected)[0]
      );
      const sosFinal = services.services.sos.map((service) =>
        sosSelectedKeys.includes(service.name) ? { ...service, status: true } : { ...service, status: false }
      );
      // tracking
      const trackingSelectedKeys = selectedServices.tracking.map(
        (selected) => Object.keys(language).filter((key) => language[key] === selected)[0]
      );
      const trackingFinal = services.services.tracking.map((service) =>
        trackingSelectedKeys.includes(service.name) ? { ...service, status: true } : { ...service, status: false }
      );

      const finalKeys = {
        emergency: emergencyFinal,
        sos: sosFinal,
        tracking: trackingFinal,
      };

      await axios.post(
        A.HOST + A.ADMIN_SECURITY_SERVICES_UPDATE,
        {
          id: services.id,
          data: finalKeys,
        },
        header
      );

      setPop({ title: language.UPDATE_SUCCESS, type: "success" });
    } catch (err) {
      setPop({ title: parseError(err), type: "error" });
    }
    setBtnLoading(false);
  };

  return loading === true ? (
    <SmallLoader />
  ) : (
    <FormWrapper btnLoading={btnLoading} submit={submit} width={"4/5"} submitBtn={true} title={language.SERVICES}>
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      <Section>
        <FieldWrapper animate={"services"} title={language.EMERGENCY}>
          <MultiSelect
            change={(e) => setSelectedServices({ ...selectedServices, emergency: e })}
            // error={formik.errors.pageViewLimits}
            allFields={services.services.emergency.map((service) => language[service.code])}
            defaultValue={selectedServices.emergency}
          ></MultiSelect>
        </FieldWrapper>
        <FieldWrapper animate={"services"} title={language.SOS}>
          <MultiSelect
            change={(e) => setSelectedServices({ ...selectedServices, sos: e })}
            // error={formik.errors.pageViewLimits}
            allFields={services.services.sos.map((service) => language[service.code])}
            defaultValue={selectedServices.sos}
          ></MultiSelect>
        </FieldWrapper>
      </Section>
      <Section>
        <FieldWrapper animate={"services"} title={language.TRACKING}>
          <MultiSelect
            change={(e) => setSelectedServices({ ...selectedServices, tracking: e })}
            // error={formik.errors.pageViewLimits}
            allFields={services.services.tracking.map((service) => language[service.code])}
            defaultValue={selectedServices.tracking}
          ></MultiSelect>
        </FieldWrapper>
      </Section>
    </FormWrapper>
  );
}
