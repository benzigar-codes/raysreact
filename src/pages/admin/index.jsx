import React from "react";
import { AdminRoutes } from "../../routes/Routes";

export default function Admin({ match }) {
  return <AdminRoutes match={match} />;
}
