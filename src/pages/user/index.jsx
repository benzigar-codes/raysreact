import React from "react";
import useLanguage from "../../../../hooks/useLanguage";

import A from "../../../../utils/API.js";
import U from "../../../../utils/utils.js";
import Table from "../../../../components/common/Table";
import NavLinks from "../../../../utils/navLinks.json";
import { SmallLoader } from "../../../../components/common/SmallLoader";
import axios from "axios";
import useAdmin from "../../../../hooks/useAdmin";
import useUtils from "../../../../hooks/useUtils";

export default function Index({ history }) {
  const { language } = useLanguage();
  const { admin } = useAdmin();
  const { morph } = useUtils();
  return (
    <Table
      title={language.USER}
      startingHeadings={[
        {
          id: 1,
          title: language.NAME,
          key: "name",
          show: true,
        },
        {
          id: 2,
          title: language.EMAIL,
          key: "email",
          show: true,
        },
        {
          id: 3,
          title: language.GENDER,
          key: "gender",
          show: true,
        },
        {
          id: 4,
          title: language.MOBILE,
          key: "phone",
          show: true,
        },
        {
          id: 5,
          title: language.AVG_RATINGS,
          key: "rating",
          show: true,
        },
        {
          id: 6,
          title: language.STATUS,
          key: "status",
          show: true,
        },
      ]}
      list={A.HOST + A.ADMIN_USERS_LIST}
      assignData={(data) =>
        data.map((user) => ({
          _id: user._id,
          name: morph(user.data.firstName) + " " + morph(user.data.lastName),
          email: morph(user.data.email),
          phone: morph(user.data.phone.code) + " " + morph(user.data.phone.number),
          gender: language[user.data.gender],
          rating: parseFloat(user.data.avgRating).toFixed(2),
          status: user.data.status === U.ACTIVE ? 1 : 0,
        }))
      }
      bread={[{ id: 1, title: language.USER }]}
      showFilter={true}
      showAdd={true}
      showArchieve={true}
      addClick={() => history.push(NavLinks.ADMIN_USERS_ADD)}
      editClick={(e) => history.push(NavLinks.ADMIN_USERS_EDIT + "/" + e)}
      showBulk={true}
      showView={true}
      viewClick={(e) => history.push(NavLinks.ADMIN_USERS_VIEW_EACH + "/" + e)}
      showEdit={true}
      showSearch={true}
      statusList={A.HOST + A.ADMIN_USERS_STATUS}
      showAction={admin.privileges.USERS.USERS_LIST ? (admin.privileges.USERS.USERS_LIST.EDIT ? true : false) : false}
      add={admin.privileges.USERS.USERS_LIST ? (admin.privileges.USERS.USERS_LIST.ADD ? true : false) : false}
      edit={admin.privileges.USERS.USERS_LIST ? (admin.privileges.USERS.USERS_LIST.EDIT ? true : false) : false}
    />
  );
}
