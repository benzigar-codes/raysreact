import React from "react";
import { Switch, Route, Redirect, useLocation, useHistory } from "react-router-dom";
import Cookies from "js-cookie";
import { ErrorBoundary } from "react-error-boundary";

import NavLinks from "../utils/navLinks.json";

// Components and Pages
import Admin from "../pages/admin/index";
import NotFound from "../components/NotFound";
import LogIn from "../pages/admin/Login/index";
import AdminValidate from "../pages/admin/AdminValidate";
import Home from "../pages/admin/home/index";

// Admin Settings
import SatelliteView from "../pages/admin/home/others/satelliteView";
import EagleView from "../pages/admin/home/others/eagleView";
import BookRide from "../pages/admin/home/others/bookRide";
import Dispatch from "../pages/admin/home/others/dispatch";
import BookRideDetails from "../pages/admin/home/others/bookRide/rideDetails";
import Profile from "../pages/admin/home/profile/index";
import SMTP from "../pages/admin/home/settings/smtp/index";

// import ChangePassword from "../pages/admin/home/profile/changePassword";
import SMS from "../pages/admin/home/settings/sms";
import General from "../pages/admin/home/settings/general";
import ReferConfig from "../pages/admin/home/settings/referConfig";

// Admin Language
import Language from "../pages/admin/home/settings/language";
import AddLanguage from "../pages/admin/home/settings/language/add";
import EditLanguage from "../pages/admin/home/settings/language/edit";
import TranslateLanguage from "../pages/admin/home/settings/language/translate";

// Users
import Users from "../pages/admin/home/users";
import AddUser from "../pages/admin/home/users/add";
import EditUser from "../pages/admin/home/users/edit";
import UserView from "../pages/admin/home/users/view";
import UnregisteredUsers from "../pages/admin/home/users/unregistered";
import TransactionUsers from "../pages/admin/home/users/transactions";

// Setup
// Vechicle Category
import VechicleCategory from "../pages/admin/home/setup/vechicleCategory";
import AddVechicleCategory from "../pages/admin/home/setup/vechicleCategory/add";
import EditVechicleCategory from "../pages/admin/home/setup/vechicleCategory/edit";
import TranslateVechicleCategory from "../pages/admin/home/setup/vechicleCategory/translate";
// City Commision
import CityCommision from "../pages/admin/home/setup/cityCommision";
import EditCityCommision from "../pages/admin/home/setup/cityCommision/edit/index.jsx";
import AddCityCommision from "../pages/admin/home/setup/cityCommision/add/";
import FareCityCommision from "../pages/admin/home/setup/cityCommision/fare/";
import ViewCityCommision from "../pages/admin/home/setup/cityCommision/view/";
// Documents
import Documents from "../pages/admin/home/setup/documents";
import AddDocuments from "../pages/admin/home/setup/documents/add";
import EditDocuments from "../pages/admin/home/setup/documents/edit";
// Cancellation Reasons
import CancellationReasons from "../pages/admin/home/setup/cancellationReasons";
import AddCancellationReasons from "../pages/admin/home/setup/cancellationReasons/add";
import EditCancellationReasons from "../pages/admin/home/setup/cancellationReasons/edit";
// Verify Documents List
import VerificationDocuments from "../pages/admin/home/setup/verificationDocuments";
import AddVerificationDocuments from "../pages/admin/home/setup/verificationDocuments/add";
import EditVerificationDocuments from "../pages/admin/home/setup/verificationDocuments/edit";
// Email Templates
import EmailTemplates from "../pages/admin/home/setup/emailTemplates";
import EmailTemplatesAdd from "../pages/admin/home/setup/emailTemplates/add";
import EmailTemplatesEdit from "../pages/admin/home/setup/emailTemplates/edit";
// Notification Templates
import NotificationTemplates from "../pages/admin/home/setup/notifications";
import NotificationTemplatesAdd from "../pages/admin/home/setup/notifications/add";
import NotificationTemplatesEdit from "../pages/admin/home/setup/notifications/edit";
import SendPromotions from "../pages/admin/home/setup/notifications/promotions";
// Toll
import Toll from "../pages/admin/home/setup/toll";
import TollAdd from "../pages/admin/home/setup/toll/add";
import TollEdit from "../pages/admin/home/setup/toll/edit";
// Popular Places
import PopularPlaces from "../pages/admin/home/setup/popularPlaces";
import PopularPlacesAdd from "../pages/admin/home/setup/popularPlaces/add";
import PopularPlacesEdit from "../pages/admin/home/setup/popularPlaces/edit";
// Coupons
import Coupons from "../pages/admin/home/setup/coupons";
import CouponsAdd from "../pages/admin/home/setup/coupons/add";
import CouponsEdit from "../pages/admin/home/setup/coupons/edit";
import CouponRides from "../pages/admin/home/setup/coupons/bookings";
// Refers
import UserRefers from "../pages/admin/home/setup/referEarn/user";
import UserRefersJoiners from "../pages/admin/home/setup/referEarn/user/referCode";
import ProfessionalRefers from "../pages/admin/home/setup/referEarn/professional";
import ProfessionalRefersJoiners from "../pages/admin/home/setup/referEarn/professional/referCode";
// Airports
import Airports from "../pages/admin/home/setup/airports";
import AirportsAdd from "../pages/admin/home/setup/airports/add";
import AirportsEdit from "../pages/admin/home/setup/airports/edit";

import AirportStops from "../pages/admin/home/setup/airports/stops";
import AirportStopsAdd from "../pages/admin/home/setup/airports/stops/add";
import AirportStopsEdit from "../pages/admin/home/setup/airports/stops/edit";

//ASSET
// Earnings
import Earnings from "../pages/admin/home/asset/earnings";
import EarningsDetails from "../pages/admin/home/asset/earnings/details";
// Payments
import Payments from "../pages/admin/home/asset/payments";
import EditPayments from "../pages/admin/home/asset/payments/edit";
// WALLET
import Wallets from "../pages/admin/home/asset/wallet";
import Refunds from "../pages/admin/home/asset/refund";
import Withdraw from "../pages/admin/home/asset/withdraw";
import TransactionView from "../pages/admin/home/asset/transactions/view";
// Amount In Wallet
import UserAmountInWallet from "../pages/admin/home/asset/amountInWallet/user";
import ProfessionalAmountInWallet from "../pages/admin/home/asset/amountInWallet/professional";
// Due Freeze Amount
import DueFreeze from "../pages/admin/home/asset/dueFreezeAmount";

// SECURITY
// Services
import Services from "../pages/admin/home/security/services";
// Subscriptions
import Subscriptions from "../pages/admin/home/security/subscriptions";
import AddSubscriptions from "../pages/admin/home/security/subscriptions/add";
import EditSubscriptions from "../pages/admin/home/security/subscriptions/edit";
//Requests
import NewRequests from "../pages/admin/home/security/newRequests";
import ClosedRequests from "../pages/admin/home/security/closedRequests";
import MyRequests from "../pages/admin/home/security/myRequests";
import NewRequestsView from "../pages/admin/home/security/newRequests/view";

// Drivers
import Drivers from "../pages/admin/home/drivers";
import AddDrivers from "../pages/admin/home/drivers/add";
import EditDrivers from "../pages/admin/home/drivers/edit";
import ViewDrivers from "../pages/admin/home/drivers/view";
import BankDrivers from "../pages/admin/home/drivers/bank";
import UnverifiedDrivers from "../pages/admin/home/drivers/unverified";
import VerifyDriver from "../pages/admin/home/drivers/unverified/verify";
import UnregisteredDrivers from "../pages/admin/home/drivers/unregistered";
import UploadedDrivers from "../pages/admin/home/drivers/uploaded";
import TransactionDrivers from "../pages/admin/home/drivers/transactions";

// Vehicles
import Vehicles from "../pages/admin/home/vehicles";
import AddVehicles from "../pages/admin/home/vehicles/add";
import ViewVehicles from "../pages/admin/home/vehicles/view";
import EditVehicles from "../pages/admin/home/vehicles/edit";

// CORPORATE
// CORPORATE LIST
import Corporate from "../pages/admin/home/corporate/corporate";
import AddCorporate from "../pages/admin/home/corporate/corporate/add";
import EditCorporate from "../pages/admin/home/corporate/corporate/edit";
import CorporatePrivileges from "../pages/admin/home/corporate/corporate/privileges";
import RidesCorporateSingle from "../pages/admin/home/corporate/corporate/rides";
import CorporateBillings from "../pages/admin/home/corporate/corporate/billings";
import ViewCorporate from "../pages/admin/home/corporate/corporate/view";

// ADMINS
// ROLE
import Roles from "../pages/admin/home/admin/role";
import AddRoles from "../pages/admin/home/admin/role/add";
import EditRoles from "../pages/admin/home/admin/role/edit";
// Hubs
import Hubs from "../pages/admin/home/admin/hubs";
import AddHubs from "../pages/admin/home/admin/hubs/add";
import EditHubs from "../pages/admin/home/admin/hubs/edit";
// Hubs
import HubsEmployees from "../pages/admin/home/admin/hubs/employees";
import AddHubsEmployees from "../pages/admin/home/admin/hubs/employees/add";
import EditHubsEmployees from "../pages/admin/home/admin/hubs/employees/edit";
import HubsPrivileges from "../pages/admin/home/admin/hubs/employees/privilege";
// Operators
import Operators from "../pages/admin/home/admin/operators";
import AddOperators from "../pages/admin/home/admin/operators/add";
import EditOperators from "../pages/admin/home/admin/operators/edit";
// Response Office
import ResponseOffice from "../pages/admin/home/admin/responseOffice";
import AddResponseOffice from "../pages/admin/home/admin/responseOffice/add";
import EditResponseOffice from "../pages/admin/home/admin/responseOffice/edit";
// Response Officers
// import ResponseOfficers from "../pages/admin/home/admin/responseOffice/officers";
// import AddResponseOfficers from "../pages/admin/home/admin/responseOffice/officers/add";
// import EditResponseOfficers from "../pages/admin/home/admin/responseOffice/officers/edit";
import ResponseOfficePrivileges from "../pages/admin/home/admin/responseOffice/officers/privilege";

// DASHBOARD
import Dashboard from "../pages/admin/home/dashboard";

// RIDES
import NewRide from "../pages/admin/home/rides/new";
import ScheduleRide from "../pages/admin/home/rides/scheduled";
import OngoingRide from "../pages/admin/home/rides/ongoing";
import IssueRide from "../pages/admin/home/rides/issue";
import ExpiredRide from "../pages/admin/home/rides/expired";
import CompletedRide from "../pages/admin/home/rides/completed";
import UserCancelRide from "../pages/admin/home/rides/userCancelled";
import DriverCancelRide from "../pages/admin/home/rides/driverCancelled";
import ViewRide from "../pages/admin/home/rides/view";
import SearchRide from "../pages/admin/home/rides/search";
import GuestRide from "../pages/admin/home/rides/guests";
import MeterRide from "../pages/admin/home/rides/meter";
import CorporateRide from "../pages/admin/home/rides/corporate";

import UserRide from "../pages/admin/home/rides/users";
import DriverRide from "../pages/admin/home/rides/drivers";

import AssignDriver from "../pages/admin/home/rides/scheduled/assignDriver";

// RESPONSE OFFICERS ALL
import ResponseOfficersAll from "../pages/admin/home/officers";
import AddResponseOfficersAll from "../pages/admin/home/officers/add";
import EditResponseOfficersEdit from "../pages/admin/home/officers/edit";

// REPORTS
import Feedback from "../pages/admin/home/reports/feedback";
import FeedbackView from "../pages/admin/home/reports/feedback/view";

// Admin Hook
import useAdmin from "../hooks/useAdmin";
import ErrorPage from "../components/common/ErrorPage";

// Admin Auth Guard
const AdminGuardedRoute = ({ path, component }) => {
  const params = new URLSearchParams(window.location.search);
  const location = useLocation();
  const { isAdmin } = useAdmin();
  params !== null ? Cookies.set("loc", location.pathname + `?${params}`, { expires: 10 }) : Cookies.set("loc", location.pathname, { expires: 10 });
  return isAdmin ? <Route path={path} component={component} /> : <Redirect from={location.pathname} to={NavLinks.ADMIN_VALIDATE} />;
};

// Admin Privilege Guard
const AdminPriRoute = ({ path, component: Component, privilege = "ADMIN.ROLES.ADD" }) => {
  const location = useLocation();
  const { admin } = useAdmin();
  const splittedPrivilege = privilege.split(".");
  return admin.userType === "ADMIN" ||
    admin.userType === "DEVELOPER" ||
    (admin.userType === "COORPERATEOFFICE" && (path.includes(NavLinks.ADMIN_DISPATCH) || path.includes(NavLinks.ADMIN_CORPORATE_CORPORATE_BILLINGS) || path.includes(NavLinks.ADMIN_CORPORATE_CORPORATE_RIDES) || path.includes(NavLinks.ADMIN_RIDES_VIEW))) ||
    (admin.userType !== "COORPERATEOFFICE" && admin.privileges[splittedPrivilege[0]] && admin.privileges[splittedPrivilege[0]][splittedPrivilege[1]] && admin.privileges[splittedPrivilege[0]][splittedPrivilege[1]][splittedPrivilege[2]] === true) ? (
    <Route
      path={path}
      render={(props) => (
        <ErrorBoundary FallbackComponent={ErrorPage}>
          <Component {...props} />
        </ErrorBoundary>
      )}
    />
  ) : (
    <Redirect from={location.pathname} to={NavLinks.ADMIN_HOME} />
  );
};

export const MainRoutes = () => {
  return (
    <>
      <Switch>
        <Route path={NavLinks.ADMIN_LOGIN} component={Admin} />
        <Route path={NavLinks.NOT_FOUND} component={NotFound} />
        <Redirect from={NavLinks.HOME} to={NavLinks.ADMIN_HOME} />
      </Switch>
    </>
  );
};

// Admin routes
export const AdminRoutes = ({ match }) => {
  return (
    <>
      <Switch>
        <AdminGuardedRoute path={NavLinks.ADMIN_HOME} component={Home} />
        <Route exact path={NavLinks.ADMIN_VALIDATE} component={AdminValidate} />
        <Route exact path={NavLinks.ADMIN_LOGIN} component={LogIn} />
      </Switch>
    </>
  );
};

// Admin home routes
export const AdminHomeRoutes = () => {
  return (
    <>
      <Switch>
        {/* CORPORATE */}
        <AdminPriRoute privilege="CORPORATE.CORPORATE.EDIT" path={NavLinks.ADMIN_CORPORATE_CORPORATE_RIDES + "/:id"} component={RidesCorporateSingle} />
        <AdminPriRoute privilege="CORPORATE.CORPORATE.EDIT" path={NavLinks.ADMIN_CORPORATE_CORPORATE_BILLINGS + "/:id"} component={CorporateBillings} />
        <AdminPriRoute privilege="CORPORATE.CORPORATE.VIEW" path={NavLinks.ADMIN_CORPORATE_CORPORATE_VIEW + "/:id"} component={ViewCorporate} />
        <AdminPriRoute privilege="CORPORATE.CORPORATE.EDIT" path={NavLinks.ADMIN_CORPORATE_CORPORATE_PRIVILEGES} component={CorporatePrivileges} />
        <AdminPriRoute privilege="CORPORATE.CORPORATE.ADD" path={NavLinks.ADMIN_CORPORATE_CORPORATE_ADD} component={AddCorporate} />
        <AdminPriRoute privilege="CORPORATE.CORPORATE.EDIT" path={NavLinks.ADMIN_CORPORATE_CORPORATE_EDIT + "/:id"} component={EditCorporate} />
        <AdminPriRoute privilege="CORPORATE.CORPORATE.VIEW" path={NavLinks.ADMIN_CORPORATE_CORPORATE_LIST} component={Corporate} />

        {/* ADmins  */}
        {/* Roles  */}
        <AdminPriRoute privilege="ADMIN.ROLES.ADD" path={NavLinks.ADMIN_ADMIN_ROLE_ADD} component={AddRoles} />
        <AdminPriRoute privilege="ADMIN.ROLES.EDIT" path={NavLinks.ADMIN_ADMIN_ROLE_EDIT + "/:id"} component={EditRoles} />
        <AdminPriRoute privilege="ADMIN.ROLES.VIEW" path={NavLinks.ADMIN_ADMIN_ROLE_LIST} component={Roles} />

        {/* Operators  */}
        <AdminPriRoute privilege="ADMIN.OPERATORS.ADD" path={NavLinks.ADMIN_ADMIN_OPERATORS_ADD} component={AddOperators} />
        <AdminPriRoute privilege="ADMIN.OPERATORS.EDIT" path={NavLinks.ADMIN_ADMIN_OPERATORS_EDIT + "/:id"} component={EditOperators} />
        <AdminPriRoute privilege="ADMIN.OPERATORS.VIEW" path={NavLinks.ADMIN_ADMIN_OPERATORS_LIST} component={Operators} />

        {/* Hubs Employees  */}
        <AdminPriRoute privilege="ADMIN.HUBS_EMPLOYEES.EDIT" path={NavLinks.ADMIN_ADMIN_HUBS_LIST + ":hubID" + NavLinks.ADMIN_ADMIN_HUBS_EMPLOYESS_EDIT + "/:id"} component={EditHubsEmployees} />
        <AdminPriRoute privilege="ADMIN.HUBS_EMPLOYEES.ADD" path={NavLinks.ADMIN_ADMIN_HUBS_LIST + ":hubID" + NavLinks.ADMIN_ADMIN_HUBS_EMPLOYESS_ADD} component={AddHubsEmployees} />
        <AdminPriRoute privilege="ADMIN.HUBS_EMPLOYEES.VIEW" path={NavLinks.ADMIN_ADMIN_HUBS_LIST + ":hubID" + NavLinks.ADMIN_ADMIN_HUBS_EMPLOYESS_LIST} component={HubsEmployees} />

        {/* Hubs  */}
        <AdminPriRoute privilege="ADMIN.HUBS.EDIT" path={NavLinks.ADMIN_ADMIN_HUBS_PRIVILEGES} component={HubsPrivileges} />
        <AdminPriRoute privilege="ADMIN.HUBS.ADD" path={NavLinks.ADMIN_ADMIN_HUBS_ADD} component={AddHubs} />
        <AdminPriRoute privilege="ADMIN.HUBS.EDIT" path={NavLinks.ADMIN_ADMIN_HUBS_EDIT + "/:id"} component={EditHubs} />
        <AdminPriRoute privilege="ADMIN.HUBS.VIEW" path={NavLinks.ADMIN_ADMIN_HUBS_LIST} component={Hubs} />

        {/* Response Officers  */}
        {/* <AdminPriRoute privilege="ADMIN.HUBS_EMPLOYEES.EDIT" path={NavLinks.ADMIN_ADMIN_RESPONSE_OFFICE_LIST + ":hubID" + NavLinks.ADMIN_ADMIN_RESPONSE_OFFICERS_EDIT + "/:id"} component={EditResponseOfficers} />
        <AdminPriRoute privilege="ADMIN.HUBS_EMPLOYEES.ADD" path={NavLinks.ADMIN_ADMIN_RESPONSE_OFFICE_LIST + ":hubID" + NavLinks.ADMIN_ADMIN_RESPONSE_OFFICERS_ADD} component={AddResponseOfficers} />
        <AdminPriRoute privilege="ADMIN.HUBS_EMPLOYEES.VIEW" path={NavLinks.ADMIN_ADMIN_RESPONSE_OFFICE_LIST + ":hubID" + NavLinks.ADMIN_ADMIN_RESPONSE_OFFICERS_LIST} component={ResponseOfficers} /> */}

        {/* Response Office  */}
        <AdminPriRoute privilege="ADMIN.EMERGENCY_RESPONSE_OFFICES.EDIT" path={NavLinks.ADMIN_ADMIN_RESPONSE_OFFICE_PRIVILEGES} component={ResponseOfficePrivileges} />
        <AdminPriRoute privilege="ADMIN.EMERGENCY_RESPONSE_OFFICES.ADD" path={NavLinks.ADMIN_ADMIN_RESPONSE_OFFICE_ADD} component={AddResponseOffice} />
        <AdminPriRoute privilege="ADMIN.EMERGENCY_RESPONSE_OFFICES.EDIT" path={NavLinks.ADMIN_ADMIN_RESPONSE_OFFICE_EDIT + "/:id"} component={EditResponseOffice} />
        <AdminPriRoute privilege="ADMIN.EMERGENCY_RESPONSE_OFFICES.VIEW" path={NavLinks.ADMIN_ADMIN_RESPONSE_OFFICE_LIST} component={ResponseOffice} />

        {/* Vehicles  */}
        <Route path={NavLinks.ADMIN_DRIVERS_LIST + ":driverID" + NavLinks.ADMIN_VEHICLES_ADD} component={AddVehicles} />
        <Route path={NavLinks.ADMIN_DRIVERS_LIST + ":driverID" + NavLinks.ADMIN_VEHICLES_EDIT + "/:vehicleId"} component={EditVehicles} />
        <Route path={NavLinks.ADMIN_DRIVERS_LIST + ":driverID" + NavLinks.ADMIN_VEHICLES_VIEW + "/:vehicleId"} component={ViewVehicles} />
        <Route path={NavLinks.ADMIN_DRIVERS_LIST + ":driverID" + NavLinks.ADMIN_VEHICLES_LIST} component={Vehicles} />

        {/* Drivers  */}
        <AdminPriRoute privilege="DRIVERS.DRIVERS_LIST.EDIT" path={NavLinks.ADMIN_DRIVERS_BANK + "/:id"} component={BankDrivers} />
        <AdminPriRoute privilege="DRIVERS.DRIVERS_LIST.VIEW" path={NavLinks.ADMIN_DRIVERS_VIEW + "/:id"} component={ViewDrivers} />
        <AdminPriRoute privilege="DRIVERS.DRIVERS_LIST.EDIT" path={NavLinks.ADMIN_DRIVERS_EDIT + "/:id"} component={EditDrivers} />
        <AdminPriRoute privilege="DRIVERS.DRIVERS_LIST.VIEW" path={NavLinks.ADMIN_DRIVERS_TRANSACTION + "/:id"} component={TransactionDrivers} />
        <AdminPriRoute privilege="DRIVERS.UNVERIFIED_DRIVERS.EDIT" path={NavLinks.ADMIN_DRIVERS_UNVERIFIED_LIST_VERIFY + "/:id"} component={VerifyDriver} />
        <AdminPriRoute privilege="DRIVERS.DRIVERS_LIST.ADD" path={NavLinks.ADMIN_DRIVERS_ADD} component={AddDrivers} />
        <AdminPriRoute privilege="DRIVERS.UNVERIFIED_DRIVERS.VIEW" path={NavLinks.ADMIN_DRIVERS_UNVERIFIED_LIST} component={UnverifiedDrivers} />
        <AdminPriRoute privilege="DRIVERS.UNVERIFIED_DRIVERS.VIEW" path={NavLinks.ADMIN_DRIVERS_UPLOADED_LIST} component={UploadedDrivers} />
        <AdminPriRoute privilege="DRIVERS.UNREGISTERED_DRIVERS.VIEW" path={NavLinks.ADMIN_DRIVERS_UNREGISTERTED_LIST} component={UnregisteredDrivers} />
        <AdminPriRoute privilege="DRIVERS.DRIVERS_LIST.VIEW" path={NavLinks.ADMIN_DRIVERS_LIST} component={Drivers} />

        {/* RIDES */}
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_VIEW + "/:id"} component={ViewRide} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_USERS + "/:id"} component={UserRide} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_DRIVERS + "/:id"} component={DriverRide} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_NEW} component={NewRide} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_SEARCH} component={SearchRide} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_SCHEDULED_ASSIGN_DRIVER + "/:id"} component={AssignDriver} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_SCHEDULED} component={ScheduleRide} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_ONGOING} component={OngoingRide} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_COMPLETED} component={CompletedRide} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_EXPIRED} component={ExpiredRide} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDESD_DRIVER_CANCEL} component={DriverCancelRide} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_USER_CANCEL} component={UserCancelRide} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_ISSUE} component={IssueRide} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_GUESTS} component={GuestRide} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_METERS} component={MeterRide} />
        <AdminPriRoute privilege="RIDES.RIDES_LIST.VIEW" path={NavLinks.ADMIN_RIDES_CORPORATE} component={CorporateRide} />

        {/* Subscriptions  */}
        <AdminPriRoute privilege="SECURITY.SERVICES.EDIT" path={NavLinks.ADMIN_SECURITY_SERVICES} component={Services} />

        <AdminPriRoute privilege="SECURITY.SUBSCRIPTIONS.EDIT" path={NavLinks.ADMIN_SECURITY_SUBSCRIPTIONS_EDIT + "/:id"} component={EditSubscriptions} />
        <AdminPriRoute privilege="SECURITY.SUBSCRIPTIONS.ADD" path={NavLinks.ADMIN_SECURITY_SUBSCRIPTIONS_ADD} component={AddSubscriptions} />
        <AdminPriRoute privilege="SECURITY.SUBSCRIPTIONS.VIEW" path={NavLinks.ADMIN_SECURITY_SUBSCRIPTIONS} component={Subscriptions} />
        {/* Requests  */}
        <AdminPriRoute privilege="SECURITY.NEW_REQUESTS.VIEW" path={NavLinks.ADMIN_SECURITY_REQUESTS_VIEW + "/:id"} component={NewRequestsView} />
        <AdminPriRoute privilege="SECURITY.NEW_REQUESTS.VIEW" path={NavLinks.ADMIN_SECURITY_REQUESTS} component={NewRequests} />
        <AdminPriRoute privilege="SECURITY.CLOSED_REQUESTS.VIEW" path={NavLinks.ADMIN_SECURITY_CLOSED_REQUESTS} component={ClosedRequests} />
        <AdminPriRoute privilege="SECURITY.CLOSED_REQUESTS.VIEW" path={NavLinks.ADMIN_SECURITY_MY_REQUESTS} component={MyRequests} />

        {/* Setup  */}
        {/* Airports Stops */}
        <AdminPriRoute privilege="SETUP.AIRPORTS.VIEW" path={NavLinks.ADMIN_SETUP_AIRPORTS + ":airportID" + NavLinks.ADMIN_SETUP_AIRPORTS_STOPS_ADD} component={AirportStopsAdd} />
        <AdminPriRoute privilege="SETUP.AIRPORTS.EDIT" path={NavLinks.ADMIN_SETUP_AIRPORTS + ":airportID" + NavLinks.ADMIN_SETUP_AIRPORTS_STOPS_EDIT + "/:stopID"} component={AirportStopsEdit} />
        <AdminPriRoute privilege="SETUP.AIRPORTS.VIEW" path={NavLinks.ADMIN_SETUP_AIRPORTS + ":airportID" + NavLinks.ADMIN_SETUP_AIRPORTS_STOPS_LISTS} component={AirportStops} />
        {/* Airports */}
        <AdminPriRoute privilege="SETUP.AIRPORTS.EDIT" path={NavLinks.ADMIN_SETUP_AIRPORTS_EDIT + "/:id"} component={AirportsEdit} />
        <AdminPriRoute privilege="SETUP.AIRPORTS.ADD" path={NavLinks.ADMIN_SETUP_AIRPORTS_ADD} component={AirportsAdd} />
        <AdminPriRoute privilege="SETUP.AIRPORTS.VIEW" path={NavLinks.ADMIN_SETUP_AIRPORTS} component={Airports} />
        {/* Popular Places */}
        {/* Coupons */}
        <AdminPriRoute privilege="SETUP.COUPONS.EDIT" path={NavLinks.ADMIN_SETUP_COUPONS_EDIT + "/:id"} component={CouponsEdit} />
        <AdminPriRoute privilege="SETUP.COUPONS.EDIT" path={NavLinks.ADMIN_SETUP_COUPONS_BOOKINGS + "/:id"} component={CouponRides} />
        <AdminPriRoute privilege="SETUP.COUPONS.ADD" path={NavLinks.ADMIN_SETUP_COUPONS_ADD} component={CouponsAdd} />
        <AdminPriRoute privilege="SETUP.COUPONS.VIEW" path={NavLinks.ADMIN_SETUP_COUPONS} component={Coupons} />
        <AdminPriRoute privilege="SETUP.POPULAR_PLACES.EDIT" path={NavLinks.ADMIN_SETUP_POPULAR_PLACES_EDIT + "/:id"} component={PopularPlacesEdit} />
        <AdminPriRoute privilege="SETUP.POPULAR_PLACES.ADD" path={NavLinks.ADMIN_SETUP_POPULAR_PLACES_ADD} component={PopularPlacesAdd} />
        <AdminPriRoute privilege="SETUP.POPULAR_PLACES.VIEW" path={NavLinks.ADMIN_SETUP_POPULAR_PLACES} component={PopularPlaces} />
        {/* Toll */}
        <AdminPriRoute privilege="SETUP.TOLLS.EDIT" path={NavLinks.ADMIN_SETUP_TOLLS_EDIT + "/:id"} component={TollEdit} />
        <AdminPriRoute privilege="SETUP.TOLLS.ADD" path={NavLinks.ADMIN_SETUP_TOLLS_ADD} component={TollAdd} />
        <AdminPriRoute privilege="SETUP.TOLLS.VIEW" path={NavLinks.ADMIN_SETUP_TOLLS} component={Toll} />

        {/* Email Templates  */}
        <AdminPriRoute privilege="SETUP.EMAIL_TEMPLATES.EDIT" path={NavLinks.ADMIN_SETUP_NOTIFICATIONS_SEND_PROMOTIONS} component={SendPromotions} />

        <AdminPriRoute privilege="SETUP.EMAIL_TEMPLATES.EDIT" path={NavLinks.ADMIN_SETUP_EMAIL_TEMPLATES_EDIT + "/:id"} component={EmailTemplatesEdit} />
        <AdminPriRoute privilege="SETUP.EMAIL_TEMPLATES.ADD" path={NavLinks.ADMIN_SETUP_EMAIL_TEMPLATES_ADD} component={EmailTemplatesAdd} />
        <AdminPriRoute privilege="SETUP.EMAIL_TEMPLATES.VIEW" path={NavLinks.ADMIN_SETUP_EMAIL_TEMPLATES} component={EmailTemplates} />

        {/* Notificaion Templates  */}
        <AdminPriRoute privilege="SETUP.NOTIFICATION_TEMPLATES.EDIT" path={NavLinks.ADMIN_SETUP_NOTIFICATIONS_EDIT + "/:type" + "/:id"} component={NotificationTemplatesEdit} />
        <AdminPriRoute privilege="SETUP.NOTIFICATION_TEMPLATES.ADD" path={NavLinks.ADMIN_SETUP_NOTIFICATIONS_ADD} component={NotificationTemplatesAdd} />
        <AdminPriRoute privilege="SETUP.NOTIFICATION_TEMPLATES.VIEW" path={NavLinks.ADMIN_SETUP_NOTIFICATIONS} component={NotificationTemplates} />

        {/* Cancellation Reasons  */}
        <AdminPriRoute privilege="SETUP.CANCELLATION_REASONS.EDIT" path={NavLinks.ADMIN_SETUP_CANCELLATION_REASONS_EDIT + "/:id"} component={EditCancellationReasons} />
        <AdminPriRoute privilege="SETUP.CANCELLATION_REASONS.ADD" path={NavLinks.ADMIN_SETUP_CANCELLATION_REASONS_ADD} component={AddCancellationReasons} />
        <AdminPriRoute privilege="SETUP.CANCELLATION_REASONS.VIEW" path={NavLinks.ADMIN_SETUP_CANCELLATION_REASONS} component={CancellationReasons} />

        {/* Docuents  */}
        <AdminPriRoute privilege="SETUP.DOCUMENTS.EDIT" path={NavLinks.ADMIN_SETUP_DOCUMENTS_EDIT + "/:id"} component={EditDocuments} />
        <AdminPriRoute privilege="SETUP.DOCUMENTS.ADD" path={NavLinks.ADMIN_SETUP_DOCUMENTS_ADD} component={AddDocuments} />
        <AdminPriRoute privilege="SETUP.DOCUMENTS.VIEW" path={NavLinks.ADMIN_SETUP_DOCUMENTS} component={Documents} />
        {/* Verification Documents  */}
        <AdminPriRoute privilege="SETUP.VERIFICATION_DOCUMENTS.VIEW" path={NavLinks.ADMIN_SETUP_VERIFICATION_DOCUMENTS_EDIT + "/:id"} component={EditVerificationDocuments} />
        <AdminPriRoute privilege="SETUP.VERIFICATION_DOCUMENTS.VIEW" path={NavLinks.ADMIN_SETUP_VERIFICATION_DOCUMENTS_ADD} component={AddVerificationDocuments} />
        <AdminPriRoute privilege="SETUP.VERIFICATION_DOCUMENTS.VIEW" path={NavLinks.ADMIN_SETUP_VERIFICATION_DOCUMENTS} component={VerificationDocuments} />
        {/* City Commision  */}
        <AdminPriRoute privilege="SETUP.CITY_COMMISIONS.EDIT" path={NavLinks.ADMIN_SETUP_CITY_COMMISION_FARE + "/:cityId/:categoryId"} component={FareCityCommision} />
        <AdminPriRoute privilege="SETUP.CITY_COMMISIONS.EDIT" path={NavLinks.ADMIN_SETUP_CITY_COMMISION_EDIT + "/:id"} component={EditCityCommision} />
        <AdminPriRoute privilege="SETUP.CITY_COMMISIONS.EDIT" path={NavLinks.ADMIN_SETUP_CITY_COMMISION_VIEW + "/:id"} component={ViewCityCommision} />
        <AdminPriRoute privilege="SETUP.CITY_COMMISIONS.ADD" path={NavLinks.ADMIN_SETUP_CITY_COMMISION_ADD} component={AddCityCommision} />
        <AdminPriRoute privilege="SETUP.CITY_COMMISIONS.VIEW" path={NavLinks.ADMIN_SETUP_CITY_COMMISION} component={CityCommision} />

        {/* Vechicle Category  */}
        <AdminPriRoute privilege="SETUP.VEHICLE_CATEGORYS.EDIT" path={NavLinks.ADMIN_SETUP_VECHICLE_CATEGORY_TRANSLATE + "/:id"} component={TranslateVechicleCategory} />
        <AdminPriRoute privilege="SETUP.VEHICLE_CATEGORYS.EDIT" path={NavLinks.ADMIN_SETUP_VECHICLE_CATEGORY_EDIT + "/:id"} component={EditVechicleCategory} />
        <AdminPriRoute privilege="SETUP.VEHICLE_CATEGORYS.ADD" path={NavLinks.ADMIN_SETUP_VECHICLE_CATEGORY_ADD} component={AddVechicleCategory} />
        <AdminPriRoute privilege="SETUP.VEHICLE_CATEGORYS.VIEW" path={NavLinks.ADMIN_SETUP_VECHICLE_CATEGORY} component={VechicleCategory} />

        <AdminPriRoute privilege="SETUP.REFER_EARN.VIEW" path={NavLinks.ADMIN_SETUP_REFER_PROFESSIONAL_JOINERS_LIST + "/:id"} component={ProfessionalRefersJoiners} />
        <AdminPriRoute privilege="SETUP.REFER_EARN.VIEW" path={NavLinks.ADMIN_SETUP_REFER_PROFESSIONAL_LIST} component={ProfessionalRefers} />
        <AdminPriRoute privilege="SETUP.REFER_EARN.VIEW" path={NavLinks.ADMIN_SETUP_REFER_USER_JOINERS_LIST + "/:id"} component={UserRefersJoiners} />
        <AdminPriRoute privilege="SETUP.REFER_EARN.VIEW" path={NavLinks.ADMIN_SETUP_REFER_USER_LIST} component={UserRefers} />

        {/* Users  */}
        <AdminPriRoute privilege="USERS.UNREGISTERED_USER.VIEW" path={NavLinks.ADMIN_UNREGISTERED_USERS} component={UnregisteredUsers} />
        <AdminPriRoute privilege="USERS.USERS_LIST.ADD" path={NavLinks.ADMIN_USERS_ADD} component={AddUser} />
        <AdminPriRoute privilege="USERS.USERS_LIST.VIEW" path={NavLinks.ADMIN_USERS_VIEW_EACH + "/:id"} component={UserView} />
        <AdminPriRoute privilege="USERS.USERS_LIST.EDIT" path={NavLinks.ADMIN_USERS_EDIT + "/:id"} component={EditUser} />
        <AdminPriRoute privilege="USERS.USERS_LIST.VIEW" path={NavLinks.ADMIN_USERS_TRANSACTION + "/:id"} component={TransactionUsers} />
        <AdminPriRoute privilege="USERS.USERS_LIST.VIEW" path={NavLinks.ADMIN_USERS_VIEW} component={Users} />

        {/* Response Officers List  */}
        <AdminPriRoute privilege="RESPONSE_OFFICERS.RESPONSE_OFFICERS_LIST.ADD" path={NavLinks.ADMIN_RESPONSE_OFFICERS_ADD} component={AddResponseOfficersAll} />
        <AdminPriRoute privilege="RESPONSE_OFFICERS.RESPONSE_OFFICERS_LIST.EDIT" path={NavLinks.ADMIN_RESPONSE_OFFICERS_EDIT + "/:id"} component={EditResponseOfficersEdit} />
        <AdminPriRoute privilege="RESPONSE_OFFICERS.RESPONSE_OFFICERS_LIST.VIEW" path={NavLinks.ADMIN_RESPONSE_OFFICERS_LIST} component={ResponseOfficersAll} />

        {/* EARNINGS */}
        <AdminPriRoute privilege="ASSET.SITE_EARNINGS.VIEW" path={NavLinks.ADMIN_ASSET_EARNINGS_DETAILS} component={EarningsDetails} />
        <AdminPriRoute privilege="ASSET.SITE_EARNINGS.VIEW" path={NavLinks.ADMIN_ASSET_EARNINGS} component={Earnings} />
        {/* Payments  */}
        <AdminPriRoute privilege="ASSET.PAYMENTS.EDIT" path={NavLinks.ADMIN_SETUP_PAYMENTS_EDIT + "/:id"} component={EditPayments} />
        <AdminPriRoute privilege="ASSET.PAYMENTS.VIEW" path={NavLinks.ADMIN_SETUP_PAYMENTS} component={Payments} />
        {/* Amount in Wallet  */}
        <AdminPriRoute privilege="ASSET.WALLETS.VIEW" path={NavLinks.ADMIN_ASSET_WALLET_AMOUNT_USER} component={UserAmountInWallet} />
        <AdminPriRoute privilege="ASSET.WALLETS.VIEW" path={NavLinks.ADMIN_ASSET_WALLET_AMOUNT_PROFESSIONAL} component={ProfessionalAmountInWallet} />
        {/* WALLET */}
        <AdminPriRoute privilege="ASSET.WALLETS.VIEW" path={NavLinks.ADMIN_ASSET_WALLET} component={Wallets} />
        {/* REFUND */}
        <AdminPriRoute privilege="ASSET.WALLETS.VIEW" path={NavLinks.ADMIN_ASSET_REFUND} component={Refunds} />
        {/* WITHDRAW */}
        <AdminPriRoute privilege="ASSET.WALLETS.VIEW" path={NavLinks.ADMIN_ASSET_WITHDRAW} component={Withdraw} />
        {/* TRANSACTION VIEW */}
        <AdminPriRoute privilege="ASSET.WALLETS.VIEW" path={NavLinks.ADMIN_ASSET_TRANSACTIONS_VIEW + "/:id"} component={TransactionView} />
        {/* DUE FREEZED AMOUNT  */}
        <AdminPriRoute privilege="ASSET.DUE_FREEZED.VIEW" path={NavLinks.ADMIN_ASSET_DUE_FREEZE_AMOUNT} component={DueFreeze} />

        {/* Settings  */}

        {/* <AdminPriRoute privilege="SETTINGS.USERS_VIEW.VIEW" path={NavLinks.ADMIN_CHANGE_PASSWORD} component={ChangePassword} /> */}
        <Route path={NavLinks.ADMIN_PROFILE} component={Profile} />
        <AdminPriRoute privilege="SETTINGS.SMTP.EDIT" path={NavLinks.ADMIN_SMTP} component={SMTP} />
        <AdminPriRoute privilege="SETTINGS.GENERAL.EDIT" path={NavLinks.ADMIN_GENERAL} component={General} />
        <AdminPriRoute privilege="SETTINGS.SMS.EDIT" path={NavLinks.ADMIN_SMS} component={SMS} />
        <AdminPriRoute privilege="SETTINGS.REFER_EARN_CONFIG.EDIT" path={NavLinks.ADMIN_REFER_CONFIG} component={ReferConfig} />
        {/* Translate  */}
        <AdminPriRoute privilege="SETTINGS.LANGUAGE.EDIT" path={NavLinks.ADMIN_LANGUAGE_TRANSLATE + "/:id"} component={TranslateLanguage} />
        <AdminPriRoute privilege="SETTINGS.LANGUAGE.EDIT" path={NavLinks.ADMIN_LANGUAGE_EDIT + "/:id"} component={EditLanguage} />
        <AdminPriRoute privilege="SETTINGS.LANGUAGE.EDIT" path={NavLinks.ADMIN_LANGUAGE_ADD} component={AddLanguage} />
        <AdminPriRoute privilege="SETTINGS.LANGUAGE.ADD" path={NavLinks.ADMIN_LANGUAGE} component={Language} />

        {/* VIEWS  */}
        <AdminPriRoute privilege="OTHERS.SATELLITE_VIEW.VIEW" path={NavLinks.ADMIN_SATILLITE_VIEW} component={SatelliteView} />
        <AdminPriRoute privilege="OTHERS.EAGLE_VIEW.VIEW" path={NavLinks.ADMIN_EAGLE_VIEW} component={EagleView} />
        <AdminPriRoute privilege="OTHERS.BOOK_RIDE.VIEW" path={NavLinks.ADMIN_BOOK_RIDE_ACCEPT_DETAILS + "/:id"} component={BookRideDetails} />
        <AdminPriRoute privilege="OTHERS.BOOK_RIDE.VIEW" path={NavLinks.ADMIN_BOOK_RIDE} component={BookRide} />
        <AdminPriRoute privilege="OTHERS.DISPATCH.VIEW" path={NavLinks.ADMIN_DISPATCH} component={Dispatch} />

        {/* REPORTS  */}
        <AdminPriRoute privilege="REPORTS.FEEDBACK.VIEW" path={NavLinks.ADMIN_REPORTS_FEEDBACK_VIEW + "/:id"} component={FeedbackView} />
        <AdminPriRoute privilege="REPORTS.FEEDBACK.VIEW" path={NavLinks.ADMIN_REPORTS_FEEDBACK} component={Feedback} />
        {/* Dashboard  */}
        <Route path={NavLinks.ADMIN_HOME} component={Dashboard} />
      </Switch>
    </>
  );
};
