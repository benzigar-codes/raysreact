const utils = require("./utils");

const urls = {};
if (utils.mode === "localhost") {
  urls.HOST = "http://192.168.1.20:8080";
  urls.SOCKET_HOST_ADMIN = "http://192.168.1.20:5000/admin";
  urls.SOCKET_HOST_NOTIFICATIONS = "http://192.168.1.20:5000/notification";
}
if (utils.mode === "development") {
  urls.HOST = "https://api.zervx.com";
  urls.SOCKET_HOST_ADMIN = "https://socket.zervx.com/admin";
  urls.SOCKET_HOST_NOTIFICATIONS = "https://socket.zervx.com/notification";
}
if (utils.mode === "demo") {
  urls.HOST = "https://api.demo.zervx.com";
  urls.SOCKET_HOST_ADMIN = "https://socket.demo.zervx.com/admin";
  urls.SOCKET_HOST_NOTIFICATIONS = "https://socket.demo.zervx.com/notification";
}
if (utils.mode === "zayRide") {
  urls.HOST = "https://api.fastrack.site";
  urls.SOCKET_HOST_ADMIN = "https://socket.fastrack.site/admin";
  urls.SOCKET_HOST_NOTIFICATIONS = "https://socket.fastrack.site/notification";
}
if (utils.mode === "zayRide development") {
  urls.HOST = "https://api.development.fastrack.site";
  urls.SOCKET_HOST_ADMIN = "https://socket.development.fastrack.site/admin";
  urls.SOCKET_HOST_NOTIFICATIONS =
    "https://socket.development.fastrack.site/notification";
}
if (utils.mode === "zayRide kubernetes") {
  urls.HOST = "https://api.kube.fastrack.site";
  urls.SOCKET_HOST_ADMIN = "https://socket.kube.fastrack.site/admin";
  urls.SOCKET_HOST_NOTIFICATIONS = "https://socket.kube.fastrack.site/notification";
}
if (utils.mode === "trail") {
  urls.HOST = "https://api.trail.zervx.com";
  urls.SOCKET_HOST_ADMIN = "https://socket.trail.zervx.com/admin";
  urls.SOCKET_HOST_NOTIFICATIONS = "https://socket.trail.zervx.com/notification";
}
if (utils.mode === "quickzy") {
  urls.HOST = "https://api.quickzy.zervx.com";
  urls.SOCKET_HOST_ADMIN = "https://socket.quickzy.zervx.com/admin";
  urls.SOCKET_HOST_NOTIFICATIONS = "https://socket.quickzy.zervx.com/notification";
}
if (utils.mode === "pamworld") {
  urls.HOST = "https://api.server.pamworld.com";
  urls.SOCKET_HOST_ADMIN = "https://socket.server.pamworld.com/admin";
  urls.SOCKET_HOST_NOTIFICATIONS = "https://socket.server.pamworld.com/notification";
}
if (utils.mode === "starMov") {
  urls.HOST = "https://api.appstarmov.com";
  urls.SOCKET_HOST_ADMIN = "https://socket.appstarmov.com/admin";
  urls.SOCKET_HOST_NOTIFICATIONS = "https://socket.appstarmov.com/notification";
}
if (utils.mode === "jetride") {
  urls.HOST = "https://api1.jetrideapp.com";
  urls.SOCKET_HOST_ADMIN = "https://socket.jetrideapp.com/admin";
  urls.SOCKET_HOST_NOTIFICATIONS = "https://socket.jetrideapp.com/notification";
}

const data = {
  LIVE_HOST: "https://www.berarkrays.in",
  HOST: urls.HOST,
  SOCKET_HOST_ADMIN: urls.SOCKET_HOST_ADMIN,
  SOCKET_HOST_NOTIFICATIONS: urls.SOCKET_HOST_NOTIFICATIONS,
  ADMIN_LOGIN_IN_OTP: "/api/admin/loginWithOtp",
  ADMIN_LOG_IN_CONFIRM_OTP: "/api/admin/verifyOtp",
  ADMIN_VALIDATE: "/api/admin/common/loginValidation",
  ADMIN_PROFILE_GET: "/api/admin/getProfile",
  ADMIN_PROFILE_UPDATE: "/api/admin/updateProfile",
  ADMIN_ROLE_LIST: "/api/admin/getOperatorsRoleList",
  ADMIN_ROLE_UPDATE: "/api/admin/updateOperatorsRole",
  ADMIN_ROLE_READ: "/api/admin/getOperatorRole",
  ADMIN_ROLE_STATUS: "/api/admin/changeOperatorsRoleStatus",
  ADMIN_ROLE_LIST_ALL: "/api/admin/getAllOperatorsRoles",
  ADMIN_OPERATOR_LIST: "/api/admin/getOperatorsList",
  ADMIN_OPERATOR_LIST_ARCHIEVE: "/api/admin/getOperatorsArchiveList",
  ADMIN_OPERATOR_UPDATE: "/api/admin/updateOperator",
  ADMIN_OPERATOR_STATUS: "/api/admin/changeOperatorStatus",
  ADMIN_OPERATOR_READ: "/api/admin/getOperator",
  ADMIN_RESPONSE_OFFICE_LIST: "/api/admin/getResponseOfficeList",
  ADMIN_RESPONSE_OFFICE_ALL: "/api/admin/getAllResponseOffices",
  ADMIN_RESPONSE_OFFICE_READ: "/api/admin/getResponseOffice",
  ADMIN_RESPONSE_OFFICE_UPDATE: "/api/admin/updateResponseOffice",
  ADMIN_RESPONSE_OFFICE_STATUS: "/api/admin/changeResponseOfficeStatus",
  ADMIN_RESPONSE_OFFICE_PRIVILEGE_READ: "/api/admin/getResponseOfficePrivileges",
  ADMIN_RESPONSE_OFFICE_PRIVILEGE_UPDATE: "/api/admin/updateResponseOfficePrivileges",
  ADMIN_HUB_LIST: "/api/admin/getHubsList",
  ADMIN_HUB_UPDATE: "/api/admin/updateHubs",
  ADMIN_HUB_READ: "/api/admin/getEditHub",
  ADMIN_HUB_STATUS: "/api/admin/changeHubsStatus",
  ADMIN_HUB_READ_FROM_CITY: "/api/admin/professional/getHubListByLocation",
  ADMIN_HUB_PRIVILEGE_READ: "/api/admin/getHubsPrivileges",
  ADMIN_HUB_PRIVILEGE_UPDATE: "/api/admin/updateHubsPrivileges",
  ADMIN_HUB_EMPLOYEE_LIST: "/api/admin/getHubEmployeeList",
  ADMIN_HUB_EMPLOYEE_READ: "/api/admin/getEditHubEmployees",
  ADMIN_HUB_EMPLOYEE_UPDATE: "/api/admin/updateHubsEmployee",
  ADMIN_HUB_EMPLOYEE_STATUS: "/api/admin/changeHubEmployeesStatus",
  ADMIN_CORPORATE_LIST: "/api/admin/getCoorperateOfficeList",
  ADMIN_CORPORATE_LIST__ALL: "/api/admin/getAllCoorperateOffices",
  ADMIN_CORPORATE_STATUS: "/api/admin/changeCoorperateOfficeStatus",
  ADMIN_CORPORATE_UPDATE: "/api/admin/updateCoorperateOffice",
  ADMIN_CORPORATE_READ: "/api/admin/getCoorperateOffice",
  ADMIN_CORPORATE_PRIVILEGE_READ: "/api/admin/getCoorperateOfficePrivileges",
  ADMIN_CORPORATE_PRIVILEGE_UPDATE: "/api/admin/updateCoorperateOfficePrivileges",
  ADMIN_CORPORATE_SINGLE_LIST: "/api/booking/cooprateRideList",
  ADMIN_CORPORATE_BILLING_LIST: "/api/admin/getCoorperateOfficeBillingList",
  ADMIN_CORPORATE_CONFIRM_PAYMENT: "/api/admin/changeCoorperatePaidStatus",
  ADMIN_CORPORATE_BILLING_AMOUNT: "/api/booking/getFareCalculation",
  ADMIN_VEHICLE_CATEGORY_LIST: "/api/admin/setup/getVehicleCategoryList",
  ADMIN_VEHICLE_CATEGORY_UPDATE: "/api/admin/setup/updateVehicleCategory",
  ADMIN_VEHICLE_CATEGORY_READ: "/api/admin/setup/editVehicleCategory",
  ADMIN_VEHICLE_CATEGORY_STATUS: "/api/admin/setup/changeVehicleCategoryStatus",
  ADMIN_VEHICLE_CATEGORY_LIST_ALL: "/api/admin/setup/getAllVehicleCategory",
  ADMIN_VEHCILE_CATEGORY_BASED_ON_CITY: "/api/admin/getCityBasedVehicleCategory",
  ADMIN_PAYMENT_LIST: "/api/admin/setup/getAllPaymentGateway",
  ADMIN_PAYMENT_READ: "/api/admin/setup/getEditPaymentGateway",
  ADMIN_PAYMENT_UPDATE: "/api/admin/setup/updatePaymentGateway",
  ADMIN_PAYMENT_STATUS: "/api/admin/setup/changePaymentGatewayStatus",
  ADMIN_CITY_LIST: "/api/admin/getAllServiceCategory",
  ADMIN_CITY_UPDATE: "/api/admin/updateServiceCategory",
  ADMIN_CITY_READ: "/api/admin/editServiceCategory",
  ADMIN_CITY_TABLE_LIST: "/api/admin/getServiceCategoryList",
  ADMIN_CITY_STATUS: "/api/admin/changeServiceCategoryStatus",
  ADMIN_CITY_POLYGON_LIST: "/api/admin/getServiceBasedLocation",
  ADMIN_DOCUMENT_LIST: "/api/admin/setup/getDocumentsList",
  ADMIN_DOCUMENT_LIST_ARCHIEVE: "/api/admin/setup/getDocumentsArchiveList",
  ADMIN_DOCUMENT_READ: "/api/admin/setup/getEditDocument",
  ADMIN_DOCUMENT_UPDATE: "/api/admin/setup/updateDocument",
  ADMIN_DOCUMENT_STATUS: "/api/admin/setup/changeDocumentStatus",
  ADMIN_CANCELLATION_LIST: "/api/admin/setup/getCancellationReasonsList",
  ADMIN_CANCELLATION_READ: "/api/admin/setup/getEditCancellationReason",
  ADMIN_CANCELLATION_UPDATE: "/api/admin/setup/updateCancellationReason",
  ADMIN_CANCELLATION_STATUS: "/api/admin/setup/changeCancellationReasonStatus",
  ADMIN_VERIFICATION_LIST: "/api/admin/driver/getVerificationDocumentList",
  ADMIN_VERIFICAIINO_READ: "/api/admin/driver/getEditVerificationDocument",
  ADMIN_VERIFICATION_UPDATE: "/api/admin/driver/updateVerificationDocument",
  ADMIN_VERIFICATION_STATUS: "/api/admin/driver/changeVerificationDocumentStatus",
  ADMIN_SETTINGS_GENERAL_READ: "/api/admin/config/getGenerelConfig",
  ADMIN_SETTINGS_GENERAL_UPLOAD: "/api/admin/config/uploadGeneralImages",
  ADMIN_SETTINGS_GENERAL_UPDATE: "/api/admin/config/updateGenerelConfig",
  ADMIN_SETTINGS_SMS_READ: "/api/admin/config/getSMSConfig",
  ADMIN_SETTINSG_SMS_UPDATE: "/api/admin/config/updateSMSConfig",
  ADMIN_SETTINGS_SMTP_READ: "/api/admin/config/getSMTPConfig",
  ADMIN_SETTINGS_SMTP_UPDATE: "/api/admin/config/updateSMTPConfig",
  ADMIN_USERS_LIST: "/api/admin/user/getUsersList",
  ADMIN_USERS_LIST_ARCHIEVE: "/api/admin/user/getUsersArchiveList",
  ADMIN_USERS_READ: "/api/admin/user/getEditUser",
  ADMIN_USERS_UPDATE: "/api/admin/user/updateUser",
  ADMIN_USERS_STATUS: "/api/admin/user/changeUsersStatus",
  ADMIN_USERS_VIEW: "/api/user/adminViewDetails",
  ADMIN_USERS_TRANACTIONS: "/api/transaction/user/getParticularTransactionList",
  ADMIN_USERS_RECHARGE_WALLET: "/api/user/addWalletAmount",
  ADMIN_USERS_UPDATE_BYPASS: "/api/user/updatePasswordBypass",
  ADMIN_UNREGISTERED_USERS_LIST: "/api/admin/user/getUnregisteredUserList",
  ADMIN_UNREGISTERED_USERS_CLOSE: "/api/admin/user/closeUnregisteredUser",
  ADMIN_UNREGISTERED_USERS_ADD_NOTES: "/api/admin/user/addUnregisteredUserNotes",
  ADMIN_UNREGISTERED_USERS_STATUS: "/api/admin/user/changeUnregisteredUsersStatus",
  ADMIN_UNREGISTERED_DRIVERS_LIST:
    "/api/admin/professional/getUnregisteredProfessionalList",
  ADMIN_UNREGISTERED_DRIVERS_CLOSE:
    "/api/admin/professional/closeUnregisteredProfessional",
  ADMIN_UNREGISTERED_DRIVERS_ADD_NOTES:
    "/api/admin/professional/addUnregisteredProfessionalNotes",
  ADMIN_UNREGISTERED_DRIVERS_STATUS:
    "/api/admin/professional/addUnregisteredProfessionalStatusChange",
  ADMIN_DRIVERS_LIST: "/api/admin/professional/getProfessionalList",
  ADMIN_DRIVERS_READ: "/api/admin/professional/getEditProfessional",
  ADMIN_DRIVERS_STATUS: "/api/admin/professional/changeProfessionalsStatus",
  ADMIN_DRIVERS_BANK_SAVE: "/api/admin/professional/updateBankDetailsProfessional",
  ADMIN_DRIVERS_ADD_STEP_1: "/api/admin/professional/updateProfessionalStep1",
  ADMIN_DRIVERS_PROFILE_DOC_UPLOAD: "/api/admin/professional/updateProfessionalStep2",
  ADMIN_DRIVERS_PROFILE_VERIFY:
    "/api/admin/professional/professionalProfileDocumentVerification",
  ADMIN_DRIVERS_ADD_STEP_3: "/api/admin/professional/updateProfessionalStep3",
  ADMIN_DRIVERS_ADD_STEP_4: "/api/admin/professional/updateProfessionalStep4",
  ADMIN_DRIVERS_DOC_LIST: "/api/admin/professional/getAllDocumentForUpload",
  ADMIN_DRIVERS_VERIFICATION: "/api/admin/professional/professionalVerification",
  ADMIN_DRIVERS_NOTES: "/api/admin/professional/addProfessionalNotes",
  ADMIN_DRIVERS_UNVERIFIED_LIST: "/api/admin/professional/getUnverifiedProfessionalList",
  ADMIN_DRIVERS_UPLOADED_LIST: "/api/professional/getUploadedList",
  ADMIN_DRIVERS_WALLET_RECHARGE: "/api/professional/addWalletAmount",
  ADMIN_DRIVERS_TRANACTIONS: "/api/transaction/professional/getParticularTransactionList",
  ADMIN_DRIVERS_UPDATE_BYPASS: "/api/professional/updatePasswordBypass",
  ADMIN_VEHICLES_LIST: "/api/admin/professional/getVehicleList",
  ADMIN_VEHICLES_STATUS: "/api/admin/professional/changeVehicleStatus",
  ADMIN_VEHICLES_READ: "/api/admin/professional/editVehicleDetails",
  ADMIN_VEHICLES_NOTES: "/api/admin/professional/addVehicleNotes",
  ADMIN_VEHICLES_DOC_VERIFY:
    "/api/admin/professional/professionalVehicleDocumentVerification",
  ADMIN_LANGUAGE_LIST: "/api/admin/getLanguagesList",
  ADMIN_LANGUAGE_READ: "/api/admin/getEditLanguage",
  ADMIN_LANGUAGE_UPDATE: "/api/admin/updateLanguage",
  ADMIN_LANGUAGE_STATUS: "/api/admin/changeLanguageStatus",
  ADMIN_LANGUAGE_DEFAULT: "/api/admin/setDefaultLanguage",
  ADMIN_LANGUAGE_DEFAULT_READ: "/api/admin/getDefaultLanguage",
  ADMIN_LANGUAGE_KEYS: "/api/admin/getLanguageKeys",
  ADMIN_LANGUAGE_TRANSLATE: "/api/admin/updateTranslateKeys",
  ADMIN_LANGUAGE_LIST_ALL: "/api/admin/getAllLanguages",
  ADMIN_LANGUAGE_USER_SET: "/api/admin/setUserBasedLanguage",
  ADMIN_RIDES_NEW_LIST: "/api/admin/getNewRidesList",
  ADMIN_RIDES_SCHEDULED_LIST: "/api/admin/getScheduleRidesList",
  ADMIN_RIDES_SCHEDULED_ASSIGN_DRIVER_LIST: "/api/admin/getAssignProfessionalList",
  ADMIN_RIDES_SCHEDULED_ASSIGN_DRIVER_REQUEST: "/api/admin/sendRequstToProfessional",
  ADMIN_RIDES_ONGOING_LIST: "/api/admin/getOngoingRidesList",
  ADMIN_RIDES_ENDED_LIST: "/api/admin/getEndedRidesList",
  ADMIN_RIDES_EXPIRED_LIST: "/api/admin/getExpiredRidesList",
  ADMIN_RIDES_CANCELLED_LIST: "/api/admin/getCancelledRidesList",
  ADMIN_RIDES_USER_CANCELLED: "/api/admin/getUserCancelledRidesList",
  ADMIN_RIDES_DRIVER_CANCELLED: "/api/admin/getProfessionalCancelledRidesList",
  ADMIN_RIDES_ISSUE_LIST: "/api/admin/getIssueRidesList",
  ADMIN_RIDES_GUEST_LIST: "/api/admin/getGuestRidesList",
  ADMIN_RIDES_METER_LIST: "/api/admin/getManualMeterRidesList",
  ADMIN_RIDES_CORPORATE_LIST: "/api/admin/getCoorperateRidesList",
  ADMIN_RIDES_SEARCH: "/api/admin/getSearchRidesList",
  ADMIN_RIDES_READ: "/api/admin/viewRideDetails",
  ADMIN_RIDES_USERS_LIST: "/api/booking/getUserRideList",
  ADMIN_RIDES_PROFESSIONALS_LIST: "/api/booking/getProfessionalRideList",
  ADMIN_RIDES_CHANGE_STATUS_ARRIVED: "/api/booking/bookingStatusChange/arrived",
  ADMIN_RIDES_CHANGE_STATUS_STARTED: "/api/booking/bookingStatusChange/started",
  ADMIN_RIDES_CHANGE_STATUS_CANCEL: "/api/booking/adminCancelBooking",
  ADMIN_RIDES_CHANGE_STATUS_ENDED: "/api/booking/bookingStatuschange/Ended",
  ADMIN_RESPONSE_OFFICERS_LIST: "/api/admin/officer/getOfficerList",
  ADMIN_RESPONSE_OFFICERS_STATUS: "/api/admin/officer/changeOfficerStatus",
  ADMIN_RESPONSE_OFFICERS_UPDATE: "/api/admin/officer/updateOfficer",
  ADMIN_RESPONSE_OFFICERS_READ: "/api/admin/officer/getEditOfficer",
  ADMIN_SECURITY_SUBSCRIPTIONS_LIST: "/api/admin/subscription/getSubscriptionList",
  ADMIN_SECURITY_SUBSCRIPTIONS_STATUS: "/api/admin/subscription/changeSubscriptionStatus",
  ADMIN_SECURITY_SUBSCRIPTIONS_UPDATE: "/api/admin/subscription/updateSubscription",
  ADMIN_SECURITY_SUBSCRIPTIONS_READ: "/api/admin/subscription/getEditSubscription",
  ADMIN_SECURITY_SERVICES_LIST: "/api/admin/getSecurityServiceList",
  ADMIN_SECURITY_SERVICES_UPDATE: "/api/admin/updateSecurityServiceList",
  ADMIN_SECURITY_NEW_REQUESTS: "/api/admin/security/getList",
  ADMIN_SECURITY_CLOSED_REQUESTS: "/api/admin/security/getClosedList",
  ADMIN_SECURITY_NOTES_ADD: "/api/admin/addSecurityNotes",
  ADMIN_SECURITY_REQUESTS_STATUS: "/api/admin/changeSecurityStatus",
  ADMIN_SECURITY_REQUESTS_READ: "/api/securityEscort/viewSecurityRequest",
  ADMIN_SECURITY_CLOSED_REQUESTS_READ: "/api/securityEscort/viewClosedSecurityRequest",
  ADMIN_SECURITY_OPERATOR_VIEW: "/api/securityEscort/viewSecurityRequestBasedOperator",
  ADMIN_SECURITY_OPERATOR_REQUEST: "/api/admin/security/getOperatorList",
  ADMIN_SECURITY_REQUESTS_ACCEPT_OPERATOR: "/api/admin/operatorAcceptSecurity",
  ADMIN_SECURITY_REQUESTS_PRIORITY: "/api/securityEscort/priorityChange",
  ADMIN_SECURITY_REQUESTS_VIEW_LIST_OFFICERS: "/api/securityEscort/getAvaliableOfficer",
  ADMIN_SECURITY_ASSIGN_DRIVER_REQUEST_ACCEPT: "/api/securityEscort/assignOfficer",
  ADMIN_SECURITY_SEND_REQUEST: "/api/securityEscort/sendOfficerRequest",
  ADMIN_SECURITY_CANCEL_REQUEST: "/api/securityEscort/cancelEscortBooking",
  ADMIN_SECURITY_BOOKING_STATUS_CHANGE: "/api/securityEscort/escortAdminStatusChange",
  ADMIN_ASSETS_EARNINGS: "/api/booking/getSiteEarnings",
  ADMIN_DASHBOARD_USERS: "/api/user/common/dashboard/getUserData",
  ADMIN_DASHBOARD_DRIVERS: "/api/professional/common/dashboard/getProfessionalData",
  ADMIN_DASHBOARD_OPERATORS: "/api/admin/common/dashboard/getOperatorData",
  ADMIN_DASHBOARD_OFFICERS: "/api/officer/common/dashboard/getOfficerData",
  ADMIN_DASHBOARD_RIDE_WITH_CITIES_COUNT:
    "/api/booking/common/dashboard/getCityBasedRidesData",
  ADMIN_DASHBOARD_HUBS: "/api/admin/common/dashboard/getHubsData",
  ADMIN_DASHBOARD_SERVICE_BASED_HUBS: "/api/admin/common/dashboard/getHubsBasedCityData",
  ADMIN_DASHBOARD_VEHICLES: "/api/professional/common/dashboard/getVehiclesData",
  ADMIN_DASHBOARD_RIDES: "/api/booking/common/dashboard/getGraphBookingData",
  ADMIN_DASHBOARD_VEHICLE_CATEGORY:
    "/api/professional/common/dashboard/getVehiclesDataBasedVehicleCategory",
  ADMIN_DASHBOARD_NOTIFICATION: "/api/admin/common/dashboard/getNotificationData",
  ADMIN_DASHBOARD_CORPORATE_BOOKINGS: "/api/booking/getOperatorBasedBooking",
  ADMIN_DASHBOARD_CORPORATE_FARE: "/api/booking/getTotalFareData",
  ADMIN_CITY_LIST_ALL: "/api/professional/vehicleVerification/getServiceLocationList",
  ADMIN_SATELLITE_VIEW: "/api/admin/gods/location/getProfessionalList",
  ADMIN_EAGLE_VIEW: "/api/admin/eagle/getAllRidesList",
  ADMIN_EAGLE_VEHICLE_CATEGORY: "/api/admin/setup/getEagleVehicleCategory",
  ADMIN_COMMON_CONFIG: "/api/admin/reactConfig",
  ADMIN_BOOK_CHECK_USER: "/api/user/adminCheckUserAvail",
  ADMIN_BOOK_CREATE_USER: "/api/user/registerUserByBooking",
  ADMIN_BOOK_DRIVERS_LIST: "/api/booking/getAvailableProfessionals",
  ADMIN_BOOK_CATEGORIES_GET: "/api/category/user/ride/getLocationBasedCategory",
  ADMIN_BOOK_RIDE_INSTANT: "/api/booking/newRideBookingFromAdmin/instant",
  ADMIN_BOOK_RIDE_SCHEDULE: "/api/booking/newRideBookingFromAdmin/schedule",
  ADMIN_BOOK_RIDE_CORPORATE_INSTANT: "/api/booking/coorperateBooking/instant",
  ADMIN_BOOK_RIDE_CORPORATE_SCHEDULE: "/api/booking/coorperateBooking/schedule",
  ADMIN_BOOK_RIDE_TABLE_LIST: "/api/booking/adminRideList",
  ADMIN_BOOK_RETRY: "/api/booking/user/ride/retryBooking",
  ADMIN_BOOK_EXPIRY: "/api/booking/adminBookingExpiryOrCancel",
  ADMIN_BOOK_RETRY_AWAITING: "/api/booking/user/ride/expiredRetryBooking",
  ADMIN_EARNINGS_BILLING_CYCLE: "/api/admin/fetchBillingCycle",
  ADMIN_EARNINGS_SUMMARY: "/api/booking/getSiteEarningsSummary",
  ADMIN_ASSET_WALLET: "/api/transaction/getWalletTransactionList",
  ADMIN_ASSET_REFUND: "/api/admin/wallet/refund",
  ADMIN_ASSET_WITHDRAW: "/api/admin/wallet/withdraw",
  ADMIN_ASSET_DUE_FREEEZE: "/api/admin/getWalletAmountList",
  ADMIN_ASSET_TRANSACTION_VIEW: "/api/admin/transaction/get",
  ADMIN_REPORTS_FEEDBACK: "/api/admin/reports/getList",
  ADMIN_REPORTS_FEEDBACK_NOTES_ADD: "/api/admin/addReportsNotes",
  ADMIN_REPORTS_FEEBACK_STATUS_CHANGE: "/api/admin/changeReportStatus",
  ADMIN_REPORTS_FEEDBACK_VIEW: "/api/admin/viewReports",
  ADMIN_LOCATION_USER: "/api/user/trackUserLocation",
  ADMIN_LOCATION_PROFESSIONAL: "/api/professional/trackProfessionalLocation",
  ADMIN_LOCATION_OFFICER: "/api/officer/trackOfficerLocation",
  ADMIN_REFER_CONFIG: "/api/admin/getInviteAndEarnConfig",
  ADMIN_REFER_CONFIG_IMAGE_UPLOAD: "/api/admin/updateInviteAndEarnImage",
  ADMIN_REFER_CONFIG_UPDATE: "/api/admin/updateInviteAndEarn",
  ADMIN_REFER_USER_LIST: "/api/user/getInviteAndEarnList",
  ADMIN_REFER_USER_JOINER_LIST: "/api/user/getUsersJoinerList",
  ADMIN_REFER_PROFESSIONAL_LIST: "/api/professional/getInviteAndEarnList",
  ADMIN_REFER_PROFESSIONAL_JOINER_LIST: "/api/professional/getUsersJoinerList",
  ADMIN_SETUP_EMAIL_TEMPLATES_LISTS: "/api/admin/getEmailTemplateList",
  ADMIN_SETUP_EMAIL_TEMPLATES_READ: "/api/admin/editEmailTemplate",
  ADMIN_SETUP_EMAIL_TEMPLATES_UPDATE: "/api/admin/updateEmailTemplate",
  ADMIN_SETUP_EMAIL_TEMPLATES_STATUS_CHANGE: "/api/admin/changeEmailTemplateStatus",
  ADMIN_TOLL_LIST: "/api/admin/getTollList",
  ADMIN_TOLL_STATUS_CHANGE: "/api/admin/changeTollStatus",
  ADMIN_TOLL_UPDATE: "/api/admin/updateToll",
  ADMIN_TOLL_READ: "/api/admin/getEditToll",
  ADMIN_POPULAR_PLACES_LIST: "/api/admin/getPopularPlacesList",
  ADMIN_POPULAR_PLACES_UPDATE: "/api/admin/updatePopularPlace",
  ADMIN_POPULAR_PLACES_IMAGE_UPLOAD: "/api/admin/uploadPopularPlaceImage",
  ADMIN_POPULAR_PLACES_STATUS_CHANGE: "/api/admin/changePopularPlaceStatus",
  ADMIN_POPULAR_PLACES_READ: "/api/admin/getEditPopularPlace",
  ADMIN_SETUP_CITY_MORE_INFO_UPDATE: "/api/admin/updateCategoryFareBreakup",
  ADMIN_SETUP_CITY_MORE_INFO_READ: "/api/admin/getCategoryFareBreakup",
  ADMIN_OTHERS_UPLOAD_IMAGE: "/api/admin/getCategoryFareBreakupImages",
  ADMIN_COUPONS_LIST: "/api/admin/getCouponList",
  ADMIN_COUPONS_UPDATE: "/api/admin/updateCoupon",
  ADMIN_COUPONS_READ: "/api/admin/getEditCoupon",
  ADMIN_COUPONS_STATUS_CHANGE: "/api/admin/changeCouponStatus",
  ADMIN_COUPON_BASED_RIDES: "/api/booking/getCouponBasedRideList",
  ADMIN_AIRPORTS_LIST: "/api/admin/getAirportList",
  ADMIN_AIRPORTS_UPDATE: "/api/admin/updateAirport",
  ADMIN_AIRPORTS_IMAGE_UPLOAD: "/api/admin/uploadAirportImage",
  ADMIN_AIRPORTS_READ: "/api/admin/getEditAirport",
  ADMIN_AIRPORTS_CHANGE_STATUS: "/api/admin/changeAirportStatus",
  ADMIN_AIRPORTS_STOPS_LISTS: "/api/admin/getAirportStopList",
  ADMIN_AIRPORTS_STOPS_READ: "/api/admin/getEditAirportStop",
  ADMIN_AIRPORTS_STOPS_UPDATE: "/api/admin/updateAirportStops",
  ADMIN_AIRPORTS_STOPS_CHANGE_STATUS: "/api/admin/changeAirportStopStatus",
  ADMIN_SETUP_NOTIFICATIONS_LIST: "/api/admin/getNotificationTemplatesList",
  ADMIN_SETUP_NOTIFICATIONS_CHANGE_STATUS: "/api/admin/changeNotificationTemplateStatus",
  ADMIN_SETUP_NOTIFICAIONS_UPDATE: "/api/admin/updateNotificationTemplates",
  ADMIN_SETUP_NOTIFICATIONS_READ: "/api/admin/getNotificationTemplates",
  ADMIN_SETUP_NOTIFICATIONS_SEND_PROMO: "/api/admin/sendPromotion",
  ADMIN_SETUP_NOTIFICATIONS_GET_LISTS: "/api/admin/getUpdateEmailEditableFields",
};

module.exports = data;
