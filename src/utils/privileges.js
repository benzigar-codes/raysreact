const utils = require("./utils");

const data = {
  ADMIN: {
    ROLES: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    OPERATORS: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    HUBS: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    HUBS_EMPLOYEES: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
  },
  SETUP: {
    VEHICLE_CATEGORYS: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    CITY_COMMISIONS: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    DOCUMENTS: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    CANCELLATION_REASONS: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    VERIFICATION_DOCUMENTS: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    REFER_EARN: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    EMAIL_TEMPLATES: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    NOTIFICATION_TEMPLATES: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    TOLLS: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    POPULAR_PLACES: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    COUPONS: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    AIRPORTS: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
  },
  USERS: {
    USERS_LIST: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    UNREGISTERED_USER: {
      VIEW: true,
      EDIT: true,
    },
  },
  DRIVERS: {
    DRIVERS_LIST: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    UNVERIFIED_DRIVERS: {
      VIEW: true,
      ADD: true,
      EDIT: true,
    },
    UNREGISTERED_DRIVERS: {
      VIEW: true,
      EDIT: true,
    },
  },
  RIDES: {
    RIDES_LIST: {
      VIEW: true,
    },
  },
  ASSET: {
    SITE_EARNINGS: {
      VIEW: true,
    },
    PAYMENTS: {
      VIEW: true,
      EDIT: true,
    },
    WALLETS: {
      VIEW: true,
    },
    DUE_FREEZED: {
      VIEW: true,
    },
  },
  SETTINGS: {
    LANGUAGE: {
      ADD: true,
      EDIT: true,
    },
    REFER_EARN_CONFIG: {
      EDIT: true,
    },
  },
  SECURITY: {
    NEW_REQUESTS: {
      VIEW: true,
    },
    CLOSED_REQUESTS: {
      VIEW: true,
    },
    MY_REQUESTS: {
      VIEW: true,
    },
  },
  OTHERS: {
    EAGLE_VIEW: {
      VIEW: true,
    },
    SATELLITE_VIEW: {
      VIEW: true,
    },
    BOOK_RIDE: {
      VIEW: true,
    },
    SUPPORT: {
      VIEW: true,
    },
    DISPATCH: {
      VIEW: true,
    },
  },
  REPORTS: {
    FEEDBACK: {
      VIEW: true,
    },
  },
};

if (utils.mode === "development") {
  data["ADMIN"]["EMERGENCY_RESPONSE_OFFICES"] = {
    VIEW: true,
    ADD: true,
    EDIT: true,
  };
  // Response Officers
  data["RESPONSE_OFFICERS"] = {};
  data["RESPONSE_OFFICERS"]["RESPONSE_OFFICERS_LIST"] = {
    VIEW: true,
    ADD: true,
    EDIT: true,
  };
  data["SECURITY"]["SUBSCRIPTIONS"] = {
    VIEW: true,
    ADD: true,
    EDIT: true,
  };
  data["SECURITY"]["SERVICES"] = {
    EDIT: true,
  };
  // Corporate
}

if (utils.mode === "demo") {
  data["OTHERS"]["MORPH_TEXT"] = {
    VIEW: true,
  };
}

data["CORPORATE"] = {};
data["CORPORATE"]["CORPORATE"] = {
  VIEW: true,
  ADD: true,
  EDIT: true,
};

module.exports = data;
