import { format } from "date-fns/esm";
import React from "react";
import { useHistory } from "react-router-dom";
import {
  FiCheck,
  FiChevronRight,
  FiChevronUp,
  FiMessageCircle,
  FiSend,
  FiX,
  FiEye,
} from "react-icons/fi";
import useAdmin from "../../hooks/useAdmin";

import axios from "axios";
import A from "../../utils/API";
import NavLinks from "../../utils/navLinks.json";

import useFirebase from "../../hooks/useFirebase";

const SupportChatWindow = ({ id, right, user }) => {
  const { db, firebase } = useFirebase();
  const { admin, header, authFailure } = useAdmin();
  const doc = db.collection("Support").doc(id);
  const [msgText, setMsgText] = React.useState("");
  const [messages, setMessages] = React.useState([]);
  const scrollArea = React.useRef();
  const history = useHistory();
  const [showContent, setShowContent] = React.useState(true);
  const [showDelete, setShowDelete] = React.useState(false);

  const [userDetails, setUserDetails] = React.useState(null);

  const fetchUserDetails = async () => {
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_USERS_LIST,
        {
          skip: 0,
          limit: "10",
          search: user,
          filter: "",
        },
        header
      );
      if (data?.count > 0 && data?.response?.length > 0)
        setUserDetails(data?.response[0]);
    } catch (err) {
      alert(err);
      authFailure(err);
    }
  };

  React.useEffect(() => {
    fetchUserDetails();
    doc
      .collection("messages")
      // .orderBy("createdAt")
      .onSnapshot((snap) => {
        setMessages([]);
        snap.forEach((doc) => {
          setMessages((message) => [...message, { id: doc.id, ...doc.data() }]);
        });
        setTimeout(
          () =>
            scrollArea.current !== null &&
            scrollArea.current.scrollIntoView({ behavior: "smooth" }),
          500
        );
      });
  }, []);
  const sendMessage = () => {
    doc
      .collection("messages")
      .add({
        from: admin.phone.number,
        sender: "ADMIN",
        message: msgText,
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      })
      .then(
        () =>
          scrollArea.current !== null &&
          scrollArea.current.scrollIntoView({ behavior: "smooth" })
      );
    setMsgText("");
  };

  React.useEffect(
    () =>
      showContent === true &&
      setTimeout(
        () =>
          scrollArea.current !== null &&
          scrollArea.current.scrollIntoView({ behavior: "smooth" }),
        300
      ),
    [showContent]
  );

  const closeChat = async () => {
    const messages = await db.collection("messages").get();
    messages.forEach(async (doc) => await doc.delete());
    doc.delete();
  };

  return (
    <div
      className="fixed bottom-0"
      style={{ zIndex: 50, right, border: "2px solid black" }}
    >
      <div
        className={
          "bg-blue-800 bg-white shadow-xl text-white flex items-center justify-between"
        }
        style={{ width: 300 }}
      >
        <div className="px-5 py-2">
          <h1 className="text-white font-bold">
            {(userDetails?.data?.firstName ?? "....") +
              " " +
              (userDetails?.data?.lastName ?? "")}
          </h1>
          <p className="text-sm">{user}</p>
        </div>
        <div className="flex h-full">
          {userDetails && (
            <FiEye
              onClick={() => {
                history.push(NavLinks.ADMIN_USERS_VIEW_EACH + "/" + userDetails?._id);
                if (showContent) setShowContent(!showContent);
              }}
              className={
                "cursor-pointer mx-1 h-full text-2xl bg-green-800 text-white p-1"
              }
            />
          )}
          <FiChevronUp
            onClick={() => setShowContent(!showContent)}
            className={"cursor-pointer mx-1 h-full text-2xl bg-green-800 text-white p-1"}
            style={{
              transform: showContent ? "rotateZ(180deg)" : "rotateZ(0deg)",
            }}
          />
          <FiX
            onClick={() => setShowDelete(true)}
            className={"mx-1 h-full text-2xl bg-green-800 text-white p-1"}
          />
        </div>
      </div>
      {showContent === true && (
        <>
          {" "}
          {showDelete === true && (
            <div
              className="transition bg-red-500 px-2 py-2 text-white flex justify-between items-center"
              style={{ width: 300 }}
            >
              <p>Are you sure to remove the chat ?</p>
              <div className="flex items-center">
                <FiCheck onClick={closeChat} className="mx-1 cursor-pointer" />
                <FiX
                  onClick={() => setShowDelete(false)}
                  className="mx-1 cursor-pointer"
                />
              </div>
            </div>
          )}
          <div className="bg-white overflow-y-scroll" style={{ height: 200 }}>
            {/* {JSON.stringify(messages, null, 2)} */}
            {messages
              ?.sort((a, b) => (a.createdAt?.seconds > b.createdAt?.seconds ? 1 : -1))
              .map((each) => (
                <div>
                  <h1
                    className={`flex text-right ${
                      each.sender === "ADMIN" && "justify-end"
                    } text-sm text-gray-600 px-4 py-2`}
                    style={{ width: 300 }}
                  >
                    <div>
                      <p className="bg-gray-200 rounded-lg p-2">
                        <span className="flex justify-end">{each.message}</span>
                      </p>
                      <p
                        className={`flex ${
                          each.sender === "ADMIN" && "justify-end"
                        } mx-1 text-sm mt-1`}
                        style={{ fontSize: 11 }}
                      >
                        {each &&
                          each.createdAt &&
                          format(new Date(each.createdAt.seconds * 1000), "h:mm aa")}
                      </p>
                    </div>
                  </h1>
                </div>
              ))}
            <div style={{ float: "left", clear: "both" }} ref={scrollArea}></div>
          </div>
          <div className="flex">
            <input
              placeholder="Type..."
              value={msgText}
              onKeyUp={(e) => e.key === "Enter" && msgText.length > 0 && sendMessage()}
              onChange={(e) => setMsgText(e.target.value)}
              type="text"
              className="focus:outline-none text-gray-600 bg-gray-200 px-2 py-2 w-full text-sm"
            />
            <FiSend
              onClick={() => {
                msgText.length > 0 && sendMessage();
              }}
              className="h-full text-4xl bg-blue-800 p-2 text-white"
            />
          </div>
        </>
      )}
    </div>
  );
};

export const Support = () => {
  const { db } = useFirebase();
  const { admin } = useAdmin();
  const [requests, setRequests] = React.useState([]);
  const [acceptedPhones, setAcceptedPhones] = React.useState([]);
  const support = db.collection("Support");
  const [showSupport, setShowSupport] = React.useState(false);

  React.useEffect(() => {
    support
      .where("admin", "==", "NOT_ASSIGNED")
      // .orderBy("createdAt", "asc")
      .onSnapshot((snap) => {
        setRequests([]);
        snap.forEach((doc) => {
          setRequests((request) => [...request, { id: doc.id, ...doc.data() }]);
        });
      });

    support
      .where("admin", "==", admin.phone.number)
      // .orderBy("createdAt")
      .onSnapshot((snap) => {
        setAcceptedPhones([]);
        snap.forEach((doc) => {
          setAcceptedPhones((request) => [...request, { id: doc.id, ...doc.data() }]);
        });
      });
  }, []);

  const accept = (request) => {
    support.doc(request.id).set(
      {
        admin: admin.phone.number,
      },
      { merge: true }
    );
  };

  return (
    <>
      {requests.length > 0 && acceptedPhones.length < 5 && (
        <>
          <div
            className="fixed px-3 overflow-y-scroll py-2 bg-red-500 mb-2 shadow-xl transition duration-500"
            style={{
              top: 80,
              right: 0,
              transform:
                showSupport === true
                  ? "translateX(0) rotateZ(0)"
                  : "translateX(300px) rotateZ(90deg)",
              transformOrigin: "right bottom",
              height: 200,
              width: 250,
              borderTopLeftRadius: 10,
              borderBottomLeftRadius: 10,
              zIndex: 100,
            }}
          >
            <h1 className="text-white">Support Requests ({requests.length})</h1>
            {requests
              ?.sort((a, b) => (a.createdAt?.seconds > b.createdAt?.seconds ? 1 : -1))
              .map((request) => (
                <div
                  className={
                    "px-5 py-2 bg-white my-2 rounded-lg flex justify-between items-center"
                  }
                >
                  <div>
                    <h1 className="text-gray-600 font-bold">{request.user}</h1>
                    <p className="text-gray-500" style={{ fontSize: 12 }}>
                      {request &&
                        request.createdAt &&
                        format(
                          new Date(request.createdAt.seconds * 1000),
                          "d MMM h:mm aa"
                        )}
                    </p>
                  </div>
                  <FiCheck
                    onClick={() => accept(request)}
                    className="mx-2 text-2xl bg-blue-800 rounded-full text-white p-1 cursor-pointer"
                  />
                </div>
              ))}
          </div>
          <div
            onClick={() => setShowSupport(!showSupport)}
            className="fixed bg-white flex justify-center items-center bg-red-500 text-white shadow-xl transition duration-500"
            style={{
              top: 120,
              transform: showSupport === true ? "translateX(0)" : "translateX(250px)",
              right: 250,
              height: 35,
              width: 35,
              borderTopLeftRadius: 10,
              borderBottomLeftRadius: 10,
              zIndex: 10,
            }}
          >
            {showSupport && (
              <FiChevronRight
                className="text-2xl"
                style={{ transform: showSupport ? "rotateZ(0)" : "rotateZ(180deg)" }}
              />
            )}
            {!showSupport && (
              <>
                <FiMessageCircle
                  className="text-2xl"
                  style={{ transform: showSupport ? "rotateZ(0)" : "rotateZ(180deg)" }}
                />
                <FiMessageCircle
                  className="text-2xl animate-ping absolute"
                  style={{ transform: showSupport ? "rotateZ(0)" : "rotateZ(180deg)" }}
                />
              </>
            )}
          </div>
        </>
      )}
      {acceptedPhones.length > 0 && (
        <div className="fixed bottom-0 right-0 flex">
          {acceptedPhones.map((user, idx) => (
            <SupportChatWindow
              key={user.id}
              right={idx * 320 + 10}
              id={user.id}
              user={user.user}
            />
          ))}
        </div>
      )}
    </>
  );
};
