import React from "react";
import { FaAngleDoubleUp, FaDoorClosed } from "react-icons/fa";
import { FiEye, FiLoader, FiRefreshCw, FiSearch, FiUserPlus, FiX } from "react-icons/fi";
import axios from "axios";

import useLanguage from "../../hooks/useLanguage";
import { TextField } from "../common/TextField";
import A from "../../utils/API.js";
import U from "../../utils/utils.js";
import NavLinks from "../../utils/navLinks.json";
import useAdmin from "../../hooks/useAdmin";
import useSettings from "../../hooks/useSettings";
import { format } from "date-fns";
import useUtils from "../../hooks/useUtils";
import { Waypoint } from "react-waypoint";
import { NavLink, useHistory } from "react-router-dom";
import { DropdownNormal } from "../common/DropDownNormal";

export default function BookRideTable({
  tableFetch = 0,
  expandFromParent = false,
  setManualAssign = () => {},
  retryRide = () => {},
}) {
  const { language } = useLanguage();
  const { settings } = useSettings();
  const { header, authFailure } = useAdmin();
  const { truncate, morph } = useUtils();
  const history = useHistory();

  const [showAwaiting, setShowAwaiting] = React.useState("ALL");

  const [searchText, setSearchText] = React.useState("");
  const [total, setTotal] = React.useState(0);
  const [expandTable, setExpandTable] = React.useState(false);
  const [loading, setLoading] = React.useState(true);
  const [rides, setRides] = React.useState([]);
  const [categories, setCategories] = React.useState([]);

  const [loadMoreLoading, setLoadMoreLoading] = React.useState(false);

  const [showBottomLoader, setShowBottomLoader] = React.useState(false);

  const [pagination, setPagination] = React.useState({
    skip: 0,
    limit: settings.pageViewLimits[0],
  });

  const loadMore = async () => {
    setLoadMoreLoading(true);
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_BOOK_RIDE_TABLE_LIST,
        {
          skip: parseInt(pagination.skip) + parseInt(pagination.limit),
          limit: pagination.limit,
          search: searchText,
          filter: showAwaiting,
        },
        header
      );
      setRides([...rides, ...data.response]);
    } catch (err) {
      authFailure(err);
    }
    setLoadMoreLoading(false);
    setPagination({
      ...pagination,
      skip: parseInt(pagination.skip) + parseInt(pagination.limit),
    });
  };

  const fetchVehicleCategory = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_EAGLE_VEHICLE_CATEGORY, {}, header);
      setCategories(data);
    } catch (err) {
      authFailure(err);
    }
  };

  const clearSearch = async () => {
    setLoading(true);
    setPagination({ ...pagination, skip: 0 });
    setSearchText("");
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_BOOK_RIDE_TABLE_LIST,
        {
          skip: 0,
          limit: pagination.limit,
          search: "",
          filter: showAwaiting,
        },
        header
      );
      setRides(data.response);
      setTotal(data.count);
    } catch (err) {
      authFailure(err);
    }
    setLoading(false);
  };

  const fetchRides = async () => {
    try {
      setLoading(true);
      setShowBottomLoader(FaDoorClosed);
      const { data } = await axios.post(
        A.HOST + A.ADMIN_BOOK_RIDE_TABLE_LIST,
        {
          skip: 0,
          limit: pagination.limit,
          search: searchText,
          filter: showAwaiting,
        },
        header
      );
      setPagination({ ...pagination, skip: 0 });
      setRides(data.response);
      setTotal(data.count);
      setTimeout(() => setShowBottomLoader(true), 500);
      setLoading(false);
    } catch (err) {
      authFailure(err);
    }
  };

  React.useEffect(() => {
    // alert(JSON.stringify(pagination));
  }, [pagination]);

  React.useEffect(() => {
    fetchRides();
    fetchVehicleCategory();
  }, [tableFetch, showAwaiting]);

  React.useEffect(() => {
    setExpandTable(expandFromParent);
  }, [expandFromParent]);

  const searchSubmit = () => {};
  return (
    <>
      <div
        className="transition-all duration-300 fixed left-0 right-0 table w-full bg-white"
        style={{ zIndex: 50, top: expandTable ? "0%" : "90%" }}
      >
        <div className="w-full p-2 bg-gray-100 dark:bg-gray-700 dark:text-white flex justify-between items-center">
          <h1 className="mx-5">{language.RECENT_RIDES}</h1>
          <div className="flex items-center">
            <DropdownNormal
              defaultValue={language.ALL}
              change={(e) => setShowAwaiting(e)}
              fields={[
                { id: 1, label: language.ALL, value: "ALL" },
                { id: 2, label: language.AWAITING, value: "AWAITING" },
                { id: 3, label: language.ARRIVED, value: "ARRIVED" },
                { id: 4, label: language.ACCEPTED, value: "ACCEPTED" },
                { id: 5, label: language.USERCANCELLED, value: "USERCANCELLED" },
                { id: 6, label: language.USERDENY, value: "USERDENY" },
                { id: 7, label: language.PROFESSIONALCANCELLED, value: "PROFESSIONALCANCELLED" },
                { id: 8, label: language.EXPIRED, value: "EXPIRED" },
                { id: 9, label: language.CANCELLED, value: "CANCELLED" },
                { id: 10, label: language.STARTED, value: "STARTED" },
                { id: 11, label: language.ENDED, value: "ENDED" },
              ]}
            />
            <FaAngleDoubleUp
              className="mx-3 cursor-pointer"
              onClick={() => setExpandTable(!expandTable)}
              style={{ transform: expandTable ? "rotateZ(180deg)" : "rotateZ(0deg)" }}
            />
            <button
              onClick={fetchRides}
              data-title={language.REFRESH}
              className={`${
                loading && "animate-spin"
              } bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
              }`}
            >
              <FiRefreshCw />
            </button>
            <TextField
              padding={false}
              onKeyDown={(e) => {
                if (e.key === "Enter") fetchRides();
              }}
              iconSelect={searchSubmit}
              change={(e) => setSearchText(e)}
              icon={<FiSearch />}
              placeholder={language.SEARCH}
              value={searchText}
            />
            {searchText !== "" && (
              <button
                onClick={() => clearSearch()}
                data-title={language.NO}
                className={`bg-red-500 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                  document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "ml-2" : "mr-2"
                }`}
              >
                <FiX />
              </button>
            )}
          </div>
        </div>
        <div className="overflow-y-scroll overflow-x-scroll dark:bg-gray-800" style={{ height: "95vh" }}>
          <table
            cellPadding="10"
            className="shadow-xl w-full overflow-x-scroll relative text-gray-800 text-left dark:bg-gray-800 text-sm overflow-x-scroll"
            style={{ height: "100%" }}
          >
            <thead>
              <tr>
                <td className="sticky top-0 bg-blue-800 text-white p-3">{language.NO}</td>
                <td className="sticky top-0 bg-blue-800 text-white p-3">{language.BOOKING_ID}</td>
                <td className="sticky top-0 bg-blue-800 text-white p-3">{language.PICKUP_LOCATION}</td>
                <td className="sticky top-0 bg-blue-800 text-white p-3">{language.DROP_LOCATION}</td>
                <td className="sticky top-0 bg-blue-800 text-white p-3">{language.DRIVER}</td>
                <td className="sticky top-0 bg-blue-800 text-white p-3">{language.USER}</td>
                <td className="sticky top-0 bg-blue-800 text-white p-3">{language.CATEGORY}</td>
                <td className="sticky top-0 bg-blue-800 text-white p-3">{language.BOOKING_TIME}</td>
                <td className="sticky top-0 bg-blue-800 text-white p-3">{language.BOOKING_TYPE}</td>
                <td className="sticky top-0 bg-blue-800 text-white p-3">{language.STATUS}</td>
                <td className="sticky top-0 bg-blue-800 text-white p-3">{language.ACTION}</td>
              </tr>
            </thead>
            <tbody className="dark:text-white overflow-x-scroll">
              {loading === false &&
                rides.map((ride, idx) => (
                  <tr className="border-b-2 dark:border-gray-500">
                    <td>{idx + 1}</td>
                    <td>{ride.bookingId}</td>
                    <td>{morph(truncate(ride?.origin?.shortAddress, 20))}</td>
                    <td>{morph(truncate(ride?.destination?.shortAddress, 20))}</td>
                    <td>
                      {ride.professional.firstName ? (
                        <div className="flex flex-col">
                          <p>{morph(ride.professional.firstName + " " + ride.professional.lastName)}</p>
                          <p className="mt-2">
                            {morph(ride.professional.phone.code) + " " + morph(ride.professional.phone.number)}
                          </p>
                        </div>
                      ) : (
                        "_____"
                      )}
                    </td>
                    <td>
                      {ride.bookingFor ? (
                        <div className="flex flex-col">
                          <p>{morph(ride.bookingFor.name)}</p>
                          <p className="mt-2">
                            {morph(ride.bookingFor.phoneCode) + " " + morph(ride.bookingFor.phoneNumber)}
                          </p>
                        </div>
                      ) : (
                        "_____"
                      )}
                    </td>
                    <td>
                      {categories.filter((category) => category._id === ride.vehicle.vehicleCategoryId).length > 0
                        ? categories.filter((category) => category._id === ride.vehicle.vehicleCategoryId)[0].name
                        : ""}
                    </td>
                    <td>
                      <div className="flex flex-col">
                        <p>{format(new Date(ride.bookingDate), "do MMM, yyyy")}</p>
                        <p className="">{format(new Date(ride.bookingDate), "p")}</p>
                      </div>
                    </td>
                    <td>{language[ride.bookingType]}</td>
                    <td>{truncate(language[ride.bookingStatus], 10)}</td>
                    <td>
                      <div className="flex">
                        <a target="_blank" rel="noreferrer" href={NavLinks.ADMIN_RIDES_VIEW + "/" + ride._id}>
                          <button
                            title={language.VIEW}
                            className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                              document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                            }`}
                          >
                            <FiEye />
                          </button>
                        </a>
                        {((ride.bookingStatus === U.AWAITING && ride.bookingType === U.INSTANT) ||
                          (ride.bookingType === U.SCHEDULE &&
                            ride.bookingStatus === U.AWAITING &&
                            ride.isAssignAllowed === true)) && (
                          // <a
                          //   target="_blank"
                          //   rel="noreferrer"
                          //   href={NavLinks.ADMIN_RIDES_SCHEDULED_ASSIGN_DRIVER + "/" + ride._id}
                          // >
                          <button
                            onClick={() => setManualAssign(ride._id)}
                            title={language.ASSIGN_DRIVER}
                            className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                              document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                            }`}
                          >
                            <FiUserPlus />
                          </button>
                          // </a>
                        )}
                        {((ride.bookingStatus === U.AWAITING && ride.bookingType === U.INSTANT) ||
                          (ride.bookingStatus === U.EXPIRED && ride.bookingType === U.INSTANT)) && (
                          // <a
                          //   target="_blank"
                          //   rel="noreferrer"
                          //   href={NavLinks.ADMIN_RIDES_SCHEDULED_ASSIGN_DRIVER + "/" + ride._id}
                          // >
                          <button
                            onClick={() =>
                              retryRide({
                                id: ride._id,
                                bookingStatus: ride.bookingStatus,
                              })
                            }
                            title={language.RETRY}
                            className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                              document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                            }`}
                          >
                            <FiRefreshCw />
                          </button>
                          // </a>
                        )}
                      </div>
                    </td>
                  </tr>
                ))}
              {/* {showBottomLoader === true && rides.length > 0 && rides.length !== total && total !== 0 && (
                <Waypoint
                  onEnter={() => {
                    loading === false && rides.length > 0 && loadMore() && alert("RUN");
                  }}
                >
                  <tr>
                    <td colSpan={rides.length + 2} align="center">
                      <FiLoader className="animate-spin" />
                      <h1>Load More</h1>
                    </td>
                  </tr>
                </Waypoint>
              )} */}
              {showBottomLoader === true && rides.length > 0 && rides.length !== total && total !== 0 && (
                // <Waypoint
                //   topOffset={200}
                //   onEnter={() => {
                //     loading === false && rides.length > 0 && loadMore() && alert("RUN");
                //   }}
                // >
                <tr>
                  <td colSpan={rides.length + 2} align="center">
                    <div className="flex justify-center items-center">
                      <h1
                        className={"flex items-center mb-4 cursor-pointer bg-blue-800 px-4 rounded-xl py-2 text-white"}
                        onClick={loadMore}
                      >
                        {loadMoreLoading && <FiLoader className="animate-spin" />}
                        <p className="mx-2">{language.LOAD_MORE}</p>
                      </h1>
                    </div>
                  </td>
                </tr>
                // </Waypoint>
              )}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}
