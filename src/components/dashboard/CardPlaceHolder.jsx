import { FiUsers } from "react-icons/fi";
import { FiArrowUp } from "react-icons/fi";
import { FiArrowDown } from "react-icons/fi";
const CardPlaceHolder = ({ animate = null, width = "1/4", showChart = true, direction = "UP" }) => {
  return (
    <div className={`w-${width} p-2 ${animate !== null && `opacity-0 ${animate}`}`}>
      <div className="animate-pulse bg-gray-200 dark:bg-gray-900 dark:border-gray-900 border-t-2 cursor-pointer rounded-xl">
        <div className={"p-3"}>
          <div className="flex justify-between items-center text-md dark:text-gray-200">
            <h1 className={"bg-gray-300 dark:bg-gray-800 dark:text-gray-800 text-gray-300 rounded-xl"}>
              Professionals
            </h1>
            <div className={"text-gray-300 dark:bg-gray-800 dark:text-gray-800 bg-gray-300 rounded-xl p-1"}>
              <FiUsers />
            </div>
          </div>
          <div className={"overflow-y-scroll pt-3"} style={{ height: showChart ? 125 : 175 }}>
            <div className="flex justify-between items-center dark:border-gray-800 border-gray-100">
              <h1
                className={
                  "text-gray-300 h-4 bg-gray-300 dark:bg-gray-800 dark:text-gray-800 rounded-xl dark:text-gray-300"
                }
                style={{ fontSize: 13 }}
              >
                Professionals
              </h1>
              <div
                className={
                  "font-bold dark:bg-gray-800 dark:text-gray-800 mt-4 h-4 w-4 mr-1 text-gray-300 bg-gray-300 rounded-full dark:text-white"
                }
              ></div>
            </div>
            <div className="flex justify-between items-center dark:border-gray-800 border-gray-100">
              <h1
                className={
                  "dark:bg-gray-800 dark:text-gray-800 text-gray-300 h-4 bg-gray-300 rounded-xl dark:text-gray-300"
                }
                style={{ fontSize: 13 }}
              >
                Professionals
              </h1>
              <div
                className={
                  "dark:bg-gray-800 dark:text-gray-800 font-bold mt-4 h-4 w-4 mr-1 text-gray-300 bg-gray-300 rounded-full dark:text-white"
                }
              ></div>
            </div>
            <div className="flex justify-between items-center dark:border-gray-800 border-gray-100">
              <h1
                className={
                  "dark:bg-gray-800 dark:text-gray-800 text-gray-300 h-4 bg-gray-300 rounded-xl dark:text-gray-300"
                }
                style={{ fontSize: 13 }}
              >
                Professionals
              </h1>
              <div
                className={
                  "dark:bg-gray-800 dark:text-gray-800 font-bold mt-4 h-4 w-4 mr-1 text-gray-300 bg-gray-300 rounded-full dark:text-white"
                }
              ></div>
            </div>
          </div>
        </div>
        {showChart === true && (
          <div className="bg-gray-300 dark:bg-gray-800 dark:text-gray-800 rounded-b-xl w-full">
            <div
              style={{
                height: 50,
              }}
            ></div>
          </div>
        )}
        {false && (
          <div
            className="p-2 text-gray-100 text-center"
            style={{ fontSize: 13, backgroundColor: direction === "UP" ? "#5ACD69" : "#E43C45" }}
          >
            <div className={"flex items-center justify-center"}>
              {direction === "UP" ? <FiArrowUp className={"mx-1"} /> : <FiArrowDown className={"mx-1"} />} 20 %{" "}
              <p className={"mx-1 opacity-75"}>Since last 30 days</p>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default CardPlaceHolder;
