import React from "react";
import axios from "axios";
import gsap from "gsap";

import SuccessJSON from "../../assets/lottie/success.json";
import useAdmin from "../../hooks/useAdmin";
import useLanguage from "../../hooks/useLanguage";
import Lottie from "react-lottie";
import { TextArea } from "../common/TextArea";
import { ToggleButton } from "../common/ToggleButton";

const WalletRecharge = ({
  setShowRechargePage = () => {},
  type = "CREDIT",
  fetchData = () => {},
  link = "",
  wallet = 0,
  userDetails = {},
}) => {
  const { language } = useLanguage();
  const { header } = useAdmin();
  const [amount, setAmount] = React.useState(null);
  const [completed, setCompleted] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [reason, setReason] = React.useState("");
  const [reimbursement, setReImbursement] = React.useState(0);
  const recharge = async () => {
    if (type === "DEBIT" && wallet < amount) {
      alert(language.INSUFFICIENT_BALANCE);
      return;
    }
    const sendData = {
      id: userDetails.id,
      type,
      amount,
      reason,
    };
    if (type === "CREDIT") sendData["isReimbursement"] = reimbursement === 1 ? true : false;
    setLoading(true);
    try {
      const { data } = await axios.post(link, sendData, header);
      fetchData();
      setCompleted(true);
      if (data.code === 100) setCompleted(true);
    } catch (err) {
      alert(language.PROBLEM_IN_RECHARGE);
    }
    setLoading(false);
  };
  const close = () => {
    gsap.fromTo(".viewRechargePop", 0.3, { opacity: 1 }, { opacity: 0, stagger: 0.1 });
    setTimeout(() => setShowRechargePage(false), 300);
  };
  React.useEffect(() => {
    gsap.fromTo(".viewRechargePop", 0.3, { opacity: 0, y: 20 }, { opacity: 1, y: 0, stagger: 0.3 });
  }, []);
  return (
    <div className="fixed inset-0 flex justify-center items-center opacity-0 viewRechargePop" style={{ zIndex: 30 }}>
      <div className="fixed inset-0 bg-gray-900 opacity-75" onClick={() => close()} style={{ zIndex: 20 }}></div>
      <div className="px-5 py-3 dark:bg-gray-800 bg-white rounded-xl opacity-0 viewRechargePop" style={{ zIndex: 30 }}>
        {completed ? (
          <div className="px-3 py-2">
            <h1 className="text-xl font-bold">{language[type] + " " + language.SUCCESSFULL}</h1>
            <Lottie
              options={{
                animationData: SuccessJSON,
                loop: false,
                autoplay: true,
                rendererSettings: {
                  preserveAspectRatio: "xMidYMid slice",
                },
              }}
              isPaused={false}
              isStopped={false}
              height={200}
            />
            <button
              onClick={() => close()}
              className="dark:bg-gray-900 bg-gray-100 px-3 py-2 w-full focus:outline-none rounded-md text-sm"
            >
              {language.CLOSE}
            </button>
          </div>
        ) : (
          <>
            <h1 className="my-2 text-sm text-xl font-bold">{userDetails.phone}</h1>
            <h1 className="my-2 text-sm">{language.AMOUNT} :</h1>
            <input
              type="number"
              onKeyDown={(e) => e.key === "Enter" && amount > 0 && recharge()}
              className="focus:outline-none rounded-xl transition p-5 text-center text-5xl dark:bg-gray-900 bg-gray-200 dark:text-gray-100 text-gray-900 w-auto"
              style={{ width: 250 }}
              placeholder="0"
              onChange={(e) => setAmount(e.target.value)}
              value={amount}
            />
            <h1 className="my-2 text-sm">{language.REASON} :</h1>
            <TextArea
              // error={formik.errors.forProfessional && formik.errors.forProfessional.description}
              change={(e) => setReason(e)}
              value={reason}
            />
            {type === "CREDIT" && (
              <div className="flex items-center">
                <ToggleButton defaultValue={reimbursement} change={(e) => setReImbursement(e)} />
                <p className="mx-2 text-sm text-gray-500">{language.REIMBURSEMENT}</p>
              </div>
            )}
            <div className="flex mt-3">
              <div className="w-1/2 px-1">
                <button
                  onClick={() => close()}
                  className="dark:bg-gray-900 bg-gray-100 px-3 py-2 w-full focus:outline-none rounded-md text-sm"
                >
                  {language.CANCEL}
                </button>
              </div>
              <div className="w-1/2 px-1">
                <button
                  onClick={() => amount > 0 && recharge()}
                  className="bg-blue-800 px-3 py-2 w-full text-white focus:outline-none rounded-md text-sm"
                >
                  {loading ? language.LOADING : language.CONFIRM}
                </button>
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default WalletRecharge;
