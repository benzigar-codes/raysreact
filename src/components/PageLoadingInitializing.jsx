import React from "react";

export default function PageLoading() {
  return (
    <div className="text-xl bg-gray-800 text-white min-h-screen flex justify-center items-center">
      <div className="animate-pulse">
        <div className="flex items-center">
          <h1>Initializing ... </h1>
        </div>
      </div>
    </div>
  );
}
