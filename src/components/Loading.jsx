import React from "react";
import { GiCarWheel } from "react-icons/gi";
import Logo from "../assets/images/Front/logoLight.svg";

export default function PageLoading() {
  return (
    <div className="text-5xl bg-gray-800 text-white min-h-screen flex justify-center items-center">
      <div className="animate-pulse">
        <div className="flex items-center">
          {/* <GiCarWheel
            style={{ fontSize: 100 }}
            className="transition animate-spin text-5xl"
          /> */}
          <img src={Logo} alt="" />
        </div>
      </div>
    </div>
  );
}
