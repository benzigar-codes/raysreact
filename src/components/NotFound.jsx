import React from "react";
import { Link } from "react-router-dom";
import NavLinks from "../utils/navLinks.json";

export default function NotFound({ history }) {
  return (
    <div className="h-screen bg-blue-800 text-white dark:bg-gray-800 flex justify-center items-center">
      <div>
        <h1 className="flex flex-col justify-center items-center text-xl xl:text-5xl font-bold">404</h1>
        <p className="text-sm mt-2">We think you landed on the wrong page !!</p>
        <p onClick={() => history.push(NavLinks.ADMIN_LOGIN)} className="text-sm mt-2 text-center">
          Go to Admin Page !{" "}
        </p>
      </div>
    </div>
  );
}
