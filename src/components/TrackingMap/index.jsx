import { Loader } from "@googlemaps/js-api-loader";
import { format } from "date-fns";
import React from "react";
import axios from "axios";

import A from "../../utils/API.js";

import useDebug from "../../hooks/useDebug";
import useLanguage from "../../hooks/useLanguage";
import useSettings from "../../hooks/useSettings";
import useSocket from "../../hooks/useSocket";
import useAdmin from "../../hooks/useAdmin";

const TrackingMap = ({ user = false, professional = false, responseOfficer = false }) => {
  const userInterval = React.useRef();
  const { language } = useLanguage();
  const professionalInterval = React.useRef();
  const officerInterval = React.useRef();
  const [googleLoaded, setGoogleLoaded] = React.useState(false);
  const { settings } = useSettings();
  const [map, setMap] = React.useState(null);
  const mapRef = React.useRef(null);
  const { header, authFailure } = useAdmin();
  const { notificationSocket } = useSocket();
  const [locations, setLocations] = React.useState([]);
  const [firstTime, setFirstTime] = React.useState(true);

  const [userLocation, setUserLocation] = React.useState(null);
  const [professionalLocation, setProfessionalLocation] = React.useState(null);
  const [officerLocation, setOfficerLocation] = React.useState(null);

  const secondsInterval = 3000;

  const [markers, setMarkers] = React.useState([]);

  React.useEffect(() => {
    markers.map((each) => each.setMap(null));
    if (userLocation && map !== null) {
      const marker = new window.google.maps.Marker({
        map,
        position: { lat: userLocation.lat, lng: userLocation.lng },
      });
      const infowindow = new window.google.maps.InfoWindow({
        content: `
          <div>
          <h1 style="color:black;font-size:20px;">${userLocation.name}</h1>
          <p style="color:black;margin-top:5px;">${userLocation.phone}</p>
          <p style="color:black;margin-top:5px;">${language.LAST_UPDATED} : ${userLocation.date}</p>
          </div>
          `,
      });
      marker.addListener("click", () => {
        infowindow.open(map, marker);
      });
      setMarkers([...markers, marker]);
    }
    if (professionalLocation && map !== null) {
      const marker = new window.google.maps.Marker({
        map,
        position: { lat: professionalLocation.lat, lng: professionalLocation.lng },
      });
      const infowindow = new window.google.maps.InfoWindow({
        content: `
          <div>
          <h1 style="color:black;font-size:20px;">${professionalLocation.name}</h1>
          <p style="color:black;margin-top:5px;">${professionalLocation.phone}</p>
          <p style="color:black;margin-top:5px;">${language.LAST_UPDATED} : ${professionalLocation.date}</p>
          </div>
          `,
      });
      marker.addListener("click", () => {
        infowindow.open(map, marker);
      });
      setMarkers([...markers, marker]);
    }
    if (officerLocation && map !== null) {
      const marker = new window.google.maps.Marker({
        map,
        position: { lat: officerLocation.lat, lng: officerLocation.lng },
      });
      const infowindow = new window.google.maps.InfoWindow({
        content: `
          <div>
          <h1 style="color:black;font-size:20px;">${officerLocation.name}</h1>
          <p style="color:black;margin-top:5px;">${officerLocation.phone}</p>
          <p style="color:black;margin-top:5px;">${language.LAST_UPDATED} : ${officerLocation.date}</p>
          </div>
          `,
      });
      marker.addListener("click", () => {
        infowindow.open(map, marker);
      });
      setMarkers([...markers, marker]);
    }
  }, [userLocation, professionalLocation, officerLocation, map]);

  React.useEffect(() => {
    if (map !== null && markers.length > 0) {
      var bounds = new window.google.maps.LatLngBounds();
      const locations = [];
      if (userLocation) locations.push({ lat: userLocation.lat, lng: userLocation.lng });
      if (professionalLocation) locations.push({ lat: professionalLocation.lat, lng: professionalLocation.lng });
      if (officerLocation) locations.push({ lat: officerLocation.lat, lng: officerLocation.lng });
      if (locations.length === 2 && firstTime === true) {
        locations.forEach((location) => bounds.extend({ lat: location.lat, lng: location.lng }));
        map.fitBounds(bounds);
        setFirstTime(false);
      } else {
        if (locations.length === 1) {
          map.setCenter({ lat: locations[0].lat, lng: locations[0].lng });
        }
      }
      // firstTime ? setTimeout(() => map.setZoom(16), 200) : map.setZoom(16);
    }
  }, [markers, map, userLocation, professionalLocation, officerLocation]);

  React.useEffect(() => {
    const loader = new Loader({
      apiKey: settings.mapApi.web,
      version: "weekly",
      libraries: ["places"],
    });
    loader
      .load()
      .then(() => {
        setGoogleLoaded(true);
      })
      .catch((e) => {
        // alert(e);
      });
  }, []);

  const fetchUsers = async () => {
    if (user._id) {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_LOCATION_USER, { userId: user._id }, header);
        setUserLocation(
          data && data.data && data.data.lat !== 0
            ? {
                name: user.firstName + " " + user.lastName,
                phone: user?.phone?.code + " " + user?.phone?.number,
                lat: data.data.lat,
                lng: data.data.lng,
                date: format(new Date(data.data.locationUpdatedTime), "PP p"),
              }
            : userLocation
        );
      } catch (err) {
        // alert(err);
        authFailure(err);
      }
    }
  };

  const fetchProfessionals = async () => {
    if (professional._id) {
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_LOCATION_PROFESSIONAL,
          { professionalId: professional._id },
          header
        );
        setProfessionalLocation(
          data && data.data && data.data.lat !== 0
            ? {
                name: professional.firstName + " " + professional.lastName,
                phone: professional?.phone?.code + " " + professional?.phone?.number,
                lat: data.data.lat,
                lng: data.data.lng,
                date: format(new Date(data.data.locationUpdatedTime), "PP p"),
              }
            : professionalLocation
        );
      } catch (err) {
        // alert(err);
        authFailure(err);
      }
    }
  };

  const fetchOfficer = async () => {
    if (responseOfficer._id) {
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_LOCATION_OFFICER,
          { officerId: responseOfficer._id },
          header
        );
        setOfficerLocation(
          data && data.data && data.data.lat !== 0
            ? {
                name: responseOfficer.firstName + " " + responseOfficer.lastName,
                phone: responseOfficer?.phone?.code + " " + responseOfficer?.phone?.number,
                lat: data.data.lat,
                lng: data.data.lng,
                date: format(new Date(data.data.locationUpdatedTime), "PP p"),
              }
            : officerLocation
        );
      } catch (err) {
        // alert(err);
        authFailure(err);
      }
    }
  };

  React.useEffect(() => {
    fetchUsers();
    fetchProfessionals();
    fetchOfficer();
    let userInterval = setInterval(fetchUsers, secondsInterval);
    let professionlInterval = setInterval(fetchProfessionals, secondsInterval);
    let officerInterval = setInterval(fetchOfficer, secondsInterval);
    return () => {
      clearInterval(userInterval);
      clearInterval(professionlInterval);
      clearInterval(officerInterval);
    };
  }, []);

  // React.useEffect(() => {
  //   if (professional._id) {
  //     const fetchProfessionalLocation = () => {
  //       notificationSocket.current.emit("trackProfessionalLocation", { professionalId: professional._id });
  //       notificationSocket.current.on("trackProfessionalLocation", (data) => {
  //         // console.log("professional", data);
  //         setProfessionalLocation(
  //           data && data.payload && data.payload.data && data.payload.data.lat !== 0
  //             ? {
  //                 name: professional.firstName + " " + professional.lastName,
  //                 phone: professional?.phone?.code + " " + professional?.phone?.number,
  //                 lat: data.payload.data.lat,
  //                 lng: data.payload.data.lng,
  //                 date: format(new Date(), "p"),
  //               }
  //             : professionalLocation
  //         );
  //       });
  //     };
  //     fetchProfessionalLocation();
  //     professionalInterval.current = setInterval(fetchProfessionalLocation, secondsInterval);
  //   }
  //   if (user._id) {
  //     const fetchUerLocation = () => {
  //       notificationSocket.current.emit("trackUserLocation", { userId: user._id });
  //       notificationSocket.current.on("trackUserLocation", (data) => {
  //         // console.log("user", data);
  //         setUserLocation(
  //           data && data.payload && data.payload.data && data.payload.data.lat !== 0
  //             ? {
  //                 name: user.firstName + " " + user.lastName,
  //                 phone: responseOfficer?.phone?.code + " " + user?.phone?.number,
  //                 lat: data.payload.data.lat,
  //                 lng: data.payload.data.lng,
  //                 date: format(new Date(), "p"),
  //               }
  //             : userLocation
  //         );
  //       });
  //     };
  //     fetchUerLocation();
  //     userInterval.current = setInterval(fetchUerLocation, secondsInterval);
  //   }
  //   if (responseOfficer._id) {
  //     const fetchOfficerLocation = () => {
  //       notificationSocket.current.emit("trackOfficerLocation", { officerId: responseOfficer._id });
  //       notificationSocket.current.on("trackOfficerLocation", (data) => {
  //         // console.log("officer", data);
  //         setOfficerLocation(
  //           data && data.payload && data.payload.data && data.payload.data.lat !== 0
  //             ? {
  //                 name: responseOfficer.firstName + " " + responseOfficer.lastName,
  //                 phone: responseOfficer?.phone?.code + " " + responseOfficer?.phone?.number,
  //                 lat: data.payload.data.lat,
  //                 lng: data.payload.data.lng,
  //                 date: format(new Date(), "p"),
  //               }
  //             : officerLocation
  //         );
  //       });
  //     };
  //     fetchOfficerLocation();
  //     officerInterval.current = setInterval(fetchOfficerLocation, secondsInterval);
  //   }
  // }, [user, professional, responseOfficer]);

  React.useEffect(() => {
    if (googleLoaded === true && (userLocation || professionalLocation || officerLocation)) {
      if (map === null) {
        const googleMap = new window.google.maps.Map(mapRef.current, {
          center: { lat: 12.9791551, lng: 80.2007085 },
          zoom: 16,
          zoomControl: true,
          mapTypeControl: false,
          scaleControl: true,
          streetViewControl: false,
          rotateControl: false,
          fullscreenControl: true,
        });
        googleMap.setZoom(16);
        setMap(googleMap);
      }
    }
  }, [googleLoaded, map, officerLocation, professionalLocation, userLocation]);
  return googleLoaded ? (
    <div
      className="rounded-xl bg-gray-100 dark:bg-gray-900 flex justify-center items-center"
      ref={mapRef}
      id="requestMap"
      style={{ height: 400 }}
    >
      <h1>{language.NO_LOCATION_INFO}</h1>
    </div>
  ) : null;
};

export default TrackingMap;
