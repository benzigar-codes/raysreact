import React from "react";
import { useDropzone } from "react-dropzone";
import Cropper from "cropperjs";
import "cropperjs/dist/cropper.css";
import { FiCheckSquare } from "react-icons/fi";
import useLanguage from "../../hooks/useLanguage";

export const ImageCropper = ({ animate, src, setTargetSrc, setShowCrop, ratio }) => {
  let cropper;
  const direction = document.getElementsByTagName("html")[0].getAttribute("dir");
  const CropImageRef = React.useRef(null);
  const [temp, setTemp] = React.useState(null);

  const cropComplete = () => {
    setTargetSrc(temp);
    setShowCrop(false);
  };

  React.useEffect(() => {
    if (src) {
      cropper = new Cropper(CropImageRef.current, {
        aspectRatio: ratio,
        guides: "true",
        crop: () => {
          const canvas = cropper.getCroppedCanvas();
          setTemp(canvas.toDataURL("image/png"));
        },
      });
    }
  }, [src]);

  return (
    <div>
      <div className={"fixed inset-0 z-40 text-white flex justify-center items-center"}>
        <div className="flex flex-col justify-center items-center">
          <div className="mx-auto">
            <img
              ref={CropImageRef}
              id="crop"
              guides={false}
              dragMode={"move"}
              style={{ height: 400, width: "auto" }}
              src={src}
              alt=""
              crossOrigin="true"
            />
          </div>
          <button onClick={cropComplete} className="flex items-center bg-green-800 mt-2 px-3 py-2">
            <FiCheckSquare />
            <p className={direction === "ltr" ? "ml-2" : "mr-2"}>Done</p>
          </button>
        </div>
      </div>
      <div className="fixed inset-0 bg-black opacity-50 z-30"></div>
    </div>
  );
};

export const FileUpload = ({
  animate,
  accept = ["image/jpeg", "image/png", "image/jpg"],
  ratio = 0,
  change = () => {},
  padding = false,
  crop = true,
  margin = true,
  defaultValue = "",
}) => {
  const [file, setFile] = React.useState(null);
  const { language } = useLanguage();

  const [mainImage, setMainImage] = React.useState(null);

  const [showCrop, setShowCrop] = React.useState(false);
  const [cropSrc, setCropSrc] = React.useState(null);

  const cropComplete = (e) => {
    change(e);
    setMainImage(e);
  };

  React.useEffect(() => {
    if (defaultValue !== "" && defaultValue !== null) {
      setMainImage(defaultValue);
      setFile(true);
    }
  }, []);

  const onDrop = React.useCallback(async (acceptedFiles) => {
    const ifTypeMatched = accept
      .map((targetFormat) => (acceptedFiles[0].type === targetFormat ? true : false))
      .filter((matchedFileType) => matchedFileType === true);
    if (ifTypeMatched.length > 0) {
      if (crop === true) {
        setCropSrc(URL.createObjectURL(acceptedFiles[0]));
        setShowCrop(true);
        setFile(true);
      } else {
        const reader = new FileReader();
        reader.readAsDataURL(acceptedFiles[0]);
        reader.onload = () => {
          change(reader.result);
        };
        setMainImage(URL.createObjectURL(acceptedFiles[0]));
        setFile(true);
      }
    }
  }, []);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  return (
    <>
      {showCrop === true && (
        <ImageCropper
          ratio={ratio}
          animate={animate}
          setTargetSrc={cropComplete}
          setShowCrop={setShowCrop}
          src={cropSrc}
        />
      )}
      <div
        className={`${animate && "opacity-0 " + animate} ${
          file == null &&
          "w-full focus:outline-none border-2 border-dashed dark:border-gray-700 text-gray-400 text-center text-sm"
        } cursor-pointer ${padding === true && "p-5"}`}
        {...getRootProps()}
      >
        {file != null && (
          <div
            className={`${
              margin === true && "mx-3"
            } focus:outline-none flex items-center dark:border-gray-700 justify-center focus:outline-none text-sm text-center text-gray-800 p-3 border-2 border-dashed`}
          >
            <div>
              <img
                className="w-full object-cover full"
                src={mainImage}
                // onError={setFile(null)}
                alt=""
              />
            </div>
          </div>
        )}
        <input {...getInputProps()} accept={accept.join(",")} />
        {file === null && (
          <div className="dark:text-gray-400 p-5">
            {isDragActive ? (
              <p>{language.DROP_FILES}</p>
            ) : (
              <>
                <p>{language.SELECT_FILES}</p>
                <p>
                  {language.SUPPORTED_FORMATS}
                  {accept.map((type) => type.split("/")[1]).join(", ")}
                </p>
              </>
            )}
          </div>
        )}
      </div>
    </>
  );
};
