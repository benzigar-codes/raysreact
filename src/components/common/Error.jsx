import React from "react";
import { FiInfo } from "react-icons/fi";

export const Error = ({ title }) => {
  return (
    <div className="flex bg-blue-800 text-white items-center my-2 px-3 py-2 justify-between">
      <p className="fade flex items-center text-sm">
        <FiInfo />
        <p className="mx-3">{title}</p>
      </p>
    </div>
  );
};
