import React from "react";
import {
  FiChevronLeft,
  FiChevronRight,
  FiDownloadCloud,
  FiHome,
  FiPlus,
  FiRefreshCw,
  FiSearch,
  FiX,
} from "react-icons/fi";
import { TextField } from "./TextField";
import useLanguage from "../../hooks/useLanguage";
import { useHistory } from "react-router-dom";
import NavLinks from "../../utils/navLinks.json";
import gsap from "gsap/gsap-core";
import { RiUserStarLine } from "react-icons/ri";
import { AiOutlineFileExcel, AiOutlineFilePdf } from "react-icons/ai";
import OutsideClickHandler from "react-outside-click-handler";

export const TableWrapperRight = ({
  title,
  showSearch = true,
  showAdd = true,
  searchChange = () => {},
  searchHint = "",
  searchPlaceHolder = null,
  clearSearch = () => {},
  children,
  total = 0,
  showRestore = false,
  refreshLoading = false,
  showBulkActive = true,
  showBulkInActive = true,
  showBulkArchieve = true,
  showBulkAttend = false,
  showBulkDelete = true,
  downloadLoading = false,
  showDownload = false,
  downloadClick = () => {},
  showNext = false,
  showPrev = false,
  showPrivilege = false,
  privilegeClick = () => {},
  bulkActive = () => {},
  bulkArchive = () => {},
  bulkDelete = () => {},
  bulkInActive = () => {},
  bulkRestore = () => {},
  bulkAttend = () => {},
  width = "10/12",
  searchText = "",
  bulkEdit = true,
  className,
  refreshClick = () => {},
  addClick = () => {},
  searchSubmit = () => {},
  icon = "",
  selected = [],
  bread = [],
  animate,
}) => {
  const { language } = useLanguage();
  const history = useHistory();

  const [showDownloadOption, setShowDownloadOption] = React.useState(false);

  React.useEffect(() => {
    gsap.fromTo(".tablewrapper", 0.5, { opacity: 0 }, { opacity: 1, delay: 0.5, stagger: 0.1 });
  }, []);
  return (
    <div
      className={`w-full lg:w-${width} h-full p-4 dark:bg-gray-800 overflow-y-hidden ${className} opacity-0 tablewrapper`}
    >
      <div className="flex justify-between items-center">
        <div className="text-base md:text-lg lg:text-xl flex items-center truncate">
          <div
            onClick={() => history.push(NavLinks.ADMIN_HOME)}
            className={
              document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
                ? "text-green-800 dark:text-gray-200 flex items-center cursor-pointer hover:text-blue-800"
                : "text-green-800 dark:text-gray-200 flex items-center cursor-pointer hover:text-blue-800"
            }
          >
            {<FiHome />}
            {document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? (
              <FiChevronRight />
            ) : (
              <FiChevronLeft />
            )}
          </div>
          <h1 className="flex items-center">
            {bread.map((el) => (
              <span
                onClick={() => history.push(el.path)}
                className="flex items-center text-gray-500 cursor-pointer hover:underline text-green-800 dark:text-gray-200"
              >
                {el.title}{" "}
                {document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? (
                  <FiChevronRight />
                ) : (
                  <FiChevronLeft />
                )}
              </span>
            ))}
            <span className="flex items-center text-blue-800">{title} </span>
            {total !== 0 && (
              <span className="text-gray-400 mx-2" style={{ fontSize: 14 }}>
                ({total})
              </span>
            )}
          </h1>
        </div>
        <div className="flex items-center">
          {selected.length > 0 && bulkEdit === true && (
            <div
              className={`relative flex bg-gray-200 dark:bg-gray-900 p-2 rounded-full ${
                document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
              }`}
            >
              {showRestore === false && (
                <>
                  {" "}
                  {showBulkActive === true && (
                    <button
                      onClick={bulkActive}
                      data-title={language.ACTIVE}
                      className={`bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-1 px-3 ${
                        document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                      }`}
                    >
                      <p style={{ fontSize: 13 }}>{language.ACTIVE}</p>
                    </button>
                  )}
                  {showBulkInActive === true && (
                    <button
                      onClick={bulkInActive}
                      data-title={language.INACTIVE}
                      className={`bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-1 px-3 ${
                        document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                      }`}
                    >
                      <p style={{ fontSize: 13 }}>{language.INACTIVE}</p>
                    </button>
                  )}
                  {showBulkAttend === true && (
                    <button
                      onClick={bulkAttend}
                      data-title={language.ATTENDED}
                      className={`bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-1 px-3 ${
                        document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                      }`}
                    >
                      <p style={{ fontSize: 13 }}>{language.ATTENDED}</p>
                    </button>
                  )}
                  {showBulkArchieve === true && (
                    <button
                      onClick={bulkArchive}
                      data-title={language.ARCHIEVE}
                      className={`bg-purple-500 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-1 px-3 ${
                        document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                      }`}
                    >
                      <p style={{ fontSize: 13 }}>{language.ARCHIEVE}</p>
                    </button>
                  )}
                </>
              )}
              {showRestore === true && (
                <button
                  onClick={bulkRestore}
                  data-title={language.RESTORE}
                  className={`bg-purple-500 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-1 px-3 ${
                    document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                  }`}
                >
                  <p style={{ fontSize: 13 }}>{language.RESTORE}</p>
                </button>
              )}
              {/* {showBulkDelete === true && (
                <button
                  onClick={bulkDelete}
                  data-title={language.DELETE}
                  className={`bg-red-500 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-1 px-3 ${
                    document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                  }`}
                >
                  <p style={{ fontSize: 13 }}>{language.DELETE}</p>
                </button>
              )} */}
            </div>
          )}
          {showDownload === true && (
            <div className="relative">
              <button
                onClick={() => setShowDownloadOption(true)}
                data-title={language.DOWNLOAD}
                className={`${
                  downloadLoading === true && "animate-spin"
                } bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                  document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
                }`}
              >
                <FiDownloadCloud />
              </button>
              {showDownloadOption && (
                <OutsideClickHandler onOutsideClick={() => setShowDownloadOption(false)}>
                  <div
                    className="absolute shadow-xl flex flex-col right-0 bg-white dark:bg-gray-900 rounded-xl"
                    style={{ top: "130%", zIndex: 10 }}
                  >
                    {/* <div
                      onClick={() => {
                        downloadClick("PDF");
                        setShowDownloadOption(false);
                      }}
                      className="opacity-50 cursor-pointer px-3 py-3 bg-white dark:bg-gray-900 flex items-center"
                    >
                      <AiOutlineFilePdf className="text-4xl" />
                      <h1 className="text-sm mx-2">Pdf</h1>
                    </div> */}
                    <div
                      onClick={() => {
                        downloadClick("EXCEL");
                        setShowDownloadOption(false);
                      }}
                      className="cursor-pointer hover:bg-gray-500 px-3 py-3 bg-white dark:bg-gray-900 flex items-center"
                    >
                      <AiOutlineFileExcel className="text-4xl" />
                      <h1 className="text-sm mx-2">Excel</h1>
                    </div>
                  </div>
                </OutsideClickHandler>
              )}
            </div>
          )}
          <button
            onClick={refreshClick}
            data-title={language.REFRESH}
            className={`${
              refreshLoading === true && "animate-spin"
            } bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
              document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
            }`}
          >
            <FiRefreshCw />
          </button>
          {showAdd === true && (
            <button
              onClick={addClick}
              data-title={language.ADD}
              className={`bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
              }`}
            >
              <FiPlus />
            </button>
          )}
          {showPrivilege === true && (
            <button
              onClick={privilegeClick}
              data-title={language.PRIVILIGES}
              className={`bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
              }`}
            >
              <RiUserStarLine />
            </button>
          )}
          {showSearch === true && (
            <>
              <TextField
                padding={false}
                onKeyDown={(e) => {
                  if (e.key === "Enter") searchSubmit();
                }}
                iconSelect={searchSubmit}
                change={(e) => searchChange(e)}
                hint={searchHint}
                icon={<FiSearch />}
                placeholder={searchPlaceHolder ? searchPlaceHolder : language.SEARCH}
                value={searchText}
              />
              {searchText != "" && (
                <button
                  onClick={clearSearch}
                  data-title={language.NO}
                  className={`bg-red-500 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                    document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "ml-2" : "mr-2"
                  }`}
                >
                  <FiX />
                </button>
              )}
            </>
          )}
        </div>
      </div>
      <div className="flex">{children}</div>
    </div>
  );
};
