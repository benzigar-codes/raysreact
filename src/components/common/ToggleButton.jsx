import React from "react";
import { RiToggleFill, RiToggleLine } from "react-icons/ri";

export const ToggleButton = ({ defaultValue = 0, change = () => {}, className, ...rest }) => {
  const toggle = defaultValue;
  const toggleData = (e) => {
    change(e);
  };

  return toggle === 1 ? (
    <RiToggleFill
      onClick={() => toggleData(0)}
      className={`cursor-pointer text-3xl text-gray-400 text-blue-800 ${className}`}
      {...rest}
    />
  ) : (
    <RiToggleLine
      onClick={() => toggleData(1)}
      className={`cursor-pointer text-3xl text-gray-400 ${className}`}
      {...rest}
    />
  );
};
