import React from "react";

export default function FlexCol({ children, className, ...rest }) {
    return (
        <div className={"flex flex-col " + className} {...rest}>
            {children}
        </div>
    );
}
