import React from "react";

export const Heading = ({ title, ...rest }) => (
  <h1 className="mx-4 text-lg text-blue-800" {...rest}>
    {title}
  </h1>
);
