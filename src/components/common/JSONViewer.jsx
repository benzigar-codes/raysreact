import React from "react";
import { Border } from "./Border";
import { Heading } from "./Heading";
import ReactJson from "react-json-view";
import useAdmin from "../../hooks/useAdmin";

export default function JSONViewer({ data = {} }) {
  const [expand, setExpand] = React.useState(true);
  const { admin } = useAdmin();
  return admin.userType === "DEVELOPER" ? (
    <Border>
      <Heading title={"JSON"} />
      <p className="mx-3 my-5" onClick={() => setExpand(!expand)}>
        {expand ? "Expand" : "Collapse"}
      </p>
      <ReactJson
        theme="ocean"
        displayDataTypes={false}
        iconStyle="square"
        collapsed={expand === true ? 1 : false}
        src={data}
        style={{ padding: 20, borderRadius: "10" }}
      />
    </Border>
  ) : null;
}
