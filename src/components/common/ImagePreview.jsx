import useLanguage from "../../hooks/useLanguage";
const ImagePreview = ({ show = false, close = () => {} }) => {
  const { language } = useLanguage();
  return show !== false ? (
    <div className="fixed inset-0 flex justify-center items-center z-50">
      <a className={"z-50"} href={show} target="_blank" rel={"noreferrer"} alt={""}>
        <img
          src={show}
          className={"bg-blue-800 object-cover z-50 p-4"}
          style={{
            maxHeight: 500,
          }}
          alt=""
        />
      </a>
      <button
        onClick={close}
        className={"bg-blue-800 px-4 py-2 rounded-full text-white z-50 fixed"}
        style={{ bottom: 20 }}
      >
        {language.CLOSE}
      </button>
      <div onClick={() => close()} className="fixed inset-0 bg-gray-900 z-30 opacity-75"></div>
    </div>
  ) : null;
};

export default ImagePreview;
