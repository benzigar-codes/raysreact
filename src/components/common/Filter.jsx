import React from "react";
import gsap from "gsap";
import useLanguage from "../../hooks/useLanguage";
import { TextFormat } from "./TextFormat";
import ReactDatePicker from "react-datepicker";
import { ToggleButton } from "./ToggleButton";

const CustomDateComponent = React.forwardRef(({ value, onClick }, ref) => (
  <button
    className={`w-full dark:bg-gray-900 dark:text-white text-sm bg-gray-100 outline-none rounded-xl px-3 py-2 border-b-2 border-gray-100 dark:border-gray-800 focus:border-blue-800 text-sm dark:focus:border-blue-800 cursor-pointer`}
    onClick={onClick}
    style={{ fontSize: 12 }}
    ref={ref}
  >
    {value}
  </button>
));

export const Filter = ({ title, fields, defaultValue = "", type = "dropDown", change = () => {} }) => {
  const { language } = useLanguage();
  const [customDateActive, setCustomDateActive] = React.useState(false);
  const [customDates, setCustomDates] = React.useState([new Date(), new Date()]);

  React.useEffect(() => {
    if (customDateActive) {
      change(customDates);
    } else change(["", ""]);
  }, [customDateActive, customDates]);

  React.useEffect(() => {
    gsap.fromTo(".filter", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, []);
  return (
    <div className="mb-3 space-y-2 opacity-0 filter">
      <div className="flex justify-between items-center">
        <h1 className="font-bold text-md text-gray-500 dark:text-gray-400 opacity-0 filter">{title}</h1>
        {type === "toggle" && (
          <ToggleButton
            change={(e) => change(e === 1 ? true : false)}
            defaultValue={defaultValue ? 1 : 0}
          />
        )}
        {type === "date" && (
          <ToggleButton
            change={(e) => setCustomDateActive(e === 1 ? true : false)}
            defaultValue={customDateActive ? 1 : 0}
          />
        )}
      </div>
      <div className="flex items-center mb-1 cursor-pointer opacity-0 filter">
        {type === "dropDown" && (
          <select
            onChange={(e) => change(e.target.value)}
            name=""
            id=""
            className="w-full dark:bg-gray-900 dark:text-gray-300 text-sm focus:outline-none bg-gray-100 rounded-xl px-4 py-3"
          >
            {fields.map((page, index) => (
              <option selected={defaultValue === page ? "selected" : ""} key={index} value={page}>
                {page}
              </option>
            ))}
          </select>
        )}
        {type === "date" && (
          <ReactDatePicker
            selectsRange={true}
            startDate={customDates[0]}
            endDate={customDates[1]}
            customInput={<CustomDateComponent />}
            onChange={(update) => {
              setCustomDates(update);
              setCustomDateActive(true);
            }}
            withPortal
          />
        )}
      </div>
    </div>
  );
};
