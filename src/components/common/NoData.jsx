import React from "react";

export const NoData = () => <p className="flex justify-center w-full">No Data</p>;
