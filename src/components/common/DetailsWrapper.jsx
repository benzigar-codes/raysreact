import React from "react";

export const DetailsWrapper = ({ children, animate }) => (
  <div className={`w-full flex flex-wrap ${animate && "opacity-0 " + animate}`}>{children}</div>
);
