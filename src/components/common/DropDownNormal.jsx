import React from "react";
import { FiChevronDown } from "react-icons/fi";
import useLanguage from "../../hooks/useLanguage";

export const DropdownNormal = ({
  width = "full",
  error = "",
  type,
  icon = <FiChevronDown />,
  hint = "",
  fields = [],
  change = () => {},
  placeholder,
  defaultValue = "",
  className,
  selectText = "",
  ...rest
}) => {
  const direction = document.getElementsByTagName("html")[0].getAttribute("dir");
  const { language } = useLanguage();
  return (
    <div className="flex flex-col w-full">
      {/* // <div className={`relative w-${width}`}> */}
      <select
        onChange={(e) => change(e.target.value)}
        className={`w-${width} dark:bg-gray-900 bg-gray-100 outline-none rounded-xl px-4 py-3 border-b-2 border-gray-100 dark:border-gray-800 focus:border-blue-800 text-sm dark:focus:border-blue-800 cursor-pointer ${
          error && "border-2 border-red-500 dark:border-red-500"
        }`}
      >
        {/* <optgroup className="h-72"> */}
        <option disabled selected value>
          {selectText !== "" ? selectText : language.SELECT}
        </option>
        {fields.map((field) => (
          <option selected={defaultValue === field.label ? "selected" : false} key={field.id} value={field.value}>
            {field.label}
          </option>
        ))}
        {/* </optgroup> */}
      </select>
      {/* {icon && (
        <div
          className={`absolute ${
            direction === "ltr" ? "right-0" : "left-0"
          } top-0 bottom-0 text-gray-500 flex justify-center items-center mx-3 rounded-xl ${
            error != "" && "text-red-800"
          }`}
        >
          {icon}
        </div>
      )} */}
      {hint && error === "" && (
        <div className="flex">
          <div className="mx-2">
            {hint && (
              <p className="text-gray-300 dark:text-gray-500 text-sm mt-1" style={{ fontSize: 11 }}>
                {hint}
              </p>
            )}
          </div>
        </div>
      )}
      {error != "" && (
        <div className="flex">
          <div className="mx-2">
            {error && (
              <p className="text-red-600 dark:text-red-500 text-sm mt-1" style={{ fontSize: 11 }}>
                {error}
              </p>
            )}
          </div>
        </div>
      )}
      {/* // </div> */}
    </div>
  );
};
