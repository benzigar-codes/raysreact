import React from "react";

export const Border = ({ children, animate = null, ...props }) => {
  return (
    <div
      className={` ${animate && "opacity-0 " + animate} border-2 py-3 px-2 rounded-xl mb-5 dark:border-gray-700`}
      {...props}
    >
      {children}
    </div>
  );
};
