import React from "react";
import NumberFomat from "react-number-format";

export const TextFormat = ({
  width = "full",
  error = "",
  type = "text",
  required = true,
  icon = "",
  hint = "",
  iconSelect = () => {},
  change = () => {},
  placeholder = "",
  className = "",
  padding = true,
  margin = 0,
  ...rest
}) => {
  const direction = document.getElementsByTagName("html")[0].getAttribute("dir");
  return (
    <div
      className={`relative w-${width} overflow-hidden rounded-xl ${
        document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? `mr-${margin}` : `ml-${margin}`
      }`}
    >
      {icon != "" && (
        <div
          onClick={iconSelect}
          className={`absolute cursor-pointer ${
            direction === "ltr" ? "right-0" : "left-0"
          } text-gray-400 flex justify-center items-center ${
            error != "" && "border-2 border-red-500 text-white dark:placeholder-white"
          }`}
        >
          <span
            className={`bg-gray-100 p-3 dark:bg-gray-900 ${
              error != "" && "border-2 border-red-500 text-white dark:placeholder-white"
            }`}
          >
            {icon}
          </span>
        </div>
      )}
      <NumberFomat
        onChange={(e) => change(e.target.value)}
        type={type}
        name=""
        required={required}
        placeholder={placeholder}
        className={
          className +
          ` w-full dark:bg-gray-900 text-sm focus:outline-none bg-gray-100 dark:text-white rounded-xl px-4 py-3 ${
            error != "" && "border-2 border-red-500 placeholder-red-500"
          }`
        }
        id=""
        {...rest}
      />
      {hint && error === "" && (
        <div className="w-full flex">
          <div className="mx-2">
            {hint && (
              <p className="text-gray-400 dark:text-gray-500 text-sm mt-1" style={{ fontSize: 11 }}>
                {hint}
              </p>
            )}
          </div>
        </div>
      )}
      {error != "" && (
        <div className="flex">
          <div className="mx-2">
            {error && (
              <p className="text-red-600 dark:text-red-500 text-sm mt-1" style={{ fontSize: 11 }}>
                {error}
              </p>
            )}
          </div>
        </div>
      )}
    </div>
  );
};
