import React from "react";
import { FiChevronDown } from "react-icons/fi";

export const Dropdown = ({
  width = "full",
  error = "",
  type,
  icon = <FiChevronDown />,
  hint = "",
  fields = [],
  change = () => {},
  placeholder,
  defaultValue = "",
  className,
  ...rest
}) => {
  const [show, setShow] = React.useState(false);
  const [value, setValue] = React.useState(
    defaultValue != "" ? defaultValue : null
  );
  const direction = document
    .getElementsByTagName("html")[0]
    .getAttribute("dir");

  // const handleClickOutside = () => {
  //   setTimeout(() => {
  //     setShow(false);
  //   }, 100);
  // };
  // React.useEffect(() => {
  //   document.addEventListener("click", handleClickOutside, true);
  //   return () => {
  //     document.removeEventListener("click", handleClickOutside, true);
  //   };
  // });
  return (
    <div
      // onClick={() => setShow(!show)}
      className={`relative w-${width}`}
    >
      <p onClick={() => setShow(!show)} className="dark:bg-gray-900 bg-gray-100 rounded-xl px-4 py-3  text-sm cursor-pointer">
        {value}
      </p>
      <div
        className={`absolute ${
          direction === "ltr" ? "right-0" : "left-0"
        } top-0 bottom-0 text-gray-500 flex justify-center items-center mx-3 rounded-xl ${
          error != "" && "text-red-800"
        }`}
      >
        {icon}
      </div>
      {show === true && (
        <div
          className="absolute left-0 right-0 dark:bg-gray-900 dark:text-gray-600 mt-1 rounded-xl bg-white shadow-lg z-10 text-gray-400 text-sm max-h-32 overflow-y-scroll"
          style={{ top: "100%" }}
        >
          {fields.length > 0 &&
            fields.map((field) => (
              <div
                onClick={() => {
                  setValue(field.label);
                  setShow(false)
                  change(field.value);
                }}
                className="p-2 dark:hover:text-gray-100 cursor-pointer hover:text-black"
                key={field.id}
                value={field.value}
              >
                {field.label}
              </div>
            ))}
        </div>
      )}

      {icon && (
        <div
          className={`absolute ${
            direction === "ltr" ? "right-0" : "left-0"
          } top-0 bottom-0 text-gray-500 flex justify-center items-center mx-3 rounded-xl ${
            error != "" && "text-red-800"
          }`}
        >
          {icon}
        </div>
      )}
      {hint && error === "" && (
        <div className="flex">
          <div className="mx-2">
            {hint && (
              <p
                className="text-gray-300 dark:text-gray-500 text-sm mt-1"
                style={{ fontSize: 11 }}
              >
                {hint}
              </p>
            )}
          </div>
        </div>
      )}
      {error != "" && (
        <div className="flex">
          <div className="mx-2">
            {error && (
              <p
                className="text-red-600 dark:text-red-500 text-sm mt-1"
                style={{ fontSize: 11 }}
              >
                {error}
              </p>
            )}
          </div>
        </div>
      )}
    </div>
  );
};
