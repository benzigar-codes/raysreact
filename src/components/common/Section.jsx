import React from "react";

export const Section = ({
  className,
  children,
  border = false,
  width = "full",
  padding = true,
}) => (
  <div className={`w-${width} ${border === true && "border-2"} ${padding === true && "px-2"} ${className}`}>
    {children}
  </div>
);
