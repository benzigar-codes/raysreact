import React, { useEffect, useState } from "react";
import { FiChevronDown, FiMinus } from "react-icons/fi";
import CountryCodes from "../../utils/countryCodes.json";
import currency from "../../utils/currency.json";
import OutsideClick from "react-outside-click-handler";

export const DropDownSearch = ({
  width = "full",
  error = "",
  type,
  icon = <FiChevronDown />,
  hint = "",
  fields = [],
  change = () => {},
  placeholder,
  defaultValue = "",
  className,
  margin = "0",
  ...rest
}) => {
  const [show, setShow] = React.useState(defaultValue !== "" ? false : true);
  const [search, setSearch] = React.useState("");
  const [final, setFinal] = React.useState(defaultValue ? defaultValue : "");
  const textRef = React.useRef(null);
  const [focused, setFocused] = React.useState(false);

  React.useEffect(() => {
    // show === true && textRef.current.focus();
  }, [show]);

  return (
    <div
      className={`w-${width} ${
        document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? `mr-${margin}` : `ml-${margin}`
      }`}
    >
      {show === false && (
        <div
          onClick={() => {
            setShow(true);
            setTimeout(() => textRef.current.focus(), 100);
          }}
          className={`cursor-pointer flex items-center dark:bg-gray-900 dark:text-gray-300 bg-gray-100 p-2 py-3 px-3 rounded-xl`}
        >
          {final !== "" ? (
            <p className="mx-2" style={{ fontSize: 13 }}>
              {final}
            </p>
          ) : (
            <input
              placeholder={placeholder}
              className="w-full dark:bg-gray-900 text-gray-400 text-sm focus:outline-none bg-gray-100 border-gray-100 dark:focus:border-blue-800 dark:border-gray-800 focus:border-blue-800"
            />
          )}
          {/* <button className="bg-blue-800 p-1 rounded-full text-white text-sm" onClick={clearFinals}><FiMinus/></button> */}
        </div>
      )}

      {show === true && (
        <div className="relative">
          <OutsideClick
            onOutsideClick={() => {
              setTimeout(() => {
                setFocused(false);
                setShow(false);
              }, 100);
            }}
          >
            <input
              ref={textRef}
              onFocus={() => setFocused(true)}
              onChange={(e) => setSearch(e.target.value)}
              placeholder={placeholder}
              value={search}
              className="w-full dark:bg-gray-900 text-gray-400 text-sm focus:outline-none bg-gray-100 rounded-xl px-4 py-3 border-b-2 border-gray-100 dark:focus:border-blue-800 dark:border-gray-800 focus:border-blue-800"
            />
          </OutsideClick>
          {focused === true && (
            <ul className="absolute mt-2 w-full flex flex-col z-20 rounded-lg max-h-44 shadow-xl overflow-y-scroll">
              {fields
                .filter((field) => search.length === 0 || field.label.toLowerCase().includes(search.toLowerCase()))
                .map((field) => (
                  <div
                    id={field.key}
                    onClick={() => {
                      setFinal(field.label);
                      setShow(false);
                      setSearch(field.label);
                      change(field.value);
                    }}
                    className="border-b-1 hover:bg-gray-100 dark:bg-gray-900 border-gray-200 cursor-pointer flex items-center bg-gray-50 px-2 py-2 text-sm"
                  >
                    <div className="w-full mx-2 text-gray-700 dark:text-gray-300">{field.label}</div>
                  </div>
                ))}
            </ul>
          )}
        </div>
      )}
    </div>
  );
};
