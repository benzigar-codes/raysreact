import { format } from "date-fns";
import React from "react";
import "../../styles/DayPicker.css";
// import 'react-day-picker/lib/style.css';

import dateFnsFormat from "date-fns/format";
import dateFnsParse from "date-fns/parse";

import DayPicker from "react-day-picker/DayPicker";
import DayPickerInput from "react-day-picker/DayPickerInput";
import { DateUtils } from "react-day-picker";
import { TextField } from "./TextField";

export const DatePicker = ({
  width = "full",
  error = "",
  type = "time",
  required = true,
  icon = "",
  hint = "",
  iconSelect = () => {},
  change = () => {},
  placeholder = "",
  className = "",
  padding = true,
  margin = 0,
  defaultValue,
  ...rest
}) => {
  const direction = document.getElementsByTagName("html")[0].getAttribute("dir");
  const clock = React.useRef(null);
  const [open, setOpen] = React.useState(false);
  const [time, setTime] = React.useState(defaultValue ? defaultValue : format(Date.now(), "hh : mm a"));

  const FORMAT = "dd/MM/yyyy";

  function parseDate(str, format, locale) {
    const parsed = dateFnsParse(str, format, new Date(), { locale });
    if (DateUtils.isDate(parsed)) {
      return parsed;
    }
    return undefined;
  }

  function formatDate(date, format, locale) {
    return dateFnsFormat(date, format, { locale });
  }

  return (
    <div
      className={`relative w-${width} ${
        document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? `mr-${margin}` : `ml-${margin}`
      }`}
    >
      <DayPickerInput
        classNames=""
        showOverlay={false}
        component={(props) => <TextField {...props} />}
        formatDate={formatDate}
        format={FORMAT}
        parseDate={parseDate}
        placeholder={`${dateFnsFormat(new Date(), FORMAT)}`}
      />
      {/* <DayPicker/> */}
      {/* <PickerPanel/> */}
      {/* <p
        onClick={() => setOpen(!open)}
        className={
          className +
          ` absolute top-0 flex left-0 w-full dark:bg-gray-900 text-sm focus:outline-none bg-gray-100 rounded-xl px-4 py-3 ${
            error != "" && "border-2 border-red-600"
          }`
        }
      >
        {time}
      </p> */}
      {hint && error === "" && (
        <div className="flex">
          <div className="mx-2">
            {hint && (
              <p className="text-gray-300 dark:text-gray-500 text-sm mt-1" style={{ fontSize: 11 }}>
                {hint}
              </p>
            )}
          </div>
        </div>
      )}
      {error != "" && (
        <div className="flex">
          <div className="mx-2">
            {error && (
              <p className="text-red-600 dark:text-red-500 text-sm mt-1" style={{ fontSize: 11 }}>
                {error}
              </p>
            )}
          </div>
        </div>
      )}
    </div>
  );
};
