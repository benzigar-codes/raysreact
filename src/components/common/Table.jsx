import React from "react";
import gsap from "gsap";
import { FiArchive, FiChevronRight, FiHome, FiUsers, FiLoader } from "react-icons/fi";
import * as XL from "xlsx";
import { saveAs } from "file-saver";

import NavLinks from "../../utils/navLinks.json";
import U from "../../utils/utils.js";
import A from "../../utils/API.js";

import { useHistory, useLocation } from "react-router-dom";
import usePrompt from "../../hooks/usePrompt";
import useLanguage from "../../hooks/useLanguage";
import useSettings from "../../hooks/useSettings";
import useDataTable from "../../hooks/useDataTable";
import { TableWrapper } from "./TableWrapper";
import { PopUp } from "./PopUp";
import { TableWrapperLeft } from "./TableWrapperLeft";
import { ColumnHeadings } from "./ColumnHeadings";
import { Prompt } from "./Prompt";
import { Filter } from "./Filter";
import { TableWrapperRight } from "./TableWrapperRight";
import { DataTable } from "./DataTable";
import { NoData } from "./NoData";
import axios from "axios";
import useAdmin from "../../hooks/useAdmin";
import useUtils from "../../hooks/useUtils";
import { Modal } from "./Modal";
import { FormWrapper } from "./FormWrapper";
import { CgNotes } from "react-icons/cg";
import { Section } from "./Section";
import { FieldWrapper } from "./FieldWrapper";
import { TextArea } from "./TextArea";
import { Button } from "./Button";
import { IoMdCash } from "react-icons/io";
import { addDays } from "date-fns";
import { DropdownNormal } from "./DropDownNormal";
import CalenderDatePicker from "./CalenderDatePicker";

const EachCard = ({ bg, title, number, Icon }) => {
  const [hovered, setHovered] = React.useState(false);
  React.useEffect(
    () => gsap.fromTo(".card", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 }),
    []
  );
  return (
    <div
      className="w-full mb-4 cursor-pointer opacity-0 card"
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
    >
      <div
        className={
          "rounded-xl bg-blue-800 hover:bg-green-800 border-b-2 w-full flex justify-between relative text-white"
        }
        style={
          {
            // backgroundColor: bg,
          }
        }
      >
        <div className="p-4 flex flex-col justify-start">
          <h1 className={"text-lg transition font-bold"}>{number}</h1>
          <h1 className="text-sm w-full text-gray-200" style={{ fontSize: 13 }}>
            {title}
          </h1>
        </div>
        {/* <div
          className="absolute h-10 w-10 bg-gray-200 rounded-full transition"
          style={{
            bottom: -10,
            right: -10,
            transform: hovered ? "translateX(-50px)" : "translate(0)",
          }}
        ></div> */}
      </div>
    </div>
  );
};

export default function Index({
  title = "",
  startingHeadings = [],
  list = null,
  deleteLink = null,
  unregStatus = null,
  defaultLink = null,
  notesLink = null,
  statusList = null,
  driverID = null,
  showBulk = false,
  bread = [],
  archieve = false,
  showUserTypeFilter = false,
  showCorporateBillingDaysFilter = false,
  priorityTypeFilter = false,
  showRefund = false,
  showDateFilter = false,
  showStatusCheck = false,
  showRefersFilter = false,
  showUserType = false,
  showRides = false,
  showAmountInWalletFilter = false,
  enableFreshClick = true,
  showWalletActiveInactive = false,
  showAction = true,
  showBulkAttend = false,
  showAdd = false,
  dueFreezeDashboard = false,
  showEdit = false,
  showUserProfessionalFilterDueWallet = false,
  showTranslate = false,
  showRefundStatusFilter = false,
  showDelete = false,
  showRideFilter = false,
  walletDashboard = false,
  showInfo = false,
  showArchieve = false,
  showDownload = true,
  showRequestPageFilter = false,
  showUnregisteredDelete = false,
  showRetryVerify = false,
  showDueFreezedFilter = false,
  showReimburseFilter = false,
  showView = false,
  showNotes = false,
  showSearch = true,
  requestStatusFilter = false,
  showStatus = true,
  professionalFilterClick = false,
  showMobile = false,
  showMoney = false,
  showVehicle = false,
  showUserProfessionalFilter = false,
  showGuestRideFilter = false,
  showColumnHeadings = true,
  showVerify = false,
  showAssignDriver = false,
  showNotify = false,
  showFilter = true,
  defaultShowField = "ALL",
  showMembers = false,
  showNotificationTypeFilter = false,
  showPrivilege = false,
  showProfessionalFilter = false,
  showCityFilter = false,
  showRequestsFilter = false,
  showActiveInactiveFilter = true,
  showStops = false,
  showBookingByFiler = false,
  corporateID = null,
  corporateConfirmPayLink = null,
  showCorporatePaidFilter = false,
  privilegeClick = () => {},
  rideFilterClick = () => {},
  assignDriverClick = () => {},
  addClick = () => {},
  editClick = () => {},
  requestFilterClick = () => {},
  assignData = () => {},
  viewClick = () => {},
  translateClick = () => {},
  verifyClick = () => {},
  membersClick = () => {},
  rideClick = () => {},
  moneyClick = () => {},
  vehicleClick = () => {},
  add = true,
  edit = true,
}) {
  const { prompt, showPrompt } = usePrompt();
  const { language } = useLanguage();
  const { settings } = useSettings();
  const history = useHistory();
  const { header, authFailure } = useAdmin();
  const { parseError } = useUtils();
  const [note, setNote] = React.useState(null);
  const [notesText, setNoteText] = React.useState("");
  const [infoText, setInfoText] = React.useState("");
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [corporatePayModel, setCorporatePayModel] = React.useState(null);
  const { pathname } = useLocation();
  const query = new URLSearchParams(useLocation().search);
  const showNotificationFilter = query.get("showNotificationFilter") ? true : false;

  const RidesRouteList = {};
  RidesRouteList[language.NEW_RIDES] = NavLinks.ADMIN_RIDES_NEW;
  RidesRouteList[language.SCHEDULED_RIDES] = NavLinks.ADMIN_RIDES_SCHEDULED;
  RidesRouteList[language.ONGOING_RIDES] = NavLinks.ADMIN_RIDES_ONGOING;
  RidesRouteList[language.ISSUE_RIDES] = NavLinks.ADMIN_RIDES_ISSUE;
  RidesRouteList[language.EXPIRED_RIDES] = NavLinks.ADMIN_RIDES_EXPIRED;
  RidesRouteList[language.COMPLETED_RIDES] = NavLinks.ADMIN_RIDES_COMPLETED;
  RidesRouteList[language.USERCANCELLED] = NavLinks.ADMIN_RIDES_USER_CANCEL;
  RidesRouteList[language.DRIVERCANCELLED] = NavLinks.ADMIN_RIDESD_DRIVER_CANCEL;
  RidesRouteList[language.SEARCH_RIDE] = NavLinks.ADMIN_RIDES_SEARCH;
  // RidesRouteList[language.GUESTS_RIDE] = NavLinks.ADMIN_RIDES_GUESTS;
  // RidesRouteList[language.CORPORATE_RIDE] = NavLinks.ADMIN_RIDES_CORPORATE;
  const AmountInWalletRoute = {};
  AmountInWalletRoute[language.USER] = NavLinks.ADMIN_ASSET_WALLET_AMOUNT_USER;
  AmountInWalletRoute[language.PROFESSIONAL] =
    NavLinks.ADMIN_ASSET_WALLET_AMOUNT_PROFESSIONAL;

  const GuestFilterList = {};
  GuestFilterList[language.ALL] = "ALL";
  GuestFilterList[language.AWAITING] = "AWAITING";
  GuestFilterList[language.ARRIVED] = "ARRIVED";
  GuestFilterList[language.ACCEPTED] = "ACCEPTED";
  GuestFilterList[language.USERCANCELLED] = "USERCANCELLED";
  GuestFilterList[language.USERDENY] = "USERDENY";
  GuestFilterList[language.PROFESSIONALCANCELLED] = "PROFESSIONALCANCELLED";
  GuestFilterList[language.EXPIRED] = "EXPIRED";
  GuestFilterList[language.CANCELLED] = "CANCELLED";
  GuestFilterList[language.STARTED] = "STARTED";
  GuestFilterList[language.ENDED] = "ENDED";

  const corporateBillingDaysFilterList = {};
  corporateBillingDaysFilterList[language.ALL] = "ALL";
  corporateBillingDaysFilterList[language.NOT_BILLED_YET] = "NOT_BILLED_YET";

  const corporatePaidStatusFilter = {};
  corporatePaidStatusFilter[language.ALL] = "ALL";
  corporatePaidStatusFilter[language.PAID] = "PAID";
  corporatePaidStatusFilter[language.UNPAID] = "UNPAID";

  const bookingByFilter = {};
  bookingByFilter[language.ALL] = "";
  bookingByFilter[language.USER] = "USER";
  bookingByFilter[language.PROFESSIONAL] = "PROFESSIONAL";
  bookingByFilter[language.CORPORATE] = "COORPERATEOFFICE";

  const notificationTypeFilter = {};
  bookingByFilter[language.EMAIL] = "EMAIL";
  bookingByFilter[language.SMS] = "SMS";
  bookingByFilter[language.PUSH] = "PUSH";

  const ProfessionalRouteList = {};
  ProfessionalRouteList[language.DRIVERS] = NavLinks.ADMIN_DRIVERS_LIST;
  ProfessionalRouteList[language.UPLOADED_DRIVERS] = NavLinks.ADMIN_DRIVERS_UPLOADED_LIST;
  ProfessionalRouteList[language.UNVERIFIED_DRIVERS] =
    NavLinks.ADMIN_DRIVERS_UNVERIFIED_LIST;
  ProfessionalRouteList[language.UNREGISTERED_DRIVERS] =
    NavLinks.ADMIN_DRIVERS_UNREGISTERTED_LIST;

  const NotificationRouteList = {};
  NotificationRouteList[language.REQUESTS] = NavLinks.ADMIN_SECURITY_REQUESTS;
  NotificationRouteList[language.FEEDBACK] = NavLinks.ADMIN_REPORTS_FEEDBACK;

  const RequestRouteList = {};
  RequestRouteList[language.NEW_REQUESTS] = NavLinks.ADMIN_SECURITY_REQUESTS;
  RequestRouteList[language.CLOSED_REQUESTS] = NavLinks.ADMIN_SECURITY_CLOSED_REQUESTS;
  RequestRouteList[language.MY_REQUESTS] = NavLinks.ADMIN_SECURITY_MY_REQUESTS;

  const ReferRouteList = {};
  ReferRouteList[language.USERS] = NavLinks.ADMIN_SETUP_REFER_USER_LIST;
  ReferRouteList[language.PROFESSIONALS] = NavLinks.ADMIN_SETUP_REFER_PROFESSIONAL_LIST;

  const [cities, setCities] = React.useState(null);

  const {
    // States
    headings,
    fields,
    total,
    pagination,
    showField,
    popup,
    search,
    loading,
    selected,
    // Set States
    setLoading,
    setSearch,
    setHeadings,
    setFields,
    setTotal,
    setPopup,
    setPagination,
    setSelected,
    setShowField,
    removeFromDom,
  } = useDataTable(
    startingHeadings,
    [],
    defaultShowField === "ALL" ? language.ALL : defaultShowField
  );

  const [dashboardData, setDashboardData] = React.useState({});
  const [dates, setDates] = React.useState(["", ""]);
  const [downloadDates, setDownloadDates] = React.useState([new Date(), new Date()]);
  const [userProfessional, setUserProfessional] = React.useState(language.USER);
  const [successFailure, setSuccessFailure] = React.useState(language.SUCCESS);
  const [dueFreezed, setDueFreezed] = React.useState(language.ALL);
  const [guestFilter, setGuestFilter] = React.useState(language.ALL);
  const [refundStatus, setReFundStatus] = React.useState(language.ALL);
  const [corporatePaymentStatus, setCorporatePaymentStatus] = React.useState(
    language.ALL
  );
  const [reImbursementStatus, setReImbursementStatus] = React.useState(false);
  const [corporateFilter, setCorporateFilter] = React.useState(true);
  const [cityFilter, setCityFilter] = React.useState("");
  const [userTypeFilter, setUserTypeFilter] = React.useState("user");
  const [showDownloadPop, setDownloadPop] = React.useState(false);
  const [showCorporateAmount, setCorporateAmount] = React.useState(null);
  const [userTypeStatus, setUserTypeStatus] = React.useState(language.USER);
  const [bookedByStatus, setBookingByStatus] = React.useState(language.ALL);

  const [notificationTypeStatus, setNotificationTypeStatus] = React.useState(
    language.EMAIL
  );

  React.useEffect(() => {
    setUserProfessional(language.USER);
    setSuccessFailure(language.SUCCESS);
    setDueFreezed(language.ALL);
  }, []);

  const fetchCities = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_CITY_LIST_ALL, {}, header);
      setCities(data.data);
    } catch (er) {
      authFailure(er);
    }
  };

  React.useEffect(() => {
    if (showCityFilter) fetchCities();
  }, []);

  document.title = title;

  const setBulkRestore = async () => {
    try {
      await axios.post(statusList, { ids: selected, status: U.ACTIVE }, header);
      removeFromDom();
      setPopup({ title: language.UPDATE_SUCCESS, type: "success" });
    } catch (err) {
      authFailure(err);
      setPopup({ title: parseError(err), type: "error" });
    }
    setSelected([]);
  };

  const setBulkActive = async () => {
    try {
      await axios.post(
        statusList,
        driverID
          ? { ids: selected, status: U.ACTIVE, professionalId: driverID }
          : { ids: selected, status: U.ACTIVE },
        header
      );
      if (showField === language.INACTIVE) {
        removeFromDom();
      } else {
        setFields(
          fields.map((field) =>
            selected.filter((select) => select === field._id).length > 0
              ? { ...field, status: 1 }
              : field
          )
        );
      }
      setPopup({ title: language.UPDATE_SUCCESS, type: "success" });
    } catch (err) {
      authFailure(err);
      setPopup({ title: parseError(err), type: "error" });
    }
    setSelected([]);
  };

  const setBulkInActive = async () => {
    try {
      await axios.post(
        statusList,
        driverID
          ? { ids: selected, status: U.INACTIVE, professionalId: driverID }
          : { ids: selected, status: U.INACTIVE },
        header
      );
      if (showField === language.ACTIVE) {
        removeFromDom();
      } else {
        setFields(
          fields.map((field) =>
            selected.filter((select) => select === field._id).length > 0
              ? { ...field, status: 0 }
              : field
          )
        );
      }
      setPopup({ title: language.UPDATE_SUCCESS, type: "success" });
    } catch (err) {
      authFailure(err);
      setPopup({ title: parseError(err), type: "error" });
    }
    setSelected([]);
  };

  const setBulkArchieve = async () => {
    try {
      await axios.post(statusList, { ids: selected, status: U.ARCHIEVE }, header);
      removeFromDom();
      setSelected([]);
      setPopup({ title: language.UPDATE_SUCCESS, type: "success" });
    } catch (err) {
      authFailure(err);
      setPopup({ title: parseError(err), type: "error" });
    }
    setSelected([]);
  };

  const archieveField = async (id) => {
    try {
      await axios.post(statusList, { ids: [id], status: U.ARCHIEVE }, header);
      setFields(fields.filter((field) => field._id !== id));
      setTotal(total - 1);
      setPagination({
        ...pagination,
        skip: parseInt(pagination.skip) - 1,
      });
      setPopup({ title: language.UPDATE_SUCCESS, type: "success" });
    } catch (err) {
      authFailure(err);
      setPopup({ title: parseError(err), type: "error" });
    }
    setSelected([]);
  };

  const restoreField = async (id) => {
    try {
      await axios.post(statusList, { ids: [id], status: U.ACTIVE }, header);
      setFields(fields.filter((field) => field._id !== id));
      setTotal(total - 1);
      setPagination({
        ...pagination,
        skip: parseInt(pagination.skip) - 1,
      });
      setPopup({ title: language.UPDATE_SUCCESS, type: "success" });
    } catch (err) {
      authFailure(err);
      setPopup({ title: parseError(err), type: "error" });
    }
    setSelected([]);
  };

  const fetchTable = async () => {
    setLoading(true);
    setPagination({ ...pagination, skip: 0 });
    try {
      const { data } = await axios.post(
        list,
        { skip: 0, ...getQuery(showField) },
        header
      );
      setTotal(data.count);
      setFields(assignData(data.response));
      if (data.dashboardData) setDashboardData(data.dashboardData);
    } catch (err) {
      authFailure(err);
    }
    setLoading(false);
  };

  const getQuery = (showField) => {
    let query = {};
    if (showField === language.ARCHIEVE) {
      query = driverID
        ? {
            limit: pagination.limit,
            search,
            filter: U.ARCHIEVE,
            professionalId: driverID,
          }
        : { limit: pagination.limit, search, filter: U.ARCHIEVE };
    }
    if (showField === language.ACTIVE)
      query = driverID
        ? { limit: pagination.limit, search, filter: U.ACTIVE, professionalId: driverID }
        : { limit: pagination.limit, search, filter: U.ACTIVE };
    else if (showField === language.INACTIVE)
      query = driverID
        ? {
            limit: pagination.limit,
            search,
            filter: U.INACTIVE,
            professionalId: driverID,
          }
        : { limit: pagination.limit, search, filter: U.INACTIVE };
    else if (showField === language.USER + " " + language.ALL_AMOUNT)
      query = {
        limit: pagination.limit,
        search,
        userType: "user",
        reimbursefilter: reImbursementStatus,
        filter: "",
        fromDate: dates[0] || "",
        toDate: dates[1] || "",
      };
    else if (showField === language.PROFESSIONAL + " " + language.ALL_AMOUNT)
      query = {
        limit: pagination.limit,
        search,
        userType: "professional",
        reimbursefilter: reImbursementStatus,
        filter: "",
        fromDate: dates[0] || "",
        toDate: dates[1] || "",
      };
    else if (showField === language.USER + " " + language.SUCCESS)
      query = {
        limit: pagination.limit,
        search,
        userType: "user",
        reimbursefilter: reImbursementStatus,
        filter: "SUCCESS",
        fromDate: dates[0] || "",
        toDate: dates[1] || "",
      };
    else if (showField === language.PROFESSIONAL + " " + language.SUCCESS)
      query = {
        limit: pagination.limit,
        search,
        userType: "professional",
        reimbursefilter: reImbursementStatus,
        filter: "SUCCESS",
        fromDate: dates[0] || "",
        toDate: dates[1] || "",
      };
    else if (showField === language.USER + " " + language.FAILURE)
      query = {
        limit: pagination.limit,
        search,
        reimbursefilter: reImbursementStatus,
        userType: "user",
        filter: "FAILED",
        fromDate: dates[0] || "",
        toDate: dates[1] || "",
      };
    else if (showField === language.PROFESSIONAL + " " + language.FAILURE)
      query = {
        limit: pagination.limit,
        search,
        reimbursefilter: reImbursementStatus,
        userType: "professional",
        filter: "FAILED",
        fromDate: dates[0] || "",
        toDate: dates[1] || "",
      };
    else if (showField === language.USER + " " + language.DUE_AMOUNT)
      query = {
        limit: pagination.limit,
        search,
        userType: "user",
        filter: "DUEAMOUNT",
        fromDate: dates[0] || "",
        toDate: dates[1] || "",
      };
    else if (showField === language.USER + " " + language.FREEZED_AMOUNT)
      query = {
        limit: pagination.limit,
        search,
        userType: "user",
        filter: "FREEZEDAMOUNT",
        fromDate: dates[0] || "",
        toDate: dates[1] || "",
      };
    else if (showField === language.PROFESSIONAL + " " + language.DUE_AMOUNT)
      query = {
        limit: pagination.limit,
        search,
        userType: "professional",
        filter: "DUEAMOUNT",
        fromDate: dates[0] || "",
        toDate: dates[1] || "",
      };
    else if (showField === language.PROFESSIONAL + " " + language.FREEZED_AMOUNT)
      query = {
        limit: pagination.limit,
        search,
        userType: "professional",
        filter: "FREEZEDAMOUNT",
        fromDate: dates[0] || "",
        toDate: dates[1] || "",
      };
    else {
      query["limit"] = pagination.limit;
      query["search"] = search;
      if (!query.filter) query["filter"] = "";
      query["fromDate"] = dates[0] || "";
      query["toDate"] = dates[1] || "";
      if (driverID) query["professionalId"] = driverID;
      if (userTypeFilter) query["userTypefilter"] = "";
      if (priorityTypeFilter) query["priorityFilter"] = "";
      if (requestStatusFilter) query["statusfilter"] = "";
      if (showGuestRideFilter) query["filter"] = guestFilter;
      if (showCorporateBillingDaysFilter) query["recentFilter"] = corporateFilter;
      if (
        showCorporatePaidFilter &&
        corporatePaymentStatus !== "ALL" &&
        corporatePaymentStatus !== undefined
      ) {
        if (corporatePaymentStatus === "UNPAID") query["paidStatusFilter"] = true;
      }
      if (cityFilter) query["city"] = cityFilter;
      if (showBookingByFiler) query["bookingByFilter"] = bookedByStatus;
      if (showReimburseFilter) query["reimbursefilter"] = reImbursementStatus;
      if (showNotificationTypeFilter && notificationTypeStatus === language.EMAIL)
        query["type"] = "EMAIL";
      if (showNotificationTypeFilter && notificationTypeStatus === language.SMS)
        query["type"] = "SMS";
      if (showNotificationTypeFilter && notificationTypeStatus === language.PUSH)
        query["type"] = "PUSH";
      if (showUserTypeFilter && userTypeStatus === language.USER)
        query["userType"] = "user";
      if (showUserTypeFilter && userTypeStatus === language.PROFESSIONAL)
        query["userType"] = "professional";
      if (showRefundStatusFilter) {
        if (showUserProfessionalFilter) {
          if (userProfessional === language.USER) query["userType"] = "user";
          if (userProfessional === language.PROFESSIONAL)
            query["userType"] = "professional";
        }
        if (refundStatus === language.PENDING) query["filter"] = "PENDING";
        if (refundStatus === language.PROCESSING) query["filter"] = "PROCESSING";
        if (refundStatus === language.SUCCESS) query["filter"] = "SUCCESS";
        if (refundStatus === language.FAILURE) query["filter"] = "FAILED";
      }
    }
    return query;
  };

  const getDownloadData = async () => {
    try {
      const { data } = await axios.post(
        list,
        {
          skip: 0,
          ...getQuery(showField),
          limit: 1000000,
          fromDate: downloadDates[0],
          toDate: downloadDates[1],
        },
        header
      );
      const fileType =
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
      const fileExtension = ".xlsx";

      function fitToColumn(arrayOfArray) {
        return arrayOfArray[0].map((a, i) => ({
          wch: Math.max(
            ...arrayOfArray
              .filter((a2) => a2[i] !== undefined && a2[i] !== null)
              .map((a2) => a2[i].toString().length)
          ),
        }));
      }

      const fields = assignData(data.response);

      if (fields.length > 0) {
        console.log(JSON.stringify(fields, null, 2));
        const sheetHeadings = [
          language.NO,
          ...Object.keys(fields[0])
            .filter((each) => each !== "_id" || each !== "notes")
            .map((each) =>
              headings.filter((heading) => heading.key === each).length > 0
                ? headings.filter((heading) => heading.key === each)[0].title
                : ""
            )
            .filter((each) => each !== ""),
        ];
        const sheetData = [
          sheetHeadings,
          ...fields.map((each, idx) => [
            idx + 1,
            ...Object.keys(each)
              .filter((each) => each !== "_id")
              .map((key) =>
                key === "status"
                  ? each[key] === 1
                    ? "ACTIVE"
                    : "INACTIVE"
                  : key === "oneByOne" || key === "oneByOne2" || key === "oneByOne3"
                  ? each[key].join(" ")
                  : each[key]
              ),
          ]),
        ];
        const ws = XL.utils.aoa_to_sheet(sheetData);
        ws["!cols"] = fitToColumn(sheetData);
        ws["!rows"] = [...new Array(sheetData.length)].map((each) => ({
          hpt: 20,
        }));
        const wb = { Sheets: { export: ws }, SheetNames: ["export"] };
        const excelBuffer = XL.write(wb, { bookType: "xlsx", type: "array" });
        const data = new Blob([excelBuffer], { type: fileType });
        saveAs(data, document.title + fileExtension);
      }
    } catch (err) {
      authFailure(err);
    }
    setDownloadPop(false);
  };

  const toggleStatusField = async (id) => {
    setLoading(false);
    const singleData = fields.filter((field) => field._id === id)[0];
    try {
      let query = driverID
        ? {
            ids: [id],
            status: singleData.status === 1 ? U.INACTIVE : U.ACTIVE,
            professionalId: driverID,
          }
        : { ids: [id], status: singleData.status === 1 ? U.INACTIVE : U.ACTIVE };
      if (showNotificationTypeFilter && notificationTypeStatus === language.EMAIL)
        query["type"] = "EMAIL";
      if (showNotificationTypeFilter && notificationTypeStatus === language.SMS)
        query["type"] = "SMS";
      if (showNotificationTypeFilter && notificationTypeStatus === language.PUSH)
        query["type"] = "PUSH";
      await axios.post(statusList, query, header);
      setFields(
        fields.map((user) =>
          user._id === id ? { ...user, status: user.status === 1 ? 2 : 1 } : user
        )
      );
      setPopup({ title: language.UPDATE_SUCCESS, type: "success" });
    } catch (err) {
      setPopup({ title: parseError(err), type: "error" });
    }
  };

  const clearSearch = async () => {
    setSearch("");
    setTimeout(() => fetchTable(), 500);
    // setLoading(true);
    // setPagination({ ...pagination, skip: 0 });
    // setSearch("");
    // try {
    //   const { data } = await axios.post(
    //     list,
    //     { userType: "user", skip: 0, limit: pagination.limit, search: "", filter: "" },
    //     header
    //   );
    //   setTotal(data.count);
    //   setFields(assignData(data.response));
    // } catch (err) {
    //   setPopup({ title: parseError(err), type: "error" });
    //   authFailure(err);
    // }
    // setLoading(false);
  };

  React.useEffect(() => {
    setSelected([]);
    fetchTable();
    if (
      showField === language.ARCHIEVE ||
      showField === language.ACTIVE ||
      showField === language.INACTIVE
    ) {
      setHeadings(headings.filter((field) => field.key !== "status"));
    }
    if (showField === language.ALL) {
      showStatus === true
        ? setHeadings(
            headings.filter((field) => field.key === "status").length > 0
              ? headings
              : [
                  ...headings,
                  {
                    id: 6,
                    title: language.STATUS,
                    key: "status",
                    show: true,
                  },
                ]
          )
        : setHeadings(headings);
    }
  }, [
    pagination.limit,
    showField,
    dates,
    guestFilter,
    corporateFilter,
    corporatePaymentStatus,
    cityFilter,
    bookedByStatus,
    notificationTypeStatus,
    reImbursementStatus,
    userTypeStatus,
  ]);

  const unregisterStatusClick = async (id) => {
    try {
      await axios.post(
        unregStatus,
        {
          ids: [id],
          status:
            fields.filter((field) => field._id === id).length > 0
              ? fields.filter((field) => field._id === id)[0].unregister_status ===
                  U.INCOMPLETE ||
                fields.filter((field) => field._id === id)[0].unregister_status === U.NEW
                ? U.ATTENDED
                : U.INCOMPLETE
              : "",
        },
        header
      );
      fetchTable();
    } catch (err) {
      authFailure(err);
      setPopup({ title: parseError(err), type: "error" });
    }
  };

  const notesSubmit = async () => {
    setBtnLoading(true);
    try {
      await axios.post(
        notesLink,
        {
          id: note,
          notes: notesText,
        },
        header
      );
      setNoteText("");
      setNote(null);
      setPopup({ title: language.UPDATE_SUCCESS, type: "success" });
      fetchTable();
    } catch (err) {
      authFailure(err);
    }
    setBtnLoading(false);
  };
  const deleteClick = async (id) => {
    try {
      await axios.post(
        deleteLink,
        {
          id,
        },
        header
      );
      setFields(fields.filter((field) => field._id !== id));
      setTotal(total - 1);
      setPagination({
        ...pagination,
        skip: parseInt(pagination.skip) - 1,
      });
      setPopup({ title: language.REMOVE_SUCCESS, type: "success" });
    } catch (err) {
      authFailure(err);
      setPopup({ title: parseError(err), type: "error" });
    }
  };

  const refundStatusChange = async (id, action = "") => {
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_ASSET_REFUND + "/" + action + "/" + id,
        {},
        header
      );
      // setFields(fields.filter((field) => field._id !== id));
      // setTotal(total - 1);
      // setPagination({
      //   ...pagination,
      //   skip: parseInt(pagination.skip) - 1,
      // });
      // fetchTable();
      setPopup({ title: data?.message, type: data?.code === 200 ? "success" : "error" });
    } catch (err) {
      authFailure(err);
      setPopup({ title: language.FAILURE, type: "error" });
    }
  };

  const setDefault = async (id) => {
    setLoading(true);
    try {
      await axios.post(defaultLink, { id }, header);
      fetchTable();
    } catch (err) {
      authFailure(err);
    }
  };

  const loadMore = async () => {
    // history.push({
    //   pathname: history?.location?.pathname,
    //   search:
    //     "?" +
    //     new URLSearchParams({
    //       skip: pagination.skip,
    //       limit: pagination.limit,
    //     }).toString(),
    // });
    try {
      const { data } = await axios.post(
        list,
        {
          skip: parseInt(pagination.skip) + parseInt(pagination.limit),
          ...getQuery(showField),
        },
        header
      );
      setFields([...fields, ...assignData(data.response)]);
    } catch (err) {
      authFailure(err);
    }

    setPagination({
      ...pagination,
      skip: parseInt(pagination.skip) + parseInt(pagination.limit),
    });
  };

  const corporateConfirmPay = async (e) => {
    setBtnLoading(true);
    try {
      const { data } = await axios.post(
        corporateConfirmPayLink,
        {
          billingId: e,
          coorperateId: corporateID,
          comments: notesText,
          status: true,
        },
        header
      );
      clearSearch();
      setBtnLoading(false);
      setCorporatePayModel(null);
    } catch (err) {}
  };

  const corporateBillingAmountClick = async (e) => {
    setCorporateAmount("loading");
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_CORPORATE_BILLING_AMOUNT,
        {
          ids: e,
        },
        header
      );
      if (data && data.amount) setCorporateAmount(data.amount);
      else setCorporateAmount(null);
    } catch (err) {
      setCorporateAmount(null);
    }
  };

  return (
    <TableWrapper>
      {note !== null && (
        <Modal>
          <FormWrapper
            width="2/4"
            enterSubmit={false}
            btnLoading={btnLoading}
            model={true}
            icon={<CgNotes />}
            title={language.NOTES}
            closeBtn={true}
            submitBtn={true}
            submit={notesSubmit}
            closeClick={() => setNote(null)}
          >
            <Section>
              <FieldWrapper padding={false} title={language.NOTES}>
                <TextArea change={(e) => setNoteText(e)} defaultValue={notesText} />
              </FieldWrapper>
            </Section>
          </FormWrapper>
        </Modal>
      )}

      {infoText && (
        <Modal>
          <FormWrapper
            width="2/4"
            model={true}
            icon={<CgNotes />}
            title={language.INFO}
            closeBtn={true}
            closeClick={() => setInfoText("")}
          >
            <Section>
              <FieldWrapper padding={false} title={language.INFO}>
                {/* <TextArea defaultValue={infoText} /> */}
                {infoText}
              </FieldWrapper>
            </Section>
          </FormWrapper>
        </Modal>
      )}

      {corporatePayModel !== null && (
        <Modal>
          <FormWrapper
            width="2/4"
            enterSubmit={false}
            btnLoading={btnLoading}
            model={true}
            icon={<CgNotes />}
            title={language.CONFIRM_PAYMENT}
            closeBtn={true}
            submitBtn={true}
            submit={() => corporateConfirmPay(corporatePayModel)}
            closeClick={() => setCorporatePayModel(null)}
          >
            <Section>
              <FieldWrapper padding={false} title={language.NOTES}>
                <TextArea change={(e) => setNoteText(e)} defaultValue={notesText} />
              </FieldWrapper>
            </Section>
          </FormWrapper>
        </Modal>
      )}
      {showDownloadPop && (
        <Modal>
          <FormWrapper
            width="2/4"
            enterSubmit={false}
            btnLoading={btnLoading}
            model={true}
            icon={<CgNotes />}
            title={document.title + " " + language.EXCEL}
            closeBtn={true}
            downloadBtn={true}
            submit={getDownloadData}
            closeClick={() => setDownloadPop(false)}
          >
            <Section>
              <FieldWrapper title={language.FROM}>
                <CalenderDatePicker
                  startTime={
                    new Date(new Date().setMinutes(new Date().getMinutes() + 70))
                  }
                  // afterDays={5}
                  showTime={false}
                  disableBeforeDays={false}
                  change={(e) => setDownloadDates([e, downloadDates[1]])}
                  defaultValue={downloadDates[0]}
                />
              </FieldWrapper>
            </Section>
            <Section>
              <FieldWrapper title={language.TO}>
                <CalenderDatePicker
                  startTime={
                    new Date(new Date().setMinutes(new Date().getMinutes() + 70))
                  }
                  // afterDays={5}
                  showTime={false}
                  disableBeforeDays={false}
                  change={(e) => setDownloadDates([downloadDates[0], e])}
                  defaultValue={downloadDates[1]}
                />
              </FieldWrapper>
            </Section>
          </FormWrapper>
        </Modal>
      )}
      {showCorporateAmount !== null && (
        <Modal>
          <div className="flex items-center justify-center">
            <div
              className="p-5 bg-gray-100 dark:bg-gray-800 text-3xl"
              style={{ width: 300 }}
            >
              <div className="flex justify-between items-center mb-4">
                <h1 className="text-xl">{language.AMOUNT + " : "}</h1>
                <Button click={() => setCorporateAmount(null)} title={language.CLOSE} />
              </div>
              {showCorporateAmount === "loading" && (
                <div className="flex justify-center">
                  <FiLoader className="animate-spin" />
                </div>
              )}
              {showCorporateAmount !== "loading" && showCorporateAmount !== null && (
                <p className="p-5 bg-gray-300 dark:bg-gray-900 flex justify-center">
                  {showCorporateAmount}
                </p>
              )}
            </div>
          </div>
        </Modal>
      )}

      {popup != null && (
        <PopUp unmount={() => setPopup(null)} title={popup.title} type={popup.type} />
      )}

      {prompt.show === true && (
        <Prompt
          title={prompt.title}
          message={prompt.message}
          type={prompt.type}
          close={prompt.close}
          click={prompt.click}
        />
      )}
      <TableWrapperLeft>
        {showColumnHeadings && (
          <ColumnHeadings
            headings={headings}
            change={(e) => setHeadings(e)}
          ></ColumnHeadings>
        )}
        {walletDashboard === true && (
          <>
            <h1 className="font-bold text-md text-gray-500 dark:text-gray-400 mb-2">
              {language.INFO}
            </h1>
            <EachCard
              bg="#335EFD"
              title={language.TOTAL_TRANSACTION_AMOUNT}
              number={parseFloat(dashboardData.totalTransactionAmount).toFixed(2)}
              Icon={IoMdCash}
            />
            <EachCard
              bg="#F69534"
              title={
                U.mode === "zayRide" ? language.ZAYRIDE_WALLET : language.ZERVX_WALLET
              }
              number={parseFloat(dashboardData.totalWalletAmount).toFixed(2)}
              Icon={IoMdCash}
            />
            {dashboardData.totalReimbursementAmount !== undefined && (
              <EachCard
                bg="#F69534"
                title={language.REIMBURSEMENT}
                number={parseFloat(dashboardData.totalReimbursementAmount).toFixed(2)}
                Icon={IoMdCash}
              />
            )}
          </>
        )}
        {dueFreezeDashboard === true && (
          <>
            <h1 className="font-bold text-md text-gray-500 dark:text-gray-400 mb-2">
              {language.INFO}
            </h1>
            <EachCard
              bg="#335EFD"
              title={language.TOTAL_DUE_AMOUNT}
              number={parseFloat(dashboardData.totalDueAmount).toFixed(2)}
              Icon={IoMdCash}
            />
            <EachCard
              bg="#F69534"
              title={language.TOTAL_FREEZE_AMOUNT}
              number={parseFloat(dashboardData.totalFreezedAmount).toFixed(2)}
              Icon={IoMdCash}
            />
          </>
        )}
        {showNotificationFilter !== false && (
          <Filter
            defaultValue={
              Object.keys(NotificationRouteList).filter(
                (each) => NotificationRouteList[each] === pathname
              )[0]
            }
            title={language.NOTIFICATIONS}
            change={(e) =>
              JSON.stringify(e) !== '["",""]' &&
              history?.push(NotificationRouteList[e] + "?showNotificationFilter=true")
            }
            fields={[language.REQUESTS, language.FEEDBACK]}
          />
        )}
        {showAmountInWalletFilter !== false && (
          <Filter
            defaultValue={
              Object.keys(AmountInWalletRoute).filter(
                (each) => AmountInWalletRoute[each] === pathname
              )[0]
            }
            title={language.TYPE}
            change={(e) =>
              JSON.stringify(e) !== '["",""]' && history?.push(AmountInWalletRoute[e])
            }
            fields={[language.USER, language.PROFESSIONAL]}
          />
        )}
        {showFilter === true && (
          <>
            {showUserTypeFilter === true && (
              <Filter
                // defaultValue={language.ALL}
                title={language.TYPE}
                change={(e) => JSON.stringify(e) !== '["",""]' && setUserTypeStatus(e)}
                fields={[language.USER, language.PROFESSIONAL]}
              />
            )}
            {showCorporatePaidFilter === true && (
              <Filter
                // defaultValue={language.ALL}
                title={language.PAYMENT_STATUS}
                change={(e) => setCorporatePaymentStatus(corporatePaidStatusFilter[e])}
                fields={[language.ALL, language.UNPAID]}
              />
            )}
            {showReimburseFilter === true && (
              <Filter
                defaultValue={reImbursementStatus}
                title={language.REIMBURSEMENT}
                change={(e) =>
                  JSON.stringify(e) !== '["",""]' && setReImbursementStatus(e)
                }
                // fields={[language.ALL, language.UNPAID]}
                type={"toggle"}
              />
            )}
            {showBookingByFiler === true && (
              <Filter
                // defaultValue={language.ALL}
                title={language.BOOKED_BY}
                change={(e) => setBookingByStatus(bookingByFilter[e])}
                fields={[
                  language.ALL,
                  language.USER,
                  language.PROFESSIONAL,
                  language.CORPORATE,
                ]}
              />
            )}
            {showNotificationTypeFilter === true && (
              <Filter
                // defaultValue={language.ALL}
                title={language.TYPE}
                change={(e) =>
                  JSON.stringify(e) !== '["",""]' && setNotificationTypeStatus(e)
                }
                fields={[language.EMAIL, language.SMS, language.PUSH]}
              />
            )}
            {showArchieve === true && (
              <Filter
                title={language.LIST}
                change={(e) => setShowField(e)}
                fields={[language.ALL, language.ARCHIEVE]}
              />
            )}
            {showActiveInactiveFilter === true &&
              showField !== language.ARCHIEVE &&
              showStatus !== false && (
                <Filter
                  title={language.FILTER}
                  change={(e) => setShowField(e)}
                  fields={[language.ALL, language.ACTIVE, language.INACTIVE]}
                />
              )}
            {showUserProfessionalFilter === true &&
              showField !== language.ARCHIEVE &&
              showStatus !== false && (
                <Filter
                  title={language.USER_PROFESSIONAL_FILTER}
                  change={(e) => {
                    setUserProfessional(e);
                    showWalletActiveInactive && setShowField(e + " " + successFailure);
                    showDueFreezedFilter && setShowField(e + " " + dueFreezed);
                    showRefundStatusFilter && setShowField(e + " " + refundStatus);
                  }}
                  fields={[language.USER, language.PROFESSIONAL]}
                />
              )}
            {showWalletActiveInactive === true &&
              showField !== language.ARCHIEVE &&
              showStatus !== false && (
                <Filter
                  title={language.STATUS}
                  change={(e) => {
                    setSuccessFailure(e);
                    setShowField(userProfessional + " " + e);
                  }}
                  fields={[language.SUCCESS, language.FAILURE]}
                />
              )}
            {showRefundStatusFilter === true &&
              showField !== language.ARCHIEVE &&
              showStatus !== false && (
                <Filter
                  title={language.STATUS}
                  change={(e) => {
                    JSON.stringify(e) !== '["",""]' && setReFundStatus(e);
                    JSON.stringify(e) !== '["",""]' &&
                      setShowField(userProfessional + " " + e);
                  }}
                  fields={[
                    language.ALL,
                    language.SUCCESS,
                    language.PENDING,
                    language.PROCESSING,
                    language.FAILURE,
                  ]}
                />
              )}
            {showDueFreezedFilter === true &&
              showField !== language.ARCHIEVE &&
              showStatus !== false && (
                <Filter
                  title={language.DUE_FREEZED}
                  change={(e) => {
                    setDueFreezed(e);
                    setShowField(userProfessional + " " + e);
                  }}
                  fields={[
                    language.ALL_AMOUNT,
                    language.DUE_AMOUNT,
                    language.FREEZED_AMOUNT,
                  ]}
                />
              )}
            {showDateFilter === true &&
              showField !== language.ARCHIEVE &&
              showStatus !== false && (
                <>
                  <Filter
                    title={language.CUSTOM_DATE}
                    change={(e) =>
                      e[1] !== null &&
                      setDates([addDays(new Date(e[0]), 1), addDays(new Date(e[1]), 1)])
                    }
                    type={"date"}
                  />
                </>
              )}
            {showRideFilter !== false && (
              <Filter
                defaultValue={
                  Object.keys(RidesRouteList).filter(
                    (each) => RidesRouteList[each] === pathname
                  )[0]
                }
                title={language.RIDES_LIST}
                change={(e) => rideFilterClick(RidesRouteList[e])}
                fields={[
                  language.NEW_RIDES,
                  language.SCHEDULED_RIDES,
                  language.ONGOING_RIDES,
                  language.COMPLETED_RIDES,
                  language.EXPIRED_RIDES,
                  language.USERCANCELLED,
                  language.DRIVERCANCELLED,
                  language.ISSUE_RIDES,
                  // language.GUESTS_RIDE,
                  language.SEARCH_RIDE,
                  // language.CORPORATE_RIDE,
                ]}
              />
            )}
            {showProfessionalFilter !== false && (
              <Filter
                defaultValue={
                  Object.keys(ProfessionalRouteList).filter(
                    (each) => ProfessionalRouteList[each] === pathname
                  )[0]
                }
                title={language.DRIVERS_LIST}
                change={(e) => professionalFilterClick(ProfessionalRouteList[e])}
                fields={[
                  language.DRIVERS,
                  language.UPLOADED_DRIVERS,
                  language.UNVERIFIED_DRIVERS,
                  language.UNREGISTERED_DRIVERS,
                ]}
              />
            )}
            {showRequestsFilter !== false && (
              <Filter
                defaultValue={
                  Object.keys(RequestRouteList).filter(
                    (each) => RequestRouteList[each] === pathname
                  )[0]
                }
                title={language.REQUESTS_LISTS}
                change={(e) => history.push(RequestRouteList[e])}
                fields={[
                  language.NEW_REQUESTS,
                  language.CLOSED_REQUESTS,
                  language.MY_REQUESTS,
                ]}
              />
            )}
            {showRefersFilter !== false && (
              <Filter
                defaultValue={
                  Object.keys(ReferRouteList).filter(
                    (each) => ReferRouteList[each] === pathname
                  )[0]
                }
                title={language.REFER_LIST}
                change={(e) => history.push(ReferRouteList[e])}
                fields={[language.USERS, language.PROFESSIONALS]}
              />
            )}
            {showGuestRideFilter && (
              <Filter
                // defaultValue={language.ALL}
                title={language.BOOKING_STATUS}
                change={(e) => setGuestFilter(GuestFilterList[e])}
                fields={[
                  language.ALL,
                  language.AWAITING,
                  language.ARRIVED,
                  language.ACCEPTED,
                  language.USERCANCELLED,
                  language.USERDENY,
                  language.PROFESSIONALCANCELLED,
                  language.EXPIRED,
                  language.CANCELLED,
                  language.STARTED,
                  language.ENDED,
                ]}
              />
            )}
            {showCorporateBillingDaysFilter && (
              <Filter
                defaultValue={language.NOT_YET_BILLED}
                title={language.RIDES}
                change={(e) => setCorporateFilter(e === language.ALL ? false : true)}
                fields={[language.ALL, language.NOT_YET_BILLED]}
              />
            )}
            {cities && (
              <Filter
                // defaultValue={}
                title={language.CITY}
                change={(e) =>
                  setCityFilter(
                    cities.filter((each) => each.locationName === e).length > 0
                      ? cities.filter((each) => each.locationName === e)[0]._id
                      : ""
                  )
                }
                fields={[language.ALL, ...cities.map((each) => each.locationName)]}
              />
            )}
          </>
        )}
        <Filter
          title={language.PAGE_LIMIT}
          change={(e) => setPagination({ ...pagination, limit: e })}
          fields={settings.arrayData.pageViewLimits.map((each) => parseInt(each)).sort()}
        />
      </TableWrapperLeft>
      <TableWrapperRight
        refreshClick={() => {
          fetchTable();
        }}
        bread={bread}
        showBulkArchieve={showArchieve}
        showBulkActive={showField !== showArchieve}
        addClick={addClick}
        refreshLoading={loading}
        showRestore={showField === language.ARCHIEVE}
        searchChange={(e) => setSearch(e)}
        searchSubmit={() => {
          fetchTable();
        }}
        showSearch={showSearch}
        downloadClick={(type) => setDownloadPop(true)}
        searchText={search}
        selected={selected}
        clearSearch={clearSearch}
        bulkActive={() =>
          showPrompt(language.ACTIVE, language.PROMPT_STATUS, "status", () =>
            setBulkActive()
          )
        }
        bulkInActive={() =>
          showPrompt(language.INACTIVE, language.PROMPT_STATUS, "status", () =>
            setBulkInActive()
          )
        }
        bulkArchive={() =>
          showPrompt(language.ARCHIVE, language.PROMPT_ARCHIEVE, "archieve", () =>
            setBulkArchieve()
          )
        }
        bulkRestore={() =>
          showPrompt(language.RESTORE, language.PROMPT_RESTORE, "archieve", () =>
            setBulkRestore()
          )
        }
        // bulkDelete={() => showPrompt(language.DELETE, language.PROMPT_DELETE, "delete", () => setBulkDelete())}
        total={total}
        icon={showField === language.ARCHIEVE ? <FiArchive /> : <FiUsers />}
        title={
          showField === language.ALL ||
          showField === language.ACTIVE ||
          showField === language.INACTIVE ||
          showField === language.ARCHIEVE
            ? showField + " " + title
            : showField
        }
        showAdd={showAdd && add}
        showPrivilege={showPrivilege}
        privilegeClick={privilegeClick}
        showDownload={showDownload}
      >
        {loading === false &&
          (fields.length > 0 ? (
            <DataTable
              bulkSelect={showBulk && edit}
              showRides={showRides}
              selected={selected}
              selectAll={() => setSelected(fields.map((field) => field._id))}
              deselectAll={() => setSelected([])}
              select={(e) =>
                selected.filter((field) => field === e).length > 0
                  ? setSelected(selected.filter((field) => field != e))
                  : setSelected([...selected, e])
              }
              showAction={showAction}
              corporateBillingAmountClick={corporateBillingAmountClick}
              corporateConfirmPay={(id) => setCorporatePayModel(id)}
              loadMore={loadMore}
              enableFreshClick={enableFreshClick}
              total={total}
              fields={fields}
              archieveClick={(e) =>
                showPrompt(language.ARCHIVE, language.PROMPT_ARCHIEVE, "status", () =>
                  archieveField(e)
                )
              }
              showMembers={showMembers}
              changeStatus={(e) =>
                edit === true
                  ? showPrompt(language.STATUS, language.PROMPT_STATUS, "status", () =>
                      toggleStatusField(e)
                    )
                  : null
              }
              // deleteClick={(e) => showPrompt(language.DELETE, language.PROMPT_DELETE, "delete", () => deleteField(e))}
              showArchieve={
                showField === language.ARCHIEVE
                  ? false
                  : showArchieve && edit
                  ? true
                  : false
              }
              editClick={(e) =>
                showNotificationTypeFilter
                  ? editClick(e, notificationTypeStatus)
                  : editClick(e)
              }
              restoreClick={(e) =>
                showPrompt(language.RESTORE, language.PROMPT_RESTORE, "status", () =>
                  restoreField(e)
                )
              }
              showVerify={showVerify}
              showStatusCheck={showStatusCheck}
              showView={showView}
              viewClick={viewClick}
              showUnarchieve={showField === language.ARCHIEVE && edit === true}
              showEdit={
                showEdit && edit
                  ? showField === language.ARCHIEVE
                    ? false
                    : true
                  : false
              }
              headings={headings}
              unregisterStatusClick={(id) =>
                showPrompt(language.ATTEND, language.PROMPT_ATTEND, "status", () =>
                  unregisterStatusClick(id)
                )
              }
              showDelete={showDelete}
              showRefund={showRefund}
              deleteClick={(id) =>
                showPrompt(language.DELETE, language.PROMPT_DELETE, "status", () =>
                  deleteClick(id)
                )
              }
              membersClick={membersClick}
              showNotes={showNotes}
              showAssignDriver={showAssignDriver}
              assignDriverClick={assignDriverClick}
              rideClick={rideClick}
              showMoney={showMoney}
              showVehicle={showField !== language.ARCHIEVE ? showVehicle : false}
              showTranslate={showTranslate}
              moneyClick={moneyClick}
              showStops={showStops}
              showInfo={showInfo}
              infoClick={(e) => {
                setInfoText(fields.filter((each) => each._id === e)[0].infoText);
              }}
              vehicleClick={vehicleClick}
              showUnregisteredDelete={showUnregisteredDelete}
              translateClick={translateClick}
              verifyClick={verifyClick}
              changeDefault={(e) =>
                showPrompt(
                  fields.filter((field) => field._id === e)[0].lang_name,
                  language.PROMPT_DEFAULT_LANGUAGE,
                  "add",
                  () => setDefault(e)
                )
              }
              notesClick={(e) => {
                setNote(e);
                setNoteText(fields.filter((each) => each._id === e)[0].notes);
              }}
            />
          ) : (
            <NoData></NoData>
          ))}
      </TableWrapperRight>
    </TableWrapper>
  );
}
