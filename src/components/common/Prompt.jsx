import React from "react";
import { FiCheck, FiTrash, FiX } from "react-icons/fi";
import gsap from "gsap";

export const Prompt = ({ title, message, type, click =() => {}, close }) => {
  React.useEffect(() => {
    gsap.fromTo(
      ".prompt",
      0.3,
      { opacity: 0 },
      { opacity: 1, stagger: 0.1 }
    );
  }, []);
  return (
    <div className="fixed opacity-0 prompt inset-0 bg-black z-40 bg-opacity-75 flex justify-center items-center">
      <div
        className={`p-5 opacity-0 prompt bg-white dark:bg-gray-900 rounded-xl border-4 ${
          type === "delete" ? "border-red-500" : "border-blue-800"
        } shadow-xl space-y-3`}
      >
        <div className="opacity-0 prompt text-lg font-bold text-gray-800">
          <h1 className={type === "delete" ? "text-red-500" : "text-blue-800"}>
            {title}
          </h1>
        </div>
        <div className="opacity-0 prompt text-sm text-gray-800 dark:text-gray-200">
          <h1>{message}</h1>
        </div>
        <div className="opacity-0 prompt flex border-t-2 pt-3">
          <div
            onClick={close}
            className="cursor-pointer w-1/2 flex justify-center items-center"
          >
            <div>
              <FiX />
            </div>
          </div>
          <div
            onClick={(e) => {
              close();
              click(e);
            }}
            className="cursor-pointer w-1/2 flex justify-center items-center"
          >
            {type === "delete" ? (
              <FiTrash className="text-red-500" />
            ) : (
              <FiCheck />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
