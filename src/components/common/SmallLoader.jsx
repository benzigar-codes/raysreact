import React from "react";
import { FaSpinner } from "react-icons/fa";

export const SmallLoader = () => (
  <div className="h-screen flex justify-center dark:text-white items-center mt-22">
    <FaSpinner className="animate-spin" />
  </div>
);
