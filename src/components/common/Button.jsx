import React from "react";
import { FiCheck, FiSave } from "react-icons/fi";
import { FaSpinner } from "react-icons/fa";

export const Button = ({
  click = () => {},
  loading = false,
  title,
  color = "",
  done,
  icon,
  className,
  iconPosition = "left",
  ...rest
}) => (
  <button
    onClick={click}
    disabled={loading}
    className={`focus:outline-none dark:border-gray-700 border-gray-200 dark:text-gray-200 cursor-pointer hover:border-gray-900 dark:hover:border-gray-200 border-2 border-black flex items-center text-black text-sm rounded-full px-3 py-2 mx-1 ${className}`}
    {...rest}
  >
    <div className={document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"}>
      {iconPosition === "left" ? done === true ? <FiCheck /> : icon : null}
    </div>
    <p className={document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"}>{title}</p>
    {iconPosition === "right" ? done === true ? <FiCheck /> : icon : null}
    {loading === true && <FaSpinner className="animate-spin" />}
  </button>
);
