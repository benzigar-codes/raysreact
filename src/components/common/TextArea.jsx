import React from "react";

export const TextArea = ({
  width = "full",
  error = "",
  type = "text",
  required = false,
  icon = "",
  hint = "",
  iconSelect = () => {},
  change = () => {},
  placeholder = "",
  className = "",
  padding = true,
  rows = 8,
  margin = 0,
  ...rest
}) => {
  const direction = document.getElementsByTagName("html")[0].getAttribute("dir");
  return (
    <div
      className={`relative w-${width} ${
        document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? `mr-${margin}` : `ml-${margin}`
      }`}
    >
      <div
        onClick={iconSelect}
        className={`absolute cursor-pointer ${
          direction === "ltr" ? "right-0" : "left-0"
        } text-gray-500 flex justify-center items-center bg-gray-100 dark:bg-gray-900 py-3 mx-3 rounded-xl ${
          error != "" && "text-red-800 mt-1"
        }`}
      >
        {icon}
      </div>
      <textarea
        onChange={(e) => change(e.target.value)}
        type={type}
        name=""
        rows={rows}
        required={required}
        placeholder={placeholder}
        className={
          className +
          ` w-full dark:bg-gray-900 text-sm focus:outline-none bg-gray-200 rounded-xl px-4 py-3 ${
            error != "" && "border-2 border-red-500 dark:border-red-700"
          }`
        }
        id=""
        {...rest}
      />
      {hint && error === "" && (
        <div className="flex">
          <div className="mx-2">
            {hint && (
              <p className="text-gray-300 dark:text-gray-500 text-sm mt-1" style={{ fontSize: 11 }}>
                {hint}
              </p>
            )}
          </div>
        </div>
      )}
      {error != "" && (
        <div className="flex">
          <div className="mx-2">
            {error && (
              <p className="text-red-600 dark:text-red-500 text-sm mt-1" style={{ fontSize: 11 }}>
                {error}
              </p>
            )}
          </div>
        </div>
      )}
    </div>
  );
};
