import React from "react";

export const TextField = ({
  width = "full",
  error = "",
  type = "text",
  required = false,
  icon = "",
  hint = "",
  iconSelect = () => {},
  change = () => {},
  placeholder = "",
  className = "",
  padding = true,
  margin = 0,
  align = "",
  ref = null,
  ...rest
}) => {
  const direction = document.getElementsByTagName("html")[0].getAttribute("dir");
  return (
    <div
      className={`relative w-8/12 w-4/12 w-auto w-${width} rounded-xl overflow-hidden  ${
        document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
          ? `mr-${margin}`
          : `ml-${margin}`
      }`}
    >
      {icon !== "" && error === "" && (
        <div
          onClick={iconSelect}
          className={`text-sm absolute cursor-pointer ${
            direction === "ltr" ? "right-0" : "left-0"
          } text-gray-400 flex justify-center items-center ${
            error !== "" && "border-red-500 text-white placeholder-white"
          }`}
        >
          <span
            className={`bg-gray-100 p-3 dark:bg-gray-900 ${
              error !== "" && "border-red-500 text-white placeholder-white"
            }`}
          >
            {icon}
          </span>
        </div>
      )}
      <input
        ref={ref}
        onChange={(e) => change(e.target.value)}
        type={type}
        onWheel={(e) => e.currentTarget.blur()}
        name=""
        required={required}
        placeholder={placeholder}
        className={
          className +
          ` w-full ${
            align === "center" && "text-center"
          } dark:bg-gray-900 text-sm focus:outline-none bg-gray-100 rounded-xl px-4 py-3 border-b-2 border-gray-100 dark:border-gray-800 dark:focus:border-blue-800 focus:border-blue-800 ${
            error !== "" &&
            "border-red-500 border-2 placeholder-red-500 dark:placeholder-white"
          }`
        }
        id=""
        {...rest}
      />
      {hint && error === "" && (
        <div className="w-full flex">
          <div className="mx-2">
            {hint && (
              <p
                className="text-gray-400 dark:text-gray-500 text-sm mt-1"
                style={{ fontSize: 11 }}
              >
                {hint}
              </p>
            )}
          </div>
        </div>
      )}
      {error !== "" && (
        <div className="flex">
          <div className="mx-2">
            {error && (
              <p
                className="text-red-600 dark:text-red-500 text-sm mt-1"
                style={{ fontSize: 11 }}
              >
                {error}
              </p>
            )}
          </div>
        </div>
      )}
    </div>
  );
};
