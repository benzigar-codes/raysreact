import React from "react";

export const MultiSelect = ({ allFields = [], defaultValue = [], error = "", hint = "", change = () => {} }) => {
  // const [defaultValue, change] = React.useState(defaultValue);
  // React.useEffect(() => change(defaultValue), [defaultValue]);

  return (
    <div>
      <div className={`flex flex-wrap ${error !== "" && "border-2 rounded-xl p-2 border-red-500"}`}>
        {allFields.map((limit) =>
          defaultValue.filter((each) => each == limit).length > 0 ? (
            <p
              key={limit}
              onClick={() => change(defaultValue.filter((f) => f !== limit))}
              className={`${
                document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
              } my-1 cursor-pointer bg-blue-800 px-3 py-2 text-sm rounded-full text-white`}
            >
              {limit}
            </p>
          ) : (
            <p
              key={limit}
              onClick={() => change([...defaultValue, limit.toString()])}
              className={`${
                document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
              } my-1 cursor-pointer dark:bg-gray-800 dark:text-900 bg-white px-3 border-2 py-1 text-sm rounded-full opacity-50`}
            >
              {limit}
            </p>
          )
        )}
      </div>
      {hint && error === "" && (
        <div className="flex">
          <div className="mx-2">
            {hint && (
              <p className="text-gray-300 dark:text-gray-500 text-sm mt-1" style={{ fontSize: 11 }}>
                {hint}
              </p>
            )}
          </div>
        </div>
      )}
      {error !== "" && (
        <div className="flex">
          <div className="mx-2">
            {error && (
              <p className="text-red-600 dark:text-red-500 text-sm mt-1" style={{ fontSize: 11 }}>
                {error}
              </p>
            )}
          </div>
        </div>
      )}
    </div>
  );
};
