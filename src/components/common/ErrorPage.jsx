import React from "react";
import { FiInfo } from "react-icons/fi";
import { useHistory } from "react-router-dom";

import useLanguage from "../../hooks/useLanguage";
import NavLinks from "../../utils/navLinks.json";

export default function ErrorPage() {
  const { language } = useLanguage();
  const history = useHistory();
  const [counter, setCounter] = React.useState(8);
  const counterInterval = React.useRef();
  React.useEffect(() => {
    counterInterval.current = setInterval(() => setCounter((counter) => counter - 1), 1000);
  }, []);
  React.useEffect(() => {
    if (counter === 0) {
      clearInterval(counterInterval.current);
      history.replace(NavLinks.ADMIN_HOME);
    }
  }, [counter]);
  return (
    <div className="flex flex-col justify-center items-center" style={{ height: 500 }}>
      <div className="flex items-center">
        <FiInfo className="mx-4 text-5xl" />
        <h1 className="text-4xl font-bold">{language.SOMETHING_WENT_WRONG}</h1>
      </div>
      <p className="mt-2">
        {language.REDIRECTING} {counter}
      </p>
    </div>
  );
}
