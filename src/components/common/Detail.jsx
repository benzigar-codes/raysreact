import React from "react";

export const Detail = ({ title, value, ...rest }) => (
  <>
    <div className="p-1 w-1/3 border-b-2 border-gray-100 dark:border-gray-900">{title}</div>
    <div className="p-1 w-2/3 border-b-2 border-gray-100 dark:border-gray-900 text-gray-500 dark:text-gray-300">
      <p className="" title={typeof value !== "object" ? value : false} {...rest}>
        {value}
      </p>
    </div>
  </>
);
