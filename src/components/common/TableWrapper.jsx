import React from "react";

export const TableWrapper = ({ children }) => (
  <div
    className="flex"
    style={{
      height: "90vh",
    }}
  >
    {children}
  </div>
);
