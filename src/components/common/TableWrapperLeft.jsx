import React from "react";

export const TableWrapperLeft = ({ children, width = "2/12" }) => (
  <div className={`hidden lg:block w-2/12 border-r-2 dark:border-gray-700 p-4 text-sm text-gray-800 overflow-y-scroll`}>
    <div className="flex flex-col h-full justify-between">
      <div className="p-3 flex flex-col">{children}</div>
    </div>
  </div>
);
