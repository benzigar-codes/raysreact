import { addDays, isToday, setHours, setMinutes } from "date-fns";
import React from "react";
import { forwardRef } from "react";
import ReactDatePicker from "react-datepicker";
import { TextField } from "./TextField";

export default function CalenderDatePicker({
  disableBeforeDays = false,
  afterDays = false,
  defaultValue = false,
  startTime = false,
  change = () => {},
  showTime = false,
  ...rest
}) {
  const filterPassedTime = (time) => {
    const currentDate = startTime;
    const selectedDate = new Date(time);

    return currentDate.getTime() < selectedDate.getTime();
  };
  return (
    <ReactDatePicker
      className={`w-full dark:bg-gray-900 text-sm focus:outline-none bg-gray-100 rounded-xl px-4 py-3 border-b-2 border-gray-100 dark:border-gray-800 dark:focus:border-blue-800 focus:border-blue-800`}
      selected={defaultValue}
      timeIntervals={15}
      showTimeSelect={showTime}
      dateFormat={showTime ? "MMMM d, yyyy | h:mm aa" : "MMMM d, yyyy"}
      filterTime={startTime ? filterPassedTime : false}
      maxDate={afterDays ? addDays(new Date(), afterDays) : false}
      minDate={disableBeforeDays ? new Date() : false}
      onChange={(e) => change(e)}
    />
  );
}
