import React from "react";
import {
  FiAlertCircle,
  FiArchive,
  FiBell,
  FiCheck,
  FiCheckCircle,
  FiDelete,
  FiDownload,
  FiEdit,
  FiEye,
  FiLoader,
  FiTrash,
  FiUserPlus,
  FiUsers,
} from "react-icons/fi";
import { MdPlace } from "react-icons/md";
import { CgNotes } from "react-icons/cg";
import { IoCloseSharp } from "react-icons/io5";
import {
  AiFillCloseCircle,
  AiOutlineCar,
  AiOutlineClose,
  AiOutlineTranslation,
} from "react-icons/ai";
import { GoDeviceMobile } from "react-icons/go";
import { BiMoney, BiRecycle } from "react-icons/bi";
import { IoMdCash } from "react-icons/io";
import gsap from "gsap";
import { Waypoint } from "react-waypoint";
import U from "../../utils/utils.js";

import { ToggleButton } from "./ToggleButton";
import { CheckBoxWhite } from "./CheckBoxWhite";
import { CheckBox } from "./CheckBox";
import useLanguage from "../../hooks/useLanguage";
import useUtils from "../../hooks/useUtils";
import useAdmin from "../../hooks/useAdmin";

export const DataTable = ({
  headings = [],
  fields = [],
  selected = [],
  total = 1,
  selectAll = () => {},
  deselectAll = () => {},
  select = () => {},
  changeStatus = () => {},
  changeDefault = () => {},
  assignDriverClick = () => {},
  loadMore = () => {},
  bulkSelect = true,
  showUnarchieve = false,
  showAction = true,
  showEdit = true,
  enableFreshClick = true,
  showView = true,
  showTranslate = false,
  showUnregisteredDelete = false,
  showArchieve = true,
  showDelete = true,
  showNotes = false,
  showMobile = false,
  showAssignDriver = false,
  showMoney = false,
  showVehicle = false,
  showVerify = false,
  showNotify = false,
  showMembers = false,
  showStops = false,
  showRides = false,
  showRefund = false,
  showInfo = false,
  showStatusCheck = false,
  statusCheckClick = () => {},
  refundClick = () => {},
  verifyClick = () => {},
  unregisterStatusClick = () => {},
  corporateConfirmPay = () => {},
  restoreClick = () => {},
  deleteClick = () => {},
  editClick = () => {},
  infoClick = () => {},
  viewClick = () => {},
  translateClick = () => {},
  archieveClick = () => {},
  mobileClick = () => {},
  notesClick = () => {},
  moneyClick = () => {},
  vehicleClick = () => {},
  membersClick = () => {},
  corporateBillingAmountClick = () => {},
  rideClick = () => {},
}) => {
  const { language } = useLanguage();
  const { morph } = useUtils();
  const { admin } = useAdmin();
  React.useEffect(() => {
    gsap.fromTo(".dataTable", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, []);
  return (
    <div className="w-full">
      <div
        className="w-full shadow-md mt-4 rounded-lg overflow-y-scroll opacity-0 dataTable"
        style={{ maxHeight: "79vh" }}
      >
        <table
          cellPadding="12"
          className="w-full relative text-gray-800 text-left dark:bg-gray-800 opacity-0 dataTable"
          style={{ fontSize: 13 }}
        >
          <thead className="z-10">
            <tr>
              {bulkSelect === true && (
                <td className="sticky top-0 bg-blue-800 text-white">
                  <CheckBoxWhite
                    onClick={selected.length === fields.length ? deselectAll : selectAll}
                    defaultValue={selected.length === fields.length ? 1 : 0}
                  />
                </td>
              )}
              <td className="text-center sticky top-0 bg-blue-800 text-white">
                {language.TABLE_NO}
              </td>
              {headings
                .filter((field) => field.show === true)
                .map((field) => (
                  <td key={field.id} className="sticky top-0 bg-blue-800 text-white">
                    {field.title}
                  </td>
                ))}
              {showAction === true && (
                <td className="text-center sticky top-0 bg-blue-800 text-white">
                  {language.ACTION}
                </td>
              )}
            </tr>
          </thead>
          <tbody className="text-sm text-gray-600 dark:bg-gray-800">
            {fields.map((field, index) => (
              <tr
                key={field._id}
                className="bg-white dark:bg-gray-800 dark:text-gray-200 dark:border-gray-900 hover:bg-gray-100 cursor-pointer border-b-4 border-gray-100"
              >
                {bulkSelect === true && (
                  <td onClick={() => select(field._id)}>
                    <CheckBox
                      defaultValue={
                        selected.filter((select) => select === field._id).length > 0
                          ? 1
                          : 0
                      }
                    />
                  </td>
                )}
                <td
                  onClick={() => bulkSelect === true && select(field._id)}
                  className="text-center"
                >
                  {index + 1}
                </td>
                {headings.map((head) => {
                  if (head.show === true) {
                    if (head.key === "status") {
                      if (field.default_status === 1)
                        return (
                          <td className="opacity-25">
                            <ToggleButton defaultValue={field[head.key]} />
                          </td>
                        );
                      else
                        return (
                          <td onClick={() => changeStatus(field._id)}>
                            <ToggleButton defaultValue={field[head.key]} />
                          </td>
                        );
                    } else if (head.key === "default_status") {
                      if (field.status === 0)
                        return (
                          <td className="opacity-25">
                            <ToggleButton defaultValue={field[head.key]} />
                          </td>
                        );
                      else
                        return (
                          <td onClick={() => changeDefault(field._id)}>
                            <ToggleButton defaultValue={field[head.key]} />
                          </td>
                        );
                    } else if (head.key === "payment_type") {
                      if (field.payment_type === 1)
                        return (
                          <td>
                            <button
                              onClick={() => editClick(field._id)}
                              data-data-title={language.EDIT}
                              className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                                document
                                  .getElementsByTagName("html")[0]
                                  .getAttribute("dir") === "ltr"
                                  ? "mr-2"
                                  : "ml-2"
                              }`}
                            >
                              <FiEdit />
                            </button>
                          </td>
                        );
                      else return <td></td>;
                    } else if (head.key === "unregister_status") {
                      if (field.unregister_status === U.ATTENDED)
                        return (
                          <td>
                            <div className="flex items-center mt-1">
                              <FiCheckCircle className="text-xl text-purple-800 dark:text-purple-500" />
                              <p className="mx-2">{language.ATTENDED}</p>
                            </div>
                          </td>
                        );
                      else if (field.unregister_status === U.CLOSED)
                        return (
                          <td>
                            <div className="flex items-center mt-1">
                              <FiCheck className="text-xl text-purple-800 dark:text-purple-500" />
                              <p className="mx-2">{language.CLOSED}</p>
                            </div>
                          </td>
                        );
                      else
                        return (
                          // <td>{U.INC</td>
                          <td
                            onClick={() =>
                              enableFreshClick && unregisterStatusClick(field._id)
                            }
                          >
                            <div className="flex items-center mt-1">
                              <FiAlertCircle className="text-xl text-red-500" />
                              <p className="mx-2">{language.INCOMPLETE}</p>
                            </div>
                          </td>
                        );
                    } else if (head.key === "driver_documents")
                      return (
                        <td>
                          {field.driver_documents.map((document) =>
                            document.isExpired === 2 ? (
                              <p className="text-red-500">{document.docs_name}</p>
                            ) : (
                              <p>{document.docs_name}</p>
                            )
                          )}
                        </td>
                      );
                    else if (head.key === "vehicle_documents")
                      return <p>Vehicle Docments</p>;
                    else if (head.key === "oneByOne")
                      return (
                        <td>
                          {field.oneByOne.map((each) => (
                            <p className="mt-2">{each}</p>
                          ))}
                        </td>
                      );
                    else if (head.key === "oneByOne2")
                      return (
                        <td>
                          {field.oneByOne2.map((each) => (
                            <p className="mt-2">{each}</p>
                          ))}
                        </td>
                      );
                    else if (head.key === "oneByOne3")
                      return (
                        <td>
                          {field.oneByOne3.map((each) => (
                            <p className="mt-2">{each}</p>
                          ))}
                        </td>
                      );
                    else if (head.key === "corporatePayment")
                      return (
                        <td>
                          {field.corporatePayment === true ? (
                            <div className="flex items-center">
                              {language.PAID}
                              <FiCheck className="text-xl mx-3 text-blue-800" />
                            </div>
                          ) : admin.userType !== U.CORPORATE ? (
                            <div
                              onClick={() => corporateConfirmPay(field._id)}
                              className="flex"
                            >
                              <div className="p-2 bg-red-500 text-sm text-white rounded-xl">
                                {language.CLICK_TO_CONFIRM_PAYMENT}
                              </div>
                            </div>
                          ) : (
                            <div className="flex items-center">{language.UNPAID}</div>
                          )}
                        </td>
                      );
                    else if (head.key === "corporateBillingAmountButton")
                      return (
                        <td align="center">
                          <div className="flex justify-center items-center">
                            <button
                              onClick={() =>
                                corporateBillingAmountClick(
                                  field.corporateBillingAmountButton
                                )
                              }
                              title={language.VIEW}
                              className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                                document
                                  .getElementsByTagName("html")[0]
                                  .getAttribute("dir") === "ltr"
                                  ? "mr-2"
                                  : "ml-2"
                              }`}
                            >
                              <FiEye />
                            </button>
                          </div>
                        </td>
                      );
                    else if (head.key === "downloadPdf")
                      return (
                        <td align="center">
                          <div className="flex justify-center items-center">
                            {field.downloadPdf && field.downloadPdf.includes(".pdf") ? (
                              <button
                                onClick={() => window.open(field.downloadPdf, "_blank")}
                                title={language.DOWNLOAD}
                                className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                                  document
                                    .getElementsByTagName("html")[0]
                                    .getAttribute("dir") === "ltr"
                                    ? "mr-2"
                                    : "ml-2"
                                }`}
                              >
                                <FiDownload />
                              </button>
                            ) : (
                              <div></div>
                            )}
                          </div>
                        </td>
                      );
                    else
                      return (
                        <td onClick={() => bulkSelect === true && select(field._id)}>
                          {field[head.key]}
                        </td>
                      );
                  }
                })}
                {showAction === true && (
                  <td className="w-28">
                    <div className="flex items-center justify-center">
                      {showMembers === true && (
                        <button
                          onClick={() =>
                            membersClick(field._id, field.hubsName ? field.hubsName : "")
                          }
                          title={language.MEMBERS}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <FiUsers />
                        </button>
                      )}
                      {showAssignDriver === true && (
                        <button
                          onClick={() =>
                            field.isAssignAllowed ? assignDriverClick(field._id) : null
                          }
                          title={language.ASSIGN_DRIVER}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          } ${field.isAssignAllowed === false && "opacity-50"} `}
                        >
                          <FiUserPlus />
                        </button>
                      )}
                      {showView === true && (
                        <button
                          onClick={() => viewClick(field._id)}
                          title={language.VIEW}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <FiEye />
                        </button>
                      )}
                      {showMoney === true && (
                        <button
                          onClick={() => moneyClick(field._id)}
                          title={language.BANK_DETAILS}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <IoMdCash />
                        </button>
                      )}
                      {showMobile === true && (
                        <button
                          onClick={() => mobileClick(field._id)}
                          title={language.MOBILE}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <GoDeviceMobile />
                        </button>
                      )}
                      {showNotes === true && (
                        <button
                          onClick={() => notesClick(field._id)}
                          title={language.NOTES}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <CgNotes />
                        </button>
                      )}
                      {showInfo === true && (
                        <button
                          onClick={() => infoClick(field._id)}
                          title={language.NOTES}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <CgNotes />
                        </button>
                      )}
                      {showEdit === true && (
                        <button
                          onClick={() => editClick(field._id)}
                          title={language.EDIT}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <FiEdit />
                        </button>
                      )}
                      {showVehicle === true && (
                        <button
                          onClick={() => vehicleClick(field._id)}
                          title={language.VEHICLES}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <AiOutlineCar />
                        </button>
                      )}
                      {showRides === true && (
                        <button
                          onClick={() => rideClick(field._id)}
                          title={language.RIDES}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <AiOutlineCar />
                        </button>
                      )}
                      {showStops === true && (
                        <button
                          onClick={() => vehicleClick(field._id)}
                          title={language.STOPS}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <MdPlace />
                        </button>
                      )}
                      {showTranslate === true && (
                        <button
                          onClick={() => translateClick(field._id)}
                          title={language.TRANSLATE}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <AiOutlineTranslation />
                        </button>
                      )}
                      {showUnarchieve === true && (
                        <button
                          onClick={() => restoreClick(field._id)}
                          title={language.RESTORE}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <BiRecycle />
                        </button>
                      )}
                      {showArchieve === true && (
                        <button
                          onClick={() => archieveClick(field._id)}
                          title={language.ARCHIEVE}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <FiArchive />
                        </button>
                      )}
                      {field.default_status !== 1 && showDelete === true && (
                        <button
                          onClick={() => deleteClick(field._id)}
                          title={language.DELETE}
                          className={`bg-red-500 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <IoCloseSharp />
                        </button>
                      )}{" "}
                      {field.unregister_status === U.ATTENDED &&
                        showUnregisteredDelete === true && (
                          <button
                            onClick={() => deleteClick(field._id)}
                            title={language.DELETE}
                            className={`bg-red-500 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                              document
                                .getElementsByTagName("html")[0]
                                .getAttribute("dir") === "ltr"
                                ? "mr-2"
                                : "ml-2"
                            }`}
                          >
                            <IoCloseSharp />
                          </button>
                        )}
                      {showRefund === true && (
                        <div
                          onClick={() =>
                            field.verifyStatus !== language.SUCCESS &&
                            field.verifyStatus !== language.PROCESSING
                              ? refundClick(field._id)
                              : () => {}
                          }
                          className={`mx-2 bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 px-3 flex items-center ${
                            !(
                              field.verifyStatus !== language.SUCCESS &&
                              field.verifyStatus !== language.PROCESSING
                            ) && "opacity-0"
                          }`}
                        >
                          <p className="text-sm">{language.REFUND}</p>
                          <div className="mx-1"></div>
                          {/* <button
                            onClick={() => refundClick(field._id)}
                            title={language.REFUND}
                            // style={{ backgroundColor: "#6EBC41" }}
                          >
                            <FiCheck />
                          </button> */}
                        </div>
                      )}
                      {showStatusCheck && (
                        <div
                          onClick={() => statusCheckClick(field._id)}
                          className="mx-2 bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 px-3 flex items-center"
                        >
                          <p className="text-sm">{language.CHECK_CURRENT_STATUS}</p>
                        </div>
                      )}
                      {showVerify === true && (
                        <div
                          onClick={() => verifyClick(field._id)}
                          className="mx-2 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 px-3 flex items-center"
                          style={{ backgroundColor: "green" }}
                        >
                          <p className="text-sm">{language.VERIFY}</p>
                          <div className="mx-1"></div>
                          <button
                            onClick={() => verifyClick(field._id)}
                            title={language.VERIFY}
                            // style={{ backgroundColor: "#6EBC41" }}
                          >
                            <FiCheck />
                          </button>
                        </div>
                      )}
                      {showNotify === true && (
                        <button
                          onClick={() => deleteClick(field._id)}
                          title={language.DELETE}
                          className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
                            document
                              .getElementsByTagName("html")[0]
                              .getAttribute("dir") === "ltr"
                              ? "mr-2"
                              : "ml-2"
                          }`}
                        >
                          <FiBell />
                        </button>
                      )}
                    </div>
                  </td>
                )}
              </tr>
            ))}
            {fields.length != total && total != 0 && (
              <Waypoint onEnter={loadMore}>
                <tr>
                  <td colSpan={fields.length + 2} align="center">
                    <FiLoader className="animate-spin" />
                  </td>
                </tr>
              </Waypoint>
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
};
