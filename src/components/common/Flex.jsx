import React from "react";

const Flex = ({ children, between = false, border = false, align = true, wrap = false, ...rest }) => {
  return (
    <div
      className={`flex ${wrap === true && "flex-wrap"} md:flex-nowrap ${between === true && "justify-between"} ${align === true && "items-center"}`}
      {...rest}
    >
      {children}
    </div>
  );
};

export default Flex;
