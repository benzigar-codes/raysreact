import React from "react";
import gsap from "gsap";

export const Modal = ({ children }) => {
  React.useEffect(() => {
    gsap.fromTo(".modal", 0.3, { opacity: 0, y:10 }, { opacity: 1, y:0,  stagger: 0.1 });
  }, []);
  return (
    <div className="opacity-0 modal fixed inset-0 bg-black bg-opacity-50 z-40 flex justify-center items-center">
      <div className="opacity-0 modal w-full">{children}</div>
    </div>
  );
};
