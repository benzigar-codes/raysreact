import React from "react";

export const Hint = ({ title, padding = true }) => (
  <div className={`${padding === true && "px-3 py-1"}`}>
    <p
      className="text-gray-400"
      style={{
        fontSize: 12,
      }}
    >
      {title}
    </p>
  </div>
);
