import React from "react";
import useLanguage from "../../hooks/useLanguage";
import useImage from "../../hooks/useImage";
import { FiLoader } from "react-icons/fi";

export const Documents = ({
  _id,
  loading = false,
  title,
  imageClick = () => {},
  image,
  expireDate,
  verified,
  showVerifyBtn = true,
  verifyText = "",
  verifyClick = () => {},
}) => {
  const { language } = useLanguage();
  return (
    <div className="px-3 py-2">
      <div className="flex justify-between items-center mb-2">
        <div className={"flex items-center"}>
          <img
            onClick={() => imageClick(image)}
            className="h-10 w-10 object-cover cursor-pointer rounded-full"
            src={image}
            alt={"Driver"}
          />
          <div>
            <p className="px-3 text-md text-gray-600 dark:text-gray-300">{title}</p>
            {expireDate && (
              <p className="px-3 text-sm text-gray-500 dark:text-gray-300">
                {language.EXPIRY_DATE} : {expireDate}
              </p>
            )}
          </div>
        </div>
        {showVerifyBtn && verified === true ? (
          <button
            onClick={() => verifyClick(_id)}
            className={`cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-1 px-2 text-sm ${
              document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
            }`}
            style={{ backgroundColor: "#6EBC41" }}
          >
            <p style={{ fontSize: 13 }}>
              {loading === false ? verifyText : <FiLoader className={"text-md animate-spin"} />}
            </p>
          </button>
        ) : (
          showVerifyBtn && (
            <button
              onClick={() => verifyClick(_id)}
              className={`bg-red-600 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-1 px-2 text-sm ${
                document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
              }`}
            >
              <p
                style={{
                  fontSize: 13,
                }}
              >
                {loading === false ? verifyText : <FiLoader className={"text-md animate-spin"} />}
              </p>
            </button>
          )
        )}
      </div>
    </div>
  );
};
