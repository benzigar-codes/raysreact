import React from "react";
import { MdCheckBox, MdCheckBoxOutlineBlank } from "react-icons/md";


export const CheckBoxWhite = ({
  defaultValue = 0,
  change = () => { },
  className,
  ...rest
}) => {
  const toggle = defaultValue;
  const toggleData = (e) => {
    change(e);
  };

  return toggle == 1 ? (
    <MdCheckBox
      onClick={() => toggleData(0)}
      className={`cursor-pointer text-lg text-white ${className}`}
      {...rest} />
  ) : (
      <MdCheckBoxOutlineBlank
        onClick={() => toggleData(1)}
        className={`cursor-pointer text-lg text-white ${className}`}
        {...rest} />
    );
};
