import React from "react";
import {
  FiCheck,
  FiChevronLeft,
  FiChevronRight,
  FiEdit,
  FiHome,
  FiInfo,
  FiLoader,
  FiMinus,
  FiSave,
  FiSettings,
  FiX,
} from "react-icons/fi";
import FlexCol from "./FlexCol";
import Flex from "./Flex";
import { Button } from "./Button";
import useLanguage from "../../hooks/useLanguage";
import { useHistory } from "react-router-dom";
import NavLinks from "../../utils/navLinks.json";

export const FormWrapper = ({
  width = "auto",
  closeBtn = false,
  animate,
  done = false,
  min_width = "full",
  className,
  downloadBtn = false,
  submitBtn = false,
  btnLoading = false,
  showVerify = false,
  verifyClick = () => {},
  model = false,
  info = null,
  submit = () => {},
  closeClick = () => {},
  topBar = true,
  bread = [],
  title,
  editBtn = false,
  editBtnClick = () => {},
  enterSubmit = true,
  page = null,
  totalPage = null,
  showNext = false,
  showPrev = false,
  nextClick = () => {},
  saveBtn = false,
  prevClick = () => {},
  icon = <FiSettings />,
  children,
  marginBottom = true,
}) => {
  const { language } = useLanguage();
  const history = useHistory();
  document.title = bread.length > 0 ? bread.map((each) => each.title).join(",") + " " + title : title;
  return (
    <>
      {info !== null && (
        <div className={"bg-red-600 w-full p-4 text-white flex justify-center items-center text-sm"}>
          <FiInfo className={"text-lg mx-2"} /> {info}
        </div>
      )}
      <div
        onKeyDown={(e) => e.key === "Enter" && enterSubmit === true && submit()}
        className={`w-full flex justify-center ${marginBottom === true && "mb-24"} ${className} ${
          animate && "opacity-0 " + animate
        }`}
      >
        <FlexCol
          className={`w-${model === true ? width : min_width} lg:w-${width} bg-white dark:bg-gray-800 dark:text-white ${
            model === true ? "p-8" : "px-2 lg:px-8"
          } rounded-xl ${className}`}
        >
          {topBar === true && (
            <h1
              className={`flex ${
                model === false && " pt-5 pb-2"
              } bg-white dark:bg-gray-800 items-center justify-between font-bold text-2xl ${
                model === false && "mb-2"
              } text-black dark:text-gray-200`}
              style={{ position: "sticky", top: 60, zIndex: 15 }}
            >
              <div
                className={`text-base lg:text-lg xl:text-xl flex items-center font-normal truncate ${
                  animate && "opacity-0 " + animate
                }`}
              >
                <div
                  onClick={() => history.push(NavLinks.ADMIN_HOME)}
                  className={
                    document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
                      ? "text-gray-500 flex items-center cursor-pointer hover:text-blue-800"
                      : "text-gray-500 flex items-center cursor-pointer hover:text-blue-800"
                  }
                >
                  {<FiHome />}
                  {document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? (
                    <FiChevronRight />
                  ) : (
                    <FiChevronLeft />
                  )}
                </div>
                <h1 className="flex items-center">
                  {bread.map((el) => (
                    <span
                      key={el.id}
                      onClick={() => history.push(el.path)}
                      className="flex items-center text-gray-500 cursor-pointer hover:underline"
                    >
                      {el.title}{" "}
                      {document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? (
                        <FiChevronRight />
                      ) : (
                        <FiChevronLeft />
                      )}
                    </span>
                  ))}
                </h1>
                <span className="flex items-center text-blue-800">{title} </span>
                {page !== null && totalPage !== null && (
                  <p
                    className={`text-sm text-gray-400 font-normal ${
                      document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "ml-2" : "mr-2"
                    }`}
                  >
                    ( Page : {page} / {totalPage} )
                  </p>
                )}
              </div>
              <Flex>
                {showPrev === true && (
                  <Button done={done} click={prevClick} icon={<FiChevronLeft />} title={language.PREVIOUS}></Button>
                )}
                {showNext === true && (
                  <Button
                    done={done}
                    click={nextClick}
                    iconPosition={"right"}
                    icon={<FiChevronRight />}
                    loading={btnLoading}
                    title={language.NEXT}
                  ></Button>
                )}
                {submitBtn === true && (
                  <Button
                    done={done}
                    loading={btnLoading}
                    click={() => submit()}
                    icon={<FiSave />}
                    title={language.SUBMIT}
                  ></Button>
                )}
                {saveBtn === true && (
                  <Button
                    done={done}
                    loading={btnLoading}
                    click={() => submit()}
                    icon={<FiSave />}
                    title={language.SAVE}
                  ></Button>
                )}
                {downloadBtn === true && (
                  <Button
                    done={done}
                    loading={btnLoading}
                    click={() => submit()}
                    icon={<FiSave />}
                    title={language.DOWNLOAD}
                  ></Button>
                )}
                {showVerify === true && (
                  <Button
                    done={done}
                    loading={btnLoading}
                    click={() => verifyClick()}
                    icon={<FiCheck />}
                    title={language.VERIFY_PROFESSIONAL}
                  ></Button>
                )}
                {editBtn === true && (
                  <Button
                    done={done}
                    loading={btnLoading}
                    click={() => editBtnClick()}
                    icon={<FiEdit />}
                    title={language.EDIT}
                  ></Button>
                )}
                {closeBtn === true && (
                  <Button
                    done={done}
                    click={() => closeClick()}
                    icon={<FiX />}
                    title={language.CLOSE}
                    data-title={language.CLOSE}
                  ></Button>
                )}
              </Flex>
            </h1>
          )}
          <div className={`flex ${model === false && "flex-col"} lg:flex-row`}>{children}</div>
        </FlexCol>
      </div>
    </>
  );
};
