// import React from "react";
// import {
//   FiArchive,
//   FiBell,
//   FiCheck,
//   FiChevronLeft,
//   FiChevronRight,
//   FiEdit,
//   FiEye,
//   FiHome,
//   FiLoader,
//   FiPlus,
//   FiRefreshCw,
//   FiSearch,
//   FiTrash,
//   FiUsers,
//   FiX,
// } from "react-icons/fi";

// import { DataTable } from "./DataTable";
// import { TableWrapper } from "./TableWrapper";
// import { TableWrapperLeft } from "./TableWrapperLeft";
// import { ColumnHeadings } from "./ColumnHeadings";
// import { TableWrapperRight } from "./TableWrapperRight";
// import { Filter } from "./Filter";

// import NavLinks from "../../utils/navLinks.json";

// import useLanguage from "../../hooks/useLanguage";
// import useSettings from "../../hooks/useSettings";
// import useFetchUsers from "../../hooks/api/useFetchUsers";
// import { Prompt } from "./Prompt";
// import usePrompt from "../../hooks/usePrompt";
// import { PopUp } from "./PopUp";
// import { NoData } from "./NoData";
// import { useHistory } from "react-router-dom";
// import axios from "axios";
// import useAdmin from "../../hooks/useAdmin";
// import { TextField } from "./TextField";
// import { CheckBoxWhite } from "./CheckBoxWhite";
// import { CheckBox } from "./CheckBox";
// import { ToggleButton } from "./ToggleButton";
// import useAlign from "../../hooks/useAlign";
// import { IoMdCash } from "react-icons/io";
// import { GoDeviceMobile } from "react-icons/go";
// import { CgNotes } from "react-icons/cg";
// import { AiOutlineCar, AiOutlineTranslation } from "react-icons/ai";
// import { BiRecycle } from "react-icons/bi";
// import { Waypoint } from "react-waypoint";

// export const Table = ({
//   history,
//   title,
//   startingHeadings = [],
//   startingFields = [],
//   list,
//   bulk = false,
//   bread = [{ id: 1, title: "", path: "" }],
//   active = false,
//   inactive = false,
//   archieve = false,
//   add = false,
//   showSearch = false,
//   action = false,
//   edit = false,
//   view = false,
//   translate = false,
//   notes = false,
//   mobile = false,
//   money = false,
//   vehicle = false,
//   verify = false,
//   notify = false,
//   addClick = () => {},
//   restoreClick = () => {},
//   deleteClick = () => {},
//   editClick = () => {},
//   viewClick = () => {},
//   translateClick = () => {},
//   archieveClick = () => {},
//   mobileClick = () => {},
//   notesClick = () => {},
//   moneyClick = () => {},
//   vehicleClick = () => {},
//   assignData = (data) => {},
// }) => {
//   const { prompt, showPrompt } = usePrompt();
//   const { language } = useLanguage();
//   const { header } = useAdmin();
//   const { settings, fetchSettings } = useSettings();
//   const { alignLeft, alignRight } = useAlign();
//   document.title = title;

//   // HOOKS

//   const [loading, setLoading] = React.useState(true);

//   const [search, setSearch] = React.useState("");
//   const [selected, setSelected] = React.useState([]);

//   const [pagination, setPagination] = React.useState({
//     skip: 0,
//     limit: settings.pageViewLimits[0],
//   });

//   const [showField, setShowField] = React.useState(language.ALL);

//   const [popup, setPopup] = React.useState(null);

//   const [total, setTotal] = React.useState(0);

//   const [headings, setHeadings] = React.useState(startingHeadings);
//   const [fields, setFields] = React.useState(startingFields);

//   const setBulkRestore = async () => {
//     // const { status, data } = await fetchAPIToggleStatus(selected, collection, 1);
//     // if (status === 200) {
//     //   removeFromDom();
//     //   setPopup({ title: language.RESTORED, type: "success" });
//     // } else {
//     //   setPopup({ title: data.data, type: "error" });
//     // }
//     setSelected([]);
//   };

//   const select = (e) =>
//     selected.filter((field) => field === e).length > 0
//       ? setSelected(selected.filter((field) => field != e))
//       : setSelected([...selected, e]);

//   const setDefault = async (id) => {
//     setLoading(true);
//     // await fetchAPILanguageSetDefault(id);
//     const { lang_direction, lang_code } = await fetchSettings();
//     lang_direction === 1 ? alignRight() : alignLeft();
//     // await fetchLanguage(lang_code);
//     // await fetchLanguageTable();
//   };

//   const setBulkActive = async () => {
//     // const { status, data } = await fetchAPIToggleStatus(selected, collection, 1);
//     // if (status === 200) {
//     //   if (showField === language.INACTIVE) {
//     //     removeFromDom();
//     //   } else {
//     //     setFields(
//     //       fields.map((field) =>
//     //         selected.filter((select) => select === field._id).length > 0 ? { ...field, status: 1 } : field
//     //       )
//     //     );
//     //   }
//     //   setPopup({ title: language.ACTIVATED, type: "success" });
//     // } else {
//     //   setPopup({ title: data.data, type: "error" });
//     // }
//     setSelected([]);
//   };

//   const setBulkFresh = async () => {
//     // const { status, data } = await fetchAPIToggleStatus(selected, collection, 1);
//     // if (status === 200) {
//     //   setFields(
//     //     fields.map((field) =>
//     //       selected.filter((select) => select === field._id).length > 0 ? { ...field, fresh: 1 } : field
//     //     )
//     //   );
//     //   setPopup({ title: language.ACTIVATED, type: "success" });
//     // } else {
//     //   setPopup({ title: data.data, type: "error" });
//     // }
//     setSelected([]);
//   };

//   const removeFromDom = () => {
//     setFields(
//       fields
//         .map((field) => (selected.filter((select) => select === field._id).length > 0 ? null : field))
//         .filter((field) => field != null)
//     );
//     setTotal(total - selected.length);
//     setPagination({
//       ...pagination,
//       skip: parseInt(pagination.skip) - parseInt(selected.length),
//     });
//   };

//   const setBulkInActive = async () => {
//     // const { status, data } = await fetchAPIToggleStatus(selected, collection, 2);
//     // if (status === 200) {
//     //   if (showField === language.ACTIVE) {
//     //     removeFromDom();
//     //   } else {
//     //     setFields(
//     //       fields.map((field) =>
//     //         selected.filter((select) => select === field._id).length > 0 ? { ...field, status: 0 } : field
//     //       )
//     //     );
//     //   }
//     //   setPopup({ title: language.INACTIVATED, type: "success" });
//     // } else setPopup({ title: data.data, type: "success" });
//     // setSelected([]);
//   };

//   const setBulkArchieve = async () => {
//     // const { status, data } = await fetchAPIToggleStatus(selected, collection, 0);
//     // if (status === 200) {
//     //   removeFromDom();
//     //   setSelected([]);
//     //   setPopup({ title: language.ARCHIEVE_SUCCESS, type: "success" });
//     // } else {
//     //   setPopup({ title: data.data, type: "error" });
//     // }
//   };

//   const setBulkDelete = async () => {
//     // const { status, data } = await fetchAPIDelete(selected, collection);
//     // if (status === 200) {
//     //   removeFromDom();
//     //   setSelected([]);
//     //   setPopup({ title: language.DELETE_SUCCESS, type: "success" });
//     // } else {
//     //   setPopup({ title: data.data, type: "error" });
//     // }
//   };

//   const toggleStatusField = async (id) => {
//     // setLoading(false);
//     // const singleData = fields.filter((field) => field._id === id)[0];
//     // const { status, data } = await fetchAPIToggleStatus([id], collection, singleData.status === 1 ? 2 : 1);
//     // if (status === 200)
//     //   setFields(fields.map((user) => (user._id === id ? { ...user, status: user.status === 1 ? 2 : 1 } : user)));
//   };

//   const deleteField = async (id) => {
//     // const { status, data } = await fetchAPIDelete([id], collection);
//     // if (status === 200) {
//     //   setFields(fields.filter((field) => field._id !== id));
//     //   setTotal(total - 1);
//     //   setPagination({
//     //     ...pagination,
//     //     skip: parseInt(pagination.skip) - 1,
//     //   });
//     //   setPopup({ title: language.DELETE_SUCCESS, type: "success" });
//     // } else {
//     //   setPopup({ title: data.data, type: "error" });
//     // }
//   };

//   const archieveField = async (id) => {
//     // const { status, data } = await fetchAPIToggleStatus([id], collection, 0);
//     // if (status === 200) {
//     //   setFields(fields.filter((field) => field._id !== id));
//     //   setTotal(total - 1);
//     //   setPagination({
//     //     ...pagination,
//     //     skip: parseInt(pagination.skip) - 1,
//     //   });
//     //   setPopup({ title: language.ARCHIEVE_SUCCESS, type: "success" });
//     // } else {
//     //   setPopup({ title: data.data, type: "error" });
//     // }
//   };

//   const restoreField = async (id) => {
//     // const { status, data } = await fetchAPIToggleStatus([id], collection, 1);
//     // if (status === 200) {
//     //   setFields(fields.filter((field) => field._id !== id));
//     //   setTotal(total - 1);
//     //   setPagination({
//     //     ...pagination,
//     //     skip: parseInt(pagination.skip) - 1,
//     //   });
//     //   setPopup({ title: language.RESTORED, type: "success" });
//     // } else {
//     //   setPopup({ title: data.data, type: "error" });
//     // }
//     // setSelected([]);
//   };

//   //   const { fetchUsersAll, fetchUsersArchieved } = useFetchUsers();

//   const bulkActive = () => showPrompt(language.ACTIVE, language.PROMPT_STATUS, "status", () => setBulkActive());
//   const bulkInActive = () => showPrompt(language.INACTIVE, language.PROMPT_STATUS, "status", () => setBulkInActive());
//   const bulkArchive = () => showPrompt(language.ARCHIVE, language.PROMPT_ARCHIEVE, "archieve", () => setBulkArchieve());
//   const bulkRestore = () => showPrompt(language.RESTORE, language.PROMPT_RESTORE, "archieve", () => setBulkRestore());
//   // const bulkDelete = () => showPrompt(language.DELETE, language.PROMPT_DELETE, "delete", () => setBulkDelete());

//   const fetchTable = async () => {
//     setLoading(true);
//     setPagination({ ...pagination, skip: 0 });
//     if (showField === language.ARCHIEVE) {
//       try {
//         const { data } = await axios.post(list, { skip: 0, limit: pagination.limit, search }, header);
//         setTotal(data.count);
//         setFields(assignData(data.response));
//       } catch (err) {}
//     } else if (showField === language.ACTIVE) {
//       try {
//         const { data } = await axios.post(list, { skip: 0, limit: pagination.limit, search }, header);
//         setTotal(data.count);
//         setFields(assignData(data.response));
//       } catch (err) {}
//     } else if (showField === language.INACTIVE) {
//       try {
//         const { data } = await axios.post(list, { skip: 0, limit: pagination.limit, search }, header);
//         setTotal(data.count);
//         setFields(assignData(data.response));
//       } catch (err) {}
//     } else {
//       try {
//         const { data } = await axios.post(list, { skip: 0, limit: pagination.limit, search }, header);
//         setTotal(data.count);
//         setFields(assignData(data.response));
//       } catch (err) {}
//     }
//     // const { status, data } =
//     //   archieved === true
//     //     ? await fetchUsersArchieved(0, pagination.limit, search)
//     //     : await fetchUsersAll(0, pagination.limit, search);
//     setLoading(false);
//   };

//   const clearSearch = async () => {
//     setLoading(true);
//     setPagination({ ...pagination, skip: 0 });
//     setSearch("");
//     try {
//       const { data } = await axios.post(list, { skip: 0, limit: pagination.limit, search: "" }, header);
//       setTotal(data.count);
//       setFields(assignData(data.response));
//     } catch (err) {
//       setPopup({ title: language.ERROR, type: "error" });
//     }
//     setLoading(false);
//   };

//   React.useEffect(() => {
//     setSelected([]);
//     fetchTable();
//     if (showField === language.ARCHIEVE || showField === language.ACTIVE || showField === language.INACTIVE) {
//       setHeadings(headings.filter((field) => field.key !== "status"));
//     }
//     if (showField === language.ALL) {
//       setHeadings(
//         headings.filter((field) => field.key === "status").length > 0
//           ? headings
//           : [
//               ...headings,
//               {
//                 id: 6,
//                 title: language.STATUS,
//                 key: "status",
//                 show: false,
//               },
//             ]
//       );
//     }
//   }, [pagination.limit, showField]);

//   const loadMore = async () => {
//     if (showField === language.Common.ARCHIVE) {
//       const { status, data } = await fetchUsersArchieved(
//         parseInt(pagination.skip) + parseInt(pagination.limit),
//         pagination.limit,
//         search
//       );
//       if (status === 200) {
//         setFields([...fields, ...data.list]);
//       }
//     } else if (showField === language.Common.ACTIVE) {
//       const { status, data } = await fetchUsersAll(
//         parseInt(pagination.skip) + parseInt(pagination.limit),
//         pagination.limit,
//         search,
//         1
//       );
//       if (status === 200) {
//         setFields([...fields, ...data.list]);
//       }
//     } else if (showField === language.Common.INACTIVE) {
//       const { status, data } = await fetchUsersAll(
//         parseInt(pagination.skip) + parseInt(pagination.limit),
//         pagination.limit,
//         search,
//         2
//       );
//       if (status === 200) {
//         setFields([...fields, ...data.list]);
//       }
//     } else {
//       const { status, data } = await fetchUsersAll(
//         parseInt(pagination.skip) + parseInt(pagination.limit),
//         pagination.limit,
//         search
//       );
//       if (status === 200) {
//         setFields([...fields, ...data.list]);
//       }
//     }
//     setPagination({
//       ...pagination,
//       skip: parseInt(pagination.skip) + parseInt(pagination.limit),
//     });
//   };

//   // const loadMore = async () => {
//   //   if (showField === language.ARCHIEVE) {
//   //     try {
//   //       const { data } = await axios.post(list, { skip: pagination.skip, limit: pagination.limit, search }, header);
//   //       setTotal(data.count);
//   //       setFields([...fields, ...assignData(data.response)]);
//   //     } catch (err) {}
//   //   } else if (showField === language.ACTIVE) {
//   //     try {
//   //       const { data } = await axios.post(list, { skip: pagination.skip, limit: pagination.limit, search }, header);
//   //       setTotal(data.count);
//   //       setFields([...fields, ...assignData(data.response)]);
//   //     } catch (err) {}
//   //   } else if (showField === language.INACTIVE) {
//   //     try {
//   //       const { data } = await axios.post(list, { skip: pagination.skip, limit: pagination.limit, search }, header);
//   //       setTotal(data.count);
//   //       setFields([...fields, ...assignData(data.response)]);
//   //     } catch (err) {}
//   //   } else {
//   //     try {
//   //       const { data } = await axios.post(list, { skip: pagination.skip, limit: pagination.limit, search }, header);
//   //       setTotal(data.count);
//   //       setFields([...fields, ...assignData(data.response)]);
//   //     } catch (err) {}
//   //   }
//   //   setPagination({
//   //     ...pagination,
//   //     skip: parseInt(pagination.skip) + parseInt(pagination.limit),
//   //   });
//   // };

//   return (
//     //   TableWrapper
//     <div
//       className="flex"
//       style={{
//         height: "90vh",
//       }}
//     >
//       {popup != null && <PopUp unmount={() => setPopup(null)} title={popup.title} type={popup.type} />}

//       {prompt.show === true && (
//         <Prompt
//           title={prompt.title}
//           message={prompt.message}
//           type={prompt.type}
//           close={prompt.close}
//           click={prompt.click}
//         />
//       )}
//       <div
//         className={`hidden lg:block w-2/12 border-r-2 dark:border-gray-700 p-4 text-sm text-gray-800 overflow-y-scroll`}
//       >
//         {/* Table Left  */}
//         <div className="flex flex-col h-full justify-between">
//           <div className="p-3 flex flex-col">
//             <ColumnHeadings headings={headings} change={(e) => setHeadings(e)}></ColumnHeadings>
//             {/* <Filter
//     data-title={language.PAGE_LIMIT}
//     change={(e) => setPagination({ ...pagination, limit: e })}
//     fields={settings.pageViewLimits}
//   />
//   <Filter
//     data-title={language.VIEW}
//     change={(e) => setShowField(e)}
//     fields={[language.ALL, language.ACTIVE, language.INACTIVE, language.ARCHIEVE]}
//   /> */}
//           </div>
//         </div>
//       </div>
//       {/* TableRight  */}
//       <div className={`w-full lg:w-10/12 h-full p-4 dark:bg-gray-800 overflow-y-hidden tablewrapper`}>
//         <div className="flex justify-between items-center">
//           <div className="text-base md:text-lg lg:text-xl flex items-center truncate">
//             <div
//               onClick={() => history.push(NavLinks.ADMIN_HOME)}
//               className={
//                 document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
//                   ? "text-green-800 dark:text-gray-200 flex items-center cursor-pointer hover:text-blue-800"
//                   : "text-green-800 dark:text-gray-200 flex items-center cursor-pointer hover:text-blue-800"
//               }
//             >
//               {<FiHome />}
//               {document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? (
//                 <FiChevronRight />
//               ) : (
//                 <FiChevronLeft />
//               )}
//             </div>
//             <h1 className="flex items-center">
//               {bread.map((el) => (
//                 <span
//                   onClick={() => history.push(el.path)}
//                   className="flex items-center text-gray-500 cursor-pointer hover:underline text-green-800 dark:text-gray-200"
//                 >
//                   {el.title}{" "}
//                   {document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? (
//                     <FiChevronRight />
//                   ) : (
//                     <FiChevronLeft />
//                   )}
//                 </span>
//               ))}
//               <span className="flex items-center text-blue-800">{showField + " " + title} </span>
//               {total !== 0 && (
//                 <span className="text-gray-400 mx-2" style={{ fontSize: 14 }}>
//                   ({total})
//                 </span>
//               )}
//             </h1>
//           </div>
//           <div className="flex items-center">
//             {selected.length > 0 && bulk === true && (
//               <div
//                 className={`relative flex bg-gray-200 dark:bg-gray-900 p-2 rounded-full ${
//                   document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
//                 }`}
//               >
//                 {showField === language.ARCHIVE && (
//                   <>
//                     {" "}
//                     {active === true && (
//                       <button
//                         onClick={bulkActive}
//                         data-title={language.ACTIVE}
//                         className={`bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-1 px-3 ${
//                           document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
//                         }`}
//                       >
//                         <p style={{ fontSize: 13 }}>{language.ACTIVE}</p>
//                       </button>
//                     )}
//                     {inactive === true && (
//                       <button
//                         onClick={bulkInActive}
//                         data-title={language.INACTIVE}
//                         className={`bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-1 px-3 ${
//                           document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
//                         }`}
//                       >
//                         <p style={{ fontSize: 13 }}>{language.INACTIVE}</p>
//                       </button>
//                     )}
//                     {/* {showBulkAttend === true && (
//                       <button
//                         onClick={bulkAttend}
//                         data-title={language.ATTENDED}
//                         className={`bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-1 px-3 ${
//                           document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
//                         }`}
//                       >
//                         <p style={{ fontSize: 13 }}>{language.ATTENDED}</p>
//                       </button>
//                     )} */}
//                     {archieve === true && (
//                       <button
//                         onClick={bulkArchive}
//                         data-title={language.ARCHIVE}
//                         className={`bg-purple-500 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-1 px-3 ${
//                           document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
//                         }`}
//                       >
//                         <p style={{ fontSize: 13 }}>{language.ARCHIVE}</p>
//                       </button>
//                     )}
//                   </>
//                 )}
//                 {showField === language.ARCHIVE
//                   ? false
//                   : true && (
//                       <button
//                         onClick={bulkRestore}
//                         data-title={language.RESTORE}
//                         className={`bg-purple-500 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-1 px-3 ${
//                           document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
//                         }`}
//                       >
//                         <p style={{ fontSize: 13 }}>{language.RESTORE}</p>
//                       </button>
//                     )}
//                 {/* {showBulkDelete === true && (
//                   <button
//                     onClick={bulkDelete}
//                     data-title={language.DELETE}
//                     className={`bg-red-500 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-1 px-3 ${
//                       document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
//                     }`}
//                   >
//                     <p style={{ fontSize: 13 }}>{language.DELETE}</p>
//                   </button>
//                 )} */}
//               </div>
//             )}
//             <button
//               onClick={() => fetchTable()}
//               data-title={language.REFRESH}
//               className={`${
//                 loading === true && "animate-spin"
//               } bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                 document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
//               }`}
//             >
//               <FiRefreshCw />
//             </button>
//             {add === true && (
//               <button
//                 onClick={addClick}
//                 data-title={language.ADD}
//                 className={`bg-blue-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                   document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"
//                 }`}
//               >
//                 <FiPlus />
//               </button>
//             )}
//             {showSearch === true && (
//               <>
//                 <TextField
//                   padding={false}
//                   onKeyDown={(e) => {
//                     if (e.key === "Enter") {
//                       if (search.length > 0) fetchTable();
//                     }
//                   }}
//                   iconSelect={() => {
//                     if (search.length > 0) fetchTable();
//                   }}
//                   change={(e) => setSearch(e)}
//                   icon={<FiSearch />}
//                   placeholder={language.SEARCH}
//                   value={search}
//                 />
//                 {search != "" && (
//                   <button
//                     onClick={clearSearch}
//                     data-title={language.NO}
//                     className={`bg-red-500 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                       document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "ml-2" : "mr-2"
//                     }`}
//                   >
//                     <FiX />
//                   </button>
//                 )}
//               </>
//             )}
//           </div>
//         </div>
//         {/* Table  */}
//         <div className="flex">
//           {loading === false &&
//             (fields.length > 0 ? (
//               // Table
//               <div className="w-full">
//                 <div className="w-full shadow-md mt-4 rounded-lg overflow-y-scroll" style={{ maxHeight: "79vh" }}>
//                   <table
//                     cellPadding="12"
//                     className="w-full relative text-gray-800 text-left dark:bg-gray-800 z-10 dataTable"
//                     style={{ fontSize: 13 }}
//                   >
//                     <thead>
//                       {bulk === true && (
//                         <th className="sticky top-0 bg-green-800 text-white">
//                           <CheckBoxWhite
//                             onClick={
//                               selected.length === fields.length
//                                 ? () => setSelected([])
//                                 : () => setSelected(fields.map((field) => field._id))
//                             }
//                             defaultValue={selected.length === fields.length ? 1 : 0}
//                           />
//                         </th>
//                       )}
//                       <th className="text-center sticky top-0 bg-blue-800 text-white">{language.TABLE_NO}</th>
//                       {headings
//                         .filter((field) => field.show === true)
//                         .map((field) => (
//                           <th key={field.id} className="sticky top-0 bg-blue-800 text-white">
//                             {field.title}
//                           </th>
//                         ))}
//                       {action === true && (
//                         <th className="text-center sticky top-0 bg-blue-800 text-white">{language.ACTION}</th>
//                       )}
//                     </thead>
//                     <tbody className="text-sm text-gray-600 dark:bg-gray-800">
//                       {fields.map((field, index) => (
//                         <tr
//                           key={field._id}
//                           className="bg-white dark:bg-gray-800 dark:text-gray-200 dark:border-gray-900 hover:bg-gray-100 cursor-pointer border-b-4 border-gray-100"
//                         >
//                           {bulk === true && (
//                             <td onClick={() => select(field._id)}>
//                               <CheckBox
//                                 defaultValue={selected.filter((select) => select === field._id).length > 0 ? 1 : 0}
//                               />
//                             </td>
//                           )}
//                           <td onClick={() => select(field._id)} className="text-center">
//                             {index + 1}
//                           </td>
//                           {headings.map((head) => {
//                             if (head.show === true) {
//                               if (head.key === "status") {
//                                 if (field.default_status === 1)
//                                   return (
//                                     <td className="opacity-25">
//                                       <ToggleButton defaultValue={field[head.key]} />
//                                     </td>
//                                   );
//                                 else
//                                   return (
//                                     <td
//                                       onClick={(e) =>
//                                         showPrompt(language.STATUS, language.PROMPT_STATUS, "status", () =>
//                                           toggleStatusField(field._id)
//                                         )
//                                       }
//                                     >
//                                       <ToggleButton defaultValue={field[head.key]} />
//                                     </td>
//                                   );
//                               } else if (head.key === "default_status") {
//                                 if (field.status === 0)
//                                   return (
//                                     <td className="opacity-25">
//                                       <ToggleButton defaultValue={field[head.key]} />
//                                     </td>
//                                   );
//                                 else
//                                   return (
//                                     <td
//                                       onClick={() =>
//                                         showPrompt(
//                                           fields.filter((eachField) => eachField._id === field._id)[0].lang_name,
//                                           language.PROMPT_DEFAULT_LANGUAGE,
//                                           "add",
//                                           () => setDefault(field._id)
//                                         )
//                                       }
//                                     >
//                                       <ToggleButton defaultValue={field[head.key]} />
//                                     </td>
//                                   );
//                               } else if (head.key === "payment_type") {
//                                 if (field.payment_type == 1)
//                                   return (
//                                     <td>
//                                       <button
//                                         onClick={() => editClick(field._id)}
//                                         data-title={language.EDIT}
//                                         className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                                           document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
//                                             ? "mr-2"
//                                             : "ml-2"
//                                         }`}
//                                       >
//                                         <FiEdit />
//                                       </button>
//                                     </td>
//                                   );
//                                 else return <td></td>;
//                               } else if (head.key === "driver_documents")
//                                 return (
//                                   <td>
//                                     {field.driver_documents.map((document) =>
//                                       document.isExpired === 2 ? (
//                                         <p className="text-red-500">{document.docs_name}</p>
//                                       ) : (
//                                         <p>{document.docs_name}</p>
//                                       )
//                                     )}
//                                   </td>
//                                 );
//                               else if (head.key === "vehicle_documents") return <p>Vehicle Docments</p>;
//                               else return <td onClick={() => select(field._id)}>{field[head.key]}</td>;
//                             }
//                           })}
//                           {action === true && (
//                             <td className="w-28">
//                               <div className="flex items-center justify-center">
//                                 {view === true && (
//                                   <button
//                                     onClick={() => viewClick(field._id)}
//                                     data-title={language.VIEW}
//                                     className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                                       document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
//                                         ? "mr-2"
//                                         : "ml-2"
//                                     }`}
//                                   >
//                                     <FiEye />
//                                   </button>
//                                 )}
//                                 {money === true && (
//                                   <button
//                                     onClick={() => moneyClick(field._id)}
//                                     data-title={language.MOBILE}
//                                     className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                                       document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
//                                         ? "mr-2"
//                                         : "ml-2"
//                                     }`}
//                                   >
//                                     <IoMdCash />
//                                   </button>
//                                 )}
//                                 {mobile === true && (
//                                   <button
//                                     onClick={() => mobileClick(field._id)}
//                                     data-title={language.MOBILE}
//                                     className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                                       document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
//                                         ? "mr-2"
//                                         : "ml-2"
//                                     }`}
//                                   >
//                                     <GoDeviceMobile />
//                                   </button>
//                                 )}
//                                 {notes === true && (
//                                   <button
//                                     onClick={() => notesClick(field._id)}
//                                     data-title={language.NOTES}
//                                     className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                                       document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
//                                         ? "mr-2"
//                                         : "ml-2"
//                                     }`}
//                                   >
//                                     <CgNotes />
//                                   </button>
//                                 )}
//                                 {edit === true && (
//                                   <button
//                                     onClick={() => editClick(field._id)}
//                                     data-title={language.EDIT}
//                                     className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                                       document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
//                                         ? "mr-2"
//                                         : "ml-2"
//                                     }`}
//                                   >
//                                     <FiEdit />
//                                   </button>
//                                 )}
//                                 {vehicle === true && (
//                                   <button
//                                     onClick={() => vehicleClick(field._id)}
//                                     data-title={language.Drivers.VEHICLE}
//                                     className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                                       document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
//                                         ? "mr-2"
//                                         : "ml-2"
//                                     }`}
//                                   >
//                                     <AiOutlineCar />
//                                   </button>
//                                 )}
//                                 {translate === true && (
//                                   <button
//                                     onClick={() => translateClick(field._id)}
//                                     data-title={language.TRANSLATE}
//                                     className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                                       document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
//                                         ? "mr-2"
//                                         : "ml-2"
//                                     }`}
//                                   >
//                                     <AiOutlineTranslation />
//                                   </button>
//                                 )}
//                                 {showField === language.ARCHIEVE && (
//                                   <button
//                                     onClick={() => restoreClick(field._id)}
//                                     data-title={language.RESTORE}
//                                     className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                                       document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
//                                         ? "mr-2"
//                                         : "ml-2"
//                                     }`}
//                                   >
//                                     <BiRecycle />
//                                   </button>
//                                 )}

//                                 {showField === language.ARCHIEVE
//                                   ? false
//                                   : true && (
//                                       <button
//                                         onClick={() => archieveClick(field._id)}
//                                         data-title={language.ARCHIEVE}
//                                         className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                                           document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
//                                             ? "mr-2"
//                                             : "ml-2"
//                                         }`}
//                                       >
//                                         <FiArchive />
//                                       </button>
//                                     )}
//                                 {field.default_status !== 1 && false === true && (
//                                   <button
//                                     onClick={() => deleteClick(field._id)}
//                                     data-title={language.DELETE}
//                                     className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                                       document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
//                                         ? "mr-2"
//                                         : "ml-2"
//                                     }`}
//                                   >
//                                     <FiTrash />
//                                   </button>
//                                 )}
//                                 {verify === true && (
//                                   <button
//                                     onClick={() => deleteClick(field._id)}
//                                     data-title={language.DELETE}
//                                     className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                                       document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
//                                         ? "mr-2"
//                                         : "ml-2"
//                                     }`}
//                                     // style={{ backgroundColor: "#6EBC41" }}
//                                   >
//                                     <FiCheck />
//                                   </button>
//                                 )}
//                                 {notify === true && (
//                                   <button
//                                     onClick={() => deleteClick(field._id)}
//                                     data-title={language.DELETE}
//                                     className={`bg-green-800 cursor-pointer focus:outline-none rounded-full hover:bg-green-800 text-white p-2 ${
//                                       document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
//                                         ? "mr-2"
//                                         : "ml-2"
//                                     }`}
//                                   >
//                                     <FiBell />
//                                   </button>
//                                 )}
//                               </div>
//                             </td>
//                           )}
//                         </tr>
//                       ))}
//                       {fields.length != total && total != 0 && (
//                         <Waypoint onEnter={loadMore}>
//                           <tr>
//                             <td colSpan={fields.length + 2} align="center">
//                               <FiLoader className="animate-spin" />
//                             </td>
//                           </tr>
//                         </Waypoint>
//                       )}
//                     </tbody>
//                   </table>
//                 </div>
//               </div>
//             ) : (
//               //   <DataTable
//               //     bulkSelect={bulk}
//               //     selected={selected}
//               //     selectAll={() => setSelected(fields.map((field) => field._id))}
//               //     deselectAll={() => setSelected([])}
//               //     select={(e) =>
//               //       selected.filter((field) => field === e).length > 0
//               //         ? setSelected(selected.filter((field) => field != e))
//               //         : setSelected([...selected, e])
//               //     }
//               //     loadMore={loadMore}
//               //     total={total}
//               //     fields={fields}
//               //     archieveClick={(e) =>
//               //       showPrompt(language.ARCHIEVE, language.PROMPT_ARCHIEVE, "status", () => archieveField(e))
//               //     }
//               //     changeStatus={(e) =>
//               //       showPrompt(language.STATUS, language.PROMPT_STATUS, "status", () => toggleStatusField(e))
//               //     }
//               //     deleteClick={(e) => showPrompt(language.DELETE, language.PROMPT_DELETE, "delete", () => deleteField(e))}
//               //     showArchieve={showField === language.ARCHIEVE ? false : true}
//               //     editClick={(e) => history.push(NavLinks.ADMIN_USERS_EDIT + "/" + e)}
//               //     restoreClick={(e) =>
//               //       showPrompt(language.RESTORE, language.PROMPT_RESTORE, "delete", () => restoreField(e))
//               //     }
//               //     showView={false}
//               //     showUnarchieve={showField === language.ARCHIEVE}
//               //     showEdit={showField === language.ARCHIEVE ? false : true}
//               //     headings={headings}
//               //   />
//               // )

//               <NoData></NoData>
//             ))}
//         </div>
//       </div>
//     </div>
//   );
// };
