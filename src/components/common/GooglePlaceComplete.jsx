import React from "react";
import PlacesAutocomplete from "react-places-autocomplete";
import axios from "axios";
import useSettings from "../../hooks/useSettings";

export default function GooglePlaceComplete({
  width = "full",
  error = "",
  type = "text",
  required = true,
  icon = "",
  hint = "",
  iconSelect = () => {},
  change = () => {},
  clearText = false,
  placeholder = "",
  className = "",
  padding = true,
  margin = 0,
  bounds = false,
  defaultValue = "",
  ...rest
}) {
  const { settings } = useSettings();

  const direction = document.getElementsByTagName("html")[0].getAttribute("dir");

  const [address, setAddress] = React.useState(defaultValue);

  const [googleLoaded, setGoogleLoaded] = React.useState(false);
  const [currentLocation, setCurrentLocation] = React.useState(null);

  React.useEffect(() => {
    if (clearText === true) setAddress("");
  }, [clearText]);

  const placeSelect = async (e, placeId, suggestion) => {
    console.log(suggestion);
    setAddress(
      suggestion.formattedSuggestion.mainText + ", " + suggestion.formattedSuggestion.secondaryText ||
        suggestion.description
    );
    const { status, data } = await axios.get(
      `https://maps.googleapis.com/maps/api/geocode/json?place_id=${suggestion.placeId}&key=${settings.mapApi.web}`
    );
    if (status === 200) change({ ...data, selectedAddress: e, placeId });
  };

  React.useEffect(() => {
    navigator.geolocation.getCurrentPosition((e) => {
      setCurrentLocation({ lat: e.coords.latitude, lng: e.coords.longitude });
    });
  }, []);

  return (
    <div
      className={`relative w-${width} ${
        document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? `mr-${margin}` : `ml-${margin}`
      }`}
    >
      <PlacesAutocomplete
        value={address}
        onSelect={placeSelect}
        highlightFirstSuggestion={true}
        searchOptions={
          bounds
            ? {
                bounds: {
                  north: bounds.lat + 0.1,
                  south: bounds.lat - 0.1,
                  east: bounds.lng + 0.1,
                  west: bounds.lng - 0.1,
                },
              }
            : currentLocation !== null
            ? {
                bounds: {
                  north: currentLocation.lat + 0.1,
                  south: currentLocation.lat - 0.1,
                  east: currentLocation.lng + 0.1,
                  west: currentLocation.lng - 0.1,
                },
              }
            : false
        }
        shouldFetchSuggestions={address.length > 0}
        onChange={(e) => setAddress(e)}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps }) => (
          <div>
            <input
              placeholder={placeholder}
              {...getInputProps({
                className:
                  className +
                  ` w-full dark:bg-gray-900 text-sm focus:outline-none dark:focus:border-blue-800 bg-gray-100 rounded-xl px-4 py-3 border-b-2 border-gray-100 dark:border-gray-800 focus:border-blue-800 ${
                    error != "" && "border-2 border-red-600"
                  }`,
              })}
              {...rest}
            />
            <div
              className="fixed mt-2 w-64 flex flex-col z-20 rounded-lg max-h-44 shadow-xl overflow-y-scroll"
              style={{ zIndex: 100 }}
            >
              {suggestions.map((suggestion) => {
                const className = suggestion.active
                  ? "border-b-1 bg-gray-100 dark:bg-gray-800 border-gray-200 cursor-pointer flex items-center bg-gray-50 px-2 py-2 text-sm hover:bg-gray-100"
                  : "border-b-1 dark:bg-gray-900 border-gray-200 cursor-pointer flex items-center bg-gray-50 px-2 py-2 text-sm";
                return (
                  <div
                    {...getSuggestionItemProps(suggestion, {
                      className,
                    })}
                  >
                    <span>
                      {suggestion.formattedSuggestion.mainText},{" "}
                      {suggestion.formattedSuggestion.secondaryText || suggestion.description}
                    </span>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
    </div>
  );
}
