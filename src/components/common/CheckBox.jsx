import React from "react";
import { MdCheckBox, MdCheckBoxOutlineBlank } from "react-icons/md";


export const CheckBox = ({
  defaultValue = 0,
  change = () => { },
  className,
  ...rest
}) => {
  const toggle = defaultValue;
  const toggleData = (e) => {
    change(e);
  };

  return toggle == 1 ? (
    <MdCheckBox
      onClick={() => toggleData(0)}
      className={`cursor-pointer text-lg text-blue-800 ${className}`}
      {...rest} />
  ) : (
      <MdCheckBoxOutlineBlank
        onClick={() => toggleData(1)}
        className={`cursor-pointer text-lg text-gray-400 ${className}`}
        {...rest} />
    );
};
