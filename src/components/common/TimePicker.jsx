import "../../styles/rc-time-picker.css";

import { format } from "date-fns";
import React from "react";
import { FiClock } from "react-icons/fi";
import Picker from "rc-time-picker";

export const TimePicker = ({
  width = "full",
  error = "",
  type = "time",
  required = true,
  icon = "",
  hint = "",
  iconSelect = () => {},
  change = () => {},
  placeholder = "",
  className = "",
  padding = true,
  margin = 0,
  defaultValue,
  ...rest
}) => {
  const [open, setOpen] = React.useState(false);
  const [time, setTime] = React.useState(
    defaultValue ? format(new Date(defaultValue), "hh : mm a") : format(Date.now(), "hh : mm a")
  );
  return (
    <div
      className={`relative w-${width} ${
        document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr"
          ? `mr-${margin}`
          : `ml-${margin}`
      }`}
    >
      <Picker
        onChange={(e) => {
          setTime(format(new Date(e), "hh : mm a"));
          change(e.toISOString());
        }}
        open={open}
        onClose={() => setOpen(false)}
        showSecond={false}
        use12Hours
      />
      <p
        onClick={() => setOpen(!open)}
        className={
          className +
          ` absolute top-0 flex left-0 w-full dark:bg-gray-900 text-sm focus:outline-none bg-gray-100 rounded-xl px-4 py-3 ${
            error != "" && "border-2 border-red-600"
          }`
        }
      >
        {time}
      </p>
      {hint && error === "" && (
        <div className="flex">
          <div className="mx-2">
            {hint && (
              <p className="text-gray-300 dark:text-gray-500 text-sm mt-1" style={{ fontSize: 11 }}>
                {hint}
              </p>
            )}
          </div>
        </div>
      )}
      {error != "" && (
        <div className="flex">
          <div className="mx-2">
            {error && (
              <p className="text-red-600 dark:text-red-500 text-sm mt-1" style={{ fontSize: 11 }}>
                {error}
              </p>
            )}
          </div>
        </div>
      )}
    </div>
  );
};
