import React from "react";
import { ToggleButton } from "./ToggleButton";
import gsap from "gsap";
import useLanguage from "../../hooks/useLanguage";

export const ColumnHeadings = ({ title = "Columns", headings, animate = "", change = () => {} }) => {
  const { language } = useLanguage();
  React.useEffect(() => {
    gsap.fromTo(".columnHeadings", 0.5, { opacity: 0 }, { opacity: 1, stagger: 0.1 });
  }, []);
  return (
    <div className={`mb-3 opacity-0 columnHeadings`}>
      <h1 className="font-bold text-md mb-2 text-gray-500 opacity-0 columnHeadings">
        {language.COLUMNS_FILTER ? language.COLUMNS_FILTER : "Column Headings"}
      </h1>
      <div className="opacity-0 columnHeadings">
        {headings.map((eachHeading) => (
          <div
            key={eachHeading.id}
            onClick={() =>
              change(headings.map((head) => (head.id === eachHeading.id ? { ...head, show: !head.show } : head)))
            }
            className={`flex items-center mb-1 cursor-pointer`}
          >
            <div className={document.getElementsByTagName("html")[0].getAttribute("dir") === "ltr" ? "mr-2" : "ml-2"}>
              <ToggleButton defaultValue={eachHeading.show === true ? 1 : 0} />
            </div>

            <p className="dark:text-gray-400">{eachHeading.title}</p>
          </div>
        ))}
      </div>
    </div>
  );
};
