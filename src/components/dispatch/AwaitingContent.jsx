import React from "react";
import { AiOutlineCar } from "react-icons/ai";
import { FiCheck, FiClock, FiRefreshCcw, FiUserPlus, FiX } from "react-icons/fi";
import axios from "axios";

import A from "../../utils/API.js";
import U from "../../utils/utils.js";

import useAdmin from "../../hooks/useAdmin";
import useSocket from "../../hooks/useSocket";
import useSettings from "../../hooks/useSettings";
import useLanguage from "../../hooks/useLanguage";
import { format } from "date-fns";

export default function AwaitingContent({ id = "", setManualAssign = () => {}, completed = () => {} }) {
  const { authFailure, header, token } = useAdmin();
  const { language } = useLanguage();
  const [loading, setLoading] = React.useState(true);
  const [ride, setRide] = React.useState(null);
  const { notificationSocket } = useSocket();
  const { settings } = useSettings();
  const [completeStatus, setCompleteStatus] = React.useState(false);
  const [expired, setExpired] = React.useState(false);
  const [cancelled, setCancelled] = React.useState(false);
  const [noProfessional, setNoProfessional] = React.useState(false);

  const extraTime = U.RIDE_RETRY_WAIT;
  const [timer, setTimer] = React.useState(
    (parseInt(settings.bookingRetryCount) + 1) * (parseInt(settings.driverRequestTimeout) * 1000 + extraTime)
  );

  const checkBookingStatus = React.useRef();
  const retryInterval = React.useRef();
  const progressBarInterval = React.useRef();
  const [bookingAccepted, setBookingAccepted] = React.useState(false);
  const [type, setType] = React.useState("TIMER");

  const cancelRide = async () => {
    setNoProfessional(false);
    try {
      await axios.post(
        A.HOST + A.ADMIN_BOOK_EXPIRY,
        {
          bookingId: id,
          denyType: "USERCANCELLED",
        },
        header
      );
      // completed(id);
      setCancelled(true);
      clearInterval(retryInterval.current);
      clearInterval(progressBarInterval.current);
      //   setBarWidth(0);
    } catch (err) {
      authFailure(err);
    }
  };

  const fetchDetails = async (retry = false) => {
    setNoProfessional(false);
    try {
      setType("TIMER");

      const { data } = await axios.post(A.HOST + A.ADMIN_RIDES_READ, { bookingId: id }, header);
      setRide(data);

      if (data.bookingType === U.INSTANT && data.bookingStatus === U.AWAITING) {
        if (retry) {
          try {
            const result = await axios.post(
              A.HOST + A.ADMIN_BOOK_RETRY,
              {
                bookingId: id,
              },
              header
            );
            console.log(result);
            if (result?.data?.data?.bookingStatus !== "AWAITING") {
              setCompleteStatus(true);
              console.log(data);
              clearInterval(retryInterval.current);
              clearInterval(progressBarInterval.current);
            }
          } catch (err) {
            clearInterval(retryInterval.current);
            clearInterval(progressBarInterval.current);
            //   setPage(2);
            authFailure(err);
          }
        }
        // setPage(1);
        let initialCount = 0;
        const timeWait = +settings.driverRequestTimeout * 1000 + extraTime;

        const proBarInterval = setInterval(() => {
          // setBarWidth((barW) => barW + 1000);
          setTimer((timer) => timer - 1000);
        }, 1000);

        progressBarInterval.current = proBarInterval;
        setTimer(
          (parseInt(settings.bookingRetryCount) + 1) * (parseInt(settings.driverRequestTimeout) * 1000 + extraTime)
        );

        // RETRY INTERVAL
        const reInterval = setInterval(async () => {
          const retry = +settings.bookingRetryCount;
          if (initialCount === retry) {
            clearInterval(retryInterval.current);
            clearInterval(progressBarInterval.current);
            // setBarWidth(0);
            if (bookingAccepted === false) {
              //   setPage(2);
              setType("EXPIRED");
              //   try {
              //     // await axios.post(
              //     //   A.HOST + A.ADMIN_BOOK_EXPIRY,
              //     //   {
              //     //     bookingId: id,
              //     //     denyType: "EXPIRED",
              //     //   },
              //     //   header
              //     // );
              //   } catch (err) {
              //     authFailure(err);
              //     alert(err);
              //   }
            }
          } else {
            try {
              const result = await axios.post(
                A.HOST + A.ADMIN_BOOK_RETRY,
                {
                  bookingId: id,
                },
                header
              );
              if (result?.data?.data?.bookingStatus !== "AWAITING") {
                setCompleteStatus(true);
                clearInterval(retryInterval.current);
                clearInterval(progressBarInterval.current);
              }
            } catch (err) {
              clearInterval(retryInterval.current);
              clearInterval(progressBarInterval.current);
              if (err.response.data.message === "NO PROFESSIONAL FOUND") setNoProfessional(true);
              setType("EXPIRED");
              //   setPage(2);
              authFailure(err);
            }
            initialCount = initialCount + 1;
          }
        }, timeWait);
        retryInterval.current = reInterval;
        // setTimeout(() => {
        //   clearInterval(retryInterval.current);
        //   clearInterval(progressBarInterval.current);
        //   setBarWidth(0);
        // }, timeWait * +settings.bookingRetryCount);
      } else if (data.bookingType === U.SCHEDULE) console.log("");
      //   setPage(4);
      else if (data.bookingType === U.INSTANT && data.bookingStatus !== U.AWAITING) console.log("");
      //   setPage(4);
      setLoading(false);
    } catch (err) {
      authFailure(err);
    }
  };

  React.useEffect(() => fetchDetails(), []);

  React.useEffect(() => {
    setNoProfessional(false);
    checkBookingStatus.current = setInterval(async () => {
      try {
        const { data } = await axios.post(A.HOST + A.ADMIN_RIDES_READ, { bookingId: id }, header);
        if (data.bookingStatus !== "AWAITING") {
          // completed(id);
          setCompleteStatus(true);
          clearInterval(retryInterval.current);
          clearInterval(progressBarInterval.current);
          clearInterval(checkBookingStatus.current);
        }
      } catch (err) {
        authFailure(err);
        completed(id);
        clearInterval(retryInterval.current);
        clearInterval(progressBarInterval.current);
        clearInterval(checkBookingStatus.current);
      }
    }, 3000);
    // notificationSocket.current.on(token, (data) => {
    //   if (data.action === "ACCEPTBOOKING" && data.details._id === id) {
    //     // console.log(data);
    //     // completed(id);
    //     setCompleteStatus(true);
    //     // setBarWidth(0);
    //     // setBookingAccepted(true);
    //     // setPage(4);
    //     // clearInterval(retryInterval.current);
    //     // clearInterval(progressBarInterval.current);
    //     // setBarWidth(0);
    //   }
    // });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return ride === null ? null : (
    <div className="p-3 border-b-2 bg-gray-100 text-black flex justify-between items-center">
      <div className="flex items-center">
        <AiOutlineCar className="text-xl" />
        <div className="border-l-2 px-2 mx-3 flex flex-col">
          <h1 className="font-bold">{ride.bookingId || ""}</h1>
          <h1 className="text-sm text-gray-500">
            {ride.bookingFor?.phoneCode} {ride.bookingFor?.phoneNumber}
          </h1>
          <h1 className="text-sm text-gray-500">
            {ride.bookingFor?.name === "GUEST" ? language.GUEST : ride.bookingFor?.name}
          </h1>
        </div>
        {completeStatus === false && cancelled === false && noProfessional === false && (
          <>
            <FiClock className="text-xl" />
            <div className="border-l-2 px-2 mx-3 flex flex-col">
              <h1 className="font-bold">{format(new Date(ride.activity?.bookingTime), "p")}</h1>
              <h1 className="text-sm text-gray-500">{format(new Date(ride.activity?.bookingTime), "PPP")}</h1>
            </div>
          </>
        )}
      </div>
      {completeStatus === true && cancelled === false && noProfessional === false && (
        <div className="flex items-center">
          <h1 className="font-bold">{language.PROFESSIONAL_ACCEPTED}</h1>
          <FiCheck className="text-2xl p-1 rounded-full dark:text-white mx-1" style={{ color: "green" }} />
          {/* <FiX onClick={() => completed(id)} className="bg-red-500 text-2xl p-1 rounded-full text-white mx-1" /> */}
        </div>
      )}
      {noProfessional && (
        <div className="flex items-center">
          <h1 className="font-bold text-red-500">{language.NO_PROFESSIONALS_AVAILABLE}</h1>
          {/* <FiX onClick={() => completed(id)} className="bg-red-500 text-2xl p-1 rounded-full text-white mx-1" /> */}
        </div>
      )}
      {cancelled === true && (
        <div className="flex items-center">
          <h1 className="font-bold text-red-500">{language.CANCELLED}</h1>
          <FiX className="text-2xl text-red-500 p-1 rounded-full mx-1" />
          {/* <FiX onClick={() => completed(id)} className="bg-red-500 text-2xl p-1 rounded-full text-white mx-1" /> */}
        </div>
      )}
      {type === "TIMER" && completeStatus === false && cancelled === false && (
        <div className="flex items-center">
          <p className="mx-2">{new Date(timer).toISOString().substr(11, 8)}</p>
          <FiX onClick={cancelRide} className="bg-red-500 text-2xl p-1 rounded-full text-white" />
        </div>
      )}
      {type !== "TIMER" && completeStatus === false && cancelled === false && (
        <div className="flex items-center">
          <FiRefreshCcw
            onClick={() => fetchDetails(true)}
            className="cursor-pointer bg-blue-800 text-2xl p-1 rounded-full text-white"
          />
          {noProfessional === false && (
            <FiUserPlus
              onClick={() => setManualAssign(id)}
              className="cursor-pointer mx-2 bg-blue-800 text-2xl p-1 rounded-full text-white"
            />
          )}
          <FiX onClick={cancelRide} className="cursor-pointer bg-red-500 text-2xl p-1 rounded-full text-white" />
        </div>
      )}
    </div>
  );
}
