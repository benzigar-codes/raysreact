import React from "react";
import { AiOutlineCar } from "react-icons/ai";
import { FiLoader, FiMail, FiPhone, FiStar, FiUser, FiX } from "react-icons/fi";
import { HiOutlineLocationMarker } from "react-icons/hi";
import OutsideClickHandler from "react-outside-click-handler";
import axios from "axios";

import A from "../../utils/API.js";
import NavLinks from "../../utils/navLinks.json";

import useAdmin from "../../hooks/useAdmin";
import useLanguage from "../../hooks/useLanguage";
import usePrompt from "../../hooks/usePrompt";
import useSettings from "../../hooks/useSettings";
import useUtils from "../../hooks/useUtils";
import { FieldWrapper } from "../common/FieldWrapper";
import { Button } from "../common/Button";
import { Heading } from "../common/Heading";
import { Prompt } from "../common/Prompt";
import { PopUp } from "../common/PopUp";

export default function ManualAssign({
  completedBookingId = () => {},
  manualAssign = "",
  setManualAssign = () => {},
  fetchTable = () => {},
}) {
  const id = manualAssign;
  const { language } = useLanguage();
  const { header, authFailure } = useAdmin();
  const { prompt, showPrompt } = usePrompt();
  const { parseError } = useUtils();
  const { settings } = useSettings();

  const distanceFromSettings = settings.retryRequestDistance;
  const availableDistance = [0, 500, 1000, 1500, 2000];
  const [selectedDistance, setSelectedDistance] = React.useState(0);

  const [popup, setPop] = React.useState(null);
  const [loading, setLoading] = React.useState(true);
  const [drivers, setDrivers] = React.useState([]);

  React.useEffect(() => {
    const fetchDrivers = async () => {
      setLoading(true);
      try {
        const { data } = await axios.post(
          A.HOST + A.ADMIN_RIDES_SCHEDULED_ASSIGN_DRIVER_LIST,
          {
            bookingId: id,
            checkRadius: selectedDistance,
          },
          header
        );
        setDrivers(data.response);
        setLoading(false);
      } catch (err) {
        alert(parseError(err));
        // authFailure(err);
        setManualAssign(null);
      }
    };
    fetchDrivers();
  }, [selectedDistance]);

  const assignDriver = async (driverId) => {
    setLoading(true);
    try {
      const { data } = await axios.post(
        A.HOST + A.ADMIN_RIDES_SCHEDULED_ASSIGN_DRIVER_REQUEST,
        {
          bookingId: id,
          professionalId: driverId,
        },
        header
      );
      setManualAssign(null);
      fetchTable((data) => data + 1);
      completedBookingId(id);
    } catch (err) {
      authFailure(err);
      alert(parseError(err));
      setPop({ title: parseError(err), type: "error" });
    }
    setLoading(false);
  };

  return (
    <div
      className="fixed inset-0 flex justify-center items-center bg-black bg-opacity-50 dark:text-white"
      style={{ zIndex: 100 }}
    >
      {popup != null && <PopUp unmount={() => setPop(null)} title={popup.title} type={popup.type} />}
      {prompt.show === true && (
        <Prompt
          title={prompt.title}
          message={prompt.message}
          type={prompt.type}
          close={prompt.close}
          click={prompt.click}
        />
      )}
      <OutsideClickHandler
        onOutsideClick={() => {
          setTimeout(() => setManualAssign(null), 100);
        }}
      >
        <FiX
          onClick={() => setTimeout(() => setManualAssign(null), 100)}
          className="cursor-pointer fixed right-0 top-0 text-4xl m-5 bg-white text-black p-1 rounded-full"
          style={{ zIndex: 110 }}
        />

        <div
          className="p-2 bg-white rounded-xl bg-gray-100 dark:bg-gray-800 overflow-y-scroll flex flex-col"
          style={{ height: "80vh", width: "90vw" }}
        >
          <div className="w-full flex items-center">
            <FieldWrapper title={language.SEARCH_RADIUS}>
              {availableDistance.map((distance) => (
                <Button
                  disabled={loading}
                  onClick={() => setSelectedDistance(distance)}
                  className={distance === selectedDistance && "bg-blue-800 dark:text-white border-blue-800"}
                  title={(distanceFromSettings + distance) / 1000 + " KM"}
                />
              ))}
            </FieldWrapper>
            {loading && <FiLoader className={"animate-spin mx-2"} />}
          </div>
          {loading === true ? null : (
            <div className="flex flex-wrap w-full">
              {drivers && drivers.length === 0 && (
                <Heading title={<div className={"flex items-center"}>{language.NO_PROFESSIONALS_AVAILABLE}</div>} />
              )}
              {drivers &&
                drivers.length > 0 &&
                drivers.map((driver) => (
                  <div
                    onClick={() =>
                      showPrompt(language.ASSIGN_DRIVER, language.PROMPT_ASSIGN_DRIVER, "status", () =>
                        assignDriver(driver.professionalId)
                      )
                    }
                    className="w-1/4 dark:text-white"
                  >
                    <div className="flex flex-wrap items-center cursor-pointer transition m-3 p-3 border-2 hover:border-green-800 rounded-xl dark:border-gray-500">
                      <div className="w-1/5">
                        <FiUser className={"text-blue-800"} />
                      </div>
                      <div className="w-4/5">{driver.firstName + " " + driver.lastName}</div>
                      <div className="w-1/5 mt-2 truncate" title={driver.firstName + " " + driver.lastName}>
                        <FiMail className={"text-blue-800"} />
                      </div>
                      <div className="w-4/5 mt-2 truncate" title={driver.email}>
                        {driver.email}
                      </div>
                      <div className="w-1/5 mt-2">
                        <FiPhone className={"text-blue-800"} />
                      </div>
                      <div className="w-4/5 mt-2 truncate" title={driver.phone.code + " " + driver.phone.number}>
                        {driver.phone.code + " " + driver.phone.number}
                      </div>
                      <div className="w-1/5 mt-2">
                        <AiOutlineCar className={"text-blue-800"} />
                      </div>
                      <div className="w-4/5 mt-2 truncate" title={driver.vehicleCategoryName}>
                        {driver.vehicleCategoryName}
                      </div>
                      <div className="w-1/5 mt-2">
                        <FiStar className={"text-blue-800"} />
                      </div>
                      <div className="w-4/5 mt-2 truncate" title={driver.avgRating}>
                        {driver.avgRating}
                      </div>
                      <div className="w-1/5 mt-2">
                        <HiOutlineLocationMarker className={"text-blue-800"} />
                      </div>
                      <div className="w-4/5 mt-2 truncate" title={driver.avgRating}>
                        {driver.distance + " KM"}
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          )}
        </div>
      </OutsideClickHandler>
    </div>
  );
}
