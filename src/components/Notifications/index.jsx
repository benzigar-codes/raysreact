import gsap from "gsap/gsap-core";
import React from "react";
import { AiFillExclamationCircle, AiOutlineLoading3Quarters } from "react-icons/ai";
import { FiBell } from "react-icons/fi";
import { HiCalendar, HiOutlineSpeakerphone } from "react-icons/hi";
import OutsideClickHandler from "react-outside-click-handler";
import axios from "axios";
import { Howl, Howler } from "howler";

import MainMenuIcons from "../../pages/admin/home/MainMenuIcons";
import useLanguage from "../../hooks/useLanguage";
import useSocket from "../../hooks/useSocket";

import notificationSound from "../../assets/sounds/notification.mp3";

import A from "../../utils/API.js";
import NavLinks from "../../utils/navLinks.json";
import useAdmin from "../../hooks/useAdmin";
import { format } from "date-fns/esm";
import { useHistory } from "react-router";
import useUtils from "../../hooks/useUtils";

const Notification = ({
  close,
  notificationData = [],
  removeOnlyMeNotification = (e) => {},
  rideNotification = false,
}) => {
  const history = useHistory();
  const { truncate } = useUtils();
  const { language } = useLanguage();
  React.useEffect(() => {
    gsap.fromTo(".notifications", 0.2, { opacity: 0, y: 80 }, { opacity: 1, y: 30, stagger: 0.1 });
  }, []);
  return (
    <div className="opacity-0 notifications">
      <div
        className="absolute bg-white justify-between dark:bg-gray-900 rounded-lg shadow-xl overflow-y-scroll flex flex-col"
        style={{ zIndex: 10, top: "180%", width: 360, height: 400, right: rideNotification === false ? 0 : 400 }}
      >
        <h1 className="bg-blue-800 w-full px-4 py-2 text-sm text-center text-white">
          {rideNotification === false ? language.SECURITY_FEEDBACKS : language.RIDE_NOTIFICATION}
        </h1>
        {(notificationData.length === 0 ||
          notificationData.filter((notify) => notify.category === "RIDE").length === 0) &&
          rideNotification === true && (
            <p className="text-center text-sm text-gray-500 py-5 h-full">{language.NO_NOTIFICATION}</p>
          )}
        {(notificationData.length === 0 ||
          notificationData.filter((notify) => notify.category === "REPORT" || notify.category === "SECURITY").length ===
            0) &&
          rideNotification === false && (
            <p className="text-center text-sm text-gray-500 py-5 h-full">{language.NO_NOTIFICATION}</p>
          )}
        <div className="w-full h-full overflow-y-scroll">
          {notificationData.length > 0 &&
            notificationData.map((each) => {
              if (each.category === "REPORT" && rideNotification === false) {
                return (
                  <div
                    key={each._id}
                    onClick={() => {
                      close();
                      history.push(NavLinks.ADMIN_REPORTS_FEEDBACK_VIEW + "/" + each.id);
                    }}
                    className="flex hover:bg-gray-100 cursor-pointer py-3 px-4 hover:bg-gray-200 border-b-2 dark:bg-gray-900 dark:border-gray-700"
                  >
                    <div className={`flex justify-center items-center bg-green-800 w-1/6`}>
                      <HiOutlineSpeakerphone className="text-white text-4xl p-2" />
                    </div>
                    <div className="w-5/6">
                      <div className="flex justify-between items-center">
                        <p className="text-sm px-2 text-gray-600 dark:text-gray-200 font-bold">
                          {each.firstName + " " + each.lastName}
                        </p>
                        <p className="text-sm px-2 text-gray-600 dark:text-gray-200" style={{ fontSize: 12 }}>
                          {each.phoneCode + " " + each.phoneNumber}
                        </p>
                      </div>
                      <p className="text-sm px-2 text-gray-600 dark:text-gray-200">{language.REPORTED_A_ISSUE}</p>
                      <p className="text-sm px-2 text-gray-600 dark:text-gray-200">
                        {truncate(each.comment || "", 30)}
                      </p>
                      <p className="px-2 text-sm text-gray-500 text-right w-full" style={{ fontSize: 12 }}>
                        {each.time}
                      </p>
                    </div>
                  </div>
                );
              } else if (each.category === "RIDE" && rideNotification === true) {
                return (
                  <div
                    onClick={() => {
                      close();
                      history.push(NavLinks.ADMIN_RIDES_SCHEDULED_ASSIGN_DRIVER + "/" + each.id);
                    }}
                    className="flex hover:bg-gray-100 cursor-pointer py-3 px-4 hover:bg-gray-200 border-b-2 dark:bg-gray-900 dark:border-gray-700"
                  >
                    <div className={`flex justify-center items-center bg-green-800 w-1/6`}>
                      <HiCalendar className="text-white text-4xl p-2" />
                    </div>
                    <div className="w-5/6">
                      <div className="flex justify-between items-center">
                        <p className="text-sm px-2 text-gray-600 dark:text-gray-200 font-bold">{each.name}</p>
                        <p className="text-sm px-2 text-gray-600 dark:text-gray-200" style={{ fontSize: 12 }}>
                          {each.phoneCode + " " + each.phoneNumber}
                        </p>
                      </div>
                      <p className="text-sm px-2 text-gray-600 dark:text-gray-200">
                        {each.bookingStatus === "AWAITING"
                          ? language.SCHEDULE_DRIVER_UNACCEPTED
                          : language.DRIVER_CANCELLED}
                      </p>
                      <p className="px-2 text-sm text-gray-500 text-right w-full" style={{ fontSize: 12 }}>
                        {language.BOOKING_TIME} : {each.time}
                      </p>
                    </div>
                  </div>
                );
              } else if (each.category === "SECURITY" && rideNotification === false) {
                return (
                  <div
                    onClick={() => {
                      close();
                      history.push(NavLinks.ADMIN_SECURITY_REQUESTS_VIEW + "/" + each.id + "?type=NEW");
                    }}
                    className="relative flex hover:bg-gray-100 cursor-pointer py-3 px-4 hover:bg-gray-200 border-b-2 dark:bg-gray-900 dark:border-gray-700"
                  >
                    <div className={`flex justify-center items-center bg-red-500 w-1/6`}>
                      <AiFillExclamationCircle className="text-white text-4xl p-2" />
                    </div>
                    <div className="w-5/6">
                      <div className="flex justify-between items-center">
                        <p className="text-sm px-2 text-gray-600 dark:text-gray-200 font-bold">
                          {each.firstName + " " + each.lastName}
                        </p>
                        <p className="text-sm px-2 text-gray-600 dark:text-gray-200" style={{ fontSize: 12 }}>
                          {each.phoneCode + " " + each.phoneNumber}
                        </p>
                      </div>
                      {each.category === "SECURITY" && each.subCategory === "EMERGENCY" && (
                        <p className="text-sm px-2 text-gray-600 dark:text-gray-200">{language.INITIATED_EMERGENCY}</p>
                      )}
                      {each.category === "SECURITY" && each.subCategory === "SOS" && (
                        <p className="text-sm px-2 text-gray-600 dark:text-gray-200">{language.INITIATED_SOS}</p>
                      )}
                      {each.category === "SECURITY" && each.subCategory === "TRACKING" && (
                        <p className="text-sm px-2 text-gray-600 dark:text-gray-200">{language.INITIATED_TRACKING}</p>
                      )}
                      <p className="px-2 text-sm text-gray-500 text-right w-full" style={{ fontSize: 12 }}>
                        {each.time}
                      </p>
                    </div>
                  </div>
                );
              } else if (each.category === "SECURITY_END" && rideNotification === false) {
                return (
                  <div
                    onClick={() => {
                      close();
                      history.push(NavLinks.ADMIN_SECURITY_REQUESTS_VIEW + "/" + each.id + "?type=NEW");
                      removeOnlyMeNotification(each.id);
                    }}
                    className="relative flex hover:bg-gray-100 cursor-pointer py-3 px-4 hover:bg-gray-200 border-b-2 dark:bg-gray-900 dark:border-gray-700"
                  >
                    <div className={`flex justify-center items-center bg-blue-800 w-1/6`}>
                      <AiFillExclamationCircle className="text-white text-4xl p-2" />
                    </div>
                    <div className="w-5/6">
                      <div className="flex justify-between items-center">
                        <p className="text-sm px-2 text-gray-600 dark:text-gray-200 font-bold">
                          {language.ESCORT_ENDED_NOTIFICATION}
                        </p>
                        <p className="text-sm px-2 text-gray-600 dark:text-gray-200" style={{ fontSize: 12 }}>
                          {each.phoneCode + " " + each.phoneNumber}
                        </p>
                      </div>
                      <p className="text-md px-2 text-gray-600 dark:text-gray-200" style={{ fontSize: 12 }}>
                        {each.firstName + " " + each.lastName}
                      </p>
                      <p className="px-2 text-sm text-gray-500 text-right w-full" style={{ fontSize: 12 }}>
                        {each.time}
                      </p>
                    </div>
                  </div>
                );
              }
              return null;
            })}
        </div>
        {rideNotification === false && (
          <div
            onClick={() => {
              close();
              history.push(NavLinks.ADMIN_SECURITY_REQUESTS + "?showNotificationFilter=true");
            }}
            className="w-full px-4 py-2 bg-red-500 cursor-pointer"
          >
            <h1 className="text-white text-center">{language.SEE_ALL_NOTIFICATIONS}</h1>
          </div>
        )}
      </div>
      {/* <div
        className="absolute bg-blue-800"
        style={{
          zIndex: 5,
          transform: "rotateZ(45deg)",
          right: rideNotification === false ? 10 : 410,
          top: -10,
          width: 25,
          height: 25,
        }}
      ></div> */}
    </div>
  );
};

export default function Index() {
  const [showNotification, setNotification] = React.useState(false);
  const [showAlert, setShowAlert] = React.useState(false);
  const { header, authFailure, token } = useAdmin();
  const [notificationData, setNotificationData] = React.useState([]);
  const [onlyMeNotification, setOnlyMeNotification] = React.useState([]);
  const [firstLoad, setFirstLoad] = React.useState(true);
  const { language } = useLanguage();
  const { notificationSocket } = useSocket();
  const sound = new Howl({
    src: notificationSound,
  });

  const fetchData = async () => {
    try {
      const { data } = await axios.post(A.HOST + A.ADMIN_DASHBOARD_NOTIFICATION, {}, header);
      setNotificationData([
        ...[
          ...data.reportsData,
          ...data.securityData,
          ...data.scheduleData,
          ...data.professionalCancelData,
          ...onlyMeNotification,
        ]
          .sort(function (a, b) {
            return new Date(b.createdAt) - new Date(a.createdAt);
          })
          // .map((each) => console.log({ id: each._id, time: format(new Date(each.createdAt), "p") }));
          .map((each) => {
            if (each.userType) {
              return {
                id: each._id,
                category: "REPORT",
                subCategory: null,
                firstName: each.firstName,
                lastName: each.lastName,
                phoneCode: each.phone?.code,
                phoneNumber: each.phone?.number,
                comment: each.comment,
                time: format(new Date(each.createdAt), "do LLL  p"),
              };
            } else if (each.bookingFor) {
              return {
                id: each._id,
                name: each.bookingFor.name === "GUEST" ? language["GUEST"] : each.bookingFor.name,
                phoneCode: each.bookingFor?.phoneCode,
                phoneNumber: each.bookingFor?.phoneNumber,
                bookingStatus: each.bookingStatus,
                category: "RIDE",
                time: format(new Date(each.bookingDate), "do LLL  p"),
              };
            } else if (each.category === "SECURITY_END") {
              return {
                id: each._id,
                category: "SECURITY_END",
                firstName: each.firstName,
                lastName: each.lastName,
                phoneCode: each.phoneCode,
                phoneNumber: each.phoneNumber,
                time: format(new Date(each.createdAt), "do LLL  p"),
              };
            } else {
              return {
                id: each._id,
                category: "SECURITY",
                subCategory: each.alertType,
                firstName: each.firstName,
                lastName: each.lastName,
                phoneCode: each.phone?.code,
                phoneNumber: each.phone?.number,
                time: format(new Date(each.createdAt), "do LLL  p"),
              };
            }
          }),
      ]);
      setShowAlert(
        firstLoad === true && (data.reportsData.length !== 0 || data.securityData.length !== 0) ? true : false
      );
      setFirstLoad(false);
    } catch (err) {
      authFailure(err);
    }
  };

  React.useEffect(() => console.log(notificationData), [notificationData]);

  React.useEffect(() => {
    // setOnlyMeNotification([
    //   ...onlyMeNotification,
    //   {
    //     _id: "",
    //     firstName: "Benzigar",
    //     lastName: "Codes",
    //     phoneCode: "+91",
    //     phoneNumber: "9791442121",
    //     category: "SECURITY_END",
    //     createdAt: new Date(),
    //   },
    // ]);
    notificationSocket.current.on("notifyAdmin", () => {
      setShowAlert(true);
      sound.play();
    });
    notificationSocket.current.on(token, (data) => {
      console.log(data);
      if (data.action === "ESCORTARRIVED") {
        setShowAlert(true);
        setOnlyMeNotification([
          ...onlyMeNotification,
          {
            _id: data?.details?._id || "",
            firstName: data?.details?.user?.firstName || "",
            lastName: data?.details?.user?.lastName || "",
            phoneCode: data?.details?.user?.phone?.code || "",
            phoneNumber: data?.details?.user?.phone?.number || "",
            category: "SECURITY_END",
            createdAt: new Date(),
          },
        ]);
        sound.play();
      }
    });
    fetchData();
  }, []);

  const close = () => {
    gsap.fromTo(".notifications", 0.2, { opacity: 1, y: 30 }, { opacity: 0, y: 80, stagger: 0.1 });
    setTimeout(() => setNotification(false), 400);
  };

  return (
    <MainMenuIcons
      alert={showAlert}
      onClick={() => {
        if (showNotification === true) close();
        else {
          fetchData();
          setNotification(true);
          setShowAlert(false);
        }
      }}
      icon={notificationData === null ? <AiOutlineLoading3Quarters className="animate-spin" /> : <FiBell />}
      children={
        showNotification && (
          <OutsideClickHandler onOutsideClick={close}>
            <Notification
              removeOnlyMeNotification={(e) =>
                setOnlyMeNotification(onlyMeNotification.filter((each) => each._id !== e))
              }
              close={close}
              notificationData={notificationData}
            />
            <Notification rideNotification={true} close={close} notificationData={notificationData} />
          </OutsideClickHandler>
        )
      }
    />
  );
}
