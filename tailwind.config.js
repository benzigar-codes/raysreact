const colors = require("tailwindcss/colors");

module.exports = {
  purge: {
    enabled: true,
    options: {
      safelist: [
        "mr-1",
        "w-6/8",
        "lg:w-6/8",
        "w-full",
        "lg:w-full",
        "w-3/4",
        "lg:w-3/4",
        "w-5/6",
        "lg:w-5/6",
        "w-1/2",
        "lg:w-1/2",
        "w-2/4",
        "lg:w-2/4",
        "w-4/12",
        "lg:w-4/12",
        "w-8/12",
        "lg:w-8/12",
        "w-4/5",
        "lg:w-4/5",
        "w-8/12",
        "lg:w-8/12",
        "w-2/3",
        "lg:w-2/3",
        "mr-2",
        "mr-3",
        "ml-1",
        "ml-2",
        "ml-3",
        "ml-10",
      ],
    },
    content: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  },
  darkMode: "class", // or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: ["Ubuntu", "sans-serif"],
      serif: ["Poppins", "serif"],
    },
    colors: {
      transparent: "transparent",
      current: "currentColor",
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      indigo: colors.indigo,
      red: colors.rose,
      teal: colors.teal,
      purple: colors.purple,
      amber: colors.amber,
      yellow: colors.amber,
      blue: {
        800: "#3B6BCC",
      },
      green: {
        800: "#282F65", //Darker
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
